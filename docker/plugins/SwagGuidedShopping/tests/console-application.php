<?php declare(strict_types=1);

use Shopware\Core\TestBootstrapper;
use Symfony\Bundle\FrameworkBundle\Console\Application;

/** @var TestBootstrapper $testBootstrapper */
$kernel = require_once __DIR__ . '/TestBootstrap.php';

return new Application($kernel);
