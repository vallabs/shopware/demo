<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Trait;

trait ReflectionTrait
{
    /**
     * @param array<string, mixed> $parameters
     */
    protected function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
