<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Factory;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Mercure\Factory\HubFactory;
use SwagGuidedShopping\Content\Mercure\Struct\JwtProvider;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Exception\NotConfiguredException;
use Symfony\Component\Mercure\Hub;

class HubFactoryTest extends TestCase
{
    /**
     * @dataProvider getTestBuildWithExceptionProviderData
     */
    public function testBuildWithException(
        MockObject $systemConfigService,
        string $exceptionMessage
    ): void
    {
        $factory = $this->getFactory($systemConfigService);
        static::expectException(NotConfiguredException::class);
        static::expectExceptionMessage($exceptionMessage);
        $factory->build('test-jwt-token');
    }

    public static function getTestBuildWithExceptionProviderData(): \Generator
    {
        $self = new HubFactoryTest('test');

        yield 'invalid-hub-url-only' => [
            $self->createSystemConfigServiceMock(false, true),
            'No mercure hub url configured.'
        ];
        yield 'invalid-hub-public-url-only' => [
            $self->createSystemConfigServiceMock(true, false),
            'No mercure hub public url configured.'
        ];
        yield 'invalid-both' => [
            $self->createSystemConfigServiceMock(false, false),
            'No mercure hub url configured.'
        ];
    }

    public function testBuildSuccessfully(): void
    {
        $systemConfigService = $this->createSystemConfigServiceMock(true, true);
        $factory = $this->getFactory($systemConfigService);
        $hubRegistry = $factory->build('test-jwt-token');
        $hub = $hubRegistry->getHub();
        static::assertInstanceOf(Hub::class, $hub);
        static::assertEquals('test-hub-url', $hub->getUrl());
        static::assertEquals('test-hub-public-url', $hub->getPublicUrl());
        static::assertInstanceOf(JwtProvider::class, $hub->getProvider());
        static::assertEquals('test-jwt-token', $hub->getProvider()->getJwt());
        static::assertInstanceOf(JWTMercureTokenFactory::class, $hub->getFactory());
    }

    private function getFactory(
        ?MockObject $systemConfigServiceMock = null
    ): HubFactory
    {
        /** @var MockObject&SystemConfigService $systemConfigService */
        $systemConfigService = $systemConfigServiceMock ?? $this->createMock(SystemConfigService::class);
        /** @var MockObject&JWTMercureTokenFactory $jwtMercureTokenFactory */
        $jwtMercureTokenFactory = $this->createMock(JWTMercureTokenFactory::class);

        return new HubFactory($systemConfigService, $jwtMercureTokenFactory);
    }

    private function createSystemConfigServiceMock(
        bool $hubUrlIsValid,
        bool $hubPublicUrlIsValid
    ): MockObject
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::exactly(2))
            ->method('getString')
            ->willReturnOnConsecutiveCalls(
                $hubUrlIsValid ? 'test-hub-url' : '',
                $hubPublicUrlIsValid ? 'test-hub-public-url' : ''
            );

        return $systemConfigService;
    }
}
