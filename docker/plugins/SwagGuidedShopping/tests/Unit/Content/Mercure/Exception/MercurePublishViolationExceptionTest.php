<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Mercure\Exception\MercurePublishViolationException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class MercurePublishViolationExceptionTest extends TestCase
{
    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testInitialize(ViolationList $violateCollection): void
    {
        $exception = new MercurePublishViolationException($violateCollection);
        static::assertEquals("Could not publish mercure update \n" . $violateCollection, $exception->getMessage());
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__MERCURE_PUBLISH_VIOLATION, $exception->getErrorCode());
    }

    public static function getTestExceptionWithDifferentViolationCollections(): \Generator
    {
        $emptyViolateCollection = new ViolationList(new ConstraintViolationList());
        yield [$emptyViolateCollection];

        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $nonEmptyCollection = new ViolationList($constraintViolationList);
        yield [$nonEmptyCollection];
    }
}
