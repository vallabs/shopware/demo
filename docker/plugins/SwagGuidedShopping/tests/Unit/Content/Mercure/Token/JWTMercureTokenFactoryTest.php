<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Token;

use Firebase\JWT\JWT;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Mercure\Exception\JwtMercureInvalidTopicInTokenException;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Exception\NotConfiguredException;

class JWTMercureTokenFactoryTest extends TestCase
{
    /**
     * @param array<int, string> $subscribe
     * @param array<int, string> $publish
     * @param class-string<\Throwable> $exception
     * @dataProvider getTestCreateWithExceptionProviderData
     */
    public function testCreateWithException(
        array $subscribe,
        array $publish,
        MockObject $systemConfigService,
        string $exception,
        string $exceptionMessage
    ): void
    {
        $factory = $this->getFactory($systemConfigService);
        static::expectException($exception);
        static::expectExceptionMessage($exceptionMessage);
        $factory->create($subscribe, $publish);
    }

    public static function getTestCreateWithExceptionProviderData(): \Generator
    {
        $self = new JWTMercureTokenFactoryTest('test');

        yield 'define-both-subscriber-and-publisher' => [
            ['test-subscribe'],
            ['test-publish'],
            $self->createSystemConfigServiceMock(true, true),
            JwtMercureInvalidTopicInTokenException::class,
            'You are not allowed to define subscriber and publisher topics in one token.'
        ];
        yield 'define-nothing' => [
            [],
            [],
            $self->createSystemConfigServiceMock(true, true),
            JwtMercureInvalidTopicInTokenException::class,
            'You must set at least one subscriber or publisher topic.'
        ];
        yield 'define-only-subscriber' => [
            ['test-subscribe'],
            [],
            $self->createSystemConfigServiceMock(true, false),
            NotConfiguredException::class,
            'No mercure hub Subscriber secret configured.',
        ];
        yield 'define-only-publisher' => [
            [],
            ['test-publish'],
            $self->createSystemConfigServiceMock(false, true),
            NotConfiguredException::class,
            'No mercure hub Publisher secret configured.',
        ];
    }

    /**
     * @param array<int, string> $subscribe
     * @param array<int, string> $publish
     * @dataProvider getTestCreateTokenSuccessfullyProviderData
     */
    public function testCreateSubscriberTokenSuccessfully(
        array $subscribe,
        array $publish,
        string $expectToken
    ): void
    {
        $systemConfigService = $this->createSystemConfigServiceMock(true, true);
        $factory = $this->getFactory($systemConfigService);
        $token = $factory->create($subscribe, $publish);
        static::assertEquals($expectToken, $token);
    }

    public static function getTestCreateTokenSuccessfullyProviderData(): \Generator
    {
        yield 'create-subscriber-token' => [
            ['test-subscribe'],
            [],
            JWT::encode([
                'mercure' => [
                    'subscribe' => ['test-subscribe'],
                    'publish' => [],
                ]
            ], 'test-subscriber-secret', 'HS256')
        ];
        yield 'create-publish-token' => [
            [],
            ['test-publish'],
            JWT::encode([
                'mercure' => [
                    'subscribe' => [],
                    'publish' => ['test-publish'],
                ]
            ], 'test-publisher-secret', 'HS256')
        ];
    }

    private function getFactory(
        ?MockObject $systemConfigServiceMock = null
    ): JWTMercureTokenFactory
    {
        /** @var MockObject&SystemConfigService $systemConfigService */
        $systemConfigService = $systemConfigServiceMock ?? $this->createMock(SystemConfigService::class);
        return new JWTMercureTokenFactory($systemConfigService);
    }

    private function createSystemConfigServiceMock(
        bool $publisherSecretIsValid,
        bool $subscriberSecretIsValid
    ): MockObject
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::exactly(2))
            ->method('getString')
            ->willReturnOnConsecutiveCalls(
                $publisherSecretIsValid ? 'test-publisher-secret' : '',
                $subscriberSecretIsValid ? 'test-subscriber-secret' : '',
            );

        return $systemConfigService;
    }
}
