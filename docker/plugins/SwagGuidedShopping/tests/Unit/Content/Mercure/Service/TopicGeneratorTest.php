<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Service;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;

/**
 * @covers \SwagGuidedShopping\Content\Mercure\Service\TopicGenerator
 *
 * @internal
 */
class TopicGeneratorTest extends TestCase
{
    /**
     * @dataProvider getGuideSubscriberTopicsData
     */
    public function testGenerateGuideSubscriberTopics(int $expectedCount, string $appointmentId, string $appointmentMode): void
    {
        $topicGenerator = new TopicGenerator();
        $topics = $topicGenerator->generateGuideSubscriberTopics($appointmentId, $appointmentMode);

        static::assertCount($expectedCount, $topics);
    }

    public static function getGuideSubscriberTopicsData(): \Generator
    {
        $appointmentId = 'test_appointment_1';

        yield 'guided' => [
            4,
            $appointmentId,
            AppointmentDefinition::MODE_GUIDED,
        ];

        yield 'self' => [
            0,
            $appointmentId,
            AppointmentDefinition::MODE_SELF,
        ];
    }

    /**
     * @dataProvider generateGuidePublisherTopicData
     */
    public function testGenerateGuidePublisherTopic(?string $expected, string $appointmentId, string $appointmentMode): void
    {
        $topicGenerator = new TopicGenerator();
        $topic = $topicGenerator->generateGuidePublisherTopic($appointmentId, $appointmentMode);

        static::assertEquals($expected, $topic);
    }

    public static function generateGuidePublisherTopicData(): \Generator
    {
        $appointmentId = 'test_appointment_1';

        yield 'guided' => [
            'gs-guide-actions-' . $appointmentId,
            $appointmentId,
            AppointmentDefinition::MODE_GUIDED,
        ];

        yield 'self' => [
            null,
            $appointmentId,
            AppointmentDefinition::MODE_SELF,
        ];
    }

    /**
     * @dataProvider getGenerateClientSubscriberTopicsData
     */
    public function testGenerateClientSubscriberTopics(int $expectedCount, string $appointmentId, string $appointmentMode): void
    {
        $topicGenerator = new TopicGenerator();
        $topics = $topicGenerator->generateClientSubscriberTopics($appointmentId, $appointmentMode);

        static::assertCount($expectedCount, $topics);
    }

    public static function getGenerateClientSubscriberTopicsData(): \Generator
    {
        $appointmentId = 'test_appointment_1';

        yield 'guided' => [
            3,
            $appointmentId,
            AppointmentDefinition::MODE_GUIDED,
        ];

        yield 'self' => [
            0,
            $appointmentId,
            AppointmentDefinition::MODE_SELF,
        ];
    }

    /**
     * @dataProvider getGenerateClientPublisherTopicData
     */
    public function testGenerateClientPublisherTopic(?string $expected, string $appointmentId, string $appointmentMode): void
    {
        $topicGenerator = new TopicGenerator();
        $topic = $topicGenerator->generateClientPublisherTopic($appointmentId, $appointmentMode);

        static::assertEquals($expected, $topic);
    }

    public static function getGenerateClientPublisherTopicData(): \Generator
    {
        $appointmentId = 'test_appointment_1';

        yield 'guided' => [
            'gs-client-actions-' . $appointmentId,
            $appointmentId,
            AppointmentDefinition::MODE_GUIDED,
        ];

        yield 'self' => [
            null,
            $appointmentId,
            AppointmentDefinition::MODE_SELF,
        ];
    }
}
