<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Service;

use SwagGuidedShopping\Content\Mercure\Service\AbstractPublisher;

class PublisherFake extends AbstractPublisher
{
    /**
     * @var array<int, array<string, mixed>>
     */
    private array $publishHistory = [];

    private bool $throwException;

    public function __construct(?bool $throwException = false)
    {
        $this->throwException = $throwException ?? false;
    }

    public function publish(
        string $mercurePublisherToken,
        array $updateData,
        string $topic
    ): string
    {
        if ($this->throwException) {
            throw new \RuntimeException('Fake exception');
        }

        $data = [];
        $data['message-id'] = 'message-id-uuuid-asdf-asdf-asdf-asdf';
        $data['topic'] = $topic;
        $data['update'] = $updateData;

        $this->publishHistory[] = $data;

        $encoded = \json_encode($data);
        if (!\is_string($encoded)) {
            throw new \RuntimeException('Could not encode data');
        }

        return $encoded;
    }

    /**
     * @return array<int, array<string, mixed>>
     */
    public function getPublishHistory(): array
    {
        return $this->publishHistory;
    }
}
