<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Service;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Mercure\Factory\HubFactory;
use SwagGuidedShopping\Content\Mercure\Service\Publisher;
use SwagGuidedShopping\Exception\NotConfiguredException;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\HubRegistry;
use Symfony\Component\Mercure\Update;

class PublisherTest extends TestCase
{
    public function testPublishButGetExceptionWhenBuildTheHub(): void
    {
        $mercurePublisherToken = 'test-mercure-publisher-token';
        $hubFactory = $this->createMock(HubFactory::class);
        $hubFactory->expects(static::once())
            ->method('build')
            ->with(static::equalTo($mercurePublisherToken))
            ->willThrowException(
                new NotConfiguredException('No mercure hub url configured.')
            );

        $publisher = new Publisher($hubFactory);
        static::expectException(NotConfiguredException::class);
        $publisher->publish($mercurePublisherToken, [], 'test-topic');
    }

    public function testPublishWithInvalidUpdateData(): void
    {
        $mercurePublisherToken = 'test-mercure-publisher-token';
        $hubFactory = $this->createMock(HubFactory::class);
        $hub = $this->createMock(HubInterface::class);
        $hub->expects(static::never())->method('publish');
        $hubFactory->expects(static::once())
            ->method('build')
            ->with(static::equalTo($mercurePublisherToken))
            ->willReturn(new HubRegistry($hub));

        $publisher = new Publisher($hubFactory);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('Invalid update payload.');
        $publisher->publish($mercurePublisherToken, [\fopen('php://stdin', 'r')], 'test-topic');
    }

    public function testPublishSuccessfully(): void
    {
        $updateData = ['test-update-data'];
        $testTopic = 'test-topic';
        $mercurePublisherToken = 'test-mercure-publisher-token';
        $hubFactory = $this->createMock(HubFactory::class);
        $hub = $this->createMock(HubInterface::class);
        /** @var non-empty-string&string $encodedUpdateData */
        $encodedUpdateData = \json_encode($updateData);
        $expectUpdateData = new Update($testTopic, $encodedUpdateData, true);
        $hub->expects(static::once())
            ->method('publish')
            ->with($expectUpdateData)
            ->willReturn('test-hub-publish');
        $hubFactory->expects(static::once())
            ->method('build')
            ->with(static::equalTo($mercurePublisherToken))
            ->willReturn(new HubRegistry($hub));

        $publisher = new Publisher($hubFactory);
        $result = $publisher->publish($mercurePublisherToken, $updateData, $testTopic);
        static::assertEquals('test-hub-publish', $result);
    }
}
