<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Service;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Mercure\Exception\MercurePublishViolationException;
use SwagGuidedShopping\Content\Mercure\Service\AbstractPublisher;
use SwagGuidedShopping\Content\Mercure\Service\RequestProcessor;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;

class RequestProcessorTest extends TestCase
{
    /**
     * @dataProvider getTestProcessWithInvalidRequestParametersProviderData
     */
    public function testProcessWithInvalidRequestParameters(
        Request $request
    ): void
    {
        $validator = new DataValidator(Validation::createValidator());

        $publisher = $this->createMock(AbstractPublisher::class);
        $publisher->expects(static::never())->method('publish');

        $processor = new RequestProcessor($publisher, $validator);
        static::expectException(MercurePublishViolationException::class);
        $processor->process($request);
    }

    public static function getTestProcessWithInvalidRequestParametersProviderData(): \Generator
    {
        yield 'update-param-is-string' => [
            new Request(
                [], [], [], [], [],
                ['HTTP_gs-jwt-mercure-token' => 'test-mercure-token'],
                '{"update": "test-update", "topic": "test-topic"}'
            )
        ];
        yield 'topic-param-is-blank' => [
            new Request(
                [], [], [], [], [],
                ['HTTP_gs-jwt-mercure-token' => 'test-mercure-token'],
                '{"update": {"update-key": "update-value"}, "topic": ""}'
            )
        ];
        yield 'topic-param-is-array' => [
            new Request(
                [], [], [], [], [],
                ['HTTP_gs-jwt-mercure-token' => 'test-mercure-token'],
                '{"update": {"update-key": "update-value"}, "topic": {}}'
            )
        ];
        yield 'mercure-token-is-blank' => [
            new Request(
                [], [], [], [], [],
                ['HTTP_gs-jwt-mercure-token' => ''],
                '{"update": {"update-key": "update-value"}, "topic": "test-topic"}'
            )
        ];
        yield 'mercure-token-is-array' => [
            new Request(
                [], [], [], [], [],
                ['HTTP_gs-jwt-mercure-token' => []],
                '{"update": {"update-key": "update-value"}, "topic": "test-topic"}'
            )
        ];
    }

    public function testProcessSuccessfully(): void
    {
        $request = new Request(
            [], [], [], [], [],
            ['HTTP_gs-jwt-mercure-token' => 'test-mercure-token'],
            '{"update": {"update-key": "update-value"}, "topic": "test-topic"}'
        );

        $validator = $this->createMock(DataValidator::class);
        $constraint = new Collection(
            [
                'update' => new Type('array'),
                'topic' => [new Type('string'), new NotBlank()],
                'gs-jwt-mercure-token' => [new Type('string'), new NotBlank()],
            ]
        );
        $constraint->allowExtraFields = true;
        $validator->expects(static::once())
            ->method('validate')
            ->with(
                static::equalTo([
                    'update' => ['update-key' => 'update-value'],
                    'topic' => 'test-topic',
                    'gs-jwt-mercure-token' => 'test-mercure-token'
                ]),
                static::equalTo($constraint)
            );

        $publisher = $this->createMock(AbstractPublisher::class);
        $publisher->expects(static::once())
            ->method('publish')
            ->with(
                static::equalTo('test-mercure-token'),
                static::equalTo(['update-key' => 'update-value']),
                static::equalTo('test-topic')
            )
            ->willReturn('test-publish');

        $processor = new RequestProcessor($publisher, $validator);
        $result = $processor->process($request);
        static::assertEquals('test-publish', $result);
    }
}
