<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Mercure\Struct;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Mercure\Struct\JwtProvider;

class JwtProviderTest extends TestCase
{
    public function testGetJwt(): void
    {
        $jwtProvider = new JwtProvider('test-token');
        static::assertEquals('test-token', $jwtProvider->getJwt());
    }
}
