<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\CartInsights\Struct;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\CartInsights\Struct\CartInsightsStruct;

class CartInsightsStructTest extends TestCase
{
    public function testInitializeWithoutTopProducts(): void
    {
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('$');
        $struct = new CartInsightsStruct(1.0, [], 3, $currency);
        static::assertEquals(1.0, $struct->getCartSum());
        static::assertEquals(3, $struct->getProductCount());
        static::assertEquals('test-currency-id', $struct->getCurrencyId());
        static::assertEquals('$', $struct->getCurrencySymbol());
        static::assertEquals([], $struct->getTopProducts());
    }

    public function testInitializeWithTopProducts(): void
    {
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('$');
        $struct = new CartInsightsStruct(13.5,
            [
                'byQuantity' => [
                    'test-product-id-2' => 7,
                    'test-product-id' => 3,
                ],
                'byRevenue' => [
                    'test-product-id' => 7.0,
                    'test-product-id-2' => 6.0
                ],
            ], 7, $currency
        );
        static::assertEquals(13.5, $struct->getCartSum());
        static::assertEquals(7, $struct->getProductCount());
        static::assertEquals('test-currency-id', $struct->getCurrencyId());
        static::assertEquals('$', $struct->getCurrencySymbol());
        $expectTopProducts = [
            'byQuantity' => [
                ['productId' => 'test-product-id-2', 'value' => 7],
                ['productId' => 'test-product-id', 'value' => 3]
            ],
            'byRevenue' => [
                ['productId' => 'test-product-id', 'value' => 7.0],
                ['productId' => 'test-product-id-2', 'value' => 6.0]
            ]
        ];
        static::assertEquals($expectTopProducts, $struct->getTopProducts());
    }
}
