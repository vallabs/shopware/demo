<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\CartInsights\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartCalculator;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Tax\Struct\CalculatedTaxCollection;
use Shopware\Core\Checkout\Cart\Tax\Struct\TaxRuleCollection;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\Widgets\CartInsights\Service\CartStatisticsService;
use SwagGuidedShopping\Core\Checkout\Cart\Service\AttendeeCartService;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CartDataMockBuilder;
use Symfony\Component\HttpFoundation\Request;

class CartStatisticsServiceTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    /**
     * @dataProvider getTestCreateWithInvalidAppointmentProviderData
     *
     * @param AppointmentEntity[] $appointments
     */
    public function testCreateWithInvalidAppointment(
        array $appointments
    ): void
    {
        $appointmentId = 'test-appointment-id';

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                   AppointmentDefinition::ENTITY_NAME,
                     \count($appointments),
                    new AppointmentCollection($appointments),
                    null,
                    new Criteria(),
                    $this->context
                )
            );

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::never())->method('build');

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $attendeeCartService->expects(static::never())->method('getCartData');

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method('create');

        $cartCalculator = $this->createMock(CartCalculator::class);
        $cartCalculator->expects(static::never())->method('calculate');

        $cartStatisticsService = $this->getCartStatisticsService(
            $appointmentRepository,
            $stateServiceFactory,
            $attendeeCartService,
            $channelContextFactory,
            $cartCalculator
        );

        static::expectException(EntityNotFoundException::class);
        $cartStatisticsService->create($appointmentId, new Request(), $this->context);
    }

    public static function getTestCreateWithInvalidAppointmentProviderData(): \Generator
    {
        yield 'appointment-not-found' => [
            []
        ];

        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setId('test-appointment-id');
        $appointment->setSalesChannelDomain(null);
        yield 'appointment-without-sales-channel-domain' => [
            [$appointment]
        ];
    }

    public function testCreateButGetExceptionWhenGettingCartData(): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    AppointmentDefinition::ENTITY_NAME,
                    1,
                    new AppointmentCollection([$appointment]),
                    null,
                    new Criteria(),
                    $this->context
                )
            );

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::once())->method('build');

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $attendeeCartService->expects(static::once())
            ->method('getCartData')
            ->willThrowException(new \Exception('test-exception'));

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method('create');

        $cartCalculator = $this->createMock(CartCalculator::class);
        $cartCalculator->expects(static::never())->method('calculate');

        $cartStatisticsService = $this->getCartStatisticsService(
            $appointmentRepository,
            $stateServiceFactory,
            $attendeeCartService,
            $channelContextFactory,
            $cartCalculator
        );

        static::expectException(\Exception::class);
        static::expectExceptionMessage('test-exception');
        $cartStatisticsService->create('test-appointment-id', new Request(), $this->context);
    }

    public function testCreateWithMetaDataSuccessfully(): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    AppointmentDefinition::ENTITY_NAME,
                    1,
                    new AppointmentCollection([$appointment]),
                    null,
                    new Criteria(),
                    $this->context
                )
            );

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $attendee = new AttendeeEntity();
        $attendee->assign(['id' => 'attendee_1', 'attendeeName' => 'test-attendee-name', 'type' => 'CLIENT']);
        $stateForGuides->addClient($attendee);
        $attendee2 = clone $attendee;
        $attendee2->assign(['id' => 'attendee_2', 'attendeeName' => 'test-attendee-name-2']);
        $stateForGuides->addClient($attendee2);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())->method('getStateForGuides')->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())->method('build')->willReturn($stateService);

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $cartDataMockHelper = new CartDataMockBuilder();
        $cart = $cartDataMockHelper->createDemoCartData('gross', 'test-token', true);
        $cart2 = $cartDataMockHelper->createDemoCartData('gross', 'test-token-2');
        $cart2[0]['attendee_id'] = 'attendee_2';
        $cart[] = $cart2[0];

        $attendeeCartService->expects(static::once())
            ->method('getCartData')
            ->willReturn($cart);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::once())
            ->method('create')
            ->willReturn((new SalesChannelContextHelper())->createSalesChannelContext());

        $cartCalculator = $this->createMock(CartCalculator::class);
        $expectCart = new Cart('test-new-token');
        $expectLineItem4 = new LineItem(
            'gs-cart-statistics-item-4',
            'product',
            'item-4',
            24
        );
        $expectCart->add($expectLineItem4);
        $expectLineItem5 = new LineItem(
            'gs-cart-statistics-item-5',
            'product',
            'item-5',
            54
        );
        $expectCart->add($expectLineItem5);
        $expectLineItem6 = new LineItem(
            'gs-cart-statistics-item-6',
            'product',
            'item-6',
            32
        );
        $expectCart->add($expectLineItem6);
        $expectCart->setPrice(
            new CartPrice(
                123,
                456,
                789,
                new CalculatedTaxCollection(),
                new TaxRuleCollection(),
                'tax-status'
            )
        );
        $cartCalculator->expects(static::once())
            ->method('calculate')
            ->with(static::callback(function (Cart $callbackCart) {
                    /** @var LineItem $callbackLineItem4 */
                    $callbackLineItem4 = $callbackCart->get('gs-cart-statistics-item-4');
                    /** @var ArrayStruct<string, array<string>>|null $metaItem4 */
                    $metaItem4 =  $callbackLineItem4->getExtension('meta');
                    if (!$metaItem4) {
                        return false;
                    }

                    $checkItem4 = $metaItem4->get('attendees') === [
                        [
                            'id' => 'attendee_1',
                            'name' => 'test-attendee-name'
                        ],
                        [
                            'id' => 'attendee_2',
                            'name' => 'test-attendee-name-2'
                        ]
                    ];

                    /** @var LineItem $callbackLineItem5 */
                    $callbackLineItem5 = $callbackCart->get('gs-cart-statistics-item-5');
                    /** @var ArrayStruct<string, array<string>>|null $metaItem5 */
                    $metaItem5 =  $callbackLineItem5->getExtension('meta');
                    if (!$metaItem5) {
                        return false;
                    }
                    $checkItem5 = $metaItem5->get('attendees') === [['id' => 'attendee_1', 'name' => 'test-attendee-name']];

                    /** @var LineItem $callbackLineItem6 */
                    $callbackLineItem6 = $callbackCart->get('gs-cart-statistics-item-6');
                    /** @var ArrayStruct<string, array<string>>|null $metaItem6 */
                    $metaItem6 =  $callbackLineItem6->getExtension('meta');
                    if (!$metaItem6) {
                        return false;
                    }
                    $checkItem6 = $metaItem6->get('attendees') === [['id' => 'attendee_1', 'name' => 'test-attendee-name']];

                    return $checkItem4 && $checkItem5 && $checkItem6;
                }),
            )
            ->willReturn($expectCart);

        $cartStatisticsService = $this->getCartStatisticsService(
            $appointmentRepository,
            $stateServiceFactory,
            $attendeeCartService,
            $channelContextFactory,
            $cartCalculator
        );

        $cartStatistics = $cartStatisticsService->create($appointment->getId(), new Request(), $this->context);
        static::assertSame($cartStatistics, $expectCart);
    }

    public function testCreateSuccessfully(): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    AppointmentDefinition::ENTITY_NAME,
                    1,
                    new AppointmentCollection([$appointment]),
                    null,
                    new Criteria(),
                    $this->context
                )
            );

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::once())->method('build');

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $cartDataMockHelper = new CartDataMockBuilder();
        $cart = $cartDataMockHelper->createDemoCartData('gross', 'test-token', true);
        $cart2 = $cartDataMockHelper->createDemoCartData('gross', 'test-token-2');
        $cart2[0]['attendee_id'] = 'attendee_2';
        $cart[] = $cart2[0];

        $attendeeCartService->expects(static::once())
            ->method('getCartData')
            ->willReturn($cart);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::once())
            ->method('create')
            ->willReturn((new SalesChannelContextHelper())->createSalesChannelContext());

        $cartCalculator = $this->createMock(CartCalculator::class);
        $expectCart = new Cart('test-new-token');
        $expectLineItem4 =  new LineItem(
            'gs-cart-statistics-item-4',
            'product',
            'item-4',
            24
        );
        $expectCart->add($expectLineItem4);
        $expectLineItem5 =  new LineItem(
            'gs-cart-statistics-item-5',
            'product',
            'item-5',
            54
        );
        $expectCart->add($expectLineItem5);
        $expectLineItem6 =  new LineItem(
            'gs-cart-statistics-item-6',
            'product',
            'item-6',
            32
        );
        $expectCart->add($expectLineItem6);
        $expectCart->setPrice(
            new CartPrice(
                123,
                456,
                789,
                new CalculatedTaxCollection(),
                new TaxRuleCollection(),
                'tax-status'
            )
        );
        $cartCalculator->expects(static::once())
            ->method('calculate')
            ->with(
                static::callback(function (Cart $callbackCart) use ($expectLineItem4, $expectLineItem5, $expectLineItem6) {
                    /** @var LineItem $callbackLineItem4 */
                    $callbackLineItem4 = $callbackCart->get('gs-cart-statistics-item-4');
                    $checkItem4 = $callbackLineItem4->getReferencedId() === $expectLineItem4->getReferencedId()
                        && $callbackLineItem4->getQuantity() === $expectLineItem4->getQuantity()
                        && $callbackLineItem4->getType() === 'product';

                    /** @var LineItem $callbackLineItem5 */
                    $callbackLineItem5 = $callbackCart->get('gs-cart-statistics-item-5');
                    $checkItem5 = $callbackLineItem5->getReferencedId() === $expectLineItem5->getReferencedId()
                        && $callbackLineItem5->getQuantity() === $expectLineItem5->getQuantity()
                        && $callbackLineItem5->getType() === 'product';

                    /** @var LineItem $callbackLineItem6 */
                    $callbackLineItem6 = $callbackCart->get('gs-cart-statistics-item-6');
                    $checkItem6 = $callbackLineItem6->getReferencedId() === $expectLineItem6->getReferencedId()
                        && $callbackLineItem6->getQuantity() === $expectLineItem6->getQuantity()
                        && $callbackLineItem6->getType() === 'product';

                    return $checkItem4 && $checkItem5 && $checkItem6;
                }),
            )
            ->willReturn($expectCart);

        $cartStatisticsService = $this->getCartStatisticsService(
            $appointmentRepository,
            $stateServiceFactory,
            $attendeeCartService,
            $channelContextFactory,
            $cartCalculator
        );

        $cartStatistics = $cartStatisticsService->create($appointment->getId(), new Request(), $this->context);
        static::assertSame($cartStatistics, $expectCart);
    }

    /**
     * @dataProvider getTestCreateAndSortTheCartLineItemsProviderData
     * @param array<string> $requestQuery
     * @param array<string> $expectedSortedLineItemKeys
     */
    public function testCreateAndSortTheCartLineItems(
        array $requestQuery,
        array $expectedSortedLineItemKeys
    ): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    AppointmentDefinition::ENTITY_NAME,
                    1,
                    new AppointmentCollection([$appointment]),
                    null,
                    new Criteria(),
                    $this->context
                )
            );

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::once())->method('build');

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $cartDataMockHelper = new CartDataMockBuilder();
        $cart = $cartDataMockHelper->createDemoCartData('gross', 'test-token', true);
        $cart2 = $cartDataMockHelper->createDemoCartData('gross', 'test-token-2');
        $cart2[0]['attendee_id'] = 'attendee_2';
        $cart[] = $cart2[0];

        $attendeeCartService->expects(static::once())
            ->method('getCartData')
            ->willReturn($cart);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::once())
            ->method('create')
            ->willReturn((new SalesChannelContextHelper())->createSalesChannelContext());

        $cartCalculator = $this->createMock(CartCalculator::class);
        $expectCart = new Cart('test-new-token');
        // line item 4
        $expectLineItem4 =  new LineItem(
            'gs-cart-statistics-item-4',
            'product',
            'item-4',
            24
        );
        $expectLineItem4->setLabel('test-item-label-4');
        $expectLineItem4->setPrice(
            new CalculatedPrice(
                123,
                456,
                new CalculatedTaxCollection(),
                new TaxRuleCollection(),
                24
            )
        );
        // line item 5
        $expectCart->add($expectLineItem4);
        $expectLineItem5 =  new LineItem(
            'gs-cart-statistics-item-5',
            'product',
            'item-5',
            54
        );
        $expectLineItem5->setLabel('test-item-label-5');
        $expectLineItem5->setPrice(
            new CalculatedPrice(
                123,
                314,
                new CalculatedTaxCollection(),
                new TaxRuleCollection(),
                54
            )
        );
        $expectCart->add($expectLineItem5);
        // line item 6
        $expectLineItem6 =  new LineItem(
            'gs-cart-statistics-item-6',
            'product',
            'item-6',
            32
        );
        $expectLineItem6->setLabel('test-item-label-6');
        $expectLineItem6->setPrice(
            new CalculatedPrice(
                123,
                676,
                new CalculatedTaxCollection(),
                new TaxRuleCollection(),
                32
            )
        );
        $expectCart->add($expectLineItem6);
        // cart set price
        $expectCart->setPrice(
            new CartPrice(
                123,
                456,
                789,
                new CalculatedTaxCollection(),
                new TaxRuleCollection(),
                'tax-status'
            )
        );
        $cartCalculator->expects(static::once())
            ->method('calculate')
            ->willReturn($expectCart);

        $cartStatisticsService = $this->getCartStatisticsService(
            $appointmentRepository,
            $stateServiceFactory,
            $attendeeCartService,
            $channelContextFactory,
            $cartCalculator
        );

        $request = new Request($requestQuery);
        $cartStatistics = $cartStatisticsService->create($appointment->getId(), $request, $this->context);
        $lineItems = $cartStatistics->getLineItems()->getElements();
        static::assertSame(\array_keys($lineItems), $expectedSortedLineItemKeys);
    }

    public static function getTestCreateAndSortTheCartLineItemsProviderData(): \Generator
    {
        yield 'empty' => [
            [],
            ['gs-cart-statistics-item-5', 'gs-cart-statistics-item-6', 'gs-cart-statistics-item-4']
        ];
        yield 'name-desc' => [
            ['order' => 'name', 'sort' => 'desc'],
            ['gs-cart-statistics-item-6', 'gs-cart-statistics-item-5', 'gs-cart-statistics-item-4']
        ];
        yield 'name-asc' => [
            ['order' => 'name', 'sort' => 'asc'],
            ['gs-cart-statistics-item-4', 'gs-cart-statistics-item-5', 'gs-cart-statistics-item-6']
        ];
        yield 'quantity-desc' => [
            ['order' => 'quantity', 'sort' => 'desc'],
            ['gs-cart-statistics-item-5', 'gs-cart-statistics-item-6', 'gs-cart-statistics-item-4']
        ];
        yield 'quantity-asc' => [
            ['order' => 'quantity', 'sort' => 'asc'],
            ['gs-cart-statistics-item-4', 'gs-cart-statistics-item-6', 'gs-cart-statistics-item-5']
        ];
        yield 'revenue-desc' => [
            ['order' => 'revenue', 'sort' => 'desc'],
            ['gs-cart-statistics-item-6', 'gs-cart-statistics-item-4', 'gs-cart-statistics-item-5']
        ];
        yield 'yield-asc' => [
            ['order' => 'revenue', 'sort' => 'asc'],
            ['gs-cart-statistics-item-5', 'gs-cart-statistics-item-4', 'gs-cart-statistics-item-6']
        ];
        yield 'default' => [
            ['order' => 'default'],
            ['gs-cart-statistics-item-4', 'gs-cart-statistics-item-5', 'gs-cart-statistics-item-6']
        ];
    }

    private function getCartStatisticsService(
        ?MockObject $appointmentRepositoryMock = null,
        ?MockObject $stateServiceFactoryMock = null,
        ?MockObject $attendeeCartServiceMock = null,
        ?MockObject $channelContextFactoryMock = null,
        ?MockObject $cartCalculatorMock = null
    ): CartStatisticsService
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&PresentationStateServiceFactory $stateServiceFactory */
        $stateServiceFactory = $stateServiceFactoryMock ?: $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&AttendeeCartService $attendeeCartService */
        $attendeeCartService = $attendeeCartServiceMock ?: $this->createMock(AttendeeCartService::class);
        /** @var MockObject&AbstractSalesChannelContextFactory $channelContextFactory */
        $channelContextFactory = $channelContextFactoryMock ?: $this->createMock(AbstractSalesChannelContextFactory::class);
        /** @var MockObject&CartCalculator $cartCalculator */
        $cartCalculator = $cartCalculatorMock ?: $this->createMock(CartCalculator::class);

        return new CartStatisticsService(
            $appointmentRepository,
            $stateServiceFactory,
            $attendeeCartService,
            $channelContextFactory,
            $cartCalculator
        );
    }
}
