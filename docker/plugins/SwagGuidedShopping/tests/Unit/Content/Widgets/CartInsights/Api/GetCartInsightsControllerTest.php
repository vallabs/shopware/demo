<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\CartInsights\Api;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\Currency\CurrencyCollection;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\CartInsights\Api\GetCartInsightsController;
use SwagGuidedShopping\Content\Widgets\CartInsights\Service\CartInsightsService;
use SwagGuidedShopping\Content\Widgets\CartInsights\Struct\CartInsightsStruct;
use SwagGuidedShopping\Core\Checkout\Cart\Service\AttendeeCartService;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class GetCartInsightsControllerTest extends TestCase
{
    public function testGetInsightsButGetExceptionWhenGettingCartDataFromAppointment(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource(), [], 'test-currency-id');
        $criteria = new Criteria();

        $currencyRepository = $this->createMock(EntityRepository::class);
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('$');
        $currencyRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    CurrencyEntity::class,
                    1,
                    new CurrencyCollection([$currency]),
                    null,
                    $criteria,
                    $context
                )
            );

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $attendeeCartService->expects(static::once())
            ->method('createAttendeeCarts')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo(['test-currency-id' => $currency]),
                static::equalTo($context)
            )
            ->willThrowException(new \Exception('Test throw exception.'));

        $cartInsightsService = $this->createMock(CartInsightsService::class);
        $cartInsightsService->expects(static::never())->method('getCartInsights');

        $controller = new GetCartInsightsController($cartInsightsService, $currencyRepository, $attendeeCartService);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('Test throw exception.');
        $controller->getInsights($appointmentId, $context);
    }

    public function testGetInsights(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource(), [], 'test-currency-id');
        $criteria = new Criteria();

        $currencyRepository = $this->createMock(EntityRepository::class);
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('$');
        $currencyRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    CurrencyEntity::class,
                    1,
                    new CurrencyCollection([$currency]),
                    null,
                    $criteria,
                    $context
                )
            );

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $attendeeCart = new AttendeeCart(
            new CashRounding(),
            'test-attendee-id'
        );
        $attendeeCart->addItem(new AttendeeCartLineItem('test-product-id', 7.0, 3));
        $attendeeCart->addItem(new AttendeeCartLineItem('test-product-id-2', 6.0, 7));
        $expectCartData = [$attendeeCart];
        $attendeeCartService->expects(static::once())
            ->method('createAttendeeCarts')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo(['test-currency-id' => $currency]),
                static::equalTo($context)
            )
            ->willReturn($expectCartData);

        $cartInsightsService = $this->createMock(CartInsightsService::class);
        $expectCartInsights = new CartInsightsStruct(
            13.0,
            [
                'byQuantity' => [
                    'test-product-id-2' => 7,
                    'test-product-id' => 3,
                ],
                'byRevenue' => [
                    'test-product-id' => 7.0,
                    'test-product-id-2' => 6.0
                ],
            ],
            10,
            $currency
        );

        $cartInsightsService->expects(static::once())
            ->method('getCartInsights')
            ->with(
                static::equalTo($expectCartData),
                static::equalTo($currency)
            )
            ->willReturn($expectCartInsights);

        $controller = new GetCartInsightsController($cartInsightsService, $currencyRepository, $attendeeCartService);
        $result = $controller->getInsights($appointmentId, $context);
        /** @var string $responseContent */
        $responseContent = $result->getContent();
        $responseData = \json_decode($responseContent, true);
        static::assertArrayHasKey('currencyId', $responseData);
        static::assertEquals('test-currency-id', $responseData['currencyId']);
        static::assertArrayHasKey('currencySymbol', $responseData);
        static::assertEquals('$', $responseData['currencySymbol']);
        static::assertArrayHasKey('cartSum', $responseData);
        static::assertEquals(13.0, $responseData['cartSum']);
        static::assertArrayHasKey('productCount', $responseData);
        static::assertEquals(10, $responseData['productCount']);
        static::assertArrayHasKey('topProducts', $responseData);
        $expectTopProducts = [
            'byQuantity' => [
                [
                    'productId' => 'test-product-id-2',
                    'value' => 7
                ],
                [
                    'productId' => 'test-product-id',
                    'value' => 3
                ],
            ],
            'byRevenue' => [
                [
                    'productId' => 'test-product-id',
                    'value' => 7.0
                ],
                [
                    'productId' => 'test-product-id-2',
                    'value' => 6.0
                ],
            ],
        ];
        static::assertEquals($expectTopProducts, $responseData['topProducts']);
    }
}
