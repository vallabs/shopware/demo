<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\CartInsights\Service;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\CashRoundingConfig;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\CartInsights\Service\CartInsightsService;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class CartInsightsServiceTest extends TestCase
{
    public function testGetCartInsights(): void
    {
        $cartInsightService = new CartInsightsService(new CashRounding());

        $cashRounding = new CashRounding();
        $cartInsightCollection = new AttendeeCart($cashRounding, 'test-attendee-id');
        $cartInsightCollection->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item', 10, 1));
        $cartInsightCollection->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item-2', 5, 3));

        $cartInsightCollection2 = new AttendeeCart($cashRounding, 'test-attendee-id-2');
        $cartInsightCollection2->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item-3', 1, 4));
        $cartInsightCollection2->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item-4', 2.5, 9));

        $currency = new CurrencyEntity();
        $currency->setFactor(1.0);
        $currency->setId('test-currency-id');
        $currency->setSymbol('€');
        $currency->setItemRounding(new CashRoundingConfig(2, 0.1, true));
        $currency->setTotalRounding(new CashRoundingConfig(2, 0.1, true));

        $result = $cartInsightService->getCartInsights([$cartInsightCollection, $cartInsightCollection2], $currency);

        static::assertEquals(17, $result->getProductCount());
        static::assertEquals(18.5, $result->getCartSum());
        $expectedTopProducts = [
            'byQuantity' => [
                ['productId' => 'test-attendee-cart-line-item-4', 'value' => 9],
                ['productId' => 'test-attendee-cart-line-item-3', 'value' => 4],
                ['productId' => 'test-attendee-cart-line-item-2', 'value' => 3],
                ['productId' => 'test-attendee-cart-line-item', 'value' => 1],
            ],
            'byRevenue' => [
                ['productId' => 'test-attendee-cart-line-item', 'value' => 10.0],
                ['productId' => 'test-attendee-cart-line-item-2', 'value' => 5.0],
                ['productId' => 'test-attendee-cart-line-item-4', 'value' => 2.5],
                ['productId' => 'test-attendee-cart-line-item-3', 'value' => 1.0],
            ],
        ];
        static::assertEquals($expectedTopProducts, $result->getTopProducts());
    }
}
