<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\CartInsights\Api;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Widgets\CartInsights\Api\GetCartStatisticsController;
use SwagGuidedShopping\Content\Widgets\CartInsights\Service\CartStatisticsService;
use Symfony\Component\HttpFoundation\Request;

class GetCartStatisticsControllerTest extends TestCase
{
    public function testGetCartStatisticsButGetExceptionsWhenCreateCartStatistics(): void
    {
        $cartStatisticsService = $this->createMock(CartStatisticsService::class);
        $cartStatisticsService->expects(static::once())
            ->method('create')
            ->willThrowException(new \Exception('test'));

        $controller = new GetCartStatisticsController($cartStatisticsService);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('test');
        $controller->getCartStatistics('test', $this->createMock(Request::class), $this->createMock(Context::class));
    }

    public function testGetStatisticsSuccessfully(): void
    {
        $cartStatisticsService = $this->createMock(CartStatisticsService::class);
        $cart = new Cart('test-token');
        $lineItem = new LineItem(
            'test-line-item-id',
            'test-type',
            'test-product-id',
            5
        );
        $cart->addLineItems(new LineItemCollection([$lineItem]));
        $cartStatisticsService->expects(static::once())
            ->method('create')
            ->willReturn($cart);

        $controller = new GetCartStatisticsController($cartStatisticsService);
        $response = $controller->getCartStatistics('test', $this->createMock(Request::class), $this->createMock(Context::class));
        /** @var string $content */
        $content = $response->getContent();
        $cart = \json_decode($content, true, 512, \JSON_THROW_ON_ERROR);
        static::assertSame('test-token', $cart['token']);
        static::assertCount(1, $cart['lineItems']);
        static::assertSame('test-line-item-id', $cart['lineItems'][0]['id']);
        static::assertSame('test-type', $cart['lineItems'][0]['type']);
        static::assertSame('test-product-id', $cart['lineItems'][0]['referencedId']);
        static::assertEquals(5, $cart['lineItems'][0]['quantity']);
    }
}
