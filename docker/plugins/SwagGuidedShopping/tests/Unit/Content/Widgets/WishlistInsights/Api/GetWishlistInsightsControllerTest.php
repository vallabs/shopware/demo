<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\WishlistInsights\Api;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Widgets\WishlistInsights\Api\GetWishlistInsightsController;
use SwagGuidedShopping\Content\Widgets\WishlistInsights\Service\WishlistService;
use SwagGuidedShopping\Struct\WidgetProductListing;
use Symfony\Component\HttpFoundation\Request;

class GetWishlistInsightsControllerTest extends TestCase
{
    public function testGetWishlist(): void
    {
        $wishlistService = $this->createMock(WishlistService::class);
        $widgetResponse = new WidgetProductListing([], 0, 1, 10);
        $wishlistService->expects(static::once())
            ->method('loadLikedProducts')
            ->willReturn($widgetResponse);

        $controller = new GetWishlistInsightsController($wishlistService);

        $response = $controller->getWishlist('test-appointment-id', new Request(), Context::createDefaultContext());
        static::assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        static::assertIsString($content);

        $data = \json_decode($content, true, 512, \JSON_THROW_ON_ERROR);
        static::assertArrayHasKey('products', $data);
        static::assertSame([], $data['products']);
        static::assertArrayHasKey('total', $data);
        static::assertSame(0, $data['total']);
        static::assertArrayHasKey('page', $data);
        static::assertSame(1, $data['page']);
        static::assertArrayHasKey('limit', $data);
        static::assertSame(10, $data['limit']);
    }
}
