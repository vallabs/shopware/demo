<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\WishlistInsights\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\Widgets\WishlistInsights\Service\WishlistService;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use Symfony\Component\HttpFoundation\Request;

class WishlistServiceTest extends TestCase
{
    public function testLoadLikedProductsWithoutSalesChannelIdInState(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method(static::anything());

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );

        static::expectException(NoSalesChannelDomainMappedException::class);
        $service->loadLikedProducts('test-appointment-id', new Request(), new Context(new SystemSource()));
    }

    public function testLoadLikesProductsWithoutAttendeeIdsInState(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method(static::anything());

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );

        $listing = $service->loadLikedProducts('test-appointment-id', new Request(), new Context(new SystemSource()));
        static::assertEmpty($listing->getProducts());
        static::assertEquals(0, $listing->getTotal());
        static::assertEquals(1, $listing->getPage());
        static::assertEquals(10, $listing->getLimit());
    }


    public function testLoadLikesProductsButFoundNoLikedProducts(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $attendee = new AttendeeEntity();
        $attendee->assign([
            'id' => \md5('test-attendee-id'),
            'attendeeName' => 'test-attendee-name',
            'type' => 'CLIENT'
        ]);
        $stateForGuides->addClient($attendee);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );

        $listing = $service->loadLikedProducts('test-appointment-id', new Request(), new Context(new SystemSource()));
        static::assertEmpty($listing->getProducts());
        static::assertEquals(0, $listing->getTotal());
        static::assertEquals(1, $listing->getPage());
        static::assertEquals(10, $listing->getLimit());
    }

    /**
     * @dataProvider getTestLoadLikesProductsButDoNotOrderByRevenue
     */
    public function testLoadLikesProductsButDoNotOrderByRevenue(string $orderBy): void
    {
        $request = new Request(['order' => $orderBy]);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $attendee = new AttendeeEntity();
        $attendee->assign([
            'id' => \md5('test-attendee-id'),
            'attendeeName' => 'test-attendee-name',
            'type' => 'CLIENT'
        ]);
        $stateForGuides->addClient($attendee);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([
                \md5('test-product-id') => [
                    'productId' => \md5('test-product-id'),
                    'count' => 1,
                    'attendeeIds' => \md5('test-attendee-id')
                ]
            ]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::once())->method('create');

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $product = new ProductEntity();
        $product->setId(\md5('test-product-id'));
        $productRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                ProductDefinition::ENTITY_NAME,
                1,
                new ProductCollection([$product]),
                null,
                new Criteria(),
                new Context(new SystemSource())
            ));

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );

        $listing = $service->loadLikedProducts('test-appointment-id', $request, new Context(new SystemSource()));
        static::assertSame([$product], $listing->getProducts());
        static::assertEquals(1, $listing->getTotal());
        static::assertEquals(1, $listing->getPage());
        static::assertEquals(10, $listing->getLimit());
    }

    public static function getTestLoadLikesProductsButDoNotOrderByRevenue(): \Generator
    {
        yield ['name'];
        yield ['quantity'];
    }

    public function testLoadLikesProductsAndOrderByRevenue(): void
    {
        $request = new Request(['order' => 'revenue']);
        $productId = '9aa336557edcde476586ec9be56c22f4';
        $productId2 = '42a593b3c0b855efc03e9bbb628ab8b5';
        $productId3 = '13a593b3c0b855efc03e9bbb628ab8b5';

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $attendee = new AttendeeEntity();
        $attendee->assign([
            'id' => \md5('test-attendee-id'),
            'attendeeName' => 'test-attendee-name',
            'type' => 'CLIENT'
        ]);
        $stateForGuides->addClient($attendee);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $likedProducts = [
            $productId => [
                'productId' => $productId,
                'count' => 1,
                'attendeeIds' => \md5('test-attendee-id')
            ],
            $productId2 => [
                'productId' => $productId2,
                'count' => 1,
                'attendeeIds' => \md5('test-attendee-id-2'),
            ],
            $productId3 => [
                'productId' => $productId3,
                'count' => 1
            ],
        ];
        $mappingParents = [
            $productId => [
                'id' => $productId,
                'parentId' => $productId
            ],
            $productId2 => [
                'id' => $productId2,
                'parentId' => \md5('test-parent-of-product-id-2')
            ],
            $productId3 => [
                'id' => $productId3,
                'parentId' => $productId3
            ],
        ];
        $cheapestPrices = [
            $productId => [
                'id' => $productId,
                'parentId' => $productId,
                'grossValues' => '10.0'
            ],
            $productId2 => [
                'id' => $productId2,
                'parentId' => \md5('test-parent-of-product-id-2'),
                'grossValues' => '13.05, 150.4, 50.94'
            ],
            $productId3 => [
                'id' => $productId3,
                'parentId' => $productId3,
                'grossValues' => '1.3'
            ]
        ];
        $result->expects(static::exactly(3))
            ->method('fetchAllAssociativeIndexed')
            ->willReturnOnConsecutiveCalls(
                $likedProducts,
                $mappingParents,
                $cheapestPrices
            );
        $connection->expects(static::exactly(3))
            ->method('executeQuery')
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::once())->method('create');

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $product = new ProductEntity();
        $product->setId($productId);
        $product2 = clone $product;
        $product2->setId($productId2);
        $product3 = clone $product;
        $product3->setId($productId3);
        $productRepository->expects(static::once())
            ->method('search')
            ->with(
                static::callback(function (Criteria $criteria) use ($productId, $productId2, $productId3) {
                    return $criteria->useIdSorting() === true &&
                    $criteria->getIds() === [$productId2, $productId, $productId3];
                })
            )
            ->willReturn(new EntitySearchResult(
                ProductDefinition::ENTITY_NAME,
                1,
                new ProductCollection([$product, $product2, $product3]),
                null,
                new Criteria(),
                new Context(new SystemSource())
            ));

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );

        $listing = $service->loadLikedProducts('test-appointment-id', $request, new Context(new SystemSource()));
        static::assertSame([$product, $product2, $product3], $listing->getProducts());
        static::assertEquals(3, $listing->getTotal());
        static::assertEquals(1, $listing->getPage());
        static::assertEquals(10, $listing->getLimit());
    }

    public function testLoadLikedProductsButNoProductIdsWithSecondPage(): void
    {
        $request = new Request(['limit' => 1, 'page' => 2]);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $attendee = new AttendeeEntity();
        $attendee->assign([
            'id' => \md5('test-attendee-id'),
            'attendeeName' => 'test-attendee-name',
            'type' => 'CLIENT'
        ]);
        $stateForGuides->addClient($attendee);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([
                \md5('test-product-id') => [
                    'productId' => \md5('test-product-id'),
                    'count' => 1,
                    'attendeeIds' => \md5('test-attendee-id')
                ]
            ]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method('create');

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );

        $listing = $service->loadLikedProducts('test-appointment-id', $request, new Context(new SystemSource()));
        static::assertEmpty($listing->getProducts());
        static::assertEquals(1, $listing->getTotal());
        static::assertEquals(2, $listing->getPage());
        static::assertEquals(1, $listing->getLimit());
    }

    private function getService(
        MockObject $stateServiceFactoryMock = null,
        MockObject $connectionMock = null,
        MockObject $channelContextFactoryMock = null,
        MockObject $productRepositoryMock = null
    ): WishlistService
    {
        /** @var MockObject&PresentationStateServiceFactory $stateServiceFactory */
        $stateServiceFactory = $stateServiceFactoryMock ?? $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ?? $this->createMock(Connection::class);
        /** @var MockObject&AbstractSalesChannelContextFactory $channelContextFactory */
        $channelContextFactory = $channelContextFactoryMock ?? $this->createMock(AbstractSalesChannelContextFactory::class);
        /** @var MockObject&SalesChannelRepository<ProductCollection> $productRepository */
        $productRepository = $productRepositoryMock ?? $this->createMock(SalesChannelRepository::class);

        return new WishlistService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productRepository
        );
    }
}
