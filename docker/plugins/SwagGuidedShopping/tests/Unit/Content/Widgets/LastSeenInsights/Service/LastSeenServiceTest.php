<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\LastSeenInsights\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingLoader;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\Widgets\LastSeenInsights\Service\LastSeenService;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use Symfony\Component\HttpFoundation\Request;

class LastSeenServiceTest extends TestCase
{
    public function testGetLastSeenProductsButNoSalesChannelIdInState(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productListingLoader = $this->createMock(ProductListingLoader::class);
        $productListingLoader->expects(static::never())->method(static::anything());

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productListingLoader
        );

        static::expectException(NoSalesChannelDomainMappedException::class);
        $service->getLastSeenProducts('test-appointment-id', new Request(), new Context(new SystemSource()));
    }

    public function testGetLastSeenProductsButNoAttendeeIdsInState(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productListingLoader = $this->createMock(ProductListingLoader::class);
        $productListingLoader->expects(static::never())->method(static::anything());

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productListingLoader
        );

        $listing = $service->getLastSeenProducts('test-appointment-id', new Request(), new Context(new SystemSource()));
        static::assertSame([], $listing->getProducts());
        static::assertSame(0, $listing->getTotal());
        static::assertSame(1, $listing->getPage());
        static::assertSame(10, $listing->getLimit());
    }

    public function testGetLastSeenProductsButFoundNoProductIds(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $stateForGuides->addAttendee($this->createAttendee());
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productListingLoader = $this->createMock(ProductListingLoader::class);
        $productListingLoader->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productListingLoader
        );

        $listing = $service->getLastSeenProducts('test-appointment-id', new Request(), new Context(new SystemSource()));
        static::assertSame([], $listing->getProducts());
        static::assertSame(0, $listing->getTotal());
        static::assertSame(1, $listing->getPage());
        static::assertSame(10, $listing->getLimit());
    }

    public function testGetLastSeenProductsButNoProductIdsWithSecondPage(): void
    {
        $request = new Request(['limit' => 1, 'page' => 2]);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $stateForGuides->addAttendee($this->createAttendee());
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([
                \md5('test-product-id') => ['productId' => 'test-product-id'],
            ]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::never())->method(static::anything());

        $productListingLoader = $this->createMock(ProductListingLoader::class);
        $productListingLoader->expects(static::never())->method(static::anything());

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productListingLoader
        );

        $listing = $service->getLastSeenProducts('test-appointment-id', $request, new Context(new SystemSource()));
        static::assertSame([], $listing->getProducts());
        static::assertSame(1, $listing->getTotal());
        static::assertSame(2, $listing->getPage());
        static::assertSame(1, $listing->getLimit());
    }

    /**
     * @dataProvider getTestGetLastSeenProductsProviderData
     */
    public function testGetLastSeenProducts(
        Request $request,
        string $expectAttendeeId
    ): void
    {
        $productId = '9aa336557edcde476586ec9be56c22f4';

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForGuides->setSalesChannelId('test-sales-channel-id');
        $stateForGuides->addAttendee($this->createAttendee());
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([
                $productId => ['productId' => $productId]
            ]);
        $sql = "
            SELECT 
                DISTINCT(JSON_UNQUOTE(JSON_EXTRACT(`payload`, '$.productId'))) AS `productId`
            FROM `guided_shopping_interaction`
            WHERE `name` IN (:interactions)
                AND `attendee_id` IN (:attendeeIds)
            ORDER BY `triggered_at` DESC
        ";
        $connection->expects(static::once())
            ->method('executeQuery')
            ->with(
                static::equalTo($sql),
                static::equalTo([
                    'interactions' => ['product.viewed', 'quickview.opened'],
                    'attendeeIds' => Uuid::fromHexToBytesList([$expectAttendeeId])
                ])
            )
            ->willReturn($result);

        $channelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $channelContextFactory->expects(static::once())->method('create');

        $productListingLoader = $this->createMock(ProductListingLoader::class);
        $product = new ProductEntity();
        $product->setId($productId);
        $productListingLoader->expects(static::once())
            ->method('load')
            ->with(
                static::callback(function (Criteria $criteria) use ($productId) {
                    return $criteria->getIds() === [$productId]
                        && $criteria->useIdSorting() === true;
                })
            )
            ->willReturn(new EntitySearchResult(
                ProductDefinition::ENTITY_NAME,
                1,
                new ProductCollection([$product]),
                null,
                new Criteria(),
                new Context(new SystemSource())
            ));

        $service = $this->getService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productListingLoader
        );

        $listing = $service->getLastSeenProducts('test-appointment-id', $request, new Context(new SystemSource()));
        static::assertSame([$product], $listing->getProducts());
        static::assertSame(1, $listing->getTotal());
        static::assertSame(1, $listing->getPage());
        static::assertSame(10, $listing->getLimit());
    }

    public static function getTestGetLastSeenProductsProviderData(): \Generator
    {
        yield 'load data of all attendees' => [new Request(), '9bb436557edcde473126ec9be11c22f4'];
        yield 'load data of a particular attendee' => [new Request(['attendeeId' => '13a593b3c0b855efc03e9bbb628ab8b5']), '13a593b3c0b855efc03e9bbb628ab8b5'];
    }

    private function getService(
        MockObject $stateServiceFactoryMock = null,
        MockObject $connectionMock = null,
        MockObject $channelContextFactoryMock = null,
        MockObject $productListingLoaderMock = null
    ): LastSeenService
    {
        /** @var MockObject&PresentationStateServiceFactory $stateServiceFactory */
        $stateServiceFactory = $stateServiceFactoryMock ?? $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ?? $this->createMock(Connection::class);
        /** @var MockObject&AbstractSalesChannelContextFactory $channelContextFactory */
        $channelContextFactory = $channelContextFactoryMock ?? $this->createMock(AbstractSalesChannelContextFactory::class);
        /** @var MockObject&ProductListingLoader $productListingLoader */
        $productListingLoader = $productListingLoaderMock ?? $this->createMock(ProductListingLoader::class);

        return new LastSeenService(
            $stateServiceFactory,
            $connection,
            $channelContextFactory,
            $productListingLoader
        );
    }

    private function createAttendee(): AttendeeEntity
    {
        $attendee = new AttendeeEntity();
        $attendee->assign([
            'id' => '9bb436557edcde473126ec9be11c22f4',
            'type' => 'test-type',
        ]);

        return $attendee;
    }
}
