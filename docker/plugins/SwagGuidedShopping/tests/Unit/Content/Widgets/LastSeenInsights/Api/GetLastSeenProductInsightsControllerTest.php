<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\LastSeenInsights\Api;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Widgets\LastSeenInsights\Api\GetLastSeenProductInsightsController;
use SwagGuidedShopping\Content\Widgets\LastSeenInsights\Service\LastSeenService;
use SwagGuidedShopping\Struct\WidgetProductListing;
use Symfony\Component\HttpFoundation\Request;

class GetLastSeenProductInsightsControllerTest extends TestCase
{
    public function testGetInsights(): void
    {
        $lastSeenService = $this->createMock(LastSeenService::class);
        $lastSeenService->expects(static::once())
            ->method('getLastSeenProducts')
            ->willReturn(new WidgetProductListing([], 0, 1, 10));

        $controller = new GetLastSeenProductInsightsController($lastSeenService);

        $response = $controller->getInsights('test-appointment-id', new Request(), new Context(new SystemSource()));
        static::assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        static::assertIsString($content);

        $data = \json_decode($content, true, 512, \JSON_THROW_ON_ERROR);
        static::assertArrayHasKey('products', $data);
        static::assertSame([], $data['products']);
        static::assertArrayHasKey('total', $data);
        static::assertSame(0, $data['total']);
        static::assertArrayHasKey('page', $data);
        static::assertSame(1, $data['page']);
        static::assertArrayHasKey('limit', $data);
    }
}
