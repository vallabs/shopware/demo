<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\AttendeeInsights\Struct;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsCollection;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsStruct;

class AttendeeInsightsCollectionTest extends TestCase
{
    public function testConstructionAndAddAttendee(): void
    {
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('test-symbol');
        $collection = new AttendeeInsightsCollection($currency);
        static::assertSame('test-currency-id', $collection->getCurrencyId());
        static::assertSame('test-symbol', $collection->getCurrencySymbol());
        static::assertSame([], $collection->getAttendees());
        $attendee = new AttendeeInsightsStruct(
            'test-attendee-id',
            1.0,
            1,
            1
        );
        $collection->addAttendee($attendee);
        static::assertSame($attendee, $collection->getAttendees()['test-attendee-id']);
    }
}
