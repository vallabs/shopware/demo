<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widget\AttendeeInsights\Service;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\CashRoundingConfig;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Service\AttendeeInsightsService;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class AttendeeInsightsServiceTest extends TestCase
{
    public function testGetInsights(): void
    {
        $service = new AttendeeInsightsService();
        $cart1 = new AttendeeCart(new CashRounding(), 'test-attendee-id');
        $cart1->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item', 20.0, 3));
        $cart1->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item-2', 5, 1));

        $cart2 = new AttendeeCart(new CashRounding(), 'test-attendee-id-2');
        $cart2->addItem(new AttendeeCartLineItem('test-attendee-cart-line-item', 20.0, 2));

        $currency = new CurrencyEntity();
        $currency->setId('EURO');
        $currency->setSymbol('€');
        $currency->setItemRounding(new CashRoundingConfig(2, 0.1, true));
        $currency->setTotalRounding(new CashRoundingConfig(2, 0.1, true));

        $expected['test-attendee-id']['cartSum'] = 25.0;
        $expected['test-attendee-id']['productCount'] = 4;
        $expected['test-attendee-id']['lineItemCount'] = 2;
        $expected['test-attendee-id-2']['cartSum'] = 20.00;
        $expected['test-attendee-id-2']['productCount'] = 2;
        $expected['test-attendee-id-2']['lineItemCount'] = 1;

        $result = $service->getInsights([$cart1, $cart2], $currency);

        $attendeeInsights = $result->getAttendees();
        static::assertCount(2, $attendeeInsights);

        foreach ($attendeeInsights as $attendeeInsight) {
            static::assertEquals($expected[$attendeeInsight->getId()]['cartSum'], $attendeeInsight->getCartSum());
            static::assertEquals($expected[$attendeeInsight->getId()]['productCount'], $attendeeInsight->getProductCount());
            static::assertEquals($expected[$attendeeInsight->getId()]['lineItemCount'], $attendeeInsight->getLineItemCount());
        }
    }
}
