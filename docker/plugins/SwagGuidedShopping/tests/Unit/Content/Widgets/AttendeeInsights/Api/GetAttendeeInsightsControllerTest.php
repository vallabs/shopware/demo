<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widget\AttendeeInsights\Api;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\Currency\CurrencyCollection;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Api\GetAttendeeInsightsController;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Service\AttendeeInsightsService;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsCollection;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsStruct;
use SwagGuidedShopping\Core\Checkout\Cart\Service\AttendeeCartService;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class GetAttendeeInsightsControllerTest extends TestCase
{
    public function testGetInsightsButGetExceptionWhenGettingCartDataFromAppointment(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource(), [], 'test-currency-id');
        $criteria = new Criteria();

        $currencyRepository = $this->createMock(EntityRepository::class);
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('$');
        $currencyRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    CurrencyEntity::class,
                    1,
                    new CurrencyCollection([$currency]),
                    null,
                    $criteria,
                    $context
                )
            );

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $attendeeCartService->expects(static::once())
            ->method('createAttendeeCarts')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo(['test-currency-id' => $currency]),
                static::equalTo($context)
            )
            ->willThrowException(new \Exception('Test throw exception.'));

        $attendeeInsightsService = $this->createMock(AttendeeInsightsService::class);
        $attendeeInsightsService->expects(static::never())->method('getInsights');

        $controller = new GetAttendeeInsightsController($attendeeInsightsService, $currencyRepository, $attendeeCartService);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('Test throw exception.');
        $controller->getInsights($appointmentId, $context);
    }

    public function testGetInsightsSuccessfully(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource(), [], 'test-currency-id');
        $criteria = new Criteria();

        $currencyRepository = $this->createMock(EntityRepository::class);
        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $currency->setSymbol('$');
        $currencyRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    CurrencyEntity::class,
                    1,
                    new CurrencyCollection([$currency]),
                    null,
                    $criteria,
                    $context
                )
            );

        $attendeeCartService = $this->createMock(AttendeeCartService::class);
        $attendeeCart = new AttendeeCart(
            new CashRounding(),
            'test-attendee-id'
        );
        $attendeeCart->addItem(new AttendeeCartLineItem('test-product-id', 7.0, 3));
        $attendeeCart->addItem(new AttendeeCartLineItem('test-product-id-2', 6.0, 7));
        $expectCartData = [$attendeeCart];
        $attendeeCartService->expects(static::once())
            ->method('createAttendeeCarts')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo(['test-currency-id' => $currency]),
                static::equalTo($context)
            )
            ->willReturn($expectCartData);

        $attendeeInsightsService = $this->createMock(AttendeeInsightsService::class);
        $attendeeInsights = new AttendeeInsightsStruct($attendeeCart->getAttendeeId(), 13.0, 10, 2);
        $expectAttendeeSightsCollection = new AttendeeInsightsCollection($currency);
        $expectAttendeeSightsCollection->addAttendee($attendeeInsights);
        $attendeeInsightsService->expects(static::once())
            ->method('getInsights')
            ->with(
                static::equalTo($expectCartData),
                static::equalTo($currency)
            )
            ->willReturn($expectAttendeeSightsCollection);

        $controller = new GetAttendeeInsightsController($attendeeInsightsService, $currencyRepository, $attendeeCartService);
        $result = $controller->getInsights($appointmentId, $context);
        /** @var string $resultContent */
        $resultContent = $result->getContent();
        $responseData = \json_decode($resultContent, true);
        static::assertArrayHasKey('currencyId', $responseData);
        static::assertEquals('test-currency-id', $responseData['currencyId']);
        static::assertArrayHasKey('currencySymbol', $responseData);
        static::assertEquals('$', $responseData['currencySymbol']);
        static::assertArrayHasKey('attendees', $responseData);
        static::assertArrayHasKey('test-attendee-id', $responseData['attendees']);
        $attendeeData = $responseData['attendees']['test-attendee-id'];
        static::assertArrayHasKey('id', $attendeeData);
        static::assertEquals('test-attendee-id', $attendeeData['id']);
        static::assertArrayHasKey('cartSum', $attendeeData);
        static::assertEquals(13.0, $attendeeData['cartSum']);
        static::assertArrayHasKey('productCount', $attendeeData);
        static::assertEquals(10, $attendeeData['productCount']);
        static::assertArrayHasKey('lineItemCount', $attendeeData);
        static::assertEquals(2, $attendeeData['lineItemCount']);
    }
}
