<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Widgets\AttendeeInsights\Struct;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsStruct;

class AttendeeInsightsStructTest extends TestCase
{
    public function testInitialize(): void
    {
        $struct = new AttendeeInsightsStruct(
            'test-attendee-id',
            1.0,
            1,
            2
        );
        static::assertSame('test-attendee-id', $struct->getId());
        static::assertSame(1.0, $struct->getCartSum());
        static::assertSame(1, $struct->getProductCount());
        static::assertSame(2, $struct->getLineItemCount());
    }
}
