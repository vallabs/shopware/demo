<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;

class VideoChatEntityDoesNotExistTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new VideoChatEntityDoesNotExist('test-appointment-id');
        static::assertEquals(
            'There is no video entity found for the appointment (test-appointment-id), this has to be created before creating or modifying a room. Maybe the appointment has no video chat enabled.',
            $exception->getMessage()
        );
        static::assertEquals('GUIDED_SHOPPING__VIDEO_CHAT_ENTITY_NOT_FOUND', $exception->getErrorCode());
        static::assertEquals(404, $exception->getStatusCode());
    }
}
