<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\EventListener;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Document\DocumentDefinition;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityWriteResult;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityDeletedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\VideoChat\EventListener\VideoChatDeletedListener;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;
use SwagGuidedShopping\Content\VideoChat\VideoChatDefinition;
use SwagGuidedShopping\Tests\Unit\MockBuilder\VideoChatMockHelper;

class VideoChatDeletedListenerTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $listener = $this->createVideoChatDeletedListener();
        static::assertSame(
            [
                'guided_shopping_appointment.deleted' => ['onEntityDeleted'],
            ],
            $listener::getSubscribedEvents()
        );
    }

    /**
     * @dataProvider getTestDeletedEntityButInvalidEventProviderData
     */
    public function testDeletedEntityButInvalidEvent(EntityDeletedEvent $event): void
    {
        $videoChatRepository = $this->createMock(EntityRepository::class);
        $videoChatRepository->expects(static::never())->method('search');

        $videoRoomManager = $this->createMock(VideoRoomManager::class);
        $videoRoomManager->expects(static::never())->method('deleteRoom');

        $listener = $this->createVideoChatDeletedListener($videoRoomManager, $videoChatRepository);

        $listener->onEntityDeleted($event);
    }

    public static function getTestDeletedEntityButInvalidEventProviderData(): \Generator
    {
        $context = new Context(new SystemSource());

        $entityDeletedEvent = new EntityDeletedEvent(
            'guided_shopping_appointment',
            [],
            $context
        );
        yield 'empty-write-result' => [$entityDeletedEvent];

        $entityDeletedEvent = new EntityDeletedEvent(
            AppointmentDefinition::ENTITY_NAME,
            [new EntityWriteResult(
                ['test-primary-key' => 'test-primary-key'],
                ['key' => 'value'],
                AppointmentDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_DELETE
            )],
            $context
        );
        yield 'event-has-array-of-primary-keys' => [$entityDeletedEvent];

        $entityDeletedEvent = new EntityDeletedEvent(
            AppointmentDefinition::ENTITY_NAME,
            [new EntityWriteResult(
                'test-primary-key',
                ['key' => 'value'],
                DocumentDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_DELETE
            )],
            $context
        );
        yield 'delete-document-entity-event' => [$entityDeletedEvent];

        $entityDeletedEvent = new EntityDeletedEvent(
            AppointmentDefinition::ENTITY_NAME,
            [new EntityWriteResult(
                'test-primary-key',
                ['key' => 'value'],
                AppointmentDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_INSERT
            )],
            $context
        );
        yield 'insert-appointment-entity-event' => [$entityDeletedEvent];
    }

    public function testDeletedEntityButNoVideoChatFound(): void
    {
        $context = new Context(new SystemSource());

        $entityDeletedEvent = new EntityDeletedEvent(
            AppointmentDefinition::ENTITY_NAME,
            [new EntityWriteResult(
                'test-primary-key',
                ['key' => 'value'],
                AppointmentDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_DELETE
            )],
            $context
        );

        $videoChatRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', 'test-primary-key'));
        $videoChatRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new EntitySearchResult(
                VideoChatDefinition::ENTITY_NAME,
                0,
                new VideoChatCollection(),
                null,
                $criteria,
                $context
            ));

        $videoRoomManager = $this->createMock(VideoRoomManager::class);
        $videoRoomManager->expects(static::never())->method('deleteRoom');

        $listener = $this->createVideoChatDeletedListener($videoRoomManager, $videoChatRepository);

        $listener->onEntityDeleted($entityDeletedEvent);
    }

    public function testDeletedEntityAndVideoChatFoundButVideoChatRoomIsNotCreated(): void
    {
        $context = new Context(new SystemSource());

        $entityDeletedEvent = new EntityDeletedEvent(
            AppointmentDefinition::ENTITY_NAME,
            [new EntityWriteResult(
                'test-primary-key',
                ['key' => 'value'],
                AppointmentDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_DELETE
            )],
            $context
        );

        $videoChatRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', 'test-primary-key'));
        $videoChat = (new VideoChatMockHelper())->getVideoChatEntity();
        $videoChat->setName(null);
        $videoChatRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new EntitySearchResult(
                VideoChatDefinition::ENTITY_NAME,
                1,
                new VideoChatCollection([$videoChat]),
                null,
                $criteria,
                $context
            ));

        $videoRoomManager = $this->createMock(VideoRoomManager::class);
        $videoRoomManager->expects(static::never())->method('deleteRoom');

        $listener = $this->createVideoChatDeletedListener($videoRoomManager, $videoChatRepository);

        $listener->onEntityDeleted($entityDeletedEvent);
    }

    public function testDeletedEntitySuccessfully(): void
    {
        $context = new Context(new SystemSource());

        $event = new EntityDeletedEvent(
            AppointmentDefinition::ENTITY_NAME,
            [new EntityWriteResult(
                'test-primary-key',
                ['key' => 'value'],
                AppointmentDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_DELETE
            )],
            $context
        );

        $videoChatRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', 'test-primary-key'));
        $videoChat = (new VideoChatMockHelper())->getVideoChatEntity();
        $videoChatRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new EntitySearchResult(
                VideoChatDefinition::ENTITY_NAME,
                1,
                new VideoChatCollection([$videoChat]),
                null,
                $criteria,
                $context
            ));

        $videoRoomManager = $this->createMock(VideoRoomManager::class);
        $videoRoomManager->expects(static::once())
            ->method('deleteRoom')
            ->with(
                static::equalTo('test-video-chat-name'),
                static::equalTo($context)
            );

        $listener = $this->createVideoChatDeletedListener($videoRoomManager, $videoChatRepository);

        $listener->onEntityDeleted($event);
    }

    private function createVideoChatDeletedListener(
        ?MockObject $videoRoomManagerMock = null,
        ?MockObject $videoChatRepositoryMock = null,
    ): VideoChatDeletedListener
    {
        /** @var MockObject&VideoRoomManager $videoRoomManager */
        $videoRoomManager = $videoRoomManagerMock ?? $this->createMock(VideoRoomManager::class);
        /** @var MockObject&EntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = $videoChatRepositoryMock ?? $this->createMock(EntityRepository::class);

        return new VideoChatDeletedListener($videoRoomManager, $videoChatRepository);
    }
}
