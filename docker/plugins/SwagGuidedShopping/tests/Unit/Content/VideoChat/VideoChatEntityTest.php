<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class VideoChatEntityTest extends TestCase
{
    public function testSetAppointmentId(): void
    {
        $videoChatEntity = new VideoChatEntity();
        $videoChatEntity->setAppointmentId('test-appointment-id');
        static::assertEquals('test-appointment-id', $videoChatEntity->getAppointmentId());
    }

    public function testSetAppointment(): void
    {
        $videoChatEntity = new VideoChatEntity();
        static::assertNull($videoChatEntity->getAppointment());
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $videoChatEntity->setAppointment($appointment);
        static::assertSame($appointment, $videoChatEntity->getAppointment());
    }

    public function testSetName(): void
    {
        $videoChatEntity = new VideoChatEntity();
        $videoChatEntity->setName('test-name');
        static::assertEquals('test-name', $videoChatEntity->getName());
        $videoChatEntity->setName(null);
        static::assertNull($videoChatEntity->getName());
    }

    public function testSetOwnerToken(): void
    {
        $videoChatEntity = new VideoChatEntity();
        $videoChatEntity->setOwnerToken('test-owner-token');
        static::assertEquals('test-owner-token', $videoChatEntity->getOwnerToken());
        $videoChatEntity->setOwnerToken(null);
        static::assertNull($videoChatEntity->getOwnerToken());
    }

    public function testSetUserToken(): void
    {
        $videoChat = new VideoChatEntity();
        $videoChat->setUserToken('test-user-token');
        static::assertEquals('test-user-token', $videoChat->getUserToken());
        $videoChat->setUserToken(null);
        static::assertNull($videoChat->getUserToken());
    }

    public function testSetUrl(): void
    {
        $videoChat = new VideoChatEntity();
        $videoChat->setUrl('test-url');
        static::assertEquals('test-url', $videoChat->getUrl());
        $videoChat->setUrl(null);
        static::assertNull($videoChat->getUrl());
    }

    public function testSetStartAsBroadcast(): void
    {
        $videoChat = new VideoChatEntity();
        $videoChat->setStartAsBroadcast(true);
        static::assertTrue($videoChat->isStartAsBroadcast());
        $videoChat->setStartAsBroadcast(false);
        static::assertFalse($videoChat->isStartAsBroadcast());
    }
}
