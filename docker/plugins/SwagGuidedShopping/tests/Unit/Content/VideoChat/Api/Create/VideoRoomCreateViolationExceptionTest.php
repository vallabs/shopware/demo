<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoRoomCreateViolationException;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class VideoRoomCreateViolationExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $violationList = new ViolationList($constraintViolationList);

        $exception = new VideoRoomCreateViolationException($violationList);

        static::assertSame( "Could not create room\n" . $violationList, $exception->getMessage());
        static::assertSame(400, $exception->getStatusCode());
        static::assertSame('GUIDED_SHOPPING__ROOM_CREATE_VIOLATION', $exception->getErrorCode());
    }
}
