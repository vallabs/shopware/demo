<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Api\Delete;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\VideoChat\Api\Delete\VideoChatRoomDeleteController;
use SwagGuidedShopping\Content\VideoChat\Api\Delete\VideoChatRoomDeleteFacade;

class VideoChatRoomDeleteControllerTest extends TestCase
{
    public function testDeleteVideoChatRoom(): void
    {
        $videoChatRoomDeleteFacade = $this->createMock(VideoChatRoomDeleteFacade::class);
        $videoChatRoomDeleteFacade->expects(static::once())->method('handleRoomDeletion');

        $controller = new VideoChatRoomDeleteController($videoChatRoomDeleteFacade);

        $response = $controller->deleteVideoChatRoom('test-appointment-id', new Context(new SystemSource()));
        static::assertSame(200, $response->getStatusCode());
        /** @var string $content */
        $content = $response->getContent();
        static::assertEmpty(\json_decode($content, true, 512, \JSON_THROW_ON_ERROR));
    }
}
