<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Api\Create;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoRoomCreateException;

class VideoRoomCreateExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new VideoRoomCreateException('test-exception-message');
        static::assertSame('test-exception-message', $exception->getMessage());
        static::assertSame(400, $exception->getStatusCode());
        static::assertSame('GUIDED_SHOPPING__APPOINTMENT_COULD_NOT_CREATE_VIDEO_ROOM', $exception->getErrorCode());
    }
}
