<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Api\Delete;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Test\Stub\DataAbstractionLayer\StaticEntityRepository;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\VideoChat\Api\Delete\VideoChatRoomDeleteFacade;
use SwagGuidedShopping\Content\VideoChat\Api\Delete\VideoRoomDeleteException;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\VideoManagerMockBuilder;

class VideoChatRoomDeleteFacadeTest extends TestCase
{
    private AppointMentMockHelper $appointmentMockHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->appointmentMockHelper = new AppointMentMockHelper();
    }

    public function testHandleRoomDeletionButCanNotFindAppointment(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([])]);

        $videoRoomManager = (new VideoManagerMockBuilder())->build();

        $videoChatRoomDeleteFacade = new VideoChatRoomDeleteFacade(
            $videoRoomManager,
            $appointmentRepository
        );

        static::expectException(EntityNotFoundException::class);
        $videoChatRoomDeleteFacade->handleRoomDeletion($appointmentId, $context);
    }

    public function testHandleRoomDeletionWithAppointmentDoesNotHasVideoChat(): void
    {
        $context = new Context(new SystemSource());

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setVideoChat(null);
        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $videoRoomManager = (new VideoManagerMockBuilder())->build();

        $videoChatRoomDeleteFacade = new VideoChatRoomDeleteFacade(
            $videoRoomManager,
            $appointmentRepository
        );

        static::expectException(VideoChatEntityDoesNotExist::class);
        $videoChatRoomDeleteFacade->handleRoomDeletion($appointment->getId(), $context);
    }

    public function testHandleRoomDeletionButVideoChatRoomIsNotCreatedYet(): void
    {
        $context = new Context(new SystemSource());

        $appointment = $this->appointmentMockHelper->getAppointEntity(true);
        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $videoRoomManager = (new VideoManagerMockBuilder())->build();

        $videoChatRoomDeleteFacade = new VideoChatRoomDeleteFacade(
            $videoRoomManager,
            $appointmentRepository
        );

        $result = $videoChatRoomDeleteFacade->handleRoomDeletion($appointment->getId(), $context);
        static::assertTrue($result);
    }

    public function testHandleRoomDeletionButFailedWhenCallingExternalDeleteRoomAPI(): void
    {
        $context = new Context(new SystemSource());

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $roomResponse = '{
            "error" : "could not create room"
         }';
        $mockHandler = new MockHandler(
            [
                new Response(400, [], $roomResponse),
            ]
        );
        $videoRoomManager = (new VideoManagerMockBuilder())->build($mockHandler);

        $videoChatRoomDeleteFacade = new VideoChatRoomDeleteFacade(
            $videoRoomManager,
            $appointmentRepository
        );

        static::expectException(VideoRoomDeleteException::class);
        $videoChatRoomDeleteFacade->handleRoomDeletion($appointment->getId(), $context);
    }

    public function testHandleRoomDeletionButFailedWhenCallingExternalDeleteRoomAPIWith404(): void
    {
        $context = new Context(new SystemSource());

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                $context
            ));

        $roomResponse = '{
            "error" : "could not create room because it is already deleted"
         }';
        $mockHandler = new MockHandler(
            [
                new Response(404, [], $roomResponse),
            ]
        );

        /** @var StaticEntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = new StaticEntityRepository([
            new IdSearchResult(
                1,
                [['data' => ['test-video-chat-id'], 'primaryKey' => 'test-video-chat-id']],
                new Criteria(),
                $context
            )
        ]);

        $videoRoomManager = (new VideoManagerMockBuilder())->build($mockHandler, $videoChatRepository);

        $videoChatRoomDeleteFacade = new VideoChatRoomDeleteFacade(
            $videoRoomManager,
            $appointmentRepository
        );

        $result = $videoChatRoomDeleteFacade->handleRoomDeletion($appointment->getId(), $context);
        static::assertTrue($result);
    }

    public function testHandleRoomDeletionSuccessfully(): void
    {
        $context = new Context(new SystemSource());

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                $context
            ));

        $mockHandler = new MockHandler(
            [
                new Response(200, [], 'Video is deleted successfully'),
            ]
        );

        /** @var StaticEntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = new StaticEntityRepository([
            new IdSearchResult(
                1,
                [['data' => ['test-video-chat-id'], 'primaryKey' => 'test-video-chat-id']],
                new Criteria(),
                $context
            )
        ]);
        $videoRoomManager = (new VideoManagerMockBuilder())->build($mockHandler, $videoChatRepository);

        $videoChatRoomDeleteFacade = new VideoChatRoomDeleteFacade(
            $videoRoomManager,
            $appointmentRepository
        );

        $result = $videoChatRoomDeleteFacade->handleRoomDeletion($appointment->getId(), $context);
        static::assertTrue($result);
    }
}
