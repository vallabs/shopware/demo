<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Api\Create;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Test\Stub\DataAbstractionLayer\StaticEntityRepository;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatRoomCreateFacade;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoRoomCreateException;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\VideoManagerMockBuilder;

/**
 * @internal
 */
class VideoChatRoomCreateFacadeTest extends TestCase
{
    private AppointMentMockHelper $appointmentMockHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->appointmentMockHelper = new AppointMentMockHelper();
    }

    public function testHandleCreationRoomWithInvalidAppointment(): void
    {
        $videoManagerBuilder = new VideoManagerMockBuilder();
        $videoManager = $videoManagerBuilder->build();

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([])]);

        $facade = new VideoChatRoomCreateFacade(
            $videoManager,
            $appointmentRepository
        );

        static::expectException(EntityNotFoundException::class);
        $facade->handleRoomCreation('test-appointment-id', new Context(new SystemSource()));
    }

    public function testHandleCreationWithoutVideoChat(): void
    {
        $videoManagerBuilder = new VideoManagerMockBuilder();
        $videoManager = $videoManagerBuilder->build();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setVideoChat(null);

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $facade = new VideoChatRoomCreateFacade(
            $videoManager,
            $appointmentRepository
        );

        static::expectException(VideoChatEntityDoesNotExist::class);
        $facade->handleRoomCreation($appointment->getId(), new Context(new SystemSource()));
    }

    public function testHandleCreationWithVideoChatIsCreatedAlreadyButVideoChatIsNotCompleted(): void
    {
        $videoManagerBuilder = new VideoManagerMockBuilder();
        $videoManager = $videoManagerBuilder->build();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        /** @var VideoChatEntity $expectVideoChat */
        $expectVideoChat = $appointment->getVideoChat();
        $expectVideoChat->setUrl(null);
        $expectVideoChat->setOwnerToken(null);
        $expectVideoChat->setUserToken(null);
        $expectVideoChat->setAppointmentId($appointment->getId());
        $appointment->setVideoChat($expectVideoChat);

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $facade = new VideoChatRoomCreateFacade(
            $videoManager,
            $appointmentRepository
        );

        static::expectException(\Exception::class);
        $facade->handleRoomCreation($appointment->getId(), new Context(new SystemSource()));
    }

    public function testHandleCreationWithVideoChatIsCreated(): void
    {
        $videoManagerBuilder = new VideoManagerMockBuilder();
        $videoManager = $videoManagerBuilder->build();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        /** @var VideoChatEntity $expectVideoChat */
        $expectVideoChat = $appointment->getVideoChat();
        $expectVideoChat->setUrl('test-url');
        $expectVideoChat->setName('test-name');
        $expectVideoChat->setOwnerToken('test-owner-token');
        $expectVideoChat->setUserToken('test-user-token');
        $expectVideoChat->setStartAsBroadcast(true);
        $appointment->setVideoChat($expectVideoChat);

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $facade = new VideoChatRoomCreateFacade(
            $videoManager,
            $appointmentRepository
        );

        $roomCreateStruct = $facade->handleRoomCreation($appointment->getId(), new Context(new SystemSource()));
        static::assertSame('test-url', $roomCreateStruct->getRoomUrl());
        static::assertSame('test-name', $roomCreateStruct->getRoomName());
        static::assertSame('test-owner-token', $roomCreateStruct->getOwnerToken());
        static::assertSame('test-user-token', $roomCreateStruct->getUserToken());
        static::assertTrue($roomCreateStruct->startAsBroadcast());
    }

    public function testHandleCreationButThrowExceptionWhileCreatingRoom(): void
    {
        $roomResponse = '{
            "error" : "could not create room"
         }';

        $mockHandler = new MockHandler(
            [
                new Response(400, [], $roomResponse),
            ]
        );

        $videoManagerBuilder = new VideoManagerMockBuilder();
        $videoManager = $videoManagerBuilder->build($mockHandler);

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        /** @var VideoChatEntity $expectVideoChat */
        $expectVideoChat = $appointment->getVideoChat();
        $expectVideoChat->setName(null);
        $appointment->setVideoChat($expectVideoChat);

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);

        $facade = new VideoChatRoomCreateFacade($videoManager, $appointmentRepository);

        static::expectException(VideoRoomCreateException::class);
        $facade->handleRoomCreation($appointment->getId(), new Context(new SystemSource()));
    }

    public function testHandleCreationSuccessfully(): void
    {
        $roomResponse = '{
            "id" : "test-room-id",
            "name" : "test-room-name",
            "url": "test-url"
         }';

        $userTokenRepsonse = '{
            "token" : "test-user-token"
         }';
        $ownerTokenRepsonse = '{
            "token" : "test-owner-token"
         }';

        $mockHandler = new MockHandler(
            [
                new Response(200, [], $roomResponse),
                new Response(200, [], $userTokenRepsonse),
                new Response(200, [], $ownerTokenRepsonse),
            ]
        );

        $videoManagerBuilder = new VideoManagerMockBuilder();
        $videoManager = $videoManagerBuilder->build($mockHandler);

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        /** @var VideoChatEntity $expectVideoChat */
        $expectVideoChat = $appointment->getVideoChat();
        $expectVideoChat->setName(null);
        $appointment->setVideoChat($expectVideoChat);

        /** @var StaticEntityRepository<EntityCollection<Entity>>&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = new StaticEntityRepository([new AppointmentCollection([$appointment])]);


        $facade = new VideoChatRoomCreateFacade($videoManager, $appointmentRepository);

        $roomCreateStruct = $facade->handleRoomCreation($appointment->getId(), new Context(new SystemSource()));
        static::assertSame('test-url', $roomCreateStruct->getRoomUrl());
        static::assertSame('test-room-name', $roomCreateStruct->getRoomName());
        static::assertSame('test-owner-token', $roomCreateStruct->getOwnerToken());
        static::assertSame('test-user-token', $roomCreateStruct->getUserToken());
    }
}
