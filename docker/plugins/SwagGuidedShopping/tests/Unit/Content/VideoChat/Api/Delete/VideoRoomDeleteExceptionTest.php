<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Api\Delete;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\Api\Delete\VideoRoomDeleteException;

class VideoRoomDeleteExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new VideoRoomDeleteException('test-exception-message');
        static::assertSame('test-exception-message', $exception->getMessage());
        static::assertSame(400, $exception->getStatusCode());
        static::assertSame('GUIDED_SHOPPING__ROOM_DELETE_EXCEPTION', $exception->getErrorCode());
    }
}
