<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Api\Create;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatCreateStruct;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatRoomCreateController;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatRoomCreateFacade;

class VideoChatRoomCreateControllerTest extends TestCase
{
    public function testCreateVideoChatRoom(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $videoChatRoomCreateFacade = $this->createMock(VideoChatRoomCreateFacade::class);
        $videoChatCreateStruct = new VideoChatCreateStruct(
            'test-room-url',
            'test-room-name',
            'test-user-token',
            'test-owner-token',
            true
        );
        $videoChatRoomCreateFacade->expects(static::once())
            ->method('handleRoomCreation')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($context)
            )
            ->willReturn($videoChatCreateStruct);

        $controller = new VideoChatRoomCreateController($videoChatRoomCreateFacade);

        $response = $controller->createVideoChatRoom($appointmentId, $context);
        static::assertEquals(200, $response->getStatusCode());

        /** @var string $content */
        $content = $response->getContent();
        $data = \json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        static::assertSame($videoChatCreateStruct->getVars(), $data);
    }
}
