<?php declare(strict_types=1);

namespace Swag\GuidedShopping\Tests\Unit\Content\VideoChat\Api\Delete;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\Api\Delete\VideoRoomDeleteViolationException;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class VideoRoomDeleteViolationExceptionTestTest extends TestCase
{
    public function testInitialize(): void
    {
        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $violationList = new ViolationList($constraintViolationList);

        $exception = new VideoRoomDeleteViolationException($violationList);
        static::assertSame("Could not delete room \n" . $violationList, $exception->getMessage());
        static::assertSame(400, $exception->getStatusCode());
        static::assertSame('GUIDED_SHOPPING__ROOM_DELETE_VIOLATION', $exception->getErrorCode());
    }
}
