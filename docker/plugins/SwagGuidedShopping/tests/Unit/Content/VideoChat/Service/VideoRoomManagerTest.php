<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat\Service;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Test\Stub\DataAbstractionLayer\StaticEntityRepository;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatCreateStruct;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoRoomCreateException;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Tests\Unit\MockBuilder\VideoManagerMockBuilder;

/**
 * @internal
 */
class VideoRoomManagerTest extends TestCase
{
    private VideoManagerMockBuilder $videoRoomManagerMockBuilder;

    protected function setUp(): void
    {
        parent::setUp();

        $this->videoRoomManagerMockBuilder = new VideoManagerMockBuilder();
    }

    public function testCreateRoomButFailedWhenCallingCreateRoomAPI(): void
    {
        $roomResponse = '{
            "error" : "could not create room"
        }';

        $mockHandler = new MockHandler(
            [
                new Response(400, [], $roomResponse),
            ]
        );

        $videoManager = $this->videoRoomManagerMockBuilder->build($mockHandler);

        static::expectException(VideoRoomCreateException::class);
        static::expectExceptionMessageMatches('%"error" : "could not create room"%');
        $videoManager->createRoom(false);
    }

    public function testCreateRoomButFailedWhenCallingCreateUserToken(): void
    {
        $roomResponse = '{
            "id" : "test-room-id",
            "name" : "test-room-name",
            "url" : "test-url"
        }';

        $userTokenResponse = '{
            "invalid-request-error" => "invalid-request-error",
            "info" => "could not create user token"
        }';

        $mockHandler = new MockHandler(
            [
                new Response(200, [], $roomResponse),
                new Response(400, [], $userTokenResponse),
            ]
        );

        $videoManager = $this->videoRoomManagerMockBuilder->build($mockHandler);

        static::expectException(VideoRoomCreateException::class);
        static::expectExceptionMessageMatches('%"info" => "could not create user token"%');
        $videoManager->createRoom(false);
    }

    public function testCreateRoomButFailedWhenCallingCreateOwnerToken(): void
    {
        $roomResponse = '{
            "id" : "test-room-id",
            "name" : "test-room-name",
            "url" : "test-url"
        }';

        $userTokenResponse = '{
            "token" : "test-user-token"
        }';

        $ownerTokenResponse = '{
            "invalid-request-error" => "invalid-request-error",
            "info" => "could not create owner token"
        }';

        $mockHandler = new MockHandler(
            [
                new Response(200, [], $roomResponse),
                new Response(200, [], $userTokenResponse),
                new Response(400, [], $ownerTokenResponse),
            ]
        );

        $videoManager = $this->videoRoomManagerMockBuilder->build($mockHandler);

        static::expectException(VideoRoomCreateException::class);
        static::expectExceptionMessageMatches('%"info" => "could not create owner token"%');
        $videoManager->createRoom(false);
    }

    /**
     * @dataProvider getTestCreateRoomSuccessfullyProviderData
     */
    public function testCreateRoomSuccessfully(bool $startAsBroadcast): void
    {
        $roomResponse = '{
            "id": "test-room-id",
            "name": "test-room-name",
            "url" : "test-url"
        }';

        $userTokenResponse = '{
            "token" : "test-user-token"
        }';

        $ownerTokenResponse = '{
            "token" : "test-owner-token"
        }';

        $mockHandler = new MockHandler(
            [
                new Response(200, [], $roomResponse),
                new Response(200, [], $userTokenResponse),
                new Response(200, [], $ownerTokenResponse),
            ]
        );

        $videoManager = $this->videoRoomManagerMockBuilder->build($mockHandler);

        $result = $videoManager->createRoom($startAsBroadcast);
        static::assertSame('test-url', $result->getRoomUrl());
        static::assertSame('test-room-name', $result->getRoomName());
        static::assertSame('test-user-token', $result->getUserToken());
        static::assertSame('test-owner-token', $result->getOwnerToken());
        static::assertSame($startAsBroadcast, $result->startAsBroadcast());
    }

    public static function getTestCreateRoomSuccessfullyProviderData(): \Generator
    {
        yield 'startAsBroadcast true' => [true];
        yield 'startAsBroadcast false' => [false];
    }

    public function testSaveVideoChatData(): void
    {
        $videoChatId = 'test-video-chat-id';
        $roomData = new VideoChatCreateStruct(
            'test-url',
            'test-room-name',
            'test-user-token',
            'test-owner-token',
            true
        );

        /** @var StaticEntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = new StaticEntityRepository([]);

        $videoRoomManager = $this->videoRoomManagerMockBuilder->build(null, $videoChatRepository);

        $videoRoomManager->saveVideoChatData($videoChatId, $roomData, new Context(new SystemSource()));

        $upserts = $videoChatRepository->upserts;
        static::assertArrayHasKey(0, $upserts);
        static::assertArrayHasKey(0, $upserts[0]);
        static::assertSame([
            'id' => $videoChatId,
            'name' => $roomData->getRoomName(),
            'url' => $roomData->getRoomUrl(),
            'userToken' => $roomData->getUserToken(),
            'ownerToken' => $roomData->getOwnerToken(),
        ], $upserts[0][0]);
    }

    public function testDeleteRoomButGet400ErrorWhenCallingDeleteRoomAPI(): void
    {
        $videoChat = new VideoChatEntity();
        $videoChat->setId('test-video-chat-id');
        $videoChat->setName('test-room-name');

        $errorResponse = '{
            "error" => "unknown-error",
            "info" => "could not delete owner token"
        }';
        $mockHandler = new MockHandler(
            [
                new Response(400, [], $errorResponse),
            ]
        );

        /** @var StaticEntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = new StaticEntityRepository([
            new IdSearchResult(
                1,
                [['data' => ['test-video-chat-id'], 'primaryKey' => 'test-video-chat-id']],
                new Criteria(),
                new Context(new SystemSource())
            )
        ]);

        $videoRoomManager = $this->videoRoomManagerMockBuilder->build($mockHandler, $videoChatRepository);

        static::assertCount(0, $videoChatRepository->deletes);
        static::expectException(\Exception::class);
        static::expectExceptionCode(400);
        static::expectExceptionMessageMatches("%{$errorResponse}%");
        $videoRoomManager->deleteRoom('test-room-name', new Context(new SystemSource()));
    }

    /**
     * @dataProvider getTestDeleteRoomSuccessfullyProviderData
     */
    public function testDeleteRoomSuccessfully(): void
    {
        $videoChat = new VideoChatEntity();
        $videoChat->setId('test-video-chat-id');
        $videoChat->setName('test-room-name');

        $errorResponse = '{
            "error" => "room-not-found",
            "info" => "could not find room"
        }';
        $mockHandler = new MockHandler(
            [
                new Response(404, [], $errorResponse),
            ]
        );

        /** @var StaticEntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = new StaticEntityRepository([
            new IdSearchResult(
                1,
                [['data' => ['test-video-chat-id'], 'primaryKey' => 'test-video-chat-id']],
                new Criteria(),
                new Context(new SystemSource())
            )
        ]);

        $videoRoomManager = $this->videoRoomManagerMockBuilder->build($mockHandler, $videoChatRepository);

        $videoRoomManager->deleteRoom('test-room-name', new Context(new SystemSource()));

        $deletes = $videoChatRepository->deletes;
        static::assertEquals([[['id' => 'test-video-chat-id']]], $deletes);
    }

    public static function getTestDeleteRoomSuccessfullyProviderData(): \Generator
    {
        $errorResponse = '{
            "error" => "room-not-found",
            "info" => "could not find room"
        }';
        yield 'room not found when calling delete room API' => [
            new Response(404, [], $errorResponse)
        ];

        $successResponse = '{
            "info" => "deleted room"
        }';
        yield 'room deleted successfully' => [
            new Response(200, [], $successResponse)
        ];
    }
}
