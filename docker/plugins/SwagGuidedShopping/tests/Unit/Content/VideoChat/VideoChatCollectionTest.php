<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\VideoChat;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\VideoChat\VideoChatCollection
 *
 * @internal
 */
class VideoChatCollectionTest extends TestCase
{
    /**
     * @param array<string, mixed> $parameters
     */
    public function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testGetExpectedClass(): void
    {
        $videoChatCollection = new VideoChatCollection();
        $this->assertEquals($this->invokeMethod($videoChatCollection, 'getExpectedClass', []), 'SwagGuidedShopping\Content\VideoChat\VideoChatEntity');
    }
}
