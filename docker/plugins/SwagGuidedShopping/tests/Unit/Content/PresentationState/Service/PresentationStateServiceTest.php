<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Service;

use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\PresentationState\Event\StateInitializedEvent;
use SwagGuidedShopping\Content\PresentationState\Event\StateReInitializedEvent;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagGuidedShopping\Tests\Unit\Content\Mercure\Service\PublisherFake;
use SwagGuidedShopping\Tests\Unit\Fake\FakeRepository;
use SwagGuidedShopping\Tests\Unit\Fake\TagawareAdapterFake;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AttendeeMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CacheMockBuilder;
use SwagGuidedShopping\Tests\Unit\MockBuilder\SystemConfigMockBuilder;

/**
 * @internal
 */
class PresentationStateServiceTest extends TestCase
{
    private CacheMockBuilder $cacheMockBuilder;

    private AppointMentMockHelper $appointmentMockHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->cacheMockBuilder = new CacheMockBuilder('test');
        $this->appointmentMockHelper = new AppointMentMockHelper();
    }

    public function testLoadStatesFromCache(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();

        $this->setCacheItem($cache, [
            'currentGuideProductId' => 'test-current-guide-product-id',
            'lastActiveGuideSection' => 'test-last-active-guide-section',
            'currentPageId' => 'test-current-page-id'
        ], StateForAll::class, $appointmentId);
        $this->setCacheItem($cache, [
            'videoGuideToken' => 'test-video-guide-token',
        ], StateForGuides::class, $appointmentId);
        $this->setCacheItem($cache, [
            'videoClientToken' => 'test-video-client-token',
        ], StateForClients::class, $appointmentId);
        $this->setCacheItem($cache, [
            'attendeeName' => 'test-attendee-name',
        ], StateForMe::class, $appointmentId);

        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult();
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::never())->method('dispatch');

        $service = new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        $state = $service->getStateForAll();
        $data = $state->getData();
        static::assertSame('test-current-guide-product-id', $data['currentGuideProductId']);
        static::assertSame('test-last-active-guide-section', $data['lastActiveGuideSection']);
        static::assertSame('test-current-page-id', $data['currentPageId']);

        $state = $service->getStateForGuides();
        $data = $state->getData();
        static::assertSame('test-video-guide-token', $data['videoGuideToken']);

        $state = $service->getStateForClients();
        $data = $state->getData();
        static::assertSame('test-video-client-token', $data['videoClientToken']);

        $state = $service->getStateForMe();
        $data = $state->getData();
        static::assertSame('test-attendee-name', $data['attendeeName']);
    }

    public function testLoadStatesFromCacheButSkipItAndReInitialize(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());
        $context->addExtension(PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION, new ArrayStruct([true]));

        $cache = new TagawareAdapterFake();

        $this->setCacheItem($cache, [
            'currentGuideProductId' => 'test-current-guide-product-id',
            'lastActiveGuideSection' => 'test-last-active-guide-section',
            'currentPageId' => 'test-current-page-id'
        ], StateForAll::class, $appointmentId);

        $this->setCacheItem($cache, [
            'videoGuideToken' => 'test-video-guide-token',
        ], StateForGuides::class, $appointmentId);

        $this->setCacheItem($cache, [
            'videoClientToken' => 'test-video-client-token',
        ], StateForClients::class, $appointmentId);

        $this->setCacheItem($cache, [
            'attendeeName' => 'test-attendee-name',
        ], StateForMe::class, $appointmentId);

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId('test-appointment-id');
        $attendee = (new AttendeeMockHelper())->getAttendeeEntity($appointmentId, AttendeeDefinition::TYPE_GUIDE);
        $appointment->setAttendees(new AttendeeCollection([$attendee]));
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::once())
            ->method('dispatch')
            ->with(static::isInstanceOf(StateReInitializedEvent::class));

        $service = new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        $state = $service->getStateForAll();
        $data = $state->getData();
        static::assertSame('test-current-guide-product-id', $data['currentGuideProductId']);
        static::assertSame('test-last-active-guide-section', $data['lastActiveGuideSection']);
        static::assertSame('test-current-page-id', $data['currentPageId']);

        $state = $service->getStateForGuides();
        $data = $state->getData();
        static::assertSame('test-video-guide-token', $data['videoGuideToken']);

        $state = $service->getStateForClients();
        $data = $state->getData();
        static::assertSame('test-video-client-token', $data['videoClientToken']);

        $state = $service->getStateForMe();
        $data = $state->getData();
        static::assertSame('test-attendee-name', $data['attendeeName']);
    }

    public function testLoadStatesEvenTheStatesAlreadyLoadedBefore(): void
    {
        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $context = new Context(new SystemSource());
        $cache = new TagawareAdapterFake();
        $appointmentId = $appointment->getId();

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::once())->method('dispatch');

        $service = new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        static::expectException(\Exception::class);
        static::expectExceptionMessage('States are already loaded.');
        $service->loadStates($appointmentId, $context);
    }

    public function testReInitializeStatesWithException(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());
        $context->addExtension(PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION, new ArrayStruct([true]));

        $cache = new TagawareAdapterFake();

        $this->setCacheItem($cache, [
            'currentGuideProductId' => 'test-current-guide-product-id',
            'lastActiveGuideSection' => 'test-last-active-guide-section',
            'currentPageId' => 'test-current-page-id'
        ], StateForAll::class, $appointmentId);

        $this->setCacheItem($cache, [
            'videoGuideToken' => 'test-video-guide-token',
        ], StateForGuides::class, $appointmentId);

        $this->setCacheItem($cache, [
            'videoClientToken' => 'test-video-client-token',
        ], StateForClients::class, $appointmentId);

        $this->setCacheItem($cache, [
            'attendeeName' => 'test-attendee-name',
        ], StateForMe::class, $appointmentId);

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId('test-diff-appointment-id');
        $attendee = (new AttendeeMockHelper())->getAttendeeEntity($appointmentId, AttendeeDefinition::TYPE_GUIDE);
        $appointment->setAttendees(new AttendeeCollection([$attendee]));
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::never())->method('dispatch');

        static::expectException(\Exception::class);
        static::expectExceptionMessage('Appointment id from service must match the appointment id from the entity.');
        new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);
    }

    public function testLoadStatesWithoutCache(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId($appointmentId);
        /** @var VideoChatEntity $videoChat */
        $videoChat = $appointment->getVideoChat();
        $videoChat->setUrl('test-video-chat-url');
        $appointment->setVideoChat($videoChat);
        $appointment->setVideoAudioSettings('test-video-audio-settings');
        $attendee = (new AttendeeMockHelper())->getAttendeeEntity($appointmentId, AttendeeDefinition::TYPE_CLIENT);
        $attendees = new AttendeeCollection([$attendee]);
        $appointment->setAttendees($attendees);
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create([
            ConfigKeys::PRODUCT_DETAIL_DEFAULT_PAGE_ID => 'test-detail-default-page-id',
            ConfigKeys::PRODUCT_LISTING_DEFAULT_PAGE_ID => 'test-listing-default-page-id',
            ConfigKeys::QUICK_VIEW_PAGE_ID => 'test-quick-view-page-id',
            ConfigKeys::ALLOW_USER_ACTIONS_FOR_GUIDE => true
        ]);

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::once())
            ->method('dispatch')
            ->with(static::isInstanceOf(StateInitializedEvent::class));

        $service = new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        $stateForAll = $service->getStateForAll();
        $data = $stateForAll->getData();
        static::assertEquals($appointment->getEndedAt(), $data['endedAt']);
        static::assertEquals($appointment->getStartedAt(), $data['startedAt']);
        static::assertEquals($appointment->getAttendeeRestrictionType(), $data['attendeeRestrictionType']);
        static::assertEquals($appointment->getMode(), $data['appointmentMode']);
        static::assertEquals($appointment->getAccessibleFrom(), $data['accessibleFrom']);
        static::assertEquals($appointment->getAccessibleTo(), $data['accessibleTo']);
        static::assertEquals($appointment->getAccessibleTo(), $data['accessibleTo']);
        static::assertEquals('test-detail-default-page-id', $stateForAll->getProductDetailDefaultPageId());
        static::assertEquals('test-listing-default-page-id', $stateForAll->getProductListingDefaultPageId());
        static::assertEquals('test-quick-view-page-id', $stateForAll->getQuickViewPageId());
        static::assertTrue($stateForAll->getAllowUserActionsForGuide());
        static::assertEquals($appointment->getVideoAudioSettings(), $data['videoAudioSettings']);
        static::assertEquals($videoChat->getUrl(), $data['videoRoomUrl']);
        static::assertEquals($videoChat->isStartAsBroadcast(), $data['broadcastMode']);

        $stateForGuides = $service->getStateForGuides();
        $data = $stateForGuides->getData();
        static::assertSame($appointment->getSalesChannelDomain()?->getSalesChannelId(), $stateForGuides->getSalesChannelId());
        static::assertEquals($videoChat->getOwnerToken(), $data['videoGuideToken']);

        $stateForClients = $service->getStateForClients();
        $data = $stateForClients->getData();
        static::assertEquals($videoChat->getUserToken(), $data['videoClientToken']);

        // check if the cache is updated after publishing new value to states
        $stateForAll->setCurrentSlide('test-current-page-id', 'test-current-section-id', 1);
        $service->publishStateForAll();
        $cacheKey = PresentationStateService::CACHE_KEY_PREFIX . '_' . \md5(StateForAll::class) . '_' . $appointmentId;
        $item = $cache->getItem($cacheKey);
        $data = $item->get();
        static::assertEquals('test-current-page-id', $data['currentPageId']);
        static::assertEquals('test-current-section-id', $data['currentSectionId']);
        static::assertEquals(1, $data['currentSlideAlias']);

        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAttendeeName('test-attendee-name');
        $attendee->setVideoUserId(null);
        $attendee->setType('CLIENT');
        $stateForGuides->addClient($attendee);
        $service->publishStateForGuides();
        $cacheKey = PresentationStateService::CACHE_KEY_PREFIX . '_' . \md5(StateForGuides::class) . '_' . $appointmentId;
        $item = $cache->getItem($cacheKey);
        $data = $item->get();
        static::assertArrayHasKey('test-attendee-id', $data['clients']);
        static::assertEquals('test-attendee-id', $data['clients']['test-attendee-id']['attendeeId']);
        static::assertEquals('test-attendee-name', $data['clients']['test-attendee-id']['attendeeName']);
        static::assertNull($data['clients']['test-attendee-id']['videoUserId']);
        static::assertFalse($data['clients']['test-attendee-id']['guideCartPermissionsGranted']);

        $stateForClients->setVideoClientToken('test-video-client-token');
        $service->publishStateForClients();
        $cacheKey = PresentationStateService::CACHE_KEY_PREFIX . '_' . \md5(StateForClients::class) . '_' . $appointmentId;
        $item = $cache->getItem($cacheKey);
        $data = $item->get();
        static::assertEquals($data['videoClientToken'], 'test-video-client-token');

        $stateForMe = $service->getStateForMe();
        $stateForMe->setAttendeeName('test-my-name');
        $service->publishStateForMe();
        $cacheKey = PresentationStateService::CACHE_KEY_PREFIX . '_' . \md5(StateForMe::class) . '_' . $appointmentId;
        $item = $cache->getItem($cacheKey);
        $data = $item->get();
        static::assertEquals($data['attendeeName'], 'test-my-name');
    }

    public function testLoadStatesWithoutCacheButCanNotFindAppointment(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();

        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult();
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::never())->method('dispatch');

        static::expectException(\Exception::class);
        new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);
    }

    public function testLoadStatesWithoutCacheButAppointmentIdIsDifferentWithAppointmentIdFromService(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId('test-diff-appointment-id');
        /** @var VideoChatEntity $videoChat */
        $videoChat = $appointment->getVideoChat();
        $videoChat->setUrl('test-video-chat-url');
        $appointment->setVideoChat($videoChat);
        $appointment->setVideoAudioSettings('test-video-audio-settings');
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::never())->method('dispatch');

        static::expectException(\Exception::class);
        new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);
    }

    public function testLoadStatesWithoutCacheButAppointmentHasNoVideoChat(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId($appointmentId);
        $appointment->setVideoAudioSettings('test-video-audio-settings');
        $appointment->setVideoChat(null);
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::never())->method('dispatch');

        static::expectException(VideoChatEntityDoesNotExist::class);
        new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);
    }

    public function testThatDataArePublishedToMercure(): void
    {
        $appointmentId = 'my-appointment-test-id-1';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();
        $publisherFake = new PublisherFake();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId($appointmentId);
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);

        $service = new PresentationStateService($cache, $appointmentId, $publisherFake, '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        $state = $service->getStateForAll();
        $state->setCurrentSlide('test-current-page-id', 'test-current-section-id', 1);
        $service->publishStateForAll();

        // when we initialize the service, the publisher called 4 times. So, check the data of the 5th call
        $lastPublish = $publisherFake->getPublishHistory()[4];
        static::assertEquals('test-current-page-id', $lastPublish['update']['state-for-all']['currentPageId']);
    }

    public function testThatItIsNotInitializesWhenCacheExists(): void
    {
        $appointmentId = 'my-appointment-test-id-1';
        $context = new Context(new SystemSource());

        $publisherFake = new PublisherFake();

        $appointment = new AppointmentEntity();
        $appointment->setId('my-appointment-test-id-1');
        $appointment->setVideoAudioSettings(AppointmentDefinition::NONE_VIDEO_AUDIO);
        $accessibleFrom = new \DateTimeImmutable('2020-10-10 10:00:05');
        $accessibleTo = new \DateTimeImmutable('2020-10-10 22:00:05');
        $appointment->setAccessibleFrom($accessibleFrom);
        $appointment->setAccessibleTo($accessibleTo);
        $appointment->setAttendeeRestrictionType('restriction_1');
        $appointment->setStartedAt(null);
        $appointment->setEndedAt(null);
        $appointment->setMode('guided');
        $appointmentSearchResult = (new AppointMentMockHelper())->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $existingCache = new TagawareAdapterFake();
        $cacheKeyForAll = $this->getCacheKey(StateForAll::class, $appointment->getId());
        $item = $existingCache->getItem($cacheKeyForAll);
        $endedAt = new \DateTimeImmutable('2021-01-25 09:00:09');
        $itemValueForAll = [
            'currentGuideProductId' => null,
            'lastActiveGuideSection' => null,
            'currentPageId' => null,
            'currentSectionId' => null,
            'currentSlideAlias' => 1,
            'currentDynamicPage' => null,
            'started' => true,
            'running' => false,
            'ended' => false,
            'startedAt' => null,
            'endedAt' => $endedAt,
            'appointmentMode' => 'self',
        ];
        $item->set($itemValueForAll);
        $existingCache->fakeSetItem($item, $cacheKeyForAll);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $service = new PresentationStateService($existingCache, $appointmentId, $publisherFake, '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        $stateForAll = $service->getStateForAll();
        $stateForAllData = $stateForAll->getData();
        static::assertTrue($stateForAllData['started']);
        static::assertEquals('self', $stateForAllData['appointmentMode']);
        static::assertSame($endedAt, $stateForAllData['endedAt']);
    }

    public function testThatDataAreNotPublishedToMercureIfNothingChanged(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();

        $this->setCacheItem($cache, [
            'currentGuideProductId' => 'test-current-guide-product-id',
            'lastActiveGuideSection' => 'test-last-active-guide-section',
            'currentPageId' => 'test-current-page-id'
        ], StateForAll::class, $appointmentId);

        $this->setCacheItem($cache, [
            'videoGuideToken' => 'test-video-guide-token',
        ], StateForGuides::class, $appointmentId);

        $this->setCacheItem($cache, [
            'videoClientToken' => 'test-video-client-token',
        ], StateForClients::class, $appointmentId);

        $this->setCacheItem($cache, [
            'attendeeName' => 'test-attendee-name',
        ], StateForMe::class, $appointmentId);

        $publisherFake = new PublisherFake();
        $appointmentMockHelper = new AppointMentMockHelper();
        $appointment = $appointmentMockHelper->getAppointEntity();
        $appointment->setId('my-appointment-test-id-1');
        $appointmentSearchResult = $appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);

        $service = new PresentationStateService($cache, $appointmentId, $publisherFake, '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        // change the value to the value from the cache
        $state = $service->getStateForAll();
        $state->setCurrentGuideProductId('test-current-guide-product-id');
        $service->publishStateForAll();

        $state = $service->getStateForGuides();
        $state->setVideoGuideToken('test-video-guide-token');
        $service->publishStateForGuides();

        $state = $service->getStateForClients();
        $state->setVideoClientToken('test-video-client-token');
        $service->publishStateForClients();

        $state = $service->getStateForMe();
        $state->setAttendeeName('test-attendee-name');
        $service->publishStateForMe();

        // if the publisher has no history means that nothing is pushed
        static::assertCount(0, $publisherFake->getPublishHistory());
    }

    /**
     * @dataProvider getTestGetStateIfStateCollectionIsEmpty
     */
    public function testGetStateIfStateCollectionIsEmpty(
        string $stateType
    ): void
    {
        $context = new Context(new SystemSource());
        $appointmentId = 'test-appointment-id';

        $cache = new TagawareAdapterFake();

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId($appointmentId);
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcherMock->expects(static::once())
            ->method('dispatch')
            ->with(static::isInstanceOf(StateInitializedEvent::class));

        $service = new PresentationStateService($cache, $appointmentId, new PublisherFake(), '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        $reflectionClass = new \ReflectionClass($service);
        $reflectionProperty = $reflectionClass->getProperty('stateCollection');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($service, null);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('States are not initialized.');
        switch ($stateType) {
            case 'all':
                $service->getStateForAll();
                break;
            case 'guides':
                $service->getStateForGuides();
                break;
            case 'clients':
                $service->getStateForClients();
                break;
            case 'me':
                $service->getStateForMe();
                break;
        }
    }

    public static function getTestGetStateIfStateCollectionIsEmpty(): \Generator
    {
        yield ['all'];
        yield ['guides'];
        yield ['clients'];
        yield ['me'];
    }

    public function testPublishStateFail(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new SystemSource());

        $cache = new TagawareAdapterFake();
        $this->setCacheItem($cache, [
            'currentGuideProductId' => 'test-current-guide-product-id',
            'lastActiveGuideSection' => 'test-last-active-guide-section',
            'currentPageId' => 'test-current-page-id'
        ], StateForAll::class, $appointmentId);

        // create the publisher will throw an exception when call publish method
        $publisherFake = new PublisherFake(true);

        $appointment = $this->appointmentMockHelper->getAppointEntity();
        $appointment->setId($appointmentId);
        $appointmentSearchResult = $this->appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $configSystemMock = $systemConfigMockBuilder->create();

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);

        $service = new PresentationStateService($cache, $appointmentId, $publisherFake, '', new TopicGenerator(), $appointmentRepository, $context, $configSystemMock, $eventDispatcherMock);

        // change the value to the value from the cache
        $state = $service->getStateForAll();
        $state->setCurrentGuideProductId('test-diff-current-guide-product-id');
        static::expectException(\Exception::class);
        $service->publishStateForAll();
    }

    private function getCacheKey(string $className, string $appointmentId): string
    {
        return PresentationStateService::CACHE_KEY_PREFIX . '_' . \md5($className) . '_' . $appointmentId;
    }

    /**
     * @param array<string, mixed> $data
     */
    private function setCacheItem(TagawareAdapterFake $cache, array $data, string $state, string $appointmentId): void
    {
        $key = PresentationStateService::CACHE_KEY_PREFIX . '_' . \md5($state) . '_' . $appointmentId;
        $item = $this->cacheMockBuilder->generateCacheItem($key, $data);
        $item->expiresAfter(new \DateInterval('P10D'));
        $cache->fakeSetItem($item, $key);
    }
}
