<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\PresentationState\Exception\AttendeeMissingAppointmentException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AttendeeMissingAppointmentExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $exception = new AttendeeMissingAppointmentException();
        $expectMessage = 'Attendee missing the appointment.';
        static::assertEquals($expectMessage, $exception->getMessage());
    }

    public function testGetErrorCode(): void
    {
        $exception = new AttendeeMissingAppointmentException();
        $expectErrorCode = ErrorCode::GUIDED_SHOPPING__ATTENDEE_MISSING_APPOINTMENT;
        static::assertEquals($expectErrorCode, $exception->getErrorCode());
    }

    public function testGetStatusCode(): void
    {
        $exception = new AttendeeMissingAppointmentException();
        $expectErrorCode = Response::HTTP_BAD_REQUEST;
        static::assertEquals($expectErrorCode, $exception->getStatusCode());
    }
}
