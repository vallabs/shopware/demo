<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Event;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\PresentationState\Event\StateInitializedEvent;
use SwagGuidedShopping\Content\PresentationState\State\StateCollection;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class StateInitializedEventTest extends TestCase
{
    public function testInitialize(): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();

        $stateForAll = new StateForAll('test-appointment-id', 'test-mercure-topic');
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $stateForClients = new StateForClients('test-appointment-id', 'test-mercure-topic');
        $stateForMe = new StateForMe('test-appointment-id', 'test-mercure-topic');

        $stateCollection = new StateCollection($stateForAll, $stateForClients, $stateForGuides, $stateForMe);

        $context = new Context(new SystemSource());

        $event = new StateInitializedEvent($stateCollection, $appointment, $context);

        static::assertSame($stateCollection, $event->getStateCollection());
        static::assertSame($appointment, $event->getAppointment());
        static::assertSame($context, $event->getContext());
    }
}
