<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\SalesChannel\Get;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Tests\Unit\Stub\GetClientPresentationStateStructStub;

class GetClientPresentationStateStructTest extends TestCase
{
    public function testInitialize(): void
    {
        $stateForAll = new StateForAll(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $stateForClients = new StateForClients(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $stateForMe = new StateForMe(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );

        $getClientPresentationStateStruct = new GetClientPresentationStateStructStub(
            $stateForAll,
            $stateForClients,
            $stateForMe
        );

        static::assertSame($stateForAll->getData(), $getClientPresentationStateStruct->getStateForAll());
        static::assertSame($stateForClients->getData(), $getClientPresentationStateStruct->getStateForClients());
        static::assertSame($stateForMe->getData(), $getClientPresentationStateStruct->getStateForMe());
    }
}