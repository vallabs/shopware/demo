<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\SalesChannel\Get;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\PresentationState\SalesChannel\Get\GetClientPresentationStateResponse;
use SwagGuidedShopping\Content\PresentationState\SalesChannel\Get\GetClientPresentationStateStruct;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetClientPresentationStateResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $stateForAll = $this->createMock(StateForAll::class);
        $stateForClients = $this->createMock(StateForClients::class);
        $stateForMe = $this->createMock(StateForMe::class);
        $data = new GetClientPresentationStateStruct($stateForAll, $stateForClients, $stateForMe);
        $response = new GetClientPresentationStateResponse($data);
        static::assertSame($data, $response->getObject());
    }
}
