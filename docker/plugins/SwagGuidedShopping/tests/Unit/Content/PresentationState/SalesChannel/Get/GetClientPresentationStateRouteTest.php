<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\SalesChannel\Get;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\SalesChannel\Get\GetClientPresentationStateRoute;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Content\PresentationState\Exception\AttendeeMissingAppointmentException;

class GetClientPresentationStateRouteTest extends TestCase
{
    public function testGetDecorated(): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::never())->method('build');
        $route = new GetClientPresentationStateRoute($stateServiceFactory);
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    /**
     * @dataProvider getTestGetStateWithInvalidSalesChannelContextProviderData
     * @param class-string<\Throwable> $exception
     */
    public function testGetStateWithInvalidSalesChannelContext(
        SalesChannelContext $salesChannelContext,
        string $exception
    ): void
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::never())->method('build');
        $route = new GetClientPresentationStateRoute($stateServiceFactory);
        static::expectException($exception);
        $route->getState($salesChannelContext);
    }

    public static function getTestGetStateWithInvalidSalesChannelContextProviderData(): \Generator
    {
        $salesChannelContextHelper = new SalesChannelContextHelper();
        $withoutAttendeeSalesChannelContext = $salesChannelContextHelper->createSalesChannelContext();
        yield 'without-attendee' => [
            $withoutAttendeeSalesChannelContext,
            SalesChannelContextMissingAttendeeException::class
        ];

        $attendeeWithoutAppointmentIdSalesChannelContext = $salesChannelContextHelper->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $attendeeWithoutAppointmentIdSalesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        yield 'attendee-without-appointment-id' => [
            $attendeeWithoutAppointmentIdSalesChannelContext,
            AttendeeMissingAppointmentException::class
        ];
    }

    public function testGetStateWithValidSalesChannelContext(): void
    {
        $salesChannelContextHelper = new SalesChannelContextHelper();
        $saleChannelContext = $salesChannelContextHelper->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $attendee->setAttendeeName('test-attendee-name');
        $attendee->setGuideCartPermissionsGranted(true);
        $submittedAt = new \DateTime();
        $attendee->setAttendeeSubmittedAt($submittedAt);
        $attendee->setAppointmentId('test-appointment-id');
        $saleChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateService = $this->createMock(PresentationStateService::class);
        // StateForAll
        $stateForAll = new StateForAll('test-appointment-id', 'test-mercure-topic', []);
        $stateService->expects(static::once())
            ->method('getStateForAll')
            ->willReturn($stateForAll);
        // StateForClient
        $stateForClients = new StateForClients('test-appointment-id', 'test-mercure-topic', []);
        $stateService->expects(static::once())
            ->method('getStateForClients')
            ->willReturn($stateForClients);
        // StateForMe
        $stateForMe = new StateForMe('test-appointment-id', 'test-mercure-topic', []);
        $stateService->expects(static::once())
            ->method('getStateForMe')
            ->willReturn($stateForMe);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo('test-appointment-id'),
                static::equalTo($saleChannelContext->getContext())
            )
            ->willReturn($stateService);
        $route = new GetClientPresentationStateRoute($stateServiceFactory);
        $response = $route->getState($saleChannelContext);
        $states = $response->getObject()->getVars();

        static::assertArrayHasKey('stateForAll', $states);
        static::assertArrayHasKey('stateForClients', $states);
        static::assertArrayHasKey('stateForMe', $states);
        static::assertEquals($stateForAll->getData(), $states['stateForAll']);
        static::assertEquals($stateForClients->getData(), $states['stateForClients']);
        // expect add attendee
        $stateForMe->setAttendee($attendee);
        static::assertEquals($stateForMe->getData(), $states['stateForMe']);
    }
}
