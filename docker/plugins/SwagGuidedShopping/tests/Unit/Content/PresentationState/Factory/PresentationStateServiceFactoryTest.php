<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Factory;

use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Tests\Unit\Content\Mercure\Service\PublisherFake;
use SwagGuidedShopping\Tests\Unit\Fake\FakeRepository;
use SwagGuidedShopping\Tests\Unit\Fake\TagawareAdapterFake;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\SystemConfigMockBuilder;

/**
 * @internal
 * @coversDefaultClass \SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory
 */
class PresentationStateServiceFactoryTest extends TestCase
{
    public function testThatServiceIsASingelton(): void
    {
        $appointmentMockHelper = new AppointMentMockHelper();
        $appointment = $appointmentMockHelper->getAppointEntity();
        $appointmentId = 'my-appointment-1';
        $appointment->setId($appointmentId);
        $factory = $this->getFactory($appointment);

        $context = new Context(new SystemSource());
        $service = $factory->build($appointmentId, $context);

        $service2 = $factory->build($appointmentId, $context);

        static::assertSame($service, $service2);
    }

    public function getFactory(AppointmentEntity $appointment): PresentationStateServiceFactory
    {
        $cache = new TagawareAdapterFake();
        $systemConfigMockBuilder = new SystemConfigMockBuilder('test');

        $configSystemMock = $systemConfigMockBuilder->create([]);
        $JwtMercureTokenFactory = new JWTMercureTokenFactory($configSystemMock);

        $appointmentMockHelper = new AppointMentMockHelper();

        $appointmentSearchResult = $appointmentMockHelper->createSearchResult([$appointment]);
        /** @var EntityRepository<AppointmentCollection>&FakeRepository $appointmentRepository */
        $appointmentRepository = new FakeRepository($appointmentSearchResult);

        $eventDispatcherMock = $this->createMock(EventDispatcherInterface::class);
        return new PresentationStateServiceFactory($cache, $JwtMercureTokenFactory, new PublisherFake(), new TopicGenerator(), $appointmentRepository, $configSystemMock, $eventDispatcherMock);
    }
}
