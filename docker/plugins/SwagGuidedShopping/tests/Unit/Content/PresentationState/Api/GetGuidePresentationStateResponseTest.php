<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Api;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\PresentationState\Api\GetGuidePresentationStateResponse;
use SwagGuidedShopping\Content\PresentationState\Api\GetGuidePresentationStateStruct;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetGuidePresentationStateResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $expectStateForAll = new StateForAll(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $expectStateForGuide = new StateForGuides(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $expectStateForMe = new StateForMe(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $struct = new GetGuidePresentationStateStruct($expectStateForAll, $expectStateForGuide, $expectStateForMe);
        $response = new GetGuidePresentationStateResponse($struct);
        /** @var string $responseContent */
        $responseContent = $response->getContent();
        /** @var array<int|string, mixed> $states */
        $states = (array) @\json_decode($responseContent);
        static::assertArrayHasKey('stateForAll', $states);
        static::assertArrayHasKey('stateForGuides', $states);
        static::assertEquals($expectStateForAll->getData(), (array) $states['stateForAll']);
        static::assertEquals($expectStateForGuide->getData(), (array) $states['stateForGuides']);
        static::assertEquals($expectStateForMe->getData(), (array) $states['stateForMe']);
    }
}
