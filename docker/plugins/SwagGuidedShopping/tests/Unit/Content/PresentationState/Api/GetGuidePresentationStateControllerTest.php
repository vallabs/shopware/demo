<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Api;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\Api\GetGuidePresentationStateController;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetGuidePresentationStateControllerTest extends TestCase
{
    public function testGetStateButAttendeeNotFound(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new AdminApiSource('test-user-id'));

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateForMe = $this->createMock(StateForMe::class);
        $stateForMe->expects(static::never())->method('setAttendee');
        $stateService->expects(static::once())
            ->method('getStateForMe')
            ->willReturn($stateForMe);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeDefinition::ENTITY_NAME,
                0,
                new AttendeeCollection(),
                null,
                new Criteria(),
                $context
            ));

        $controller = new GetGuidePresentationStateController($attendeeRepository, $stateServiceFactory);

        $response = $controller->getState($appointmentId, $context);
        static::assertNotFalse($response->getContent());
        $extensionName = PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION;
        static::assertTrue($context->hasExtension($extensionName));
        static::assertInstanceOf(ArrayStruct::class, $context->getExtension($extensionName));
    }

    public function testGetStateWithAttendeeFound(): void
    {
        $appointmentId = 'test-appointment-id';
        $context = new Context(new AdminApiSource('test-user-id'));

        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateForMe = $this->createMock(StateForMe::class);
        $stateForMe->expects(static::once())
            ->method('setAttendee')
            ->with(static::equalTo($attendee));
        $stateService->expects(static::once())
            ->method('getStateForMe')
            ->willReturn($stateForMe);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeDefinition::ENTITY_NAME,
                1,
                new AttendeeCollection([$attendee]),
                null,
                new Criteria(),
                $context
            ));

        $controller = new GetGuidePresentationStateController($attendeeRepository, $stateServiceFactory);

        $response = $controller->getState($appointmentId, $context);
        static::assertNotFalse($response->getContent());
        $extensionName = PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION;
        static::assertTrue($context->hasExtension($extensionName));
        static::assertInstanceOf(ArrayStruct::class, $context->getExtension($extensionName));
    }
}
