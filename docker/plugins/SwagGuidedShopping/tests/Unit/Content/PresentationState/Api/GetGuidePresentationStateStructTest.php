<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\Api;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Tests\Unit\Stub\GetGuidePresentationStateStructStub;

class GetGuidePresentationStateStructTest extends TestCase
{
    public function testInitialize(): void
    {
        $stateForAll = new StateForAll(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $stateForGuide = new StateForGuides(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );
        $stateForMe = new StateForMe(
            'test-appointment-id',
            'test-mercure-topic',
            []
        );

        $struct = new GetGuidePresentationStateStructStub($stateForAll, $stateForGuide, $stateForMe);
        static::assertSame($stateForAll->getData(), $struct->getStateForAll());
        static::assertSame($stateForGuide->getData(), $struct->getStateForGuides());
        static::assertSame($stateForMe->getData(), $struct->getStateForMe());
    }
}
