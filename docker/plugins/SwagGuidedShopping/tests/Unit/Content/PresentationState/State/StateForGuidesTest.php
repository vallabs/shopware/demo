<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\State;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;

class StateForGuidesTest extends TestCase
{
    private StateForGuides $stateForGuides;

    protected function setUp(): void
    {
        parent::setUp();
        $this->stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
    }

    public function testAddClient(): void
    {
        $clients = $this->stateForGuides->getClients();
        static::assertCount(0, $clients);

        $client = $this->createAttendee('CLIENT');
        $submittedAt = new \DateTime('2021-01-01T00:00:00+00:00');
        $client->setAttendeeSubmittedAt($submittedAt);
        $this->stateForGuides->addClient($client);
        $clients = $this->stateForGuides->getClients();

        $clientId = $client->getId();
        static::assertSame([
            'attendeeId' => $clientId,
            'attendeeName' => $client->getAttendeeName(),
            'videoUserId' => $client->getVideoUserId(),
            'guideCartPermissionsGranted' => $client->isGuideCartPermissionsGranted(),
            'hasJoined' => true,
        ], $clients[$clientId]);

        $this->stateForGuides->setClientName($clientId, 'test-new-attendee-name');
        $this->stateForGuides->setClientVideoUserId($clientId, null);
        $this->stateForGuides->setGuideCartPermissionsGranted($clientId, true);
        $clients = $this->stateForGuides->getClients();
        static::assertSame('test-new-attendee-name', $clients[$clientId]['attendeeName']);
        static::assertNull($clients[$clientId]['videoUserId']);
        static::assertTrue($clients[$clientId]['guideCartPermissionsGranted']);

        $this->stateForGuides->removeAttendee($clientId);
        static::assertArrayNotHasKey($clientId, $this->stateForGuides->getClients());
        static::assertArrayHasKey($clientId, $this->stateForGuides->getInactiveClients());
        static::assertSame([
            'attendeeId' => $clientId,
            'attendeeName' => 'test-new-attendee-name',
            'videoUserId' => null,
            'guideCartPermissionsGranted' => true,
            'hasJoined' => true,
        ], $this->stateForGuides->getInactiveClients()[$clientId]);
    }

    public function testAddInactiveClient(): void
    {
        $inactiveClients = $this->stateForGuides->getInactiveClients();
        static::assertCount(0, $inactiveClients);

        $client = $this->createAttendee('CLIENT');
        $client->setAttendeeSubmittedAt(null);
        $this->stateForGuides->addInactiveClient($client);
        static::assertArrayHasKey($client->getId(), $this->stateForGuides->getInactiveClients());
        static::assertSame([
            'attendeeId' => $client->getId(),
            'attendeeName' => $client->getAttendeeName(),
            'videoUserId' => $client->getVideoUserId(),
            'guideCartPermissionsGranted' => $client->isGuideCartPermissionsGranted(),
            'hasJoined' => false,
        ], $this->stateForGuides->getInactiveClients()[$client->getId()]);

        $this->stateForGuides->removeInactiveAttendee($client->getId());
        static::assertArrayNotHasKey($client->getId(), $this->stateForGuides->getInactiveClients());
    }

    public function testAddGuide(): void
    {
        $guides = $this->stateForGuides->getGuides();
        static::assertCount(0, $guides);

        $guide = $this->createAttendee('GUIDE');
        $submittedAt = new \DateTime('2021-01-01T00:00:00+00:00');
        $guide->setAttendeeSubmittedAt($submittedAt);
        $this->stateForGuides->addGuide($guide);
        $guideId = $guide->getId();
        static::assertArrayHasKey($guideId, $this->stateForGuides->getGuides());
        static::assertSame([
            'attendeeId' => $guideId,
            'attendeeName' => $guide->getAttendeeName(),
            'videoUserId' => $guide->getVideoUserId(),
            'guideCartPermissionsGranted' => $guide->isGuideCartPermissionsGranted(),
            'hasJoined' => false,
        ], $this->stateForGuides->getGuides()[$guideId]);

        $this->stateForGuides->setGuideVideoUserId($guideId, null);
        $this->stateForGuides->setGuideName($guideId, 'test-new-guide-name');
        static::assertNull($this->stateForGuides->getGuides()[$guideId]['videoUserId']);
        static::assertSame('test-new-guide-name', $this->stateForGuides->getGuides()[$guideId]['attendeeName']);
    }

    public function testSetVideoGuideToken(): void
    {
        $data = $this->stateForGuides->getData();
        static::assertNull($data['videoGuideToken']);

        $this->stateForGuides->setVideoGuideToken('test-video-guide-token');
        $data = $this->stateForGuides->getData();
        static::assertSame('test-video-guide-token', $data['videoGuideToken']);
    }

    public function testGetMercureUpdatePayload(): void
    {
        $payload = $this->stateForGuides->getMercureUpdatePayload();
        static::assertArrayHasKey('state-for-guides', $payload);
        $expectData = $this->stateForGuides->getData();
        static::assertSame($expectData, $payload['state-for-guides']);
    }

    public function testSetQuickViewStateForClient(): void
    {
        $data = $this->stateForGuides->getData();
        static::assertCount(0, $data['quickViewState']);

        // add quick state of test-product-id for test-attendee-id with state is true
        $this->stateForGuides->setQuickViewStateForClient('test-product-id', 'test-attendee-id', true);
        $data = $this->stateForGuides->getData();
        static::assertCount(1, $data['quickViewState']);
        static::assertArrayHasKey('test-product-id', $data['quickViewState']);
        static::assertCount(1, $data['quickViewState']['test-product-id']);
        static::assertArrayHasKey('test-attendee-id', $data['quickViewState']['test-product-id']);
        static::assertSame('test-attendee-id', $data['quickViewState']['test-product-id']['test-attendee-id']);

        // add quick state of test-product-id for test-attendee-id-2, but state is false
        $this->stateForGuides->setQuickViewStateForClient('test-product-id', 'test-attendee-id-2', false);
        $data = $this->stateForGuides->getData();
        static::assertCount(1, $data['quickViewState']);
        static::assertArrayHasKey('test-product-id', $data['quickViewState']);
        static::assertCount(1, $data['quickViewState']['test-product-id']);
        static::assertArrayHasKey('test-attendee-id', $data['quickViewState']['test-product-id']);
        static::assertSame('test-attendee-id', $data['quickViewState']['test-product-id']['test-attendee-id']);

        // add quick state of test-product-id for test-attendee-id-2 with state is true
        $this->stateForGuides->setQuickViewStateForClient('test-product-id', 'test-attendee-id-2', true);
        $data = $this->stateForGuides->getData();
        static::assertCount(1, $data['quickViewState']);
        static::assertArrayHasKey('test-product-id', $data['quickViewState']);
        static::assertCount(2, $data['quickViewState']['test-product-id']);
        static::assertArrayHasKey('test-attendee-id', $data['quickViewState']['test-product-id']);
        static::assertArrayHasKey('test-attendee-id-2', $data['quickViewState']['test-product-id']);
        static::assertSame('test-attendee-id', $data['quickViewState']['test-product-id']['test-attendee-id']);
        static::assertSame('test-attendee-id-2', $data['quickViewState']['test-product-id']['test-attendee-id-2']);

        // add quick state of test-product-id-2 for test-attendee-id-2 with state is true
        $this->stateForGuides->setQuickViewStateForClient('test-product-id-2', 'test-attendee-id-2', true);
        $data = $this->stateForGuides->getData();
        static::assertCount(2, $data['quickViewState']);
        // check state of quickview for test-product-id
        static::assertArrayHasKey('test-product-id', $data['quickViewState']);
        static::assertCount(1, $data['quickViewState']['test-product-id']);
        static::assertArrayHasKey('test-attendee-id', $data['quickViewState']['test-product-id']);
        static::assertSame('test-attendee-id', $data['quickViewState']['test-product-id']['test-attendee-id']);
        // check state of quickview for test-product-id-2
        static::assertArrayHasKey('test-product-id-2', $data['quickViewState']);
        static::assertCount(1, $data['quickViewState']['test-product-id-2']);
        static::assertArrayHasKey('test-attendee-id-2', $data['quickViewState']['test-product-id-2']);
        static::assertSame('test-attendee-id-2', $data['quickViewState']['test-product-id-2']['test-attendee-id-2']);
    }

    public function testAddAttendee(): void
    {
        static::assertCount(0, $this->stateForGuides->getGuides());
        static::assertCount(0, $this->stateForGuides->getClients());
        static::assertCount(0, $this->stateForGuides->getInactiveClients());

        // add guide
        $guide = $this->createAttendee('GUIDE');
        $guide->setId('test-guide-id');
        $this->stateForGuides->addAttendee($guide);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(0, $this->stateForGuides->getClients());
        static::assertCount(0, $this->stateForGuides->getInactiveClients());

        // remove guide
        $this->stateForGuides->addInactiveAttendee($guide);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(0, $this->stateForGuides->getClients());
        static::assertCount(0, $this->stateForGuides->getInactiveClients());

        // add client
        $client = $this->createAttendee('CLIENT');
        $this->stateForGuides->addAttendee($client);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(1, $this->stateForGuides->getClients());
        static::assertArrayHasKey('test-attendee-id', $this->stateForGuides->getClients());
        static::assertCount(0, $this->stateForGuides->getInactiveClients());

        // remove client
        $this->stateForGuides->addInactiveAttendee($client);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(0, $this->stateForGuides->getClients());
        static::assertCount(1, $this->stateForGuides->getInactiveClients());
        static::assertArrayHasKey('test-attendee-id', $this->stateForGuides->getInactiveClients());

        // add client
        $this->stateForGuides->addAttendee($client);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(1, $this->stateForGuides->getClients());
        static::assertArrayHasKey('test-attendee-id', $this->stateForGuides->getClients());
        static::assertCount(0, $this->stateForGuides->getInactiveClients());

        // add inactive client
        $inactiveClient = $this->createAttendee('CLIENT');
        $inactiveClient->setId('test-inactive-client-id');
        $this->stateForGuides->addInactiveAttendee($inactiveClient);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(1, $this->stateForGuides->getClients());
        static::assertArrayHasKey('test-attendee-id', $this->stateForGuides->getClients());
        static::assertCount(1, $this->stateForGuides->getInactiveClients());
        static::assertArrayHasKey('test-inactive-client-id', $this->stateForGuides->getInactiveClients());

        // add client as an inactive client
        $this->stateForGuides->addInactiveAttendee($client);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(0, $this->stateForGuides->getClients());
        static::assertCount(2, $this->stateForGuides->getInactiveClients());
        static::assertArrayHasKey('test-attendee-id', $this->stateForGuides->getInactiveClients());
        static::assertArrayHasKey('test-inactive-client-id', $this->stateForGuides->getInactiveClients());

        // add client 2
        $client2 = clone $client;
        $client2->setId('test-attendee-id-2');
        $this->stateForGuides->addAttendee($client2);
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(1, $this->stateForGuides->getClients());
        static::assertArrayHasKey('test-attendee-id-2', $this->stateForGuides->getClients());
        static::assertCount(2, $this->stateForGuides->getInactiveClients());
        static::assertArrayHasKey('test-attendee-id', $this->stateForGuides->getInactiveClients());
        static::assertArrayHasKey('test-inactive-client-id', $this->stateForGuides->getInactiveClients());
        static::assertCount(3, $this->stateForGuides->getAllClients());

        // reset clients
        $this->stateForGuides->resetClients();
        static::assertCount(1, $this->stateForGuides->getGuides());
        static::assertArrayHasKey('test-guide-id', $this->stateForGuides->getGuides());
        static::assertCount(0, $this->stateForGuides->getClients());
        static::assertCount(0, $this->stateForGuides->getInactiveClients());
    }

    public function testGetAttendeeIds(): void
    {
        static::assertCount(0, $this->stateForGuides->getAttendeeIdsOfClients());
        static::assertCount(0, $this->stateForGuides->getAttendeeIdsOfGuides());

        // add client
        $client = $this->createAttendee('CLIENT');
        $this->stateForGuides->addAttendee($client);
        static::assertSame(['test-attendee-id'], $this->stateForGuides->getAttendeeIdsOfClients());

        // add inactive client
        $inactiveClient = $this->createAttendee('CLIENT');
        $inactiveClient->setId('test-inactive-attendee-id');
        $this->stateForGuides->addInactiveAttendee($inactiveClient);
        static::assertSame(['test-inactive-attendee-id'], $this->stateForGuides->getAttendeeIdsOfInactiveClients());

        // test get all attendees (active and inactive)
        static::assertSame(['test-attendee-id', 'test-inactive-attendee-id'], $this->stateForGuides->getAttendeeIdsForAllKindOfClients());
    }

    public function testAddAttendeeWithoutNameButEmail(): void
    {
        $attendee = $this->createAttendee('CLIENT');
        $attendee->setAttendeeName(null);
        $attendee->setAttendeeEmail('test-attendee-email');

        $this->stateForGuides->addAttendee($attendee);

        $clients = $this->stateForGuides->getClients();
        static::assertCount(1, $clients);
        static::assertSame([
            'attendeeId' => 'test-attendee-id',
            'attendeeName' => 'test-attendee-email',
            'videoUserId' => 'test-video-user-id',
            'guideCartPermissionsGranted' => false,
            'hasJoined' => false,
        ], $clients['test-attendee-id']);
    }

    private function createAttendee(string $type): AttendeeEntity
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAttendeeName('test-attendee-name');
        $attendee->setType($type);
        $attendee->setVideoUserId('test-video-user-id');
        $attendee->setGuideCartPermissionsGranted(false);

        return $attendee;
    }
}
