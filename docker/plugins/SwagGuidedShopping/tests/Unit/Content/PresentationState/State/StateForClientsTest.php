<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\State;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;

class StateForClientsTest extends TestCase
{
    public function testInitialize(): void
    {
        $stateForClients = new StateForClients('test-appointment-id', 'test-mercure-topic');
        $this->assertions($stateForClients);
        $stateForClients->setVideoClientToken('test-video-client-token');
        $stateForClients->setHoveredElementId('test-hovered-element-id');
        $this->assertions($stateForClients, 'test-video-client-token', 'test-hovered-element-id');
    }

    private function assertions(
        StateForClients $stateForClients,
        ?string $expectVideoClientToken = null,
        ?string $expectHoveredElementId = null
    ): void
    {
        $payload = $stateForClients->getMercureUpdatePayload();
        static::assertArrayHasKey('state-for-clients', $payload);
        $payloadForClients = $payload['state-for-clients'];
        static::assertArrayHasKey('videoClientToken', $payloadForClients);
        static::assertEquals($expectVideoClientToken, $payloadForClients['videoClientToken']);
        static::assertArrayHasKey('hoveredElementId', $payloadForClients);
        static::assertEquals($expectHoveredElementId, $payloadForClients['hoveredElementId']);
    }
}
