<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresentationState\State;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Tests\Unit\Fake\FakeState;

/**
 * @internal
 */
class AbstractPresentationStateTest extends TestCase
{
    public function testInitializeWithExtensionsHasData(): void
    {
        $state = new FakeState('1', 'mercure-topic');
        static::assertSame('1', $state->getAppointmentId());
        static::assertSame('mercure-topic', $state->getMercureTopic());

        $state->addArrayExtension('test-extension', ['test-key' => 'test-value']);
        $result = $state->getData();

        static::assertCount(2, $result);
        static::assertArrayHasKey('exposeMe', $result);
        static::assertSame('test', $result['exposeMe']);
        static::assertArrayHasKey('extensions', $result);
        static::assertArrayHasKey('test-extension', $result['extensions']);
        static::assertInstanceOf(ArrayStruct::class, $result['extensions']['test-extension']);
        /** @var ArrayStruct<string, mixed> $testExtension */
        $testExtension = $result['extensions']['test-extension'];
        static::assertSame(['test-key' => 'test-value'], $testExtension->all());
        static::assertArrayNotHasKey('dontExposeMe', $result);
    }

    public function testInitializeWithEmptyExtensions(): void
    {
        $state = new FakeState('1', 'mercure-topic');
        static::assertSame('1', $state->getAppointmentId());
        static::assertSame('mercure-topic', $state->getMercureTopic());
        $result = $state->getData();

        static::assertCount(2, $result);
        static::assertArrayHasKey('exposeMe', $result);
        static::assertSame('test', $result['exposeMe']);
        static::assertArrayHasKey('extensions', $result);
        static::assertSame([], $result['extensions']);
        static::assertArrayNotHasKey('dontExposeMe', $result);
    }

    public function testThatChangesAreDetected(): void
    {
        $state = new FakeState('1', 'mercure-topic', ['exposeMe' => 'test']);
        static::assertFalse($state->hasChanges());

        $state->setExposeMe('test-2');
        static::assertTrue($state->hasChanges());

        $state->resetHash();
        static::assertFalse($state->hasChanges());
    }
}
