<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\PresenationState\State;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicPageOpenedPayload;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

/**
 *
 * @internal
 */
class StateForAllTest extends TestCase
{
    private StateForAll $stateForAll;

    protected function setUp(): void
    {
        parent::setUp();
        $this->stateForAll =  new StateForAll('1', 'mercure-topic', []);
    }

    public function testSetCurrentGuideProductId(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['currentGuideProductId']);
        $this->stateForAll->setCurrentGuideProductId('test-current-guide-product-id');
        $data = $this->stateForAll->getData();
        static::assertSame('test-current-guide-product-id', $data['currentGuideProductId']);
        $this->stateForAll->setCurrentGuideProductId(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['currentGuideProductId']);
    }

    public function testSetBroadcastMode(): void
    {
        $data = $this->stateForAll->getData();
        static::assertFalse($data['broadcastMode']);
        $this->stateForAll->setBroadcastMode(true);
        $data = $this->stateForAll->getData();
        static::assertTrue($data['broadcastMode']);
    }

    public function testSetLastActiveGuideSection(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['lastActiveGuideSection']);
        $this->stateForAll->setLastActiveGuideSection('test-last-active-guide-section');
        $data = $this->stateForAll->getData();
        static::assertSame('test-last-active-guide-section', $data['lastActiveGuideSection']);
        $this->stateForAll->setLastActiveGuideSection(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['lastActiveGuideSection']);
    }

    public function testSetCurrentSlide(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['currentPageId']);
        static::assertNull($data['currentSectionId']);
        static::assertEquals(0, $data['currentSlideAlias']);
        static::assertNull($data['lastActiveGuideSection']);

        $this->stateForAll->setLastActiveGuideSection('test-last-active-guide-section');
        $data = $this->stateForAll->getData();
        static::assertSame('test-last-active-guide-section', $data['lastActiveGuideSection']);

        $this->stateForAll->setCurrentSlide('test-current-page-id', 'test-current-section-id', 1);
        $data = $this->stateForAll->getData();
        static::assertSame('test-current-page-id', $data['currentPageId']);
        static::assertSame('test-current-section-id', $data['currentSectionId']);
        static::assertEquals(1, $data['currentSlideAlias']);
        static::assertNull($data['lastActiveGuideSection']);

        $this->stateForAll->setCurrentSlide(null, null, null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['currentPageId']);
        static::assertNull($data['currentSectionId']);
        static::assertNull($data['currentSlideAlias']);
        static::assertEquals(0, $data['currentSlideAlias']);
    }

    public function testGetMercureUpdatePayload(): void
    {
        $data = $this->stateForAll->getData();
        $payload = $this->stateForAll->getMercureUpdatePayload();
        static::assertArrayHasKey('state-for-all', $payload);
        static::assertSame($data, $payload['state-for-all']);
    }

    public function testSetDynamicPage(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['currentDynamicPage']);
        $payload = new DynamicPageOpenedPayload('test-type');
        $this->stateForAll->setDynamicPageOpen($payload);
        $data = $this->stateForAll->getData();
        static::assertSame($payload, $data['currentDynamicPage']);
        $this->stateForAll->setDynamicPageClosed();
        $data = $this->stateForAll->getData();
        static::assertNull($data['currentDynamicPage']);
    }

    public function testCheckStatus(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['startedAt']);
        static::assertFalse($data['started']);
        static::assertFalse($data['running']);
        static::assertFalse($data['ended']);

        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setStartedAt(null);
        $endedAt = new \DateTimeImmutable('2021-01-02');
        $appointment->setEndedAt($endedAt);
        $this->stateForAll->setAppointmentState($appointment);
        $data = $this->stateForAll->getData();
        static::assertNull($data['startedAt']);
        static::assertNull($data['endedAt']);
        static::assertFalse($data['started']);
        static::assertFalse($data['running']);
        static::assertFalse($data['ended']);

        $startedAt = new \DateTimeImmutable('2021-01-01');
        $appointment->setStartedAt($startedAt);
        $this->stateForAll->setAppointmentState($appointment);
        $data = $this->stateForAll->getData();
        static::assertSame($startedAt, $data['startedAt']);
        static::assertSame($endedAt, $data['endedAt']);
        static::assertTrue($data['started']);
        static::assertFalse($data['running']);
        static::assertTrue($data['ended']);
    }

    public function testSetStartedAt(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['startedAt']);
        static::assertFalse($data['started']);
        static::assertFalse($data['running']);
        static::assertFalse($data['ended']);

        $startedAt = new \DateTimeImmutable('2021-01-01');
        $this->stateForAll->setStartedAt($startedAt);
        $data = $this->stateForAll->getData();
        static::assertSame($startedAt, $data['startedAt']);
        static::assertTrue($data['started']);
        static::assertTrue($data['running']);
        static::assertFalse($data['ended']);

        $this->stateForAll->setStartedAt(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['startedAt']);
        static::assertFalse($data['started']);
        static::assertFalse($data['running']);
        static::assertFalse($data['ended']);
    }

    public function testSetEndedAt(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['endedAt']);
        static::assertFalse($data['started']);
        static::assertFalse($data['ended']);
        static::assertFalse($data['running']);

        $endedAt = new \DateTimeImmutable('2021-01-02');
        $this->stateForAll->setEndedAt($endedAt);
        $data = $this->stateForAll->getData();
        static::assertNull($data['endedAt']);
        static::assertFalse($data['started']);
        static::assertFalse($data['ended']);
        static::assertFalse($data['running']);

        $startedAt = new \DateTimeImmutable('2021-01-01');
        $this->stateForAll->setStartedAt($startedAt);
        $data = $this->stateForAll->getData();
        static::assertSame($startedAt, $data['startedAt']);
        static::assertTrue($data['started']);
        static::assertFalse($data['ended']);
        static::assertTrue($data['running']);

        $this->stateForAll->setEndedAt($endedAt);
        $data = $this->stateForAll->getData();
        static::assertSame($startedAt, $data['startedAt']);
        static::assertSame($endedAt, $data['endedAt']);
        static::assertTrue($data['started']);
        static::assertTrue($data['ended']);
        static::assertFalse($data['running']);

        $this->stateForAll->setEndedAt(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['endedAt']);
        static::assertTrue($data['started']);
        static::assertTrue($data['running']);
        static::assertFalse($data['ended']);
    }

    public function testSetAccessibleFrom(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['accessibleFrom']);
        $accessibleFrom = new \DateTimeImmutable('2021-01-01');
        $this->stateForAll->setAccessibleFrom($accessibleFrom);
        $data = $this->stateForAll->getData();
        static::assertSame($accessibleFrom, $data['accessibleFrom']);
        $this->stateForAll->setAccessibleFrom(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['accessibleFrom']);
    }

    public function testSetAccessibleTo(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['accessibleTo']);
        $accessibleTo = new \DateTimeImmutable('2021-01-01');
        $this->stateForAll->setAccessibleTo($accessibleTo);
        $data = $this->stateForAll->getData();
        static::assertSame($accessibleTo, $data['accessibleTo']);
        $this->stateForAll->setAccessibleTo(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['accessibleTo']);
    }

    public function testSetVideoAudioSettings(): void
    {
        $data = $this->stateForAll->getData();
        static::assertSame('none', $data['videoAudioSettings']);
        $this->stateForAll->setVideoAudioSettings('test-video-audio-settings');
        $data = $this->stateForAll->getData();
        static::assertSame('test-video-audio-settings', $data['videoAudioSettings']);
    }

    public function testSetVideoRoomUrl(): void
    {
        $data = $this->stateForAll->getData();
        static::assertEmpty($data['videoRoomUrl']);
        $this->stateForAll->setVideoRoomUrl('test-video-room-url');
        $data = $this->stateForAll->getData();
        static::assertSame('test-video-room-url', $data['videoRoomUrl']);
    }

    public function testSetAppointmentMode(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['appointmentMode']);
        $this->stateForAll->setAppointmentMode('test-appointment-mode');
        $data = $this->stateForAll->getData();
        static::assertSame('test-appointment-mode', $data['appointmentMode']);
    }

    public function testSetAttendeeRestrictionType(): void
    {
        $data = $this->stateForAll->getData();
        static::assertNull($data['attendeeRestrictionType']);
        $this->stateForAll->setAttendeeRestrictionType('test-attendee-restriction-type');
        $data = $this->stateForAll->getData();
        static::assertSame('test-attendee-restriction-type', $data['attendeeRestrictionType']);
        $this->stateForAll->setAttendeeRestrictionType(null);
        $data = $this->stateForAll->getData();
        static::assertNull($data['attendeeRestrictionType']);
    }

    public function testSetProductDetailDefaultPageId(): void
    {
        static::assertNull($this->stateForAll->getProductDetailDefaultPageId());
        $this->stateForAll->setProductDetailDefaultPageId('test-product-detail-default-page-id');
        static::assertSame('test-product-detail-default-page-id', $this->stateForAll->getProductDetailDefaultPageId());
        $this->stateForAll->setProductDetailDefaultPageId(null);
        static::assertNull($this->stateForAll->getProductDetailDefaultPageId());
    }

    public function testSetQuickviewPageId(): void
    {
        static::assertNull($this->stateForAll->getQuickviewPageId());
        $this->stateForAll->setQuickviewPageId('test-quickview-page-id');
        static::assertSame('test-quickview-page-id', $this->stateForAll->getQuickviewPageId());
        $this->stateForAll->setQuickviewPageId(null);
        static::assertNull($this->stateForAll->getQuickviewPageId());
    }

    public function testSetProductListingDefaultPageId(): void
    {
        static::assertNull($this->stateForAll->getProductListingDefaultPageId());
        $this->stateForAll->setProductListingDefaultPageId('test-product-listing-default-page-id');
        static::assertSame('test-product-listing-default-page-id', $this->stateForAll->getProductListingDefaultPageId());
        $this->stateForAll->setProductListingDefaultPageId(null);
        static::assertNull($this->stateForAll->getProductListingDefaultPageId());
    }

    public function testSetAllowUserActionsForGuide(): void
    {
        static::assertFalse($this->stateForAll->getAllowUserActionsForGuide());
        $this->stateForAll->setAllowUserActionsForGuide(true);
        static::assertTrue($this->stateForAll->getAllowUserActionsForGuide());
    }
}
