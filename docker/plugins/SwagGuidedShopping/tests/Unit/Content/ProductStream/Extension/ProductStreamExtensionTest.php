<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\ProductStream\Extension;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\ProductStream\ProductStreamDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\ProductStream\Extension\ProductStreamExtension;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;

class ProductStreamExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $fieldCollection = new FieldCollection();
        $productStreamExtension = new ProductStreamExtension();
        $productStreamExtension->extendFields($fieldCollection);
        // Check the fields count
        $elements = $fieldCollection->getElements();
        static::assertCount(1, $elements);
        /** @var AssociationField $element */
        $element = $elements[0];
        static::assertInstanceOf(OneToManyAssociationField::class, $element);
        static::assertEquals('presentationCmsPages', $element->getPropertyName());
        static::assertEquals(PresentationCmsPageDefinition::class, $element->getReferenceClass());
        static::assertEquals('product_stream_id', $element->getReferenceField());
        static::assertNotNull($element->getFlag(CascadeDelete::class));
    }

    public function testGetDefinitionClass(): void
    {
        $productStreamExtension = new ProductStreamExtension();
        static::assertEquals(ProductStreamDefinition::class, $productStreamExtension->getDefinitionClass());
    }
}
