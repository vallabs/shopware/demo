<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionEntity;

class InteractionEntityTest extends TestCase
{
    public function testGetName(): void
    {
        $interaction = new InteractionEntity();
        $interaction->setName('test-interaction-name');
        static::assertSame('test-interaction-name', $interaction->getName());
    }

    public function testGetExpiresAt(): void
    {
        $interaction = new InteractionEntity();
        $expiresAt = new \DateTimeImmutable();
        $interaction->setExpiresAt($expiresAt);
        static::assertSame($expiresAt, $interaction->getExpiresAt());
    }

    public function testGetTriggeredAt(): void
    {
        $interaction = new InteractionEntity();
        $triggeredAt = new \DateTimeImmutable();
        $interaction->setTriggeredAt($triggeredAt);
        static::assertSame($triggeredAt, $interaction->getTriggeredAt());
    }

    public function testGetPayload(): void
    {
        $interaction = new InteractionEntity();
        static::assertNull($interaction->getPayload());
        $payload = ['productId' => 'test-product-id'];
        $interaction->setPayload($payload);
        static::assertSame($payload, $interaction->getPayload());
    }

    public function testGetAttendeeId(): void
    {
        $interaction = new InteractionEntity();
        $interaction->setAttendeeId('test-attendee-id');
        static::assertSame('test-attendee-id', $interaction->getAttendeeId());
    }

    public function testGetAttendee(): void
    {
        $interaction = new InteractionEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $interaction->setAttendee($attendee);
        static::assertSame($attendee, $interaction->getAttendee());
    }
}
