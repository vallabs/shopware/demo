<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;
use SwagGuidedShopping\Content\Interaction\InteractionEntity;

class InteractionCollectionTest extends TestCase
{
    public function testGetExpectedClass(): void
    {
        $reflectionClass = new InteractionCollection();

        $reflection = new \ReflectionClass($reflectionClass);
        $method = $reflection->getMethod('getExpectedClass');
        $method->setAccessible(true);

        $result = $method->invokeArgs($reflectionClass, []);
        static::assertSame(InteractionEntity::class, $result);
    }
}
