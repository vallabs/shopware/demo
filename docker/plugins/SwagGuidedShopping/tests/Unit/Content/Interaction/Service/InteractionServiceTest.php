<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Aggregation\Metric\MaxAggregation;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\AggregationResultCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\Metric\MaxResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Grouping\FieldGrouping;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionNotAllowedException;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadException;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;
use SwagGuidedShopping\Content\Interaction\InteractionEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandlerCollection;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\GuideHoveredPayloadHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\ProductPayloadHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandlerCollection;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\Validator\Validation;

/**
 * @internal
 * @coversDefaultClass \SwagGuidedShopping\Content\Interaction\Service\InteractionService
 */
class InteractionServiceTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;

    protected Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $this->context = $this->salesChannelContext->getContext();
    }

    public function testHandleWithInteractionIsNotAllowed(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setType(AttendeeDefinition::TYPE_CLIENT);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->setAppointment($appointment);

        $payloadHandlers = $this->createMock(InteractionPayloadHandlerCollection::class);
        $payloadHandlers->expects(static::never())->method('getHandler');

        $validator = $this->createMock(DataValidator::class);
        $validator->expects(static::never())->method('validate');

        $interactionHandlers = $this->createMock(InteractionHandlerCollection::class);
        $interactionHandlers->expects(static::never())->method('handle');

        $service = $this->getService(null, $validator, $interactionHandlers, $payloadHandlers);
        static::expectException(InteractionNotAllowedException::class);
        $service->handle(InteractionName::DYNAMIC_PAGE_OPENED, new \DateTimeImmutable(), 13, $attendee, [], $this->salesChannelContext);
    }

    public function testHandleWithInteractionIsAllowedButInvalidPayload(): void
    {
        $interactionName = 'test-interaction-name';
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setType(AttendeeDefinition::TYPE_CLIENT);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->setAppointment($appointment);

        $payloadHandlers = $this->createMock(InteractionPayloadHandlerCollection::class);
        $productPayloadHandler = new ProductPayloadHandler();
        $payloadHandlers->expects(static::once())
            ->method('getHandler')
            ->with(static::equalTo($interactionName))
            ->willReturn($productPayloadHandler);

        $validator = new DataValidator(Validation::createValidator());

        $interactionHandlers = $this->createMock(InteractionHandlerCollection::class);
        $interactionHandlers->expects(static::never())->method('handle');

        $interactionRepository = $this->createMock(EntityRepository::class);

        $service = new InteractionService($interactionRepository, $validator, $interactionHandlers, $payloadHandlers);
        static::expectException(InteractionPayloadException::class);
        $service->handle($interactionName, new \DateTimeImmutable(), 13, $attendee, [], $this->salesChannelContext);
    }

    public function testHandleSuccessfully(): void
    {
        $interactionName = 'test-interaction-name';
        $triggeredAt = new \DateTimeImmutable();
        $liveTime = 13;
        $payload = ['productId' => 'test-product-id'];
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setType(AttendeeDefinition::TYPE_CLIENT);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->setAppointment($appointment);

        $payloadHandlers = $this->createMock(InteractionPayloadHandlerCollection::class);
        $productPayloadHandler = new ProductPayloadHandler();
        $payloadHandlers->expects(static::once())
            ->method('getHandler')
            ->with(static::equalTo($interactionName))
            ->willReturn($productPayloadHandler);

        $validator = new DataValidator(Validation::createValidator());

        $interactionHandlers = $this->createMock(InteractionHandlerCollection::class);
        $interaction = new Interaction($interactionName, $triggeredAt, $liveTime, $attendee, $productPayloadHandler->createPayload($payload));
        $interactionHandlers->expects(static::once())
            ->method('handle')
            ->with(static::equalTo($interaction));

        $interactionRepository = $this->createMock(EntityRepository::class);

        $service = new InteractionService($interactionRepository, $validator, $interactionHandlers, $payloadHandlers);
        $service->handle($interactionName, $triggeredAt, $liveTime, $attendee, $payload, $this->salesChannelContext);
    }

    /**
     * @dataProvider getTestInteractionIsAllowedProviderData
     */
    public function testInteractionIsAllowed(
        string $interactionName,
        string $attendeeType,
        string $appointmentMode,
        bool $expectIsAllowed
    ): void
    {
        $service = $this->getService();
        $isAllowed = $service->isInteractionAllowed($interactionName, $attendeeType, $appointmentMode);
        static::assertEquals($expectIsAllowed, $isAllowed);
    }

    public static function getTestInteractionIsAllowedProviderData(): \Generator
    {
        yield 'client-join-self-mode' => [
            'test-interaction-name',
            AttendeeDefinition::TYPE_CLIENT,
            AppointmentDefinition::MODE_SELF,
            true
        ];
        yield 'guide-join-self-mode' => [
            'test-interaction-name',
            AttendeeDefinition::TYPE_GUIDE,
            AppointmentDefinition::MODE_SELF,
            true
        ];
        yield 'client-join-guided-mode-use-allowed-interaction' => [
            'test-interaction-name',
            AttendeeDefinition::TYPE_CLIENT,
            AppointmentDefinition::MODE_GUIDED,
            true
        ];
        yield 'client-join-guided-mode-use-disallowed-interaction' => [
            InteractionName::DYNAMIC_PAGE_OPENED,
            AttendeeDefinition::TYPE_CLIENT,
            AppointmentDefinition::MODE_GUIDED,
            false
        ];
        yield 'guide-join-guided-mode-use-allowed-interaction' => [
            'test-interaction-name',
            AttendeeDefinition::TYPE_GUIDE,
            AppointmentDefinition::MODE_GUIDED,
            true
        ];
    }

    public function testCleanupInteractionsButCanNotFindAnyExpiredInteractions(): void
    {
        $interactionRepository = $this->createMock(EntityRepository::class);
        $criteria = $this->getCleanupInteractionsCriteria();
        $interactionRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::equalTo($criteria),
                static::equalTo($this->context)
            )
            ->willReturn(new IdSearchResult(0, [], $criteria, $this->context));

        $interactionRepository->expects(static::never())->method('delete');

        $service = $this->getService($interactionRepository);
        $deleteCount = $service->cleanupInteractions($this->context);
        static::assertEquals(0, $deleteCount);
    }

    public function testCleanupInteractionsSuccessfullyWhenFindExpiredInteractions(): void
    {
        $interactionRepository = $this->createMock(EntityRepository::class);
        $criteria = $this->getCleanupInteractionsCriteria();
        $interactionRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::equalTo($criteria),
                static::equalTo($this->context)
            )
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['test-interaction-id'], 'primaryKey' => 'test-interaction-id']],
                $criteria,
                $this->context
            ));

        $interactionRepository->expects(static::once())
            ->method('delete')
            ->with(
                static::equalTo([['id' => 'test-interaction-id']]),
                static::equalTo($this->context)
            );

        $service = $this->getService($interactionRepository);
        $deleteCount = $service->cleanupInteractions($this->context);
        static::assertEquals(1, $deleteCount);
    }

    public function testGetLastInteractionDateTimeButCanNotFindLastInteraction(): void
    {
        $appointmentId = 'test-appointment-id';

        $interactionRepository = $this->createMock(EntityRepository::class);
        $criteria = $this->getLastInteractionCriteria($appointmentId);
        $interactionRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($this->context)
            )
            ->willReturn(new EntitySearchResult(
                InteractionEntity::class,
                0,
                new InteractionCollection(),
                null,
                $criteria,
                $this->context
            ));

        $service = $this->getService($interactionRepository);
        $interactionDateTime = $service->getLastInteractionDateTime($appointmentId, $this->context);
        static::assertNull($interactionDateTime);
    }

    public function testGetLastInteractionDateTimeSuccessfullyWhenFindLastInteraction(): void
    {
        $appointmentId = 'test-appointment-id';

        $interactionRepository = $this->createMock(EntityRepository::class);
        $criteria = $this->getLastInteractionCriteria($appointmentId);
        $triggeredAt = new \DateTimeImmutable();
        $expectTriggeredAt = $triggeredAt->format('Y-m-d H:i:s');
        $maxAggregation = new MaxResult('lastInteractionTriggered', $expectTriggeredAt);
        $interactionRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($this->context)
            )
            ->willReturn(new EntitySearchResult(
                InteractionEntity::class,
                1,
                new InteractionCollection(),
                new AggregationResultCollection([$maxAggregation]),
                $criteria,
                $this->context
            ));

        $service = $this->getService($interactionRepository);
        $interactionDateTime = $service->getLastInteractionDateTime($appointmentId, $this->context);
        static::assertEquals(new \DateTime($expectTriggeredAt), $interactionDateTime);
    }

    public function testGetInteractionsWithOnlyInteractionNamesParameterButCanNotFindAnyInteractions(): void
    {
        $entityRepository = $this->createMock(EntityRepository::class);
        $interactionNames = ['test-interaction-name'];
        $expectCriteria = new Criteria();
        $expectCriteria->addFilter(new EqualsAnyFilter('guided_shopping_interaction.name', $interactionNames));
        $expectCriteria->addSorting(new FieldSorting('triggeredAt', FieldSorting::DESCENDING));
        $entityRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(new EntitySearchResult(
                InteractionEntity::class,
                0,
                new InteractionCollection(),
                null,
                $expectCriteria,
                $this->context
            ));

        $payloadHandlers = $this->createMock(InteractionPayloadHandlerCollection::class);
        $payloadHandlers->expects(static::never())->method('getHandler');

        $service = $this->getService($entityRepository, null, null, $payloadHandlers);
        $interactions = $service->getInteractions($interactionNames, $this->context, null);
        static::assertCount(0, $interactions);
    }

    public function testGetInteractionsWithAllPossibleParametersAndCanNotFindInteractions(): void
    {
        $entityRepository = $this->createMock(EntityRepository::class);
        $interactionNames = ['test-interaction-name', 'test-interaction-name-2'];
        $attendeeId = 'test-attendee-id';
        $appointmentId = 'test-appointment-id';
        $sorting = [new FieldSorting('test-sorting-field')];
        $groupFields = ['test-group-field'];
        $attendeeType = 'test-attendee-type';
        $expectCriteria = new Criteria();
        $nameFilter = new EqualsAnyFilter('guided_shopping_interaction.name', $interactionNames);
        $expectCriteria->addFilter(
                new MultiFilter(
                    MultiFilter::CONNECTION_AND, [
                        $nameFilter,
                        new EqualsFilter('attendeeId', $attendeeId),
                    ]
                )
            );
        $expectCriteria->addFilter(
            new MultiFilter(
                MultiFilter::CONNECTION_AND, [
                    $nameFilter,
                    new EqualsFilter('attendee.appointmentId', $appointmentId),
                ]
            )
        );
        $expectCriteria->addFilter(new EqualsFilter('attendee.type', $attendeeType));
        $expectCriteria->addGroupField(new FieldGrouping($groupFields[0]));
        $expectCriteria->resetSorting();
        $expectCriteria->addSorting($sorting[0]);
        // interaction
        $interaction = new InteractionEntity();
        $interaction->setId('test-interaction-id');
        $interaction->setName('test-interaction-name');
        $interaction->setTriggeredAt(new \DateTimeImmutable());
        $interaction->setPayload(['productId' => 'test-product-id']);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setType($attendeeType);
        $interaction->setAttendee($attendee);
        // interaction 2
        $interaction2 = new InteractionEntity();
        $interaction2->setId('test-interaction-id-2');
        $interaction2->setName('test-interaction-name-2');
        $interaction2->setTriggeredAt(new \DateTimeImmutable());
        $interaction2->setPayload(null);
        $interaction2->setAttendee($attendee);
        $entityRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(new EntitySearchResult(
                InteractionEntity::class,
                1,
                new InteractionCollection([$interaction, $interaction2]),
                null,
                $expectCriteria,
                $this->context
            ));

        $payloadHandlers = $this->createMock(InteractionPayloadHandlerCollection::class);
        $payloadHandlers->expects(static::exactly(2))
            ->method('getHandler')
            ->willReturnOnConsecutiveCalls(new ProductPayloadHandler(), new GuideHoveredPayloadHandler());

        $validator = new DataValidator(Validation::createValidator());
        $interactionHandlersMock = $this->createMock(InteractionHandlerCollection::class);

        $service = new InteractionService($entityRepository, $validator, $interactionHandlersMock, $payloadHandlers);
        $interactions = $service->getInteractions($interactionNames, $this->context, $attendeeId, $appointmentId, $sorting, $groupFields, $attendeeType);
        static::assertCount(1, $interactions);
        $expectInteraction = new Interaction(
            $interaction->getName(),
            $interaction->getTriggeredAt(),
            -1,
            $interaction->getAttendee(),
            new ProductPayload('test-product-id')
        );
        static::assertEquals($expectInteraction ,$interactions[0]);
    }

    private function getService(
        ?MockObject $interactionRepositoryMock = null,
        ?MockObject $validatorMock = null,
        ?MockObject $interactionHandlersMock = null,
        ?MockObject $payloadHandlersMock = null
    ): InteractionService
    {
        /** @var MockObject&EntityRepository<InteractionCollection> $interactionRepository */
        $interactionRepository = $interactionRepositoryMock ?? $this->createMock(EntityRepository::class);
        /** @var MockObject&DataValidator $validator */
        $validator = $validatorMock ?? $this->createMock(DataValidator::class);
        /** @var MockObject&InteractionHandlerCollection $interactionHandlers */
        $interactionHandlers = $interactionHandlersMock ?? $this->createMock(InteractionHandlerCollection::class);
        /** @var MockObject&InteractionPayloadHandlerCollection $payloadHandlers */
        $payloadHandlers = $payloadHandlersMock ?? $this->createMock(InteractionPayloadHandlerCollection::class);

        return new InteractionService($interactionRepository, $validator, $interactionHandlers, $payloadHandlers);
    }

    private function getCleanupInteractionsCriteria(): Criteria
    {
        $criteria = new Criteria();
        $criteria->addFilter(new RangeFilter('expiresAt', [RangeFilter::LTE => (new \DateTime())->format('Y-m-d H:i:s')]));
        $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('expiresAt', null)]));

        return $criteria;
    }

    private function getLastInteractionCriteria(string $appointmentId): Criteria
    {
        $criteria = new Criteria();
        $criteria->addAggregation(new MaxAggregation('lastInteractionTriggered', 'triggeredAt'));
        $criteria->addFilter(new EqualsFilter('attendee.appointmentId', $appointmentId));
        $criteria->setLimit(1);

        return $criteria;
    }
}
