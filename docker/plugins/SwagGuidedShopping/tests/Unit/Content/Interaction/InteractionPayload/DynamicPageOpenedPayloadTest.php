<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicPageOpenedPayload;

class DynamicPageOpenedPayloadTest extends TestCase
{
    public function testGetType(): void
    {
        $payload = new DynamicPageOpenedPayload('test-type');
        static::assertSame('test-type', $payload->getType());
    }
}
