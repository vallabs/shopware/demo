<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicProductPageOpenedPayload;

class DynamicProductPageOpenedPayloadTest extends TestCase
{
    public function testInitialize(): void
    {
        $payload = new DynamicProductPageOpenedPayload('test-type', 'test-product-id');
        static::assertSame('test-type', $payload->getType());
        static::assertSame('test-product-id', $payload->getProductId());
    }
}
