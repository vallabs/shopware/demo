<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\GuideHoveredPayload;

class GuideHoveredPayloadTest extends TestCase
{
    public function testInitializeWithoutHoveredElementId(): void
    {
        $payload = new GuideHoveredPayload();
        static::assertNull($payload->getHoveredElementId());
    }

    public function testInitializeWithHoveredElementId(): void
    {
        $payload = new GuideHoveredPayload('test-hovered-element-id');
        static::assertEquals('test-hovered-element-id', $payload->getHoveredElementId());
    }
}
