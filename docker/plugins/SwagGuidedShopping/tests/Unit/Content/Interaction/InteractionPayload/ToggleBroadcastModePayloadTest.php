<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ToggleBroadcastModePayload;

class ToggleBroadcastModePayloadTest extends TestCase
{
    /**
     * @dataProvider getTestInitialize
     */
    public function testInitialize(
        bool $isActive
    ): void
    {
        $payload = new ToggleBroadcastModePayload($isActive);
        static::assertSame($isActive, $payload->getActive());
    }

    public static function getTestInitialize(): \Generator
    {
        yield 'active' => [true];
        yield 'inactive' => [false];
    }
}
