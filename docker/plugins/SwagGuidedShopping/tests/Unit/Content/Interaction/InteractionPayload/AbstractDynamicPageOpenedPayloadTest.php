<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractDynamicPageOpenedPayload;

class AbstractDynamicPageOpenedPayloadTest extends TestCase
{
    protected AbstractDynamicPageOpenedPayload $newAnonymousClassFromAbstract;

    public function setUp(): void
    {
        $this->newAnonymousClassFromAbstract = new class extends AbstractDynamicPageOpenedPayload {
            public function __construct(
                protected string $type = 'test-type'
            ) {
            }
        };
    }

    public function testAbstractClassInternalMethod(): void
    {
        static::assertSame('test-type', $this->newAnonymousClassFromAbstract->getType());
        static::assertTrue($this->newAnonymousClassFromAbstract->isOpened());
    }
}
