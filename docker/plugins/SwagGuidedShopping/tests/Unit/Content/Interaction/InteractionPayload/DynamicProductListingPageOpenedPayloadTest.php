<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicProductListingPageOpenedPayload;

class DynamicProductListingPageOpenedPayloadTest extends TestCase
{
    public function testInitialize(): void
    {
        $payload = new DynamicProductListingPageOpenedPayload('test-type', 1);
        static::assertSame('test-type', $payload->getType());
        static::assertSame(1, $payload->getPage());
    }
}
