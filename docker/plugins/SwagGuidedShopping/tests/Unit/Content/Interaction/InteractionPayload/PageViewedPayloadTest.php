<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayload;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\PageViewedPayload;

class PageViewedPayloadTest extends TestCase
{
    public function testInitialize(): void
    {
        $payload = new PageViewedPayload('test-page-id', 'test-section-id', 3);
        static::assertSame('test-page-id', $payload->getPageId());
        static::assertSame('test-section-id', $payload->getSectionId());
        static::assertSame(3, $payload->getSlideAlias());
    }
}
