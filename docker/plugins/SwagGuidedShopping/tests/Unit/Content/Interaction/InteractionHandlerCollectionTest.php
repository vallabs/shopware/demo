<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\AbstractInteractionHandler;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandlerCollection;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class InteractionHandlerCollectionTest extends TestCase
{
    private SalesChannelContext $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testHandleWithUnsupportedInteraction(): void
    {
        $handler = $this->createMock(AbstractInteractionHandler::class);
        $handler->expects(static::once())
            ->method('supports')
            ->willReturn(false);
        $handler->expects(static::never())->method('handle');

        $collection = new InteractionHandlerCollection([
            $handler
        ]);

        $interaction = $this->createMock(Interaction::class);
        $collection->handle($interaction, $this->context);
    }

    public function testHandleWithSupportedInteraction(): void
    {
        $handler = $this->createMock(AbstractInteractionHandler::class);
        $handler->expects(static::once())
            ->method('supports')
            ->willReturn(true);
        $handler->expects(static::once())->method('handle');

        $collection = new InteractionHandlerCollection([
            $handler
        ]);

        $interaction = $this->createMock(Interaction::class);
        $collection->handle($interaction, $this->context);
    }
}
