<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\KeepAliveHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class KeepAliveHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestSupportsProviderData
     */
    public function testSupports(
        Interaction $interaction,
        bool $isSupport
    ): void
    {
        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('update');

        $handler = new KeepAliveHandler($attendeeRepository);
        static::assertEquals($isSupport, $handler->supports($interaction));
    }

    public static function getTestSupportsProviderData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        yield 'supported interaction' => [
            new Interaction(
                'keep.alive',
                new \DateTimeImmutable(),
                -1,
                $attendee,
                new EmptyPayload()
            ),
            true
        ];

        yield 'unsupported interaction' => [
            new Interaction(
                'is.not.keep.alive',
                new \DateTimeImmutable(),
                -1,
                $attendee,
                new EmptyPayload()
            ),
            false
        ];
    }

    public function testHandle(): void
    {
        $context = (new SalesChannelContextHelper())->createSalesChannelContext();

        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        $interaction = new Interaction(
            'keep.alive',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new EmptyPayload()
        );

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('update')
            ->with(
                static::callback(function (array $updateData) {
                    return \count($updateData) === 1
                        && $updateData[0]['id'] === 'test-attendee-id'
                        && \DateTimeImmutable::createFromFormat(Defaults::STORAGE_DATE_TIME_FORMAT, $updateData[0]['lastActive']) !== false;
                }),
                static::equalTo($context->getContext())
            );

        $handler = new KeepAliveHandler($attendeeRepository);
        $handler->handle($interaction, $context);
    }
}
