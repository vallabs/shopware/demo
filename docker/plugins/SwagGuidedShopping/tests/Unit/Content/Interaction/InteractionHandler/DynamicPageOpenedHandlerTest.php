<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\DynamicPageOpenedHandler;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicPageOpenedPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class DynamicPageOpenedHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestSupportsProviderData
     */
    public function testSupports(Interaction $interaction, bool $expectValue): void
    {
        $handler = $this->getHandler();
        static::assertEquals($expectValue, $handler->supports($interaction));
    }

    public static function getTestSupportsProviderData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');

        $dynamicPageOpenedInteraction = new Interaction(
            'dynamicPage.opened',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new DynamicPageOpenedPayload('test-type')
        );
        yield 'dynamic page opened interaction' => [$dynamicPageOpenedInteraction, true];

        $dynamicProductPageOpenedInteraction = new Interaction(
            'dynamicProductPage.opened',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new DynamicPageOpenedPayload('test-type')
        );
        yield 'dynamic product page opened interaction' => [$dynamicProductPageOpenedInteraction, true];

        $dynamicProductListingPageOpenedInteraction = new Interaction(
            'dynamicProductListingPage.opened',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new DynamicPageOpenedPayload('test-type')
        );
        yield 'dynamic product listing page opened interaction' => [$dynamicProductListingPageOpenedInteraction, true];

        $dynamicPageClosedInteraction= new Interaction(
            'dynamicPage.closed',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new EmptyPayload()
        );
        yield 'dynamic page closed interaction' => [$dynamicPageClosedInteraction, false];
    }

    public function testHandle(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');
        $payload = new DynamicPageOpenedPayload('test-type');
        $interaction = new Interaction(
            'dynamicPage.opened',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            $payload
        );

        $factory = $this->createMock(PresentationStateServiceFactory::class);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateForAll = new StateForAll('test-appointment-id', 'test-mecure-topic');
        $stateService->expects(static::once())
            ->method('getStateForAll')
            ->willReturn($stateForAll);
        $stateService->expects(static::once())->method('publishStateForAll');
        $factory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo('test-appointment-id'),
                static::equalTo($salesChannelContext->getContext())
            )
            ->willReturn($stateService);

        $handler = $this->getHandler($factory);
        $handler->handle($interaction, $salesChannelContext);
        $stateData = $stateForAll->getData();
        static::assertArrayHasKey('currentDynamicPage', $stateData);
        static::assertSame($payload, $stateData['currentDynamicPage']);
    }

    private function getHandler(
        ?MockObject $factoryMock = null
    ): DynamicPageOpenedHandler
    {
        /** @var MockObject&PresentationStateServiceFactory $factory */
        $factory = $factoryMock ?: $this->createMock(PresentationStateServiceFactory::class);
        return new DynamicPageOpenedHandler($factory);
    }
}
