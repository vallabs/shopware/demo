<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\QuickViewHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class QuickViewHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestSupportsProviderData
     */
    public function testSupports(
        Interaction $interaction, bool $expectResult
    ): void
    {
        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $handler = new QuickViewHandler($presentationStateServiceFactory);
        $result = $handler->supports($interaction);
        static::assertSame($expectResult, $result);
    }

    public static function getTestSupportsProviderData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');

        $invalidInteraction = new Interaction(
            'test-invalid-interaction',
            new \DateTimeImmutable(),
            1,
            $attendee,
            new EmptyPayload()
        );
        yield 'invalid-interaction' => [$invalidInteraction, false];

        $validInteraction = new Interaction(
            InteractionName::QUICKVIEW_OPENED,
            new \DateTimeImmutable(),
            1,
            $attendee,
            new EmptyPayload()
        );
        yield 'valid-interaction' => [$validInteraction, true];

        $validInteraction2 = new Interaction(
            InteractionName::QUICKVIEW_CLOSED,
            new \DateTimeImmutable(),
            1,
            $attendee,
            new EmptyPayload()
        );
        yield 'valid-interaction2' => [$validInteraction2, true];
    }

    public function testHandleWhenOpeningQuickView(): void
    {
        $context = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        $interaction = new Interaction(
            InteractionName::QUICKVIEW_OPENED,
            new \DateTimeImmutable(),
            1,
            $attendee,
            new ProductPayload('test-product-id')
        );

        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $presentationStateService = $this->createMock(PresentationStateService::class);
        $presentationStateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $presentationStateService->expects(static::once())
            ->method('publishStateForGuides');

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo('test-appointment-id'),
                static::equalTo($context->getContext())
            )
            ->willReturn($presentationStateService);


        $handler = new QuickViewHandler($presentationStateServiceFactory);
        $handler->handle($interaction, $context);

        $data = $stateForGuides->getData();
        static::assertCount(1, $data['quickViewState']);
        static::assertArrayHasKey('test-product-id', $data['quickViewState']);
        static::assertCount(1, $data['quickViewState']['test-product-id']);
        static::assertArrayHasKey('test-attendee-id', $data['quickViewState']['test-product-id']);
    }

    public function testHandleWhenClosingQuickView(): void
    {
        $context = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        $interaction = new Interaction(
            InteractionName::QUICKVIEW_CLOSED,
            new \DateTimeImmutable(),
            1,
            $attendee,
            new ProductPayload('test-product-id')
        );

        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $presentationStateService = $this->createMock(PresentationStateService::class);
        $presentationStateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $presentationStateService->expects(static::once())
            ->method('publishStateForGuides');

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo('test-appointment-id'),
                static::equalTo($context->getContext())
            )
            ->willReturn($presentationStateService);


        $handler = new QuickViewHandler($presentationStateServiceFactory);
        $handler->handle($interaction, $context);

        $data = $stateForGuides->getData();
        static::assertCount(1, $data['quickViewState']);
        static::assertArrayHasKey('test-product-id', $data['quickViewState']);
        static::assertCount(0, $data['quickViewState']['test-product-id']);
        static::assertArrayNotHasKey('test-attendee-id', $data['quickViewState']['test-product-id']);
    }
}
