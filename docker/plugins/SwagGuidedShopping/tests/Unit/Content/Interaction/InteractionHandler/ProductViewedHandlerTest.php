<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\ProductViewedHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class ProductViewedHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestSupportsProviderData
     */
    public function testSupports(
        Interaction $interaction, bool $expectResult
    ): void
    {
        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $handler = new ProductViewedHandler($presentationStateServiceFactory);
        $result = $handler->supports($interaction);
        static::assertSame($expectResult, $result);
    }

    public static function getTestSupportsProviderData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $invalidInteraction = new Interaction(
            'test-invalid-interaction',
            new \DateTimeImmutable(),
            1,
            $attendee,
            new EmptyPayload()
        );

        yield 'invalid-interaction' => [$invalidInteraction, false];

        $validInteraction = new Interaction(
            InteractionName::PRODUCT_VIEWED,
            new \DateTimeImmutable(),
            1,
            $attendee,
            new EmptyPayload()
        );

        yield 'valid-interaction' => [$validInteraction, true];
    }

    public function testHandle(): void
    {
        $context = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        $interaction = new Interaction(
            InteractionName::PRODUCT_VIEWED,
            new \DateTimeImmutable(),
            1,
            $attendee,
            new ProductPayload('test-product-id')
        );

        $stateForAll = new StateForAll('test-appointment-id', 'test-mercure-topic');
        $stateForAll->setCurrentGuideProductId('test-init-product-id');
        $presentationStateService = $this->createMock(PresentationStateService::class);
        $presentationStateService->expects(static::once())
            ->method('getStateForAll')
            ->willReturn($stateForAll);
        $presentationStateService->expects(static::once())
            ->method('publishStateForAll');

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo('test-appointment-id'),
                static::equalTo($context->getContext())
            )
            ->willReturn($presentationStateService);


        $handler = new ProductViewedHandler($presentationStateServiceFactory);
        $handler->handle($interaction, $context);

        $data = $stateForAll->getData();
        static::assertSame('test-product-id', $data['currentGuideProductId']);
    }
}
