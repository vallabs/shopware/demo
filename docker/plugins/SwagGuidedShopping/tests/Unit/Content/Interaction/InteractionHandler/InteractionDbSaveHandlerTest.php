<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;
use SwagGuidedShopping\Content\Interaction\InteractionEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\InteractionDbSaveHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Tests\Unit\Fake\FakeRepository;

/**
 * @internal
 */
class InteractionDbSaveHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestHandleWithSupportedInteractionProviderData
     */
    public function testHandleWithSupportedInteraction(
        int $lifeTimeInSeconds
    ): void
    {
        $searchResult = new EntitySearchResult(
            InteractionEntity::class,
            0,
            new EntityCollection(),
            null,
            new Criteria(),
            new Context(new SystemSource())
        );
        /** @var EntityRepository<InteractionCollection>&FakeRepository $interactionRepository */
        $interactionRepository = new FakeRepository($searchResult);
        $handler = new InteractionDbSaveHandler($interactionRepository);

        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $interaction = new Interaction(
            'test-interaction',
            new \DateTimeImmutable(),
            $lifeTimeInSeconds,
            $attendee,
            new ProductPayload('test-product-id')
        );

        static::assertTrue($handler->supports($interaction));
        $handler->handle($interaction, $this->createMock(SalesChannelContext::class));
        static::assertCount(1, $interactionRepository->writtenData);
        $writtenData = $interactionRepository->writtenData[0];
        static::assertEquals($interaction->getName(), $writtenData['name']);
        static::assertEquals($interaction->getAttendeeId(), $writtenData['attendeeId']);
        static::assertEquals($interaction->getTriggeredAt(), $writtenData['triggeredAt']);
        static::assertEquals($interaction->getExpiresAt(), $writtenData['expiresAt']);
        static::assertEquals($interaction->getPayloadAsArray(), $writtenData['payload']);
    }

    public static function getTestHandleWithSupportedInteractionProviderData(): \Generator
    {
        yield [-1];
        yield [3];
    }

    /**
     * @dataProvider getTestHandleWithUnsupportedInteractionProviderData
     */
    public function testHandleWithUnsupportedInteraction(
        Interaction $interaction
    ): void
    {
        $searchResult = new EntitySearchResult(
            InteractionEntity::class,
            0,
            new EntityCollection(),
            null,
            new Criteria(),
            new Context(new SystemSource())
        );
        /** @var EntityRepository<InteractionCollection>&FakeRepository $interactionRepository */
        $interactionRepository = new FakeRepository($searchResult);
        $handler = new InteractionDbSaveHandler($interactionRepository);

        static::assertFalse($handler->supports($interaction));
    }

    public static function getTestHandleWithUnsupportedInteractionProviderData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        yield 'unsupported interaction' => [
            new Interaction(
                'keep.alive',
                new \DateTimeImmutable(),
                0,
                $attendee,
                new EmptyPayload()
            )
        ];

        yield 'interaction with unsupported life time' => [
            new Interaction(
                'test.interaction',
                new \DateTimeImmutable(),
                -2,
                $attendee,
                new EmptyPayload()
            )
        ];
    }
}
