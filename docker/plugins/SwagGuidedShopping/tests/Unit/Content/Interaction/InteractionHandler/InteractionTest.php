<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;

class InteractionTest extends TestCase
{
    public function testInitialize(): void
    {
        $payload = new ProductPayload('test-product-id');
        $triggeredAt = new \DateTimeImmutable();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        $interaction = new Interaction(
            'test-interaction-name',
            $triggeredAt,
            -1,
            $attendee,
            $payload
        );

        static::assertEquals('test-interaction-name', $interaction->getName());
        static::assertEquals('test-appointment-id', $interaction->getAppointmentId());
        static::assertEquals($triggeredAt, $interaction->getTriggeredAt());
        static::assertSame($attendee, $interaction->getAttendee());
        static::assertEquals('test-attendee-id', $interaction->getAttendeeId());
        static::assertSame($payload, $interaction->getPayload());
        static::assertArrayNotHasKey('extensions', $interaction->getPayloadAsArray());
    }

    /**
     * @dataProvider getTestInitializeWithDifferentLifeTimeInSecondsProviderData
     */
    public function testInitializeWithDifferentLifeTimeInSeconds(
        \DateTimeImmutable $triggeredAt,
        int $lifeTimeInSeconds,
        ?\DateTimeImmutable $expectExpiresAt
    ): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');

        $interaction = new Interaction(
            'test-interaction-name',
            $triggeredAt,
            $lifeTimeInSeconds,
            $attendee,
            new EmptyPayload()
        );

        static::assertEquals($lifeTimeInSeconds, $interaction->getLifeTimeInSeconds());
        static::assertEquals($expectExpiresAt, $interaction->getExpiresAt());
    }

    public static function getTestInitializeWithDifferentLifeTimeInSecondsProviderData(): \Generator
    {
        $triggeredAt = new \DateTimeImmutable();
        yield '3-seconds-life-time' => [
            $triggeredAt,
            3,
            $triggeredAt->add(new \DateInterval('PT3S'))
        ];

        yield '-1-seconds-life-time' => [
            $triggeredAt,
            -1,
            null
        ];
    }
}
