<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionHandler;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\GuideHoveredHandler;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\GuideHoveredPayload;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class GuideHoveredHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestSupportsProviderData
     */
    public function testSupports(Interaction $interaction, bool $expectValue): void
    {
        $handler = $this->getHandler();
        static::assertEquals($expectValue, $handler->supports($interaction));
    }

    public static function getTestSupportsProviderData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');

        $guideHoveredInteraction = new Interaction(
            'guide.hovered',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new GuideHoveredPayload('test-hovered-element-id')
        );
        yield 'guided hovered interaction' => [$guideHoveredInteraction, true];

        $clientHoveredInteraction = new Interaction(
            'client.hovered',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new EmptyPayload()
        );
        yield 'client hovered interaction' => [$clientHoveredInteraction, false];
    }

    public function testHandle(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId('test-appointment-id');
        $interaction = new Interaction(
            'guide.hovered',
            new \DateTimeImmutable(),
            -1,
            $attendee,
            new GuideHoveredPayload('test-hovered-element-id')
        );

        $factory = $this->createMock(PresentationStateServiceFactory::class);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateForClients = new StateForClients('test-appointment-id', 'test-mecure-topic');
        $stateService->expects(static::once())
            ->method('getStateForClients')
            ->willReturn($stateForClients);
        $stateService->expects(static::once())->method('publishStateForClients');
        $factory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo('test-appointment-id'),
                static::equalTo($salesChannelContext->getContext())
            )
            ->willReturn($stateService);

        $handler = $this->getHandler($factory);
        $handler->handle($interaction, $salesChannelContext);
        $stateData = $stateForClients->getData();
        static::assertArrayHasKey('hoveredElementId', $stateData);
        static::assertEquals('test-hovered-element-id', $stateData['hoveredElementId']);
    }

    private function getHandler(
        ?MockObject $factoryMock = null
    ): GuideHoveredHandler
    {
        /** @var MockObject&PresentationStateServiceFactory $factory */
        $factory = $factoryMock ?: $this->createMock(PresentationStateServiceFactory::class);
        return new GuideHoveredHandler($factory);
    }
}
