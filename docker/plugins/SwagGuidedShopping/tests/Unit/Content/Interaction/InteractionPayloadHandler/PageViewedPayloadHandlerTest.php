<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\PageViewedPayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class PageViewedPayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new PageViewedPayloadHandler();
        static::assertSame([InteractionName::PAGE_VIEWED], $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $payloadHandler = new PageViewedPayloadHandler();
        $validationConstraint = $payloadHandler->getPayloadValidationConstraint();
        $expectValidationConstraint = new Collection(
            [
                'pageId' => [new Type('string'), new NotBlank()],
                'sectionId' => [new Type('string'), new NotBlank()],
                'slideAlias' => [new Type('int'), new NotBlank()],
            ]
        );
        static::assertEquals($expectValidationConstraint, $validationConstraint);
    }

    /**
     * @dataProvider getTestCreatePayloadWithInvalidPayloadProviderData
     *
     * @param array<string, mixed> $payload
     * @param class-string<\Throwable> $exception
     */
    public function testCreatePayloadWithInvalidPayload(
        array $payload,
        string $exception
    ): void
    {
        $payloadHandler = new PageViewedPayloadHandler();
        static::expectException($exception);
        $payloadHandler->createPayload($payload);
    }

    public static function getTestCreatePayloadWithInvalidPayloadProviderData(): \Generator
    {
        yield [['pageId' => 'test-page-id'], \Exception::class];
        yield [['sectionId' => 'test-section-id'], \Exception::class];
        yield [['slideAlias' => 1], \Exception::class];
        yield [['pageId' => 'test-page-id', 'sectionId' => 'test-section-id'], \Exception::class];
        yield [['pageId' => 'test-page-id', 'slideAlias' => 1], \Exception::class];
        yield [['sectionId' => 'test-section-id', 'slideAlias' => 1], \Exception::class];
        yield [['pageId' => 2, 'sectionId' => 'test-section-id', 'slideAlias' => 1], \TypeError::class];
        yield [['pageId' => 'test-page-id', 'sectionId' => 2, 'slideAlias' => 1], \TypeError::class];
        yield [['pageId' => 'test-page-id', 'sectionId' => 'test-section-id', 'slideAlias' => 'test-slide-alias'], \TypeError::class];
    }

    public function testCreatePayloadWithValidPayload(): void
    {
        $payloadHandler = new PageViewedPayloadHandler();
        $payload = $payloadHandler->createPayload([
            'pageId' => 'test-page-id',
            'sectionId' => 'test-section-id',
            'slideAlias' => 3
        ]);
        static::assertEquals('test-page-id', $payload->getPageId());
        static::assertEquals('test-section-id', $payload->getSectionId());
        static::assertEquals(3, $payload->getSlideAlias());
    }
}
