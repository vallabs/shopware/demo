<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\ToggleBroadcastModePayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;

class ToggleBroadcastModePayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new ToggleBroadcastModePayloadHandler();
        static::assertSame([InteractionName::BROADCAST_MODE_TOGGLED], $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $payloadHandler = new ToggleBroadcastModePayloadHandler();
        $validationConstraint = $payloadHandler->getPayloadValidationConstraint();
        $expectValidationConstraint = new Collection(
            [
                'active' => [new Type('bool'), new NotNull()]
            ]
        );
        static::assertEquals($expectValidationConstraint, $validationConstraint);
    }

    /**
     * @dataProvider getTestCreatePayloadWithInvalidPayloadProviderData
     *
     * @param array<string, mixed> $payload
     * @param class-string<\Throwable> $exception
     */
    public function testCreatePayloadWithInvalidPayload(
        array $payload,
        string $exception
    ): void
    {
        $payloadHandler = new ToggleBroadcastModePayloadHandler();
        static::expectException($exception);
        $payloadHandler->createPayload($payload);
    }

    public static function getTestCreatePayloadWithInvalidPayloadProviderData(): \Generator
    {
        yield [[], \Exception::class];
        yield [['test-key' => 'test-value'], \Exception::class];
        yield [['active' => 'true'], \TypeError::class];
        yield [['active' => 'false'], \TypeError::class];
        yield [['active' => 2], \TypeError::class];
        yield [['active' => null], \Exception::class];
    }

    public function testCreatePayloadWithValidPayload(): void
    {
        $payloadHandler = new ToggleBroadcastModePayloadHandler();
        $payload = $payloadHandler->createPayload(['active' => true]);
        static::assertTrue($payload->getActive());
        $payload = $payloadHandler->createPayload(['active' => false]);
        static::assertFalse($payload->getActive());
    }
}
