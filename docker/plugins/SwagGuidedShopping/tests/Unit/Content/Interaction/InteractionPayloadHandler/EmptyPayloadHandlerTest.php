<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\EmptyPayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

class EmptyPayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new EmptyPayloadHandler();
        $expectInteractionNames = [
            InteractionName::REMOTE_CHEKOUT_ACCEPTED,
            InteractionName::REMOTE_CHEKOUT_DENIED,
            InteractionName::DYNAMIC_PAGE_CLOSED,
            InteractionName::KEEP_ALIVE
        ];
        static::assertSame($expectInteractionNames, $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $payloadHandler = new EmptyPayloadHandler();
        $validationConstraint = $payloadHandler->getPayloadValidationConstraint();
        $expectValidationConstraint = new Collection([
            'fields' => [],
        ]);
        static::assertEquals($expectValidationConstraint, $validationConstraint);
    }

    public function testCreatePayloadWithValidPayload(): void
    {
        $payloadHandler = new EmptyPayloadHandler();
        $payload = $payloadHandler->createPayload(['test-key' => 'test-value']);
        static::assertCount(1, $payload->getVars());
        static::assertArrayHasKey('extensions', $payload->getVars());
    }
}
