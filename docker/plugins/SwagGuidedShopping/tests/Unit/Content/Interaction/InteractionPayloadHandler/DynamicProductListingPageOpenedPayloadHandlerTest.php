<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\DynamicProductListingPageOpenedPayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DynamicProductListingPageOpenedPayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new DynamicProductListingPageOpenedPayloadHandler();
        static::assertSame([InteractionName::DYNAMIC_PRODUCT_LISTING_PAGE_OPENED], $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $payloadHandler = new DynamicProductListingPageOpenedPayloadHandler();
        $validationConstraint = $payloadHandler->getPayloadValidationConstraint();
        $expectValidationConstraint = new Collection(
            [
                'type' => [new Type('string'), new NotBlank()],
                'page' => [new Type('int'), new NotBlank()],
            ]
        );
        static::assertEquals($expectValidationConstraint, $validationConstraint);
    }

    /**
     * @dataProvider getTestCreatePayloadWithInvalidPayloadProviderData
     *
     * @param array<string, mixed> $payload
     * @param class-string<\Throwable> $exception
     */
    public function testCreatePayloadWithInvalidPayload(
        array $payload,
        string $exception
    ): void
    {
        $payloadHandler = new DynamicProductListingPageOpenedPayloadHandler();
        static::expectException($exception);
        $payloadHandler->createPayload($payload);
    }

    public static function getTestCreatePayloadWithInvalidPayloadProviderData(): \Generator
    {
        yield [['test-key' => 'test-value'], \Exception::class];
        yield [['type' => 'test-type'], \Exception::class];
        yield [['page' => 1], \Exception::class];
        yield [['type' => 1, 'page' => 1], \TypeError::class];
        yield [['type' => 'test-type', 'page' => 'test-page'], \TypeError::class];
    }

    public function testCreatePayloadWithValidPayload(): void
    {
        $payloadHandler = new DynamicProductListingPageOpenedPayloadHandler();
        $payload = $payloadHandler->createPayload(['type' => 'test-type', 'page' => 13]);
        static::assertEquals('test-type', $payload->getType());
        static::assertEquals(13, $payload->getPage());
    }
}
