<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\DynamicProductPageOpenedPayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DynamicProductPageOpenedPayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new DynamicProductPageOpenedPayloadHandler();
        static::assertSame([InteractionName::DYNAMIC_PRODUCT_PAGE_OPENED], $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $payloadHandler = new DynamicProductPageOpenedPayloadHandler();
        $validationConstraint = $payloadHandler->getPayloadValidationConstraint();
        $expectValidationConstraint = new Collection(
            [
                'type' => [new Type('string'), new NotBlank()],
                'productId' => [new Type('string'), new NotBlank()],
            ]
        );
        static::assertEquals($expectValidationConstraint, $validationConstraint);
    }

    /**
     * @dataProvider getTestCreatePayloadWithInvalidPayloadProviderData
     *
     * @param array<string, mixed> $payload
     * @param class-string<\Throwable> $exception
     */
    public function testCreatePayloadWithInvalidPayload(
        array $payload,
        string $exception
    ): void
    {
        $payloadHandler = new DynamicProductPageOpenedPayloadHandler();
        static::expectException($exception);
        $payloadHandler->createPayload($payload);
    }

    public static function getTestCreatePayloadWithInvalidPayloadProviderData(): \Generator
    {
        yield [['test-key' => 'test-value'], \Exception::class];
        yield  [['type' => 'test-type'], \Exception::class];
        yield [['productId' => 'test-product-id'], \Exception::class];
        yield [['type' => 1, 'productId' => 'test-product-id'], \TypeError::class];
        yield [['type' => 'test-type', 'productId' => 1], \TypeError::class];
    }

    public function testCreatePayloadWithValidPayload(): void
    {
        $payloadHandler = new DynamicProductPageOpenedPayloadHandler();
        $payload = $payloadHandler->createPayload(['type' => 'test-type', 'productId' => 'test-product-id']);
        static::assertEquals('test-type', $payload->getType());
        static::assertEquals('test-product-id', $payload->getProductId());
    }
}
