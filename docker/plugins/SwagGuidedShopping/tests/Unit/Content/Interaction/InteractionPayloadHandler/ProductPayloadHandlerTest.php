<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\ProductPayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class ProductPayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new ProductPayloadHandler();
        $expectInteractionNames = [
            InteractionName::PRODUCT_VIEWED,
            InteractionName::QUICKVIEW_OPENED,
            InteractionName::QUICKVIEW_CLOSED,
            InteractionName::ATTENDEE_PRODUCT_COLLECTION_DISLIKED,
            InteractionName::ATTENDEE_PRODUCT_COLLECTION_LIKED,
            InteractionName::ATTENDEE_PRODUCT_COLLECTION_REMOVED
        ];
        static::assertSame($expectInteractionNames, $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $payloadHandler = new ProductPayloadHandler();
        $validationConstraint = $payloadHandler->getPayloadValidationConstraint();
        $expectValidationConstraint = new Collection(
            [
                'productId' => [new Type('string'), new NotBlank()],
            ]
        );
        static::assertEquals($expectValidationConstraint, $validationConstraint);
    }

    /**
     * @dataProvider getTestCreatePayloadWithInvalidPayloadProviderData
     *
     * @param array<string, mixed> $payload
     * @param class-string<\Throwable> $exception
     */
    public function testCreatePayloadWithInvalidPayload(
        array $payload,
        string $exception
    ): void
    {
        $payloadHandler = new ProductPayloadHandler();
        static::expectException($exception);
        $payloadHandler->createPayload($payload);
    }

    public static function getTestCreatePayloadWithInvalidPayloadProviderData(): \Generator
    {
        yield [['test-key' => 'test-value'], \Exception::class];
        yield [['productId' => 1], \TypeError::class];
    }

    public function testCreatePayloadWithValidPayload(): void
    {
        $payloadHandler = new ProductPayloadHandler();
        $payload = $payloadHandler->createPayload(['productId' => 'test-product-id']);
        static::assertEquals('test-product-id', $payload->getProductId());
    }
}
