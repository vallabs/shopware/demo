<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\InteractionPayloadHandler;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\GuideHoveredPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\GuideHoveredPayloadHandler;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Type;

class GuideHoveredPayloadHandlerTest extends TestCase
{
    public function testGetSupportedInteractions(): void
    {
        $payloadHandler = new GuideHoveredPayloadHandler();
        static::assertSame([InteractionName::GUIDE_HOVERED], $payloadHandler::getSupportedInteractions());
    }

    public function testGetPayloadValidationConstraint(): void
    {
        $constraint = new Collection(
            [
                'hoveredElementId' => [new Type('string')],
            ]
        );
        $payloadHandler = new GuideHoveredPayloadHandler();
        static::assertEquals($constraint, $payloadHandler->getPayloadValidationConstraint());
    }

    public function testCreatePayload(): void

    {
        $payload = ['hoveredElementId' => 'test-hovered-element-id'];
        $payloadHandler = new GuideHoveredPayloadHandler();
        static::assertEquals(new GuideHoveredPayload($payload['hoveredElementId']), $payloadHandler->createPayload($payload));
    }
}
