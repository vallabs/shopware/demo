<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadHandlerNotFound;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\ProductPayloadHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandlerCollection;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;

class InteractionPayloadHandlerCollectionTest extends TestCase
{
    public function testInitialize(): void
    {
        $productPayloadHandler = new ProductPayloadHandler();
        $collection = new InteractionPayloadHandlerCollection([new ProductPayloadHandler()]);
        static::assertCount(6, $productPayloadHandler::getSupportedInteractions());

        $payload = ['productId' => 'test-product-id'];
        $interactions = [
            InteractionName::PRODUCT_VIEWED,
            InteractionName::QUICKVIEW_OPENED,
            InteractionName::QUICKVIEW_CLOSED,
            InteractionName::ATTENDEE_PRODUCT_COLLECTION_DISLIKED,
            InteractionName::ATTENDEE_PRODUCT_COLLECTION_LIKED,
            InteractionName::ATTENDEE_PRODUCT_COLLECTION_REMOVED,
        ];

        foreach ($interactions as $interactionName) {
            // test with valid interaction name
            static::assertInstanceOf(ProductPayloadHandler::class, $collection->getHandler($interactionName));
            static::assertInstanceOf(ProductPayload::class, $collection->createPayload($interactionName, $payload));
        }

        // test with invalid interaction name
        static::expectException(InteractionPayloadHandlerNotFound::class);
        $collection->getHandler('test-get-handler-but-get-exception-instead');
    }
}
