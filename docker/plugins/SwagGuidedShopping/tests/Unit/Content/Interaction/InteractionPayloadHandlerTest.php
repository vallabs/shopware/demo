<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadHandlerNotFound;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\ProductPayloadHandler;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandlerCollection;

/**
 * @internal
 * @coversDefaultClass \SwagGuidedShopping\Content\Interaction\InteractionPayloadHandlerCollection
 */
class InteractionPayloadHandlerTest extends TestCase
{
    public function testThatInvalidNameCantBeCreated(): void
    {
        $payloadHandlerCollection = new InteractionPayloadHandlerCollection([new ProductPayloadHandler()]);
        $this->expectException(InteractionPayloadHandlerNotFound::class);
        $payloadHandlerCollection->getHandler('invalid_');
    }
}
