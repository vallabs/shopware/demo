<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\SalesChannel\CreateInteraction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionPayloadHandlerNotFoundException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class CreateInteractionPayloadHandlerNotFoundExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new CreateInteractionPayloadHandlerNotFoundException('test-message');
        static::assertSame('test-message', $exception->getMessage());
        static::assertSame(Response::HTTP_NOT_FOUND, $exception->getStatusCode());
        static::assertSame(ErrorCode::GUIDED_SHOPPING__CREATE_INTERACTION_PAYLOAD_HANDLER_NOT_FOUND, $exception->getErrorCode());
    }
}
