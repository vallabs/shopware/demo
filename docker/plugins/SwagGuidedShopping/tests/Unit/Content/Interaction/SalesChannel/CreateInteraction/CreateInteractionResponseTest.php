<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\SalesChannel\CreateInteraction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionResponse;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionStruct;

class CreateInteractionResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $data = new CreateInteractionStruct();
        $response = new CreateInteractionResponse($data);
        static::assertSame($data, $response->getObject());
    }
}
