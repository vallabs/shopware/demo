<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\SalesChannel\CreateInteraction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionViolationException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationList;

class CreateInteractionViolationExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $violationList = new ViolationList(new ConstraintViolationList());
        $exception = new CreateInteractionViolationException($violationList);
        static::assertEquals("Could not add interaction \n" . $violationList, $exception->getMessage());
    }

    public function testGetStatusCode(): void
    {
        $violationList = new ViolationList(new ConstraintViolationList());
        $exception = new CreateInteractionViolationException($violationList);
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
    }

    public function testGetErrorCode(): void
    {
        $violationList = new ViolationList(new ConstraintViolationList());
        $exception = new CreateInteractionViolationException($violationList);
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__INTERACTION_ADD_VIOLATION, $exception->getErrorCode());
    }
}
