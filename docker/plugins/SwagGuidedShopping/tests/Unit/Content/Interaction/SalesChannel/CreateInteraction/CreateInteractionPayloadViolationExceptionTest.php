<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\SalesChannel\CreateInteraction;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionPayloadViolationException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class CreateInteractionPayloadViolationExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList = new ConstraintViolationList();
        $constraintViolationList->add($violate);
        $violationList = new ViolationList($constraintViolationList);

        $exception = new CreateInteractionPayloadViolationException($violationList);
        static::assertSame(
            "Could not add interaction, there are errors with the payload \n" . $violationList,
            $exception->getMessage()
        );
        static::assertSame(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
        static::assertSame(ErrorCode::GUIDED_SHOPPING__INTERACTION_ADD_PAYLOAD_VIOLATION, $exception->getErrorCode());
    }
}
