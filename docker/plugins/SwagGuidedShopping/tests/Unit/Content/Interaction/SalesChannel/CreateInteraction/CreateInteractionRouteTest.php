<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\SalesChannel\CreateInteraction;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionPayloadViolationException;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionRequestHandler;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionRoute;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionViolationException;
use SwagGuidedShopping\Service\Validator\ViolationList;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationList;

class CreateInteractionRouteTest extends TestCase
{
    protected Request $request;

    protected function setUp(): void
    {
        parent::setUp();
        $this->request = new Request();
    }

    public function testGetDecorated(): void
    {
        $requestHandler = $this->createMock(CreateInteractionRequestHandler::class);
        $requestHandler->expects(static::never())->method('validateRequest');
        $requestHandler->expects(static::never())->method('handleRequest');
        $route = new CreateInteractionRoute($requestHandler);
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testCreateButRequestIsInvalid(): void
    {
        $requestHandler = $this->createMock(CreateInteractionRequestHandler::class);
        $requestHandler->expects(static::once())
            ->method('validateRequest')
            ->willThrowException(new CreateInteractionViolationException(new ViolationList(new ConstraintViolationList())));
        $requestHandler->expects(static::never())->method('handleRequest');
        $route = new CreateInteractionRoute($requestHandler);
        static::expectException(CreateInteractionViolationException::class);
        $route->create(
            new RequestDataBag($this->request->request->all()),
            $this->request,
            (new SalesChannelContextHelper())->createSalesChannelContext()
        );
    }

    public function testCreateButHaveErrorWhileHandlingRequest(): void
    {
        $requestHandler = $this->createMock(CreateInteractionRequestHandler::class);
        $requestHandler->expects(static::once())->method('validateRequest');
        $requestHandler->expects(static::once())
            ->method('handleRequest')
            ->willThrowException(new CreateInteractionPayloadViolationException(new ViolationList(new ConstraintViolationList())));
        $route = new CreateInteractionRoute($requestHandler);
        static::expectException(CreateInteractionPayloadViolationException::class);
        $route->create(
            new RequestDataBag($this->request->request->all()),
            $this->request,
            (new SalesChannelContextHelper())->createSalesChannelContext()
        );
    }

    public function testCreateWithInvalidRequestAndHandleRequestSuccessfully(): void
    {
        $requestHandler = $this->createMock(CreateInteractionRequestHandler::class);
        $requestHandler->expects(static::once())->method('validateRequest');
        $requestHandler->expects(static::once())->method('handleRequest');
        $route = new CreateInteractionRoute($requestHandler);
        $route->create(
            new RequestDataBag($this->request->request->all()),
            $this->request,
            (new SalesChannelContextHelper())->createSalesChannelContext()
        );
    }
}
