<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\SalesChannel\CreateInteraction;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadException;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionPayloadViolationException;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionRequestHandler;
use SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction\CreateInteractionViolationException;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Service\Validator\ViolationList;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validation;

class CreateInteractionRequestHandlerTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;

    protected AttendeeEntity $attendee;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $this->attendee = new AttendeeEntity();
        $this->attendee->setId('test-attendee-id');
        $this->salesChannelContext->addExtension(
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME,
            $this->attendee
        );
    }

    public function testHandleRequestWithRequestDataBagDoNotHasInteractionName(): void
    {
        $request = new Request();
        $requestDataBag = new RequestDataBag();

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::never())->method('handle');

        $handler = $this->getHandler(null, $interactionService);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('Interaction name is empty.');
        $handler->handleRequest($request, $requestDataBag, $this->salesChannelContext);
    }

    public function testHandleRequestWithRequestDataBagHasInteractionNameButCanNotGenerateTriggeredDate(): void
    {
        $request = new Request();
        $requestDataBag = new RequestDataBag(['name' => 'test-interaction-name', 'triggeredAt' => '2000-01-01']);

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::never())->method('handle');

        $handler = $this->getHandler(null, $interactionService);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('Could not generate a triggered at date.');
        $handler->handleRequest($request, $requestDataBag, $this->salesChannelContext);
    }

    public function testHandleRequestWithRequestDataBagHasInteractionButGetInteractionPayloadExceptionMeanwhileHandling(): void
    {
        $request = new Request();
        $requestDataBag = new RequestDataBag(
            [
                'name' => 'test-interaction-name',
                'triggeredAt' => '2000-01-01 01:01:01',
                'payload' => [
                    'productId' => 'test-product-id'
                ],
                'lifeTimeInSeconds' => 13
            ]
        );

        $interactionService = $this->createMock(InteractionService::class);
        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $violationList = new ViolationList($constraintViolationList);
        $interactionService->expects(static::once())
            ->method('handle')
            ->with(
                static::equalTo('test-interaction-name'),
                static::equalTo(\DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2000-01-01 01:01:01')),
                static::equalTo(13),
                static::equalTo($this->attendee),
                static::equalTo(['productId' => 'test-product-id']),
                $this->salesChannelContext
            )
            ->willThrowException(new InteractionPayloadException($violationList));

        $handler = $this->getHandler(null, $interactionService);
        static::expectException(CreateInteractionPayloadViolationException::class);
        $handler->handleRequest($request, $requestDataBag, $this->salesChannelContext);
    }

    /**
     * @dataProvider getTestHandleRequestWithRequestDataBagHasInteractionNameProviderData
     *
     * @param array<string, mixed> $data
     * @param array<string> $expectHandlePayload
     */
    public function testHandleRequestWithRequestDataBagHasInteractionName(
        array $data,
        array $expectHandlePayload,
        callable $expectHandleTriggeredAt
    ): void
    {
        $request = new Request();
        $data['name'] = 'test-interaction-name';
        $requestDataBag = new RequestDataBag($data);

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::once())
            ->method('handle')
            ->with(
                static::equalTo('test-interaction-name'),
                static::callback($expectHandleTriggeredAt),
                static::equalTo(-1),
                static::equalTo($this->attendee),
                static::equalTo($expectHandlePayload),
                $this->salesChannelContext
            );

        $handler = $this->getHandler(null, $interactionService);
        $handler->handleRequest($request, $requestDataBag, $this->salesChannelContext);
    }

    public static function getTestHandleRequestWithRequestDataBagHasInteractionNameProviderData(): \Generator
    {
        yield 'empty payload' => [
            [],
            [],
            function (\DateTimeImmutable $triggeredAt) {
                $triggeredTimeDiff = $triggeredAt->diff(new \DateTimeImmutable('now'));
                return $triggeredTimeDiff->format('%s') <= 5;
            }
        ];

        yield 'payload with productId' => [
            ['payload' => ['productId' => 'test-product-id']],
            ['productId' => 'test-product-id'],
            function (\DateTimeImmutable $triggeredAt) {
                $triggeredTimeDiff = $triggeredAt->diff(new \DateTimeImmutable('now'));
                return $triggeredTimeDiff->format('%s') <= 5;
            }
        ];

        yield 'payload with triggeredAt' => [
            ['triggeredAt' => '2000-01-01 01:01:01'],
            [],
            function (\DateTimeImmutable $triggeredAt) {
                /** @var \DateTimeInterface $expectTriggeredAt */
                $expectTriggeredAt = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2000-01-01 01:01:01');
                $triggeredTimeDiff = $triggeredAt->diff($expectTriggeredAt);
                return $triggeredTimeDiff->format('%s') == 0;
            }
        ];

        yield 'payload with triggeredAt and productId' => [
            ['payload' => ['productId' => 'test-product-id'], 'triggeredAt' => '2000-01-01 01:01:01'],
            ['productId' => 'test-product-id'],
            function (\DateTimeImmutable $triggeredAt) {
                /** @var \DateTimeInterface $expectTriggeredAt */
                $expectTriggeredAt = \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', '2000-01-01 01:01:01');
                $triggeredTimeDiff = $triggeredAt->diff($expectTriggeredAt);
                return $triggeredTimeDiff->format('%s') == 0;
            }
        ];
    }

    /**
     * @dataProvider getTestValidateProviderData
     *
     * @param array<string, mixed> $data
     */
    public function testValidateRequest(
        array $data
    ): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $interactionService = $this->createMock(InteractionService::class);
        $handler = new CreateInteractionRequestHandler($validator, $interactionService);
        static::expectException(CreateInteractionViolationException::class);
        $handler->validateRequest(new Request($data));
    }

    public static function getTestValidateProviderData(): \Generator
    {
        yield [['name' => []]];
        yield [['name' => 'test-interaction-name', 'lifeTimeInSeconds' => '13']];
        yield [['name' => 'test-interaction-name', 'lifeTimeInSeconds' => 13, 'payload' => '']];
        yield [['name' => 'test-interaction-name', 'lifeTimeInSeconds' => 13, 'payload' => []], 'triggeredAt' => '2000-01-01'];
    }

    private function getHandler(
        ?MockObject $validatorMock = null,
        ?MockObject $interactionServiceMock = null
    ): CreateInteractionRequestHandler
    {
        /** @var MockObject&DataValidator $validator */
        $validator = $validatorMock ?? $this->createMock(DataValidator::class);
        /** @var MockObject&InteractionService $interactionService */
        $interactionService = $interactionServiceMock ?? $this->createMock(InteractionService::class);

        return new CreateInteractionRequestHandler($validator, $interactionService);
    }
}
