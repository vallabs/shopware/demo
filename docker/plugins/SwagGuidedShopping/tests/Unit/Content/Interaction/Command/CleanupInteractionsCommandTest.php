<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\Command;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Command\CleanupInteractionsCommand;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use Symfony\Component\Console\Tester\CommandTester;

class CleanupInteractionsCommandTest extends TestCase
{
    public function testExecuteCommand(): void
    {
        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::once())
            ->method('cleanupInteractions')
            ->willReturn(13);

        $cleanupInteractionsCommand = new CleanupInteractionsCommand($interactionService);
        $commandTester = new CommandTester($cleanupInteractionsCommand);

        $commandTester->execute([]);
        $commandOutput = $commandTester->getDisplay();
        $expectCommandOutput = "Start deleting outdated interactions.\nDeleted 13 interactions.\n";
        static::assertEquals($expectCommandOutput, $commandOutput);
    }
}
