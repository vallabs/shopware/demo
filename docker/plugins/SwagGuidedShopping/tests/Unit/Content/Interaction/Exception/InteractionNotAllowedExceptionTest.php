<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionNotAllowedException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class InteractionNotAllowedExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new InteractionNotAllowedException();
        static::assertSame('This interaction is not allowed.', $exception->getMessage());
        static::assertSame(Response::HTTP_METHOD_NOT_ALLOWED, $exception->getStatusCode());
        static::assertSame(ErrorCode::GUIDED_SHOPPING__INTERACTION_NOT_ALLOWED, $exception->getErrorCode());
    }
}
