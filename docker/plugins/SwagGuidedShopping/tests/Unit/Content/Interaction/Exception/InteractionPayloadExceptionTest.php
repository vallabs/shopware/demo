<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadException;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class InteractionPayloadExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $expectViolationCollection = new ViolationList($constraintViolationList);

        $exception = new InteractionPayloadException($expectViolationCollection);

        static::assertSame($expectViolationCollection, $exception->getViolationsCollection());
        static::assertSame("The provided interaction payload is not valid \n" . $expectViolationCollection, $exception->getMessage());
        static::assertSame('GUIDED_SHOPPING__INTERACTION_ADD_PAYLOAD_VIOLATION', $exception->getErrorCode());
        static::assertSame(400, $exception->getStatusCode());
    }
}
