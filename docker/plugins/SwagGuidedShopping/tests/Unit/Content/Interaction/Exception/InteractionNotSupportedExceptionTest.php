<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionNotSupportedException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class InteractionNotSupportedExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new InteractionNotSupportedException('test-handler', 'test-interaction');
        static::assertSame(\sprintf('The interaction test-interaction is not support by the handler test-handler'), $exception->getMessage());
        static::assertSame(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
        static::assertSame(ErrorCode::GUIDED_SHOPPING__INTERACTION_NOT_SUPPORTED, $exception->getErrorCode());
    }
}
