<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Interaction\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadHandlerNotFound;

class InteractionPayloadHandlerNotFoundTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new InteractionPayloadHandlerNotFound(
            'test-interaction',
            ['test-handler', 'test-handler-2']
        );

        static::assertSame('There is no interaction payload handler for the interaction with the name test-interaction. Possible interactions are (test-handler, test-handler-2)', $exception->getMessage());
        static::assertSame(400, $exception->getStatusCode());
        static::assertSame('GUIDED_SHOPPING__CREATE_INTERACTION_PAYLOAD_HANDLER_NOT_FOUND', $exception->getErrorCode());
    }
}
