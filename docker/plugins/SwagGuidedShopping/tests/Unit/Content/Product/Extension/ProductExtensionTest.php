<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\Extension;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionDefinition;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;
use SwagGuidedShopping\Content\Product\Extension\ProductExtension;

class ProductExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $collection = new FieldCollection();
        $extension = new ProductExtension();
        $extension->extendFields($collection);
        // Check the fields count
        $elements = $collection->getElements();
        static::assertCount(2, $elements);
        // Check the first field
        /** @var AssociationField $element */
        $element = $elements[0];
        static::assertInstanceOf(OneToManyAssociationField::class, $element);
        static::assertEquals('attendeeProductCollections', $element->getPropertyName());
        static::assertEquals(AttendeeProductCollectionDefinition::class, $element->getReferenceClass());
        static::assertEquals('product_id', $element->getReferenceField());
        static::assertNotNull($element->getFlag(CascadeDelete::class));
        static::assertNotNull($element->getFlag(ApiAware::class));
        // Check the second field
        /** @var AssociationField $element2 */
        $element2 = $elements[1];
        static::assertInstanceOf(OneToManyAssociationField::class, $element2);
        static::assertEquals('presentationCmsPages', $element2->getPropertyName());
        static::assertEquals(PresentationCmsPageDefinition::class, $element2->getReferenceClass());
        static::assertEquals('product_id', $element2->getReferenceField());
        static::assertNotNull($element2->getFlag(CascadeDelete::class));
    }

    public function testGetDefinitionClass(): void
    {
        $extension = new ProductExtension();
        static::assertEquals(ProductDefinition::class, $extension->getDefinitionClass());
    }
}
