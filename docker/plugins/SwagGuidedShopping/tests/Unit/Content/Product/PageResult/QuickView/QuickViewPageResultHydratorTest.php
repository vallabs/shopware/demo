<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\PageResult\QuickView;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Category\Service\CategoryBreadcrumbBuilder;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use Shopware\Core\Content\Seo\SeoUrl\SeoUrlCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use SwagGuidedShopping\Content\Product\PageResult\QuickView\QuickViewPageResultHydrator;
use Shopware\Storefront\Framework\Routing\Router;

class QuickViewPageResultHydratorTest extends TestCase
{
    /**
     * @dataProvider getTestHydrate
     */
    public function testHydrate(
        SalesChannelProductEntity $product,
        ?PropertyGroupCollection $propertyGroupCollection
    ): void
    {
        /** @var MockObject&Router $router */
        $router = $this->createMock(Router::class);
        /** @var MockObject&EntityRepository<SeoUrlCollection> $seoUrlRepository */
        $seoUrlRepository = $this->createMock(EntityRepository::class);
        /** @var MockObject&CategoryBreadcrumbBuilder $categoryBreadcrumbBuilder */
        $categoryBreadcrumbBuilder = $this->createMock(CategoryBreadcrumbBuilder::class);

        $hydrator = new QuickViewPageResultHydrator(
            $router,
            $seoUrlRepository,
            $categoryBreadcrumbBuilder
        );

        $originalCmsPage = $product->getCmsPage();

        $pageResult = $hydrator->hydrate($product, $propertyGroupCollection);

        static::assertSame($product, $pageResult->getProduct());
        if ($propertyGroupCollection) {
            static::assertSame($propertyGroupCollection, $pageResult->getConfigurator());
        }
        static::assertSame($originalCmsPage, $pageResult->getCmsPage());
        static::assertInstanceOf(CmsPageEntity::class, $pageResult->getProduct()->getCmsPage());
        static::assertEquals(new CmsPageEntity(), $pageResult->getProduct()->getCmsPage());
    }

    public static function getTestHydrate(): \Generator
    {
        $product = new SalesChannelProductEntity();
        $product->setId('test-sales-channel-product-id');
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId('test-cms-page-id');
        $product->setCmsPage($cmsPage);
        yield 'without-property-group' => [
            $product,
            null
        ];

        $propertyGroup = new PropertyGroupEntity();
        $propertyGroup->setId('test-property-group-id');
        yield 'with-property-group' => [
            $product,
            new PropertyGroupCollection([$propertyGroup])
        ];

        $product2 = new SalesChannelProductEntity();
        $product2->setId('test-sales-channel-product-id-2');
        yield 'without-property-group-2' => [
            $product2,
            null
        ];

        $propertyGroup2 = new PropertyGroupEntity();
        $propertyGroup2->setId('test-property-group-id-2');
        yield 'with-property-group-2' => [
            $product2,
            new PropertyGroupCollection([$propertyGroup2])
        ];
    }
}
