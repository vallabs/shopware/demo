<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\PageResult;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use SwagGuidedShopping\Content\Product\PageResult\ProductPageResult;

class ProductPageResultTest extends TestCase
{
    public function testProduct(): void
    {
        $pageResult = new ProductPageResult();
        $product = new SalesChannelProductEntity();
        $product->setId('test-sales-channel-product-id');
        $pageResult->setProduct($product);
        static::assertSame($product, $pageResult->getProduct());
    }

    public function testConfigurator(): void
    {
        $pageResult = new ProductPageResult();
        $propertyGroup = new PropertyGroupEntity();
        $propertyGroup->setId('test-property-group-id');
        $propertyGroupCollection = new PropertyGroupCollection([$propertyGroup]);
        $pageResult->setConfigurator($propertyGroupCollection);
        static::assertSame($propertyGroupCollection, $pageResult->getConfigurator());
    }
}
