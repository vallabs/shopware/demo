<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\SalesChannel\Listing;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingLoader;
use Shopware\Core\Content\Product\SalesChannel\ProductAvailableFilter;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Product\SalesChannel\Listing\ProductListingRoute;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class ProductListingRouteTest extends TestCase
{
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }
    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    /**
     * @dataProvider getTestLoadProductsProviderData
     */
    public function testLoadProductsWithAllIdsButNoVariantsIncluded(
        bool $loadInteraction,
        int $expectCalledTimes
    ): void
    {
        $criteria = new Criteria();
        $request = new Request([], ['interaction' => $loadInteraction, 'useIdSorting' => true, 'loadVariants' => false, 'loadAllIds' => true]);

        $listingLoader = $this->createListingLoaderMock(true);

        $eventDispatcher = $this->createEventDispatcherMock($expectCalledTimes);

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method('search');
        $productRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::callback(function (Criteria $criteria) {
                    $groupFields = $criteria->getGroupFields();
                    if (!isset($groupFields[0]) || $groupFields[0]->getField() !== 'displayGroup') {
                        return false;
                    }

                    $filters = $criteria->getFilters();
                    $displayGroupFilter = new NotFilter(
                        MultiFilter::CONNECTION_AND,
                        [new EqualsFilter('displayGroup', null)]
                    );
                    if (!\in_array($displayGroupFilter, $filters)) {
                        return false;
                    }

                    return true;
                })
            )
            ->willReturn(
                new IdSearchResult(
                    1,
                    [['data' => ['test-product-id'], 'primaryKey' => 'test-product-id']],
                    new Criteria(),
                    $this->salesChannelContext->getContext()
                )
            );

        $route = $this->getRoute($listingLoader, $eventDispatcher, $productRepository);

        $response = $route->load($request, $this->salesChannelContext, $criteria);

        $filters = $criteria->getFilters();

        $productAvailableFilter = new ProductAvailableFilter($this->salesChannelContext->getSalesChannel()->getId());
        static::assertEquals($productAvailableFilter, $filters[0]);

        $result = $response->getResult();
        static::assertTrue($result->hasState('test-state'));
        static::assertEquals(1, $result->getTotal());
        static::assertTrue($result->hasExtension('allIds'));
        $extension = $result->getExtension('allIds');
        static::assertInstanceOf(ArrayStruct::class, $extension);
        static::assertEquals(1, $extension->get('total'));
        static::assertEquals(['test-product-id'], $extension->get('data'));
    }

    /**
     * @dataProvider getTestLoadProductsProviderData
     */
    public function testLoadProductsWithAllIdsAndVariantsIncluded(
        bool $loadInteraction,
        int $expectCalledTimes
    ): void
    {
        $criteria = new Criteria();
        $request = new Request([], ['interaction' => $loadInteraction, 'useIdSorting' => true, 'loadVariants' => true, 'loadAllIds' => true]);

        $listingLoader = $this->createListingLoaderMock(false);

        $eventDispatcher = $this->createEventDispatcherMock($expectCalledTimes);

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::once())
            ->method('search')
            ->with(
                static::callback(function (Criteria $criteria) {
                    if (!$criteria->hasAssociation('options')) {
                        return false;
                    }

                    $options = $criteria->getAssociation('options');
                    if (!$options->hasAssociation('group')) {
                        return false;
                    }

                    return true;
                }),
                static::equalTo($this->salesChannelContext)
            )
            ->willReturn($this->createProductSearchResult());
        $productRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::callback(function (Criteria $criteria) {
                    $groupFields = $criteria->getGroupFields();
                    if (\count($groupFields)) {
                        return false;
                    }

                    $filters = $criteria->getFilters();
                    $displayGroupFilter = new NotFilter(
                        MultiFilter::CONNECTION_AND,
                        [new EqualsFilter('displayGroup', null)]
                    );
                    if (\in_array($displayGroupFilter, $filters)) {
                        return false;
                    }

                    return true;
                })
            )
            ->willReturn(
                new IdSearchResult(
                    1,
                    [['data' => ['test-product-id'], 'primaryKey' => 'test-product-id']],
                    new Criteria(),
                    $this->salesChannelContext->getContext()
                )
            );

        $route = $this->getRoute($listingLoader, $eventDispatcher, $productRepository);

        $response = $route->load($request, $this->salesChannelContext, $criteria);

        $filters = $criteria->getFilters();

        $productAvailableFilter = new ProductAvailableFilter($this->salesChannelContext->getSalesChannel()->getId());
        static::assertEquals($productAvailableFilter, $filters[0]);

        $result = $response->getResult();
        static::assertTrue($result->hasState('test-state'));
        static::assertEquals(1, $result->getTotal());
        static::assertTrue($result->hasExtension('allIds'));
        $extension = $result->getExtension('allIds');
        static::assertInstanceOf(ArrayStruct::class, $extension);
        static::assertEquals(1, $extension->get('total'));
        static::assertEquals(['test-product-id'], $extension->get('data'));
    }

    public static function getTestLoadProductsProviderData(): \Generator
    {
        yield 'no-load-interaction' => [false, 4];
        yield 'load-interaction' => [true, 5];
    }

    public function testLoadProductWithNoAllIdsIncluded(): void
    {
        $criteria = new Criteria();
        $request = new Request([], ['loadVariants' => false, 'loadAllIds' => false]);

        $listingLoader = $this->createListingLoaderMock(true);

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::any())
            ->method('dispatch');

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method('search');
        $productRepository->expects(static::never())->method('searchIds');

        $route = $this->getRoute($listingLoader, $eventDispatcher, $productRepository);

        $response = $route->load($request, $this->salesChannelContext, $criteria);

        $filters = $criteria->getFilters();

        $productAvailableFilter = new ProductAvailableFilter($this->salesChannelContext->getSalesChannel()->getId());
        static::assertEquals($productAvailableFilter, $filters[0]);

        $result = $response->getResult();
        static::assertFalse($result->hasExtension('allIds'));
    }

    public function testChunkSortedIds(): void
    {
        $criteria = new Criteria();
        $sortedIds =  [
            'test-product-id-1',
            'test-product-id-2',
            'test-product-id-3'
        ];
        $criteria->setIds($sortedIds);

        $request = new Request(
            [],
            [
                'useIdSorting' => true,
                'limit' => 1,
                'p' => 2,
                'ids' => $sortedIds,
                'loadVariants' => true
            ]
        );

        $listingLoader = $this->createListingLoaderMock(false);

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::any())
            ->method('dispatch');

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::once())
            ->method('search')
            ->with(
                static::callback(function (Criteria $criteria) {
                    return $criteria->getOffset() === 0;
                }),
            )
            ->willReturn($this->createProductSearchResult());
        $productRepository->expects(static::never())->method('searchIds');

        $route = $this->getRoute($listingLoader, $eventDispatcher, $productRepository);

        $response = $route->load($request, $this->salesChannelContext, $criteria);
        $result = $response->getResult();

        static::assertEquals(1, $result->getEntities()->count());
        static::assertEquals(3, $result->getTotal());
    }

    private function getRoute(
        ?MockObject $listingLoaderMock = null,
        ?MockObject $eventDispatcherMock = null,
        ?MockObject $productRepositoryMock = null
    ): ProductListingRoute
    {
        /** @var MockObject&ProductListingLoader $listingLoader */
        $listingLoader = $listingLoaderMock ?? $this->createMock(ProductListingLoader::class);
        /** @var MockObject&EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $eventDispatcherMock ?? $this->createMock(EventDispatcherInterface::class);
        /** @var MockObject&SalesChannelRepository<ProductCollection> $productRepository */
        $productRepository = $productRepositoryMock ?? $this->createMock(SalesChannelRepository::class);

        return new ProductListingRoute($listingLoader, $eventDispatcher, $productRepository);
    }

    private function createListingLoaderMock(
        bool $isRun
    ): MockObject
    {
        $listingLoader = $this->createMock(ProductListingLoader::class);

        if ($isRun) {
            $listingLoader->expects(static::once())
                ->method('load')
                ->willReturn($this->createProductSearchResult());
        } else {
            $listingLoader->expects(static::never())->method('load');
        }

        return $listingLoader;
    }

    private function createEventDispatcherMock(int $expectCalledTimes): MockObject
    {
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::exactly($expectCalledTimes))
            ->method('dispatch');

        return $eventDispatcher;
    }

    /**
     * @return EntitySearchResult<ProductCollection>
     */
    private function createProductSearchResult(): EntitySearchResult
    {
        $product = new ProductEntity();
        $product->setId('test-product-id');
        $searchResult = new EntitySearchResult(
            ProductDefinition::ENTITY_NAME,
            1,
            new ProductCollection([$product]),
            null,
            new Criteria(),
            new Context(new SystemSource())
        );
        $state = 'test-state';
        $searchResult->addState($state);

        return $searchResult;
    }
}
