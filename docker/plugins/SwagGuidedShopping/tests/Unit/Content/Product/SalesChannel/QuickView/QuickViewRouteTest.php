<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\SalesChannel\QuickView;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Product\PageResult\ProductPageResult;
use SwagGuidedShopping\Content\Product\SalesChannel\PageLoaderService;
use SwagGuidedShopping\Content\Product\SalesChannel\QuickView\QuickViewRoute;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\SystemConfigMockBuilder;
use Symfony\Component\HttpFoundation\Request;

class QuickViewRouteTest extends TestCase
{
    protected SystemConfigMockBuilder $systemConfigMockBuilder;
    protected SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->systemConfigMockBuilder = new SystemConfigMockBuilder('test-config');
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testLoad(): void
    {
        $request = new Request();
        $productId = 'test-product-id';
        $cmsLayoutId = 'test-cms-layout-id';
        $config = [
            ConfigKeys::QUICK_VIEW_PAGE_ID => 'test-quick-view-page-id',
        ];

        $pageLoaderService = $this->createMock(PageLoaderService::class);
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsSection = (new CmsSectionMockHelper())->getCmsSectionEntity(
            'test-cms-section-id',
            'test-cms-page-id',
            1,
            new CmsBlockCollection()
        );
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection]));
        $result = new ProductPageResult();
        $product = new SalesChannelProductEntity();
        $product->setId($productId);
        $product->setCmsPage($cmsPage);
        $result->setProduct($product);
        $result->setCmsPage($product->getCmsPage());
        $propertyGroup = new PropertyGroupEntity();
        $propertyGroup->setId('test-property-group-id');
        $configurator = new PropertyGroupCollection([$propertyGroup]);
        $result->setConfigurator($configurator);
        $pageLoaderService->expects(static::once())
            ->method('load')
            ->with(
                static::equalTo('test-quick-view-page-id'),
                static::callback(
                    static function (Request $request) use ($productId, $cmsLayoutId): bool {
                        return $request->request->get('productId') === $productId
                            && $request->request->get('cmsPageLayoutId') === $cmsLayoutId;
                    }
                ),
            )
            ->willReturn($result);

        $route = $this->getRoute($config, $pageLoaderService);

        $response = $route->load($productId, $cmsLayoutId, $request, $this->salesChannelContext);
        $pageResult = $response->getObject();
        static::assertInstanceOf(ProductPageResult::class, $pageResult);
        static::assertSame($product, $pageResult->getProduct());
        static::assertSame($configurator, $pageResult->getConfigurator());
        static::assertSame($cmsPage, $pageResult->getCmsPage());
        static::assertSame($cmsPage, $pageResult->getProduct()->getCmsPage());
    }

    /**
     * @param array<string, mixed>|null $config
     */
    private function getRoute(
        ?array $config = null,
        ?MockObject $pageLoaderServiceMock = null
    ): QuickViewRoute
    {
        $systemConfig = $this->systemConfigMockBuilder->create($config ?? []);
        /** @var MockObject&PageLoaderService $pageLoaderService */
        $pageLoaderService = $pageLoaderServiceMock ?: $this->createMock(PageLoaderService::class);

        return new QuickViewRoute(
            $systemConfig,
            $pageLoaderService
        );
    }
}
