<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\SalesChannel;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\Detail\AbstractProductDetailRoute;
use Shopware\Core\Content\Product\SalesChannel\Detail\ProductDetailRouteResponse;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Presentation\Event\AfterLoadProductConfiguratorEvent;
use SwagGuidedShopping\Content\Product\Exception\ProductPageLoadInvalidRequestParameter;
use SwagGuidedShopping\Content\Product\PageResult\ProductPageResult;
use SwagGuidedShopping\Content\Product\PageResult\QuickView\QuickViewPageResultHydrator;
use SwagGuidedShopping\Content\Product\SalesChannel\PageLoaderService;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;

class PageLoaderServiceTest extends TestCase
{
    protected SalesChannelContext $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    /**
     * @param array<string, mixed> $params
     * @param class-string<\Throwable> $expectedException
     *
     * @dataProvider getTestLoadWithExceptionProviderData
     */
    public function testLoadWithException(
        array $params,
        string $expectedException
    ): void
    {
        $productDetailRoute = $this->createMock(AbstractProductDetailRoute::class);
        $productDetailRoute->expects(static::never())->method('load');

        $cmsPageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $cmsPageLoader->expects(static::never())->method('load');

        $notesElementHandler = $this->createMock(NotesElementHandler::class);
        $notesElementHandler->expects(static::never())->method('removeNotes');

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::never())->method('dispatch');

        $resultHydrator = $this->createMock(QuickviewPageResultHydrator::class);
        $resultHydrator->expects(static::never())->method('hydrate');

        $service = $this->getService($productDetailRoute, $cmsPageLoader, $notesElementHandler, $eventDispatcher, $resultHydrator);

        static::expectException($expectedException);

        $request = new Request([], $params);
        $service->load('test-product-detail-page-id', $request, $this->context);
    }

    public static function getTestLoadWithExceptionProviderData(): \Generator
    {
        $expectedMissingRequestParameterException = RoutingException::class;
        yield 'empty request params' => [
            [],
            $expectedMissingRequestParameterException
        ];

        $invalidRequestParamException = ProductPageLoadInvalidRequestParameter::class;
        yield 'request has invalid productId' => [
            [
                'productId' => 1,
            ],
            $invalidRequestParamException
        ];

        yield 'request has invalid cmsPageLayoutId' => [
            [
                'cmsPageLayoutId' => 1
            ],
            $invalidRequestParamException
        ];
    }

    /**
     * @dataProvider getTestLoadProvider
     *
     * @param SalesChannelProductEntity $product
     * @param array<string, mixed> $params
     * @param string $expectedCmsPageLayoutId
     */
    public function testLoad(
        SalesChannelProductEntity $product,
        array $params,
        string $expectedCmsPageLayoutId
    ): void
    {
        $request = new Request([], $params);

        $productDetailRoute = $this->createMock(AbstractProductDetailRoute::class);
        $propertyGroup = new PropertyGroupEntity();
        $propertyGroup->setId('test-property-group-id');
        $configurator = new PropertyGroupCollection([$propertyGroup]);
        $response = new ProductDetailRouteResponse($product, $configurator);
        $productDetailRoute->expects(static::once())
            ->method('load')
            ->with(
                static::equalTo($request->request->get('productId')),
                static::equalTo($request),
                static::equalTo($this->context),
                static::callback(static function (Criteria $criteria) {
                    return $criteria->hasAssociation('media')
                        && $criteria->hasAssociation('properties')
                        && $criteria->hasAssociation('productReviews')
                        && $criteria->hasAssociation('manufacturer')
                        && $criteria->getAssociation('properties')->hasAssociation('group')
                        && $criteria->getAssociation('manufacturer')->hasAssociation('media');
                })
            )
            ->willReturn($response);

        $cmsPageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsSection = (new CmsSectionMockHelper())->getCmsSectionEntity(
            'test-cms-section-id',
            'test-cms-page-id',
            1,
            new CmsBlockCollection()
        );
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection]));
        $cmsPageLoader->expects(static::once())
            ->method('load')
            ->with(
                static::equalTo($request),
                static::callback(static function (Criteria $criteria) use ($expectedCmsPageLayoutId) {
                    return $criteria->getIds() === [$expectedCmsPageLayoutId];
                }),
                static::equalTo($this->context),
                static::isNull(),
                static::isInstanceOf(EntityResolverContext::class)
            )
            ->willReturn(new EntitySearchResult(
                CmsPageDefinition::ENTITY_NAME,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $this->context->getContext()
            ));

        $notesElementHandler = $this->createMock(NotesElementHandler::class);
        $notesElementHandler->expects(static::once())
            ->method('removeNotes')
            ->with($cmsSection);

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::once())
            ->method('dispatch')
            ->with(
                static::isInstanceOf(AfterLoadProductConfiguratorEvent::class)
            );

        $resultHydrator = $this->createMock(QuickviewPageResultHydrator::class);
        $result = new ProductPageResult();
        $result->setProduct($product);
        $result->setConfigurator($configurator);
        $result->setCmsPage($cmsPage);
        $resultHydrator->expects(static::once())
            ->method('hydrate')
            ->willReturn($result);

        $service = $this->getService($productDetailRoute, $cmsPageLoader, $notesElementHandler, $eventDispatcher, $resultHydrator);

        $result = $service->load('test-product-detail-page-id', $request, $this->context);
        static::assertSame($product, $result->getProduct());
        static::assertSame($configurator, $result->getConfigurator());
        static::assertSame($cmsPage, $result->getCmsPage());
        static::assertSame($cmsPage, $result->getProduct()->getCmsPage());
    }

    public static function getTestLoadProvider(): \Generator
    {
        $product = new SalesChannelProductEntity();
        $product->setId('test-product-id');

        yield 'product has cmsPageId and no custom cmsPageLayoutId in request' => [
            $product,
            [
                'productId' => 'test-product-id',
            ],
            'test-product-detail-page-id'
        ];

        yield 'product has no cmsPageId and has custom cmsPageLayoutId in request' => [
            $product,
            [
                'productId' => 'test-product-id',
                'cmsPageLayoutId' => 'test-custom-cms-page-layout-id'
            ],
            'test-custom-cms-page-layout-id'
        ];

        $productHasCmsPageId = clone $product;
        $productHasCmsPageId->setCmsPageId('test-cms-page-id');
        yield 'product has cmsPageId and has no customPageLayoutId in request' => [
            $productHasCmsPageId,
            [
                'productId' => 'test-product-id',
                'useProductLayout' => true
            ],
            'test-cms-page-id'
        ];

        yield 'product has cmsPageId and has customPageLayoutId in request, and use product layout is true' => [
            $productHasCmsPageId,
            [
                'productId' => 'test-product-id',
                'cmsPageLayoutId' => 'test-custom-cms-page-layout-id',
                'useProductLayout' => true
            ],
            'test-cms-page-id'
        ];

        yield 'product has cmsPageId and has customPageLayoutId in request, and use product layout is false' => [
            $productHasCmsPageId,
            [
                'productId' => 'test-product-id',
                'cmsPageLayoutId' => 'test-custom-cms-page-layout-id',
                'useProductLayout' => false
            ],
            'test-custom-cms-page-layout-id'
        ];
    }

    private function getService(
        ?MockObject $productDetailRouteMock = null,
        ?MockObject $cmsPageLoaderMock = null,
        ?MockObject $notesElementHandlerMock = null,
        ?MockObject $eventDispatcherMock = null,
        ?MockObject $resultHydratorMock = null
    ): PageLoaderService
    {
        $dataValidator = new DataValidator(Validation::createValidator());
        /** @var MockObject&AbstractProductDetailRoute $productDetailRoute */
        $productDetailRoute = $productDetailRouteMock ?? $this->createMock(AbstractProductDetailRoute::class);
        /** @var MockObject&SalesChannelCmsPageLoaderInterface $cmsPageLoader */
        $cmsPageLoader = $cmsPageLoaderMock ?? $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        /** @var MockObject&NotesElementHandler $notesElementHandler */
        $notesElementHandler = $notesElementHandlerMock ?? $this->createMock(NotesElementHandler::class);
        /** @var MockObject&EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $eventDispatcherMock ?? $this->createMock(EventDispatcherInterface::class);
        /** @var MockObject&QuickviewPageResultHydrator $resultHydrator */
        $resultHydrator = $resultHydratorMock ?? $this->createMock(QuickViewPageResultHydrator::class);

        return new PageLoaderService(
            $dataValidator,
            $productDetailRoute,
            new ProductDefinition(),
            $cmsPageLoader,
            $notesElementHandler,
            $eventDispatcher,
            $resultHydrator
        );
    }
}
