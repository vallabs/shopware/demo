<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\Subscriber;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\ProductStream\Service\ProductStreamBuilder;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use SwagGuidedShopping\Content\Product\Subscriber\ProductListingCollectProductStreamFilterSubscriber;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\HttpFoundation\Request;

class ProductListingCollectProductStreamFilterSubscriberTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $subscriber = $this->getSubscriber();
        $events = $subscriber::getSubscribedEvents();
        static::assertEquals([ProductListingCriteriaEvent::class => 'addStreamFilters'], $events);
    }

    /**
     * @dataProvider getTestGetAddStreamFiltersWithEmptyRequestProviderData
     */
    public function testGetAddStreamFiltersWithEmptyRequest(
        Request $request
    ): void
    {
        $criteria = new Criteria();
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $event = new ProductListingCriteriaEvent($request, $criteria, $salesChannelContext);

        $productStreamBuilder = $this->createMock(ProductStreamBuilder::class);
        $productStreamBuilder->expects(static::never())->method('buildFilters');

        $subscriber = $this->getSubscriber($productStreamBuilder);
        $subscriber->addStreamFilters($event);
        static::assertCount(0, $criteria->getFilters());
    }

    public static function getTestGetAddStreamFiltersWithEmptyRequestProviderData(): \Generator
    {
        yield 'no-streams-key' => [new Request()];
        yield 'empty-streams' => [new Request(['streams' => ''])];
    }

    public function testGetAddStreamFiltersWithNoStreamFiltersBuiltFromBuilder(): void
    {
        $request = new Request(['streams' => 'test-product-id|test-product-id-2']);
        $criteria = new Criteria();
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $event = new ProductListingCriteriaEvent($request, $criteria, $salesChannelContext);

        $productStreamBuilder = $this->createMock(ProductStreamBuilder::class);
        $productStreamBuilder->expects(static::exactly(2))
            ->method('buildFilters')
            ->willReturnOnConsecutiveCalls([],[]);

        $subscriber = $this->getSubscriber($productStreamBuilder);
        $subscriber->addStreamFilters($event);
        static::assertCount(0, $criteria->getFilters());
    }

    /**
     * @dataProvider getTestAddStreamFiltersProviderData
     */
    public function testAddStreamFilters(
        Request $request
    ): void
    {
        $criteria = new Criteria();
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $event = new ProductListingCriteriaEvent($request, $criteria, $salesChannelContext);

        $productStreamBuilder = $this->createMock(ProductStreamBuilder::class);
        $filter = new EqualsFilter('test-field', 'test-value');
        $filter2 = new EqualsFilter('test-field-2', 'test-value-2');
        $filter3 = new EqualsFilter('test-field-3', 'test-value-3');
        $productStreamBuilder->expects(static::exactly(3))
            ->method('buildFilters')
            ->willReturnOnConsecutiveCalls([$filter], [$filter2], [$filter3]);

        $subscriber = $this->getSubscriber($productStreamBuilder);
        $subscriber->addStreamFilters($event);
        $filters = $criteria->getFilters();
        $expectFilter = new MultiFilter(MultiFilter::CONNECTION_OR, [$filter, $filter2, $filter3]);
        static::assertCount(1, $filters);
        static::assertEquals($expectFilter, $filters[0]);
    }

    public static function getTestAddStreamFiltersProviderData(): \Generator
    {
        $testRequestParams = ['streams' => 'test-product-id|test-product-id-2|test-product-id-3'];

        $getRequest = new Request($testRequestParams);
        yield 'with-GET-request' => [
            $getRequest
        ];

        $postRequest = new Request([], $testRequestParams);
        $postRequest->setMethod(Request::METHOD_POST);
        yield 'with-POST-request' => [
            $postRequest
        ];
    }

    private function getSubscriber(
        ?MockObject $productStreamBuilderMock = null
    ): ProductListingCollectProductStreamFilterSubscriber
    {
        /** @var MockObject&ProductStreamBuilder $productStreamBuilder */
        $productStreamBuilder = $productStreamBuilderMock ?? $this->createMock(ProductStreamBuilder::class);

        return new ProductListingCollectProductStreamFilterSubscriber($productStreamBuilder);
    }
}
