<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Product\Cms;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ProductListingStruct;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Product\Cms\ProductListingCmsElementResolver;
use Shopware\Core\Content\Product\Cms\ProductListingCmsElementResolver as ShopwareProductListingCmsElementResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSlotMockHelper;
use Symfony\Component\HttpFoundation\Request;

class ProductListingCmsElementResolverTest extends TestCase
{
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetType(): void
    {
        $resolver = $this->getResolver();
        static::assertEquals('product-listing', $resolver->getType());
    }

    public function testCollect(): void
    {
        $slot = $this->createCmsSlot();
        $resolverContext = new ResolverContext($this->salesChannelContext, new Request());

        $originalResolver = $this->createMock(ShopwareProductListingCmsElementResolver::class);
        $originalResolver->expects(static::once())
            ->method('collect')
            ->with(
                static::equalTo($slot),
                static::equalTo($resolverContext)
            );

        $resolver = $this->getResolver($originalResolver);
        $resolver->collect($slot, $resolverContext);
    }

    public function testEnrichWithResolverContextDoNotHasExtension(): void
    {
        $slot = $this->createCmsSlot();
        $resolverContext = new ResolverContext($this->salesChannelContext, new Request());
        $result = new ElementDataCollection();

        $originalResolver = $this->createMock(ShopwareProductListingCmsElementResolver::class);
        $originalResolver->expects(static::once())
            ->method('enrich')
            ->with(
                static::equalTo($slot),
                static::equalTo($resolverContext),
                static::equalTo($result)
            );

        $resolver = $this->getResolver($originalResolver);
        $resolver->enrich($slot, $resolverContext, $result);
    }

    /**
     * @dataProvider getTestEnrichWithResolverContextHasExtensionProviderData
     *
     * @param array<string, string>|null $expectPropertyWhiteList
     * @param array<string, string>|null $expectAvailableSortings
     * @return void
     */
    public function testEnrichWithResolverContextHasExtension(
        CmsSlotEntity $slot,
        Request $request,
        ?array $expectPropertyWhiteList,
        ?bool $expectManufactureFilter,
        ?bool $expectRatingWhiteList,
        ?array $expectAvailableSortings,
        ?string $expectOrder
    ): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $salesChannelContext->addArrayExtension('swagGuidedShoppingPresentation', ['test' => true]);
        $resolverContext = new ResolverContext($salesChannelContext, $request);
        $result = new ElementDataCollection();

        $originalResolver = $this->createMock(ShopwareProductListingCmsElementResolver::class);
        $originalResolver->expects(static::never())->method('enrich');

        $resolver = $this->getResolver($originalResolver);
        $resolver->enrich($slot, $resolverContext, $result);

        $slotData = $slot->getData();
        static::assertInstanceOf(ProductListingStruct::class, $slotData);
        $listing = $slotData->getListing();
        static::assertInstanceOf(EntitySearchResult::class, $listing);
        static::assertEquals('cms::product-listing', $listing->getCriteria()->getTitle());
        static::assertEquals(1, $listing->getEntities()->count());
        $resolverContextRequest = $resolverContext->getRequest();
        static::assertEquals($expectPropertyWhiteList, $resolverContextRequest->get('property-whitelist'));
        static::assertEquals($expectManufactureFilter, $resolverContextRequest->get('manufacturer-filter'));
        static::assertEquals($expectRatingWhiteList, $resolverContextRequest->get('rating-filter'));
        static::assertEquals($expectAvailableSortings, $resolverContextRequest->get('availableSortings'));
        static::assertEquals($expectOrder, $resolverContextRequest->get('order'));
    }

    public static function getTestEnrichWithResolverContextHasExtensionProviderData(): \Generator
    {
        $self = new ProductListingCmsElementResolverTest('test');

        yield 'only-has-property-whitelist' => [
            $self->createCmsSlot(true),
            $self->createRequest(),
            [
                'test-property-key' => 'test-property-value'
            ],
            null,
            null,
            null,
            null
        ];
        yield 'only-has-filters' => [
            $self->createCmsSlot(false, true),
            $self->createRequest(),
            null,
            false,
            false,
            null,
            null
        ];
        yield 'has-property-whitelist-and-filters' => [
            $self->createCmsSlot(true, true),
            $self->createRequest(),
            [
                'test-property-key' => 'test-property-value'
            ],
            false,
            false,
            null,
            null
        ];
        yield 'only-use-custom-sorting-but-no-available-sorting-in-both-slot-and-request' => [
            $self->createCmsSlot(false, false, true),
            $self->createRequest(),
            null,
            null,
            null,
            null,
            null
        ];
        yield 'only-use-custom-sorting-and-only-has-available-sorting-in-slot' => [
            $self->createCmsSlot(false, false, true, true),
            $self->createRequest(),
            null,
            null,
            null,
            [
                'test-sorting-key-in-slot' => 'asc',
            ],
            'test-sorting-key-in-slot'
        ];
        yield 'only-use-custom-sorting-and-has-order-in-request-and-only-has-default-sorting-in-slot' => [
            $self->createCmsSlot(false, false, true, false, true),
            $self->createRequest(true),
            null,
            null,
            null,
            null,
            'test-order-in-request'
        ];
        yield 'only-use-custom-sorting-and-do-not-has-order-in-request-and-has-default-sorting-in-slot' => [
            $self->createCmsSlot(false, false, true, false, true),
             $self->createRequest(),
            null,
            null,
            null,
            null,
            'test-default-sorting'
        ];
        yield 'only-use-custom-sorting-and-do-not-has-order-and-default-sorting-in-request-but-has-available-sorting-in-request' => [
            $self->createCmsSlot(false, false, true),
            $self->createRequest(false, true),
            null,
            null,
            null,
            [
                'test-sorting-key-in-request' => 'asc',
                'test-sorting-key-in-request-2' => 'desc',
            ],
            'test-sorting-key-in-request-2'
        ];
        yield 'only-use-custom-sorting-and-do-not-has-order-in-request-but-has-available-sorting-in-request-and-has-all-kind-of-sorting-in-slot' => [
            $self->createCmsSlot(false, false, true, true, true),
            $self->createRequest(false, true),
            null,
            null,
            null,
            [
                'test-sorting-key-in-slot' => 'asc',
            ],
            'test-default-sorting'
        ];
        yield 'has-all-but-do-not-has-order-in-request' => [
            $self->createCmsSlot(true, true, true, true, true),
            $self->createRequest(false, true),
            [
                'test-property-key' => 'test-property-value'
            ],
            false,
            false,
            [
                'test-sorting-key-in-slot' => 'asc',
            ],
            'test-default-sorting'
        ];
    }

    private function getResolver(?MockObject $originalResolverMock = null): ProductListingCmsElementResolver
    {
        /** @var MockObject&ShopwareProductListingCmsElementResolver $originalResolver */
        $originalResolver = $originalResolverMock ?? $this->createMock(ShopwareProductListingCmsElementResolver::class);
        return new ProductListingCmsElementResolver($originalResolver);
    }

    private function createCmsSlot(
        ?bool $hasPropertyWhitelist = false,
        ?bool $hasFilters = false,
        ?bool $useCustomSorting = false,
        ?bool $hasAvailableSortings = false,
        ?bool $hasDefaultSorting = false
    ): CmsSlotEntity
    {
        $slot = (new CmsSlotMockHelper())->getCmsSlotEntity(
            'test-cms-slot-id',
            'test-cms-block-id',
            [],
            new FieldConfigCollection()
        );

        $config = [];
        if ($hasPropertyWhitelist) {
            $config['propertyWhitelist'] = [
                'value' => [
                    'test-property-key' => 'test-property-value'
                ]
            ];
        }

        if ($hasFilters) {
            $config['filters'] = [
                'value' => 'manufacturer-filter, rating-filter, test-filter-value'
            ];
        }

        if ($useCustomSorting) {
            $translatedConfig = [
                'useCustomSorting' => [
                    'value' => true
                ]
            ];

            if ($hasAvailableSortings) {
                $translatedConfig['availableSortings'] = [
                    'value' => [
                        'test-sorting-key-in-slot' => 'asc',
                    ]
                ];
            }

            if ($hasDefaultSorting) {
                $translatedConfig['defaultSorting'] = [
                    'value' => 'test-default-sorting'
                ];
            }

            $slot->setTranslated([
                'config' => $translatedConfig
            ]);
        }

        $slot->setConfig($config);

        return $slot;
    }

    private function createRequest(
        ?bool $hasOrder = false,
        ?bool $hasAvailableSortings = false
    ): Request
    {
        $requestParams = [];

        if ($hasOrder) {
            $requestParams['order'] = 'test-order-in-request';
        }

        if ($hasAvailableSortings) {
            $requestParams['availableSortings'] = [
                'test-sorting-key-in-request' => 'asc',
                'test-sorting-key-in-request-2' => 'desc',
            ];
        }

        return new Request([], $requestParams);
    }
}
