<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Aggregate\CmsSectionTranslation;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationDefinition;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationEntity;

class CmsSectionTranslationEntityTest extends TestCase
{
    public function testSetCmsSectionId(): void
    {
        $entity = new CmsSectionTranslationEntity();
        $entity->setCmsSectionId('test-cms-section-id');
        static::assertEquals('test-cms-section-id', $entity->getCmsSectionId());
    }

    public function testSetName(): void
    {
        $entity = new CmsSectionTranslationEntity();
        $entity->setName('test-name');
        static::assertEquals('test-name', $entity->getName());
        $entity->setName(null);
        static::assertNull($entity->getName());
    }

    public function testSetCmsSection(): void
    {
        $entity = new CmsSectionTranslationEntity();
        $cmsSection = new CmsSectionEntity();
        $cmsSection->setId('test-cms-section-id');
        $entity->setCmsSection($cmsSection);
        static::assertSame($cmsSection, $entity->getCmsSection());
        $entity->setCmsSection(null);
        static::assertNull($entity->getCmsSection());
    }
}
