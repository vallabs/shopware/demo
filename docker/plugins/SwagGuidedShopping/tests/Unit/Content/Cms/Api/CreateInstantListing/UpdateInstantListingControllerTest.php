<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Api\CreateInstantListing;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Cms\Api\CreateInstantListing\UpdateInstantListingController;
use SwagGuidedShopping\Content\Cms\Service\InstantListingRequestHandler;
use Symfony\Component\HttpFoundation\Request;

class UpdateInstantListingControllerTest extends TestCase
{
    protected Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testUpdateInstantListingWithAllThingsIsValid(): void
    {
        $instantListingRequestHandler = $this->createMock(InstantListingRequestHandler::class);
        $instantListingRequestHandler->expects(static::once())
            ->method('handleUpdateInstantListingRequest')
            ->willReturn([
                'key' => 'value'
            ]);

        $controller = new UpdateInstantListingController($instantListingRequestHandler);

        $response = $controller->update('1', $this->context, new Request());
        static::assertEquals(200, $response->getStatusCode());

        /** @var string $content */
        $content = $response->getContent();
        $updatedData = \json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        static::assertSame(['key' => 'value'], $updatedData);
    }
}
