<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Api\CreateInstantListing;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Cms\Api\CreateInstantListing\CreateInstantListingController;
use SwagGuidedShopping\Content\Cms\Service\InstantListingRequestHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CreateInstantListingControllerTest extends TestCase
{
    protected Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testCreateInstantListingWithValidAllAndRequestValidAlso(): void
    {
        $instantListingHandler = $this->createMock(InstantListingRequestHandler::class);
        $instantListingHandler->expects(static::once())
            ->method('handleCreateInstantListingRequest')
            ->willReturn(3);

        $controller = new CreateInstantListingController($instantListingHandler);

        $response = $controller->create('test-appointment-id', $this->context, new Request());
        static::assertSame(201, $response->getStatusCode());

        /** @var string  $content */
        $content = $response->getContent();
        $data= \json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        static::assertArrayHasKey('index', $data);
        static::assertEquals(3, $data['index']);
    }
}
