<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Service;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotCollection;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsBlockMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSlotMockHelper;

/**
 * @internal
 */
class NotesElementHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestRemoveNotesProviderData
     * @param array<int, string> $notesContents
     */
    public function testNotesSection(
        CmsSectionEntity $section,
        int $notesCount,
        array $notesContents
    ): void
    {
        $handler = new NotesElementHandler();
        $notes = $handler->removeNotes($section);
        static::assertCount($notesCount, $notes);
        foreach ($notes as $key => $note) {
            static::assertEquals($notesContents[$key], $note->getTranslated()['config']['content']);
        }
    }

    public static function getTestRemoveNotesProviderData(): \Generator
    {
        $cmsSectionMockHelper = new CmsSectionMockHelper();
        $cmsBlockMockHelper = new CmsBlockMockHelper();
        $cmsSlotMockHelper = new CmsSlotMockHelper();

        $sectionWithoutBlocks = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-without-blocks-id',
            'test-cms-page-id',
            0,
            null
        );
        yield 'section without blocks' => [
            $sectionWithoutBlocks,
            0,
            []
        ];

        $sectionWithEmptyBlock = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-with-empty-block-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection()
        );
        yield 'section with empty block' => [
            $sectionWithEmptyBlock,
            0,
            []
        ];

        $blockDoNotHaveSlots = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-do-not-have-slots-id',
            'test-cms-section-with-block-do-not-have-slots-id',
            0,
            null
        );
        $sectionWithBlockDoNotHaveSlots = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-with-block-do-not-have-slots-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$blockDoNotHaveSlots])
        );
        yield 'section with block but do not have slots' => [
            $sectionWithBlockDoNotHaveSlots,
            0,
            []
        ];

        $blockHaveEmptySlot = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-have-empty-slot-id',
            'test-cms-section-with-block-have-empty-slot-id',
            0,
            new CmsSlotCollection()
        );
        $sectionWithBlockHaveEmptySlot = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-with-block-have-empty-slot-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$blockHaveEmptySlot])
        );
        yield 'section with block have empty slot' => [
            $sectionWithBlockHaveEmptySlot,
            0,
            []
        ];

        $notNotesSlot = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-cms-not-notes-slot-id',
            'test-cms-block-id',
            [],
            new FieldConfigCollection()
        );
        $blockDoNotHaveNotesSlot = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-with-block-but-no-notes-slot-id',
            0,
            new CmsSlotCollection([$notNotesSlot])
        );
        $sectionWithBlockDoNotHaveNotesSlot = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-with-block-but-no-notes-slot-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$blockDoNotHaveNotesSlot])
        );
        yield 'section with block but do not have notes slot' => [
            $sectionWithBlockDoNotHaveNotesSlot,
            0,
            []
        ];

        $notesSlot = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-cms-notes-slot-id',
            'test-cms-block-id',
            ['content' => 'This is a note'],
            new FieldConfigCollection()
        );
        $notesSlot->setType('notes');

        $notesSlot2 = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-cms-notes-slot-id-2',
            'test-cms-block-id',
            ['content' => 'This is a note 2'],
            new FieldConfigCollection()
        );
        $notesSlot2->setType('notes');

        $blockHaveOneNotesSlot = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-with-block-have-one-notes-slot-id',
            0,
            new CmsSlotCollection([$notesSlot])
        );

        $blockHaveTwoNotesSlot = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-with-block-have-two-notes-slot-id',
            0,
            new CmsSlotCollection([$notesSlot, $notesSlot2])
        );

        $sectionWithBlockHaveOneNotesSlot = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-with-block-have-one-notes-slot-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$blockHaveOneNotesSlot])
        );
        yield 'section with block have one notes slot' => [
            $sectionWithBlockHaveOneNotesSlot,
            1,
            ['This is a note']
        ];

        $sectionWithBlockHaveTwoNotesSlot = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-with-block-have-two-notes-slot-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$blockHaveTwoNotesSlot])
        );
        yield 'section with block have two notes slot' => [
            $sectionWithBlockHaveTwoNotesSlot,
            2,
            ['This is a note', 'This is a note 2']
        ];
    }

    public function testEntityCustomFields(): void
    {
        $sectionId = 'test-section-id';
        $blockId = 'test-block-id';
        $slot = (new CmsSlotMockHelper())->getCmsSlotEntity('test-slot-id', $blockId, [], new FieldConfigCollection());
        $slot2 = (new CmsSlotMockHelper())->getCmsSlotEntity('test-slot-id-2', $blockId, [], new FieldConfigCollection());
        $block = (new CmsBlockMockHelper())->getCmsBlockEntity($blockId, $sectionId, 0, new CmsSlotCollection([$slot, $slot2]));
        $section = (new CmsSectionMockHelper())->getCmsSectionEntity($sectionId, 'test-page-id', 0, new CmsBlockCollection([$block]));

        $handler = new NotesElementHandler();
        $handler->removeNotes($section);

        static::assertEquals($sectionId, $section->getCustomFieldsValue('_uniqueIdentifier'));
        static::assertEquals($blockId, $block->getCustomFieldsValue('_uniqueIdentifier'));
        static::assertEquals('test-slot-id', $slot->getCustomFieldsValue('_uniqueIdentifier'));
        static::assertEquals('test-slot-id-2', $slot2->getCustomFieldsValue('_uniqueIdentifier'));
    }
}
