<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Service;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\AggregationResultCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\Metric\MaxResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use SwagGuidedShopping\Content\Cms\Service\CmsPageCreatorService;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;

class CmsPageCreatorServiceTest extends TestCase
{
    /**
     * @param array<int, string> $productIds
     * @dataProvider getTestCreatePresentationCmsPageProviderData
     */
    public function testCreatePresentationPage(
        string $layoutId,
        array $productIds,
        string $presentationId,
        string $presentationVersionId,
        int $position,
        bool $isInstantListing,
        ?string $pageName
    ): void
    {
        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPageRepository->expects(static::once())->method('search');
        $presentationCmsPageRepository->expects(static::exactly(2))->method('upsert');

        $service = new CmsPageCreatorService($presentationCmsPageRepository);

        $service->createPresentationPage(
            $layoutId,
            $productIds,
            $presentationId,
            $presentationVersionId,
            $position,
            $isInstantListing,
            $pageName,
            new Context(new SystemSource())
        );
    }


    public static function getTestCreatePresentationCmsPageProviderData(): \Generator
    {
        yield 'normal page with position is 0 and no page name' => [
            'test-layout-id',
            [],
            'test-presentation-id',
            'test-presentation-version-id',
            0,
            false,
            null
        ];

        yield 'normal page with position is 1 and no page name' => [
            'test-layout-id',
            ['test-product-id'],
            'test-presentation-id',
            'test-presentation-version-id',
            1,
            false,
            null
        ];

        yield 'instant listing page with position is 0 and no page name' => [
            'test-layout-id',
            [],
            'test-presentation-id',
            'test-presentation-version-id',
            0,
            true,
            null
        ];

        yield 'instant listing page with position is 1 and no page name' => [
            'test-layout-id',
            [],
            'test-presentation-id',
            'test-presentation-version-id',
            1,
            true,
            null
        ];

        yield 'normal page with position is 1 and page name' => [
            'test-layout-id',
            ['test-product-id'],
            'test-presentation-id',
            'test-presentation-version-id',
            1,
            false,
            'test-page-name'
        ];

        yield 'instant listing page with position is 1 and page name' => [
            'test-layout-id',
            [],
            'test-presentation-id',
            'test-presentation-version-id',
            1,
            true,
            'test-page-name'
        ];
    }

    /**
     * @param array<int, string>|null $addProductIds
     * @param array<int, string>|null $removeProductIds
     * @param array<int, string> $expectedPickedProductIds
     * @dataProvider getTestUpdatePresentationPageProviderData
     */
    public function testUpdatePresentationPage(
        ?array $addProductIds,
        ?array $removeProductIds,
        array $expectedPickedProductIds,
        string $presentationPageId,
        ?string $pageName
    ): void
    {
        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationCmsPageEntity::class,
                1,
                new PresentationCmsPageCollection([
                    (new PresentationCmsPageEntity())->assign([
                        'id' => $presentationPageId,
                        'pickedProductIds' => ['test-picked-product-id'],
                    ])
                ]),
                null,
                new Criteria([$presentationPageId]),
                new Context(new SystemSource())
            ));

        $expectedUpdatedData = [
            'id' => $presentationPageId,
            'pickedProductIds' => \array_values($expectedPickedProductIds),
            'title' => $pageName
        ];

        $presentationCmsPageRepository->expects(static::once())
            ->method('upsert')
            ->with(static::equalTo([
                [
                    'id' => $presentationPageId,
                    'pickedProductIds' => \array_values($expectedPickedProductIds),
                    'title' => $pageName
                ]
            ]));

        $service = new CmsPageCreatorService($presentationCmsPageRepository);

        $updatedData = $service->updatePresentationPage(
            $addProductIds ?? [],
            $removeProductIds ?? [],
            $presentationPageId,
            $pageName,
            new Context(new SystemSource())
        );

        static::assertSame($expectedUpdatedData, $updatedData);
    }

    public static function getTestUpdatePresentationPageProviderData(): \Generator
    {
        yield 'update nothing' => [
            null,
            null,
            ['test-picked-product-id'],
            'test-presentation-page-id',
            null
        ];

        yield 'add product id' => [
            ['test-add-product-id'],
            null,
            ['test-add-product-id', 'test-picked-product-id'],
            'test-presentation-page-id',
            null
        ];

        yield 'remove product id' => [
            null,
            ['test-remove-product-id'],
            ['test-picked-product-id'],
            'test-presentation-page-id',
            null
        ];

        yield 'add and remove product id' => [
            ['test-add-product-id'],
            ['test-remove-product-id'],
            ['test-add-product-id', 'test-picked-product-id'],
            'test-presentation-page-id',
            null
        ];

        yield 'add and remove product id, but has duplicate add product id' => [
            ['test-add-product-id', 'test-add-product-id'],
            ['test-remove-product-id'],
            ['test-add-product-id', 'test-picked-product-id'],
            'test-presentation-page-id',
            null
        ];

        yield 'update page name' => [
            null,
            null,
            ['test-picked-product-id'],
            'test-presentation-page-id',
            'test-page-name'
        ];

        yield 'add product id and update page name' => [
            ['test-add-product-id'],
            null,
            ['test-add-product-id', 'test-picked-product-id'],
            'test-presentation-page-id',
            'test-page-name'
        ];

        yield 'remove product id and update page name' => [
            null,
            ['test-remove-product-id'],
            ['test-picked-product-id'],
            'test-presentation-page-id',
            'test-page-name'
        ];

        yield 'add and remove product id and update page name' => [
            ['test-add-product-id'],
            ['test-remove-product-id'],
            ['test-add-product-id', 'test-picked-product-id'],
            'test-presentation-page-id',
            'test-page-name'
        ];

        yield 'add and remove product id, but has duplicate add product id and update page name' => [
            ['test-add-product-id', 'test-add-product-id'],
            ['test-remove-product-id'],
            ['test-add-product-id', 'test-picked-product-id'],
            'test-presentation-page-id',
            'test-page-name'
        ];
    }

    public function testGetPositionForSlideWithException(): void
    {
        $context = new Context(new SystemSource());

        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $searchResult = new EntitySearchResult(
            CmsPageEntity::class,
            0,
            new CmsPageCollection([]),
            null,
            new Criteria(),
            $context
        );
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn($searchResult);

        $service = new CmsPageCreatorService($presentationCmsPageRepository);

        static::expectException(\Exception::class);

        $service->getPositionForSlide('test-presentation-cms-page-id', $context);
    }

    public function testGetPositionForSlideWithValidPresentationCmsPage(): void
    {
        $context = new Context(new SystemSource());
        $presentationCmsPageId = 'test-presentation-cms-page-id';
        $position = 13;

        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId($presentationCmsPageId);
        $presentationCmsPage->setPosition($position);
        $searchResult = new EntitySearchResult(
            PresentationCmsPageEntity::class,
            1,
            new PresentationCmsPageCollection([$presentationCmsPage]),
            null,
            new Criteria(),
            $context
        );
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn($searchResult);

        $service = new CmsPageCreatorService($presentationCmsPageRepository);

        $result = $service->getPositionForSlide($presentationCmsPageId, $context);

        static::assertEquals($position, $result);
    }

    public function testGetPositionForLastSlideWithAggregationDoNotHaveMaxPosition(): void
    {
        $context = new Context(new SystemSource());
        $presentationCmsPageId = 'test-presentation-cms-page-id';

        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $aggregations = new AggregationResultCollection();
        $searchResult = new EntitySearchResult(
            PresentationCmsPageEntity::class,
            1,
            new PresentationCmsPageCollection(),
            $aggregations,
            new Criteria(),
            $context
        );
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn($searchResult);

        $service = new CmsPageCreatorService($presentationCmsPageRepository);

        $result = $service->getPositionForLastSlide($presentationCmsPageId, 'test-presentation-version-id', $context);

        static::assertEquals(0, $result);
    }

    /**
     * @dataProvider getTestGetPositionForLastSlideWithAggregationHaveMaxPositionProviderData
     */
    public function testGetPositionForLastSlideWithAggregationHaveMaxPosition(
        ?int $maxPosition,
        int $expectResult
    ): void
    {
        $context = new Context(new SystemSource());
        $presentationCmsPageId = 'test-presentation-cms-page-id';

        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $maxResult = new MaxResult('maxPosition', $maxPosition);
        $aggregations = new AggregationResultCollection();
        $aggregations->add($maxResult);
        $searchResult = new EntitySearchResult(
            PresentationCmsPageEntity::class,
            1,
            new PresentationCmsPageCollection(),
            $aggregations,
            new Criteria(),
            $context
        );
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn($searchResult);

        $service = new CmsPageCreatorService($presentationCmsPageRepository);

        $result = $service->getPositionForLastSlide($presentationCmsPageId, 'test-presentation-version-id', $context);

        static::assertEquals($expectResult, $result);
    }

    public static function getTestGetPositionForLastSlideWithAggregationHaveMaxPositionProviderData(): \Generator
    {
        yield 'max-position-is-null' => [null, 0];
        yield 'max-position-is-integer' => [13, 13];
    }

    public function testUpdateSlidePositions(): void
    {
        $context = new Context(new SystemSource());
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationId', 'test-presentation-id'));
        $criteria->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', 'test-presentation-version-id'));
        $criteria->addSorting(new FieldSorting('position'));
        $criteria->addAssociation('cmsPage.sections');

        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $presentationCmsPage->setCmsPageId($cmsPage->getId());
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setInstantListing(false);
        $presentationCmsPage->setGuidedShoppingPresentationVersionId('test-presentation-version-id');
        $presentationCmsPage->setPosition(2);
        $presentationCmsPage->setPresentationId('test-presentation-id');

        $presentationCmsPage2 = clone $presentationCmsPage;
        $cmsPage2 = clone $cmsPage;
        $section = (new CmsSectionMockHelper())->getCmsSectionEntity(
            'test-cms-section-id',
            $cmsPage2->getId(),
            1,
            null
        );
        $section2 = clone $section;
        $section2->assign([
            'id' => 'test-cms-section-id-2',
            'position' => 2
        ]);
        $cmsPage2->setSections(new CmsSectionCollection([$section, $section2]));
        $presentationCmsPage2->assign([
            'id' => 'test-presentation-cms-page-id-2',
            'position' => 1,
            'cmsPage' => $cmsPage2
        ]);

        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new EntitySearchResult(
                PresentationCmsPageEntity::class,
                1,
                new PresentationCmsPageCollection([$presentationCmsPage, $presentationCmsPage2]),
                null,
                $criteria,
                $context
            ));

        $presentationCmsPageRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::equalTo([
                    [
                        'id' => 'test-presentation-cms-page-id',
                        'position' => 3
                    ]
                ]),
                static::equalTo($context)
            );

        $cmsPageCreator = new CmsPageCreatorService($presentationCmsPageRepository);

        $result = $cmsPageCreator->updateSlidePositions(
            2,
            'test-presentation-id',
            'test-presentation-version-id',
            $context
        );

        static::assertEquals(3, $result);
    }
}
