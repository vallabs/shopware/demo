<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Service;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\DataResolver\CmsSlotsDataResolver;
use Shopware\Core\Content\Cms\DataResolver\FieldConfig;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Cms\Service\PresentationCmsPageLoader;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsBlockMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSlotMockHelper;
use Symfony\Component\HttpFoundation\Request;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

/**
 * @internal
 */
class PresentationCmsPageLoaderTest extends TestCase
{
    private CmsPageMockHelper $cmsPageMockHelper;
    private CmsSectionMockHelper $cmsSectionMockHelper;
    private CmsBlockMockHelper $cmsBlockMockHelper;
    private CmsSlotMockHelper $cmsSlotMockHelper;
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->cmsPageMockHelper = new CmsPageMockHelper();
        $this->cmsSectionMockHelper = new CmsSectionMockHelper();
        $this->cmsBlockMockHelper = new CmsBlockMockHelper();
        $this->cmsSlotMockHelper = new CmsSlotMockHelper();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testLoadPageWithoutSection(): void
    {
        $cmsPageId = Uuid::randomHex();
        $cmsPage = $this->cmsPageMockHelper->getCmsPageEntity($cmsPageId);
        $cmsPageSearchResult = $this->cmsPageMockHelper->createSearchResult([$cmsPage]);
        $salesChannelCmsPageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $salesChannelCmsPageLoader->expects(static::once())
            ->method('load')
            ->willReturn($cmsPageSearchResult);

        $slotDataResolver = $this->createMock(CmsSlotsDataResolver::class);
        $slotDataResolver->expects(static::never())->method('resolve');

        $cmsPageLoader = new PresentationCmsPageLoader(
            $salesChannelCmsPageLoader,
            $slotDataResolver
        );

        $criteria = new Criteria();
        $request = new Request();
        $config = [];

        $pages = $cmsPageLoader->load(
            $request,
            $criteria,
            $this->salesChannelContext,
            $config
        );

        static::assertEquals(1, $pages->getTotal());
        /** @var CmsPageEntity $page */
        $page = $pages->first();
        $sections = $page->getSections();
        static::assertNull($sections);
    }

    /**
     * @param array<string, mixed> $data
     *
     * @dataProvider dataProvider
     */
    public function testLoadPageWithCmsPageHasSections(array $data): void
    {
        $cmsPageId = \md5('testCmsPage');
        $cmsSectionId = \md5('testCmsSection');
        $cmsSectionId2 = \md5('testCmsSection2');
        $cmsBlockId = \md5('testCmsBlock');
        $cmsBlockId2 = \md5('testCmsBlock2');
        $cmsSlotId = \md5('testCmsSlot');
        $cmsSlotId2 = \md5('testCmsSlot2');
        $cmsSlotId3 = \md5('testCmsSlot3');
        $config = $data['config'];
        $fieldConfigCollection = $data['fieldConfig'];

        $cmsSlot = $this->cmsSlotMockHelper->getCmsSlotEntity($cmsSlotId, $cmsBlockId, $config[$cmsSlotId], $fieldConfigCollection[$cmsSlotId]);
        $cmsSlot2 = $this->cmsSlotMockHelper->getCmsSlotEntity($cmsSlotId2, $cmsBlockId, $config[$cmsSlotId2], $fieldConfigCollection[$cmsSlotId2]);
        $cmsSlot3 = $this->cmsSlotMockHelper->getCmsSlotEntity($cmsSlotId3, $cmsBlockId, $config[$cmsSlotId3], $fieldConfigCollection[$cmsSlotId3]);
        $cmsBlock = $this->cmsBlockMockHelper->getCmsBlockEntity($cmsBlockId, $cmsSectionId,0, new CmsSlotCollection([$cmsSlot, $cmsSlot2]));
        $cmsBlock2 = $this->cmsBlockMockHelper->getCmsBlockEntity($cmsBlockId2, $cmsSectionId,1, new CmsSlotCollection([$cmsSlot, $cmsSlot3]));
        $cmsSection = $this->cmsSectionMockHelper->getCmsSectionEntity($cmsSectionId, $cmsPageId, 0, new CmsBlockCollection([$cmsBlock, $cmsBlock2]));
        $cmsSection2 = $this->cmsSectionMockHelper->getCmsSectionEntity($cmsSectionId2, $cmsPageId, 0, new CmsBlockCollection([$cmsBlock]));
        $cmsPage = $this->cmsPageMockHelper->getCmsPageEntity($cmsPageId);
        $cmsSections = new CmsSectionCollection([$cmsSection, $cmsSection2]);
        $cmsPage->setSections($cmsSections);

        $cmsPageSearchResult = $this->cmsPageMockHelper->createSearchResult([$cmsPage]);
        $salesChannelCmsPageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $salesChannelCmsPageLoader->expects(static::once())
            ->method('load')
            ->willReturn($cmsPageSearchResult);

        $slotDataResolver = $this->createMock(CmsSlotsDataResolver::class);
        $slotDataResolver->expects(static::once())
            ->method('resolve')
            ->willReturn($cmsSections->getBlocks()->getSlots());
        $cmsPageLoader = new PresentationCmsPageLoader(
            $salesChannelCmsPageLoader,
            $slotDataResolver
        );

        $criteria = new Criteria();
        $request = new Request();
        $overwrittenConfig = $data['overwrittenConfig'];

        $pages = $cmsPageLoader->load(
            $request,
            $criteria,
            $this->salesChannelContext,
            $overwrittenConfig
        );

        static::assertEquals(1, $pages->getTotal());

        /** @var CmsPageEntity $page */
        $page = $pages->first();
        /** @var CmsSectionCollection $sections */
        $sections = $page->getSections();
        static::assertEquals(2, $sections->count());
        $sectionsArr = $sections->getElements();
        static::assertTrue(\array_key_exists($cmsSectionId, $sectionsArr));
        static::assertTrue(\array_key_exists($cmsSectionId, $sectionsArr));

        $blocks = $sections->getBlocks();
        static::assertEquals(2, $blocks->count());
        $blocksArr = $blocks->getElements();
        static::assertTrue(\array_key_exists($cmsBlockId, $blocksArr));
        static::assertTrue(\array_key_exists($cmsBlockId2, $blocksArr));

        $slots = $blocks->getSlots();
        static::assertEquals(3, $slots->count());
        $slotsArr = $slots->getElements();
        static::assertTrue(\array_key_exists($cmsSlotId, $slotsArr));
        static::assertTrue(\array_key_exists($cmsSlotId2, $slotsArr));
        static::assertTrue(\array_key_exists($cmsSlotId3, $slotsArr));

        /** @var CmsSlotEntity $slot */
        foreach ($slots as $slot) {
            if (isset($overwrittenConfig[$slot->getId()]) && $overwrittenConfig[$slot->getId()]) {
                static::assertNotEquals($config[$slot->getId()], $slot->getConfig());
                static::assertEquals($overwrittenConfig[$slot->getId()], $slot->getConfig());
                static::assertNotEquals($config[$slot->getId()], $slot->getTranslated()['config']);
                static::assertEquals($overwrittenConfig[$slot->getId()], $slot->getTranslated()['config']);
                static::assertNotEquals($fieldConfigCollection[$slot->getId()], $slot->getFieldConfig());
            } else {
                static::assertEquals($config[$slot->getId()], $slot->getConfig());
                static::assertEquals($config[$slot->getId()], $slot->getTranslated()['config']);
                static::assertEquals($fieldConfigCollection[$slot->getId()], $slot->getFieldConfig());
            }
        }
    }

    public static function dataProvider(): \Generator
    {
        yield 'no override config' => [
            [
                'config' => [
                    \md5('testCmsSlot') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial'
                        ]
                    ],
                    \md5('testCmsSlot2') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial2'
                        ]
                    ],
                    \md5('testCmsSlot3') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial3'
                        ]
                    ],
                ],
                'fieldConfig' => [
                    \md5('testCmsSlot') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial')]),
                    \md5('testCmsSlot2') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial2')]),
                    \md5('testCmsSlot3') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial3')]),
                ],
                'overwrittenConfig' => []
            ]
        ];

        yield 'no override config 2' => [
            [
                'config' => [
                    \md5('testCmsSlot') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial'
                        ]
                    ],
                    \md5('testCmsSlot2') => null,
                    \md5('testCmsSlot3') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial3'
                        ]
                    ],
                ],
                'fieldConfig' => [
                    \md5('testCmsSlot') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial')]),
                    \md5('testCmsSlot2') => new FieldConfigCollection(),
                    \md5('testCmsSlot3') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial3')]),
                ],
                'overwrittenConfig' => []
            ]
        ];

        yield 'override config' => [
            [
                'config' => [
                    \md5('testCmsSlot') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial'
                        ]
                    ],
                    \md5('testCmsSlot2') => null,
                    \md5('testCmsSlot3') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'initial3'
                        ]
                    ],
                ],
                'fieldConfig' => [
                    \md5('testCmsSlot') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial')]),
                    \md5('testCmsSlot2') => new FieldConfigCollection(),
                    \md5('testCmsSlot3') => new FieldConfigCollection([new FieldConfig('content', 'static', 'initial3')]),
                ],
                'overwrittenConfig' => [
                    \md5('testCmsSlot2') => [
                        'content' => [
                            'source' => 'static',
                            'value' => 'overwritten2'
                        ]
                    ],
                ]
            ],
        ];
    }
}
