<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsSelfModeException;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Cms\Service\CmsPageCreatorService;
use SwagGuidedShopping\Content\Cms\Service\InstantListingRequestHandler;
use SwagGuidedShopping\Content\Exception\InvalidParameterException;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Exception\NotConfiguredException;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\Validator\Validation;

/**
 * @internal
 */
class InstantListingRequestHandlerTest extends TestCase
{
    private Context $context;
    /** @var array<string, mixed> $createRequest */
    private array $createRequest;
    /** @var array<string, mixed> $updateRequest */
    private array $updateRequest;


    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
        $this->createRequest = [
            'currentPageGroupId' => 'test-page-group-id',
            'productIds' => ['test-product-id', 'test-product-id-2'],
            'pageName' => 'test-page-name'
        ];
        $this->updateRequest = [
            'currentPageGroupId' => 'test-page-group-id',
            'addProductIds' => ['test-product-id-3'],
            'removeProductIds' => ['test-product-id-2']
        ];
    }

    /**
     * @dataProvider  getHandleCreateInstantListingRequestWithValidCurrentPageGroupIdProviderData
     */
    public function testHandleCreateInstantListingRequestSuccessfullyWithValidCurrentPageGroupId(
        string $pageId,
        int $pagePosition,
        ?string $presentationVersion = null,
        ?string $pageName = null
    ): void
    {
        $request = [
            'currentPageGroupId' => $pageId,
            'productIds' => ['test-product-id', 'test-product-id-2'],
            'pageName' => $pageName
        ];
        $appointment = $this->getAppointment($presentationVersion);
        $context = new Context(new SystemSource());

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->with(
                static::equalTo($appointment->getId()),
                static::equalTo($context)
            )
            ->willReturn($appointment);

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())
            ->method('createPresentationVersion')
            ->with(
                static::equalTo($appointment),
                static::equalTo($context)
            )
            ->willReturn('test-new-version-id');
        $expectContext = new Context(
            new SystemSource(),
            $context->getRuleIds(),
            $context->getCurrencyId(),
            $context->getLanguageIdChain(),
            'test-new-version-id',
            $context->getCurrencyFactor(),
            $context->considerInheritance(),
            $context->getTaxState(),
            $context->getRounding()
        );

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(
                static::equalTo(ConfigKeys::PRODUCT_LISTING_DEFAULT_PAGE_ID)
            )
            ->willReturn('test-listing-layout-id');

        $cmsPageCreator = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreator->expects(static::once())
            ->method('getPositionForSlide')
            ->with(
                static::equalTo($request['currentPageGroupId']),
                static::equalTo($expectContext)
            )
            ->willReturn($pagePosition);

        $presentationVersionHandler->expects(static::once())
            ->method('getContextForAppointment')
            ->with(
                static::equalTo('test-new-version-id'),
                static::equalTo($context)
            )
            ->willReturn($expectContext);

        $cmsPageCreator->expects(static::once())
            ->method('createPresentationPage')
            ->with(
                static::equalTo('test-listing-layout-id'),
                static::equalTo($request['productIds']),
                static::equalTo($appointment->getPresentationId()),
                static::equalTo('test-new-version-id'),
                static::equalTo($pagePosition + 1),
                static::equalTo(true),
                static::equalTo($pageName),
                static::equalTo($context)
            )->willReturn(1);


        $handler = $this->getHandler(
            $cmsPageCreator,
            $systemConfigService,
            $appointmentService,
            $presentationVersionHandler
        );

        $result = $handler->handleCreateInstantListingRequest($appointment->getId(), $request, $context);

        static::assertEquals(1, $result);
    }

    public static function getHandleCreateInstantListingRequestWithValidCurrentPageGroupIdProviderData(): \Generator
    {
        yield 'has-version-already-with-page-name' => [
            'test-page-group-id',
            1,
            'test-presentation-version-id',
            'test-page-name'
        ];

        yield 'has-version-already-without-page-name' => [
            'test-page-group-id',
            1,
            'test-presentation-version-id',
            null
        ];

        yield 'has-no-version-with-page-name' => [
            'test-page-group-id',
            1,
            null,
            'test-page-name'
        ];

        yield 'has-default-version-with-page-name' => [
            'test-page-group-id',
            1,
            Defaults::LIVE_VERSION,
            'test-page-name'
        ];
    }

    public function testHandleCreateInstantListingRequestSuccessfullyWithInvalidCurrentPageGroupId(): void
    {
        $request = [
            'currentPageGroupId' => '',
            'productIds' => ['test-product-id', 'test-product-id-2'],
            'pageName' => 'test-page-name'
        ];
        $appointment = $this->getAppointment('test-presentation-version-id');
        $context = new Context(new SystemSource());

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->with(
                static::equalTo($appointment->getId()),
                static::equalTo($context)
            )
            ->willReturn($appointment);

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())
            ->method('createPresentationVersion')
            ->with(
                static::equalTo($appointment),
                static::equalTo($context)
            )
            ->willReturn('test-new-version-id');

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(
                static::equalTo(ConfigKeys::PRODUCT_LISTING_DEFAULT_PAGE_ID)
            )
            ->willReturn('test-listing-layout-id');

        $cmsPageCreator = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreator->expects(static::never())->method('getPositionForSlide');
        $presentationVersionHandler->expects(static::never())->method('getContextForAppointment');

        $cmsPageCreator->expects(static::once())
            ->method('createPresentationPage')
            ->with(
                static::equalTo('test-listing-layout-id'),
                static::equalTo($request['productIds']),
                static::equalTo($appointment->getPresentationId()),
                static::equalTo('test-new-version-id'),
                static::equalTo(1),
                static::equalTo(true),
                static::equalTo('test-page-name'),
                static::equalTo($context)
            )->willReturn(2);

        $handler = $this->getHandler(
            $cmsPageCreator,
            $systemConfigService,
            $appointmentService,
            $presentationVersionHandler
        );

        $result = $handler->handleCreateInstantListingRequest($appointment->getId(), $request, $context);

        static::assertEquals(2, $result);
    }

    public function testCreateInstantListingRequestButCanNotFindAppointment(): void
    {
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willThrowException(new EntityNotFoundException(AppointmentEntity::class, 'test-appointment-id'));

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::never())->method('createPresentationVersion');
        $presentationVersionHandler->expects(static::never())->method('getContextForAppointment');

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::never())->method('getString');

        $cmsPageCreator = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreator->expects(static::never())->method('getPositionForSlide');
        $cmsPageCreator->expects(static::never())->method('createPresentationPage');

        $handler = $this->getHandler(
            $cmsPageCreator,
            $systemConfigService,
            $appointmentService,
            $presentationVersionHandler
        );

        static::expectException(EntityNotFoundException::class);
        $handler->handleCreateInstantListingRequest('test-appointment-id', $this->createRequest, $this->context);
    }

    public function testCreateInstantListingRequestCanFindAppointmentButInvalidAppointment(): void
    {
        $appointmentIsSelfMode = $this->getAppointment();
        $appointmentIsSelfMode->setMode(AppointmentDefinition::MODE_SELF);

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointmentIsSelfMode);

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::never())->method('createPresentationVersion');
        $presentationVersionHandler->expects(static::never())->method('getContextForAppointment');

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::never())->method('getString');

        $cmsPageCreator = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreator->expects(static::never())->method('getPositionForSlide');
        $cmsPageCreator->expects(static::never())->method('createPresentationPage');

        $handler = $this->getHandler(
            $cmsPageCreator,
            $systemConfigService,
            $appointmentService,
            $presentationVersionHandler
        );

        $createRequest = [
            'currentPageGroupId' => 'test-page-group-id',
            'productIds' => ['test-product-id', 'test-product-id-2'],
            'pageName' => 'test-page-name'
        ];

        static::expectException(AppointmentIsSelfModeException::class);
        $handler->handleCreateInstantListingRequest('test-appointment-id', $createRequest, new Context(new SystemSource()));
    }

    /**
     * @dataProvider getTestCreateInstantListingRequestButUseInvalidRequestProviderData
     * @param array<string, mixed> $request
     */
    public function testCreateInstantListingRequestButUseInvalidRequest(
        array $request
    ): void
    {
        $appointment = $this->getAppointment();
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');
        $presentationVersionHandler->expects(static::never())->method('getContextForAppointment');

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::never())->method('getString');

        $cmsPageCreator = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreator->expects(static::never())->method('getPositionForSlide');
        $cmsPageCreator->expects(static::never())->method('createPresentationPage');

        $handler = $this->getHandler(
            $cmsPageCreator,
            $systemConfigService,
            $appointmentService,
            $presentationVersionHandler
        );

        static::expectException(InvalidParameterException::class);
        $handler->handleCreateInstantListingRequest('test-appointment-id', $request, $this->context);
    }

    public static function getTestCreateInstantListingRequestButUseInvalidRequestProviderData(): \Generator
    {
        yield [[]];
        yield [['productIds' => 'test-value']];
        yield [['productIds' => ['test-value']]];
        yield [['currentPageGroupId' => []]];
        yield [['currentPageGroupId' => '']];
        yield [['currentPageGroupId' => 'test-value']];
        yield [['productIds' => 'test-value', 'currenPageGroupId' => 'test-value']];
        yield [['productIds' => ['test-value'], 'currenPageGroupId' => []]];
        yield [['productIds' => ['test-value'], 'currenPageGroupId' => '1']];
    }

    public function testCreateInstantListingRequestButCanNotFindInstantListingLayoutId(): void
    {
        $appointment = $this->getAppointment();
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');
        $presentationVersionHandler->expects(static::never())->method('getContextForAppointment');

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->willReturn('');

        $cmsPageCreator = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreator->expects(static::never())->method('getPositionForSlide');
        $cmsPageCreator->expects(static::never())->method('createPresentationPage');

        $handler = $this->getHandler(
            $cmsPageCreator,
            $systemConfigService,
            $appointmentService,
            $presentationVersionHandler
        );

        static::expectException(NotConfiguredException::class);
        $handler->handleCreateInstantListingRequest('test-appointment-id', $this->createRequest, $this->context);
    }

    public function testHandleUpdateInstantListingRequestSuccessfully(): void
    {
        $cmsPageCreatorService = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreatorService->expects(static::once())
            ->method('updatePresentationPage')
            ->with(
                static::equalTo($this->updateRequest['addProductIds']),
                static::equalTo($this->updateRequest['removeProductIds']),
                static::equalTo($this->updateRequest['currentPageGroupId']),
                static::equalTo($this->updateRequest['pageName'] ?? null),
                static::equalTo($this->context)
            )->willReturn(['key' => 'value']);

        $handler = $this->getHandler($cmsPageCreatorService);

        $updatedData = $handler->handleUpdateInstantListingRequest('test-appointment-id', $this->updateRequest, $this->context);

        static::assertSame(['key' => 'value'], $updatedData);
    }

    public function testHandleUpdateInstantListingRequestButCanNotFindAppointment(): void
    {
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willThrowException(
                new EntityNotFoundException(AppointmentEntity::class, 'test-appointment-id')
            );

        $handler = $this->getHandler(null, null, $appointmentService);

        static::expectException(EntityNotFoundException::class);
        $handler->handleUpdateInstantListingRequest('test-appointment-id', $this->updateRequest, $this->context);
    }

    /**
     * @dataProvider getTestHandleUpdateInstantListingRequestButUseInvalidRequestProviderData
     * @param array<string, mixed> $requestParameters
     */
    public function testHandleUpdateInstantListingRequestButUseInvalidRequest(
        array $requestParameters
    ): void
    {
        $appointment = $this->getAppointment();
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $handler = $this->getHandler(null, null, $appointmentService);

        static::expectException(InvalidParameterException::class);
        $handler->handleUpdateInstantListingRequest('test-appointment-id', $requestParameters, $this->context);
    }

    public static function getTestHandleUpdateInstantListingRequestButUseInvalidRequestProviderData(): \Generator
    {
        yield [[]];
        yield [['addProductIds' => []]];
        yield [['removeProductIds' => []]];
        yield [['currentPageGroupId' => 'test-value']];
        yield [['addProductIds' => [], 'removeProductIds' => []]];
        yield [['addProductIds' => [], 'currentPageGroupId' => 'test-value']];
        yield [['removeProductIds' => [], 'currentPageGroupId' => 'test-value']];
        yield [['addProductIds' => '', 'removeProductIds' => [], 'currentPageGroupId' => 'test-value']];
        yield [['addProductIds' => [], 'removeProductIds' => '', 'currentPageGroupId' => 'test-value']];
        yield [['addProductIds' => [], 'removeProductIds' => [], 'currentPageGroupId' => '']];
    }

    private function getHandler(
        ?MockObject $cmsPageCreatorServiceMock = null,
        ?MockObject $configServiceMock = null,
        ?MockObject $appointmentServiceMock = null,
        ?MockObject $presentationVersionHandlerMock = null
    ): InstantListingRequestHandler
    {
        /** @var MockObject&CmsPageCreatorService $cmsPageCreatorService */
        $cmsPageCreatorService = $cmsPageCreatorServiceMock ?: $this->createMock(CmsPageCreatorService::class);
        /** @var MockObject&SystemConfigService $systemConfigService */
        $systemConfigService = $configServiceMock ?: $this->createMock(SystemConfigService::class);
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentServiceMock ?: $this->createMock(AppointmentService::class);
        /** @var MockObject&PresentationVersionHandler $presentationVersionHandler */
        $presentationVersionHandler = $presentationVersionHandlerMock ?: $this->createMock(PresentationVersionHandler::class);

        return new InstantListingRequestHandler(
            $cmsPageCreatorService,
            $systemConfigService,
            new DataValidator(Validation::createValidator()),
            $appointmentService,
            $presentationVersionHandler
        );
    }

    private function getAppointment(?string $presentationVersion = null): AppointmentEntity
    {
        $appointmentMockHelper = new AppointMentMockHelper();
        $appointment = $appointmentMockHelper->getAppointEntity();
        $appointment->setId('test-appointment-id');
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');

        if ($presentationVersion) {
            $presentation->setVersionId($presentationVersion);
        }

        $appointment->setPresentationId($presentation->getId());
        $appointment->setPresentationVersionId($presentation->getVersionId());

        return $appointment;
    }
}
