<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Cms;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Runtime;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationDefinition;
use SwagGuidedShopping\Content\Cms\CmsSectionExtension;

class CmsSectionExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $collection = new FieldCollection();
        $extension = new CmsSectionExtension();
        $extension->extendFields($collection);
        // Check the fields count
        $elements = $collection->getElements();
        static::assertCount(2, $elements);
        // Check the first field
        static::assertInstanceOf(TranslatedField::class, $elements[0]);
        static::assertEquals('name', $elements[0]->getPropertyName());
        static::assertNotNull($elements[0]->getFlag(Runtime::class));
        // Check the second field
        static::assertInstanceOf(TranslationsAssociationField::class, $elements[1]);
        static::assertEquals(CmsSectionTranslationDefinition::class, $elements[1]->getReferenceClass());
        static::assertEquals('cms_section_id', $elements[1]->getReferenceField());
        static::assertNotNull($elements[1]->getFlag(ApiAware::class));
    }

    public function testGetDefinitionClass(): void
    {
        $extension = new CmsSectionExtension();
        static::assertEquals(CmsSectionDefinition::class, $extension->getDefinitionClass());
    }
}
