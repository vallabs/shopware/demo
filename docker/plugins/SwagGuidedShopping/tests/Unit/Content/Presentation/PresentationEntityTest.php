<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\PresentationMockHelper;

class PresentationEntityTest extends TestCase
{
    public function testPresentationWithName(): void
    {
        $presentation = new PresentationEntity();
        $name = 'test-name';
        $presentation->setName($name);
        static::assertEquals($name, $presentation->getName());
    }

    public function testActivePresentation(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setActive(true);
        static::assertTrue($presentation->isActive());
        $presentation->setActive(false);
        static::assertFalse($presentation->isActive());
    }

    public function testPresentationTranslations(): void
    {
        $presentation = new PresentationEntity();
        $translationCollection = new PresentationTranslationCollection();
        $presentation->setTranslations($translationCollection);
        static::assertSame($translationCollection, $presentation->getTranslations());
    }

    public function testPresentationWithAppointments(): void
    {
        $presentation = new PresentationEntity();
        $appointmentMockHelper = new AppointMentMockHelper();
        $appointment = $appointmentMockHelper->getAppointEntity();
        $appointmentCollection = new AppointmentCollection([$appointment]);
        $presentation->setAppointments($appointmentCollection);
        static::assertSame($appointmentCollection, $presentation->getAppointments());
    }

    public function testPresentationWithCmsPages(): void
    {
        $presentation = new PresentationEntity();
        $cmsPageCollection = new PresentationCmsPageCollection();
        $presentation->setCmsPages($cmsPageCollection);
        static::assertSame($cmsPageCollection, $presentation->getCmsPages());
    }

    public function testPresentationWithParentId(): void
    {
        $presentation = new PresentationEntity();
        static::assertNull($presentation->getParentId());
        $parentId = 'test-parent-id';
        $presentation->setParentId($parentId);
        static::assertEquals($parentId, $presentation->getParentId());
        $presentation->setParentId(null);
        static::assertNull($presentation->getParentId());
    }

    public function testPresentationWithParent(): void
    {
        $presentation = new PresentationEntity();
        static::assertNull($presentation->getParent());
        $presentationMockHelper = new PresentationMockHelper();
        $parent = $presentationMockHelper->getPresentationEntity('test-id');
        $presentation->setParent($parent);
        static::assertSame($parent, $presentation->getParent());
        $presentation->setParent(null);
        static::assertNull($presentation->getParent());
    }

    public function testPresentationWithCreatedById(): void
    {
        $presentation = new PresentationEntity();
        $createdById = 'test-created-by-id';
        $presentation->setCreatedById($createdById);
        static::assertEquals($createdById, $presentation->getCreatedById());
        $presentation->setCreatedById(null);
        static::assertNull($presentation->getCreatedById());
    }

    public function testPresentationWithUpdatedById(): void
    {
        $presentation = new PresentationEntity();
        $updatedById = 'test-updated-by-id';
        $presentation->setUpdatedById($updatedById);
        static::assertEquals($updatedById, $presentation->getUpdatedById());
        $presentation->setUpdatedById(null);
        static::assertNull($presentation->getUpdatedById());
    }

    public function testPresentationWithCreatedBy(): void
    {
        $presentation = new PresentationEntity();
        $createdBy = new UserEntity();
        $presentation->setCreatedBy($createdBy);
        static::assertSame($createdBy, $presentation->getCreatedBy());
        $presentation->setCreatedBy(null);
        static::assertNull($presentation->getCreatedBy());
    }

    public function testPresentationWithUpdatedBy(): void
    {
        $presentation = new PresentationEntity();
        $updatedBy = new UserEntity();
        $presentation->setUpdatedBy($updatedBy);
        static::assertSame($updatedBy, $presentation->getUpdatedBy());
        $presentation->setUpdatedBy(null);
        static::assertNull($presentation->getUpdatedBy());
    }
}
