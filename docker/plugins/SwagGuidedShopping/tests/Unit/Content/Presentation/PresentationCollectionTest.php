<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;

class PresentationCollectionTest extends TestCase
{
    public function testGetExpectedClass(): void
    {
        $presentationCollection = new PresentationCollection();
        $reflection = new \ReflectionClass(\get_class($presentationCollection));
        $method = $reflection->getMethod('getExpectedClass');
        $method->setAccessible(true);

        $result = $method->invokeArgs($presentationCollection, []);
        $expectedClass = PresentationEntity::class;
        static::assertSame($expectedClass, $result);
    }
}