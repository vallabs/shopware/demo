<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationStructure;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationCollection;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationEntity;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\Exception\PresentationNotFoundException;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationPageResult;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationPageResults;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationStructureRoute;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsBlockMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSlotMockHelper;
use Symfony\Component\HttpFoundation\Request;

class PresentationStructureRouteTest extends TestCase
{
    protected SalesChannelContext $validSalesChannelContext;
    protected AppointmentEntity $appointment;

    protected function setUp(): void
    {
        parent::setUp();
        $this->validSalesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $attendee->setType(AttendeeDefinition::TYPE_GUIDE);
        $this->appointment = (new AppointMentMockHelper())->getAppointEntity();
        $this->appointment->setPresentationId('test-presentation-id');
        $attendee->setAppointment($this->appointment);
        $this->validSalesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testGetStructureWithInvalidSalesChannelContext(): void
    {
        $withoutAttendeeSalesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::never())->method('createPresentationVersion');

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::never())->method('search');

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::never())->method('search');

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);

        static::expectException(SalesChannelContextMissingAttendeeException::class);
        $route->structure(new Request(), $withoutAttendeeSalesChannelContext);
    }

    public function testGetStructureButCanNotFindPresentationIdWhenCreateNewVersion(): void
    {
        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())
            ->method('createPresentationVersion')
            ->with(
                static::equalTo($this->appointment),
                static::equalTo($this->validSalesChannelContext->getContext())
            )
            ->willThrowException(new PresentationNotFoundException());

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::never())->method('search');

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::never())->method('search');

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        static::expectException(PresentationNotFoundException::class);
        $route->structure(new Request(), $this->validSalesChannelContext);
    }

    /**
     * @dataProvider getTestGetStructureWithValidSalesChannelContextButCanNotFoundPresentationWithDifferentPresentationVersionIdProviderData
     */
    public function testGetStructureWithValidSalesChannelContextButCanNotFoundPresentationWithDifferentPresentationVersionId(
        SalesChannelContext $salesChannelContext,
        AppointmentEntity $appointment,
        string $presentationVersionId
    ): void
    {
        $context = $salesChannelContext->getContext();

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())
            ->method('createPresentationVersion')
            ->willReturn($presentationVersionId);

        if ($presentationVersionId === Defaults::LIVE_VERSION) {
            $presentationVersionHandler->expects(static::never())->method('updatePresentationCmsPages');
        } else {
            $presentationVersionHandler->expects(static::once())
                ->method('updatePresentationCmsPages')
                ->with(
                    static::equalTo($appointment),
                    static::equalTo($presentationVersionId),
                    static::equalTo($context)
                );
        }

        $presentationRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        /** @var string $presentationId */
        $presentationId = $appointment->getPresentationId();
        $criteria->setIds([$presentationId]);
        $criteria->getAssociation('cmsPages')
            ->addSorting(new FieldSorting('position'))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', $presentationVersionId));
        $presentationRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new EntitySearchResult(
                PresentationEntity::class,
                0,
                new PresentationCollection([]),
                null,
                $criteria,
                $context
            ));

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::never())->method('search');

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        static::expectException(PresentationNotFoundException::class);
        $route->structure(new Request(), $salesChannelContext);
    }

    public static function getTestGetStructureWithValidSalesChannelContextButCanNotFoundPresentationWithDifferentPresentationVersionIdProviderData(): \Generator
    {
        $salesChannelContextHelper = new SalesChannelContextHelper();

        $salesChannelContextHasGuideAppointmentWithoutPresentationVersionId = $salesChannelContextHelper->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $guideAppointmentWithoutPresentationVersionId = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->setAppointment($guideAppointmentWithoutPresentationVersionId);
        $salesChannelContextHasGuideAppointmentWithoutPresentationVersionId->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        yield  'sales-channel-context-has-guide-appointment-without-presentation-version-id' => [
            $salesChannelContextHasGuideAppointmentWithoutPresentationVersionId,
            $guideAppointmentWithoutPresentationVersionId,
            'test-create-random-version-id'
        ];

        $salesChannelContextHasSelfDefaultAppointmentWithoutPresentationVersionId = $salesChannelContextHelper->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $selfDefaultAppointmentWithoutPresentationVersionId = (new AppointMentMockHelper())->getAppointEntity();
        $selfDefaultAppointmentWithoutPresentationVersionId->setDefault(true);
        $selfDefaultAppointmentWithoutPresentationVersionId->setMode(AppointmentDefinition::MODE_SELF);
        $attendee->setAppointment($selfDefaultAppointmentWithoutPresentationVersionId);
        $salesChannelContextHasSelfDefaultAppointmentWithoutPresentationVersionId->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        yield 'sales-channel-context-has-self-default-appointment-without-presentation-version-id' => [
            $salesChannelContextHasSelfDefaultAppointmentWithoutPresentationVersionId,
            $selfDefaultAppointmentWithoutPresentationVersionId,
            Defaults::LIVE_VERSION
        ];

        $salesChannelContextHasGuideAppointmentWithPresentationVersionId = $salesChannelContextHelper->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $guideAppointmentWithPresentationVersionId = (new AppointMentMockHelper())->getAppointEntity();
        $guideAppointmentWithPresentationVersionId->setPresentationVersionId('test-presentation-version-id');
        $attendee->setAppointment($guideAppointmentWithPresentationVersionId);
        $salesChannelContextHasGuideAppointmentWithPresentationVersionId->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        yield 'sales-channel-context-has-guide-appointment-with-presentation-version-id' => [
            $salesChannelContextHasGuideAppointmentWithPresentationVersionId,
            $guideAppointmentWithPresentationVersionId,
            'test-presentation-version-id'
        ];

        $salesChannelContextHasSelfDefaultAppointmentWithPresentationVersionId = $salesChannelContextHelper->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $selfDefaultAppointmentWithPresentationVersionId = (new AppointMentMockHelper())->getAppointEntity();
        $selfDefaultAppointmentWithPresentationVersionId->setDefault(true);
        $selfDefaultAppointmentWithPresentationVersionId->setMode(AppointmentDefinition::MODE_SELF);
        $selfDefaultAppointmentWithPresentationVersionId->setPresentationVersionId('test-presentation-version-id');
        $attendee->setAppointment($selfDefaultAppointmentWithPresentationVersionId);
        $salesChannelContextHasSelfDefaultAppointmentWithPresentationVersionId->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        yield 'sales-channel-context-has-self-default-appointment-with-presentation-version-id' => [
            $salesChannelContextHasSelfDefaultAppointmentWithPresentationVersionId,
            $selfDefaultAppointmentWithPresentationVersionId,
            Defaults::LIVE_VERSION
        ];
    }

    public function testStructureWithValidSalesChannelContextUsingPresentationThatHasCmsPageWithoutCmsSection(): void
    {
        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $presentationCmsPage->setCmsPageId('test-cms-page-id');
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationEntity::class,
                1,
                new PresentationCollection([$presentation]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria(['test-cms-page-id']);
        $criteria->addAssociation('sections.blocks.slots');
        $criteria->addAssociation('sections.translations');
        $criteria->getAssociation('sections')->addSorting(new FieldSorting('position'));
        $criteria->getAssociation('sections.blocks')->addSorting(new FieldSorting('position'));
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($this->validSalesChannelContext->getContext())
            );

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        $response = $route->structure(new Request(), $this->validSalesChannelContext);
        $presentationPageResult = $response->getObject();
        static::assertInstanceOf(PresentationPageResults::class, $presentationPageResult);

        $navigation = $presentationPageResult->getNavigation();
        static::assertIsArray($navigation);
        static::assertCount(0, $navigation);
    }

    /**
     * @dataProvider getTestStructureWithValidSalesChannelContextHasDifferentTypeOfAttendeeProviderData
     */
    public function testStructureWithValidSalesChannelContextHasDifferentTypeOfAttendeeUsingPresentationThatHasCmsPageWithNotesSlot(
        string $attendeeType,
        bool $hasNotes
    ): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $attendee->setType($attendeeType);
        $this->appointment = (new AppointMentMockHelper())->getAppointEntity();
        $this->appointment->setPresentationId('test-presentation-id');
        $attendee->setAppointment($this->appointment);
        $salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsSectionMockHelper = new CmsSectionMockHelper();
        $cmsBlockMockHelper = new CmsBlockMockHelper();
        $cmsSlotMockHelper = new CmsSlotMockHelper();
        $notesSlot = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-notes-slot-id',
            'test-cms-block-id',
            null,
            new FieldConfigCollection()
        );
        $notesSlot->setType('notes');
        $cmsBlock = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-id',
            0,
            new CmsSlotCollection([$notesSlot])
        );
        $cmsSection = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlock])
        );
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId('test-cms-page-id');
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection]));
        $presentationCmsPage->setCmsPageId('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationEntity::class,
                1,
                new PresentationCollection([$presentation]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                CmsPageEntity::class,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        $response = $route->structure(new Request(), $salesChannelContext);

        $presentationPageResult = $response->getObject();
        static::assertInstanceOf(PresentationPageResults::class, $presentationPageResult);

        $navigations = $presentationPageResult->getNavigation();
        static::assertIsArray($navigations);
        static::assertCount(1, $navigations);

        $firstNavigation = $navigations[0];
        static::assertSame($hasNotes, isset($firstNavigation['notes']));
        if ($hasNotes) {
            static::assertSame([$notesSlot], $firstNavigation['notes']);
        }
    }

    public static function getTestStructureWithValidSalesChannelContextHasDifferentTypeOfAttendeeProviderData(): \Generator
    {
        yield 'is-guide-attendee' => [
            AttendeeDefinition::TYPE_GUIDE,
            true
        ];

        yield 'is-client-attendee' => [
            AttendeeDefinition::TYPE_CLIENT,
            false
        ];
    }

    /**
     * @dataProvider getTestIfPresentationCmsPageIsInstantListingProviderData
     * @param array<string>|null $pickedProductIds
     */
    public function testStructureWithValidSalesChannelContextUsingPresentationThatHasCmsPageIfItIsInstantListing(
        bool $isInstantListing,
        ?array $pickedProductIds,
        ?int $expectPickedProductsCount
    ): void
    {
        $salesChannelContext = $this->validSalesChannelContext;

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsSectionMockHelper = new CmsSectionMockHelper();
        $cmsBlockMockHelper = new CmsBlockMockHelper();
        $cmsSlotMockHelper = new CmsSlotMockHelper();
        $cmsSlot = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-slot-id',
            'test-cms-block-id',
            null,
            new FieldConfigCollection()
        );
        $cmsBlock = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-id',
            0,
            new CmsSlotCollection([$cmsSlot])
        );
        $cmsSection = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlock])
        );
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId('test-cms-page-id');
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection]));
        $presentationCmsPage->setCmsPageId('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setInstantListing($isInstantListing);
        $presentationCmsPage->setPickedProductIds($pickedProductIds);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationEntity::class,
                1,
                new PresentationCollection([$presentation]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                CmsPageEntity::class,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        $response = $route->structure(new Request(), $salesChannelContext);
        $presentationPageResult = $response->getObject();
        static::assertInstanceOf(PresentationPageResults::class, $presentationPageResult);

        $navigations = $presentationPageResult->getNavigation();
        static::assertIsArray($navigations);
        static::assertCount(1, $navigations);

        $firstNavigation = $navigations[0];
        static::assertSame(1, $firstNavigation['index']);
        static::assertSame($cmsSection->getId(), $firstNavigation['sectionId']);
        static::assertSame($cmsSection->getName(), $firstNavigation['sectionName']);
        static::assertSame($presentationCmsPage->getId(), $firstNavigation['groupId']);
        static::assertSame($presentationCmsPage->getTitle(), $firstNavigation['groupName']);
        static::assertSame($cmsPage->getId(), $firstNavigation['cmsPageId']);
        static::assertSame($isInstantListing, $firstNavigation['isInstantListing']);
        static::assertEquals($expectPickedProductsCount, $firstNavigation['pickedProductsCount']);

        $cmsPageResults = $presentationPageResult->getCmsPageResults();
        static::assertEquals(1, $cmsPageResults->count());

        /** @var PresentationPageResult $firstCmsPageResult */
        $firstCmsPageResult = $cmsPageResults->getElements()[0];
        static::assertEquals(PresentationStructureRoute::PRESENTATION_PAGE_ROUTE, $firstCmsPageResult->getResourceType());
        static::assertEquals($presentation->getId(), $firstCmsPageResult->getResourceIdentifier());
        static::assertEquals($cmsPage, $firstCmsPageResult->getCmsPage());
    }

    public static function getTestIfPresentationCmsPageIsInstantListingProviderData(): \Generator
    {
        yield 'is-instant-listing-without-picked-products' => [
            true,
            null,
            0
        ];

        yield 'is-instant-listing-with-picked-products' => [
            true,
            ['test-product-id', 'test-product-id-2'],
            2
        ];

        yield 'is-not-instant-listing' => [
            false,
            null,
            0
        ];
    }

    public function testStructureWithValidSalesChannelContextUsingPresentationThatHasCmsPageWithSlotHasProductConfig(): void
    {
        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $presentationCmsPage->setTitle('test-presentation-cms-page-title');
        $cmsSectionMockHelper = new CmsSectionMockHelper();
        $cmsBlockMockHelper = new CmsBlockMockHelper();
        $cmsSlotMockHelper = new CmsSlotMockHelper();
        $cmsSlot = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-cms-slot-id',
            'test-cms-block-id',
            null,
            new FieldConfigCollection()
        );
        $cmsBlock = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-id',
            0,
            new CmsSlotCollection([$cmsSlot])
        );
        $cmsSection = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlock])
        );
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId('test-cms-page-id');
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection]));
        $presentationCmsPage->setCmsPageId('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setTranslated([
            'slotConfig' => [
                'test-cms-slot-id' => [
                    'title' => 'test-title',
                    'products' => [
                        'source' => 'test',
                        'value' => ['test-product-id']
                    ]
                ]
            ]
        ]);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationEntity::class,
                1,
                new PresentationCollection([$presentation]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                CmsPageEntity::class,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        $response = $route->structure(new Request(), $this->validSalesChannelContext);
        $presentationPageResult = $response->getObject();
        static::assertInstanceOf(PresentationPageResults::class, $presentationPageResult);

        $navigations = $presentationPageResult->getNavigation();
        static::assertIsArray($navigations);
        static::assertCount(1, $navigations);

        $cmsPageResults = $presentationPageResult->getCmsPageResults();
        static::assertEquals(1, $cmsPageResults->count());

        /** @var PresentationPageResult $firstCmsPageResult */
        $firstCmsPageResult = $cmsPageResults->getElements()[0];
        static::assertEquals(PresentationStructureRoute::PRESENTATION_PAGE_ROUTE, $firstCmsPageResult->getResourceType());
        static::assertEquals($presentation->getId(), $firstCmsPageResult->getResourceIdentifier());

        /** @var CmsPageEntity $cmsPage */
        $cmsPage = $firstCmsPageResult->getCmsPage();
        static::assertEquals($cmsPage, $firstCmsPageResult->getCmsPage());

        /** @var CmsSectionCollection $cmsSections */
        $cmsSections = $cmsPage->getSections();
        $cmsSlots = $cmsSections->getBlocks()->getSlots();
        /** @var CmsSlotEntity $testSlot */
        $testSlot = $cmsSlots->get('test-cms-slot-id');
        static::assertEquals(['products' => ['value' => ['test-product-id']]], $testSlot->getTranslated());
        static::assertEquals(['products' => ['value' => ['test-product-id']]], $testSlot->getConfig());
    }

    public function testStructureWithSectionsWithDifferentKindOfName(): void
    {
        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::once())->method('createPresentationVersion');

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $presentationCmsPage->setTitle('test-presentation-cms-page-title');
        $cmsSectionMockHelper = new CmsSectionMockHelper();
        $cmsBlockMockHelper = new CmsBlockMockHelper();
        $cmsSlotMockHelper = new CmsSlotMockHelper();
        $cmsSlot = $cmsSlotMockHelper->getCmsSlotEntity(
            'test-cms-slot-id',
            'test-cms-block-id',
            null,
            new FieldConfigCollection()
        );
        $cmsBlock = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id',
            'test-cms-section-id',
            0,
            new CmsSlotCollection([$cmsSlot])
        );
        $cmsSection = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlock])
        );
        $cmsSection2 = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id-2',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlock])
        );
        $cmsSectionTranslation = new CmsSectionTranslationEntity();
        $defaultLangId = Defaults::LANGUAGE_SYSTEM;
        $cmsSectionTranslation->setUniqueIdentifier("test-cms-section-id-2-{$defaultLangId}");
        $cmsSectionTranslation->setName('test-translation-name');
        $cmsSection2->setExtensions([
            'translations' => new CmsSectionTranslationCollection([$cmsSectionTranslation])
        ]);
        $cmsSection3 = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id-3',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlock]),
            false
        );
        $cmsSection4 = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id-4',
            'test-cms-page-id',
            0,
            new CmsBlockCollection()
        );
        $cmsBlockWithoutSlot = $cmsBlockMockHelper->getCmsBlockEntity(
            'test-cms-block-id-2',
            'test-cms-section-id-5',
            0,
            new CmsSlotCollection()
        );
        $cmsSection5 = $cmsSectionMockHelper->getCmsSectionEntity(
            'test-cms-section-id-5',
            'test-cms-page-id',
            0,
            new CmsBlockCollection([$cmsBlockWithoutSlot])
        );
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId('test-cms-page-id');
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection, $cmsSection2, $cmsSection3, $cmsSection4, $cmsSection5]));
        $presentationCmsPage->setCmsPageId('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setTranslated([
            'slotConfig' => [
                'test-cms-slot-id' => [
                    'title' => 'test-title',
                    'products' => [
                        'source' => 'test',
                        'value' => ['test-product-id']
                    ]
                ]
            ]
        ]);

        $cmsPage2 = clone $cmsPage;
        $cmsPage2->setSections(new CmsSectionCollection());
        $presentationCmsPage2 = clone $presentationCmsPage;
        $presentationCmsPage2->setId('test-presentation-cms-page-id-2');
        $presentationCmsPage2->setCmsPageId('test-cms-page-id');
        $presentationCmsPage2->setCmsPage($cmsPage2);

        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage, $presentationCmsPage2]));
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationEntity::class,
                1,
                new PresentationCollection([$presentation]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::exactly(2))
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    CmsPageEntity::class,
                    1,
                    new CmsPageCollection([$cmsPage]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                ),
                new EntitySearchResult(
                    CmsPageEntity::class,
                    1,
                    new CmsPageCollection([$cmsPage2]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $route = $this->getRoute($presentationRepository, $cmsPageRepository, $presentationVersionHandler);
        $response = $route->structure(new Request(), $this->validSalesChannelContext);

        $presentationPageResult = $response->getObject();
        static::assertInstanceOf(PresentationPageResults::class, $presentationPageResult);

        $navigations = $presentationPageResult->getNavigation();
        static::assertIsArray($navigations);
        static::assertCount(5, $navigations);
        static::assertSame($cmsSection->getName(), $navigations[0]['sectionName']);
        static::assertSame('test-translation-name', $navigations[1]['sectionName']);
        static::assertNull($navigations[2]['sectionName']);
    }

    private function getRoute(
        ?MockObject $presentationRepositoryMock = null,
        ?MockObject $cmsPageRepositoryMock = null,
        ?MockObject $presentationVersionHandlerMock = null
    ): PresentationStructureRoute
    {
        /** @var MockObject&EntityRepository<PresentationCollection> $presentationRepository */
        $presentationRepository = $presentationRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<CmsPageCollection> $cmsPageRepository */
        $cmsPageRepository = $cmsPageRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&PresentationVersionHandler $presentationVersionHandler */
        $presentationVersionHandler = $presentationVersionHandlerMock ?: $this->createMock(PresentationVersionHandler::class);

        return new PresentationStructureRoute(
            $presentationRepository,
            $cmsPageRepository,
            $presentationVersionHandler,
            new NotesElementHandler()
        );
    }
}
