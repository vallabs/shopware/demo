<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationSlideProducts;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingResult;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideProducts\PresentationSlideProductsResponse;

class PresentationSlideProductsResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $productListingResult = new ProductListingResult(
            ProductEntity::class,
            0,
            new ProductCollection([]),
            null,
            new Criteria(),
            new Context(new SystemSource())
        );
        $response = new PresentationSlideProductsResponse($productListingResult);
        static::assertSame($productListingResult, $response->getObject());
    }
}
