<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationSlideData;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Category\CategoryDefinition;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\Detail\ProductConfiguratorLoader;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductCollection;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\Product\ProductPageCriteriaEvent;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideData\PresentationSlideDataRoute;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsBlockMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use SwagShopwarePwa\Pwa\PageResult\AbstractPageResult;
use SwagShopwarePwa\Pwa\PageResult\Navigation\NavigationPageResult;
use SwagShopwarePwa\Pwa\PageResult\Product\ProductPageResult;

class PresentationSlideDataRouteTest extends TestCase
{
    protected SalesChannelContext $validSalesChannelContext;
    protected Request $validRequest;

    protected function setUp(): void
    {
        parent::setUp();
        $this->validSalesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->setAppointment($appointment);
        $this->validSalesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        $this->validRequest = new Request(
            [],
            [
                'sectionId' => 'test-section-id',
                'presentationCmsPageId' => 'test-presentation-cms-page-id'
            ]
        );
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    /**
     * @dataProvider getTestFetchSlideDataWithInvalidRequestAndDifferentSalesChannelContextProviderData
     */
    public function testFetchSlideDataWithInvalidRequest(Request $request): void
    {
        $route = $this->getRoute();
        static::expectException(RoutingException::class);
        $route->data($request, (new SalesChannelContextHelper())->createSalesChannelContext());
    }

    public static function getTestFetchSlideDataWithInvalidRequestAndDifferentSalesChannelContextProviderData(): \Generator
    {
        $requestWithoutParameters = new Request();
        yield 'request-without-params' => [$requestWithoutParameters];

        $requestWithSectionIdButPresentationCmsPageId = new Request(
            [],
            ['sectionId' => 'test-section-id']
        );
        yield 'request-with-section-id' => [$requestWithSectionIdButPresentationCmsPageId];

        $requestWithPresentationCmsPageIdButSectionId = new Request(
            [],
            ['presentationCmsPageId' => 'test-presentation-cms-page-id']
        );
        yield 'request-with-presentation-cms-page-id' => [$requestWithPresentationCmsPageIdButSectionId];
    }

    public function testFetchSlideDataWithValidRequestButSalesChannelContext(): void
    {
        $withoutAttendeeSalesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $route = $this->getRoute();

        static::expectException(SalesChannelContextMissingAttendeeException::class);
        $route->data($this->validRequest, $withoutAttendeeSalesChannelContext);
    }

    public function testFetchSlideDataButNotFoundPresentation(): void
    {
        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    0,
                    new EntityCollection([]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );
        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::never())->method('load');

        $route = $this->getRoute($presentationRepository, $pageLoader);
        static::expectException(EntityNotFoundException::class);
        $route->data($this->validRequest, $this->validSalesChannelContext);
    }

    /**
     * @dataProvider getTestFetchSlideDataUsePresentationThatDoNotHaveValidCmsPagesProviderData
     * @param class-string<\Throwable> $exception
     */
    public function testFetchSlideDataUsePresentationThatDoNotHaveValidCmsPages(
        PresentationEntity $presentation,
        string $exception
    ): void
    {
        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new PresentationCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::never())->method('dispatch');

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::never())->method('search');

        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::never())->method('load');

        $route = $this->getRoute($presentationRepository, $pageLoader, $productRepository, $eventDispatcher);
        static::expectException($exception);
        $route->data($this->validRequest, $this->validSalesChannelContext);
    }

    public static function getTestFetchSlideDataUsePresentationThatDoNotHaveValidCmsPagesProviderData(): \Generator
    {
        $doNotHavePresentationCmsPage = new PresentationEntity();
        $doNotHavePresentationCmsPage->setId('test-presentation-id');
        yield 'do-not-have-presentation-cmsPage' => [
            $doNotHavePresentationCmsPage,
            EntityNotFoundException::class
        ];

        $presentationCmsPageDoNotHaveCmsPage = new PresentationEntity();
        $presentationCmsPageDoNotHaveCmsPage->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $presentationCmsPageDoNotHaveCmsPage->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));
        yield 'presentation-cmsPage-do-not-have-cmsPage' => [
            $presentationCmsPageDoNotHaveCmsPage,
            EntityNotFoundException::class
        ];

        $presentationCmsPagWithProductDetailCmsPage = new PresentationEntity();
        $presentationCmsPagWithProductDetailCmsPage->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsPage->setType('product_detail');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPagWithProductDetailCmsPage->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));
        yield 'presentation-cmsPage-with-product-detail-cmsPage' => [
            $presentationCmsPagWithProductDetailCmsPage,
            \Exception::class
        ];
    }

    public function testFetchSlideDataUsePresentationThatHaveCmsPageIsProductDetailButCanNotFindProduct(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsPage->setType('product_detail');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setProductId('test-product-id');
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new PresentationCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $criteria = new Criteria(['test-product-id']);
        $criteria->addAssociation('manufacturer.media')
            ->addAssociation('options.group')
            ->addAssociation('properties.group')
            ->addAssociation('mainCategories.category')
            ->addAssociation('productReviews')
            ->addAssociation('media')
            ->addAssociation('crossSellings');

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::once())
            ->method('dispatch')
            ->with(new ProductPageCriteriaEvent('test-product-id', $criteria, $this->validSalesChannelContext));

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    SalesChannelProductEntity::class,
                    0,
                    new SalesChannelProductCollection([]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::never())->method('load');

        $route = $this->getRoute($presentationRepository, $pageLoader, $productRepository, $eventDispatcher);
        static::expectException(EntityNotFoundException::class);
        $route->data($this->validRequest, $this->validSalesChannelContext);
    }

    public function testFetchSlideDataButHaveErrorMeanwhileLoadingCmsPage(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new PresentationCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::once())
            ->method('load')
            ->willThrowException(new \Exception());

        $route = $this->getRoute($presentationRepository, $pageLoader);
        static::expectException(\Exception::class);
        $route->data($this->validRequest, $this->validSalesChannelContext);
    }

    public function testFetchSlideDataWithLoadingCmsPageSuccessfulButDoNotHaveCmsPage(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new PresentationCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::once())
            ->method('load')
            ->willReturn(new EntitySearchResult(
                CmsPageEntity::class,
                0,
                new CmsSectionCollection([]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $route = $this->getRoute($presentationRepository, $pageLoader);
        static::expectException(EntityNotFoundException::class);
        $route->data($this->validRequest, $this->validSalesChannelContext);
    }

    public function testFetchSlideDataUsePresentationThatHaveProductDetailCmsPageButCanNotFindProduct(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsPage->setType('product_detail');
        $presentationCmsPage->setCmsPage($cmsPage);
        $productId = 'test-product-id';
        $product = new SalesChannelProductEntity();
        $product->setId($productId);
        $presentationCmsPage->setProductId($productId);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new EntityCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::once())->method('searchIds');
        $productRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    SalesChannelProductEntity::class,
                    0,
                    new SalesChannelProductCollection([]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::never())->method('load');

        $route = $this->getRoute($presentationRepository, $pageLoader, $productRepository);
        static::expectException(EntityNotFoundException::class);
        $route->data($this->validRequest, $this->validSalesChannelContext);
    }

    public function testFetchSlideDataUsePresentationThatHaveProductDetailCmsPageAndCanFindProduct(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsPage->setType('product_detail');
        $presentationCmsPage->setCmsPage($cmsPage);
        $productId = 'test-product-id';
        $product = new SalesChannelProductEntity();
        $product->setId($productId);
        $presentationCmsPage->setProductId($productId);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new EntityCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $productRepository = $this->createMock(SalesChannelRepository::class);
        $productRepository->expects(static::once())->method('searchIds');
        $productRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    SalesChannelProductEntity::class,
                    1,
                    new SalesChannelProductCollection([$product]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsSection = (new CmsSectionMockHelper())->getCmsSectionEntity('test-cms-section-id', $cmsPage->getId(), 1, new CmsBlockCollection([]));
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection]));
        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::once())
            ->method('load')
            ->willReturn(new EntitySearchResult(
                CmsPageEntity::class,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $route = $this->getRoute($presentationRepository, $pageLoader, $productRepository);
        $response = $route->data($this->validRequest, $this->validSalesChannelContext);
        /** @var AbstractPageResult $pageResultFromResponse */
        $pageResultFromResponse = $response->getObject();
        static::assertInstanceOf(ProductPageResult::class, $pageResultFromResponse);
        static::assertSame($cmsPage, $pageResultFromResponse->getCmsPage());
        static::assertSame($presentationCmsPage, $pageResultFromResponse->getExtension('cmsPageRelation'));
    }

    public function testFetchSlideDataUsePresentationThatHaveProductListingCmsPage(): void
    {
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsPage->setType('product_list');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setPickedProductIds(['test-product-id', 'test-product-id', 'test-product-id-2']);
        $presentation->setCmsPages(new PresentationCmsPageCollection([$presentationCmsPage]));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationEntity::class,
                    1,
                    new PresentationCollection([$presentation]),
                    null,
                    new Criteria(),
                    $this->validSalesChannelContext->getContext()
                )
            );

        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $cmsSection1 = (new CmsSectionMockHelper())->getCmsSectionEntity('test-cms-section-id-1', $cmsPage->getId(), 1, new CmsBlockCollection([]));
        $cmsBlock = (new CmsBlockMockHelper())->getCmsBlockEntity('test-cms-block-id', 'test-cms-section-id-2', 1);
        $cmsSection2 = (new CmsSectionMockHelper())->getCmsSectionEntity('test-cms-section-id-2', $cmsPage->getId(), 1, new CmsBlockCollection([$cmsBlock]));
        $cmsPage->setSections(new CmsSectionCollection([$cmsSection1, $cmsSection2]));
        $pageLoader = $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $pageLoader->expects(static::once())
            ->method('load')
            ->willReturn(new EntitySearchResult(
                CmsPageEntity::class,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $this->validSalesChannelContext->getContext()
            ));

        $noteElementHandler = $this->createMock(NotesElementHandler::class);
        $noteElementHandler->expects(static::exactly(2))->method('removeNotes');

        $route = $this->getRoute($presentationRepository, $pageLoader, null, null, $noteElementHandler);
        $response = $route->data($this->validRequest, $this->validSalesChannelContext);
        /** @var AbstractPageResult $pageResultFromResponse */
        $pageResultFromResponse = $response->getObject();
        $expectedNavigationPageResult = NavigationPageResult::class;
        static::assertInstanceOf($expectedNavigationPageResult, $pageResultFromResponse);
        static::assertSame($cmsPage, $pageResultFromResponse->getCmsPage());
        static::assertSame($presentationCmsPage, $pageResultFromResponse->getExtension('cmsPageRelation'));
        /** @var array<string> $pickedProductIds */
        $pickedProductIds = $presentationCmsPage->getPickedProductIds();
        static::assertSame(\array_values($pickedProductIds), ['test-product-id', 'test-product-id-2']);
    }

    private function getRoute(
        MockObject $presentationRepositoryMock = null,
        MockObject $pageLoaderMock = null,
        MockObject $productRepositoryMock = null,
        MockObject $eventDispatcherMock = null,
        MockObject $noteElementHandlerMock = null
    ): PresentationSlideDataRoute
    {
        /** @var MockObject&EntityRepository<PresentationCollection> $presentationRepository */
        $presentationRepository = $presentationRepositoryMock ?:$this->createMock(EntityRepository::class);
        /** @var MockObject&SalesChannelRepository<SalesChannelProductCollection> $productRepository */
        $productRepository = $productRepositoryMock ?: $this->createMock(SalesChannelRepository::class);
        $configuratorLoader =  $this->createMock(ProductConfiguratorLoader::class);
        /** @var MockObject&SalesChannelCmsPageLoaderInterface $pageLoader */
        $pageLoader = $pageLoaderMock ?: $this->createMock(SalesChannelCmsPageLoaderInterface::class);
        $categoryDefinition = $this->createMock(CategoryDefinition::class);
        $productDefinition = $this->createMock(ProductDefinition::class);
        /** @var MockObject&EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $eventDispatcherMock ?: $this->createMock(EventDispatcherInterface::class);
        /** @var MockObject&NotesElementHandler $noteElementHandler */
        $noteElementHandler = $noteElementHandlerMock ?: $this->createMock(NotesElementHandler::class);

        return new PresentationSlideDataRoute(
            $presentationRepository,
            $productRepository,
            $configuratorLoader,
            $pageLoader,
            $categoryDefinition,
            $productDefinition,
            $eventDispatcher,
            $noteElementHandler
        );
    }
}
