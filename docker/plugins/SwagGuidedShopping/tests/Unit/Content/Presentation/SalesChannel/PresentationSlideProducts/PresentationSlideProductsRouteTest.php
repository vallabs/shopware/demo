<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationSlideProducts;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingLoader;
use Shopware\Core\Content\Product\SalesChannel\ProductAvailableFilter;
use Shopware\Core\Content\ProductStream\Service\ProductStreamBuilder;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\AndFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\OrFilter;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideProducts\PresentationSlideProductsRoute;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class PresentationSlideProductsRouteTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;
    protected Request $validRequest;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $this->validRequest = new Request(
            [],
            [
                'presentationCmsPageId' => 'test-presentation-cms-page-id',
                'sectionId' => 'test-section-id'
            ]
        );
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    /**
     * @dataProvider getTestGetProductsWithInvalidRequestProviderData
     */
    public function testGetProductsWithInvalidRequest(Request $request): void
    {
        $route = $this->getRoute();
        static::expectException(RoutingException::class);
        $route->products($request, $this->salesChannelContext, new Criteria());
    }

    public static function getTestGetProductsWithInvalidRequestProviderData(): \Generator
    {
        $requestWithoutSectionId = new Request();
        yield 'request-without-section-id' => [$requestWithoutSectionId];

        $requestWithSectionIdButPresentationCmsPageId = new Request(
            [],
            ['sectionId' => 'test-section-id']
        );
        yield 'request-with-section-id-but-presentation-cms-page-id' => [$requestWithSectionIdButPresentationCmsPageId];
    }

    public function testGetProductsUsePresentationCmsPageDoNotHaveProduct(): void
    {
        $presentationCmsRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setPickedProductIds(null);
        $presentationCmsRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                PresentationCmsPageEntity::class,
                1,
                new PresentationCmsPageCollection([$presentationCmsPage]),
                null,
                new Criteria(),
                $this->salesChannelContext->getContext()
            ));

        $listingLoader = $this->createMock(ProductListingLoader::class);
        $listingLoader->expects(static::never())->method(static::anything());

        $productStreamBuilder = $this->createMock(ProductStreamBuilder::class);
        $productStreamBuilder->expects(static::never())->method('buildFilters');

        $route = $this->getRoute($presentationCmsRepository, $listingLoader, $productStreamBuilder);
        $criteria = new Criteria();
        $route->products($this->validRequest, $this->salesChannelContext, $criteria);
    }

    public function testGetProductUsePresentationCmsPageHaveProduct(): void
    {
        $presentationCmsRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $pickedProductIds = ['test-product-id'];
        $presentationCmsPage->setPickedProductIds($pickedProductIds);
        $entities = new EntitySearchResult(
            PresentationCmsPageEntity::class,
            1,
            new PresentationCmsPageCollection([$presentationCmsPage]),
            null,
            new Criteria(),
            $this->salesChannelContext->getContext()
        );
        $state = 'testState';
        $entities->addState($state);
        $presentationCmsRepository->expects(static::once())
            ->method('search')
            ->willReturn($entities);

        $listingLoader = $this->createMock(ProductListingLoader::class);
        $listingLoader->expects(static::once())
            ->method('load')
            ->willReturn($entities);

        $productStreamBuilder = $this->createMock(ProductStreamBuilder::class);
        $productStreamBuilder->expects(static::never())->method('buildFilters');

        $route = $this->getRoute($presentationCmsRepository, $listingLoader, $productStreamBuilder);
        $criteria = new Criteria();
        $response = $route->products($this->validRequest, $this->salesChannelContext, $criteria);
        static::assertEquals(2, \count($criteria->getFilters()));
        static::assertInstanceOf(ProductAvailableFilter::class, $criteria->getFilters()[0]);
        static::assertInstanceOf(MultiFilter::class, $criteria->getFilters()[1]);
        $criteriaMultiFilter = $criteria->getFilters()[1];
        static::assertEquals(MultiFilter::CONNECTION_OR, $criteriaMultiFilter->getOperator());
        static::assertInstanceOf(EqualsAnyFilter::class, $criteriaMultiFilter->getQueries()[0]);
        static::assertSame('parentId', $criteriaMultiFilter->getQueries()[0]->getField());
        static::assertInstanceOf(EqualsAnyFilter::class, $criteriaMultiFilter->getQueries()[1]);
        static::assertSame('id', $criteriaMultiFilter->getQueries()[1]->getField());
        static::assertSame([], $criteria->getIds());
        static::assertSame([], $criteria->getAggregations());
        static::assertTrue(\in_array($state, $response->getResult()->getStates()));
    }

    public function testGetProductsUsePresentationCmsPageHaveProductStreamId(): void
    {
        $presentationCmsRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $presentationCmsPage->setPickedProductIds(null);
        $presentationCmsPage->setProductStreamId('test-product-stream-id');
        $entities = new EntitySearchResult(
            PresentationCmsPageEntity::class,
            1,
            new PresentationCmsPageCollection([$presentationCmsPage]),
            null,
            new Criteria(),
            $this->salesChannelContext->getContext()
        );
        $state = 'testState';
        $entities->addState($state);
        $presentationCmsRepository->expects(static::once())
            ->method('search')
            ->willReturn($entities);

        $listingLoader = $this->createMock(ProductListingLoader::class);
        $listingLoader->expects(static::once())
            ->method('load')
            ->willReturn($entities);

        $productStreamBuilder = $this->createMock(ProductStreamBuilder::class);
        $filters = [
            new AndFilter(),
            new OrFilter()
        ];
        $productStreamBuilder->expects(static::once())
            ->method('buildFilters')
            ->willReturn($filters);

        $route = $this->getRoute($presentationCmsRepository, $listingLoader, $productStreamBuilder);
        $criteria = new Criteria();
        $response = $route->products($this->validRequest, $this->salesChannelContext, $criteria);
        static::assertEquals(3, \count($criteria->getFilters()));
        static::assertInstanceOf(ProductAvailableFilter::class, $criteria->getFilters()[0]);
        static::assertInstanceOf(AndFilter::class, $criteria->getFilters()[1]);
        static::assertInstanceOf(OrFilter::class, $criteria->getFilters()[2]);
        static::assertSame([], $criteria->getAggregations());
        static::assertTrue(\in_array($state, $response->getResult()->getStates()));
    }

    public function testGetInteractionData(): void
    {
        $presentationCmsRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-presentation-cms-page-id');
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        $pickedProductIds = ['test-product-id'];
        $presentationCmsPage->setPickedProductIds($pickedProductIds);
        $entities = new EntitySearchResult(
            PresentationCmsPageEntity::class,
            1,
            new PresentationCmsPageCollection([$presentationCmsPage]),
            null,
            new Criteria(),
            $this->salesChannelContext->getContext()
        );
        $state = 'testState';
        $entities->addState($state);
        $presentationCmsRepository->expects(static::once())
            ->method('search')
            ->willReturn($entities);

        $listingLoader = $this->createMock(ProductListingLoader::class);
        $product = new ProductEntity();
        $product->setId('test-product-id');
        $products = new EntitySearchResult(
            ProductDefinition::ENTITY_NAME,
            1,
            new ProductCollection([$product]),
            null,
            new Criteria(),
            $this->salesChannelContext->getContext()
        );
        $listingLoader->expects(static::once())
            ->method('load')
            ->willReturn($products);

        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher->expects(static::exactly(3))
            ->method('dispatch');

        $route = $this->getRoute($presentationCmsRepository, $listingLoader, null, $eventDispatcher);

        $route->products($this->validRequest, $this->salesChannelContext, new Criteria());
    }

    private function getRoute(
        MockObject $presentationCmsPageRepositoryMock = null,
        MockObject $listingLoaderMock = null,
        MockObject $productStreamBuilderMock = null,
        MockObject $eventDispatcherMock = null
    ): PresentationSlideProductsRoute
    {
        /** @var MockObject&EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository */
        $presentationCmsPageRepository = $presentationCmsPageRepositoryMock ?:$this->createMock(EntityRepository::class);
        /** @var MockObject&ProductListingLoader $listingLoader */
        $listingLoader = $listingLoaderMock ?: $this->createMock(ProductListingLoader::class);
        /** @var MockObject&ProductStreamBuilder $productStreamBuilder */
        $productStreamBuilder = $productStreamBuilderMock ?: $this->createMock(ProductStreamBuilder::class);
        /** @var MockObject&EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $eventDispatcherMock ?? $this->createMock(EventDispatcherInterface::class);

        return new PresentationSlideProductsRoute(
            $presentationCmsPageRepository,
            $listingLoader,
            $productStreamBuilder,
            $eventDispatcher
        );
    }
}
