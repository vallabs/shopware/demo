<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\ShopPages;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\Exception\InvalidKeyException;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Presentation\SalesChannel\ShopPages\ShopPagesRoute;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSectionMockHelper;

class ShopPagesRouteTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetDecoratedThrowsException(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testGetShopPageWithInvalidConfigKey(): void
    {
        $pageName = 'invalidPage';

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('get')
            ->with('core.basicInformation.invalidPage')
            ->willReturn(null);

        $route = $this->getRoute($systemConfigService);

        static::expectException(InvalidKeyException::class);
        $route->getShopPage($pageName, $this->salesChannelContext);
    }

    public function testGetShopPageWithValidConfigKeyButPageIsNotFound(): void
    {
        $pageName = 'validPage';

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('get')
            ->with('core.basicInformation.validPage')
            ->willReturn('validPageId');

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                CmsPageDefinition::ENTITY_NAME,
                0,
                new CmsPageCollection(),
                null,
                new Criteria(),
                $this->salesChannelContext->getContext()
            ));

        $route = $this->getRoute($systemConfigService, $cmsPageRepository);

        static::expectException(EntityNotFoundException::class);
        $route->getShopPage($pageName, $this->salesChannelContext);
    }

    public function testGetShopPageWithValidConfigKeyButPageWithoutSectionsIsFound(): void
    {
        $pageName = 'validPage';

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('get')
            ->with('core.basicInformation.validPage')
            ->willReturn('validPageId');

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId('validPageId');
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                CmsPageDefinition::ENTITY_NAME,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $this->salesChannelContext->getContext()
            ));

        $route = $this->getRoute($systemConfigService, $cmsPageRepository);

        static::expectException(EntityNotFoundException::class);
        $route->getShopPage($pageName, $this->salesChannelContext);
    }

    public function testGetShopPageSuccessfullyButPage(): void
    {
        $pageName = 'validPage';

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('get')
            ->with('core.basicInformation.validPage')
            ->willReturn('validPageId');

        $cmsPageRepository = $this->createMock(EntityRepository::class);
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $section = (new CmsSectionMockHelper())->getCmsSectionEntity('test-cms-section-id', 'test-cms-page-id', null, null);
        $cmsPage->setSections(new CmsSectionCollection([$section]));
        $cmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                CmsPageDefinition::ENTITY_NAME,
                1,
                new CmsPageCollection([$cmsPage]),
                null,
                new Criteria(),
                $this->salesChannelContext->getContext()
            ));

        $notesElementHandler = $this->createMock(NotesElementHandler::class);
        $notesElementHandler->expects(static::once())->method('removeNotes');

        $route = $this->getRoute($systemConfigService, $cmsPageRepository, $notesElementHandler);
        $route->getShopPage($pageName, $this->salesChannelContext);
    }

    private function getRoute(
        ?MockObject $systemConfigServiceMock = null,
        ?MockObject $cmsPageRepositoryMock = null,
        ?MockObject $notesElementHandlerMock = null
    ): ShopPagesRoute
    {
        /** @var MockObject&SystemConfigService $systemConfigService */
        $systemConfigService = $systemConfigServiceMock ?? $this->createMock(SystemConfigService::class);
        /** @var MockObject&EntityRepository<CmsPageCollection> $cmsPageRepository */
        $cmsPageRepository = $cmsPageRepositoryMock ?? $this->createMock(EntityRepository::class);
        /** @var MockObject&NotesElementHandler $notesElementHandler */
        $notesElementHandler = $notesElementHandlerMock ?? $this->createMock(NotesElementHandler::class);

        return new ShopPagesRoute(
            $systemConfigService,
            $cmsPageRepository,
            $notesElementHandler
        );
    }
}
