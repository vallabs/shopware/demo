<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationStructure;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationStructureResponse;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationPageResults;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;

class PresentationStructureResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $presentationPageResults = new PresentationPageResults();
        $presentationPageResults->setCmsPage((new CmsPageMockHelper())->getCmsPageEntity('cms-page-a'));
        $response = new PresentationStructureResponse($presentationPageResults);
        static::assertSame($presentationPageResults, $response->getObject());
    }
}
