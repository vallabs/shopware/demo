<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\ShopPages;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Presentation\SalesChannel\ShopPages\ShopPagesResponse;

class ShopPagesResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $struct = new ArrayStruct([
            'test' => 'test',
        ]);

        $response = new ShopPagesResponse($struct);
        static::assertSame($struct, $response->getObject());
    }
}
