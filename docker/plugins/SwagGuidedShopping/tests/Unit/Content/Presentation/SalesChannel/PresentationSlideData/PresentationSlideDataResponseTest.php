<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationSlideData;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideData\PresentationSlideDataResponse;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;

class PresentationSlideDataResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page');
        $response = new PresentationSlideDataResponse($cmsPage);
        static::assertSame($cmsPage, $response->getObject());
    }
}
