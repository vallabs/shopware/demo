<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\SalesChannel\PresentationStructure;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationPageResult;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationPageResultCollection;
use SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure\PresentationPageResults;
use SwagGuidedShopping\Tests\Unit\MockBuilder\IteratorMockHelper;

class PresentationPageResultsTest extends TestCase
{
    public function testCmsPageResults(): void
    {
        $pageResults = new PresentationPageResults();
        static::assertEquals(new PresentationPageResultCollection(), $pageResults->getCmsPageResults());
        $pageResultCollection = new PresentationPageResultCollection([new PresentationPageResult()]);
        $pageResults->setCmsPageResults($pageResultCollection);
        static::assertSame($pageResultCollection, $pageResults->getCmsPageResults());
    }

    public function testNavigation(): void
    {
        $pageResults = new PresentationPageResults();
        $navigation = new IteratorMockHelper(['nav', 'nav-1', 'nav-2']);
        $pageResults->setNavigation($navigation);
        static::assertSame($navigation, $pageResults->getNavigation());
    }

}
