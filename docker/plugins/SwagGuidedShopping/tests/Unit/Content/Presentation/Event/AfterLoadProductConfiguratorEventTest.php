<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Event;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use SwagGuidedShopping\Content\Presentation\Event\AfterLoadProductConfiguratorEvent;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class AfterLoadProductConfiguratorEventTest extends TestCase
{
    public function testInitializeWithoutGroups(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $event = new AfterLoadProductConfiguratorEvent(null, $salesChannelContext);

        static::assertNull($event->getGroups());
        static::assertSame($salesChannelContext, $event->getSalesChannelContext());
        static::assertSame($salesChannelContext->getContext(), $event->getContext());
    }

    public function testInitializeWithGroups(): void
    {
        $group = new PropertyGroupEntity();
        $group->setId('test-group-id');
        $groups = new PropertyGroupCollection([$group]);
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $event = new AfterLoadProductConfiguratorEvent($groups, $salesChannelContext);

        static::assertSame($groups, $event->getGroups());
        static::assertSame($salesChannelContext, $event->getSalesChannelContext());
        static::assertSame($salesChannelContext->getContext(), $event->getContext());
    }
}
