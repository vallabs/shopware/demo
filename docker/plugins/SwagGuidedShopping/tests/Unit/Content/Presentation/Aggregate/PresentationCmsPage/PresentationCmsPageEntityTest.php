<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Aggregate\PresentationCmsPage;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\ProductStream\ProductStreamEntity;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;

class PresentationCmsPageEntityTest extends TestCase
{
    public function testGetPresentationId(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setPresentationId('test-presentation-id');
        static::assertSame('test-presentation-id', $presentationCmsPage->getPresentationId());
    }

    public function testGetCmsPageId(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setCmsPageId('test-cms-page-id');
        static::assertSame('test-cms-page-id', $presentationCmsPage->getCmsPageId());
    }

    public function testGetPosition(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setPosition(1);
        static::assertSame(1, $presentationCmsPage->getPosition());
    }

    public function testPresentationCmsPageIsInstantListing(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setInstantListing(true);
        static::assertTrue($presentationCmsPage->isInstantListing());
        $presentationCmsPage->setInstantListing(false);
        static::assertFalse($presentationCmsPage->isInstantListing());
    }

    public function testGetCmsPage(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getCmsPage());
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $presentationCmsPage->setCmsPage($cmsPage);
        static::assertSame($cmsPage, $presentationCmsPage->getCmsPage());
        $presentationCmsPage->setCmsPage(null);
        static::assertNull($presentationCmsPage->getCmsPage());
    }

    public function testGetPresentation(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getPresentation());
        $presentation = new PresentationEntity();
        $presentation->setId('test-presenation-id');
        $presentationCmsPage->setPresentation($presentation);
        static::assertSame($presentation, $presentationCmsPage->getPresentation());
        $presentationCmsPage->setPresentation(null);
        static::assertNull($presentationCmsPage->getPresentation());
    }

    public function testGetProductId(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getProductId());
        $presentationCmsPage->setProductId('test-product-id');
        static::assertSame('test-product-id', $presentationCmsPage->getProductId());
    }

    public function testGetProductVersionId(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setProductVersionId('test-product-version-id');
        static::assertSame('test-product-version-id', $presentationCmsPage->getProductVersionId());
    }

    public function testGetProduct(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getProduct());
        $product = new ProductEntity();
        $product->setId('test-product-id');
        $presentationCmsPage->setProduct($product);
        static::assertSame($product, $presentationCmsPage->getProduct());
        $presentationCmsPage->setProduct(null);
        static::assertNull($presentationCmsPage->getProduct());
    }

    public function testGetProductStreamId(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getProductStreamId());
        $presentationCmsPage->setProductStreamId('test-product-stream-id');
        static::assertSame('test-product-stream-id', $presentationCmsPage->getProductStreamId());
    }

    public function testGetProductStream(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getProductStream());
        $productStream = new ProductStreamEntity();
        $productStream->setId('test-product-stream-id');
        $presentationCmsPage->setProductStream($productStream);
        static::assertSame($productStream, $presentationCmsPage->getProductStream());
    }

    public function testGetPickedProductIds(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setPickedProductIds(['test-product-id']);
        static::assertSame(['test-product-id'], $presentationCmsPage->getPickedProductIds());
    }

    public function testGetTitle(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getTitle());
        $presentationCmsPage->setTitle('test-title');
        static::assertSame('test-title', $presentationCmsPage->getTitle());
        $presentationCmsPage->setTitle(null);
        static::assertNull($presentationCmsPage->getTitle());
    }

    public function testGetGuidedShoppingPresentationVersionId(): void
    {
        $presentationCmsPage = new PresentationCmsPageEntity();
        static::assertNull($presentationCmsPage->getGuidedShoppingPresentationVersionId());
        $presentationCmsPage->setGuidedShoppingPresentationVersionId('test-guided-shopping-presentation-version-id');
        static::assertSame('test-guided-shopping-presentation-version-id', $presentationCmsPage->getGuidedShoppingPresentationVersionId());
        $presentationCmsPage->setGuidedShoppingPresentationVersionId(null);
        static::assertNull($presentationCmsPage->getGuidedShoppingPresentationVersionId());
    }
}