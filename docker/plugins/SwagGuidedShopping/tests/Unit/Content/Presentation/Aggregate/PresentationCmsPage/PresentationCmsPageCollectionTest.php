<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Aggregate\PresentationCmsPage;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use Shopware\Core\Content\Cms\CmsPageCollection;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\PresentationMockHelper;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection
 *
 * @internal
 */
class PresentationCmsPageCollectionTest extends TestCase
{
    /**
     * @param array<string, mixed> $parameters
     */
    public function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testGetExpectedClass(): void
    {
        $presentationCmsPageCollection = new PresentationCmsPageCollection();
        static::assertEquals($this->invokeMethod($presentationCmsPageCollection, 'getExpectedClass', []),
            'SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity');
    }

    public function testGetApiAlias(): void
    {
        $expectedResult = 'guided_shopping_presentation_cms_page_collection';
        $presentationCmsPageCollection = new PresentationCmsPageCollection();
        $actualResult = $presentationCmsPageCollection->getApiAlias();
        static::assertEquals($actualResult, $expectedResult);
    }

    public function testFilterByCmsPageIdReturnFilterData(): void
    {
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);
        $actualResult = $presentationCmsPageCollection->filterByCmsPageId('cms-page-id-a');

        static::assertEquals(
            new PresentationCmsPageCollection([
                $presentationCmsPageEntities[0]
            ]),
            $actualResult
        );
    }

    public function testFilterByCmsPageIdReturnCollection(): void
    {
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);

        static::assertNotSame(
            new PresentationCmsPageCollection([
                $presentationCmsPageEntities[0]
            ]),
            $presentationCmsPageCollection->filterByCmsPageId('cms-page-id-b')
        );
    }

    public function testGetCmsPageIds(): void
    {
        $expectedResult = [
            'id-a' => 'cms-page-id-a',
            'id-b' => 'cms-page-id-b',
        ];
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);
        $actualResult = $presentationCmsPageCollection->getCmsPageIds();
        static::assertEquals($actualResult, $expectedResult);
    }

    public function testFilterByPresentationIdReturnFilterData(): void
    {
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);
        $actualResult = $presentationCmsPageCollection->filterByPresentationId('presentation-id-a');

        static::assertEquals(
            new PresentationCmsPageCollection([
                $presentationCmsPageEntities[0]
            ]),
            $actualResult
        );
    }

    public function testFilterByPresentationIdReturnCollection(): void
    {
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);
        static::assertNotSame(
            new PresentationCmsPageCollection([
                $presentationCmsPageEntities[0]
            ]),
            $presentationCmsPageCollection->filterByPresentationId('presentation-id-a')
        );
    }

    public function testPresentationIds(): void
    {
        $expectedResult = [
            'id-a' => 'presentation-id-a',
            'id-b' => 'presentation-id-b',
        ];
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);
        $actualResult = $presentationCmsPageCollection->getPresentationIds();
        static::assertEquals($actualResult, $expectedResult);
    }

    public function testGetCmsPage(): void
    {
        $expectedResult = new CmsPageCollection([
            (new CmsPageMockHelper())->getCmsPageEntity('cms-page-id-a'),
            (new CmsPageMockHelper())->getCmsPageEntity('cms-page-id-b')
        ]);
        $presentationCmsPageEntities = $this->preparePresentationCmsPageEntities();
        $presentationCmsPageCollection = new PresentationCmsPageCollection($presentationCmsPageEntities);
        $actualResult = $presentationCmsPageCollection->getCmsPage();
        static::assertEquals($actualResult, $expectedResult);
    }

    /**
     * @return array<int, mixed>
     */
    private function preparePresentationCmsPageEntities(): array
    {
        $presentationCmsPageEntityA = new PresentationCmsPageEntity();
        $presentationCmsPageEntityA->setId('id-a');
        $presentationCmsPageEntityA->setCmsPage((new CmsPageMockHelper())->getCmsPageEntity('cms-page-id-a'));
        $presentationCmsPageEntityA->setCmsPageId('cms-page-id-a');
        $presentationCmsPageEntityA->setPresentation((new PresentationMockHelper())->getPresentationEntity('presentation-id-a'));
        $presentationCmsPageEntityA->setPresentationId('presentation-id-a');

        $presentationCmsPageEntityB = new PresentationCmsPageEntity();
        $presentationCmsPageEntityB->setId('id-b');
        $presentationCmsPageEntityB->setCmsPage((new CmsPageMockHelper())->getCmsPageEntity('cms-page-id-b'));
        $presentationCmsPageEntityB->setCmsPageId('cms-page-id-b');
        $presentationCmsPageEntityB->setPresentation((new PresentationMockHelper())->getPresentationEntity('presentation-id-b'));
        $presentationCmsPageEntityB->setPresentationId('presentation-id-b');

        return [
            $presentationCmsPageEntityA,
            $presentationCmsPageEntityB
        ];
    }
}
