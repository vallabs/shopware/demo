<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Aggregate\PresentationCmsPageTranslation;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPageTranslation\PresentationCmsPageTranslationEntity;

class PresentationCmsPageTranslationEntityTest extends TestCase
{
    public function testGetName(): void
    {
        $translation = new PresentationCmsPageTranslationEntity();
        static::assertNull($translation->getName());
        $translation->setName('test-name');
        static::assertSame('test-name', $translation->getName());
        $translation->setName(null);
        static::assertNull($translation->getName());
    }

    public function testGetGuidedShoppingPresentationCmsPageId(): void
    {
        $translation = new PresentationCmsPageTranslationEntity();
        $translation->setGuidedShoppingPresentationCmsPageId('test-id');
        static::assertSame('test-id', $translation->getGuidedShoppingPresentationCmsPageId());
    }

    public function testGetGuidedShoppingPresentationCmsPage(): void
    {
        $translation = new PresentationCmsPageTranslationEntity();
        $presentationCmsPage = new PresentationCmsPageEntity();
        $presentationCmsPage->setId('test-id');
        $translation->setGuidedShoppingPresentationCmsPage($presentationCmsPage);
        static::assertSame($presentationCmsPage, $translation->getGuidedShoppingPresentationCmsPage());
    }
}