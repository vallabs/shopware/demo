<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Aggregate\PresentationTranslation;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationEntity;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;

class PresentationTranslationEntityTest extends TestCase
{
    public function testGetName(): void
    {
        $translation = new PresentationTranslationEntity();
        $translation->setName('test-name');
        static::assertSame('test-name', $translation->getName());
    }

    public function testGetGuidedShoppingPresentationId(): void
    {
        $translation = new PresentationTranslationEntity();
        $translation->setGuidedShoppingPresentationId('test-id');
        static::assertSame('test-id', $translation->getGuidedShoppingPresentationId());
    }

    public function testGetGuidedShoppingPresentation(): void
    {
        $translation = new PresentationTranslationEntity();
        $presentation = new PresentationEntity();
        $presentation->setId('test-presentation-id');
        $translation->setGuidedShoppingPresentation($presentation);
        static::assertSame($presentation, $translation->getGuidedShoppingPresentation());
    }
}