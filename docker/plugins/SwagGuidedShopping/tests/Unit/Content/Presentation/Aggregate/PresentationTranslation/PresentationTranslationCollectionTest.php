<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Aggregate\PresentationTranslation;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationEntity;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationCollection
 *
 * @internal
 */
class PresentationTranslationCollectionTest extends TestCase
{
    /**
     * @param array<string, mixed> $parameters
     */
    public function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testGetExpectedClass(): void
    {
        $presentationTranslationCollection = new PresentationTranslationCollection();
        static::assertEquals($this->invokeMethod($presentationTranslationCollection, 'getExpectedClass', []),
            'SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationEntity');
    }

    public function testFilterByLanguageIdReturnFilterData(): void
    {
        $presentationTranslationEntities = $this->preparePresentationEntities();
        $presentationTranslationCollection = new PresentationTranslationCollection($presentationTranslationEntities);

        static::assertEquals(
            new PresentationTranslationCollection([
                $presentationTranslationEntities[0]
            ]),
            $presentationTranslationCollection->filterByLanguageId('filter-a')
        );
    }

    public function testFilterByLanguageIdReturnCollection(): void
    {
        $presentationTranslationEntities = $this->preparePresentationEntities();
        $presentationTranslationCollection = new PresentationTranslationCollection($presentationTranslationEntities);

        static::assertNotSame(
            new PresentationTranslationCollection([
                $presentationTranslationEntities[0]
            ]),
            $presentationTranslationCollection->filterByLanguageId('filter-b')
        );
    }

    /**
     * @return array<int, mixed>
     */
    private function preparePresentationEntities(): array
    {
        $presentationTranslationEntityA = new PresentationTranslationEntity();
        $presentationTranslationEntityA->setUniqueIdentifier('test-a');
        $presentationTranslationEntityA->setLanguageId('filter-a');

        $presentationTranslationEntityB = new PresentationTranslationEntity();
        $presentationTranslationEntityB->setUniqueIdentifier('test-b');
        $presentationTranslationEntityB->setLanguageId('filter-b');

        return [
            $presentationTranslationEntityA,
            $presentationTranslationEntityB
        ];
    }
}
