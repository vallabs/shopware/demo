<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\ContextSource;
use Shopware\Core\Framework\Api\Context\SalesChannelApiSource;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\CashRoundingConfig;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\DataAbstractionLayer\Write\CloneBehavior;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Cms\Service\CmsPageCreatorService;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsPageMockHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\PresentationMockHelper;

class PresentationVersionHandlerTest extends TestCase
{
    protected Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    /**
     * @dataProvider getTestCreatePresentationVersionSuccessfullyProviderData
     */
    public function testCreatePresentationVersionSuccessfully(
        AppointmentEntity $appointment,
        MockObject $presentationRepository,
        MockObject $appointmentRepository,
        string $expectVersionId,
        Context $context
    ): void
    {
        $handler = $this->getHandler($presentationRepository, null, $appointmentRepository);
        $versionId = $handler->createPresentationVersion($appointment, $context);
        static::assertEquals($expectVersionId, $versionId);
    }

    public static function getTestCreatePresentationVersionSuccessfullyProviderData(): \Generator
    {
        $self = new PresentationVersionHandlerTest('test');
        $context = new Context(new SystemSource());
        $noRunPresentationRepository = $self->getNoRunPresentationRepository();
        $noRunAppointmentRepository = $self->getNoRunAppointmentRepository();

        $selfDefaultAppointmentWithPresentationVersion = $self->getAppointment('test-presentation-version-id');
        $selfDefaultAppointmentWithPresentationVersion->setDefault(true);
        $selfDefaultAppointmentWithPresentationVersion->setMode(AppointmentDefinition::MODE_SELF);
        yield 'self-default-appointment-with-presentation-version-id' => [
            $selfDefaultAppointmentWithPresentationVersion,
            $noRunPresentationRepository,
            $noRunAppointmentRepository,
            Defaults::LIVE_VERSION,
            $context
        ];

        $selfDefaultAppointmentWithoutPresentationVersion = $self->getAppointment('test-presentation-version-id');
        $selfDefaultAppointmentWithoutPresentationVersion->setDefault(true);
        $selfDefaultAppointmentWithoutPresentationVersion->setMode(AppointmentDefinition::MODE_SELF);
        $selfDefaultAppointmentWithoutPresentationVersion->setPresentationVersionId(null);
        yield 'self-default-appointment-without-presentation-version-id-' => [
            $selfDefaultAppointmentWithoutPresentationVersion,
            $noRunPresentationRepository,
            $noRunAppointmentRepository,
            Defaults::LIVE_VERSION,
            $context
        ];

        $selfAppointmentWithPresentationVersion = $self->getAppointment('test-presentation-version-id');
        $selfAppointmentWithPresentationVersion->setMode(AppointmentDefinition::MODE_SELF);
        yield 'self-appointment-with-presentation-version-id' => [
            $selfAppointmentWithPresentationVersion,
            $noRunPresentationRepository,
            $noRunAppointmentRepository,
            'test-presentation-version-id',
            $context
        ];

        $selfAppointmentWithoutPresentationVersion = $self->getAppointment('test-presentation-version-id');
        $selfAppointmentWithoutPresentationVersion->setMode(AppointmentDefinition::MODE_SELF);
        $selfAppointmentWithoutPresentationVersion->setPresentationVersionId(null);
        yield 'self-appointment-without-presentation-version-id' => [
            $selfAppointmentWithoutPresentationVersion,
            $self->getRunPresentationRepository($selfAppointmentWithoutPresentationVersion, $context),
            $self->getRunAppointmentRepository($selfAppointmentWithoutPresentationVersion, $context),
            'test-create-new-presentation-version-id',
            $context
        ];

        $defaultAppointmentWithPresentationVersion = $self->getAppointment('test-presentation-version-id');
        $defaultAppointmentWithPresentationVersion->setDefault(true);
        yield 'default-appointment-with-presentation-version-id' => [
            $defaultAppointmentWithPresentationVersion,
            $noRunPresentationRepository,
            $noRunAppointmentRepository,
            'test-presentation-version-id',
            $context
        ];

        $defaultAppointmentWithoutPresentationVersion = $self->getAppointment('test-presentation-version-id');
        $defaultAppointmentWithoutPresentationVersion->setDefault(true);
        $defaultAppointmentWithoutPresentationVersion->setPresentationVersionId(null);
        yield 'default-appointment-without-presentation-version-id' => [
            $defaultAppointmentWithoutPresentationVersion,
            $self->getRunPresentationRepository($defaultAppointmentWithoutPresentationVersion, $context),
            $self->getRunAppointmentRepository($defaultAppointmentWithoutPresentationVersion, $context),
            'test-create-new-presentation-version-id',
            $context
        ];

        $appointmentWithPresentationVersionId = $self->getAppointment('test-presentation-version-id');
        yield 'appointment-with-presentation-version-id' => [
            $appointmentWithPresentationVersionId,
            $noRunPresentationRepository,
            $noRunAppointmentRepository,
            'test-presentation-version-id',
            $context
        ];

        $appointmentWithoutPresentationVersionId = $self->getAppointment('test-presentation-version-id');
        $appointmentWithoutPresentationVersionId->setPresentationVersionId(null);
        yield 'appointment-without-presentation-version-id' => [
            $appointmentWithoutPresentationVersionId,
            $self->getRunPresentationRepository($appointmentWithoutPresentationVersionId, $context),
            $self->getRunAppointmentRepository($appointmentWithoutPresentationVersionId, $context),
            'test-create-new-presentation-version-id',
            $context
        ];
    }


    /**
     * @param array<int, mixed> $ruleIds
     * @param array<int, mixed> $languageIds
     * @dataProvider getTestGetContextForAppointmentWithNoRoundingContextProviderData
     */
    public function testGetContextForAppointmentWithNoRoundingContext(
        string $presentationVersionId,
        ContextSource $source,
        array $ruleIds,
        string $currencyId,
        array $languageIds,
        float $currencyFactor,
        bool $considerInheritance,
        string $taxState
    ): void
    {
        $context = new Context(
            $source,
            $ruleIds,
            $currencyId,
            $languageIds,
            $presentationVersionId,
            $currencyFactor,
            $considerInheritance,
            $taxState
        );
        $handler = $this->getHandler();

        $newContext = $handler->getContextForAppointment($presentationVersionId, $context);

        static::assertNotSame($source, $newContext->getSource());
        static::assertInstanceOf(SystemSource::class, $newContext->getSource());
        static::assertEquals($ruleIds, $newContext->getRuleIds());
        static::assertEquals($currencyId, $newContext->getCurrencyId());
        static::assertEquals($languageIds, $newContext->getLanguageIdChain());
        static::assertEquals($presentationVersionId, $newContext->getVersionId());
        static::assertEquals($currencyFactor, $newContext->getCurrencyFactor());
        static::assertEquals($considerInheritance, $newContext->considerInheritance());
        static::assertEquals($taxState, $newContext->getTaxState());
        static::assertEquals(new CashRoundingConfig(2, 0.01, true), $newContext->getRounding());
    }

    public static function getTestGetContextForAppointmentWithNoRoundingContextProviderData(): \Generator
    {
        yield 'admin-api-source' => [
            'presentation-version-id',
            new AdminApiSource('user-id'),
            [],
            '',
            ['test-language-id'],
            0,
            false,
            ''
        ];

        yield 'sales-channel-api-source' => [
            'presentation-version-id',
            new SalesChannelApiSource('sales-channel-id'),
            [],
            '',
            ['test-language-id'],
            0,
            false,
            ''
        ];

        yield 'system-source' => [
            'presentation-version-id',
            new SystemSource(),
            ['test-rule-id'],
            'test-currency-id',
            ['test-language-id'],
            1.1,
            false,
            'tax-state-id'
        ];

        yield 'system-source-without-rule-id' => [
            'presentation-version-id',
            new SystemSource(),
            [],
            'test-currency-id',
            ['test-language-id'],
            1.1,
            false,
            'tax-state-id'
        ];
    }

    /**
     * @return void
     */
    public function testGetContextForAppointmentWithRoundingContext(): void
    {
        $presentationVersionId = 'presentation-version-id';
        $source = new SystemSource();
        $cashRoundingConfig = new CashRoundingConfig(3, 1.0, true);

        $context = new Context(
            $source,
            ['rule-id'],
            'currency-id',
            ['language-id'],
            $presentationVersionId,
            1.0,
            false,
            'tax-state',
            $cashRoundingConfig
        );

        $handler = $this->getHandler();

        $newContext = $handler->getContextForAppointment($presentationVersionId, $context);

        static::assertNotSame($source, $newContext->getSource());
        static::assertInstanceOf(SystemSource::class, $newContext->getSource());
        static::assertEquals(['rule-id'], $newContext->getRuleIds());
        static::assertEquals('currency-id', $newContext->getCurrencyId());
        static::assertEquals(['language-id'], $newContext->getLanguageIdChain());
        static::assertEquals($presentationVersionId, $newContext->getVersionId());
        static::assertEquals(1.0, $newContext->getCurrencyFactor());
        static::assertFalse($newContext->considerInheritance());
        static::assertEquals('tax-state', $newContext->getTaxState());
        static::assertEquals($cashRoundingConfig, $newContext->getRounding());
    }

    /**
     * @dataProvider getTestUpdatePresentationCmsPagesUsingAppointmentAlreadyStartedOrEndedProviderData
     */
    public function testUpdatePresentationCmsPagesUsingAppointmentAlreadyStartedOrEnded(AppointmentEntity $appointment): void
    {
        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPageRepository->expects(static::never())->method('search');
        $presentationCmsPageRepository->expects(static::never())->method('delete');
        $presentationCmsPageRepository->expects(static::never())->method('clone');

        $cmsPageCreatorService = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreatorService->expects(static::never())->method('updateSlidePositions');

        $handler = $this->getHandler(null, $presentationCmsPageRepository, null, $cmsPageCreatorService);
        $handler->updatePresentationCmsPages($appointment, 'test-presentation-version-id', $this->context);
    }

    public static function getTestUpdatePresentationCmsPagesUsingAppointmentAlreadyStartedOrEndedProviderData(): \Generator
    {
        $self = new PresentationVersionHandlerTest('test');

        $appointmentAlreadyStarted = $self->getAppointment('test-presentation-version-id');
        $appointmentAlreadyStarted->setStartedAt(new \DateTime('now'));
        yield 'already-started' => [
            $appointmentAlreadyStarted
        ];

        $appointmentAlreadyEnded = $self->getAppointment('test-presentation-version-id');
        $appointmentAlreadyEnded->setEndedAt(new \DateTime('now'));
        yield 'already-ended' => [
            $appointmentAlreadyEnded
        ];

        $appointmentAlreadyBoth = $self->getAppointment('test-presentation-version-id');
        $appointmentAlreadyBoth->setStartedAt(new \DateTime('now'));
        $appointmentAlreadyBoth->setEndedAt(new \DateTime('now'));
        yield 'already-both' => [
            $appointmentAlreadyBoth
        ];
    }

    public function testUpdatePresentationCmsPagesWithoutOriginalCmsPages(): void
    {
        $appointmentWithPresentation = $this->getAppointment('test-suite');

        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationCmsPageDefinition::ENTITY_NAME,
                    0,
                    new PresentationCmsPageCollection(),
                    null,
                    new Criteria(),
                    $this->context
                )
            );

        $presentationCmsPageRepository->expects(static::once())
            ->method('delete')
            ->with(static::equalTo([['id' => 'test-original-cms-page-id']]));

        $cmsPageCreatorService = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreatorService->expects(static::never())->method('updateSlidePositions');

        $handler = $this->getHandler(null, $presentationCmsPageRepository, null, $cmsPageCreatorService);
        $handler->updatePresentationCmsPages($appointmentWithPresentation, 'test-suite', $this->context);
    }

    /**
     * @dataProvider getTestUpdatePresentationCmsPageProviderData
     */
    public function testUpdatePresentationCmsPage(
        string $presentationVersionId,
        AppointmentEntity $appointment,
        MockObject $presentationCmsPageRepository,
        Context $context
    ): void
    {
        $cmsPageCreatorService = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreatorService->expects(static::once())
            ->method('updateSlidePositions')
            ->with(
                static::equalTo(1),
                static::equalTo('test-suite'),
                static::equalTo('presentation_test_1'),
                static::equalTo($context)
            );
        $handler = $this->getHandler(null, $presentationCmsPageRepository, null, $cmsPageCreatorService);
        $handler->updatePresentationCmsPages($appointment, $presentationVersionId, $context);
    }

    public static function getTestUpdatePresentationCmsPageProviderData(): \Generator
    {
        $self = new PresentationVersionHandlerTest('test');
        $context = new Context(new SystemSource());
        $originalCmsPageDiffPositionWithInstantListing = $self->createOriginalCmsPage('test-suite', 2);
        $originalCmsPageSamePositionWithInstantListing = $self->createOriginalCmsPage('test-suite', 3);

        $appointmentWithPresentation = $self->getAppointment('test-suite');
        yield 'using-appointment-with-presentation-includes-instant-listing-position-same-with-original-cms-page-position' => [
            'test-suite',
            $appointmentWithPresentation,
            $self->getPresentationCmsPageRepositoryMockUsingAppointmentWithPresentation(
                'test-suite',
                $originalCmsPageSamePositionWithInstantListing,
                2,
                $context
            ),
            $context
        ];
        yield 'using-appointment-with-presentation-includes-instant-listing-position-diff-with-original-cms-page-position' => [
            'test-suite',
            $appointmentWithPresentation,
            $self->getPresentationCmsPageRepositoryMockUsingAppointmentWithPresentation(
                'test-suite',
                $originalCmsPageDiffPositionWithInstantListing,
                2,
                $context
            ),
            $context
        ];

        $appointmentWithoutPresentation = new AppointmentEntity();
        $appointmentWithoutPresentation->assign([
            'id' => 'test-appointment-id',
            'presentationId' => 'presentation_test_1',
        ]);

        yield 'using-appointment-without-presentation-includes-instant-listing-position-same-with-original-cms-page-position' => [
            'test-suite',
            $appointmentWithoutPresentation,
            $self->getPresentationCmsPageRepositoryMockUsingAppointmentWithoutPresentation(
                'test-suite',
                $self->getPresentationCmsPages('test-suite'),
                $originalCmsPageSamePositionWithInstantListing,
                2,
                $context
            ),
            $context
        ];

        yield 'using-appointment-without-presentation-includes-instant-listing-position-diff-with-original-cms-page-position' => [
            'test-suite',
            $appointmentWithoutPresentation,
            $self->getPresentationCmsPageRepositoryMockUsingAppointmentWithoutPresentation(
                'test-suite',
                $self->getPresentationCmsPages('test-suite'),
                $originalCmsPageDiffPositionWithInstantListing,
                2,
                $context
            ),
            $context
        ];
    }

    private function getHandler(
        ?MockObject $presentationRepositoryMock = null,
        ?MockObject $presentationCmsPageRepositoryMock = null,
        ?MockObject $appointmentRepositoryMock = null,
        ?MockObject $cmsPageCreatorServiceMock = null
    ): PresentationVersionHandler
    {
        /** @var MockObject&EntityRepository<PresentationCollection> $presentationRepository */
        $presentationRepository = $presentationRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository */
        $presentationCmsPageRepository = $presentationCmsPageRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&CmsPageCreatorService $cmsPageCreatorService */
        $cmsPageCreatorService = $cmsPageCreatorServiceMock ?: $this->createMock(CmsPageCreatorService::class);

        return new PresentationVersionHandler($presentationRepository, $presentationCmsPageRepository, $appointmentRepository, $cmsPageCreatorService);
    }

    private function getAppointment(string $presentationVersionId): AppointmentEntity
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $presentationId = $appointment->getPresentationId();
        $appointment->setStartedAt(null);
        $appointment->setEndedAt(null);
        $presentation = (new PresentationMockHelper())->getPresentationEntity($presentationId);
        $existingPresentationCmsPages = $this->getPresentationCmsPages($presentationVersionId);
        $presentation->setCmsPages($existingPresentationCmsPages);
        $appointment->setPresentation($presentation);
        $appointment->setPresentationVersionId($presentationVersionId);

        return $appointment;
    }

    private function getPresentationCmsPages(string $presentationVersionId): PresentationCmsPageCollection
    {
        $presentationCmsPages = [];

        $instantListing = $this->createInstantListing($presentationVersionId, 1);
        $presentationCmsPages[] = $instantListing;

        $originalCmsPage = $this->createOriginalCmsPage($presentationVersionId, 2);
        $presentationCmsPages[] = $originalCmsPage;

        $instantListing2 = $this->createInstantListing($presentationVersionId, 3);
        $presentationCmsPages[] = $instantListing2;

        return new PresentationCmsPageCollection($presentationCmsPages);
    }

    private function createInstantListing(string $presentationVersionId, int $position): PresentationCmsPageEntity
    {
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $instantListing = new PresentationCmsPageEntity();
        $instantListing->setId('test-instant-listing-page-id-' . $position);
        $instantListing->setCmsPageId($cmsPage->getId());
        $instantListing->setCmsPage($cmsPage);
        $instantListing->setInstantListing(true);
        $instantListing->setGuidedShoppingPresentationVersionId($presentationVersionId);
        $instantListing->setPosition($position);

        return $instantListing;
    }

    private function createOriginalCmsPage(string $presentationVersionId, int $position): PresentationCmsPageEntity
    {
        $cmsPage = (new CmsPageMockHelper())->getCmsPageEntity('test-cms-page-id');
        $originalCmsPage = new PresentationCmsPageEntity();
        $originalCmsPage->setId('test-original-cms-page-id');
        $originalCmsPage->setCmsPageId($cmsPage->getId());
        $originalCmsPage->setCmsPage($cmsPage);
        $originalCmsPage->setInstantListing(false);
        $originalCmsPage->setGuidedShoppingPresentationVersionId($presentationVersionId);
        $originalCmsPage->setPosition($position);

        return $originalCmsPage;
    }

    private function getPresentationCmsPageRepositoryMockUsingAppointmentWithPresentation(
        string $presentationVersionId,
        PresentationCmsPageEntity $originalCmsPage,
        int $expectPosition,
        Context $context
    ): MockObject
    {
        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationId', 'presentation_test_1'))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', Defaults::LIVE_VERSION))
            ->addFilter(new EqualsFilter('isInstantListing', false))
            ->addSorting(new FieldSorting('position'));
        $originalCollection = new PresentationCmsPageCollection([$originalCmsPage]);
        $presentationCmsPageRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new EntitySearchResult(
                PresentationCmsPageEntity::class,
                1,
                $originalCollection,
                null,
                $criteria,
                $context
            ));
        $removeIds = \array_map(function (string $id) {
            return ['id' => $id];
        }, \array_values($originalCollection->getIds()));
        $presentationCmsPageRepository->expects(static::once())
            ->method('delete')
            ->with(
                static::equalTo($removeIds),
                static::equalTo($context)
            );
        $modifyData = [
            'guidedShoppingPresentationVersionId' => $presentationVersionId,
            'cmsPageVersionId' => $originalCmsPage->getVersionId(),
            'createdAt' => $originalCmsPage->getCreatedAt()
        ];
        if ($expectPosition > 0) {
            $modifyData['position'] = $expectPosition;
        }
        $presentationCmsPageRepository->expects(static::once())
            ->method('clone')
            ->with(
                static::equalTo($originalCmsPage->getId()),
                static::equalTo($context),
                static::equalTo(null),
                static::equalTo(new CloneBehavior($modifyData))
            );

        return $presentationCmsPageRepository;
    }

    private function getPresentationCmsPageRepositoryMockUsingAppointmentWithoutPresentation(
        string $presentationVersionId,
        PresentationCmsPageCollection $existingCmsPages,
        PresentationCmsPageEntity $originalCmsPage,
        int $expectPosition,
        Context $context
    ): MockObject
    {
        $presentationCmsPageRepository = $this->createMock(EntityRepository::class);
        $existingCriteria = new Criteria();
        $existingCriteria->addFilter(new EqualsFilter('presentationId', 'presentation_test_1'))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', $presentationVersionId))
            ->addSorting(new FieldSorting('position'));
        $originalCriteria = new Criteria();
        $originalCriteria->addFilter(new EqualsFilter('presentationId', 'presentation_test_1'))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', Defaults::LIVE_VERSION))
            ->addFilter(new EqualsFilter('isInstantListing', false))
            ->addSorting(new FieldSorting('position'));
        $originalCollection = new PresentationCmsPageCollection([$originalCmsPage]);
        $presentationCmsPageRepository->expects(static::exactly(2))
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    PresentationCmsPageEntity::class,
                    $existingCmsPages->count(),
                    $existingCmsPages,
                    null,
                    $existingCriteria,
                    $context
                ),
                new EntitySearchResult(
                    PresentationCmsPageEntity::class,
                    1,
                    $originalCollection,
                    null,
                    $originalCriteria,
                    $context
                )
            );
        $removeIds = \array_map(function (string $id) {
            return ['id' => $id];
        }, \array_values($originalCollection->getIds()));
        $presentationCmsPageRepository->expects(static::once())
            ->method('delete')
            ->with(
                static::equalTo($removeIds),
                static::equalTo($context)
            );
        $modifyData = [
            'guidedShoppingPresentationVersionId' => $presentationVersionId,
            'cmsPageVersionId' => $originalCmsPage->getVersionId(),
            'createdAt' => $originalCmsPage->getCreatedAt()
        ];
        if ($expectPosition > 0) {
            $modifyData['position'] = $expectPosition;
        }
        $presentationCmsPageRepository->expects(static::once())
            ->method('clone')
            ->with(
                static::equalTo($originalCmsPage->getId()),
                static::equalTo($context),
                static::equalTo(null),
                static::equalTo(new CloneBehavior($modifyData))
            );

        return $presentationCmsPageRepository;
    }

    private function getNoRunPresentationRepository(): MockObject
    {
        $noRunPresentationRepository = $this->createMock(EntityRepository::class);
        $noRunPresentationRepository->expects(static::never())->method('createVersion');

        return $noRunPresentationRepository;
    }

    private function getRunPresentationRepository(AppointmentEntity $appointment, Context $context): MockObject
    {
        $runPresentationRepository = $this->createMock(EntityRepository::class);
        $runPresentationRepository->expects(static::once())
            ->method('createVersion')
            ->with(
                static::equalTo($appointment->getPresentationId()),
                static::equalTo($context),
                static::equalTo( 'version for appointment ' . $appointment->getId())
            )->willReturn('test-create-new-presentation-version-id');

        return $runPresentationRepository;
    }

    private function getNoRunAppointmentRepository(): MockObject
    {
        $noRunAppointmentRepository = $this->createMock(EntityRepository::class);
        $noRunAppointmentRepository->expects(static::never())->method('update');

        return $noRunAppointmentRepository;
    }

    private function getRunAppointmentRepository(AppointmentEntity $appointment, Context $context): MockObject
    {
        $runAppointmentRepository = $this->createMock(EntityRepository::class);
        $runAppointmentRepository->expects(static::once())
            ->method('update')
            ->with(
                static::equalTo([
                    [
                        'id' => $appointment->getId(),
                        'guidedShoppingPresentationVersionId' => 'test-create-new-presentation-version-id'
                    ]
                ]),
                static::equalTo($context)
            );

        return $runAppointmentRepository;
    }
}
