<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\Common\IteratorFactory;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\Common\LastIdQuery;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\QueryBuilder;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use SwagGuidedShopping\Content\Presentation\PresentationAdminSearchIndexer;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;

class PresentationAdminSearchIndexerTest extends TestCase
{
    public function testGetDecorated(): void
    {
        $searchIndexer = $this->getSearchIndexer();
        static::expectException(DecorationPatternException::class);
        $searchIndexer->getDecorated();
    }

    public function testGetName(): void
    {
        $searchIndexer = $this->getSearchIndexer();
        static::assertSame('guided_shopping_presentation-listing', $searchIndexer->getName());
    }

    public function testGetEntity(): void
    {
        $searchIndexer = $this->getSearchIndexer();
        static::assertSame('guided_shopping_presentation', $searchIndexer->getEntity());
    }

    public function testGetIterator(): void
    {
        $iterator = $this->createMock(IteratorFactory::class);
        $query = new LastIdQuery($this->createMock(QueryBuilder::class));
        $iterator->expects(static::once())
            ->method('createIterator')
            ->with(
                static::equalTo('guided_shopping_presentation'),
                static::isNull(),
                static::equalTo(100)
            )
            ->willReturn($query);

        $searchIndexer = $this->getSearchIndexer($iterator);
        $result = $searchIndexer->getIterator();
        static::assertSame($query, $result);
    }

    public function testFetch(): void
    {
        $id = \md5('test-id');

        $connection = $this->createMock(Connection::class);
        $connectionResult = $this->createMock(Result::class);
        $connectionResult->expects(static::once())
            ->method('fetchAllAssociative')
            ->willReturn([
                'test-id' => [
                    'id' => 'test-id',
                    'name' => 'test-name'
                ]
            ]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($connectionResult);

        $searchIndexer = $this->getSearchIndexer(null, $connection);

        $result = $searchIndexer->fetch([$id]);
        static::assertArrayHasKey('test-id', $result);
        static ::assertEquals('test-id', $result['test-id']['id']);
        static ::assertEquals('test-id test-name', $result['test-id']['text']);
    }

    /**
     * @dataProvider getTestGlobalDataProviderData
     *
     * @param array<string, mixed> $result
     * @param array<string>|null $expectedCriteriaIds
     */
    public function testGlobalData(
        array $result,
        ?array $expectedCriteriaIds,
        int $expectedTotal
    ): void
    {
        $context = new Context(new SystemSource());

        $repository = $this->createMock(EntityRepository::class);
        /** @var EntityCollection<Entity> $entities */
        $entities = new EntityCollection([]);
        $repository->expects(static::once())
            ->method('search')
            ->with(
                static::callback(function (Criteria $criteria) use ($expectedCriteriaIds) {
                    return $criteria->getIds() === $expectedCriteriaIds;
                })
            )->willReturn(new EntitySearchResult(
                'test-entity',
                0,
                $entities,
                null,
                new Criteria(),
                $context
            ));

        $searchIndexer = $this->getSearchIndexer(null, null, $repository);

        $data = $searchIndexer->globalData($result, $context);
        static::assertEquals($expectedTotal, $data['total']);
        static::assertSame($entities, $data['data']);
    }

    public static function getTestGlobalDataProviderData(): \Generator
    {
        yield 'no hits' => [
            ['total' => 1],
            [],
            1
        ];

        yield 'hits, but no total' => [
            ['hits' => [['id' => 'test-id']]],
            ['test-id'],
            0
        ];

        yield 'hits and total' => [
            ['hits' => [['id' => 'test-id']], 'total' => 1],
            ['test-id'],
            1
        ];

        yield 'hits and total, but no hits id' => [
            ['hits' => [['test-key' => 'test-id']], 'total' => 1],
            [],
            1
        ];

        yield 'nothing' => [
            [],
            [],
            0
        ];
    }

    private function getSearchIndexer(
        ?MockObject $iteratorFactoryMock = null,
        ?MockObject $connectionMock = null,
        ?MockObject $repositoryMock = null
    ): PresentationAdminSearchIndexer
    {
        /** @var MockObject&IteratorFactory $iteratorFactory */
        $iteratorFactory = $iteratorFactoryMock ?? $this->createMock(IteratorFactory::class);
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ??$this->createMock(Connection::class);
        /** @var MockObject&EntityRepository<PresentationCollection> $repository */
        $repository = $repositoryMock ??$this->createMock(EntityRepository::class);

        return new PresentationAdminSearchIndexer(
            $iteratorFactory,
            $connection,
            $repository,
            100
        );
    }
}
