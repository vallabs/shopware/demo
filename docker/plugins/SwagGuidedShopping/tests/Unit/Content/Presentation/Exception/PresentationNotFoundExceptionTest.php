<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Exception\PresentationNotFoundException;

class PresentationNotFoundExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new PresentationNotFoundException();

        static::assertSame('Presentation not found.', $exception->getMessage());
        static::assertSame('GUIDED_SHOPPING__PRESENTATION_NOT_FOUND', $exception->getErrorCode());
        static::assertSame(404, $exception->getStatusCode());
    }
}