<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class SalesChannelContextMissingAttendeeExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $exception = new SalesChannelContextMissingAttendeeException();
        $expectMessage = 'Sales channel context missing attendee.';
        static::assertEquals($expectMessage, $exception->getMessage());
    }

    public function testGetErrorCode(): void
    {
        $exception = new SalesChannelContextMissingAttendeeException();
        $expectErrorCode = ErrorCode::GUIDED_SHOPPING__SALES_CHANNEL_CONTEXT_MISSING_ATTENDEE;
        static::assertEquals($expectErrorCode, $exception->getErrorCode());
    }

    public function testGetStatusCode(): void
    {
        $exception = new SalesChannelContextMissingAttendeeException();
        $expectErrorCode = Response::HTTP_BAD_REQUEST;
        static::assertEquals($expectErrorCode, $exception->getStatusCode());
    }
}
