<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Presentation\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Presentation\Exception\AttendeeMissingPresentationException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AttendeeMissingPresentationExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $exception = new AttendeeMissingPresentationException();
        $expectMessage = 'Attendee missing the presentation.';
        static::assertEquals($expectMessage, $exception->getMessage());
    }

    public function testGetErrorCode(): void
    {
        $exception = new AttendeeMissingPresentationException();
        $expectErrorCode = ErrorCode::GUIDED_SHOPPING__ATTENDEE_MISSING_PRESENTATION;
        static::assertEquals($expectErrorCode, $exception->getErrorCode());
    }

    public function testGetStatusCode(): void
    {
        $exception = new AttendeeMissingPresentationException();
        $expectErrorCode = Response::HTTP_BAD_REQUEST;
        static::assertEquals($expectErrorCode, $exception->getStatusCode());
    }
}
