<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Struct;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Struct\AppointmentJoinStruct;

class AppointmentJoinStructTest extends TestCase
{
    public function testInitialize(): void
    {
        $struct = new AppointmentJoinStruct(
            'test-id',
            'test-context-token',
            'test-attendee-id',
            'test-sales-channel-id',
            'test-presentation-mode',
            false
        );
        $struct->setMercureSubscriberTopics(['test-mercure-subscriber-topics']);
        $struct->setMercurePublisherTopic('test-mercure-publisher-topic');
        $struct->setJWTMercureSubscriberToken('test-jwt-mercure-subcriber-token');
        $struct->setMercureHubPublicUrl('test-mercure-hub-url');
        $struct->setJWTMercurePublisherToken('test-jwt-mercure-publisher-token');
        $struct->setSalesChannelName('test-sales-channel-name');
        $struct->setAppointmentName('test-appointment-name');

        static::assertSame('test-id', $struct->getId());
        static::assertSame('test-context-token', $struct->getNewContextToken());
        static::assertSame('test-attendee-id', $struct->getAttendeeId());
        static::assertSame('test-sales-channel-id', $struct->getVars()['salesChannelId']);
        static::assertSame('test-presentation-mode', $struct->getVars()['presentationGuideMode']);
        static::assertFalse($struct->getVars()['isPreview']);
        static::assertSame(['test-mercure-subscriber-topics'], $struct->getMercureSubscriberTopics());
        static::assertSame('test-mercure-publisher-topic', $struct->getMercurePublisherTopic());
        static::assertSame('test-jwt-mercure-subcriber-token', $struct->getVars()['JWTMercureSubscriberToken']);
        static::assertSame('test-mercure-hub-url', $struct->getVars()['mercureHubPublicUrl']);
        static::assertSame('test-jwt-mercure-publisher-token', $struct->getVars()['JWTMercurePublisherToken']);
        static::assertSame('test-sales-channel-name', $struct->getVars()['salesChannelName']);
        static::assertSame('test-appointment-name', $struct->getVars()['appointmentName']);

        $struct->setAppointmentName(null);
        static::assertNull($struct->getVars()['appointmentName']);
    }
}
