<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Rule\RuleEntity;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;

class AppointmentEntityTest extends TestCase
{
    public function testAppointmentWithName(): void
    {
        $appointment = new AppointmentEntity();
        $name = 'test-name';
        $appointment->setName($name);
        static::assertEquals($name, $appointment->getName());
        $appointment->setName(null);
        static::assertNull($appointment->getName());
    }

    public function testActiveAppointment(): void
    {
        $appointment = new AppointmentEntity();
        $appointment->setActive(true);
        static::assertTrue($appointment->isActive());
        $appointment->setActive(false);
        static::assertFalse($appointment->isActive());
    }

    public function testAppointmentWithAccessibleFrom(): void
    {
        $appointment = new AppointmentEntity();
        $accessibleFrom = new \DateTime('now');
        $appointment->setAccessibleFrom($accessibleFrom);
        static::assertEquals($accessibleFrom, $appointment->getAccessibleFrom());
        $appointment->setAccessibleFrom(null);
        static::assertNull($appointment->getAccessibleFrom());
    }

    public function testAppointmentWithAccessibleTo(): void
    {
        $appointment = new AppointmentEntity();
        $accessibleTo = new \DateTime('now');
        $appointment->setAccessibleTo($accessibleTo);
        static::assertEquals($accessibleTo, $appointment->getAccessibleTo());
        $appointment->setAccessibleTo(null);
        static::assertNull($appointment->getAccessibleTo());
    }

    public function testAppointmentWithGuideUserId(): void
    {
        $appointment = new AppointmentEntity();
        $guideUserId = Uuid::randomHex();
        $appointment->setGuideUserId($guideUserId);
        static::assertEquals($guideUserId, $appointment->getGuideUserId());
        $appointment->setGuideUserId(null);
        static::assertNull($appointment->getGuideUserId());
    }

    public function testAppointmentWithGuideUser(): void
    {
        $appointment = new AppointmentEntity();
        $guideUser = new UserEntity();
        $guideUser->setEmail('test@gmail.com');
        $guideUser->setFirstName('test-first-name');
        $guideUser->setLastName('test-last-name');
        $appointment->setGuideUser($guideUser);
        static::assertSame($guideUser, $appointment->getGuideUser());
        static::assertSame([
            'email' => 'test@gmail.com',
            'name' => 'test-first-name test-last-name'
        ], $appointment->getGuideInfo());
        $appointment->setGuideUser(null);
        static::assertNull($appointment->getGuideUser());
        static::assertEquals('The guide', $appointment->getGuideName());
        static::assertNull($appointment->getGuideInfo());
    }

    public function testAppointmentWithSalesChannelDomainId(): void
    {
        $appointment = new AppointmentEntity();
        $salesChannelDomainId = Uuid::randomHex();
        $appointment->setSalesChannelDomainId($salesChannelDomainId);
        static::assertEquals($salesChannelDomainId, $appointment->getSalesChannelDomainId());
        $appointment->setSalesChannelDomainId(null);
        static::assertNull($appointment->getSalesChannelDomainId());
    }

    public function testAppointmentWithSalesChannel(): void
    {
        $appointment = new AppointmentEntity();
        $salesChannelDomain = new SalesChannelDomainEntity();
        $appointment->setSalesChannelDomain($salesChannelDomain);
        static::assertSame($salesChannelDomain, $appointment->getSalesChannelDomain());
        $appointment->setSalesChannelDomain(null);
        static::assertNull($appointment->getSalesChannelDomain());
    }

    public function testAppointmentWithPresentationPath(): void
    {
        $appointment = new AppointmentEntity();
        $presentationPath = 'test-path';
        $appointment->setPresentationPath($presentationPath);
        static::assertEquals($presentationPath, $appointment->getPresentationPath());
        $appointment->setPresentationPath(null);
        static::assertNull($appointment->getPresentationPath());
    }

    public function testAppointmentWithMode(): void
    {
        $appointment = new AppointmentEntity();
        $mode = 'test-mode';
        $appointment->setMode($mode);
        static::assertEquals($mode, $appointment->getMode());
    }

    public function testPreviewAppointment(): void
    {
        $appointment = new AppointmentEntity();
        $appointment->setPreview(true);
        static::assertTrue($appointment->isPreview());
        $appointment->setPreview(false);
        static::assertFalse($appointment->isPreview());
    }

    public function testGetVideoAudioSettings(): void
    {
        $appointment = new AppointmentEntity();
        static::assertEquals(AppointmentDefinition::NONE_VIDEO_AUDIO, $appointment->getVideoAudioSettings());
        $appointment->setVideoAudioSettings('test-video-audio-settings');
        static::assertEquals('test-video-audio-settings', $appointment->getVideoAudioSettings());
    }

    public function testAppointmentWithPresentationId(): void
    {
        $appointment = new AppointmentEntity();
        $presentationId = Uuid::randomHex();
        $appointment->setPresentationId($presentationId);
        static::assertEquals($presentationId, $appointment->getPresentationId());
    }

    public function testAppointmentWithPresentation(): void
    {
        $appointment = new AppointmentEntity();
        static::assertNull($appointment->getPresentation());
        $presentation = new PresentationEntity();
        $appointment->setPresentation($presentation);
        static::assertSame($presentation, $appointment->getPresentation());
    }

    public function testAppointmentWithAttendees(): void
    {
        $appointment = new AppointmentEntity();
        $attendees = new AttendeeCollection([]);
        $appointment->setAttendees($attendees);
        static::assertSame($attendees, $appointment->getAttendees());
    }

    public function testAppointmentWithVideoChat(): void
    {
        $appointment = new AppointmentEntity();
        $videoChat = new VideoChatEntity();
        $appointment->setVideoChat($videoChat);
        static::assertSame($videoChat, $appointment->getVideoChat());
        $appointment->setVideoChat(null);
        static::assertNull($appointment->getVideoChat());
    }

    public function testAppointmentWithAttendeeRuleIds(): void
    {
        $appointment = new AppointmentEntity();
        $ruleIds = ['id-1', 'id-2'];
        $appointment->setAttendeeRuleIds($ruleIds);
        static::assertEquals($ruleIds, $appointment->getAttendeeRuleIds());
        $appointment->setAttendeeRuleIds(null);
        static::assertNull($appointment->getAttendeeRuleIds());
    }

    public function testAppointmentWithAttendeeRule(): void
    {
        $appointment = new AppointmentEntity();
        $rule = new RuleEntity();
        $appointment->setAttendeeRule($rule);
        static::assertSame($rule, $appointment->getAttendeeRule());
        $appointment->setAttendeeRule(null);
        static::assertNull($appointment->getAttendeeRule());
    }

    public function testAppointmentWithAttendeeRestrictionType(): void
    {
        $appointment = new AppointmentEntity();
        $restrictionRule = 'rule-1';
        $appointment->setAttendeeRestrictionType($restrictionRule);
        static::assertEquals($restrictionRule, $appointment->getAttendeeRestrictionType());
        $appointment->setAttendeeRestrictionType(null);
        static::assertNull($appointment->getAttendeeRestrictionType());
    }

    public function testStartAppointment(): void
    {
        $appointment = new AppointmentEntity();
        static::assertNull($appointment->getStartedAt());
        $startedAt = new \DateTime('now');
        $appointment->setStartedAt($startedAt);
        static::assertEquals($startedAt, $appointment->getStartedAt());
        $appointment->setStartedAt(null);
        static::assertNull($appointment->getStartedAt());
    }

    public function testEndAppointment(): void
    {
        $appointment = new AppointmentEntity();
        static::assertNull($appointment->getEndedAt());
        $endedAt = new \DateTime('now');
        $appointment->setEndedAt($endedAt);
        static::assertEquals($endedAt, $appointment->getEndedAt());
        $appointment->setEndedAt(null);
        static::assertNull($appointment->getEndedAt());
    }

    public function testDefaultAppointment(): void
    {
        $appointment = new AppointmentEntity();
        $appointment->setDefault(true);
        static::assertTrue($appointment->isDefault());
        $appointment->setDefault(false);
        static::assertFalse($appointment->isDefault());
    }

    public function testAppointmentWithPresentationVersionId(): void
    {
        $appointment = new AppointmentEntity();
        $presentationVersionId = 'version-id';
        $appointment->setPresentationVersionId($presentationVersionId);
        static::assertEquals($presentationVersionId, $appointment->getPresentationVersionId());
        $appointment->setPresentationVersionId(null);
        static::assertNull($appointment->getPresentationVersionId());
    }

    public function testAppointmentWithCreatedById(): void
    {
        $appointment = new AppointmentEntity();
        $createdById = Uuid::randomHex();
        $appointment->setCreatedById($createdById);
        static::assertEquals($createdById, $appointment->getCreatedById());
        $appointment->setCreatedById(null);
        static::assertNull($appointment->getCreatedById());
    }

    public function testAppointmentWithCreatedBy(): void
    {
        $appointment = new AppointmentEntity();
        $createdBy = new UserEntity();
        $appointment->setCreatedBy($createdBy);
        static::assertSame($createdBy, $appointment->getCreatedBy());
        $appointment->setCreatedBy(null);
        static::assertNull($appointment->getCreatedBy());
    }

    public function testAppointmentWithUpdatedById(): void
    {
        $appointment = new AppointmentEntity();
        $updatedById = Uuid::randomHex();
        $appointment->setUpdatedById($updatedById);
        static::assertEquals($updatedById, $appointment->getUpdatedById());
        $appointment->setUpdatedById(null);
        static::assertNull($appointment->getUpdatedById());
    }

    public function testAppointmentWithUpdatedBy(): void
    {
        $appointment = new AppointmentEntity();
        $updatedBy = new UserEntity();
        $appointment->setUpdatedBy($updatedBy);
        static::assertSame($updatedBy, $appointment->getUpdatedBy());
        $appointment->setUpdatedBy(null);
        static::assertNull($appointment->getUpdatedBy());
    }

    public function testAppointmentWithMessage(): void
    {
        $appointment = new AppointmentEntity();
        static::assertNull($appointment->getMessage());
        $appointment->setMessage('test-message');
        static::assertEquals('test-message', $appointment->getMessage());
        $appointment->setMessage(null);
        static::assertNull($appointment->getMessage());
    }

    public function testAppointmentWithPresentationUrl(): void
    {
        $appointment = new AppointmentEntity();
        $appointment->setSalesChannelDomain(null);
        static::assertNull($appointment->getPresentationUrl());
        $appointment->setPresentationPath('test-path');
        $salesChannelDomain = new SalesChannelDomainEntity();
        $salesChannelDomain->setId('test-sales-channel-id');
        $salesChannelDomain->setUrl('http://test.com');
        $appointment->setSalesChannelDomain($salesChannelDomain);
        static::assertEquals('http://test.com/presentation/test-path', $appointment->getPresentationUrl());
    }
}
