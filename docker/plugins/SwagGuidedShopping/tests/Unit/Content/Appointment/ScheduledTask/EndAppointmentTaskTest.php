<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\ScheduledTask;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\ScheduledTask\EndAppointmentTask;

/**
 * @internal
 */
class EndAppointmentTaskTest extends TestCase
{
    public function testGetTaskName(): void
    {
        $task = new EndAppointmentTask();
        static::assertEquals('shopware_guided_shopping.end_appointment_task', $task::getTaskName());
    }

    public function testDefaultInterval(): void
    {
        $task = new EndAppointmentTask();
        static::assertEquals(3600, $task::getDefaultInterval());
    }
}
