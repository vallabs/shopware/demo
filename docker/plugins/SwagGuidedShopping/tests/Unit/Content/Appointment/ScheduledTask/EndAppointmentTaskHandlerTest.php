<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\ScheduledTask;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskCollection;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Exception\CouldNotEndAppointmentException;
use SwagGuidedShopping\Content\Appointment\ScheduledTask\EndAppointmentTaskHandler;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;

/**
 * @internal
 */
class EndAppointmentTaskHandlerTest extends TestCase
{
    public function testRun(): void
    {
        $configService = $this->createMock(SystemConfigService::class);
        $configService->expects(static::once())
            ->method('getInt')
            ->willReturn(1);

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = new AppointmentEntity();
        $appointment->setId('test-appointment-id');
        /** @var \DateTimeInterface|null $startedAt */
        $startedAt = \DateTime::createFromFormat('d/m/Y', '01/01/0001');
        $appointment->setStartedAt($startedAt);
        $appointment->setPresentationId('test-presentation-id');
        $appointment->setPresentationVersionId('test-presentation-version-id');
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentEntity::class,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                new Context(new SystemSource())
            ));

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::once())->method('getLastInteractionDateTime');

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())->method('isPresentationEnable')->willReturn(true);
        $appointmentService->expects(static::once())
            ->method('endPresentation')
            ->with(
                static::equalTo('test-appointment-id'),
                static::callback(function (Context $context) {
                    $extension = $context->getExtension(PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION);
                    if (!$extension instanceof ArrayStruct) {
                        return false;
                    }

                    return $extension->offsetExists(0) && $extension->offsetGet(0) === true;
                })
            );

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::never())->method('critical');

        $handler = $this->getHandler(
            $appointmentRepository,
            $appointmentService,
            $interactionService,
            $configService,
            $logger
        );

        $handler->run();
    }

    public function testRunWithAppointmentHasNoStartedDateAndNoInteraction(): void
    {
        $configService = $this->createMock(SystemConfigService::class);
        $configService->expects(static::once())
            ->method('getInt')
            ->willReturn(1);

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = new AppointmentEntity();
        $appointment->setId('test-appointment-id');
        $appointment->setStartedAt(null);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentEntity::class,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                new Context(new SystemSource())
            ));

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::once())->method('getLastInteractionDateTime');

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::never())->method('isPresentationEnable');
        $appointmentService->expects(static::never())->method('endPresentation');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('critical')
            ->with(
                static::equalTo('could not end appointment automatically'),
                static::equalTo([
                    'appointmentId' => 'test-appointment-id',
                    'error' => new CouldNotEndAppointmentException('could not end appointment as there is no interaction and appointment has no started date')
                ])
            );

        $handler = $this->getHandler(
            $appointmentRepository,
            $appointmentService,
            $interactionService,
            $configService,
            $logger
        );

        $handler->run();
    }

    /**
     * @dataProvider getTestRunButCouldNotEndPresentationProviderData
     */
    public function testRunButCouldNotEndPresentation(
        AppointmentEntity $appointment,
        MockObject $appointmentService
    ): void
    {
        $configService = $this->createMock(SystemConfigService::class);
        $configService->expects(static::once())->method('getInt');

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentEntity::class,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                new Context(new SystemSource())
            ));

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::once())->method('getLastInteractionDateTime');

        $appointmentService->expects(static::never())->method('endPresentation');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('critical')
            ->with(
                static::equalTo('could not end appointment automatically'),
                static::equalTo([
                    'appointmentId' => 'test-appointment-id',
                    'error' => new CouldNotEndAppointmentException(
                        'could not end appointment as there is a presentation not enabled yet and the time interval from the last interaction to now not greater than the max hours to close appointment you configure'
                    )
                ])
            );

        $handler = $this->getHandler(
            $appointmentRepository,
            $appointmentService,
            $interactionService,
            $configService,
            $logger
        );

        $handler->run();
    }

    public static function getTestRunButCouldNotEndPresentationProviderData(): \Generator
    {
        $self = new EndAppointmentTaskHandlerTest('test');

        $appointmentHasStartedAtIsNow = new AppointmentEntity();
        $appointmentHasStartedAtIsNow->setId('test-appointment-id');
        $appointmentHasStartedAtIsNow->setStartedAt(new \DateTime());

        $appointmentServiceDoNotRunMethod = $self->createMock(AppointmentService::class);
        $appointmentServiceDoNotRunMethod->expects(static::never())->method('isPresentationEnable');

        yield [
            $appointmentHasStartedAtIsNow,
            $appointmentServiceDoNotRunMethod
        ];

        $appointmentHasStartedAtIsForALongTimesAgo = new AppointmentEntity();
        $appointmentHasStartedAtIsForALongTimesAgo->setId('test-appointment-id');
        /** @var \DateTimeInterface|null $startedAt */
        $startedAt = \DateTime::createFromFormat('d/m/Y', '01/01/0001');
        $appointmentHasStartedAtIsForALongTimesAgo->setStartedAt($startedAt);

        $appointmentServiceRunMethodAndReturnFalseValue = $self->createMock(AppointmentService::class);
        $appointmentServiceRunMethodAndReturnFalseValue->expects(static::once())->method('isPresentationEnable')->willReturn(false);

        yield [
            $appointmentHasStartedAtIsForALongTimesAgo,
            $appointmentServiceRunMethodAndReturnFalseValue
        ];
    }

    private function getHandler(
        ?MockObject $appointmentRepositoryMock = null,
        ?MockObject $appointmentServiceMock = null,
        ?MockObject $interactionServiceMock = null,
        ?MockObject $configServiceMock = null,
        ?MockObject $loggerMock = null
    ): EndAppointmentTaskHandler
    {
        /** @var MockObject&LoggerInterface $logger */
        $logger = $loggerMock ?: $this->createMock(LoggerInterface::class);
        /** @var MockObject&EntityRepository<ScheduledTaskCollection> $scheduledTaskRepository */
        $scheduledTaskRepository = $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentServiceMock ?: $this->createMock(AppointmentService::class);
        /** @var MockObject&InteractionService $interactionService */
        $interactionService = $interactionServiceMock ?: $this->createMock(InteractionService::class);
        /** @var MockObject&SystemConfigService $configService */
        $configService = $configServiceMock ?: $this->createMock(SystemConfigService::class);

        return new EndAppointmentTaskHandler(
            $logger,
            $scheduledTaskRepository,
            $appointmentRepository,
            $appointmentService,
            $interactionService,
            $configService
        );
    }
}
