<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\UpdateAttendee;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Appointment\SalesChannel\UpdateAttendee\UpdateAttendeeResponse;

class UpdateAttendeeResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $data = [
            'test-key' => 'test-value'
        ];

        $response = new UpdateAttendeeResponse($data);

        static::assertSame('guided-shopping.appointment.update-attendee', $response->getResult()->getApiAlias());
        static::assertSame($data, $response->getData());
    }
}
