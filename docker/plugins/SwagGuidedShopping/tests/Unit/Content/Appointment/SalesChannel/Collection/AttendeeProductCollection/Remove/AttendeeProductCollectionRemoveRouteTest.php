<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Remove;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Remove\AttendeeProductCollectionRemoveRoute;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeProductCollectionService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class AttendeeProductCollectionRemoveRouteTest extends TestCase
{
    public function testGetDecorated(): void
    {
        $attendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);
        $attendeeProductCollectionService->expects(static::never())->method('remove');
        $route = new AttendeeProductCollectionRemoveRoute($attendeeProductCollectionService);
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testRemoveEntry(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $salesChannelContext->addExtension(
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME,
            $attendee
        );

        $attendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);
        $attendeeProductCollectionService->expects(static::once())
            ->method('remove')
            ->with(
                static::equalTo('test-alias'),
                static::equalTo($attendee->getId()),
                static::equalTo('test-product-id'),
                static::equalTo($salesChannelContext->getContext())
            );

        $route = new AttendeeProductCollectionRemoveRoute($attendeeProductCollectionService);
        $route->removeEntry('test-alias', 'test-product-id', $salesChannelContext, new Criteria());
    }
}
