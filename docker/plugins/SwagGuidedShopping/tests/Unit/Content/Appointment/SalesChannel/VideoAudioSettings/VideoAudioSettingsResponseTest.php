<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\VideoAudioSettings;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\SalesChannel\VideoAudioSettings\VideoAudioSettingsResponse;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class VideoAudioSettingsResponseTest extends TestCase
{
    public function testInitializeWithAppointment(): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setVideoAudioSettings('test-video-audio-settings');
        $response = new VideoAudioSettingsResponse($appointment);
        static::assertEquals('appointment_video_audio_settings', $response->getResult()->getApiAlias());
        static::assertEquals('test-video-audio-settings', $response->getVideoAudioSettings());
    }

    public function testInitializeWithoutAppointment(): void
    {
        $response = new VideoAudioSettingsResponse(null);
        static::assertEquals('appointment_video_audio_settings', $response->getResult()->getApiAlias());
        static::assertNull($response->getVideoAudioSettings());
    }
}
