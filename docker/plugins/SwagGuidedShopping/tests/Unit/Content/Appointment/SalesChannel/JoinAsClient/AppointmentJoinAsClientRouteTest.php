<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\JoinAsClient;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Service\Validator\ViolationList;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAuthModeNotSetException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentForbiddenException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetAccessibleException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentJoinInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\SalesChannel\JoinAsClient\AppointmentJoinAsClientRoute;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Appointment\Service\ClientJoinAppointmentValidator;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class AppointmentJoinAsClientRouteTest extends TestCase
{
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testJoinButFailedValidatingRequestBody(): void
    {
        $request = new Request(
            ['attendeeName' => 123123]
        );
        $constraint = new Collection(
            [
                'attendeeName' => [new Type('string')],
            ]
        );
        $constraint->allowMissingFields = true;
        $constraintViolationList = new ConstraintViolationList();
        $constraintViolation = new ConstraintViolation(
            'This value should be of type string.',
            'This value should be of type {{ type }}.',
            [
                '{{ value }}' => '123123',
                '{{ type }}' => 'string'
            ],
            [
                'attendeeName' => '123123'
            ],
            '[attendeeName]',
            123123,
            null,
            null,
            new Type('string'),
            null
        );
        $constraintViolationList->add($constraintViolation);
        $expectedViolationList = new ViolationList($constraintViolationList);

        $dataValidator = $this->createMock(DataValidator::class);
        $dataValidator->expects(static::once())
            ->method('validate')
            ->with(
                static::equalTo($request->request->all()),
                static::equalTo($constraint)
            )
            ->willReturn($expectedViolationList);

        $route = $this->getRoute(null, null, null, $dataValidator);
        static::expectException(AppointmentJoinInvalidRequestParameter::class);
        static::expectExceptionMessage("There are the following parameter errors in the request body \nThis value should be of type string. [attendeeName]\n");
        $route->join('test-presentation-path', new RequestDataBag([]), $request, $this->salesChannelContext);
    }

    public function testJoinButCanNotFindAppointment(): void
    {
        $presentationPath = 'test-presentation-path';
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('findAppointmentByUrl')
            ->with(
                static::equalTo($presentationPath),
                static::equalTo($this->salesChannelContext->getContext())
            )
            ->willReturn(null);

        $validator = $this->createMock(ClientJoinAppointmentValidator::class);
        $validator->expects(static::never())->method('validate');

        $joinAppointmentService = $this->createMock(JoinAppointmentService::class);
        $joinAppointmentService->expects(static::never())->method('joinMeetingAsClient');

        $route = $this->getRoute($appointmentService);
        static::expectException(AppointmentNotFoundException::class);
        $route->join($presentationPath, new RequestDataBag([]), new Request(), $this->salesChannelContext);
    }

    /**
     * @dataProvider getTestJoinButFailedAtValidatingAppointmentProviderData
     *
     * @param class-string<\Throwable> $exception
     */
    public function testJoinButFailedAtValidatingAppointment(
        string $exception
    ): void
    {
        $presentationPath = 'test-presentation-path';
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentService->expects(static::once())
            ->method('findAppointmentByUrl')
            ->with(
                static::equalTo($presentationPath),
                static::equalTo($this->salesChannelContext->getContext())
            )->willReturn($appointment);

        $validator = $this->createMock(ClientJoinAppointmentValidator::class);
        $validator->expects(static::once())
            ->method('validate')
            ->with(
                static::equalTo($appointment),
                static::equalTo($this->salesChannelContext)
            )
            ->willThrowException(new $exception());

        $joinAppointmentService = $this->createMock(JoinAppointmentService::class);
        $joinAppointmentService->expects(static::never())->method('joinMeetingAsClient');

        $route = $this->getRoute($appointmentService, $validator, $joinAppointmentService);
        static::expectException($exception);
        $route->join($presentationPath, new RequestDataBag([]), new Request(), $this->salesChannelContext);
    }

    public static function getTestJoinButFailedAtValidatingAppointmentProviderData(): \Generator
    {
        yield [AppointmentNotFoundException::class];
        yield [AppointmentForbiddenException::class];
        yield [AppointmentAuthModeNotSetException::class];
        yield [AppointmentNotYetAccessibleException::class];
    }

    /**
     * @dataProvider getTestJoinWithValidAppointmentProviderData
     */
    public function testJoinWithValidAppointment(
        Request $request
    ): void
    {
        $presentationPath = 'test-presentation-path';
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentService->expects(static::once())
            ->method('findAppointmentByUrl')
            ->with(
                static::equalTo($presentationPath),
                static::equalTo($this->salesChannelContext->getContext())
            )->willReturn($appointment);

        $validator = $this->createMock(ClientJoinAppointmentValidator::class);
        $validator->expects(static::once())
            ->method('validate')
            ->with(
                static::equalTo($appointment),
                static::equalTo($this->salesChannelContext)
            );

        $joinAppointmentService = $this->createMock(JoinAppointmentService::class);
        $joinAppointmentService->expects(static::once())
            ->method('joinMeetingAsClient')
            ->with(
                static::equalTo($appointment->getId()),
                static::equalTo($this->salesChannelContext),
                static::equalTo($appointment->getMode()),
                static::equalTo($request->get('attendeeName'))
            );

        $route = $this->getRoute($appointmentService, $validator, $joinAppointmentService);
        $route->join($presentationPath, new RequestDataBag([]), $request, $this->salesChannelContext);
    }

    public static function getTestJoinWithValidAppointmentProviderData(): \Generator
    {
        yield [new Request()];
        yield [new Request(['attendeeName' => 'test-attendee-name'])];
    }

    private function getRoute(
        ?MockObject $appointmentServiceMock = null,
        ?MockObject $validatorMock = null,
        ?MockObject $joinAppointmentServiceMock = null,
        ?MockObject $dataValidatorMock = null
    ): AppointmentJoinAsClientRoute
    {
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentServiceMock ?: $this->createMock(AppointmentService::class);
        /** @var MockObject&ClientJoinAppointmentValidator $validator */
        $validator = $validatorMock ?: $this->createMock(ClientJoinAppointmentValidator::class);
        /** @var MockObject&JoinAppointmentService $joinAppointmentService */
        $joinAppointmentService = $joinAppointmentServiceMock ?: $this->createMock(JoinAppointmentService::class);
        /** @var MockObject&DataValidator $dataValidator */
        $dataValidator = $dataValidatorMock ?: $this->createMock(DataValidator::class);

        return new AppointmentJoinAsClientRoute($appointmentService, $validator, $joinAppointmentService, $dataValidator);
    }
}
