<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\DownloadCalendarFile;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeInvitationInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\SalesChannel\DownloadCalendarFile\DownloadCalendarFileRoute;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;

class DownloadCalendarFileRouteTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    /**
     * @dataProvider getTestDownloadIcsWithInvalidRequestProviderData
     * @param array<string, mixed> $requestData
     */
    public function testDownloadIcsWithInvalidRequest(array $requestData): void
    {
        $dataValidator = new DataValidator(Validation::createValidator());

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::never())->method('getAppointment');

        $route = $this->getRoute($dataValidator, $appointmentService);
        static::expectException(AttendeeInvitationInvalidRequestParameter::class);
        $route->downloadIcs('test-appointment-id', new Request([], $requestData), $this->salesChannelContext);
    }

    public static function getTestDownloadIcsWithInvalidRequestProviderData(): \Generator
    {
        yield [[]];
        yield [['invalid-key' => 'invalid-value']];
        yield [['token' => 1]];
        yield [['token' => '']];
    }

    public function testDownloadIcsWithInvalidToken(): void
    {
        $token = $this->createTestToken();
        $request = new Request([], ['token' => "{$token}1"]);

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::never())->method('getAppointment');

        $route = $this->getRoute(null, $appointmentService);
        $response = $route->downloadIcs('test-appointment-id', $request, $this->salesChannelContext);
        static::assertEquals('Invalid token.', $response->getContent());
        static::assertEquals(401, $response->getStatusCode());
    }

    /**
     * @dataProvider getTestDownloadIcsWithInvalidAppointmentProviderData
     */
    public function testDownloadIcsWithInvalidAppointment(
        AppointmentEntity $appointment,
        string $errorMessage,
        int $statusCode
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $token = $this->createTestToken();
        $request = new Request([], ['token' => $token]);

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($this->salesChannelContext->getContext()),
                static::equalTo(['guideUser', 'attendees.customer', 'salesChannelDomain.salesChannel'])
            )
            ->willReturn($appointment);

        $route = $this->getRoute(null, $appointmentService);
        $response = $route->downloadIcs($appointmentId, $request, $this->salesChannelContext);
        static::assertEquals($errorMessage, $response->getContent());
        static::assertEquals($statusCode, $response->getStatusCode());
    }

    public static function getTestDownloadIcsWithInvalidAppointmentProviderData(): \Generator
    {
        $noSalesChannelDomainAppointment = new AppointmentEntity();
        $noSalesChannelDomainAppointment->setId('test-appointment-id');
        $noSalesChannelDomainAppointment->setSalesChannelDomain(null);
        yield 'no-sales-channel-domain-appointment' => [
            $noSalesChannelDomainAppointment,
            'Appointment sales channel domain is not set yet.',
            400
        ];

        $selfAppointment = new AppointmentEntity();
        $selfAppointment->setId('test-appointment-id');
        $salesChannelDomain = new SalesChannelDomainEntity();
        $salesChannelDomain->setId('test-sales-channel-domain-id');
        $salesChannelDomain->setUrl('http://test.local');
        $selfAppointment->setSalesChannelDomain($salesChannelDomain);
        $selfAppointment->setMode(AppointmentDefinition::MODE_SELF);
        yield 'self-appointment' => [
            $selfAppointment,
            'Appointment is self mode.',
            400
        ];
    }

    public function testDownloadIcsWithValidAppointment(): void
    {
        $appointmentId = 'test-appointment-id';
        $token = $this->createTestToken();
        $request = new Request([], ['token' => $token]);

        $appointment = new AppointmentEntity();
        $appointment->setId('test-appointment-id');
        $appointment->setName('test-appointment-name');
        $user = new UserEntity();
        $user->setId('test-user-id');
        $user->setEmail('test@gmail.com');
        $user->setFirstName('test-first-name');
        $user->setLastName('test-last-name');
        $appointment->setGuideUser($user);
        $salesChannelDomain = new SalesChannelDomainEntity();
        $salesChannelDomain->setId('test-sales-channel-domain-id');
        $salesChannelDomain->setUrl('http://test.local');
        $appointment->setSalesChannelDomain($salesChannelDomain);
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointment->setAccessibleFrom(new \DateTime());
        $appointment->setAccessibleTo(new \DateTime());
        $appointment->setPresentationPath('test-presentation-path');
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($this->salesChannelContext->getContext()),
                static::equalTo(['guideUser', 'attendees.customer', 'salesChannelDomain.salesChannel'])
            )
            ->willReturn($appointment);

        $route = $this->getRoute(null, $appointmentService);
        $response = $route->downloadIcs($appointmentId, $request, $this->salesChannelContext);
        static::assertNotEmpty($response->getContent());
        static::assertEquals('text/calendar', $response->headers->get('content-type'));
        static::assertEquals('attachment; filename=test-appointment-name.ics', $response->headers->get('content-disposition'));
    }

    private function getRoute(
        ?DataValidator $dataValidator = null,
        ?MockObject $appointmentServiceMock = null
    ): DownloadCalendarFileRoute
    {
        $dataValidator = $dataValidator ?: $this->createMock(DataValidator::class);
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentServiceMock ?: $this->createMock(AppointmentService::class);

        return new DownloadCalendarFileRoute($dataValidator, $appointmentService);
    }

    private function createTestToken(): string
    {
        return EncryptHelper::encryptString('test@gmail.com');
    }
}
