<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\JoinAsClient;

use PHPUnit\Framework\TestCase;
use Shopware\Core\PlatformRequest;
use SwagGuidedShopping\Content\Appointment\SalesChannel\JoinAsClient\AppointmentJoinAsClientResponse;
use SwagGuidedShopping\Content\Appointment\Struct\AppointmentJoinStruct;

class AppointmentJoinAsClientResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $struct = new AppointmentJoinStruct(
            'test-appointment-id',
            'test-context-token',
            'test-attendee-id',
            'test-sales-channel-id',
            'test-presentation-mode'
        );
        $response = new AppointmentJoinAsClientResponse($struct);
        static::assertEquals('test-context-token', $response->headers->get(PlatformRequest::HEADER_CONTEXT_TOKEN));
        static::assertEquals($struct, $response->getObject());
    }
}
