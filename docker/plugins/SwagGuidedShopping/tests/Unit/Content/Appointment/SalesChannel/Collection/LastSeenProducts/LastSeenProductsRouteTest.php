<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\Collection\LastSeenProducts;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionStruct;
use SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\LastSeenProducts\LastSeenProductsRoute;
use SwagGuidedShopping\Content\Appointment\Service\LastSeenProductsService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\HttpFoundation\Request;

class LastSeenProductsRouteTest extends TestCase
{
    public function testGetDecorated(): void
    {
        $lastSeenProductsService = $this->createMock(LastSeenProductsService::class);
        $lastSeenProductsService->expects(static::never())->method('getProducts');
        $route = new LastSeenProductsRoute($lastSeenProductsService);
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testGetList(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $salesChannelContext->addExtension(
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME,
            $attendee
        );

        $lastSeenProductsService = $this->createMock(LastSeenProductsService::class);
        $lastSeenProductsService->expects(static::once())
            ->method('getProducts')
            ->with(
                static::equalTo($attendee),
                static::equalTo($salesChannelContext)
            )
            ->willReturn(['test-product-id']);

        $route = new LastSeenProductsRoute($lastSeenProductsService);
        $response = $route->getList(new Request(), $salesChannelContext);
        $lastSeenProducts = $response->getObject();
        static::assertInstanceOf(AttendeeProductCollectionStruct::class, $lastSeenProducts);
        static::assertEquals(['lastSeen' => ['test-product-id']], $lastSeenProducts->getCollection());
    }
}
