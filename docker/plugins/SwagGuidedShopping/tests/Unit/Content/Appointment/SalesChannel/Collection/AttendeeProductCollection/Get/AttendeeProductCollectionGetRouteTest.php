<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Get;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Get\AttendeeProductCollectionGetRoute;
use SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Get\AbstractAttendeeProductCollectionGetRoute;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeProductCollectionAliasNotFoundException;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeProductCollectionService;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\SalesChannel\Exception\RouteAliasNotFoundException;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Get\AttendeeProductCollectionGetRoute
 *
 * @internal
 */
class AttendeeProductCollectionGetRouteTest extends TestCase
{
    private AttendeeProductCollectionService $attendeeProductCollectionService;

    private AbstractAttendeeProductCollectionGetRoute $attendeeProductCollectionGetRoute;

    protected function setUp(): void
    {
        parent::setUp();

        $this->attendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);

        $this->attendeeProductCollectionGetRoute = new AttendeeProductCollectionGetRoute($this->attendeeProductCollectionService);
    }

    public function testItThrowsDecorationPatternException(): void
    {
        static::expectException(DecorationPatternException::class);

        $this->attendeeProductCollectionGetRoute->getDecorated();
    }

    /**
     * @dataProvider getProductsDataProvider
     */
    public function testGetProducts(string $alias): void
    {
        $salesChannelContext = $this->getSalesChannelContext();
        $actualResult = $this->attendeeProductCollectionGetRoute->getProducts($alias, $salesChannelContext);
        static::assertEquals('200', $actualResult->getStatusCode());
    }

    /**
     * @dataProvider getProductsDataProvider
     */
    public function testGetProductsWithError(string $alias): void
    {
        $exception = new AttendeeProductCollectionAliasNotFoundException();

        $mockAttendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);
        $mockAttendeeProductCollectionService->expects(static::any())->method('getProducts')->willThrowException($exception);
        $mockAttendeeProductCollectionGetRoute = new AttendeeProductCollectionGetRoute($mockAttendeeProductCollectionService);

        $salesChannelContext = $this->getSalesChannelContext();

        static::expectException(RouteAliasNotFoundException::class);
        $mockAttendeeProductCollectionGetRoute->getProducts($alias, $salesChannelContext);
    }

    private function getSalesChannelContext(): SalesChannelContext
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');

        $salesChannelContextHelper = new SalesChannelContextHelper();
        $salesChannelContext = $salesChannelContextHelper->createSalesChannelContext();
        $salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        return $salesChannelContext;
    }

    public static function getProductsDataProvider(): \Generator
    {
        yield ['alias' => 'like'];
        yield ['alias' => 'dislike'];
        yield ['alias' => 'test data'];
    }
}
