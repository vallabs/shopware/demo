<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\MailInvitation\Helper;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;

class EncryptHelperTest extends TestCase
{
    public function testDecryptStringWithValidToken(): void
    {
        $email = 'test.123@gmail.com';
        $encryptedToken = EncryptHelper::encryptString($email);
        $decryptResult = EncryptHelper::decryptString($encryptedToken);
        static::assertArrayHasKey('valid', $decryptResult);
        static::assertArrayHasKey('email', $decryptResult);
        static::assertTrue($decryptResult['valid']);
        static::assertEquals($email, $decryptResult['email']);
    }

    /**
     * @dataProvider getTestDecryptStringWithInvalidToken
     */
    public function testDecryptStringWithInvalidToken(
        string $encryptedToken,
        string $error,
        string $message
    ): void
    {
        $decryptResult = EncryptHelper::decryptString($encryptedToken);
        static::assertArrayHasKey('valid', $decryptResult);
        static::assertArrayHasKey('error', $decryptResult);
        static::assertArrayHasKey('message', $decryptResult);
        static::assertFalse($decryptResult['valid']);
        static::assertEquals($error, $decryptResult['error']);
        static::assertEquals($message, $decryptResult['message']);
    }

    public static function getTestDecryptStringWithInvalidToken(): \Generator
    {
        yield 'invalid-token' => [
            EncryptHelper::encryptString('test.123@gmail.com') . 'invalid-token',
            'INVALID_TOKEN',
            'Invalid token.'
        ];

        yield 'expired-token' => [
            EncryptHelper::encryptString('test.123@gmail.com', -1),
            'EXPIRED_TOKEN',
            'Token already expired.'
        ];
    }
}
