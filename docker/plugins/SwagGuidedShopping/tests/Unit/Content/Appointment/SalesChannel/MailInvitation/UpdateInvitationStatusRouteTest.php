<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\MailInvitation;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeInvitationInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\UpdateInvitationStatusRoute;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;

class UpdateInvitationStatusRouteTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    /**
     * @dataProvider getTestUpdateInvitationStatusWithInvalidRequest
     * @param array<string, mixed> $requestData
     */
    public function testUpdateInvitationStatusWithInvalidRequest(array $requestData): void
    {
        $dataValidator = new DataValidator(Validation::createValidator());

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::never())->method('search');

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $route = $this->getRoute($attendeeService, $dataValidator, $appointmentRepository);
        static::expectException(AttendeeInvitationInvalidRequestParameter::class);
        $route->updateInvitationStatus('test-appointment-id', new Request([], $requestData), $this->salesChannelContext);
    }

    public static function getTestUpdateInvitationStatusWithInvalidRequest(): \Generator
    {
        $validToken = EncryptHelper::encryptString('test.123@gmail.com');

        yield [[]];
        yield [['token' => $validToken]];
        yield [['answer' => 'accepted']];
        yield [['token' => '', 'answer' => 'accepted']];
        yield [['token' => 1, 'answer' => 'accepted']];
        yield [['token' => $validToken, 'answer' => '']];
        yield [['token' => $validToken, 'answer' => 1]];
        yield [['token' => $validToken, 'answer' => 'pending']];
    }

    public function testUpdateInvitationStatusWithInvalidToken(): void
    {
        $request = new Request([], [
            'token' => EncryptHelper::encryptString('test.123@gmail.com') . 'invalid-token',
            'answer' => 'accepted'
        ]);
        $dataValidator = new DataValidator(Validation::createValidator());

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::never())->method('search');

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $route = $this->getRoute($attendeeService, $dataValidator, $appointmentRepository);
        $response = $route->updateInvitationStatus('test-appointment-id', $request, $this->salesChannelContext);
        static::assertEquals(401, $response->getStatusCode());
    }

    public function testUpdateInvitationStatusButCouldNotFindAppointment(): void
    {
        $request = new Request([], [
            'token' => EncryptHelper::encryptString('test.123@gmail.com'),
            'answer' => 'accepted'
        ]);
        $dataValidator = new DataValidator(Validation::createValidator());

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo(new Criteria(['test-appointment-id'])),
                static::equalTo($this->salesChannelContext->getContext())
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    0,
                    new AppointmentCollection(),
                    null,
                    new Criteria(['test-appointment-id']),
                    $this->salesChannelContext->getContext()
                )
            );

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $route = $this->getRoute($attendeeService, $dataValidator, $appointmentRepository);
        $response = $route->updateInvitationStatus('test-appointment-id', $request, $this->salesChannelContext);
        static::assertEquals(400, $response->getStatusCode());
        /** @var string $responseContent */
        $responseContent = $response->getContent();
        $decodedResponseContent = \json_decode($responseContent, true);
        static::assertEquals('APPOINTMENT_NOT_FOUND', $decodedResponseContent['error']);
    }

    public function testUpdateInvitationStatusButFailedAtUpdatingInvitationStatus(): void
    {
        $request = new Request([], [
            'token' => EncryptHelper::encryptString('test.123@gmail.com'),
            'answer' => 'accepted'
        ]);
        $dataValidator = new DataValidator(Validation::createValidator());

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo(new Criteria(['test-appointment-id'])),
                static::equalTo($this->salesChannelContext->getContext())
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$appointment]),
                    null,
                    new Criteria(['test-appointment-id']),
                    $this->salesChannelContext->getContext()
                )
            );

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::once())
            ->method('updateInvitationStatus')
            ->with(
                static::equalTo(['test.123@gmail.com']),
                static::equalTo('test-appointment-id'),
                static::equalTo('accepted'),
                static::equalTo($this->salesChannelContext->getContext())
            )->willReturn(false);

        $route = $this->getRoute($attendeeService, $dataValidator, $appointmentRepository);
        $response = $route->updateInvitationStatus('test-appointment-id', $request, $this->salesChannelContext);
        static::assertEquals(400, $response->getStatusCode());
        /** @var string $responseContent */
        $responseContent = $response->getContent();
        $decodedResponseContent = \json_decode($responseContent, true);
        static::assertEquals('ATTENDEE_NOT_FOUND', $decodedResponseContent['error']);
    }

    /**
     * @dataProvider getTestUpdateInvitationStatusSuccessfullyProviderData
     */
    public function testUpdateInvitationStatusSuccessfully(
        ?\DateTime $startedAt,
        ?\DateTime $endedAt,
        ?string $invitationStatus
    ): void
    {
        $request = new Request([], [
            'token' => EncryptHelper::encryptString('test.123@gmail.com'),
            'answer' => 'accepted'
        ]);
        $dataValidator = new DataValidator(Validation::createValidator());

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setId('test-appointment-id');
        $appointment->setStartedAt($startedAt);
        $appointment->setEndedAt($endedAt);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo(new Criteria(['test-appointment-id'])),
                static::equalTo($this->salesChannelContext->getContext())
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$appointment]),
                    null,
                    new Criteria(['test-appointment-id']),
                    $this->salesChannelContext->getContext()
                )
            );

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::once())
            ->method('updateInvitationStatus')
            ->with(
                static::equalTo(['test.123@gmail.com']),
                static::equalTo('test-appointment-id'),
                static::equalTo('accepted'),
                static::equalTo($this->salesChannelContext->getContext())
            )->willReturn(true);

        $route = $this->getRoute($attendeeService, $dataValidator, $appointmentRepository);
        $response = $route->updateInvitationStatus('test-appointment-id', $request, $this->salesChannelContext);
        static::assertEquals(200, $response->getStatusCode());
        /** @var string $responseContent */
        $responseContent = $response->getContent();
        $decodedResponseContent = \json_decode($responseContent, true);
        static::assertEquals([
            'appointment' => [
                'id' => $appointment->getId(),
                'accessibleFrom' => $appointment->getAccessibleFrom(),
                'accessibleTo' => $appointment->getAccessibleTo(),
                'status' => $invitationStatus
            ],
            "answer" => 'accepted'
        ], $decodedResponseContent);
    }

    public static function getTestUpdateInvitationStatusSuccessfullyProviderData(): \Generator
    {
        yield 'not-started-yet' => [null, null, null];
        yield 'started' => [new \DateTime(), null, 'started'];
        yield 'ended' => [new \DateTime(), new \DateTime(), 'ended'];
    }

    private function getRoute(
        ?MockObject $attendeeServiceMock = null,
        ?DataValidator $dataValidator = null,
        ?MockObject $appointmentRepositoryMock = null
    ): UpdateInvitationStatusRoute
    {
        /** @var MockObject&AttendeeService $attendeeService */
        $attendeeService = $attendeeServiceMock ?: $this->createMock(AttendeeService::class);
        $dataValidator = $dataValidator ?: $this->createMock(DataValidator::class);
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createMock(EntityRepository::class);

        return new UpdateInvitationStatusRoute($attendeeService, $dataValidator, $appointmentRepository);
    }
}
