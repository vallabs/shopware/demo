<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\UpdateAttendee;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentJoinInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\SalesChannel\UpdateAttendee\UpdateAttendeeRoute;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Service\Validator\ViolationList;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class UpdateAttendeeRouteTest extends TestCase
{
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetDecorated(): void
    {
        $route = $this->getRoute();
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testUpdateAttendeeWithInvalidRequest(): void
    {
        $dataValidator = $this->createMock(DataValidator::class);
        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $violationList = new ViolationList($constraintViolationList);
        $dataValidator->expects(static::once())
            ->method('validate')
            ->willReturn($violationList);

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateAttendee');

        $route = $this->getRoute($attendeeService, $dataValidator);
        static::expectException(AppointmentJoinInvalidRequestParameter::class);
        $route->updateAttendee(new Request(), $this->salesChannelContext);
    }

    public function testUpdateAttendeeWithEmptyRequest(): void
    {
        $dataValidator = $this->createMock(DataValidator::class);
        $dataValidator->expects(static::once())
            ->method('validate');

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateAttendee');

        $route = $this->getRoute($attendeeService, $dataValidator);
        $response = $route->updateAttendee(new Request(), $this->salesChannelContext);
        static::assertEmpty($response->getData());
    }

    /**
     * @dataProvider getTestUpdateAttendeeWithInvalidRequestProviderData
     *
     * @param array<string, mixed> $requestPayload
     */
    public function testUpdateAttendeeWithValidRequest(array $requestPayload): void
    {
        $request = new Request([], $requestPayload);
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $salesChannelContext->setExtensions([
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME => $attendee
        ]);

        $dataValidator = $this->createMock(DataValidator::class);
        $dataValidator->expects(static::once())
            ->method('validate');

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::once())
            ->method('updateAttendee')
            ->with(
                static::equalTo($attendee->getId()),
                static::equalTo($requestPayload),
                $salesChannelContext->getContext()
            );

        $route = $this->getRoute($attendeeService, $dataValidator);
        $response = $route->updateAttendee($request, $salesChannelContext);
        static::assertSame($requestPayload, $response->getData());
    }

    public static function getTestUpdateAttendeeWithInvalidRequestProviderData(): \Generator
    {
        yield [
            [
                'attendeeName' => 'test-attendee-name',
                'videoUserId' => 'test-user-id',
                'guideCartPermissionsGranted' => true
            ]
        ];
        yield[
            [
                'attendeeName' => 'test-attendee-name',
                'videoUserId' => 'test-user-id'
            ]
        ];
        yield[
            [
                'attendeeName' => 'test-attendee-name',
                'guideCartPermissionsGranted' => true
            ]
        ];
        yield[
            [
                'videoUserId' => 'test-user-id',
                'guideCartPermissionsGranted' => true
            ]
        ];
        yield [['attendeeName' => 'test-attendee-name']];
        yield [['videoUserId' => 'test-user-id']];
        yield [['guideCartPermissionsGranted' => true]];
    }

    public function testUpdateAttendeeWithValidRequestContainsAttendeeSubmitted(): void
    {
        $request = new Request([], [
            'attendeeSubmitted' => true
        ]);
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $salesChannelContext->setExtensions([
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME => $attendee
        ]);

        $dataValidator = $this->createMock(DataValidator::class);
        $dataValidator->expects(static::once())
            ->method('validate');

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::once())
            ->method('updateAttendee')
            ->with(
                static::equalTo($attendee->getId()),
                /** @var array<string, mixed> $updateData */
                static::callback(function (array $updateData) {
                    $diffDate = \date_diff($updateData['attendeeSubmittedAt'], new \DateTime());

                    return $diffDate->format('%i') <= 5;
                }),
                $salesChannelContext->getContext()
            );

        $route = $this->getRoute($attendeeService, $dataValidator);
        $response = $route->updateAttendee($request, $salesChannelContext);
        static::assertArrayHasKey('attendeeSubmittedAt', $response->getData());
    }

    private function getRoute(
        ?MockObject $attendeeServiceMock = null,
        ?MockObject $dataValidatorMock = null
    ): UpdateAttendeeRoute
    {
        /** @var MockObject&AttendeeService $attendeeService */
        $attendeeService = $attendeeServiceMock ?: $this->createMock(AttendeeService::class);
        /** @var MockObject&DataValidator $dataValidator */
        $dataValidator = $dataValidatorMock ?: $this->createMock(DataValidator::class);

        return new UpdateAttendeeRoute($attendeeService, $dataValidator);
    }
}
