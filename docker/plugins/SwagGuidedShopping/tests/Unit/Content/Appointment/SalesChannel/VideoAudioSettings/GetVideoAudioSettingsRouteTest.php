<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\VideoAudioSettings;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use SwagGuidedShopping\Content\Appointment\SalesChannel\VideoAudioSettings\GetVideoAudioSettingsRoute;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class GetVideoAudioSettingsRouteTest extends TestCase
{
    public function testGetDecoratedThrowsException(): void
    {
        $route = new GetVideoAudioSettingsRoute(
            $this->createMock(AppointmentService::class)
        );

        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testGetVideoAudioSettings(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setVideoAudioSettings('test-video-audio-settings');

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('findAppointmentByUrl')
            ->willReturn($appointment);

        $route = new GetVideoAudioSettingsRoute($appointmentService);

        $response = $route->getVideoAudioSettings('test-appointment-id', $salesChannelContext);
        static::assertEquals('test-video-audio-settings', $response->getVideoAudioSettings());
    }

    public function testGetVideoAudioSettingsButAppointmentNotFound(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setVideoAudioSettings('test-video-audio-settings');

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('findAppointmentByUrl')
            ->willReturn(null);

        $route = new GetVideoAudioSettingsRoute($appointmentService);

        $response = $route->getVideoAudioSettings('test-appointment-id', $salesChannelContext);
        static::assertNull($response->getVideoAudioSettings());
    }
}
