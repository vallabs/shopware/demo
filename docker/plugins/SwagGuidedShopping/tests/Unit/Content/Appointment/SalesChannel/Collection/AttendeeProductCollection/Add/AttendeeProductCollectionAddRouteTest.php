<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Add;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeProductCollectionAliasNotFoundException;
use SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Add\AttendeeProductCollectionAddRoute;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeProductCollectionService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\SalesChannel\Exception\RouteAliasNotFoundException;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class AttendeeProductCollectionAddRouteTest extends TestCase
{
    public function testGetDecorated(): void
    {
        $attendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);
        $attendeeProductCollectionService->expects(static::never())->method('addProduct');
        $route = new AttendeeProductCollectionAddRoute($attendeeProductCollectionService);
        static::expectException(DecorationPatternException::class);
        $route->getDecorated();
    }

    public function testAddWithInvalidAlias(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $salesChannelContext->addExtension(
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME,
            $attendee
        );

        $attendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);
        $attendeeProductCollectionService->expects(static::once())
            ->method('addProduct')
            ->with(
                static::equalTo('test-invalid-alias'),
                static::equalTo($attendee->getId()),
                static::equalTo('test-product-id'),
                static::equalTo($salesChannelContext->getContext())
            )
            ->willThrowException(new AttendeeProductCollectionAliasNotFoundException());

        $route = new AttendeeProductCollectionAddRoute($attendeeProductCollectionService);
        static::expectException(RouteAliasNotFoundException::class);
        $route->add('test-invalid-alias', 'test-product-id', $salesChannelContext, new Criteria());
    }

    /**
     * @dataProvider getTestAddWithValidAliasProviderData
     */
    public function testAddWithValidAlias(string $alias): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $salesChannelContext->addExtension(
            GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME,
            $attendee
        );

        $attendeeProductCollectionService = $this->createMock(AttendeeProductCollectionService::class);
        $attendeeProductCollectionService->expects(static::once())
            ->method('addProduct')
            ->with(
                static::equalTo($alias),
                static::equalTo($attendee->getId()),
                static::equalTo('test-product-id'),
                static::equalTo($salesChannelContext->getContext())
            );

        $route = new AttendeeProductCollectionAddRoute($attendeeProductCollectionService);
        $route->add($alias, 'test-product-id', $salesChannelContext, new Criteria());
    }

    public static function getTestAddWithValidAliasProviderData(): \Generator
    {
        yield [AttendeeProductCollectionService::ALIAS_LIKED];
        yield [AttendeeProductCollectionService::ALIAS_DISLIKED];
    }
}
