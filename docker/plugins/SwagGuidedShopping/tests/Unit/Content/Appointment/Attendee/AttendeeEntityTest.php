<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Attendee;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionCollection;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionEntity;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;
use SwagGuidedShopping\Content\Interaction\InteractionEntity;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity
 *
 * @internal
 */
class AttendeeEntityTest extends TestCase
{
    public function testConstructor(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->__constructor();
        static::assertEquals(0, $attendee->getInteractions()->count());
        static::assertEquals(0, $attendee->getProductCollections()->count());
    }

    public function testGetAppointmentId(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setAppointmentId('test-appointment-id');
        static::assertEquals('test-appointment-id', $attendee->getAppointmentId());
    }

    public function testGetCustomerId(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getCustomerId());
        $attendee->setCustomerId('test-customer-id');
        static::assertEquals('test-customer-id', $attendee->getCustomerId());
        $attendee->setCustomerId(null);
        static::assertNull($attendee->getCustomerId());
    }

    public function testGetUserId(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getUserId());
        $attendee->setUserId('test-user-id');
        static::assertEquals('test-user-id', $attendee->getUserId());
        $attendee->setUserId(null);
        static::assertNull($attendee->getUserId());
    }

    public function testGetUser(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getUser());
        $user = new UserEntity();
        $user->setId('test-user-id');
        $user->setFirstName('test-first-name');
        $user->setLastName('test-last-name');
        $attendee->setUser($user);
        static::assertSame($user, $attendee->getUser());
        static::assertEquals('test-first-name test-last-name', $attendee->getGuideName());
        $attendee->setUser(null);
        static::assertNull($attendee->getUser());
    }

    public function testGetGuideNameWithoutGuide(): void
    {
        $attendee = new AttendeeEntity();
        static::expectExceptionMessage('No guide set');
        $attendee->getGuideName();
    }

    public function testGetAppointment(): void
    {
        $attendee = new AttendeeEntity();
        $appointment = new AppointmentEntity();
        $appointment->setId('test-appointment-id');
        $attendee->setAppointment($appointment);
        static::assertSame($appointment, $attendee->getAppointment());
    }

    public function testGetCustomer(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getCustomer());
        $customer = new CustomerEntity();
        $customer->setId('test-customer-id');
        $attendee->setCustomer($customer);
        static::assertSame($customer, $attendee->getCustomer());
        $attendee->setCustomer(null);
        static::assertNull($attendee->getCustomer());
    }

    public function testGetInteractions(): void
    {
        $attendee = new AttendeeEntity();
        static::assertEquals(0, $attendee->getInteractions()->count());
        $interaction = new InteractionEntity();
        $interaction->setId('test-interaction-id');
        $interactionCollection = new InteractionCollection([$interaction]);
        $attendee->setInteractions($interactionCollection);
        static::assertSame($interactionCollection, $attendee->getInteractions());
    }

    public function testGetProductCollection(): void
    {
        $attendee = new AttendeeEntity();
        static::assertEquals(0, $attendee->getProductCollections()->count());
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $attendeeProductCollection->setId('test-attendee-product-collection-id');
        $attendeeProductCollectionCollection = new AttendeeProductCollectionCollection([$attendeeProductCollection]);
        $attendee->setProductCollections($attendeeProductCollectionCollection);
        static::assertSame($attendeeProductCollectionCollection, $attendee->getProductCollections());
    }

    public function testAttendeeType(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setType(AttendeeDefinition::TYPE_CLIENT);
        static::assertEquals(AttendeeDefinition::TYPE_CLIENT, $attendee->getType());
        static::assertTrue($attendee->isClient());
        static::assertFalse($attendee->isGuide());
        $attendee->setType(AttendeeDefinition::TYPE_GUIDE);
        static::assertEquals(AttendeeDefinition::TYPE_GUIDE, $attendee->getType());
        static::assertFalse($attendee->isClient());
        static::assertTrue($attendee->isGuide());
    }

    public function testGetJoinedAt(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getJoinedAt());
        $joinedAt = new \DateTime();
        $attendee->setJoinedAt($joinedAt);
        static::assertSame($joinedAt, $attendee->getJoinedAt());
        $attendee->setJoinedAt(null);
        static::assertNull($attendee->getJoinedAt());
    }

    public function testGetLastActive(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getLastActive());
        $lastActiveTime = new \DateTime();
        $attendee->setLastActive($lastActiveTime);
        static::assertSame($lastActiveTime, $attendee->getLastActive());
        $attendee->setLastActive(null);
        static::assertNull($attendee->getLastActive());
    }

    public function testGetAttendeeName(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getAttendeeName());
        $attendee->setAttendeeName('test-attendee-name');
        static::assertEquals('test-attendee-name', $attendee->getAttendeeName());
        $attendee->setAttendeeName(null);
        static::assertNull($attendee->getAttendeeName());
    }

    public function testGetAttendeeEmail(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getAttendeeEmail());
        $attendee->setAttendeeEmail('test-attendee-email@gmail.com');
        static::assertEquals('test-attendee-email@gmail.com', $attendee->getAttendeeEmail());
        $attendee->setAttendeeEmail(null);
        static::assertNull($attendee->getAttendeeEmail());
    }

    public function testGetIsGuideCartPermissionsGranted(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setGuideCartPermissionsGranted(true);
        static::assertTrue($attendee->isGuideCartPermissionsGranted());
        $attendee->setGuideCartPermissionsGranted(false);
        static::assertFalse($attendee->isGuideCartPermissionsGranted());
    }

    public function testGetAttendeeSubmittedAt(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getAttendeeSubmittedAt());
        $submittedTime = new \DateTime();
        $attendee->setAttendeeSubmittedAt($submittedTime);
        static::assertSame($submittedTime, $attendee->getAttendeeSubmittedAt());
        $attendee->setAttendeeSubmittedAt(null);
        static::assertNull($attendee->getAttendeeSubmittedAt());
    }

    public function testGetVideoUserId(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getVideoUserId());
        $attendee->setVideoUserId('test-video-user-id');
        static::assertEquals('test-video-user-id', $attendee->getVideoUserId());
        $attendee->setVideoUserId(null);
        static::assertNull($attendee->getVideoUserId());
    }

    public function testGetInvitationStatus(): void
    {
        $attendee = new AttendeeEntity();
        static::assertNull($attendee->getInvitationStatus());
        $attendee->setInvitationStatus('test-invitation-status');
        static::assertEquals('test-invitation-status', $attendee->getInvitationStatus());
        $attendee->setInvitationStatus(null);
        static::assertNull($attendee->getInvitationStatus());
    }
}
