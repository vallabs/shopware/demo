<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Attendee;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\System\Country\CountryEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection
 *
 * @internal
 */
class AttendeeCollectionTest extends TestCase
{
    /**
     * @param array<string, mixed> $parameters
     */
    public function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testGetExpectedClass(): void
    {
        $attendeeCollection = new AttendeeCollection();
        $expectedClass = $this->invokeMethod($attendeeCollection, 'getExpectedClass', []);
        static::assertEquals(
            'SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity',
            $expectedClass
        );
    }

    /**
     * @dataProvider getTestGetAppointmentRecipientsProviderData
     */
    public function testGetAppointmentRecipientsWithValidAttendee(
        AttendeeEntity $attendee,
        string $attendeeEmail,
        string $attendeeName,
        ?string $invitationStatus,
        ?string $timeZone
    ): void
    {
        $attendeeCollection = new AttendeeCollection([$attendee]);
        $recipients = $attendeeCollection->getAppointmentRecipients();
        static::assertCount(1, $recipients);
        static::assertArrayHasKey($attendeeEmail, $recipients);
        $recipient = $recipients[$attendeeEmail];
        static::assertArrayHasKey('name', $recipient);
        static::assertArrayHasKey('invitationStatus', $recipient);
        static::assertEquals($attendeeName, $recipient['name']);
        static::assertEquals($invitationStatus, $recipient['invitationStatus']);
        static::assertArrayHasKey('timeZone', $recipient);
        static::assertEquals($timeZone, $recipient['timeZone']);
    }

    public static function getTestGetAppointmentRecipientsProviderData(): \Generator
    {
        $attendeeIsInternalCustomer = new AttendeeEntity();
        $attendeeIsInternalCustomer->setId('test-attendee-id');
        $customer = new CustomerEntity();
        $customer->setId('test-customer-id');
        $customer->setEmail('test-customer-email@gmail.com');
        $customer->setFirstName('test-first-name');
        $customer->setLastName('test-last-name');
        $customerAddress = new CustomerAddressEntity();
        $country = new CountryEntity();
        $country->assign([
            'id' => 'test-country-id',
            'iso' => 'DE'
        ]);
        $customerAddress->assign([
            'id' => 'test-customer-address-id',
            'countryId' => 'test-country-id',
            'country' => $country
        ]);
        $customer->setDefaultShippingAddress($customerAddress);
        $attendeeIsInternalCustomer->setCustomerId('test-customer-id');
        $attendeeIsInternalCustomer->setCustomer($customer);
        $attendeeIsInternalCustomer->setInvitationStatus(null);
        yield 'attendee-is-internal-customer' => [
            $attendeeIsInternalCustomer,
            'test-customer-email@gmail.com',
            'test-first-name test-last-name',
            null
        ];

        $attendeeIsExternalCustomerHasName = new AttendeeEntity();
        $attendeeIsExternalCustomerHasName->setId('test-attendee-id');
        $attendeeIsExternalCustomerHasName->setAttendeeEmail('test-attendee-email-has-name@gmail.com');
        $attendeeIsExternalCustomerHasName->setAttendeeName('test-attendee-name');
        $attendeeIsExternalCustomerHasName->setInvitationStatus('pending');
        yield 'attendee-is-external-customer-has-name' => [
            $attendeeIsExternalCustomerHasName,
            'test-attendee-email-has-name@gmail.com',
            'test-attendee-name',
            'pending'
        ];

        $attendeeIsExternalCustomerDoNotHasName = new AttendeeEntity();
        $attendeeIsExternalCustomerDoNotHasName->setId('test-attendee-id');
        $attendeeIsExternalCustomerDoNotHasName->setAttendeeEmail('test-attendee-email-do-not-has-name@gmail.com');
        $attendeeIsExternalCustomerDoNotHasName->setInvitationStatus('accepted');

        return [
            'attendee-is-internal-customer' => [
                $attendeeIsInternalCustomer,
                'test-customer-email@gmail.com',
                'test-first-name test-last-name',
                null,
                'Europe/Berlin'
            ],
            'attendee-is-external-customer-has-name' => [
                $attendeeIsExternalCustomerHasName,
                'test-attendee-email-has-name@gmail.com',
                'test-attendee-name',
                'pending',
                null
            ],
            'attendee-is-external-customer-do-not-has-name' => [
                $attendeeIsExternalCustomerDoNotHasName,
                'test-attendee-email-do-not-has-name@gmail.com',
                'test-attendee-email-do-not-has-name',
                'accepted',
                null
            ]
        ];
    }

    public function testGetAppointmentRecipientsWithInvalidAttendee(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setInvitationStatus(null);
        $attendeeCollection = new AttendeeCollection([$attendee]);

        $recipients = $attendeeCollection->getAppointmentRecipients();
        static::assertCount(0, $recipients);
    }
}
