<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Traits;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Traits\IcsTrait;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class IcsTraitTest extends TestCase
{
    use IcsTrait;

    /**
     * @dataProvider getTestGenerateIcsFileProviderData
     */
    public function testGenerateIcsFile(
        string $invitationAction,
        string $expectStatus
    ): void
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $dateFrom = new \DateTime('2000-01-01 12:54:11');
        $dateTo = new \DateTime('2000-01-01 13:00:11');
        $appointment->setAccessibleFrom($dateFrom);
        $appointment->setAccessibleTo($dateTo);
        $user = new UserEntity();
        $user->setId('test-user-id');
        $user->setEmail('user@test.com');
        $user->setFirstName('Test');
        $user->setLastName('User');
        $appointment->setGuideUser($user);
        $appointment->setName('Test  appointment  name');
        $appointment->setMessage('test-message');
        $appointment->setTranslated(['name' => 'Test appointment name', 'message' => 'test-message']);

        $result = $this->generateIcsFile($appointment, ['attendee@gmail.com' => ['name' => 'Attendee']], $invitationAction);

        static::assertEquals('text/calendar', $result['mimeType']);
        static::assertEquals('invite.ics', $result['fileName']);
        $content = \str_replace("\r\n", '', $result['content']);
        static::assertStringContainsString('CALSCALE:GREGORIAN', $content);
        static::assertStringContainsString('METHOD:REQUEST', $content);
        static::assertStringContainsString("UID:{$appointment->getId()}", $content);
        static::assertStringContainsString("SUMMARY:Test appointment name", $content);
        static::assertStringContainsString("DESCRIPTION:test-message", $content);
        static::assertStringContainsString("LOCATION:http://test.com/presentation/{$appointment->getPresentationPath()}", $content);
        static::assertStringContainsString("STATUS:{$expectStatus}", $content);
        /** @var string $removedWhiteSpacesContent */
        $removedWhiteSpacesContent = \preg_replace('!\s+!', '', ($content));
        static::assertStringContainsString("ATTENDEE;CN=TestUser:MAILTO:user@test.com", $removedWhiteSpacesContent);
        static::assertStringContainsString("ATTENDEE;CN=Attendee:MAILTO:attendee@gmail.com", $removedWhiteSpacesContent);
        static::assertStringContainsString("DTSTART:20000101T125411Z", $content);
        static::assertStringContainsString("DTEND:20000101T130011Z", $content);
    }

    public static function getTestGenerateIcsFileProviderData(): \Generator
    {
        yield 'invite' => [AppointmentDefinition::INVITE_ACTION, 'CONFIRMED'];
        yield 'invite-all' => [AppointmentDefinition::INVITE_ALL_ACTION, 'CONFIRMED'];
        yield 'cancel' => [AppointmentDefinition::CANCEL_ACTION, 'CANCELLED'];
    }
}
