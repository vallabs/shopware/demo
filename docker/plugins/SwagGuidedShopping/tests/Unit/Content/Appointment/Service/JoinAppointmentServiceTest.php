<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Content\Appointment\Service\ContextDataHelper;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use SwagGuidedShopping\Content\Appointment\Struct\AppointmentJoinStruct;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class JoinAppointmentServiceTest extends TestCase
{
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testJoinMeetingAsGuideButMercureHubPublicUrlIsEmptyString(): void
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(
                static::equalTo(ConfigKeys::MERCURE_HUB_PUBLIC_URL)
            )
            ->willReturn('');

        $service = $this->getService(
            null,
            null,
            null,
            null,
            null,
            $systemConfigService,
        );
        static::expectException(\Exception::class);
        $service->joinMeetingAsGuide('test-appointment-id', $this->salesChannelContext, AppointmentDefinition::MODE_GUIDED);
    }

    public function testJoinMeetingAsGuideSuccessfully(): void
    {
        $appointmentMode = AppointmentDefinition::MODE_GUIDED;
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentId = $appointment->getId();

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(
                static::equalTo(ConfigKeys::MERCURE_HUB_PUBLIC_URL)
            )
            ->willReturn('test-mercure-hub-public-url');

        $attendee = $this->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $appointmentId, $appointment);
        $contextDataReader = $this->createContextDataReaderMock(
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $attendee,
            'credentials',
            $this->salesChannelContext,
            1
        );

        $topicGenerator = $this->createMock(TopicGenerator::class);
        $topicGenerator->expects(static::once())
            ->method('generateGuideSubscriberTopics')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($appointmentMode)
            )

            ->willReturn(['test-subscriber-topics']);
        $topicGenerator->expects(static::once())
            ->method('generateGuidePublisherTopic')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($appointmentMode)
            )

            ->willReturn('test-publisher-topics');

        $jwtMercureSubscriberToken = $this->createMock(JWTMercureTokenFactory::class);
        $jwtMercureSubscriberToken->expects(static::once())
            ->method('createSubscriberToken')
            ->with(
                static::equalTo(['test-subscriber-topics'])
            )
            ->willReturn('test-subscriber-token');
        $jwtMercureSubscriberToken->expects(static::once())
            ->method('createPublisherToken')
            ->with(
                static::equalTo(['test-publisher-topics'])
            )
            ->willReturn('test-publisher-token');

        $service = $this->getService(
            $topicGenerator,
            $jwtMercureSubscriberToken,
            null,
            null,
            null,
            $systemConfigService,
            $contextDataReader
        );
        $struct = $service->joinMeetingAsGuide($appointmentId, $this->salesChannelContext, $appointmentMode);

        /** @var SalesChannelDomainEntity $salesChannelDomain */
        $salesChannelDomain = $appointment->getSalesChannelDomain();

        $expectStruct = $this->getExpectStructWhenJoinMeeting(
            $appointment,
            $attendee->getId(),
            $salesChannelDomain->getSalesChannelId(),
            $this->salesChannelContext,
            $struct->getNewContextToken()
        );

        static::assertEquals($expectStruct, $struct);
    }

    public function testJoinMeetingAsClientButMercureHubPublicUrlIsEmptyString(): void
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(
                static::equalTo(ConfigKeys::MERCURE_HUB_PUBLIC_URL)
            )
            ->willReturn('');

        $service = $this->getService(
            null,
            null,
            null,
            null,
            null,
            $systemConfigService,
        );
        static::expectException(\Exception::class);
        $service->joinMeetingAsClient('test-appointment-id', $this->salesChannelContext, AppointmentDefinition::MODE_GUIDED);
    }

    /**
     * @dataProvider getTestJoinMeetingAsClientSuccessfullyProviderData
     */
    public function testJoinMeetingAsClientSuccessfully(
        string $appointmentMode,
        AppointmentEntity $appointment,
        AttendeeEntity $attendee,
        AppointmentJoinStruct $expectStruct,
        MockObject $jwtMercureTokenFactory,
        SalesChannelContext $salesChannelContext
    ): void
    {
        $appointmentId = $appointment->getId();

        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(
                static::equalTo(ConfigKeys::MERCURE_HUB_PUBLIC_URL)
            )
            ->willReturn('test-mercure-hub-public-url');

        $attendeeType = AttendeeDefinition::TYPE_CLIENT;
        $contextDataReader = $this->createContextDataReaderMock(
            $attendeeType,
            $appointmentId,
            $attendee,
            'credentials',
            $salesChannelContext,
            1
        );

        $topicGenerator = $this->createMock(TopicGenerator::class);
        $topicGenerator->expects(static::once())
            ->method('generateClientSubscriberTopics')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($appointmentMode)
            )
            ->willReturn(['test-subscriber-topics']);
        $topicGenerator->expects(static::once())
            ->method('generateClientPublisherTopic')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($appointmentMode)
            )
            ->willReturn('test-publisher-topics');

        $service = $this->getService(
            $topicGenerator,
            $jwtMercureTokenFactory,
            null,
            null,
            null,
            $systemConfigService,
            $contextDataReader
        );
        $struct = $service->joinMeetingAsClient($appointmentId, $salesChannelContext, $appointmentMode);
        static::assertEquals($expectStruct, $struct);
    }

    public static function getTestJoinMeetingAsClientSuccessfullyProviderData(): \Generator
    {
        $self = new JoinAppointmentServiceTest('test');
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $guidedAppointment = (new AppointMentMockHelper())->getAppointEntity();
        /** @var SalesChannelDomainEntity $guideSalesChannelDomain */
        $guideSalesChannelDomain = $guidedAppointment->getSalesChannelDomain();
        $attendeeWithGuidedMode = $self->createTestAttendee(AttendeeDefinition::TYPE_CLIENT, $guidedAppointment->getId(), $guidedAppointment);
        yield 'guided-appointment' => [
            AppointmentDefinition::MODE_GUIDED,
            $guidedAppointment,
            $attendeeWithGuidedMode,
            $self->getExpectStructWhenJoinMeeting(
                $guidedAppointment,
                $attendeeWithGuidedMode->getId(),
                $guideSalesChannelDomain->getSalesChannelId(),
                $salesChannelContext
            ),
            $self->createJWTMercureTokenFactoryMock(true, true),
            $salesChannelContext
        ];

        $selfAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $selfAppointment->setMode(AppointmentDefinition::MODE_SELF);
        /** @var SalesChannelDomainEntity $selfSalesChannelDomain */
        $selfSalesChannelDomain = $selfAppointment->getSalesChannelDomain();
        $attendeeWithSelfMode = $self->createTestAttendee(AttendeeDefinition::TYPE_CLIENT, $guidedAppointment->getId(), $selfAppointment);
        yield 'self-appointment' => [
            AppointmentDefinition::MODE_SELF,
            $selfAppointment,
            $attendeeWithSelfMode,
            $self->getExpectStructWhenJoinMeeting(
                $selfAppointment,
                $attendeeWithSelfMode->getId(),
                $selfSalesChannelDomain->getSalesChannelId(),
                $salesChannelContext
            ),
            $self->createJWTMercureTokenFactoryMock(),
            $salesChannelContext
        ];
    }

    /**
     * @dataProvider getTestJoinMeetingWithValidAttendeeButAppointmentDoNotHaveSalesChannelDomainProviderData
     */
    public function testJoinMeetingWithValidAttendeeButAppointmentDoNotHaveSalesChannelDomain(
        bool $asGuide,
        string $attendeeType,
        string $appointmentId,
        MockObject $contextDataReader,
        MockObject $stateServiceFactory,
        MockObject $channelContextPersiter,
        MockObject $attendeeService,
        SalesChannelContext $salesChannelContext
    ): void
    {
        $service = $this->getService(
            null,
            null,
            $stateServiceFactory,
            $channelContextPersiter,
            $attendeeService,
            null,
            $contextDataReader
        );
        static::expectException(NoSalesChannelDomainMappedException::class);
        $service->joinMeeting($appointmentId, $salesChannelContext, $asGuide);
    }

    public static function getTestJoinMeetingWithValidAttendeeButAppointmentDoNotHaveSalesChannelDomainProviderData(): \Generator
    {
        $self = new JoinAppointmentServiceTest('test');
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentId = $appointment->getId();
        $appointment->setSalesChannelDomain(null);

        $guideAttendee = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $appointmentId, $appointment);
        yield 'join-as-a-guide-and-get-attendee-from-credentials' => [
            true,
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $self->createContextDataReaderMock(AttendeeDefinition::TYPE_GUIDE, $appointmentId, $guideAttendee, 'credentials', $salesChannelContext),
            $self->createStateServiceFactoryMock(false),
            $self->createChannelContextPersisterMock(true, false, false, $salesChannelContext),
            $self->createAttendeeServiceMock(false, false),
            $salesChannelContext
        ];
        yield 'join-as-a-guide-and-get-attendee-from-context' => [
            true,
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $self->createContextDataReaderMock(AttendeeDefinition::TYPE_GUIDE, $appointmentId, $guideAttendee, 'context', $salesChannelContext),
            $self->createStateServiceFactoryMock(false),
            $self->createChannelContextPersisterMock(true, false, false, $salesChannelContext),
            $self->createAttendeeServiceMock(false, false),
            $salesChannelContext
        ];
        yield 'join-as-a-guide-and-get-attendee-from-service' => [
            true,
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $self->createContextDataReaderMock(
                AttendeeDefinition::TYPE_GUIDE,
                $appointmentId,
                $guideAttendee,
                'service',
                $salesChannelContext,
                1
            ),
            $self->createStateServiceFactoryMock(false),
            $self->createChannelContextPersisterMock(true, false, false, $salesChannelContext),
            $self->createAttendeeServiceMock(
                true,
                false,
                $appointmentId,
                AttendeeDefinition::TYPE_GUIDE,
                null,
                'test-user-id',
                'test-get-attendee-name',
                null,
                null,
                $salesChannelContext
            ),
            $salesChannelContext
        ];

        $clientAttendee = $self->createTestAttendee(AttendeeDefinition::TYPE_CLIENT, $appointmentId, $appointment);
        yield 'join-as-a-client-and-get-attendee-from-credentials' => [
            false,
            AttendeeDefinition::TYPE_CLIENT,
            $appointmentId,
            $self->createContextDataReaderMock(AttendeeDefinition::TYPE_CLIENT, $appointmentId, $clientAttendee, 'credentials', $salesChannelContext),
            $self->createStateServiceFactoryMock(false),
            $self->createChannelContextPersisterMock(),
            $self->createAttendeeServiceMock(false, false),
            $salesChannelContext
        ];
        yield 'join-as-a-client-and-get-attendee-from-context' => [
            false,
            AttendeeDefinition::TYPE_CLIENT,
            $appointmentId,
            $self->createContextDataReaderMock(AttendeeDefinition::TYPE_CLIENT, $appointmentId, $clientAttendee, 'context', $salesChannelContext),
            $self->createStateServiceFactoryMock(false),
            $self->createChannelContextPersisterMock(),
            $self->createAttendeeServiceMock(false, false),
            $salesChannelContext
        ];
        yield 'join-as-a-client-and-get-attendee-from-service' => [
            false,
            AttendeeDefinition::TYPE_CLIENT,
            $appointmentId,
            $self->createContextDataReaderMock(
                AttendeeDefinition::TYPE_CLIENT,
                $appointmentId,
                $clientAttendee,
                'service',
                $salesChannelContext,
                1
            ),
            $self->createStateServiceFactoryMock(false),
            $self->createChannelContextPersisterMock(),
            $self->createAttendeeServiceMock(
                true,
                false,
                $appointmentId,
                AttendeeDefinition::TYPE_CLIENT,
                'test-customer-id',
                'test-user-id',
                'test-get-attendee-name',
                null,
                null,
                $salesChannelContext
            ),
            $salesChannelContext
        ];

        $invalidGuideAttendeeDiffAppointmentId = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, 'test-invalid-appointment-id', $appointment);
        yield 'join-as-a-guide-and-get-invalid-attendee-from-credentials' => [
            true,
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $self->createContextDataReaderMock(
                AttendeeDefinition::TYPE_GUIDE,
                $appointmentId,
                $invalidGuideAttendeeDiffAppointmentId,
                'credentials-but-invalid-attendee',
                $salesChannelContext,
                1
            ),
            $self->createStateServiceFactoryMock(true, $invalidGuideAttendeeDiffAppointmentId, $salesChannelContext),
            $self->createChannelContextPersisterMock(true, true, false, $salesChannelContext,),
            $self->createAttendeeServiceMock(
                true,
                true,
                $appointmentId,
                AttendeeDefinition::TYPE_GUIDE,
                null,
                'test-user-id',
                'test-get-attendee-name',
                $invalidGuideAttendeeDiffAppointmentId,
                /** @param array<string, mixed> $attendeeData */
                function (array $attendeeData) use ($invalidGuideAttendeeDiffAppointmentId) {
                    $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));

                    return $attendeeData['id'] === $invalidGuideAttendeeDiffAppointmentId->getId()
                        && $diffActiveTime->format('%i') <= 5;
                },
                $salesChannelContext
            ),
            $salesChannelContext
        ];

        $invalidGuideAttendeeWrongType = $self->createTestAttendee(AttendeeDefinition::TYPE_CLIENT, $appointmentId, $appointment);
        $invalidClientAttendeeDiffAppointmentId = $self->createTestAttendee(AttendeeDefinition::TYPE_CLIENT, 'test-invalid-appointment-id', $appointment);
        yield 'join-as-a-client-and-get-invalid-attendee-from-credentials' => [
            false,
            AttendeeDefinition::TYPE_CLIENT,
            $appointmentId,
            $self->createContextDataReaderMock(
                AttendeeDefinition::TYPE_CLIENT,
                $appointmentId,
                $invalidClientAttendeeDiffAppointmentId,
                'credentials-but-invalid-attendee',
                $salesChannelContext,
                1
            ),
            $self->createStateServiceFactoryMock(true, $invalidGuideAttendeeDiffAppointmentId, $salesChannelContext),
            $self->createChannelContextPersisterMock(
                false,
                true,
                false,
                $salesChannelContext,
                null,
                'test-customer-id'
            ),
            $self->createAttendeeServiceMock(
                true,
                true,
                $appointmentId,
                AttendeeDefinition::TYPE_CLIENT,
                'test-customer-id',
                'test-user-id',
                'test-get-attendee-name',
                $invalidGuideAttendeeDiffAppointmentId,
                /** @param array<string, mixed> $attendeeData */
                function (array $attendeeData) use ($invalidGuideAttendeeDiffAppointmentId) {
                    $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));
                    $mins = $diffActiveTime->format('%i');

                    return $attendeeData['id'] === $invalidGuideAttendeeDiffAppointmentId->getId() && $mins <= 5;
                },
                $salesChannelContext
            ),
            $salesChannelContext
        ];
        yield 'join-as-a-guide-and-get-invalid-attendee-from-context' => [
            true,
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $self->createContextDataReaderMock(
                AttendeeDefinition::TYPE_GUIDE,
                $appointmentId,
                $invalidGuideAttendeeWrongType,
                'context-but-invalid-attendee',
                $salesChannelContext,
                1
            ),
            $self->createStateServiceFactoryMock(true, $invalidGuideAttendeeWrongType, $salesChannelContext),
            $self->createChannelContextPersisterMock(true, true, false, $salesChannelContext),
            $self->createAttendeeServiceMock(
                true,
                true,
                $appointmentId,
                AttendeeDefinition::TYPE_GUIDE,
                null,
                'test-user-id',
                'test-get-attendee-name',
                $invalidGuideAttendeeWrongType,
                /** @param array<string, mixed> $attendeeData */
                function (array $attendeeData) use ($invalidGuideAttendeeWrongType) {
                    $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));
                    $mins = $diffActiveTime->format('%i');

                    return $attendeeData['id'] === $invalidGuideAttendeeWrongType->getId()
                        && $attendeeData['lastActive'] instanceof \DateTime
                        && $mins <= 5;
                },
                $salesChannelContext
            ),
            $salesChannelContext
        ];

        $invalidClientAttendeeWrongType = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $appointmentId, $appointment);
        yield 'join-as-a-client-and-get-invalid-attendee-from-context' => [
            false,
            AttendeeDefinition::TYPE_CLIENT,
            $appointmentId,
            $self->createContextDataReaderMock(
                AttendeeDefinition::TYPE_CLIENT,
                $appointmentId,
                $invalidClientAttendeeWrongType,
                'context-but-invalid-attendee',
                $salesChannelContext,
                1
            ),
            $self->createStateServiceFactoryMock(true, $invalidClientAttendeeWrongType, $salesChannelContext),
            $self->createChannelContextPersisterMock(
                false,
                true,
                false,
                $salesChannelContext,
                null,
                'test-customer-id'
            ),
            $self->createAttendeeServiceMock(
                true,
                true,
                $appointmentId,
                AttendeeDefinition::TYPE_CLIENT,
                'test-customer-id',
                'test-user-id',
                'test-get-attendee-name',
                $invalidClientAttendeeWrongType,
                /** @param array<string, mixed> $attendeeData */
                function (array $attendeeData) use ($invalidClientAttendeeWrongType) {
                    $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));
                    $mins = $diffActiveTime->format('%i');

                    return $attendeeData['id'] === $invalidClientAttendeeWrongType->getId()
                        && $attendeeData['lastActive'] instanceof \DateTime
                        && $mins <= 5;
                },
                $salesChannelContext
            ),
            $salesChannelContext
        ];
    }

    /**
     * @dataProvider getTestJoinMeetingWithValidAttendeeAndAppointmentHaveSalesChannelDomainProviderData
     */
    public function testJoinMeetingWithValidAttendeeAndAppointmentHaveSalesChannelDomain(
        string $appointmentId,
        AppointmentEntity $appointment,
        AttendeeEntity $attendee,
        callable $callback
    ): void
    {
        $asGuide = true;
        $customer = new CustomerEntity();
        $customer->setId('test-customer-id');
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext(null, $customer);
        /** @var SalesChannelDomainEntity $salesChannelDomain */
        $salesChannelDomain = $appointment->getSalesChannelDomain();

        $contextDataReader = $this->createContextDataReaderMock(
            AttendeeDefinition::TYPE_GUIDE,
            $appointmentId,
            $attendee,
            'credentials',
            $salesChannelContext,
            1
        );

        $channelContextPersiter = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersiter->expects(static::once())
            ->method('save')
            ->with(
                static::callback(function (string $newToken) use ($salesChannelContext) {
                    // contextToken already be replaced by another using revokeContext method
                    return $newToken !== $salesChannelContext->getToken();
                }),
                static::equalTo(
                    [
                        GuidedShoppingRequestContextResolver::TOKEN_FIELD_ATTENDEE_ID => $attendee->getId(),
                        'languageId' => $salesChannelContext->getLanguageId(),
                        'currencyId' => $salesChannelContext->getCurrencyId(),
                    ]
                ),
                static::equalTo($salesChannelContext->getSalesChannelId()),
                static::equalTo(null)
            );

        $attendeeService = $this->createAttendeeServiceMock(
            false,
            true,
            null,
            null,
            null,
            null,
            null,
            $attendee,
            $callback,
            $salesChannelContext
        );

        $stateServiceFactory = $this->createStateServiceFactoryMock(true, $attendee, $salesChannelContext);

        $service = $this->getService(
            null,
            null,
            $stateServiceFactory,
            $channelContextPersiter,
            $attendeeService,
            null,
            $contextDataReader
        );
        $struct = $service->joinMeeting($appointmentId, $salesChannelContext, $asGuide);

        $expectStruct = new AppointmentJoinStruct(
            $appointmentId,
            $struct->getNewContextToken(), // contextToken already be replaced by another using revokeContext method
            $attendee->getId(),
            $salesChannelDomain->getSalesChannelId(),
            $appointment->getMode(),
            $appointment->isPreview()
        );
        $expectStruct->setSalesChannelName($salesChannelContext->getSalesChannel()->getTranslation('name'));
        $expectStruct->setAppointmentName($appointment->getTranslation('name'));
        static::assertEquals($expectStruct, $struct);
    }

    public static function getTestJoinMeetingWithValidAttendeeAndAppointmentHaveSalesChannelDomainProviderData(): \Generator
    {
        $self = new JoinAppointmentServiceTest('test');
        $guidedAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $guidedAppointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $selfAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $selfAppointment->setMode(AppointmentDefinition::MODE_SELF);

        $guidedAppointmentAndAttendeeHasNotJoinedYet = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $guidedAppointment->getId(), $guidedAppointment);
        $guidedAppointmentAndAttendeeHasNotJoinedYet->setCustomerId('test-customer-id');
        yield 'guided-appointment-and-attendee-has-not-joined-yet' => [
            $guidedAppointment->getId(),
            $guidedAppointment,
            $guidedAppointmentAndAttendeeHasNotJoinedYet,
            /** @param array<string, mixed> $attendeeData */
            function (array $attendeeData) {
                $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));

                return $attendeeData['attendeeName'] === 'test-get-attendee-name'
                    && $attendeeData['joinedAt'] === NULL
                    && $attendeeData['customerId'] === 'test-customer-id'
                    && $diffActiveTime->format('%s') <= 5; // delay time in test env
            },
        ];

        $selfAppointmentAndAttendeeHasNotJoinedYet = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $selfAppointment->getId(), $selfAppointment);
        $selfAppointmentAndAttendeeHasNotJoinedYet->setCustomerId('test-customer-id');
        yield 'self-appointment-and-attendee-has-not-joined-yet' => [
            $selfAppointment->getId(),
            $selfAppointment,
            $selfAppointmentAndAttendeeHasNotJoinedYet,
            /** @param array<string, mixed> $attendeeData */
            function (array $attendeeData) use ($selfAppointmentAndAttendeeHasNotJoinedYet) {
                $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));
                $diffJoinedTime = \date_diff($attendeeData['joinedAt'], new \DateTime('now'));

                return $attendeeData['attendeeName'] === 'test-get-attendee-name'
                    && $attendeeData['customerId'] === $selfAppointmentAndAttendeeHasNotJoinedYet->getCustomerId()
                    && $diffJoinedTime->format('%s') <= 5 // delay time in test env
                    && $diffActiveTime->format('%s') <= 5; // delay time in test env
            },
        ];

        $guidedAppointmentAndAttendeeHasJoined = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $guidedAppointment->getId(), $guidedAppointment);
        $guidedAppointmentAndAttendeeHasJoined->setJoinedAt(new \DateTime());
        $guidedAppointmentAndAttendeeHasJoined->setAttendeeName('test-attendee-name');
        yield 'guided-appointment-and-attendee-has-joined' => [
            $guidedAppointment->getId(),
            $guidedAppointment,
            $guidedAppointmentAndAttendeeHasJoined,
            /** @param array<string, mixed> $attendeeData */
            function (array $attendeeData) use ($guidedAppointmentAndAttendeeHasJoined) {
                $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));

                return $attendeeData['attendeeName'] === 'test-get-attendee-name'
                    && $attendeeData['joinedAt'] === $guidedAppointmentAndAttendeeHasJoined->getJoinedAt()
                    && $attendeeData['customerId'] === NULL
                    && $diffActiveTime->format('%s') <= 5; // delay time in test env
            },
        ];

        $selfAppointmentAndAttendeeHasJoined = $self->createTestAttendee(AttendeeDefinition::TYPE_GUIDE, $selfAppointment->getId(), $selfAppointment);
        $selfAppointmentAndAttendeeHasJoined->setJoinedAt(new \DateTime());
        $guidedAppointmentAndAttendeeHasJoined->setAttendeeName('test-attendee-name');
        yield 'self-appointment-and-attendee-has-joined' => [
            $selfAppointment->getId(),
            $selfAppointment,
            $selfAppointmentAndAttendeeHasJoined,
            /** @param array<string, mixed> $attendeeData */
            function (array $attendeeData) use ($selfAppointmentAndAttendeeHasJoined) {
                $diffActiveTime = \date_diff($attendeeData['lastActive'], new \DateTime('now'));

                return $attendeeData['attendeeName'] === 'test-get-attendee-name'
                    && $attendeeData['joinedAt'] === $selfAppointmentAndAttendeeHasJoined->getJoinedAt()
                    && $attendeeData['customerId'] === $selfAppointmentAndAttendeeHasJoined->getCustomerId()
                    && $diffActiveTime->format('%s') <= 5; // delay time in test env
            },
        ];
    }

    private function getService(
        ?MockObject $topicGeneratorMock = null,
        ?MockObject $jwtMercureTokenFactoryMock = null,
        ?MockObject $presentationStateServiceFactoryMock = null,
        ?MockObject $channelContextPersisterMock = null,
        ?MockObject $attendeeServiceMock = null,
        ?MockObject $systemConfigServiceMock = null,
        ?MockObject $contextDataReaderMock = null
    ): JoinAppointmentService
    {
        /** @var MockObject&TopicGenerator $topicGenerator */
        $topicGenerator = $topicGeneratorMock ?? $this->createMock(TopicGenerator::class);
        /** @var MockObject&JWTMercureTokenFactory $jwtMercureTokenFactory */
        $jwtMercureTokenFactory = $jwtMercureTokenFactoryMock ?? $this->createMock(JWTMercureTokenFactory::class);
        /** @var MockObject&PresentationStateServiceFactory $presentationStateServiceFactory */
        $presentationStateServiceFactory = $presentationStateServiceFactoryMock ?? $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&SalesChannelContextPersister $channelContextPersister */
        $channelContextPersister = $channelContextPersisterMock ?? $this->createMock(SalesChannelContextPersister::class);
        /** @var MockObject&AttendeeService $attendeeService */
        $attendeeService = $attendeeServiceMock ?? $this->createMock(AttendeeService::class);
        /** @var MockObject&SystemConfigService $systemConfigService */
        $systemConfigService = $systemConfigServiceMock ?? $this->createMock(SystemConfigService::class);
        /** @var MockObject&ContextDataHelper $contextDataReader */
        $contextDataReader = $contextDataReaderMock ?? $this->createMock(ContextDataHelper::class);

        return new JoinAppointmentService(
            $topicGenerator,
            $jwtMercureTokenFactory,
            $presentationStateServiceFactory,
            $channelContextPersister,
            $attendeeService,
            $systemConfigService,
            $contextDataReader
        );
    }

    private function createTestAttendee(string $attendeeType, string $appointmentId, AppointmentEntity $appointment): AttendeeEntity
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAppointmentId($appointmentId );
        $attendee->setAppointment($appointment);
        $attendee->setType($attendeeType);

        return $attendee;
    }

    private function createChannelContextPersisterMock(
        ?bool $isRevoke = false,
        ?bool $isSave = false,
        ?bool $hasNewContextToken = false,
        ?SalesChannelContext $salesChannelContext = null,
        ?string $attendeeId = null,
        ?string $customerId = null
    ): MockObject
    {
        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);

        if ($isRevoke && $salesChannelContext) {
            $channelContextPersister->expects(static::once())
                ->method('delete')
                ->with(
                    static::equalTo($salesChannelContext->getToken()),
                    static::equalTo($salesChannelContext->getSalesChannelId())
                );
        } else {
            $channelContextPersister->expects(static::never())->method('delete');
        }

        if ($isSave && $salesChannelContext) {
            $channelContextPersister->expects(static::once())
                ->method('save')
                ->with(
                    $hasNewContextToken ? static::callback(function (string $newToken) use ($salesChannelContext) {
                        // contextToken already be replaced by another using revokeContext method
                        return $newToken !== $salesChannelContext->getToken();
                    }) : static::equalTo($salesChannelContext->getToken()),
                    static::equalTo([GuidedShoppingRequestContextResolver::TOKEN_FIELD_ATTENDEE_ID => $attendeeId]),
                    static::equalTo($salesChannelContext->getSalesChannelId()),
                    static::equalTo($customerId)
                );
        } else {
            $channelContextPersister->expects(static::never())->method('save');
        }

        return $channelContextPersister;
    }

    private function createContextDataReaderMock(
        string $attendeeType,
        string $appointmentId,
        AttendeeEntity $attendee,
        string $expectGetAttendeeFrom,
        SalesChannelContext $salesChannelContext,
        ?int $timeCountToGetAttendeeName = 0
    ): MockObject
    {
        $contextDataReader = $this->createMock(ContextDataHelper::class);
        $contextDataReader->expects(static::once())
            ->method('getCustomerIdFromContext')
            ->with(
                static::equalTo($salesChannelContext)
            )
            ->willReturn('test-customer-id');

        switch ($expectGetAttendeeFrom) {
            case 'credentials':
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromCredentials')
                    ->with(
                        static::equalTo($salesChannelContext),
                        static::equalTo($appointmentId),
                        static::equalTo($attendeeType)
                    )
                    ->willReturn($attendee);
                $contextDataReader->expects(static::never())->method('getAttendeeFromContext');
                $contextDataReader->expects(static::never())->method('getUserIdFromContext');
                break;
            case 'credentials-but-invalid-attendee':
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromCredentials')
                    ->with(
                        static::equalTo($salesChannelContext),
                        static::equalTo($appointmentId),
                        static::equalTo($attendeeType)
                    )
                    ->willReturn($attendee);
                $contextDataReader->expects(static::never())->method('getAttendeeFromContext');
                $contextDataReader->expects(static::once())
                    ->method('getUserIdFromContext')
                    ->with(
                        static::equalTo($salesChannelContext)
                    )
                    ->willReturn('test-user-id');
                break;
            case 'context':
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromCredentials')
                    ->with(
                        static::equalTo($salesChannelContext),
                        static::equalTo($appointmentId),
                        static::equalTo($attendeeType)
                    )
                    ->willReturn(null);
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromContext')
                    ->with(
                        static::equalTo($salesChannelContext)
                    )
                    ->willReturn($attendee);
                $contextDataReader->expects(static::never())->method('getUserIdFromContext');
                break;
            case 'context-but-invalid-attendee':
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromCredentials')
                    ->with(
                        static::equalTo($salesChannelContext),
                        static::equalTo($appointmentId),
                        static::equalTo($attendeeType)
                    )
                    ->willReturn(null);
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromContext')
                    ->with(
                        static::equalTo($salesChannelContext)
                    )
                    ->willReturn($attendee);
                $contextDataReader->expects(static::once())
                    ->method('getUserIdFromContext')
                    ->with(
                        static::equalTo($salesChannelContext)
                    )
                    ->willReturn('test-user-id');
                break;
            case 'service':
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromCredentials')
                    ->with(
                        static::equalTo($salesChannelContext),
                        static::equalTo($appointmentId),
                        static::equalTo($attendeeType)
                    )
                    ->willReturn(null);
                $contextDataReader->expects(static::once())
                    ->method('getAttendeeFromContext')
                    ->with(
                        static::equalTo($salesChannelContext)
                    )
                    ->willReturn(null);
                $contextDataReader->expects(static::once())
                    ->method('getUserIdFromContext')
                    ->with(
                        static::equalTo($salesChannelContext)
                    )
                    ->willReturn('test-user-id');
                break;
            default:
                // nothing happen;
        }

        /** @var int $timeCountToGetAttendeeName */
        $contextDataReader->expects(static::exactly($timeCountToGetAttendeeName))
            ->method('getAttendeeNameFromContext')
            ->with(
                static::equalTo($salesChannelContext)
            )
            ->willReturn('test-get-attendee-name');

        return $contextDataReader;
    }

    private function createStateServiceFactoryMock(
        bool $isBuild,
        ?AttendeeEntity $attendee = null,
        ?SalesChannelContext $salesChannelContext = null
    ): MockObject
    {
        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        if ($isBuild && $attendee && $salesChannelContext) {
            $stateService = $this->createMock(PresentationStateService::class);
            $stateService->expects(static::once())->method('publishStateForGuides');
            $stateServiceFactory->expects(static::once())
                ->method('build')
                ->with(
                    static::equalTo($attendee->getAppointmentId()),
                    static::equalTo($salesChannelContext->getContext())
                )
                ->willReturn($stateService);
        } else {
            $stateServiceFactory->expects(static::never())->method('build');
        }

        return $stateServiceFactory;
    }

    private function createAttendeeServiceMock(
        bool $isCreate,
        bool $isUpdate,
        ?string $appointmentId = null,
        ?string $attendeeType = null,
        ?string $customerId = null,
        ?string $userId = null,
        ?string $attendeeName = null,
        ?AttendeeEntity $attendee = null,
        ?callable $callback = null,
        ?SalesChannelContext $salesChannelContext = null
    ): MockObject
    {
        $attendeeService = $this->createMock(AttendeeService::class);

        if ($isCreate) {
            $attendeeService->expects(static::once())
                ->method('createAttendee')
                ->with(
                    static::equalTo($appointmentId),
                    static::equalTo($salesChannelContext),
                    static::equalTo($attendeeType),
                    static::equalTo($customerId),
                    static::equalTo($userId),
                    static::equalTo($attendeeName)
                );
        } else {
            $attendeeService->expects(static::never())->method('createAttendee');
        }

        if ($isUpdate && $attendee && $salesChannelContext && $callback) {
            $attendeeService->expects(static::once())
                ->method('updateAttendee')
                ->with(
                    static::equalTo($attendee->getId()),
                    static::callback($callback),
                    static::equalTo($salesChannelContext->getContext())
                );
        } else {
            $attendeeService->expects(static::never())->method('updateAttendee');
        }

        return $attendeeService;
    }

    private function createJWTMercureTokenFactoryMock(?bool $isCreateSubscriberToken = false, ?bool $isCreatePublisherToken = false): MockObject
    {
        $jwtMercureTokenFactory = $this->createMock(JWTMercureTokenFactory::class);
        if ($isCreateSubscriberToken) {
            $jwtMercureTokenFactory->expects(static::once())
                ->method('createSubscriberToken')
                ->with(
                    static::equalTo(['test-subscriber-topics'])
                )
                ->willReturn('test-subscriber-token');
        }

        if ($isCreatePublisherToken) {
            $jwtMercureTokenFactory->expects(static::once())
                ->method('createPublisherToken')
                ->with(
                    static::equalTo(['test-publisher-topics'])
                )
                ->willReturn('test-publisher-token');
        }

        return $jwtMercureTokenFactory;
    }

    private function getExpectStructWhenJoinMeeting(
        AppointmentEntity $appointment,
        string $attendeeId,
        string $salesChannelId,
        SalesChannelContext $salesChannelContext,
        ?string $newContextToken = null
    ): AppointmentJoinStruct
    {
        $appointmentMode = $appointment->getMode();

        $struct = new AppointmentJoinStruct(
            $appointment->getId(),
            $newContextToken ?: $salesChannelContext->getToken(),
            $attendeeId,
            $salesChannelId,
            $appointmentMode
        );

        if ($appointmentMode === AppointmentDefinition::MODE_GUIDED) {
            $struct->setMercureSubscriberTopics(['test-subscriber-topics']);
            $struct->setJWTMercureSubscriberToken('test-subscriber-token');
            $struct->setMercurePublisherTopic('test-publisher-topics');
            $struct->setJWTMercurePublisherToken('test-publisher-token');
            $struct->setMercureHubPublicUrl('test-mercure-hub-public-url');
        }

        $struct->setSalesChannelName($salesChannelContext->getSalesChannel()->getTranslation('name'));
        $struct->setAppointmentName($appointment->getTranslation('name'));

        return $struct;
    }
}
