<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Document\DocumentCollection;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Event\BeforeAttendeeCreatedEvent;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeCouldNotBeCreatedException;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AttendeeServiceTest extends TestCase
{
    public function testUpdateAttendeeWithEmptyAttendeeData(): void
    {
        $attendeeId = 'test-attendee-id';
        $attendeeData = [];
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeData['id'] = $attendeeId;
        $attendeeRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::equalTo([$attendeeData]),
                static::equalTo($context)
            );
        $attendeeRepository->expects(static::never())->method('search');

        $service = $this->getService($attendeeRepository);
        $service->updateAttendee($attendeeId, $attendeeData, $context);
    }

    /**
     * @param array<string, mixed> $attendeeData
     * @dataProvider getTestUpdateAttendeeWithValidAttendeeDataButCanNotFindAttendeeProviderData
     */
    public function testUpdateAttendeeWithValidAttendeeDataButCanNotFindAttendee(array $attendeeData): void
    {
        $attendeeId = 'test-attendee-id';
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeData['id'] = $attendeeId;
        $attendeeRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::equalTo([$attendeeData]),
                static::equalTo($context)
            );
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo(new Criteria([$attendeeId])),
                static::equalTo($context)
            );

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::never())->method('build');

        $service = $this->getService($attendeeRepository);
        static::expectException(AttendeeNotFoundException::class);
        $service->updateAttendee($attendeeId, $attendeeData, $context);
    }

    public static function getTestUpdateAttendeeWithValidAttendeeDataButCanNotFindAttendeeProviderData(): \Generator
    {
        yield [['attendeeName' => 'test-attendee-name']];
        yield [['videoUserId' => 'test-video-user-id']];
        yield [['guideCartPermissionsGranted' => true]];
        yield [['attendeeName' => 'test-attendee-name', 'videoUserId' => 'test-video-user-id']];
        yield [['attendeeName' => 'test-attendee-name', 'guideCartPermissionsGranted' => true]];
        yield [['videoUserId' => 'test-video-user-id', 'guideCartPermissionsGranted' => true]];
        yield [['attendeeName' => 'test-attendee-name', 'videoUserId' => 'test-video-user-id', 'guideCartPermissionsGranted' => true]];
    }

    /**
     * @param array<string, mixed> $attendeeData
     * @param array<string, mixed> $expectStateForMe
     * @dataProvider getTestUpdateAttendeeWithValidAttendeeDataAndCanFindAttendeeProviderData
     */
    public function testUpdateAttendeeWithValidAttendeeDataAndCanFindAttendeeProviderData(
        string $clientType,
        string $assertionArrayKey,
        array $attendeeData,
        array $expectStateForMe
    ): void
    {
        $attendeeId = 'test-attendee-id';
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeData['id'] = $attendeeId;
        $attendeeRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::equalTo([$attendeeData]),
                static::equalTo($context)
            );
        $criteria = new Criteria([$attendeeId]);
        $attendee = $this->createDemoAttendee($attendeeId, $clientType, $attendeeData);
        $result = new EntitySearchResult(
            AttendeeEntity::class,
            1,
            new AttendeeCollection([$attendee]),
            null,
            $criteria,
            $context,
        );
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn($result);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic', []);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateForClients = new StateForClients('test-appointment-id', 'test-mercure-topic', []);
        $stateService->expects(static::once())
            ->method('getStateForClients')
            ->willReturn($stateForClients);
        $stateForMe = new StateForMe('test-appointment-id', 'test-mercure-topic', []);
        $stateService->expects(static::once())
            ->method('getStateForMe')
            ->willReturn($stateForMe);
        $stateService->expects(static::once())
            ->method('publishStateForGuides');
        $stateService->expects(static::once())
            ->method('publishStateForClients');
        $stateService->expects(static::once())
            ->method('publishStateForMe');
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo($attendee->getAppointmentId()),
                static::equalTo($context)
            )
            ->willReturn($stateService);

        $service = $this->getService($attendeeRepository, $stateServiceFactory);
        $service->updateAttendee($attendeeId, $attendeeData, $context);
        $guidePayload = $stateForGuides->getData();
        static::assertEquals($attendeeData['attendeeName'], $guidePayload[$assertionArrayKey]['test-attendee-id']['attendeeName']);
        static::assertEquals($attendeeData['videoUserId'], $guidePayload[$assertionArrayKey]['test-attendee-id']['videoUserId']);
        static::assertEquals($attendeeData['guideCartPermissionsGranted'], $guidePayload['clients']['test-attendee-id']['guideCartPermissionsGranted']);
        $clientPayload = $stateForClients->getData();
        static::assertEquals($guidePayload['clients'], $clientPayload['clients']);
        static::assertEquals($guidePayload['guides'], $clientPayload['guides']);
        $mePayload = $stateForMe->getData();
        static::assertEquals($expectStateForMe['attendeeName'], $mePayload['attendeeName']);
        static::assertEquals($expectStateForMe['guideCartPermissionsGranted'], $mePayload['guideCartPermissionsGranted']);
        static::assertEquals($expectStateForMe['attendeeSubmittedAt'], $mePayload['attendeeSubmittedAt']);
    }

    public static function getTestUpdateAttendeeWithValidAttendeeDataAndCanFindAttendeeProviderData(): \Generator
    {
        yield 'attendee-is-client-without-submit' => [
            AttendeeDefinition::TYPE_CLIENT,
            'clients',
            [
                'attendeeName' => 'test-attendee-name',
                'videoUserId' => 'test-video-user-id',
                'guideCartPermissionsGranted' => true,
                'attendeeSubmittedAt' => null
            ],
            [
                'attendeeName' => null,
                'guideCartPermissionsGranted' => false,
                'attendeeSubmittedAt' => null
            ]
        ];

        yield 'attendee-is-guide-without-submit' => [
            AttendeeDefinition::TYPE_GUIDE,
            'guides',
            [
                'attendeeName' => 'test-attendee-name',
                'videoUserId' => 'test-video-user-id',
                'guideCartPermissionsGranted' => true,
                'attendeeSubmittedAt' => null
            ],
            [
                'attendeeName' => null,
                'guideCartPermissionsGranted' => false,
                'attendeeSubmittedAt' => null
            ]
        ];

        $submittedTime = new \DateTime();

        yield 'attendee-is-client-with-submit' => [
            AttendeeDefinition::TYPE_CLIENT,
            'clients',
            [
                'attendeeName' => 'test-attendee-name',
                'videoUserId' => 'test-video-user-id',
                'guideCartPermissionsGranted' => true,
                'attendeeSubmittedAt' => $submittedTime
            ],
            [
                'attendeeName' => 'test-attendee-name',
                'guideCartPermissionsGranted' => 'test-video-user-id',
                'attendeeSubmittedAt' => $submittedTime
            ]
        ];

        yield 'attendee-is-guide-with-submit' => [
            AttendeeDefinition::TYPE_GUIDE,
            'guides',
            [
                'attendeeName' => 'test-attendee-name',
                'videoUserId' => 'test-video-user-id',
                'guideCartPermissionsGranted' => true,
                'attendeeSubmittedAt' => $submittedTime
            ],
            [
                'attendeeName' => 'test-attendee-name',
                'guideCartPermissionsGranted' => 'test-video-user-id',
                'attendeeSubmittedAt' => $submittedTime
            ]
        ];
    }

    /**
     * @dataProvider getTestFindAttendeeProviderData
     */
    public function testFindAttendeeByCustomer(
        AttendeeCollection $attendeeCollection,
        ?AttendeeEntity $attendee
    ): void
    {
        $criteria = $this->getCriteria(AttendeeDefinition::TYPE_CLIENT);
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AttendeeEntity::class,
                    $attendeeCollection->count(),
                    $attendeeCollection,
                    null,
                    $criteria,
                    $context
                )
            );

        $service = $this->getService($attendeeRepository);
        $result = $service->findAttendeeByCustomer('test-customer-id', 'test-appointment-id', $context);
        static::assertSame($attendee, $result);
    }

    /**
     * @dataProvider getTestFindAttendeeProviderData
     */
    public function testFindAttendeeByUser(
        AttendeeCollection $attendeeCollection,
        ?AttendeeEntity $attendee
    ): void
    {
        $criteria = $this->getCriteria(AttendeeDefinition::TYPE_GUIDE);
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AttendeeEntity::class,
                    $attendeeCollection->count(),
                    $attendeeCollection,
                    null,
                    $criteria,
                    $context
                )
            );

        $service = $this->getService($attendeeRepository);
        $result = $service->findAttendeeByUser('test-customer-id', 'test-appointment-id', $context);
        static::assertSame($attendee, $result);
    }

    public static function getTestFindAttendeeProviderData(): \Generator
    {
        yield 'attendee-not-found' => [
            new AttendeeCollection(),
            null
        ];

        $self = new AttendeeServiceTest('test');
        $attendee = $self->createDemoAttendee('test-attendee-id', AttendeeDefinition::TYPE_GUIDE);
        yield 'found-attendee' => [
            new AttendeeCollection([$attendee]),
            $attendee
        ];
    }

    /**
     * @param array<string, mixed> $attendeeData
     * @dataProvider getTestCreateAttendeeProviderData
     */
    public function testCreateAttendeeAndCanFindAttendee(array $attendeeData): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $context = $salesChannelContext->getContext();
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $event = new BeforeAttendeeCreatedEvent($attendeeData, $salesChannelContext);
        $eventDispatcher->expects(static::once())
            ->method('dispatch')
            ->willReturn($event);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('create')
            ->with(
                static::equalTo([$attendeeData]),
                static::equalTo($context)
            );
        $attendee = $this->createDemoAttendee($attendeeData['id'], $attendeeData['type'], $attendeeData);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    AttendeeEntity::class,
                    1,
                    new AttendeeCollection([$attendee]),
                    null,
                    new Criteria(),
                    $context
                )
            );

        $service = $this->getService($attendeeRepository, null, $eventDispatcher);
        $result = $service->createAttendee(
            $attendeeData['appointmentId'],
            $salesChannelContext,
            $attendeeData['type'],
            $attendeeData['customerId'] ?? null,
            $attendeeData['userId'] ?? null,
            $attendeeData['attendeeName']
        );
        static::assertSame($attendee, $result);
    }

    /**
     * @param array<string, mixed> $attendeeData
     * @dataProvider getTestCreateAttendeeProviderData
     */
    public function testCreateAttendeeButCanNotFindAttendee(array $attendeeData): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $context = $salesChannelContext->getContext();
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $event = new BeforeAttendeeCreatedEvent($attendeeData, $salesChannelContext);
        $eventDispatcher->expects(static::once())
            ->method('dispatch')
            ->willReturn($event);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('create')
            ->with(
                static::equalTo([$attendeeData]),
                static::equalTo($context)
            );
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(
                new EntitySearchResult(
                    AttendeeEntity::class,
                    1,
                    new DocumentCollection(),
                    null,
                    new Criteria(),
                    $context
                )
            );

        $service = $this->getService($attendeeRepository, null, $eventDispatcher);
        static::expectException(AttendeeCouldNotBeCreatedException::class);
        $service->createAttendee(
            $attendeeData['appointmentId'],
            $salesChannelContext,
            $attendeeData['type'],
            $attendeeData['customerId'] ?? null,
            $attendeeData['userId'] ?? null,
            $attendeeData['attendeeName']
        );
    }

    public static function getTestCreateAttendeeProviderData(): \Generator
    {
        yield 'attendee-is-guide' => [
            [
                'id' => 'test-attendee-id',
                'appointmentId' => 'test-appointment-id',
                'type' => AttendeeDefinition::TYPE_GUIDE,
                'userId' => 'test-user-id',
                'attendeeName' => 'test-attendee-name',
                'lastActive' => new \DateTime,
            ]
        ];

        yield 'attendee-is-client' => [
            [
                'id' => 'test-attendee-id',
                'appointmentId' => 'test-appointment-id',
                'type' => AttendeeDefinition::TYPE_CLIENT,
                'customerId' => 'test-customer-id',
                'attendeeName' => 'test-attendee-name',
                'lastActive' => new \DateTime,
            ]
        ];
    }

    public function testUpdateInvitationStatusWithEmptyAttendeeEmails(): void
    {
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('searchIds');
        $attendeeRepository->expects(static::never())->method('update');

        $service = $this->getService($attendeeRepository);
        $result = $service->updateInvitationStatus([], 'test-appointment-id', 'accepted', $context);
        static::assertFalse($result);
    }

    public function testUpdateInvitationStatusSuccessfully(): void
    {
        $appointmentId = 'test-appointment-id';
        $attendeeEmail = 'test-attendee-email';
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', $appointmentId));
        $criteria->addFilter(new EqualsAnyFilter('attendeeEmail', [$attendeeEmail]));
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendeeRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['test-attendee-id'], 'primaryKey' => 'test-attendee-id']],
                new Criteria(),
                $context
            ));
        $attendeeRepository->expects(static::once())
            ->method('update')
            ->with(
                static::equalTo([
                    [
                        'id' => $attendee->getId(),
                        'invitationStatus' => 'accepted'
                    ]
                ]),
                static::equalTo($context)
            );

        $service = $this->getService($attendeeRepository);
        $result = $service->updateInvitationStatus([$attendeeEmail], $appointmentId, 'accepted', $context);
        static::assertTrue($result);
    }

    public function testUpdateInvitationStatusButCouldNotFindAttendee(): void
    {
        $appointmentId = 'test-appointment-id';
        $attendeeEmail = 'test-attendee-email';
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', $appointmentId));
        $criteria->addFilter(new EqualsAnyFilter('attendeeEmail', [$attendeeEmail]));
        $attendeeRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new IdSearchResult(
                0,
                [],
                new Criteria(),
                $context
            ));
        $attendeeRepository->expects(static::never())->method('update');

        $service = $this->getService($attendeeRepository);
        $result = $service->updateInvitationStatus([$attendeeEmail], $appointmentId, 'maybe', $context);
        static::assertFalse($result);
    }

    public function testUpdateInvitationStatusButFailedUpdatingAttendee(): void
    {
        $appointmentId = 'test-appointment-id';
        $attendeeEmail = 'test-attendee-email';
        $context = new Context(new SystemSource());

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendeeRepository->expects(static::once())
            ->method('searchIds')
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['test-attendee-id']]],
                new Criteria(),
                $context
            ));
        $attendeeRepository->expects(static::once())
            ->method('update')
            ->willThrowException(new \Exception('test-throw-exception'));

        $service = $this->getService($attendeeRepository);
        $result = $service->updateInvitationStatus([$attendeeEmail], $appointmentId, 'declined', $context);
        static::assertFalse($result);
    }

    private function getService(
        ?MockObject $attendeeRepositoryMock = null,
        ?MockObject $stateServiceFactoryMock = null,
        ?MockObject $eventDispatcherMock = null
    ): AttendeeService
    {
        /** @var MockObject&EntityRepository<AttendeeCollection> $attendeeRepository */
        $attendeeRepository = $attendeeRepositoryMock ?? $this->createMock(EntityRepository::class);
        /** @var MockObject&PresentationStateServiceFactory $stateServiceFactory */
        $stateServiceFactory = $stateServiceFactoryMock ?? $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $eventDispatcherMock ?? $this->createMock(EventDispatcherInterface::class);

        return new AttendeeService($attendeeRepository, $stateServiceFactory, $eventDispatcher);
    }

    /**
     * @param array<string, mixed> $attendeeData
     */
    private function createDemoAttendee(string $attendeeId, string $attendeeType, ?array $attendeeData = []): AttendeeEntity
    {
        $attendee = new AttendeeEntity();
        $attendee->setId($attendeeId);
        $attendee->setAppointmentId('test-appointment-id');
        $attendee->setType($attendeeType);
        $attendee->setAttendeeName($attendeeData['attendeeName'] ?? null);
        $attendee->setGuideCartPermissionsGranted($attendeeData['guideCartPermissionsGranted'] ?? false);
        $attendee->setVideoUserId($attendeeData['videoUserId'] ?? null);
        $attendee->setAttendeeSubmittedAt($attendeeData['attendeeSubmittedAt'] ?? null);

        return $attendee;
    }

    private function getCriteria(string $clientType): Criteria
    {
        $criteria = new Criteria();
        if ($clientType === AttendeeDefinition::TYPE_CLIENT) {
            $criteria->addFilter(new EqualsFilter('customerId', 'test-customer-id'));
        } else {
            $criteria->addFilter(new EqualsFilter('userId', 'test-customer-id'));
            $criteria->addAssociation('user');
        }

        $criteria->addFilter(new EqualsFilter('appointmentId', 'test-appointment-id'));
        $criteria->addFilter(new EqualsFilter('type', $clientType));

        return $criteria;
    }
}
