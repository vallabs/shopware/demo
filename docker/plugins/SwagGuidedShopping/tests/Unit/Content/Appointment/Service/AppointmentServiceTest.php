<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAlreadyEndedException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAlreadyStartedException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsPreviewException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsSelfModeException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetStartedException;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Cms\Service\CmsPageCreatorService;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatCreateStruct;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;
use SwagGuidedShopping\Exception\NotConfiguredException;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Service\AppointmentService
 *
 * @internal
 */
class AppointmentServiceTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    /**
     * @param array<int, string> $associations
     * @dataProvider getTestGetAppointmentProviderData
     */
    public function testGetAppointmentWithException(
        array $associations,
        Criteria $expectCriteria
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $criteria = new Criteria();

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria->setIds([$appointmentId]);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    0,
                    new AppointmentCollection(),
                    null,
                    $criteria,
                    $this->context
                )
            );

        $service = $this->getAppointmentService(null, $appointmentRepository);
        static::expectException(EntityNotFoundException::class);
        $service->getAppointment($appointmentId, $this->context, $associations);
    }

    /**
     * @param array<int, string> $associations
     * @dataProvider getTestGetAppointmentProviderData
     */
    public function testGetAppointmentSuccessfully(
        array $associations,
        Criteria $expectCriteria
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $criteria = new Criteria();

        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $expectCriteria->setIds([$appointmentId]);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$expectAppointment]),
                    null,
                    $criteria,
                    $this->context
                )
            );

        $service = $this->getAppointmentService(null, $appointmentRepository);
        $appointment = $service->getAppointment($appointmentId, $this->context, $associations);
        static::assertEquals($expectAppointment, $appointment);
    }

    public static function getTestGetAppointmentProviderData(): \Generator
    {
        yield 'no-associations' => [
            [],
            new Criteria()
        ];

        yield 'has-associations' => [
            ['attendees'],
            (new Criteria())->addAssociations(['attendees'])
        ];

        $includeCmsPagesCriteria = (new Criteria())
            ->addAssociations(['presentation.cmsPages']);
        $includeCmsPagesCriteria->getAssociation('presentation.cmsPages')
            ->addSorting(new FieldSorting('position'));
        yield 'has-presentationCmsPages-associations' => [
            ['presentation.cmsPages'],
            $includeCmsPagesCriteria
        ];
    }

    public function testGetAppointmentByUrlSuccessfully(): void
    {
        $presentationPath = 'test-presentation-path';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = new Criteria();
        $expectCriteria->addFilter(new EqualsFilter('presentationPath', $presentationPath));
        $expectCriteria->addAssociations(['attendees', 'presentation']);
        $expectAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$expectAppointment]),
                    null,
                    $expectCriteria,
                    $this->context
                )
            );

        $service = $this->getAppointmentService(null, $appointmentRepository);
        $appointment = $service->findAppointmentByUrl($presentationPath, $this->context);
        static::assertEquals($expectAppointment, $appointment);
    }

    public function testGetAppointmentByUrlUnsuccessfullyAndCanFindByAppointmentIdThatIsUrl(): void
    {
        $presentationPath = 'test-presentation-path';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = new Criteria();
        $expectCriteria->addFilter(new EqualsFilter('presentationPath', $presentationPath));
        $expectCriteria->addAssociations(['attendees', 'presentation']);
        $expectCriteria2 = new Criteria([$presentationPath]);
        $expectCriteria2->addAssociation('attendees');
        $expectAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentRepository->expects(static::exactly(2))
            ->method('search')
            ->willReturnOnConsecutiveCalls(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    0,
                    new AppointmentCollection(),
                    null,
                    $expectCriteria,
                    $this->context
                ),
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$expectAppointment]),
                    null,
                    $expectCriteria2,
                    $this->context
                ),
            );

        $service = $this->getAppointmentService(null, $appointmentRepository);
        $appointment = $service->findAppointmentByUrl($presentationPath, $this->context);
        static::assertEquals($expectAppointment, $appointment);
    }

    public function testGetAppointmentByUrlUnsuccessfullyAndCanNotFindByAppointmentIdThatIsUrlAsWell(): void
    {
        $presentationPath = 'test-presentation-path';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = new Criteria();
        $expectCriteria->addFilter(new EqualsFilter('presentationPath', $presentationPath));
        $expectCriteria->addAssociations(['attendees', 'presentation']);
        $appointmentRepository->expects(static::exactly(2))
            ->method('search')
            ->willReturnOnConsecutiveCalls(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    0,
                    new AppointmentCollection(),
                    null,
                    $expectCriteria,
                    $this->context
                ),
                static::throwException(new EntityNotFoundException(AppointmentEntity::class, $presentationPath))
            );

        $service = $this->getAppointmentService(null, $appointmentRepository);
        $appointment = $service->findAppointmentByUrl($presentationPath, $this->context);
        static::assertNull($appointment);
    }

    /**
     * @dataProvider getTestCheckAppointmentPresentationIsEnableProviderData
     */
    public function testCheckAppointmentPresentationIsEnable(
        AppointmentEntity $appointment,
        bool $expectIsEnable
    ): void
    {
        $service = $this->getAppointmentService();
        $isEnable = $service->isPresentationEnable($appointment);
        static::assertEquals($expectIsEnable, $isEnable);
    }

    public static function getTestCheckAppointmentPresentationIsEnableProviderData(): \Generator
    {
        $self = new AppointmentServiceTest('test');

        yield 'guide-appointment-has-no-presentation-version-id' => [
            $self->createTestAppointment(false, true),
            false
        ];

        yield 'guide-appointment-has-presentation-version-id' => [
            $self->createTestAppointment(true, true),
            true
        ];

        yield 'self-appointment-has-no-presentation-version-id' => [
            $self->createTestAppointment(false, false),
            false
        ];

        yield 'self-appointment-has-presentation-version-id' => [
            $self->createTestAppointment(true, false),
            false
        ];
    }

    /**
     * @dataProvider getTestStartPresentationWithExceptionProviderData
     * @param array<int, AppointmentEntity> $appointments
     * @param class-string<\Throwable> $exception
     */
    public function testStartPresentationWithException(
        array $appointments,
        string $exception
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = (new Criteria([$appointmentId]))
            ->addAssociations(['presentation.cmsPages']);
        $expectCriteria->getAssociation('presentation.cmsPages')
            ->addSorting(new FieldSorting('position'));
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    \count($appointments),
                    new AppointmentCollection($appointments),
                    null,
                    $expectCriteria,
                    $this->context
                )
            );
        $appointmentRepository->expects(static::never())->method('upsert');

        $videoManager = $this->createMock(VideoRoomManager::class);
        $videoManager->expects(static::never())->method('createRoom');
        $videoManager->expects(static::never())->method('saveVideoChatData');

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $presentationVersionHandler->expects(static::never())->method('createPresentationVersion');
        $presentationVersionHandler->expects(static::never())->method('updatePresentationCmsPages');

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateServiceFactory->expects(static::never())->method('build');

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('update');

        $service = $this->getAppointmentService(
            null,
            $appointmentRepository,
            $presentationStateServiceFactory,
            $videoManager,
            null,
            null,
            $presentationVersionHandler,
            $attendeeRepository
        );

        static::expectException($exception);
        $service->startPresentation($appointmentId, $this->context);
    }

    public static function getTestStartPresentationWithExceptionProviderData(): \Generator
    {
        yield 'no-appointment' => [
            [],
            EntityNotFoundException::class
        ];

        $appointmentMockHelper = new AppointMentMockHelper();

        $alreadyStartedAppointment = $appointmentMockHelper->getAppointEntity();
        $alreadyStartedAppointment->setStartedAt(new \DateTime());
        yield 'already-started' => [
            [$alreadyStartedAppointment],
            AppointmentAlreadyStartedException::class
        ];

        $selfModeAppointment = $appointmentMockHelper->getAppointEntity();
        $selfModeAppointment->setMode(AppointmentDefinition::MODE_SELF);
        yield 'self-mode' => [
            [$selfModeAppointment],
            AppointmentIsSelfModeException::class
        ];

        $alreadyEndedAppointment = $appointmentMockHelper->getAppointEntity();
        $alreadyEndedAppointment->setEndedAt(new \DateTime());
        yield 'already-ended' => [
            [$alreadyEndedAppointment],
            AppointmentAlreadyEndedException::class
        ];

        $inexistVideoChatAppointment = $appointmentMockHelper->getAppointEntity();
        $inexistVideoChatAppointment->setVideoAudioSettings(AppointmentDefinition::BOTH_VIDEO_AUDIO);
        $inexistVideoChatAppointment->setVideoChat(null);
        yield 'disable-video-chat' => [
            [$inexistVideoChatAppointment],
            VideoChatEntityDoesNotExist::class
        ];

        $previewAppointment = $appointmentMockHelper->getAppointEntity();
        $previewAppointment->setPreview(true);
        yield 'preview-appointment' => [
            [$previewAppointment],
            AppointmentIsPreviewException::class
        ];
    }

    /**
     * @dataProvider getTestStartPresentationSuccessfullyProviderData
     */
    public function testStartPresentationSuccessfully(
        AppointmentEntity $expectAppointment,
        MockObject $videoManager,
        MockObject $stateForAll,
        MockObject $stateForGuides,
        MockObject $stateForClients,
        Context $context
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = (new Criteria([$appointmentId]))
            ->addAssociations(['presentation.cmsPages']);
        $expectCriteria->getAssociation('presentation.cmsPages')
            ->addSorting(new FieldSorting('position'));
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$expectAppointment]),
                    null,
                    $expectCriteria,
                    $context
                )
            );

        $presentationVersionHandler = $this->createMock(PresentationVersionHandler::class);
        $expectPresentationVersionId = 'test-presentation-version-id';
        $presentationVersionHandler->expects(static::once())
            ->method('createPresentationVersion')
            ->with(
                static::equalTo($expectAppointment),
                static::equalTo($context)
            )
            ->willReturn($expectPresentationVersionId);
        $presentationVersionHandler->expects(static::once())
            ->method('updatePresentationCmsPages')
            ->with(
                static::equalTo($expectAppointment),
                static::equalTo($expectPresentationVersionId),
                static::equalTo($context)
            );

        $appointmentRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::callback(
                    /** @param array<int, array<string, mixed>> $upsertData */
                    function (array $upsertData) use ($appointmentId) {
                        $diffStartedTime = \date_diff($upsertData[0]['startedAt'], new \DateTime());
                        return $upsertData[0]['id'] === $appointmentId
                            && $diffStartedTime->format('%i') <= 5;
                }),
                static::equalTo($context)
            );

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateService = $this->createMock(PresentationStateService::class);
        $presentationStateService->expects(static::once())
            ->method('getStateForAll')
            ->willReturn($stateForAll);
        $presentationStateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $presentationStateService->expects(static::once())
            ->method('getStateForClients')
            ->willReturn($stateForClients);
        $presentationStateService->expects(static::once())->method('publishStateForAll');
        $presentationStateService->expects(static::once())->method('publishStateForGuides');
        $presentationStateService->expects(static::once())->method('publishStateForClients');
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo($expectAppointment->getId()),
                static::equalTo($context)
            )
            ->willReturn($presentationStateService);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('update')
            ->with(
                static::callback(function ($updateData) {
                    $diffJoinedTime = \date_diff($updateData[0]['joinedAt'], new \DateTime());

                    return $updateData[0]['id'] === 'test-attendee-id'
                        && $updateData[1]['id'] === 'test-attendee-id-2'
                        && $updateData[2]['id'] === 'test-guide-id'
                        && $updateData[0]['joinedAt'] === $updateData[1]['joinedAt']
                        && $updateData[0]['joinedAt'] === $updateData[2]['joinedAt']
                        && $diffJoinedTime->format('%i') <= 5;
                }),
                static::equalTo($context)
            );

        $service = $this->getAppointmentService(
            null,
            $appointmentRepository,
            $presentationStateServiceFactory,
            $videoManager,
            null,
            null,
            $presentationVersionHandler,
            $attendeeRepository
        );

        $service->startPresentation($appointmentId, $context);
    }

    public static function getTestStartPresentationSuccessfullyProviderData(): \Generator
    {
        $self = new AppointmentServiceTest('test');
        $context = new Context(new SystemSource());
        $appointmentMockHelper = new AppointMentMockHelper();

        $appointmentWithoutVideoAndAudio = $appointmentMockHelper->getAppointEntity();
        $appointmentWithoutVideoAndAudio->setVideoAudioSettings(AppointmentDefinition::NONE_VIDEO_AUDIO);
        yield 'appointment-without-video-and-audio' => [
            $appointmentWithoutVideoAndAudio,
            $self->createVideoManagerWhenStartPresentationMock(false),
            $self->createStateForAllWhenStartPresentationMock($appointmentWithoutVideoAndAudio, false),
            $self->createStateForGuidesWhenStartPresentationMock(false),
            $self->createStateForClientWhenStartPresentationMock(false),
            $context
        ];

        $appointmentWithAudioOnly = $appointmentMockHelper->getAppointEntity();
        $appointmentWithAudioOnly->setVideoAudioSettings(AppointmentDefinition::AUDIO_ONLY);
        yield 'appointment-with-audio-only' => [
            $appointmentWithAudioOnly,
            $self->createVideoManagerWhenStartPresentationMock(true, $appointmentWithAudioOnly, $context),
            $self->createStateForAllWhenStartPresentationMock($appointmentWithAudioOnly, true),
            $self->createStateForGuidesWhenStartPresentationMock(true),
            $self->createStateForClientWhenStartPresentationMock(true),
            $context
        ];

        $appointmentWithBothAudioAndVideo = $appointmentMockHelper->getAppointEntity();
        $appointmentWithBothAudioAndVideo->setVideoAudioSettings(AppointmentDefinition::BOTH_VIDEO_AUDIO);
        yield 'appointment-with-both-audio-and-video' => [
            $appointmentWithBothAudioAndVideo,
            $self->createVideoManagerWhenStartPresentationMock(true, $appointmentWithBothAudioAndVideo, $context),
            $self->createStateForAllWhenStartPresentationMock($appointmentWithBothAudioAndVideo, true),
            $self->createStateForGuidesWhenStartPresentationMock(true),
            $self->createStateForClientWhenStartPresentationMock(true),
            $context
        ];
    }

    /**
     * @dataProvider getTestEndPresentationWithExceptionProviderData
     * @param array<int, AppointmentEntity> $appointments
     * @param class-string<\Throwable> $exception
     */
    public function testEndPresentationWithException(
        array $appointments,
        MockObject $systemConfigService,
        string $exception
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = (new Criteria([$appointmentId]))
            ->addAssociations(['presentation']);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($this->context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    \count($appointments),
                    new AppointmentCollection($appointments),
                    null,
                    $expectCriteria,
                    $this->context
                )
            );

        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::never())->method('getInteractions');

        $cmsPageCreatorService = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreatorService->expects(static::never())->method('getPositionForLastSlide');
        $cmsPageCreatorService->expects(static::never())->method('createPresentationPage');

        $videoManager = $this->createMock(VideoRoomManager::class);
        $videoManager->expects(static::never())->method('removeRoomDataFromAppointment');
        $videoManager->expects(static::never())->method('deleteRoom');

        $appointmentRepository->expects(static::never())->method('upsert');

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateServiceFactory->expects(static::never())->method('build');

        $service = $this->getAppointmentService(
            $systemConfigService,
            $appointmentRepository,
            $presentationStateServiceFactory,
            $videoManager,
            $cmsPageCreatorService,
            $interactionService
        );

        static::expectException($exception);
        $service->endPresentation($appointmentId, $this->context);
    }


    public static function getTestEndPresentationWithExceptionProviderData(): \Generator
    {
        $self = new AppointmentServiceTest('test');

        yield 'empty' => [
            [],
            $self->createSystemConfigServiceMock(false),
            EntityNotFoundException::class
        ];

        $appointmentMockHelper = new AppointMentMockHelper();

        $alreadyEndedAppointment = $appointmentMockHelper->getAppointEntity();
        $alreadyEndedAppointment->setEndedAt(new \DateTime());
        yield 'already-ended' => [
            [$alreadyEndedAppointment],
            $self->createSystemConfigServiceMock(false),
            AppointmentAlreadyEndedException::class
        ];

        $notYetStartedAppointment = $appointmentMockHelper->getAppointEntity();
        $notYetStartedAppointment->setPresentationVersionId(null);
        yield 'not-yet-started' => [
            [$notYetStartedAppointment],
            $self->createSystemConfigServiceMock(false),
            AppointmentNotYetStartedException::class
        ];

        $notYetStartedAppointment2 = $appointmentMockHelper->getAppointEntity();
        $notYetStartedAppointment2->setStartedAt(null);
        yield 'not-yet-started-2' => [
            [$notYetStartedAppointment2],
            $self->createSystemConfigServiceMock(false),
            AppointmentNotYetStartedException::class
        ];

        $selfModeAppointment = $appointmentMockHelper->getAppointEntity();
        $selfModeAppointment->setMode(AppointmentDefinition::MODE_SELF);
        yield 'self-mode' => [
            [$selfModeAppointment],
            $self->createSystemConfigServiceMock(false),
            AppointmentIsSelfModeException::class
        ];

        $validAppointment = $appointmentMockHelper->getAppointEntity();
        $validAppointment->setStartedAt(new \DateTime());
        $validAppointment->setPresentationId('test-presentation-id');
        $validAppointment->setPresentationVersionId('test-presentation-version-id');
        yield 'valid' => [
            [$validAppointment],
            $self->createSystemConfigServiceMock(true, false),
            NotConfiguredException::class
        ];
    }

    /**
     * @dataProvider getTestEndPresentationSuccessfullyProviderData
     * @param AppointmentEntity $expectAppointment
     */
    public function testEndPresentationSuccessfully(
        AppointmentEntity $expectAppointment,
        Context $context,
        MockObject $videoManager
    ): void
    {
        $appointmentId = 'test-appointment-id';
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $expectCriteria = (new Criteria([$appointmentId]))
            ->addAssociations(['presentation']);
        $expectAppointment->setStartedAt(new \DateTime());
        $expectAppointment->setPresentationVersionId('test-presentation-version-id');
        $expectAppointment->setVideoAudioSettings(AppointmentDefinition::BOTH_VIDEO_AUDIO);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($expectCriteria),
                static::equalTo($context)
            )
            ->willReturn(
                new EntitySearchResult(
                    AppointmentEntity::class,
                    1,
                    new AppointmentCollection([$expectAppointment]),
                    null,
                    $expectCriteria,
                    $context
                )
            );

        $systemConfigService = $this->createSystemConfigServiceMock(true, true);

        $interactionService = $this->createMock(InteractionService::class);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $productViewedInteraction = new Interaction(
            InteractionName::PRODUCT_VIEWED,
            new \DateTimeImmutable(),
            0,
            $attendee,
            new ProductPayload('test-product-id')
        );
        $interactionService->expects(static::once())
            ->method('getInteractions')
            ->with(
                static::equalTo([InteractionName::PRODUCT_VIEWED]),
                static::equalTo($context),
                static::equalTo(null),
                static::equalTo($appointmentId),
                static::equalTo([]),
                static::equalTo(['payload']),
                static::equalTo(AttendeeDefinition::TYPE_GUIDE)
            )
            ->willReturn([$productViewedInteraction]);

        $cmsPageCreatorService = $this->createMock(CmsPageCreatorService::class);
        $cmsPageCreatorService->expects(static::once())
            ->method('getPositionForLastSlide')
            ->with(
                static::equalTo($expectAppointment->getPresentationId()),
                static::equalTo($expectAppointment->getPresentationVersionId()),
                static::equalTo($context)
            )
            ->willReturn(1);
        $cmsPageCreatorService->expects(static::once())
            ->method('createPresentationPage')
            ->with(
                static::equalTo('test-presentation-ended-page-id'),
                static::equalTo(['test-product-id']),
                static::equalTo($expectAppointment->getPresentationId()),
                static::equalTo($expectAppointment->getPresentationVersionId()),
                static::equalTo(2),
                static::equalTo(false),
                static::equalTo('THE END'),
                static::equalTo($context),
            );

        $appointmentRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::callback(
                    /** @param array<int, array<string, mixed>> $upsertData */
                    function (array $upsertData) use ($expectAppointment) {
                        $diffEndedTime = \date_diff($upsertData[0]['endedAt'], new \DateTime());
                        return $upsertData[0]['id'] === $expectAppointment->getId()
                            && $upsertData[0]['mode'] === AppointmentDefinition::MODE_SELF
                            && $upsertData[0]['videoAudioSettings'] === AppointmentDefinition::NONE_VIDEO_AUDIO
                            && $diffEndedTime->format('%i') <= 5;

                }),
                static::equalTo($context)
            );

        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationStateService = $this->createMock(PresentationStateService::class);
        $stateForAll = $this->createMock(StateForAll::class);
        $stateForAll->expects(static::once())
            ->method('setEndedAt')
            ->with(
                static::callback(function (\DateTime $endedAt) {
                    $diffEndedTime = \date_diff($endedAt, new \DateTime());
                    return $diffEndedTime->format('%i') <= 5;
                })
            );
        $stateForAll->expects(static::once())->method('setVideoAudioSettings')->with(static::equalTo(AppointmentDefinition::NONE_VIDEO_AUDIO));
        $stateForAll->expects(static::once())->method('setAppointmentMode')->with(static::equalTo(AppointmentDefinition::MODE_SELF));
        $stateForAll->expects(static::once())->method('setDynamicPageClosed');
        $presentationStateService->expects(static::once())
            ->method('getStateForAll')
            ->willReturn($stateForAll);
        $presentationStateService->expects(static::once())->method('publishStateForAll');
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->with(
                static::equalTo($expectAppointment->getId()),
                static::equalTo($context)
            )
            ->willReturn($presentationStateService);

        $service = $this->getAppointmentService(
            $systemConfigService,
            $appointmentRepository,
            $presentationStateServiceFactory,
            $videoManager,
            $cmsPageCreatorService,
            $interactionService
        );

        $service->endPresentation($appointmentId, $context);
    }

    public static function getTestEndPresentationSuccessfullyProviderData(): \Generator
    {
        $self = new AppointmentServiceTest('test');
        $appointmentMockHelper = new AppointMentMockHelper();
        $context = new Context(new SystemSource());

        $appointmentWithoutVideoChat = $appointmentMockHelper->getAppointEntity();
        $appointmentWithoutVideoChat->setVideoChat(null);
        yield 'appointment-without-video-chat' => [
            $appointmentWithoutVideoChat,
            $context,
            $self->createVideoManagerWhenEndPresentationMock(false, false)
        ];

        $appointmentWithVideoChatHasName = $appointmentMockHelper->getAppointEntity();
        yield 'appointment-with-video-chat-has-name' => [
            $appointmentWithVideoChatHasName,
            $context,
            $self->createVideoManagerWhenEndPresentationMock(false, true, $appointmentWithVideoChatHasName, $context)
        ];

        $appointmentWithVideoChatDoNotHasName = $appointmentMockHelper->getAppointEntity(true);
        yield 'appointment-with-video-chat-do-not-has-name' => [
            $appointmentWithVideoChatDoNotHasName,
            $context,
            $self->createVideoManagerWhenEndPresentationMock(true, false, $appointmentWithVideoChatDoNotHasName, $context)
        ];
    }

    /**
     * @dataProvider getTestGetAppointmentInvitationMailTemplateIdProviderData
     */
    public function testGetAppointmentInvitationMailTemplateId(
        string $invitationStatus
    ): void
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::once())
            ->method('getString')
            ->with(static::equalTo(AppointmentDefinition::INVITATION_MAIL_TEMPLATE_TYPE[$invitationStatus]))
            ->willReturn('test-mail-template-id');
        $service = $this->getAppointmentService($systemConfigService);
        static::assertEquals('test-mail-template-id', $service->getAppointmentInvitationMailTemplateId($invitationStatus));
    }

    public static function getTestGetAppointmentInvitationMailTemplateIdProviderData(): \Generator
    {
        yield [AppointmentDefinition::INVITE_ACTION];
        yield [AppointmentDefinition::INVITE_ALL_ACTION];
        yield [AppointmentDefinition::CANCEL_ACTION];
    }

    private function getAppointmentService(
        ?MockObject $systemConfigServiceMock = null,
        ?MockObject $appointmentRepositoryMock = null,
        ?MockObject $presentationStateServiceFactoryMock = null,
        ?MockObject $videoRoomManagerMock = null,
        ?MockObject $cmsPageCreatorServiceMock = null,
        ?MockObject $interactionServiceMock = null,
        ?MockObject $presentationVersionHandlerMock = null,
        ?MockObject $attendeeRepositoryMock = null
    ): AppointmentService
    {
        /** @var MockObject&SystemConfigService $systemConfigService */
        $systemConfigService = $systemConfigServiceMock ?? $this->createMock(SystemConfigService::class);
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?? $this->createMock(EntityRepository::class);
        /** @var MockObject&PresentationStateServiceFactory $presentationStateServiceFactory */
        $presentationStateServiceFactory = $presentationStateServiceFactoryMock ?? $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&VideoRoomManager $videoRoomManager */
        $videoRoomManager = $videoRoomManagerMock ?? $this->createMock(VideoRoomManager::class);
        /** @var MockObject&CmsPageCreatorService $cmsPageCreatorService */
        $cmsPageCreatorService = $cmsPageCreatorServiceMock ?? $this->createMock(CmsPageCreatorService::class);
        /** @var MockObject&InteractionService $interactionService */
        $interactionService = $interactionServiceMock ?? $this->createMock(InteractionService::class);
        /** @var MockObject&PresentationVersionHandler $presentationVersionHandler */
        $presentationVersionHandler = $presentationVersionHandlerMock ?? $this->createMock(PresentationVersionHandler::class);
        /** @var MockObject&EntityRepository<AttendeeCollection> $attendeeRepository */
        $attendeeRepository = $attendeeRepositoryMock ?? $this->createMock(EntityRepository::class);

        return new AppointmentService(
            $systemConfigService,
            $appointmentRepository,
            $presentationStateServiceFactory,
            $videoRoomManager,
            $cmsPageCreatorService,
            $interactionService,
            $presentationVersionHandler,
            $attendeeRepository
        );
    }

    private function createTestAppointment(
        bool $hasPresentationVersionId,
        bool $isGuideMode
    ): AppointmentEntity
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setPresentationId('test-presentation-id');
        $appointment->setPresentationVersionId($hasPresentationVersionId ? 'test-presentation-version-id' : null);
        $appointment->setMode($isGuideMode ? AppointmentDefinition::MODE_GUIDED : AppointmentDefinition::MODE_SELF);

        return $appointment;
    }

    private function createSystemConfigServiceMock(bool $isRun, ?bool $isValid = false): MockObject
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        if ($isRun) {
            $systemConfigService->expects(static::once())
                ->method('getString')
                ->with(static::equalTo(ConfigKeys::PRESENTATION_ENDED_PAGE_ID))
                ->willReturn($isValid ? 'test-presentation-ended-page-id' : '');
        } else {
            $systemConfigService->expects(static::never())->method('getString');
        }

        return $systemConfigService;
    }

    private function createVideoManagerWhenStartPresentationMock(
        bool $isRun,
        ?AppointmentEntity $expectAppointment = null,
        ?Context $context = null
    ): MockObject
    {
        $videoManager = $this->createMock(VideoRoomManager::class);

        if ($isRun && $expectAppointment && $context) {
            $expectRoomData = new VideoChatCreateStruct(
                'test-room-url',
                'test-room-name',
                'test-user-token',
                'test-owner-token',
                true
            );
            /** @var VideoChatEntity $expectVideoChat */
            $expectVideoChat = $expectAppointment->getVideoChat();
            $videoManager->expects(static::once())
                ->method('createRoom')
                ->with(
                    static::equalTo($expectVideoChat->isStartAsBroadcast())
                )
                ->willReturn($expectRoomData);
            $videoManager->expects(static::once())
                ->method('saveVideoChatData')
                ->with(
                    static::equalTo($expectVideoChat->getId()),
                    static::equalTo($expectRoomData),
                    static::equalTo($context)
                );
        } else {
            $videoManager->expects(static::never())->method('createRoom');
            $videoManager->expects(static::never())->method('saveVideoChatData');
        }

        return $videoManager;
    }

    private function createVideoManagerWhenEndPresentationMock(
        bool $removeRoomFromAppointment,
        bool $deleteRoom,
        ?AppointmentEntity $expectAppointment = null,
        ?Context $context = null
    ): MockObject
    {
        $videoManager = $this->createMock(VideoRoomManager::class);

        if ($removeRoomFromAppointment && $expectAppointment && $expectAppointment->getVideoChat() && $context) {
            $videoManager->expects(static::once())
                ->method('removeRoomFromAppointment')
                ->with(
                    static::equalTo($expectAppointment->getVideoChat()->getId()),
                    static::equalTo($context)
                );
        } else {
            $videoManager->expects(static::never())->method('removeRoomFromAppointment');
        }

        if ($deleteRoom && $expectAppointment && $expectAppointment->getVideoChat() && $context) {
            $videoManager->expects(static::once())
                ->method('deleteRoom')
                ->with(
                    static::equalTo($expectAppointment->getVideoChat()->getName()),
                    static::equalTo($context)
                );
        } else {
            $videoManager->expects(static::never())->method('deleteRoom');
        }

        return $videoManager;
    }

    private function createStateForAllWhenStartPresentationMock(
        AppointmentEntity $expectAppointment,
        bool $roomDataIsValid
    ): MockObject
    {
        // State for all
        $stateForAll = $this->createMock(StateForAll::class);
        $stateForAll->expects(static::once())->method('setAppointmentMode')->with(static::equalTo($expectAppointment->getMode()));
        $stateForAll->expects(static::once())->method('setAttendeeRestrictionType')->with(static::equalTo($expectAppointment->getAttendeeRestrictionType()));
        $stateForAll->expects(static::once())
            ->method('setStartedAt')
            ->with(
                static::callback(function (\DateTime $startedAt) {
                    $diffStartedTime = \date_diff($startedAt, new \DateTime());
                    return $diffStartedTime->format('%i') <= 5;
                })
            );
        $stateForAll->expects(static::once())->method('setAccessibleFrom')->with(static::equalTo($expectAppointment->getAccessibleFrom()));
        $stateForAll->expects(static::once())->method('setAccessibleTo')->with(static::equalTo($expectAppointment->getAccessibleTo()));
        $stateForAll->expects(static::once())->method('setVideoAudioSettings')->with(static::equalTo($expectAppointment->getVideoAudioSettings()));

        if ($roomDataIsValid) {
            // expect values get from VideoChatCreateStruct of createVideoManagerWhenStartPresentationMock
            $stateForAll->expects(static::once())->method('setVideoRoomUrl')->with(static::equalTo('test-room-url'));
            $stateForAll->expects(static::once())->method('setBroadcastMode')->with(static::equalTo(true));
        } else {
            $stateForAll->expects(static::never())->method('setVideoRoomUrl');
            $stateForAll->expects(static::never())->method('setBroadcastMode');
        }

        return $stateForAll;
    }

    private function createStateForGuidesWhenStartPresentationMock(
        bool $roomDataIsValid
    ): MockObject
    {
        $stateForGuides = $this->createMock(StateForGuides::class);
        $stateForGuides->expects(static::once())
            ->method('getAttendeeIdsOfClients')
            ->willReturn(['test-attendee-id', 'test-attendee-id-2']);

        $stateForGuides->expects(static::once())
            ->method('getAttendeeIdsOfGuides')
            ->willReturn(['test-guide-id']);

        if ($roomDataIsValid) {
            // expect values get from VideoChatCreateStruct of createVideoManagerWhenStartPresentationMock
            $stateForGuides->expects(static::once())
                ->method('setVideoGuideToken')
                ->with(static::equalTo('test-owner-token'));
        } else {
            $stateForGuides->expects(static::never())->method('setVideoGuideToken');
        }

        return $stateForGuides;
    }

    private function createStateForClientWhenStartPresentationMock(
        bool $roomDataIsValid
    ): MockObject
    {
        $stateForClients = $this->createMock(StateForClients::class);

        if ($roomDataIsValid) {
            // expect values get from VideoChatCreateStruct of createVideoManagerWhenStartPresentationMock
            $stateForClients->expects(static::once())->method('setVideoClientToken')->with(static::equalTo('test-user-token'));
        } else {
            $stateForClients->expects(static::never())->method('setVideoClientToken');
        }

        return $stateForClients;
    }
}
