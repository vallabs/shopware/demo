<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionCollection;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeProductCollectionAliasNotFoundException;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeProductCollectionService;

/**
 * @internal
 */
class AttendeeProductCollectionServiceTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testRemoveButCanNotFindListEntry(): void
    {
        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollectionRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeProductCollectionEntity::class,
                0,
                new AttendeeProductCollectionCollection(),
                null,
                new Criteria(),
                $this->context
            ));
        $attendeeProductCollectionRepository->expects(static::never())->method('delete');

        $service = $this->getService($attendeeProductCollectionRepository);
        $service->remove('test-alias', 'test-attendee-id', 'test-product-id', $this->context);
    }

    public function testRemoveWithListEntry(): void
    {
        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $attendeeProductCollection->setId('test-attendee-product-collection-id');
        $attendeeProductCollection->setAttendeeId('test-attendee-id');
        $attendeeProductCollectionRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeProductCollectionEntity::class,
                1,
                new AttendeeProductCollectionCollection([$attendeeProductCollection]),
                null,
                new Criteria(),
                $this->context
            ));
        $attendeeProductCollectionRepository->expects(static::once())
            ->method('delete')
            ->with(
                static::equalTo([['id' => $attendeeProductCollection->getId()]]),
                static::equalTo($this->context)
            );

        $service = $this->getService($attendeeProductCollectionRepository);
        $service->remove('test-alias', 'test-attendee-id', 'test-product-id', $this->context);
    }

    public function testGetProductsWithInvalidAlias(): void
    {
        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollectionRepository->expects(static::never())->method('search');

        $service = $this->getService($attendeeProductCollectionRepository);
        static::expectException(AttendeeProductCollectionAliasNotFoundException::class);
        $service->getProducts('test-invalid-alias', 'test-attendee-id', $this->context, 'test-sales-channel-id');
    }

    /**
     * @dataProvider getTestGetProductsWithDifferentAliasesProviderData
     *
     * @param array<int, AttendeeProductCollectionEntity>|null $attendeeProductCollections
     * @param array<string, array<int, string>> $expectCollection
     * @return void
     */
    public function testGetProductsWithDifferentAliases(
        ?string $alias,
        ?array $attendeeProductCollections,
        array $expectCollection
    ): void
    {
        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollectionRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeProductCollectionEntity::class,
                $attendeeProductCollections ? \count($attendeeProductCollections) : 0,
                new AttendeeProductCollectionCollection($attendeeProductCollections ?: []),
                null,
                new Criteria(),
                $this->context
            ));

        $service = $this->getService($attendeeProductCollectionRepository);
        $products = $service->getProducts($alias, 'test-attendee-id', $this->context, 'test-sales-channel-id');
        static::assertEquals($expectCollection, $products->getCollection());
    }

    public static function getTestGetProductsWithDifferentAliasesProviderData(): \Generator
    {
        yield 'no alias and no attendee product collection' => [
            null,
            null,
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [],
                AttendeeProductCollectionService::ALIAS_DISLIKED => []
            ]
        ];

        $likedAttendeeProductCollection = new AttendeeProductCollectionEntity();
        $likedAttendeeProductCollection->setId('test-like-attendee-product-collection-id');
        $likedAttendeeProductCollection->setAlias(AttendeeProductCollectionService::ALIAS_LIKED);
        $likedAttendeeProductCollection->setProductId('test-like-product-id');
        yield 'no alias and has liked attendee product collection' => [
            null,
            [$likedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [$likedAttendeeProductCollection->getProductId()],
                AttendeeProductCollectionService::ALIAS_DISLIKED => []
            ]
        ];

        $dislikedAttendeeProductCollection = new AttendeeProductCollectionEntity();
        $dislikedAttendeeProductCollection->setId('test-dislike-attendee-product-collection-id');
        $dislikedAttendeeProductCollection->setAlias(AttendeeProductCollectionService::ALIAS_DISLIKED);
        $dislikedAttendeeProductCollection->setProductId('test-dislike-product-id');
        yield 'no alias and has disliked attendee product collection' => [
            null,
            [$dislikedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [],
                AttendeeProductCollectionService::ALIAS_DISLIKED => [$dislikedAttendeeProductCollection->getProductId()]
            ]
        ];

        yield 'no alias and has both' => [
            null,
            [$likedAttendeeProductCollection, $dislikedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [$likedAttendeeProductCollection->getProductId()],
                AttendeeProductCollectionService::ALIAS_DISLIKED => [$dislikedAttendeeProductCollection->getProductId()]
            ]
        ];

        yield 'liked alias and no attendee product collection' => [
            AttendeeProductCollectionService::ALIAS_LIKED,
            null,
            [AttendeeProductCollectionService::ALIAS_LIKED => []]
        ];

        yield 'liked alias and has liked attendee product collection' => [
            AttendeeProductCollectionService::ALIAS_LIKED,
            [$likedAttendeeProductCollection],
            [AttendeeProductCollectionService::ALIAS_LIKED => [$likedAttendeeProductCollection->getProductId()]]
        ];

        yield 'liked alias and has disliked attendee product collection' => [
            AttendeeProductCollectionService::ALIAS_LIKED,
            [$dislikedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [],
                AttendeeProductCollectionService::ALIAS_DISLIKED => [$dislikedAttendeeProductCollection->getProductId()]
            ]
        ];

        yield 'liked alias and has both' => [
            AttendeeProductCollectionService::ALIAS_LIKED,
            [$likedAttendeeProductCollection, $dislikedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [$likedAttendeeProductCollection->getProductId()],
                AttendeeProductCollectionService::ALIAS_DISLIKED => [$dislikedAttendeeProductCollection->getProductId()]
            ]
        ];

        yield 'disliked alias and no attendee product collection' => [
            AttendeeProductCollectionService::ALIAS_DISLIKED,
            null,
            [AttendeeProductCollectionService::ALIAS_DISLIKED => []]
        ];

        yield 'disliked alias and has liked attendee product collection' => [
            AttendeeProductCollectionService::ALIAS_DISLIKED,
            [$likedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [$likedAttendeeProductCollection->getProductId()],
                AttendeeProductCollectionService::ALIAS_DISLIKED => []
            ]
        ];

        yield 'disliked alias and has disliked attendee product collection' => [
            AttendeeProductCollectionService::ALIAS_DISLIKED,
            [$dislikedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_DISLIKED => [$dislikedAttendeeProductCollection->getProductId()]
            ]
        ];

        yield 'disliked alias and has both' => [
            AttendeeProductCollectionService::ALIAS_DISLIKED,
            [$likedAttendeeProductCollection, $dislikedAttendeeProductCollection],
            [
                AttendeeProductCollectionService::ALIAS_LIKED => [$likedAttendeeProductCollection->getProductId()],
                AttendeeProductCollectionService::ALIAS_DISLIKED => [$dislikedAttendeeProductCollection->getProductId()]
            ]
        ];
    }

    public function testAddProductWithInvalidAlias(): void
    {
        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollectionRepository->expects(static::never())->method('search');
        $attendeeProductCollectionRepository->expects(static::never())->method('upsert');

        $productRepository = $this->createMock(EntityRepository::class);
        $productRepository->expects(static::never())->method('searchIds');

        $service = $this->getService($attendeeProductCollectionRepository, $productRepository);
        static::expectException(AttendeeProductCollectionAliasNotFoundException::class);
        $service->addProduct('test-invalid-alias', 'test-attendee-id', 'test-product-id', $this->context);
    }

    public function testAddProductButCanNotFindProduct(): void
    {
        $productRepository = $this->createMock(EntityRepository::class);
        $productRepository->expects(static::once())
            ->method('searchIds')
            ->willReturn(new IdSearchResult(
                0,
                [],
                new Criteria(),
                $this->context
            ));

        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollectionRepository->expects(static::never())->method('search');
        $attendeeProductCollectionRepository->expects(static::never())->method('upsert');

        $service = $this->getService($attendeeProductCollectionRepository, $productRepository);
        static::expectException(EntityNotFoundException::class);
        $service->addProduct(AttendeeProductCollectionService::ALIAS_LIKED, 'test-attendee-id', 'test-product-id', $this->context);
    }

    /**
     * @dataProvider getTestAddProductProviderData
     *
     * @param array<string, string> $expectData
     */
    public function testAddProduct(
        string $alias,
        ?AttendeeProductCollectionEntity $attendeeProductCollection,
        array $expectData
    ): void
    {
        $productRepository = $this->createMock(EntityRepository::class);
        $productRepository->expects(static::once())
            ->method('searchIds')
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['test-product-id']]],
                new Criteria(),
                $this->context
            ));

        $attendeeProductCollectionRepository = $this->createMock(EntityRepository::class);
        $attendeeProductCollectionRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeProductCollectionEntity::class,
                $attendeeProductCollection ? 1 : 0,
                new AttendeeProductCollectionCollection($attendeeProductCollection ? [$attendeeProductCollection] : []),
                null,
                new Criteria(),
                $this->context
            ));

        $expectData = \array_merge(['alias' => $alias], $expectData);
        $attendeeProductCollectionRepository->expects(static::once())
            ->method('upsert')
            ->with(
                static::equalTo([$expectData]),
                static::equalTo($this->context)
            );

        $service = $this->getService($attendeeProductCollectionRepository, $productRepository);
        $service->addProduct($alias, 'test-attendee-id', 'test-product-id', $this->context);
    }

    public static function getTestAddProductProviderData(): \Generator
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $attendeeProductCollection->setId('test-attendee-product-collection-id');

        $expectDataIfCanNotFindListEntry = [
            'productId' => 'test-product-id',
            'attendeeId' => 'test-attendee-id',
            'productVersionId' => Defaults::LIVE_VERSION
        ];
        yield 'liked alias and no list entry' => [
            AttendeeProductCollectionService::ALIAS_LIKED,
            null,
            $expectDataIfCanNotFindListEntry
        ];
        yield 'disliked alias and no list entry' => [
            AttendeeProductCollectionService::ALIAS_DISLIKED,
            null,
            $expectDataIfCanNotFindListEntry
        ];

        $expectDataIfCanListEntry = [
            'id' => $attendeeProductCollection->getId(),
            'productVersionId' => Defaults::LIVE_VERSION
        ];
        yield 'liked alias and has list entry' => [
            AttendeeProductCollectionService::ALIAS_LIKED,
            $attendeeProductCollection,
            $expectDataIfCanListEntry
        ];
        yield 'disliked alias and has list entry' => [
            AttendeeProductCollectionService::ALIAS_DISLIKED,
            $attendeeProductCollection,
            $expectDataIfCanListEntry
        ];
    }

    private function getService(
        ?MockObject $attendeeProductCollectionRepositoryMock = null,
        ?MockObject $productRepositoryMock = null
    ): AttendeeProductCollectionService
    {
        /** @var MockObject&EntityRepository<AttendeeProductCollectionCollection> $attendeeProductCollectionRepository */
        $attendeeProductCollectionRepository = $attendeeProductCollectionRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<ProductCollection> $productRepository */
        $productRepository = $productRepositoryMock ?: $this->createMock(EntityRepository::class);

        return new AttendeeProductCollectionService(
            $attendeeProductCollectionRepository,
            $productRepository
        );
    }
}
