<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\AdminSalesChannelApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\User\UserCollection;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Content\Appointment\Service\ContextDataHelper;
use SwagGuidedShopping\Framework\Routing\AttendeeNotFoundException;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class ContextDataHelperTest extends TestCase
{
    /**
     * @dataProvider getTestGetAttendeeNameFromContextProviderData
     */
   public function testGetAttendeeNameFromContext(
       SalesChannelContext $salesChannelContext,
       MockObject $userRepository,
       ?string $expectAttendeeName
   ): void
   {
       $helper = $this->getHelper(null, $userRepository);
       $attendeeName = $helper->getAttendeeNameFromContext($salesChannelContext);
       static::assertEquals($expectAttendeeName, $attendeeName);
   }

   public static function getTestGetAttendeeNameFromContextProviderData(): \Generator
   {
       $self = new ContextDataHelperTest('test');
       $salesChannelContextHelper = new SalesChannelContextHelper();

       // don't have userId and customerId
       $invalidSalesChannelContext = $salesChannelContextHelper->createSalesChannelContext();
       yield 'invalid-sales-channel-context' => [
           $invalidSalesChannelContext,
           $self->createUserRepositoryMock(false),
           null
       ];

       $adminApiSourceHasUserId = new AdminApiSource('test-user-id');
       $adminScSource = new AdminSalesChannelApiSource('test-sales-channel-id', new Context($adminApiSourceHasUserId));
       $adminApiSalesChannelContextHasUserId = $salesChannelContextHelper->createSalesChannelContext(null, null, $adminScSource);
       yield 'admin-api-sales-channel-context-has-user-id' => [
           $adminApiSalesChannelContextHasUserId,
           $self->createUserRepositoryMock(true, 'test-user-id', $adminApiSalesChannelContextHasUserId),
           'test-user-first-name test-user-last-name'
       ];

       $adminApiSourceDoNotHasUserId = new AdminApiSource(null);
       $adminScSource = new AdminSalesChannelApiSource('test-sales-channel-id', new Context($adminApiSourceDoNotHasUserId));
       $adminApiSalesChannelContextDoNotHasUserId = $salesChannelContextHelper->createSalesChannelContext(null, null, $adminScSource);
       yield 'admin-api-sales-channel-context-do-not-has-user-id' => [
           $adminApiSalesChannelContextDoNotHasUserId,
           $self->createUserRepositoryMock(false),
           null
       ];

       $customer = new CustomerEntity();
       $customer->setId('test-customer-id');
       $customer->setFirstName('test-customer-first-name');
       $customer->setLastName('test-customer-last-name');
       $salesChannelContextHasCustomer = $salesChannelContextHelper->createSalesChannelContext(null, $customer);
       yield 'sales-channel-context-has-customer' => [
           $salesChannelContextHasCustomer,
           $self->createUserRepositoryMock(false),
           'test-customer-first-name test-customer-last-name'
       ];
   }

    /**
     * @dataProvider getTestGetCustomerIdFromContext
     */
   public function testGetCustomerIdFromContext(
       SalesChannelContext $salesChannelContext,
       ?string $expectCustomerId
   ): void
   {
       $helper = $this->getHelper();
       $customerId = $helper->getCustomerIdFromContext($salesChannelContext);
       static::assertEquals($expectCustomerId, $customerId);
   }

   public static function getTestGetCustomerIdFromContext(): \Generator
   {
       $salesChannelContextHelper = new SalesChannelContextHelper();

       $customer = new CustomerEntity();
       $customer->setId('test-customer-id');
       $customer->setFirstName('test-customer-first-name');
       $customer->setLastName('test-customer-last-name');
       $salesChannelContextHasCustomer = $salesChannelContextHelper->createSalesChannelContext(null, $customer);
       yield 'has-customer' => [
           $salesChannelContextHasCustomer,
           'test-customer-id'
       ];

       $salesChannelContextDoNotHasCustomer = $salesChannelContextHelper->createSalesChannelContext();
       yield 'do-not-has-customer' => [
           $salesChannelContextDoNotHasCustomer,
           null
       ];
   }

    /**
     * @dataProvider getTestGetUserIdFromContextProviderData
     */
   public function testGetUserIdFromContext(
       SalesChannelContext $salesChannelContext,
       ?string $expectUserId
   ): void
   {
        $helper = $this->getHelper();
        $userId = $helper->getUserIdFromContext($salesChannelContext);
        static::assertEquals($expectUserId, $userId);
   }

   public static function getTestGetUserIdFromContextProviderData(): \Generator
   {
       $salesChannelContextHelper = new SalesChannelContextHelper();

       $adminApiSourceHasUserId = new AdminApiSource('test-user-id');
       $adminScSource = new AdminSalesChannelApiSource('test-sales-channel-id', new Context($adminApiSourceHasUserId));
       $adminApiSalesChannelContextHasUserId = $salesChannelContextHelper->createSalesChannelContext(null, null, $adminScSource);
       yield 'valid-sales-channel-context' => [
           $adminApiSalesChannelContextHasUserId,
           'test-user-id'
       ];

       $invalidSalesChannelContext = $salesChannelContextHelper->createSalesChannelContext();
       yield 'invalid-sales-channel-context' => [
           $invalidSalesChannelContext,
           null
       ];
   }

    /**
     * @dataProvider getTestGetAttendeeFromContextProviderData
     */
   public function testGetAttendeeFromContext(
       SalesChannelContext $salesChannelContext,
       MockObject $contextResolver,
       ?AttendeeEntity $expectAttendee
   ): void
   {
       $helper = $this->getHelper(null, null, $contextResolver);
       $attendee = $helper->getAttendeeFromContext($salesChannelContext);
       static::assertSame($expectAttendee, $attendee);
   }

   public static function getTestGetAttendeeFromContextProviderData(): \Generator
   {
       $self = new ContextDataHelperTest('test');

       $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
       $attendee = new AttendeeEntity();
       $attendee->setId('test-attendee-id');

       yield 'found-attendee' => [
           $salesChannelContext,
           $self->createContextResolverMock(true, $attendee, $salesChannelContext),
           $attendee
       ];

       yield 'not-found-attendee' => [
           $salesChannelContext,
           $self->createContextResolverMock(false, null, $salesChannelContext),
           null
       ];
   }

    /**
     * @dataProvider getTestGetAttendeeFromCredentialsProviderData
     */
   public function testGetAttendeeFromCredentials(
       string $attendeeType,
       string $appointmentId,
       SalesChannelContext $salesChannelContext,
       MockObject $attendeeService,
       ?AttendeeEntity $expectAttendee
   ): void
   {
        $service = $this->getHelper($attendeeService);
        $attendee = $service->getAttendeeFromCredentials($salesChannelContext, $appointmentId, $attendeeType);
        static::assertSame($expectAttendee, $attendee);
   }

   public static function getTestGetAttendeeFromCredentialsProviderData(): \Generator
   {
       $self = new ContextDataHelperTest('test');
       $appointmentId = 'test-appointment-id';
       $salesChannelContextHelper = new SalesChannelContextHelper();

       $salesChannelContextDoNotHasCustomerId = $salesChannelContextHelper->createSalesChannelContext();

       yield 'client-type-but-empty-customer-id' => [
           AttendeeDefinition::TYPE_CLIENT,
           $appointmentId,
           $salesChannelContextDoNotHasCustomerId,
           $self->createAttendeeServiceMock('run-nothing'),
           null
       ];

       $salesChannelContextDoNotHasUserId = $salesChannelContextHelper->createSalesChannelContext();

       yield 'guide-type-but-empty-user-id' => [
           AttendeeDefinition::TYPE_GUIDE,
           $appointmentId,
           $salesChannelContextDoNotHasUserId,
           $self->createAttendeeServiceMock('run-nothing'),
           null
       ];

       $customer = new CustomerEntity();
       $customer->setId('test-customer-id');
       $salesChannelContextHasCustomerId = $salesChannelContextHelper->createSalesChannelContext(null, $customer);
       $attendeeIsCustomer = $self->createTestAttendee('test-customer-id', null, $appointmentId);

       $adminApiSourceHasUserId = new AdminApiSource('test-user-id');
       $adminScSource = new AdminSalesChannelApiSource('test-sales-channel-id', new Context($adminApiSourceHasUserId));
       $salesChannelContextHasUserId = $salesChannelContextHelper->createSalesChannelContext(null, null, $adminScSource);
       $attendeeIsUser = $self->createTestAttendee(null, 'test-user-id', $appointmentId);

       yield 'client-type-and-has-customer-id' => [
           AttendeeDefinition::TYPE_CLIENT,
           $appointmentId,
           $salesChannelContextHasCustomerId,
           $self->createAttendeeServiceMock(
               'only-find-by-customer',
               'test-customer-id',
               null,
               $appointmentId,
               $attendeeIsCustomer,
               $salesChannelContextHasCustomerId
           ),
           $attendeeIsCustomer
       ];

       yield 'guide-type-and-has-user-id' => [
           AttendeeDefinition::TYPE_GUIDE,
           $appointmentId,
           $salesChannelContextHasUserId,
           $self->createAttendeeServiceMock(
               'only-find-by-user',
               null,
               'test-user-id',
               $appointmentId,
               $attendeeIsUser,
               $salesChannelContextHasUserId
           ),
           $attendeeIsUser
       ];

       yield 'invalid-type' => [
           'invalid-type',
           $appointmentId,
           $salesChannelContextHelper->createSalesChannelContext(),
           $self->createAttendeeServiceMock('run-nothing'),
           null
       ];
   }

   private function getHelper(
       ?MockObject $attendeeServiceMock = null,
       ?MockObject $userRepositoryMock = null,
       ?MockObject $contextResolverMock = null
   ): ContextDataHelper
   {
       /** @var MockObject&AttendeeService $attendeeService */
       $attendeeService = $attendeeServiceMock ?? $this->createMock(AttendeeService::class);
       /** @var MockObject&EntityRepository<UserCollection> $userRepository */
       $userRepository = $userRepositoryMock ?? $this->createMock(EntityRepository::class);
       /** @var MockObject&GuidedShoppingRequestContextResolver $contextResolver */
       $contextResolver = $contextResolverMock ?? $this->createMock(GuidedShoppingRequestContextResolver::class);

       return new ContextDataHelper($attendeeService, $userRepository, $contextResolver);
   }

   private function createUserRepositoryMock(
       bool $isSearch,
       ?string $userId = null,
       ?SalesChannelContext $salesChannelContext = null
   ): MockObject
   {
       $userRepository = $this->createMock(EntityRepository::class);
       if ($isSearch && $salesChannelContext && $userId) {
           $user = new UserEntity();
           $user->setId($userId);
           $user->setFirstName('test-user-first-name');
           $user->setLastName('test-user-last-name');

           $criteria = new Criteria([$userId]);
           $userRepository->expects(static::once())
               ->method('search')
               ->with(
                   static::equalTo($criteria),
                   static::equalTo($salesChannelContext->getContext())
               )
               ->willReturn(
                   new EntitySearchResult(
                       UserEntity::class,
                       1,
                       new UserCollection([$user]),
                       null,
                       $criteria,
                       $salesChannelContext->getContext()
                   )
               );
       } else {
           $userRepository->expects(static::never())->method('search');
       }

       return $userRepository;
   }

   private function createContextResolverMock(bool $foundAttendee, ?AttendeeEntity $attendee, SalesChannelContext $salesChannelContext): MockObject
   {
       $contextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);

       if ($foundAttendee) {
           $contextResolver->expects(static::once())
               ->method('loadAttendee')
               ->with(
                   static::equalTo($salesChannelContext)
               )
               ->willReturn($attendee);
       } else {
           $exception = new AttendeeNotFoundException('test-attendee-id');
           $contextResolver->expects(static::once())
               ->method('loadAttendee')
               ->with(
                   static::equalTo($salesChannelContext)
               )
               ->willThrowException($exception);
       }

       return $contextResolver;
   }

   private function createAttendeeServiceMock(
       string $case,
       ?string $customerId = null,
       ?string $userId = null,
       ?string $appointmentId = null,
       ?AttendeeEntity $attendee = null,
       ?SalesChannelContext $salesChannelContext = null
   ): AttendeeService
   {
       $attendeeService = $this->createMock(AttendeeService::class);

        switch ($case) {
            case 'run-nothing':
                $attendeeService->expects(static::never())->method('findAttendeeByCustomer');
                $attendeeService->expects(static::never())->method('findAttendeeByUser');
                break;

            case 'only-find-by-customer':
                if ($salesChannelContext && $customerId && $appointmentId && $attendee) {
                    $attendeeService->expects(static::once())
                        ->method('findAttendeeByCustomer')
                        ->with(
                            static::equalTo($customerId),
                            static::equalTo($appointmentId),
                            static::equalTo($salesChannelContext->getContext())
                        )
                        ->willReturn($attendee);
                } else {
                    $attendeeService->expects(static::once())->method('findAttendeeByCustomer');
                }

                $attendeeService->expects(static::never())->method('findAttendeeByUser');
                break;

            case 'only-find-by-user':
                $attendeeService->expects(static::never())->method('findAttendeeByCustomer');
                if ($salesChannelContext && $userId && $appointmentId && $attendee) {
                    $attendeeService->expects(static::once())
                        ->method('findAttendeeByUser')
                        ->with(
                            static::equalTo($userId),
                            static::equalTo($appointmentId),
                            static::equalTo($salesChannelContext->getContext())
                        )
                        ->willReturn($attendee);
                } else {
                    $attendeeService->expects(static::once())->method('findAttendeeByUser');
                }
                break;

            default:
                // do nothing
        }

       return $attendeeService;
   }

   private function createTestAttendee(
       ?string $customerId,
       ?string $userId,
       string $appointmentId
   ): AttendeeEntity
   {
       $attendee = new AttendeeEntity();
       $attendee->setId('test-attendee-id');
       $attendee->setCustomerId($customerId);
       $attendee->setUserId($userId);
       $attendee->setAppointmentId($appointmentId);

       return $attendee;
   }
}
