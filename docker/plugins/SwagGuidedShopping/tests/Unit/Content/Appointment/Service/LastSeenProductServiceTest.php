<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Service\LastSeenProductsService;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class LastSeenProductServiceTest extends TestCase
{
    protected string $attendeeId = 'test-attendee-id';

    private AttendeeEntity $attendee;

    private SalesChannelContext $salesChannelContext;

    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();

        $this->attendee = new AttendeeEntity();
        $this->attendee->setId($this->attendeeId);
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $this->context = $this->salesChannelContext->getContext();
    }

    public function testGetProductsWithoutInteractions(): void
    {
        $interactionService = $this->createMock(InteractionService::class);
        $interactionService->expects(static::once())
            ->method('getInteractions')
            ->with(
                static::equalTo([InteractionName::PRODUCT_VIEWED, InteractionName::QUICKVIEW_OPENED]),
                static::equalTo($this->context),
                static::equalTo($this->attendeeId)
            )
            ->willReturn([]);

        $productRepository = $this->createMock(EntityRepository::class);
        $productRepository->expects(static::never())->method('search');

        $service = $this->getService($interactionService, $productRepository);
        $products = $service->getProducts($this->attendee, $this->salesChannelContext);
        static::assertCount(0, $products);
    }

    public function testGetProductsWithInteractions(): void
    {
        $interactionService = $this->createMock(InteractionService::class);
        $productViewedInteraction = new Interaction(
            InteractionName::PRODUCT_VIEWED,
            new \DateTimeImmutable(),
            0,
            $this->attendee,
            new ProductPayload('test-product-id')
        );
        $quickViewInteraction = new Interaction(
            InteractionName::QUICKVIEW_OPENED,
            new \DateTimeImmutable(),
            0,
            $this->attendee,
            new ProductPayload('test-child-product-id')
        );
        $productViewedInteraction2 = new Interaction(
            InteractionName::PRODUCT_VIEWED,
            new \DateTimeImmutable(),
            0,
            $this->attendee,
            new ProductPayload('test-product-id')
        );
        $interactionService->expects(static::once())
            ->method('getInteractions')
            ->with(
                static::equalTo([InteractionName::PRODUCT_VIEWED, InteractionName::QUICKVIEW_OPENED]),
                static::equalTo($this->context),
                static::equalTo($this->attendeeId)
            )
            ->willReturn([$productViewedInteraction, $quickViewInteraction, $productViewedInteraction2]);

        $productRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('id', ['test-product-id', 'test-child-product-id']));
        $product = new ProductEntity();
        $product->setId('test-product-id');
        $childProduct = new ProductEntity();
        $childProduct->setId('test-child-product-id');
        $childProduct->setParentId('test-product-id');
        $entitySearchResult =  new EntitySearchResult(
            ProductEntity::class,
            2,
            new ProductCollection([$product, $childProduct]),
            null,
            $criteria,
            $this->context
        );

        $productRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo($criteria),
                static::equalTo($this->context)
            )
            ->willReturn($entitySearchResult);

        $service = $this->getService($interactionService, $productRepository);
        $products = $service->getProducts($this->attendee, $this->salesChannelContext);
        static::assertCount(1, $products);
        static::assertEquals(['test-product-id'], $products);
    }

    private function getService(
        ?MockObject $interactionServiceMock = null,
        ?MockObject $productRepositoryMock = null
    ): LastSeenProductsService
    {
        /** @var MockObject&InteractionService $interactionService */
        $interactionService = $interactionServiceMock ?? $this->createMock(InteractionService::class);
        /** @var MockObject&EntityRepository<ProductCollection> $productRepository */
        $productRepository = $productRepositoryMock ?? $this->createMock(EntityRepository::class);

        return new LastSeenProductsService($interactionService, $productRepository);
    }
}
