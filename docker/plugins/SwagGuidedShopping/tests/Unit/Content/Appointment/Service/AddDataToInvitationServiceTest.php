<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerAddress\CustomerAddressEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Content\Mail\Service\AbstractMailService;
use Shopware\Core\Content\MailTemplate\MailTemplateCollection;
use Shopware\Core\Content\MailTemplate\MailTemplateEntity;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\Country\CountryEntity;
use Shopware\Core\System\User\UserCollection;
use Shopware\Core\System\User\UserDefinition;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationCouldNotSendException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationMissingStartOrEndTimeException;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;
use SwagGuidedShopping\Content\Appointment\Service\AddDataToInvitationService;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Header\Headers;
use Symfony\Component\Mime\Header\MailboxListHeader;
use Symfony\Component\Mime\Part\TextPart;

class AddDataToInvitationServiceTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testGetDecorated(): void
    {
        $mailService = $this->createMock(AbstractMailService::class);
        $service = $this->getService($mailService);
        $decorated = $service->getDecorated();
        static::assertSame($mailService, $decorated);
    }

    /**
     * @dataProvider getTestSendWithInvalidInvitationActionProviderData
     * @param array<int, mixed> $data
     */
    public function testSendWithInvalidInvitationAction(
        array $data
    ): void
    {
        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::once())
            ->method('send')
            ->with(
                static::equalTo($data),
                static::equalTo($this->context),
                static::equalTo([])
            );

        $service = $this->getService($mailService);
        $service->send($data, $this->context);
    }

    public static function getTestSendWithInvalidInvitationActionProviderData(): \Generator
    {
        yield [[]];
        yield [['invitationAction' => 'invalid-action']];
    }

    public function testSendWithValidInvitationButInvalidAppointmentId(): void
    {
        $data = [
            'invitationAction' => AppointmentDefinition::INVITE_ACTION,
            'appointmentId' => 'test-appointment-id'
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::never())->method('getAppointment');

        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::never())->method('send');

        $service = $this->getService($mailService, $appointmentService);
        static::expectException(AppointmentInvitationCouldNotSendException::class);
        static::expectExceptionMessage('Invalid appointment id.');
        $service->send($data, $this->context);
    }

    /**
     * @dataProvider getTestSendWithAppointmentWithInvalidAppointmentProviderData
     * @param class-string<\Throwable> $exception
     */
    public function testSendWithAppointmentWithInvalidAppointment(
        AppointmentEntity $appointment,
        string $exception,
        string $exceptionMessage
    ): void
    {
        $appointmentId = \md5('test-appointment-id');
        $data = [
            'invitationAction' => AppointmentDefinition::INVITE_ACTION,
            'appointmentId' => $appointmentId
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($this->context),
                static::equalTo(['guideUser', 'attendees.customer.defaultShippingAddress.country', 'salesChannelDomain.salesChannel'])
            )
            ->willReturn($appointment);

        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::never())->method('send');

        $service = $this->getService($mailService, $appointmentService);
        static::expectException($exception);
        static::expectExceptionMessage($exceptionMessage);
        $service->send($data, $this->context);
    }

    public static function getTestSendWithAppointmentWithInvalidAppointmentProviderData(): \Generator
    {
        $appointmentMockHelper = new AppointMentMockHelper();

        $appointmentNoSalesChannelDomain = $appointmentMockHelper->getAppointEntity();
        $appointmentNoSalesChannelDomain->setSalesChannelDomain(null);
        yield 'no-sales-channel-domain' => [
            $appointmentNoSalesChannelDomain,
            AppointmentInvitationCouldNotSendException::class,
            'Appointment sales channel domain is empty.'
        ];

        $appointmentNoAttendees = $appointmentMockHelper->getAppointEntity();
        $appointmentNoAttendees->setAttendees(new AttendeeCollection([]));
        yield 'no-attendees' => [
            $appointmentNoAttendees,
            AppointmentInvitationCouldNotSendException::class,
            'No attendees to send mail.'
        ];

        $appointmentNoAccessibleFrom = $appointmentMockHelper->getAppointEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $appointmentNoAccessibleFrom->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentNoAccessibleFrom->setAttendees(new AttendeeCollection([$attendee]));
        $appointmentNoAccessibleFrom->setAccessibleFrom(null);
        $appointmentNoAccessibleFrom->setAccessibleTo(new \DateTime());
        yield 'guided-appointment-no-accessible-from' => [
            $appointmentNoAccessibleFrom,
            AppointmentInvitationMissingStartOrEndTimeException::class,
            'Invitation email can not be sent because of missing start/end time.'
        ];

        $appointmentNoAccessibleTo = $appointmentMockHelper->getAppointEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $appointmentNoAccessibleTo->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentNoAccessibleTo->setAttendees(new AttendeeCollection([$attendee]));
        $appointmentNoAccessibleTo->setAccessibleFrom(new \DateTime());
        $appointmentNoAccessibleTo->setAccessibleTo(null);
        yield 'guided-appointment-no-accessible-to' => [
            $appointmentNoAccessibleTo,
            AppointmentInvitationMissingStartOrEndTimeException::class,
            'Invitation email can not be sent because of missing start/end time.'
        ];

        $appointmentNoAccessibleTimes = $appointmentMockHelper->getAppointEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $appointmentNoAccessibleTimes->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentNoAccessibleTimes->setAttendees(new AttendeeCollection([$attendee]));
        $appointmentNoAccessibleTimes->setAccessibleFrom(null);
        $appointmentNoAccessibleTimes->setAccessibleTo(null);
        yield 'guided-appointment-no-accessible-times' => [
            $appointmentNoAccessibleTimes,
            AppointmentInvitationMissingStartOrEndTimeException::class,
            'Invitation email can not be sent because of missing start/end time.'
        ];
    }

    public function testSendButNotConfigMailTemplateYet(): void
    {
        $appointmentId = \md5('test-appointment-id');
        $invitationAction = AppointmentDefinition::INVITE_ACTION;
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $appointment->setAttendees(new AttendeeCollection([$attendee]));
        $appointment->setAccessibleFrom(new \DateTime());
        $appointment->setAccessibleTo(new \DateTime());
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->with(
                static::equalTo($appointmentId),
                static::equalTo($this->context),
                static::equalTo(['guideUser', 'attendees.customer.defaultShippingAddress.country', 'salesChannelDomain.salesChannel'])
            )
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->with(static::equalTo($invitationAction))
            ->willReturn('');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplateRepository->expects(static::never())->method('search');

        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::never())->method('send');

        $service = $this->getService($mailService, $appointmentService);
        static::expectException(AppointmentInvitationCouldNotSendException::class);
        static::expectExceptionMessage('The mail template is not set in the Plugin configuration.');
        $service->send($data, $this->context);
    }

    /**
     * @dataProvider getTestSendButFailedWithInvalidMailTemplateProviderData
     * @param array<MailTemplateEntity> $mailTemplates
     */
    public function testSendButFailedWithInvalidMailTemplate(
        array $mailTemplates,
        string $exceptionMessage
    ): void
    {
        $appointmentId = \md5('test-appointment-id');
        $invitationAction = AppointmentDefinition::INVITE_ACTION;
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $appointment->setAttendees(new AttendeeCollection([$attendee]));
        $appointment->setAccessibleFrom(new \DateTime());
        $appointment->setAccessibleTo(new \DateTime());
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->with(
                static::equalTo(new Criteria(['test-mail-template-id'])),
                static::equalTo($this->context)
            )
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection($mailTemplates),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::never())->method('send');

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository);
        static::expectException(AppointmentInvitationCouldNotSendException::class);
        static::expectExceptionMessage($exceptionMessage);
        $service->send($data, $this->context);
    }

    public static function getTestSendButFailedWithInvalidMailTemplateProviderData(): \Generator
    {
        yield 'not-found-mail-template' => [
            [],
            'Mail template not found.'
        ];

        $mailTemplateNoContentHtml = new MailTemplateEntity();
        $mailTemplateNoContentHtml->setId('test-mail-template-id');
        $mailTemplateNoContentHtml->setContentHtml(null);
        yield 'mail-template-no-content-html' => [
            [$mailTemplateNoContentHtml],
            'Mail template content in html is blank.'
        ];

        $mailTemplateNoContentPlain = new MailTemplateEntity();
        $mailTemplateNoContentPlain->setId('test-mail-template-id');
        $mailTemplateNoContentPlain->setContentHtml('test-content-html');
        $mailTemplateNoContentPlain->setContentPlain(null);
        yield 'mail-template-no-content-plain' => [
            [$mailTemplateNoContentPlain],
            'Mail template content in plain text is blank.'
        ];
    }

    public function testSendInviteMailWithSelfAppointmentSuccessfullyAndUsingSystemSourceContext(): void
    {
        $invitationAction = AppointmentDefinition::INVITE_ACTION;

        $appointmentId = \md5('test-appointment-id');
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_SELF);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $expectData = \array_merge($data, [
            'senderName' => 'Test User',
            'contentHtml' => $mailTemplate->getContentHtml(),
            'contentPlain' => $mailTemplate->getContentPlain(),
            'subject' => 'Test-translated-appointment-name',
            'salesChannelId' => 'test_sales_channel_id',
            'recipients' => ['test@gmail.com' => 'test']
        ]);
        $customFields = [
            'accessibleFrom' => ['date' => '2000 01 01', 'time' => '12:54', 'timezone' => 'UTC'],
            'accessibleTo' => ['date' => '2000 01 01', 'time' => '13:00', 'timezone' => 'UTC'],
            'isSameDay' => true,
            'isGuide' => false,
            'token' => EncryptHelper::encryptString('test@gmail.com')
        ];
        $appointment->setCustomFields($customFields);
        $expectTemplateData = ['appointment' => $appointment];

        $mailService->expects(static::once())
            ->method('send')
            ->with(
                static::equalTo($expectData),
                static::equalTo($this->context),
                static::equalTo($expectTemplateData)
            )
            ->willReturn(new Email());
        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test@gmail.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger);
        $service->send($data, $this->context);
    }

    /**
     * @dataProvider getTimeZoneWithDifferentUserResult
     */
    public function testSendInviteMailWithSelfAppointmentSuccessfullyAndUsingAdminSourceApiContext(
        Context $context,
        MockObject $userRepository,
        string $expectedAccessibleFromTime,
        string $expectedAccessibleToTime,
        string $expectedTimeZone
    ): void
    {
        $invitationAction = AppointmentDefinition::INVITE_ACTION;

        $appointmentId = \md5('test-appointment-id');
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_SELF);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $expectData = \array_merge($data, [
            'senderName' => 'Test User',
            'contentHtml' => $mailTemplate->getContentHtml(),
            'contentPlain' => $mailTemplate->getContentPlain(),
            'subject' => 'Test-translated-appointment-name',
            'salesChannelId' => 'test_sales_channel_id',
            'recipients' => ['test@gmail.com' => 'test']
        ]);
        $customFields = [
            'accessibleFrom' => ['date' => '2000 01 01', 'time' => $expectedAccessibleFromTime, 'timezone' => $expectedTimeZone],
            'accessibleTo' => ['date' => '2000 01 01', 'time' => $expectedAccessibleToTime, 'timezone' => $expectedTimeZone],
            'isSameDay' => true,
            'isGuide' => false,
            'token' => EncryptHelper::encryptString('test@gmail.com')
        ];
        $appointment->setCustomFields($customFields);
        $expectTemplateData = ['appointment' => $appointment];

        $mailService->expects(static::once())
            ->method('send')
            ->with(
                static::equalTo($expectData),
                static::equalTo($context),
                static::equalTo($expectTemplateData)
            )
            ->willReturn(new Email());
        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test@gmail.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger, $userRepository);

        $service->send($data, $context);
    }

    public static function getTimeZoneWithDifferentUserResult(): \Generator
    {
        $self = new AddDataToInvitationServiceTest();

        $context = new Context(new AdminApiSource(null));
        yield 'user id in context is null' => [
            $context,
            $self->createUserRepository(false),
            '12 54',
            '13 00',
            'UTC'
        ];

        $context = new Context(new AdminApiSource('test-user-id'));

        yield 'admin user is not found' => [
            $context,
            $self->createUserRepository(true, []),
            '12 54',
            '13 00',
            'UTC'
        ];

        $user = new UserEntity();
        $user->setId('test-user-id');
        $user->setTimeZone('Asia/Ho_Chi_Minh');
        yield 'admin user is found' => [
            $context,
            $self->createUserRepository(true, [$user]),
            '19 54',
            '20 00',
            'Asia/Ho_Chi_Minh'
        ];
    }

    public function testSendInvitationMailWithRecipientIsInternalCustomer(): void
    {
        $invitationAction = AppointmentDefinition::INVITE_ACTION;
        $context = new Context(new AdminApiSource('test-user-id'));

        $appointmentId = \md5('test-appointment-id');
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_SELF);
        $customer = new CustomerEntity();
        $customer->setId('test-customer-id');
        $customer->setEmail('test-customer@gmail.com');
        $customer->setFirstName('test');
        $customer->setLastName('customer');
        $customerAddress = new CustomerAddressEntity();
        $country = new CountryEntity();
        $country->assign([
            'id' => 'test-country-id',
            'iso' => 'DE'
        ]);
        $customerAddress->assign([
            'id' => 'test-customer-address-id',
            'countryId' => 'test-country-id',
            'country' => $country
        ]);
        $customer->setDefaultShippingAddress($customerAddress);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setCustomerId('test-customer-id');
        $attendee->setCustomer($customer);
        $appointment->setAttendees(new AttendeeCollection([$attendee]));

        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $expectData = \array_merge($data, [
            'senderName' => 'Test User',
            'contentHtml' => $mailTemplate->getContentHtml(),
            'contentPlain' => $mailTemplate->getContentPlain(),
            'subject' => 'Test-translated-appointment-name',
            'salesChannelId' => 'test_sales_channel_id',
            'recipients' => [
                'test-customer@gmail.com' => 'test customer'
            ]
        ]);
        $customFields = [
            'accessibleFrom' => ['date' => '2000 01 01', 'time' => '13:54', 'timezone' => 'Europe/Berlin'],
            'accessibleTo' => ['date' => '2000 01 01', 'time' => '14:00', 'timezone' => 'Europe/Berlin'],
            'isSameDay' => true,
            'isGuide' => false,
            'token' => EncryptHelper::encryptString('test-customer@gmail.com')
        ];
        $appointment->setCustomFields($customFields);
        $expectTemplateData = ['appointment' => $appointment];

        $mailService->expects(static::once())
            ->method('send')
            ->with(
                static::equalTo($expectData),
                static::equalTo($context),
                static::equalTo($expectTemplateData)
            )
            ->willReturn(new Email());
        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test-customer@gmail.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $user = new UserEntity();
        $user->setId('test-user-id');
        $user->setTimeZone('Asia/Ho_Chi_Minh');
        $userRepository = $this->createUserRepository(true, [$user]);

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger, $userRepository);

        $service->send($data, $context);
    }

    public function testSendCancelMailWithSelfAppointmentSuccessfully(): void
    {
        $invitationAction = AppointmentDefinition::CANCEL_ACTION;

        $appointmentId = \md5('test-appointment-id');
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
            'cancelMessage' => 'cancel message'
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_SELF);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $expectData = \array_merge($data, [
            'senderName' => 'Test User',
            'contentHtml' => $mailTemplate->getContentHtml(),
            'contentPlain' => $mailTemplate->getContentPlain(),
            'subject' => 'CANCELLED: test-translated-appointment-name',
            'salesChannelId' => 'test_sales_channel_id',
            'recipients' => ['test@gmail.com' => 'test']
        ]);
        $customFields = ['cancelMessage' => 'cancel message'];
        $appointment->setCustomFields($customFields);
        $expectTemplateData = ['appointment' => $appointment];

        $mailService->expects(static::once())
            ->method('send')
            ->with(
                static::equalTo($expectData),
                static::equalTo($this->context),
                static::equalTo($expectTemplateData)
            )
            ->willReturn(new Email());

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test@gmail.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger);
        $service->send($data, $this->context);
    }


    public function testSendInviteMailWithGuideAppointmentSuccessfully(): void
    {
        $appointmentId = \md5('test-appointment-id');
        $invitationAction = AppointmentDefinition::INVITE_ACTION;
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-old-attendee-id');
        $attendee->setAttendeeEmail('test-old@gmail.com');
        $attendee->setInvitationStatus('maybe');
        $appointmentAttendees = $appointment->getAttendees();
        $appointmentAttendees->add($attendee); // add the attendee was received mail
        $appointment->setAttendees($appointmentAttendees);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->with(static::equalTo($invitationAction))
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $email = $this->createTestEmail();
        $mailService->expects(static::exactly(2))->method('send')->willReturn($email);

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::once())
            ->method('updateInvitationStatus')
            ->with(
                static::equalTo(['test@gmail.com', 'user@test.com']),
                static::equalTo($appointmentId),
                static::equalTo('pending'),
                static::equalTo($this->context)
            );

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test@gmail.com', 'user@test.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger);
        $service->send($data, $this->context);
    }

    /**
     * @dataProvider getTestSendMailWithGuideAppointmentSuccessfullyProviderData
     */
    public function testSendMailWithGuideAppointmentSuccessfully(
        string $invitationAction,
        int $updateStatusTime
    ): void
    {
        $appointmentId = \md5('test-appointment-id');
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
            'cancelMessage' => 'test-cancel-message'
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-old-attendee-id');
        $attendee->setAttendeeEmail('test-old@gmail.com');
        $attendee->setInvitationStatus('maybe');
        $appointmentAttendees = $appointment->getAttendees();
        $appointmentAttendees->add($attendee); // add the attendee was received mail
        $appointment->setAttendees($appointmentAttendees);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $email = $this->createTestEmail();
        $mailService->expects(static::exactly(3))->method('send')->willReturn($email);

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::exactly($updateStatusTime))->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test@gmail.com', 'test-old@gmail.com', 'user@test.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger);
        $service->send($data, $this->context);
    }

    public static function getTestSendMailWithGuideAppointmentSuccessfullyProviderData(): \Generator
    {
        yield 'cancel-all' => [AppointmentDefinition::CANCEL_ACTION, 0];
        yield 'invite-all' => [AppointmentDefinition::INVITE_ALL_ACTION, 1];
    }

    public function testSendCancelMailToEachRecipientWithGuideAppointmentSuccessfully(): void
    {
        $appointmentId = \md5('test-appointment-id');
        $invitationAction = AppointmentDefinition::CANCEL_ACTION;
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
            'cancelMessage' => 'test-cancel-message',
            'recipientEmail' => 'test@gmail.com'
        ];

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->with(static::equalTo($invitationAction))
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::exactly(1))->method('send')->willReturn(new Email());

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode(['test@gmail.com']))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode([]))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger);
        $service->send($data, $this->context);
    }

    public function testSendMailButSendFailed(): void
    {
        $appointmentId = \md5('test-appointment-id');
        $invitationAction = AppointmentDefinition::CANCEL_ACTION;
        $data = [
            'invitationAction' => $invitationAction,
            'appointmentId' => $appointmentId,
            'cancelMessage' => 'test-cancel-message'
        ];

        $appointment = $this->createTestAppointment();
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $appointmentService->expects(static::once())
            ->method('getAppointmentInvitationMailTemplateId')
            ->willReturn('test-mail-template-id');

        $mailTemplateRepository = $this->createMock(EntityRepository::class);
        $mailTemplate = new MailTemplateEntity();
        $mailTemplate->setId('test-mail-template-id');
        $mailTemplate->setContentHtml('test-content-html');
        $mailTemplate->setContentPlain('test-content-plain');
        $mailTemplateRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                MailTemplateEntity::class,
                1,
                new MailTemplateCollection([$mailTemplate]),
                null,
                new Criteria(['test-mail-template-id']),
                $this->context
            ));

        $mailService = $this->createMock(AbstractMailService::class);
        $mailService->expects(static::exactly(2))
            ->method('send')
            ->willReturn(null);

        $attendeeService = $this->createMock(AttendeeService::class);
        $attendeeService->expects(static::never())->method('updateInvitationStatus');

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::once())
            ->method('info')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode([]))
            ));
        $logger->expects(static::once())
            ->method('warning')
            ->with(static::equalTo(
                \sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode(['test@gmail.com', 'user@test.com']))
            ));

        $service = $this->getService($mailService, $appointmentService, $mailTemplateRepository, $attendeeService, $logger);
        $service->send($data, $this->context);
    }

    private function getService(
        ?MockObject $mailServiceMock = null,
        ?MockObject $appointmentServiceMock = null,
        ?MockObject $mailTemplateRepositoryMock = null,
        ?MockObject $attendeeServiceMock = null,
        ?MockObject $loggerMock = null,
        ?MockObject $userRepositoryMock = null
    ): AddDataToInvitationService
    {
        /** @var MockObject&AbstractMailService $mailService */
        $mailService = $mailServiceMock ?: $this->createMock(AbstractMailService::class);
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentServiceMock ?: $this->createMock(AppointmentService::class);
        /** @var MockObject&EntityRepository<MailTemplateCollection> $mailTemplateRepository */
        $mailTemplateRepository = $mailTemplateRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<UserCollection> $userRepository */
        $userRepository = $userRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&AttendeeService $attendeeService */
        $attendeeService = $attendeeServiceMock ?: $this->createMock(AttendeeService::class);
        /** @var MockObject&LoggerInterface $logger */
        $logger = $loggerMock ?: $this->createMock(LoggerInterface::class);

        return new AddDataToInvitationService(
            $mailService,
            $appointmentService,
            $mailTemplateRepository,
            $userRepository,
            $attendeeService,
            $logger
        );
    }

    private function createTestAppointment(): AppointmentEntity
    {
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setAttendeeEmail('test@gmail.com');
        $attendee->setInvitationStatus(null);
        $appointment->setAttendees(new AttendeeCollection([$attendee]));
        $appointment->setMode(AppointmentDefinition::MODE_SELF);
        $dateFrom = new \DateTime('2000-01-01 12:54:11');
        $dateTo = new \DateTime('2000-01-01 13:00:11');
        $appointment->setAccessibleFrom($dateFrom);
        $appointment->setAccessibleTo($dateTo);
        $user = new UserEntity();
        $user->setId('test-user-id');
        $user->setEmail('user@test.com');
        $user->setFirstName('Test');
        $user->setLastName('User');
        $appointment->setGuideUser($user);
        $appointment->setName('test-appointment-name');
        $appointment->setTranslated(['name' => 'test-translated-appointment-name']);

        return $appointment;
    }

    private function createTestEmail(): Email
    {
        $email = new Email();
        $email->setBody(new TextPart('test-body'));
        $email->setHeaders(new Headers(new MailboxListHeader('from', [new Address('test-address@gmail.com')])));

        return $email;
    }

    /**
     * @param array<UserEntity> $users
     */
    private function createUserRepository(bool $isRun, array $users = []): MockObject
    {
        $userRepository = $this->createMock(EntityRepository::class);

        if ($isRun) {
            $userRepository->expects(static::once())
                ->method('search')
                ->willReturn(new EntitySearchResult(
                    UserDefinition::ENTITY_NAME,
                    1,
                    new EntityCollection($users),
                    null,
                    new Criteria(),
                    new Context(new AdminApiSource(null))
                ));
        } else {
            $userRepository->expects(static::never())->method('search');
        }

        return $userRepository;
    }
}
