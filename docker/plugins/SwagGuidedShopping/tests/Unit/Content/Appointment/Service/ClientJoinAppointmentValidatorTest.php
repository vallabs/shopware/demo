<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAuthModeNotSetException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAuthRuleNotSetException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentForbiddenException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetAccessibleException;
use SwagGuidedShopping\Content\Appointment\Service\ClientJoinAppointmentValidator;
use SwagGuidedShopping\Content\Presentation\Exception\PresentationNotFoundException;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Service\ClientJoinAppointmentValidator
 *
 * @internal
 */
class ClientJoinAppointmentValidatorTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     * @dataProvider getValidateTrueProviderData
     */
    public function testValidateTrue(
        AppointmentEntity $appointment,
        SalesChannelContext $salesChannelContext
    ): void
    {
        /** @var MockObject&EntityRepository<AttendeeCollection> $attendeeRepository */
        $attendeeRepository = $this->createMock(EntityRepository::class);
        $validator = new ClientJoinAppointmentValidator($attendeeRepository);
        $validator->validate($appointment, $salesChannelContext);
    }

    public static function getValidateTrueProviderData(): \Generator
    {
        $self = new ClientJoinAppointmentValidatorTest('test');

        $salesChannelContextHelper = new SalesChannelContextHelper();
        $defaultSalesChannelContext = $salesChannelContextHelper->createSalesChannelContext();
        yield 'open-appointment' => [
            $self->getWorkingAppointment(),
            $defaultSalesChannelContext
        ];

        $guideAppointmentWithInvalidAccessibleFrom = $self->getWorkingAppointment();
        $guideAppointmentWithInvalidAccessibleFrom->setMode(AppointmentDefinition::MODE_GUIDED);
        $guideAppointmentWithInvalidAccessibleFrom->setAccessibleFrom((new \DateTime('now'))->modify('+1 day'));
        yield 'guide-appointment-with-invalid-accessible-from' => [
            $guideAppointmentWithInvalidAccessibleFrom,
            $defaultSalesChannelContext
        ];

        $guideAppointmentAfterEnded = $self->getWorkingAppointment();
        $guideAppointmentAfterEnded->setMode(AppointmentDefinition::MODE_GUIDED);
        $guideAppointmentAfterEnded->setEndedAt((new \DateTime('now'))->modify('-1 day'));
        yield 'guide-appointment-after-ended' => [
            $guideAppointmentAfterEnded,
            $defaultSalesChannelContext
        ];
        
        $userRestrictedAppointment = $self->getWorkingAppointment();
        $userRestrictedAppointment->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_CUSTOMER);
        $allowedAttendee = new AttendeeEntity();
        $allowedAttendee->setId('allowed-attendee');
        $allowedAttendee->setCustomerId('allowed-customer');
        $userRestrictedAppointment->setAttendees(new AttendeeCollection([$allowedAttendee]));
        $allowedCustomer = new CustomerEntity();
        $allowedCustomer->setId('allowed-customer');
        $allowedCustomer->setEmail('allowed-attendee@test.com');
        yield 'user-restricted-and-attendee-already-has-customer-id' => [
            $userRestrictedAppointment,
            $salesChannelContextHelper->createSalesChannelContext(null, $allowedCustomer),
        ];

        $ruleRestrictedAppointment = $self->getWorkingAppointment();
        $ruleRestrictedAppointment->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_RULES);
        $ruleRestrictedAppointment->setAttendeeRuleIds(['test-attendee_ruleId']);
        yield 'rule-restricted' => [
            $ruleRestrictedAppointment,
            $salesChannelContextHelper->createSalesChannelContext(null, null, null, ['test-attendee_ruleId'])
        ];

        $notAccessibleAppointment = $self->getWorkingAppointment();
        $notAccessibleAppointment->setMode(AppointmentDefinition::MODE_SELF);
        $notAccessibleAppointment->setAccessibleFrom(null);
        yield 'not-accessible-with-null-accessible-from' => [
            $notAccessibleAppointment,
            $defaultSalesChannelContext,
        ];
    }

    public function testValidateWithUserRestrictedButAttendeeHasNoCustomerIdAndUseMailToCheckLogin(): void
    {
        $userRestrictedAppointment = $this->getWorkingAppointment();
        $userRestrictedAppointment->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_CUSTOMER);
        $allowedAttendee = new AttendeeEntity();
        $allowedAttendee->setId('allowed-attendee-id');
        $allowedAttendee->setCustomerId(null);
        $allowedAttendee->setAttendeeEmail('allowed-attendee@test.com');
        $userRestrictedAppointment->setAttendees(new AttendeeCollection([$allowedAttendee]));
        $allowedCustomer = new CustomerEntity();
        $allowedCustomer->setId('allowed-customer-id');
        $allowedCustomer->setEmail('allowed-attendee@test.com');
        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('update')
            ->with(static::equalTo([['id' => 'allowed-attendee-id', 'customerId' => 'allowed-customer-id']]));

        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext(null, $allowedCustomer);

        $validator = new ClientJoinAppointmentValidator($attendeeRepository);
        $validator->validate($userRestrictedAppointment, $salesChannelContext);
    }

    /**
     * @dataProvider getTestValidateButGetExceptionProviderData
     */
    public function testValidateButGetException(
        \Exception $expected,
        AppointmentEntity $appointment,
        SalesChannelContext $salesChannelContext
    ): void
    {
        $this->expectExceptionObject($expected);
        /** @var MockObject&EntityRepository<AttendeeCollection> $attendeeRepository */
        $attendeeRepository = $this->createMock(EntityRepository::class);
        $validator = new ClientJoinAppointmentValidator($attendeeRepository);
        $validator->validate($appointment, $salesChannelContext);
    }

    public static function getTestValidateButGetExceptionProviderData(): \Generator
    {
        $self = new ClientJoinAppointmentValidatorTest('test');

        $salesChannelContextHelper = new SalesChannelContextHelper();
        $defaultSalesChannelContext = $salesChannelContextHelper->createSalesChannelContext();

        $notActiveAppointment = $self->getWorkingAppointment();
        $notActiveAppointment->setActive(false);
        yield 'appointment-is-not-active' => [
            new AppointmentNotFoundException(),
            $notActiveAppointment,
            $defaultSalesChannelContext
        ];

        $noPresentationAppointment = $self->getWorkingAppointment(false);
        yield 'appointment-has-no-presentation' => [
            new PresentationNotFoundException(),
            $noPresentationAppointment,
            $defaultSalesChannelContext
        ];

        $notActivePresentationAppointment = $self->getWorkingAppointment(true, false);
        yield 'appointment-has-presentation-is-not-active' => [
            new PresentationNotFoundException(),
            $notActivePresentationAppointment,
            $defaultSalesChannelContext
        ];

        $outdatedDate = new \DateTime('2020-01-01');
        $outdatedAppointment = $self->getWorkingAppointment();
        $outdatedAppointment->setMode(AppointmentDefinition::MODE_SELF);
        $outdatedAppointment->setAccessibleTo($outdatedDate);
        yield 'outdated-self-appointment' => [
            new AppointmentNotFoundException(),
            $outdatedAppointment,
            $defaultSalesChannelContext
        ];

        $notAccessibleSelfAppointmentWithInvalidAccessibleFrom = $self->getWorkingAppointment();
        $notAccessibleSelfAppointmentWithInvalidAccessibleFrom->setMode(AppointmentDefinition::MODE_SELF);
        $notAccessibleSelfAppointmentWithInvalidAccessibleFrom->setAccessibleFrom((new \DateTime('now'))->modify('+1 day'));
        yield 'not-accessible-self-appointment-with-invalid-accessible-from' => [
            new AppointmentNotYetAccessibleException(),
            $notAccessibleSelfAppointmentWithInvalidAccessibleFrom,
            $defaultSalesChannelContext
        ];

        $noAuthMethodAppointment = $self->getWorkingAppointment();
        $noAuthMethodAppointment->setAttendeeRestrictionType(null);
        yield 'no-auth-method' => [
            new AppointmentAuthModeNotSetException(),
            $noAuthMethodAppointment,
            $defaultSalesChannelContext
        ];

        $userRestrictedAppointment = $self->getWorkingAppointment();
        $userRestrictedAppointment->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_CUSTOMER);
        $allowedAttendee = new AttendeeEntity();
        $allowedAttendee->setId('allowed-attendee');
        $allowedAttendee->setCustomerId('allowed-customer');
        $userRestrictedAppointment->setAttendees(new AttendeeCollection([$allowedAttendee]));
        yield 'user-restricted-logged-out' => [
            new AppointmentForbiddenException(),
            $userRestrictedAppointment,
            $defaultSalesChannelContext
        ];

        $notAllowedCustomer = new CustomerEntity();
        $notAllowedCustomer->setId('not-allowed');
        $notAllowedCustomer->setEmail('not-allowed@test.com');
        yield 'user-restricted-logged-in-wrong-user' => [
            new AppointmentForbiddenException(),
            $userRestrictedAppointment,
            $salesChannelContextHelper->createSalesChannelContext(null, $notAllowedCustomer)
        ];

        $ruleRestrictedAppointmentWithoutRule = $self->getWorkingAppointment();
        $ruleRestrictedAppointmentWithoutRule->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_RULES);
        yield 'rule-restricted-without-rule' => [
            new AppointmentAuthRuleNotSetException(),
            $ruleRestrictedAppointmentWithoutRule,
            $defaultSalesChannelContext
        ];

        $ruleRestrictedAppointment = $self->getWorkingAppointment();
        $ruleRestrictedAppointment->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_RULES);
        $ruleRestrictedAppointment->setAttendeeRuleIds(['test-rule']);
        yield 'rule-restricted' => [
            new AppointmentForbiddenException(),
            $ruleRestrictedAppointment,
            $defaultSalesChannelContext
        ];
    }

    private function getWorkingAppointment(bool $hasPresentation = true, bool $isPresentationActive = true): AppointmentEntity
    {
        $presentation = new PresentationEntity();
        $presentation->setActive($isPresentationActive);

        $appointment = new AppointmentEntity();
        $appointment->setAccessibleFrom(null);
        $appointment->setAccessibleTo(null);
        $appointment->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_OPEN);
        $appointment->setActive(true);
        $appointment->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointment->setAttendeeRuleIds(null);
        $appointment->setStartedAt(new \DateTime('2020-01-01'));

        if ($hasPresentation) {
            $appointment->setPresentation($presentation);
        }

        return $appointment;
    }
}
