<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Aggregate\AppointmentTranslation;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation\AppointmentTranslationEntity;

class AppointmentTranslationEntityTest extends TestCase
{
    public function testGetAppointmentId(): void
    {
        $appointmentTranslation = new AppointmentTranslationEntity();
        $appointmentTranslation->setAppointmentId('test-appointment-id');
        static::assertSame('test-appointment-id', $appointmentTranslation->getAppointmentId());
    }

    public function testGetName(): void
    {
        $appointmentTranslation = new AppointmentTranslationEntity();
        $appointmentTranslation->setName('test-name');
        static::assertEquals('test-name', $appointmentTranslation->getName());
    }

    public function testGetMessage(): void
    {
        $appointmentTranslation = new AppointmentTranslationEntity();
        static::assertNull($appointmentTranslation->getMessage());

        $appointmentTranslation->setMessage('test-message');
        static::assertEquals('test-message', $appointmentTranslation->getMessage());

        $appointmentTranslation->setMessage(null);
        static::assertNull($appointmentTranslation->getMessage());
    }
}
