<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Aggregate\AppointmentTranslation;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation\AppointmentTranslationCollection;
use SwagGuidedShopping\Tests\Unit\Trait\ReflectionTrait;

class AppointmentTranslationCollectionTest extends TestCase
{
   use ReflectionTrait;

    public function testGetExpectedClass(): void
    {
        $collection = new AppointmentTranslationCollection();
        static::assertEquals(
            'SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation\AppointmentTranslationEntity',
            $this->invokeMethod($collection, 'getExpectedClass', [])
        );
    }
}
