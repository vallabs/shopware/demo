<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Event;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\Appointment\Event\AfterProductListingLoadedEvent;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class AfterProductListingLoadedEventTest extends TestCase
{
    public function testInitialize(): void
    {
        $searchResult = new EntitySearchResult(
            'test-entity',
            1,
            new ProductCollection(),
            null,
            new Criteria(),
            Context::createDefaultContext()
        );

        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $event = new AfterProductListingLoadedEvent($searchResult, $salesChannelContext);

        static::assertEquals($searchResult, $event->getSearchResult());
        static::assertEquals($salesChannelContext, $event->getSalesChannelContext());
        static::assertEquals($salesChannelContext->getContext(), $event->getContext());
    }
}
