<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Event;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Event\BeforeAttendeeCreatedEvent;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Event\BeforeAttendeeCreatedEvent
 *
 * @internal
 */
class BeforeAttendeeCreatedEventTest extends TestCase
{
    public function testMultiFunctions(): void
    {
        $salesChannelContextHelper = new SalesChannelContextHelper();
        $salesChannelContext = $salesChannelContextHelper->createSalesChannelContext();

        $attendeeData = [
            'test-a',
            'test-b'
        ];

        $beforeAttendeeCreatedEvent = new BeforeAttendeeCreatedEvent(
            [],
            $salesChannelContext
        );
        static::assertEquals([], $beforeAttendeeCreatedEvent->getAttendeeData());

        $beforeAttendeeCreatedEvent->setAttendeeData($attendeeData);

        static::assertEquals(
            $attendeeData,
            $beforeAttendeeCreatedEvent->getAttendeeData()
        );

        static::assertEquals($salesChannelContext->getContext(), $beforeAttendeeCreatedEvent->getContext());
        static::assertEquals($salesChannelContext, $beforeAttendeeCreatedEvent->getSalesChannelContext());
    }
}
