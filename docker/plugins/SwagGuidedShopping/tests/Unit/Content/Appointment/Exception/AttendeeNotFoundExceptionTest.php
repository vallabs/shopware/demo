<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AttendeeNotFoundExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $exception = $this->getException();
        static::assertEquals(\sprintf('Attendee with id %s not found', 'test-attendee-id'), $exception->getMessage());
    }

    public function testGetErrorCode(): void
    {
        $exception = $this->getException();
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__ATTENDEE_NOT_FOUND, $exception->getErrorCode());
    }

    public function testGetStatusCode(): void
    {
        $exception = $this->getException();
        static::assertEquals(Response::HTTP_NOT_FOUND, $exception->getStatusCode());
    }

    private function getException(): AttendeeNotFoundException
    {
        return new AttendeeNotFoundException('test-attendee-id');
    }
}
