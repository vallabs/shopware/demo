<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeCouldNotBeCreatedException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AttendeeCouldNotBeCreatedExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AttendeeCouldNotBeCreatedException();

        static::assertEquals('Attendee could not be created', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AttendeeCouldNotBeCreatedException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__ATTENDEE_COULD_NOT_BE_CREATED, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AttendeeCouldNotBeCreatedException();

        static::assertEquals(500, $exception->getStatusCode());
    }
}
