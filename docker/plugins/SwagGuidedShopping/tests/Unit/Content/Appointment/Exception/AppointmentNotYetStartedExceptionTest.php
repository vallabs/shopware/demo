<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetStartedException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentNotYetStartedExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentNotYetStartedException();

        static::assertEquals('Appointment not yet started', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentNotYetStartedException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_NOT_STARTED, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentNotYetStartedException();

        static::assertEquals(307, $exception->getStatusCode());
    }
}
