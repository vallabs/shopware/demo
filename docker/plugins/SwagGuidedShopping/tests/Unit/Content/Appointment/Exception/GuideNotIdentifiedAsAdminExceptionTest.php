<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\GuideNotIdentifiedAsAdminException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class GuideNotIdentifiedAsAdminExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $exception = new GuideNotIdentifiedAsAdminException();
        static::assertEquals('Guide can not be identified with admin user id', $exception->getMessage());
    }

    public function testGetErrorCode(): void
    {
        $exception = new GuideNotIdentifiedAsAdminException();
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__GUIDE_NOT_IDENTIFIED_BY_USER_ID, $exception->getErrorCode());
    }

    public function testGetStatusCode(): void
    {
        $exception = new GuideNotIdentifiedAsAdminException();
        static::assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $exception->getStatusCode());
    }
}
