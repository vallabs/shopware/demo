<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsPreviewException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentIsPreviewExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentIsPreviewException();

        static::assertEquals('The preview appointment can not started.', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentIsPreviewException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_IS_PREVIEW, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentIsPreviewException();

        static::assertEquals(405, $exception->getStatusCode());
    }
}
