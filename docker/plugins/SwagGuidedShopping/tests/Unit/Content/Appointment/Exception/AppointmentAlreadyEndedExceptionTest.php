<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAlreadyEndedException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentAlreadyEndedExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentAlreadyEndedException();

        static::assertEquals('Appointment is already ended', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentAlreadyEndedException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_ALREADY_ENDED, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentAlreadyEndedException();

        static::assertEquals(405, $exception->getStatusCode());
    }
}
