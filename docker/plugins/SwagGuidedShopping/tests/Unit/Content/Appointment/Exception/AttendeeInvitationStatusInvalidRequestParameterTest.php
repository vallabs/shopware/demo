<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeInvitationInvalidRequestParameter;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class AttendeeInvitationStatusInvalidRequestParameterTest extends TestCase
{
    public function testInitialize(): void
    {
        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $violateCollection = new ViolationList($constraintViolationList);
        $exception = new AttendeeInvitationInvalidRequestParameter($violateCollection);
        static::assertEquals("There are the following parameter errors in the request body \n" . $violateCollection, $exception->getMessage());
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__ATTENDEE_INVITATION_STATUS_INVALID_REQUEST_PATAMETER, $exception->getErrorCode());
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
    }
}
