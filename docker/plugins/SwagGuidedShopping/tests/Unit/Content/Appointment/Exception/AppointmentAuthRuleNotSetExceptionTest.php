<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAuthRuleNotSetException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentAuthRuleNotSetExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentAuthRuleNotSetException();

        static::assertEquals('No appointment auth rule set', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentAuthRuleNotSetException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_AUTH_RULE_NOT_SET, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentAuthRuleNotSetException();

        static::assertEquals(403, $exception->getStatusCode());
    }
}
