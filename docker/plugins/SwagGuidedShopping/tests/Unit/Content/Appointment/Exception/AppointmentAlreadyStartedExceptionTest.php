<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAlreadyStartedException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentAlreadyStartedExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentAlreadyStartedException();

        static::assertEquals('Appointment is already started', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentAlreadyStartedException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_ALREADY_STARTED, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentAlreadyStartedException();

        static::assertEquals(405, $exception->getStatusCode());
    }
}
