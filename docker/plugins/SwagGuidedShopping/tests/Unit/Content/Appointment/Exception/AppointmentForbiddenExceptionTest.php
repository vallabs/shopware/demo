<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentForbiddenException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentForbiddenExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentForbiddenException();

        static::assertEquals("You don't have access to this appointment", $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentForbiddenException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_AUTH_FORBIDDEN, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentForbiddenException();

        static::assertEquals(403, $exception->getStatusCode());
    }
}
