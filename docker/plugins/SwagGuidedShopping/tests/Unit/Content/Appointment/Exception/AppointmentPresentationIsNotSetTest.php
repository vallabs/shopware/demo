<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentPresentationIsNotSet;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentPresentationIsNotSetTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentPresentationIsNotSet();

        static::assertEquals('The appointment needs to have a presentation set.', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentPresentationIsNotSet();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_PRESENTATION_NOT_SET, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentPresentationIsNotSet();

        static::assertEquals(400, $exception->getStatusCode());
    }
}
