<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationCouldNotSendException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationMissingStartOrEndTimeException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AppointmentInvitationMissingStartOrEndTimeExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new AppointmentInvitationMissingStartOrEndTimeException();
        static::assertEquals('Invitation email can not be sent because of missing start/end time.', $exception->getMessage());
        static::assertEquals(ErrorCode::INVITATION_EMAIL_SENDING_TIME_ERROR, $exception->getErrorCode());
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
    }
}
