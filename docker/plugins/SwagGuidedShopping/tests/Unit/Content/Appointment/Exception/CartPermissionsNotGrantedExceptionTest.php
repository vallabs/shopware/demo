<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\CartPermissionsNotGrantedException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class CartPermissionsNotGrantedExceptionTest extends TestCase
{
    public function testGetMessage(): void
    {
        $exception = new CartPermissionsNotGrantedException();
        static::assertEquals('Guide permissions for cart not granted', $exception->getMessage());
    }

    public function testGetErrorCode(): void
    {
        $exception = new CartPermissionsNotGrantedException();
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__GUIDE_CART_PERMISSIONS_NOT_GRANTED, $exception->getErrorCode());
    }

    public function testGetStatusCode(): void
    {
        $exception = new CartPermissionsNotGrantedException();
        static::assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, $exception->getStatusCode());
    }
}
