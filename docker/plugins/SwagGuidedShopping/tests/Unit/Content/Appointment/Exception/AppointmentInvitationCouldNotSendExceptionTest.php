<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationCouldNotSendException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AppointmentInvitationCouldNotSendExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new AppointmentInvitationCouldNotSendException('test-message');
        static::assertEquals('test-message', $exception->getMessage());
        static::assertEquals(ErrorCode::INVITATION_EMAIL_SENDING_ERROR, $exception->getErrorCode());
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
    }
}
