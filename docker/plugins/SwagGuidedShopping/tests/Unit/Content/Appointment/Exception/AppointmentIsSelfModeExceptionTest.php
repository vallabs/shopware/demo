<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsSelfModeException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentIsSelfModeExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentIsSelfModeException();

        static::assertEquals('Appointment is in self mode', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentIsSelfModeException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_IS_SELF_MODE, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentIsSelfModeException();

        static::assertEquals(405, $exception->getStatusCode());
    }
}
