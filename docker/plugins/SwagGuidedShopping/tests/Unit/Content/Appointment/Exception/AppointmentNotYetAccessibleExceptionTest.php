<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetAccessibleException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentNotYetAccessibleExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentNotYetAccessibleException();

        static::assertEquals('Appointment not yet accessible', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentNotYetAccessibleException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_NOT_YET_ACCESSIBLE, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentNotYetAccessibleException();

        static::assertEquals(307, $exception->getStatusCode());
    }
}
