<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentJoinInvalidRequestParameter;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class AppointmentJoinInvalidRequestParameterTest extends TestCase
{
    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testExceptionMessage(ViolationList $violateCollection): void
    {
        $exception = new AppointmentJoinInvalidRequestParameter($violateCollection);
        $expectMessage = "There are the following parameter errors in the request body \n" . $violateCollection;
        static::assertEquals($expectMessage, $exception->getMessage());
    }

    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testExceptionErrorCode(ViolationList $violationCollection): void
    {
        $exception = new AppointmentJoinInvalidRequestParameter($violationCollection);
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_JOIN_INVALID_REQUEST_PATAMETER, $exception->getErrorCode());
    }

    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testExceptionStatusCode(ViolationList $violationCollection): void
    {
        $exception = new AppointmentJoinInvalidRequestParameter($violationCollection);
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
    }

    public static function getTestExceptionWithDifferentViolationCollections(): \Generator
    {
        $emptyViolateCollection = new ViolationList(new ConstraintViolationList());
        yield 'empty-violate-collection' => [$emptyViolateCollection];

        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $nonEmptyCollection = new ViolationList($constraintViolationList);
        yield 'non-empty-violate-collection' => [$nonEmptyCollection];
    }
}
