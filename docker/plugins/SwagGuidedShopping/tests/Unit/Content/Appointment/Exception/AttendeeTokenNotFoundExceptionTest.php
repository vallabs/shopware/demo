<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeTokenNotFoundException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AttendeeTokenNotFoundExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AttendeeTokenNotFoundException();

        static::assertEquals('No token for attendee found', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AttendeeTokenNotFoundException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__TOKEN_FOR_ATTENDEE_NOT_FOUND, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AttendeeTokenNotFoundException();

        static::assertEquals(404, $exception->getStatusCode());
    }
}
