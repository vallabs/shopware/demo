<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Exception\ErrorCode;

/**
 * @internal
 */
class AppointmentNotFoundExceptionTest extends TestCase
{
    public function testMessage(): void
    {
        $exception = new AppointmentNotFoundException();

        static::assertEquals('Appointment not found', $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new AppointmentNotFoundException();

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__APPOINTMENT_NOT_FOUND, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new AppointmentNotFoundException();

        static::assertEquals(404, $exception->getStatusCode());
    }
}
