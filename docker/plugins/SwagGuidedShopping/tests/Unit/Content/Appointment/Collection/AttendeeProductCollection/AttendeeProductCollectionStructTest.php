<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Collection\AttendeeProductCollection;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionStruct;

class AttendeeProductCollectionStructTest extends TestCase
{
    /**
     * @param array<string, mixed> $collection
     * @dataProvider getTestInitialize
     */
    public function testInitialize(array $collection): void
    {
        $struct = new AttendeeProductCollectionStruct($collection);
        static::assertSame($collection, $struct->getCollection());
    }

    public static function getTestInitialize(): \Generator
    {
        yield [[]];
        yield [['key' => 'key-value']];
        yield [['key' => 'key-value', 'key2' => 1]];
    }
}
