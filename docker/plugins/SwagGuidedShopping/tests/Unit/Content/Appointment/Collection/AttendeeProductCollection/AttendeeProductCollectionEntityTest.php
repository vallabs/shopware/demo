<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Attendee;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionEntity;
use Shopware\Core\Content\Product\ProductEntity;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionEntity
 *
 * @internal
 */
class AttendeeProductCollectionEntityTest extends TestCase
{
    public function testAttendeeProductCollectionWithAttendee(): void
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $attendee = new AttendeeEntity();
        $attendeeProductCollection->setAttendee($attendee);
        static::assertInstanceOf(AttendeeEntity::class, $attendeeProductCollection->getAttendee());
        $attendeeProductCollection->setAttendee(null);
        static::assertNull($attendeeProductCollection->getAttendee());
    }

    public function testAttendeeProductCollectionWithAttendeeId(): void
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $atendeeId = 'test-id';
        $attendeeProductCollection->setAttendeeId($atendeeId);
        static::assertEquals($atendeeId, $attendeeProductCollection->getAttendeeId());
    }

    public function testAttendeeProductCollectionWithSalesChannelContextToken(): void
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $token = 'test-token';
        $attendeeProductCollection->setSalesChannelContextToken($token);
        static::assertEquals($token, $attendeeProductCollection->getSalesChannelContextToken());
    }

    public function testAttendeeProductCollectionWithProduct(): void
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $product = new ProductEntity();
        $attendeeProductCollection->setProduct($product);
        static::assertInstanceOf(ProductEntity::class, $attendeeProductCollection->getProduct());
        $attendeeProductCollection->setProduct(null);
        static::assertNull($attendeeProductCollection->getProduct());
    }

    public function testAttendeeProductCollectionWithProductId(): void
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $prodctId = 'test-id';
        $attendeeProductCollection->setProductId($prodctId);
        static::assertEquals($prodctId, $attendeeProductCollection->getProductId());
    }

    public function testAttendeeProductCollectionWithAlias(): void
    {
        $attendeeProductCollection = new AttendeeProductCollectionEntity();
        $alias = 'test-alias';
        $attendeeProductCollection->setAlias($alias);
        static::assertEquals($alias, $attendeeProductCollection->getAlias());
    }
}
