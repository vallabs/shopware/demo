<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Collection\AttendeeProductCollection;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionCollection;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionCollection
 *
 * @internal
 */
class AttendeeProductCollectionTest extends TestCase
{
    /**
     * @param array<string, mixed> $parameters
     */
    public function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testGetExpectedClass(): void
    {
        $attendeeProductCollectionCollection = new AttendeeProductCollectionCollection();
        $this->assertEquals($this->invokeMethod($attendeeProductCollectionCollection, 'getExpectedClass', []),
            'SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionEntity');
    }
}
