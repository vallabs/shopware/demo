<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Collection\AttendeeProductCollection;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionResponse;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionStruct;

class AttendeeProductCollectionResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $data = new AttendeeProductCollectionStruct([]);
        $response = new AttendeeProductCollectionResponse($data);
        static::assertSame($data, $response->getObject());
    }
}
