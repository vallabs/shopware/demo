<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Collection;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Appointment\Collection\LastSeenProductsResponse;

class LastSeenProductsResponseTest extends TestCase
{
    public function testInitialize(): void
    {
        $struct = new ArrayStruct(['key' => 'value']);
        $response = new LastSeenProductsResponse($struct);
        static::assertSame($struct, $response->getObject());
    }
}
