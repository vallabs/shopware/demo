<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Attendee;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;

/**
 * @coversDefaultClass \SwagGuidedShopping\Content\Appointment\AppointmentCollection
 *
 * @internal
 */
class AppointmentCollectionTest extends TestCase
{
    /**
     * @param array<string, mixed> $parameters
     */
    public function invokeMethod(object $object, string $methodName, array $parameters): mixed
    {
        $reflection = new \ReflectionClass(\get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    public function testGetExpectedClass(): void
    {
        $appointmentCollection = new AppointmentCollection();
        $this->assertEquals($this->invokeMethod($appointmentCollection, 'getExpectedClass', []), 'SwagGuidedShopping\Content\Appointment\AppointmentEntity');
    }
}
