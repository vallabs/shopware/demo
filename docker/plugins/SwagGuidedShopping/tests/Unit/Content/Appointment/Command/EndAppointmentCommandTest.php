<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Tester\CommandTester;
use SwagGuidedShopping\Content\Appointment\Command\EndAppointmentCommand;
use SwagGuidedShopping\Content\Appointment\ScheduledTask\EndAppointmentTaskHandler;

/**
 * @internal
 */
class EndAppointmentCommandTest extends TestCase
{
    public function testExecute(): void
    {
        $endAppointmentTaskHandler = $this->createMock(EndAppointmentTaskHandler::class);
        $endAppointmentCommand = new EndAppointmentCommand($endAppointmentTaskHandler);
        static::assertEquals(
            $endAppointmentCommand->getDescription(),
            'Closes all outdated appointments which were not ended by user'
        );

        $commandTester = new CommandTester($endAppointmentCommand);
        $commandTester->execute([]);

        static::assertEquals(
            0,
            $commandTester->getStatusCode(),
        );
    }
}
