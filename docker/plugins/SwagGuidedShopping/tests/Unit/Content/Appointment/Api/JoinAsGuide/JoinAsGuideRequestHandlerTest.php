<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\JoinAsGuide;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\PlatformRequest;
use SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide\JoinAsGuidePermissionException;
use SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide\JoinAsGuideRequestHandler;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use SwagGuidedShopping\Content\Appointment\Struct\AppointmentJoinStruct;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class JoinAsGuideRequestHandlerTest extends TestCase
{
    protected Context $context;
    protected SalesChannelContextHelper $salesChannelContextHelper;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
        $this->salesChannelContextHelper = new SalesChannelContextHelper();
    }

    public function testHandleRequestWithUserIsNotAdmin(): void
    {
        $handler = $this->getHandler();
        $adminApiSource = new AdminApiSource('test-user-id');
        static::expectException(JoinAsGuidePermissionException::class);
        $handler->handleRequest('appointment-id', $this->context, $adminApiSource);
    }

    public function testHandleRequestButCouldNotFindSalesChannelContext(): void
    {
        $handler = $this->getHandler();
        $adminApiSource = new AdminApiSource('test-user-id');
        $adminApiSource->setIsAdmin(true);
        static::expectException(\Exception::class);
        static::expectExceptionMessage('Sales channel context is not found.');
        $handler->handleRequest('appointment-id', $this->context, $adminApiSource);
    }

    /**
     * @dataProvider getTestHandleRequestWithValidAdminApiSource
     */
    public function testHandleRequestWithUserIsAdminButCannotFoundAppointment(AdminApiSource $adminApiSource): void
    {
        $appointmentId = 'appointment-id';
        $context = new Context(new AdminApiSource('test-user-id'));
        $context->addExtension(GuidedShoppingRequestContextResolver::ADMIN_CONTEXT_EXTENSION_NAME, $this->salesChannelContextHelper->createSalesChannelContext());

        $appointmentService = $this->createMock(AppointmentService::class);
        $exception = new EntityNotFoundException(AppointmentEntity::class, $appointmentId);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willThrowException($exception);

        $joinAppointmentService = $this->createMock(JoinAppointmentService::class);
        $joinAppointmentService->expects(static::never())->method('joinMeetingAsGuide');

        $handler = $this->getHandler($appointmentService, $joinAppointmentService);
        static::expectException(EntityNotFoundException::class);
        $handler->handleRequest($appointmentId, $context, $adminApiSource);
    }

    /**
     * @dataProvider getTestHandleRequestWithValidAdminApiSource
     */
    public function testHandleRequestWithUserIsAdmin(AdminApiSource $adminApiSource): void
    {
        $appointmentId = 'appointment-id';
        $context = $this->createMock(Context::class);
        $token = 'test-sales-channel';
        $salesChannelContext = $this->salesChannelContextHelper->createSalesChannelContext($token);
        $context->expects(static::once())
            ->method('getExtension')
            ->willReturn($salesChannelContext);

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointmentService->expects(static::once())->method('getAppointment');

        $joinAppointmentService = $this->createMock(JoinAppointmentService::class);
        $expectedStruct = new AppointmentJoinStruct(
            'test-id',
            $token,
            'test-attendee-id',
            'test-sales-channel-id',
            'guided'
        );
        $joinAppointmentService->expects(static::once())
            ->method('joinMeetingAsGuide')
            ->willReturn($expectedStruct);

        $handler = $this->getHandler($appointmentService, $joinAppointmentService);
        $response = $handler->handleRequest($appointmentId, $context, $adminApiSource);
        $key = PlatformRequest::HEADER_CONTEXT_TOKEN;
        static::assertTrue($response->headers->has($key));
        static::assertSame($expectedStruct->getNewContextToken(), $response->headers->get($key));
    }

    public static function getTestHandleRequestWithValidAdminApiSource(): \Generator
    {
        $adminUser = new AdminApiSource('test-admin-user-id');
        $adminUser->setIsAdmin(true);
        yield 'user-is-admin' => [$adminUser];

        $notAdminUserButFullyPermissions = new AdminApiSource('test-not-admin-but-fully-permissions-user-id');
        $notAdminUserButFullyPermissions->setPermissions(JoinAsGuideRequestHandler::REQUIRED_GUIDE_PERMISSIONS);
        yield 'user-is-not-admin-but-fully-permissions' => [$notAdminUserButFullyPermissions];
    }

    private function getHandler(
        ?MockObject $appointmentService = null,
        ?MockObject $joinAppointmentService = null
    ): JoinAsGuideRequestHandler
    {
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentService ?: $this->createMock(AppointmentService::class);
        /** @var MockObject&JoinAppointmentService $joinAppointmentService */
        $joinAppointmentService = $joinAppointmentService ?: $this->createMock(JoinAppointmentService::class);

        return new JoinAsGuideRequestHandler(
            $appointmentService,
            $joinAppointmentService
        );
    }
}
