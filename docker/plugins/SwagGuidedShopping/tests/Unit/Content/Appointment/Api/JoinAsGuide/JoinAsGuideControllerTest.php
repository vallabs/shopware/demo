<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\JoinAsGuide;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Exception\InvalidUuidException;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceInterface;
use SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide\JoinAsGuideController;
use SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide\JoinAsGuideRequestHandler;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Framework\Routing\GuideSalesChannelContextResolver;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 */
class JoinAsGuideControllerTest extends TestCase
{
    private AppointmentService $appointmentService;
    private SalesChannelContextServiceInterface $contextService;
    private SalesChannelContextPersister $salesChannelContextPersister;
    private GuideSalesChannelContextResolver $contextResolver;
    private JoinAsGuideRequestHandler $invalidRequestHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->appointmentService = $this->createMock(AppointmentService::class);
        $this->contextService = $this->createMock(SalesChannelContextServiceInterface::class);
        $this->salesChannelContextPersister = $this->createMock(SalesChannelContextPersister::class);

        $this->contextResolver = new GuideSalesChannelContextResolver(
            $this->appointmentService, $this->contextService, $this->salesChannelContextPersister
        );
        $this->invalidRequestHandler = $this->createMock(JoinAsGuideRequestHandler::class);
        $this->invalidRequestHandler->expects(static::never())->method('handleRequest');
    }

    public function testJoinWithInvalidContext(): void
    {
        $appointmentId = Uuid::randomHex();
        $context = new Context(new SystemSource());
        $request = new Request();
        $controller = new JoinAsGuideController($this->contextResolver, $this->invalidRequestHandler);

        static::expectException(\Exception::class);
        static::expectExceptionMessage('No admin api Source given');
        $controller->join($appointmentId, $context, $request);
    }

    public function testJoinWithAppointmentNoSalesChannelDomain(): void
    {
        $appointmentId = Uuid::randomHex();
        $context = new Context(new AdminApiSource(Uuid::randomHex()));
        $request = new Request(
            [],
            ['appointmentId' => $appointmentId],
            [PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT => $context]
        );

        $controller = new JoinAsGuideController($this->contextResolver, $this->invalidRequestHandler);
        static::expectException(NoSalesChannelDomainMappedException::class);

        $controller->join($appointmentId, $context, $request);
    }

    public function testJoinWithInvalidUuid(): void
    {
        $appointmentId = 'invalid-uuid';
        $context = new Context(new AdminApiSource(Uuid::randomHex()));
        $request = new Request(
            [],
            ['appointmentId' => $appointmentId],
            [PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT => $context]
        );

        $contextResolverMock = $this->createMock(GuideSalesChannelContextResolver::class);
        $contextResolverMock->method('resolve')
            ->willThrowException(new InvalidUuidException($appointmentId));
        $controller = new JoinAsGuideController($contextResolverMock, $this->invalidRequestHandler);
        static::expectException(AppointmentNotFoundException::class);

        $controller->join($appointmentId, $context, $request);
    }

    public function testJoinWithValidData(): void
    {
        $appointmentId = Uuid::randomHex();
        $context = new Context(new AdminApiSource(Uuid::randomHex()));
        $request = new Request(
            [],
            ['appointmentId' => $appointmentId],
            [PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT => $context]
        );

        $contextResolverMock = $this->createMock(GuideSalesChannelContextResolver::class);
        $contextResolverMock->expects(static::once())->method('resolve');

        $requestHandler = $this->createMock(JoinAsGuideRequestHandler::class);
        $requestHandler->expects(static::once())->method('handleRequest');

        $controller = new JoinAsGuideController($contextResolverMock, $requestHandler);

        $controller->join($appointmentId, $context, $request);
    }
}
