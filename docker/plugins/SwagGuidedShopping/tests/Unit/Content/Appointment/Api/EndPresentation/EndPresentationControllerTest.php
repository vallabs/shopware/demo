<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\EndPresentation;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Appointment\Api\EndPresentation\EndPresentationController;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EndPresentationControllerTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testStartPresentation(): void
    {
        $appointmentService = $this->createMock(AppointmentService::class);
        $controller = new EndPresentationController($appointmentService);

        $appointmentId = '1';
        $response = $controller->end($appointmentId, $this->context);
        static::assertSame(Response::HTTP_OK, $response->getStatusCode());
    }
}
