<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\StartPresentation;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Appointment\Api\StartPresentation\StartPresentationController;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use Symfony\Component\HttpFoundation\Response;

class StartPresentationControllerTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testStartPresentation(): void
    {
        $appointmentService = $this->createMock(AppointmentService::class);
        $controller = new StartPresentationController($appointmentService);

        $appointmentId = '1';
        $response = $controller->start($appointmentId, $this->context);
        static::assertSame(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}
