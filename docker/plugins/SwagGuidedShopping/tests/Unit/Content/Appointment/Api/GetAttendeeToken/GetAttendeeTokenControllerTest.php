<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\GetAttendeeToken;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\Appointment\Api\GetAttendeeToken\GetAttendeeTokenController;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeTokenNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\CartPermissionsNotGrantedException;

class GetAttendeeTokenControllerTest extends TestCase
{
    protected Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    /**
     * @dataProvider getTestGetTokenWithExceptionProviderData
     *
     * @param class-string<\Throwable> $exception
     */
    public function testGetTokenWithException(
        ?AttendeeEntity $attendee,
        ?MockObject $connectionMock,
        string $exception
    ): void
    {
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ?: $this->createMock(Connection::class);
        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeEntity::class,
                $attendee ? 1 : 0,
                $attendee ? new AttendeeCollection([$attendee]) : new AttendeeCollection(),
                null,
                new Criteria(),
                $this->context
            ));

        $controller =  new GetAttendeeTokenController($connection, $attendeeRepository);
        static::expectException($exception);
        $controller->getToken('test-attendee-id', $this->context);
    }

    public function testGetToken(): void
    {
        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchOne')->willReturn('test-token');
        $connection->expects(static::once())
            ->method('executeQuery')
            ->with(
                static::equalTo("SELECT `token` FROM `sales_channel_api_context` WHERE JSON_UNQUOTE(JSON_EXTRACT(payload, '$.attendeeId')) = :attendeeId"),
                static::equalTo([
                    'attendeeId' => 'test-attendee-id'
                ])
            )
            ->willReturn($result);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setGuideCartPermissionsGranted(true);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AttendeeEntity::class,
                1,
                new AttendeeCollection([$attendee]),
                null,
                new Criteria(),
                $this->context
            ));

        $controller =  new GetAttendeeTokenController($connection, $attendeeRepository);
        $response = $controller->getToken('test-attendee-id', $this->context);
        $content = $response->getContent();
        static::assertNotFalse($content);
        $decodedContent = (array) @\json_decode($content);
        static::assertArrayHasKey('attendee-sw-context-token', $decodedContent);
        static::assertEquals('test-token', $decodedContent['attendee-sw-context-token']);
    }

    public static function getTestGetTokenWithExceptionProviderData(): \Generator
    {
        $self = new GetAttendeeTokenControllerTest('test');

        yield 'attendee not found' => [
            null,
            null,
            AttendeeNotFoundException::class
        ];

        $attendeeWithoutGuideCartPermission = new AttendeeEntity();
        $attendeeWithoutGuideCartPermission->setId('test-attendee-id');
        yield 'attendee without guide cart permission' => [
            $attendeeWithoutGuideCartPermission,
            null,
            CartPermissionsNotGrantedException::class
        ];

        $attendeeWithGuideCartPermission = new AttendeeEntity();
        $attendeeWithGuideCartPermission->setId('test-attendee-id');
        $attendeeWithGuideCartPermission->setGuideCartPermissionsGranted(true);

        $connectionWithException = $self->createMock(Connection::class);
        $connectionWithException->expects(static::once())
            ->method('executeQuery')
            ->with(
                static::equalTo("SELECT `token` FROM `sales_channel_api_context` WHERE JSON_UNQUOTE(JSON_EXTRACT(payload, '$.attendeeId')) = :attendeeId"),
                static::equalTo([
                    'attendeeId' => 'test-attendee-id'
                ])
            )
            ->willThrowException(new \Exception('test-throw-exception'));
        yield 'attendee with guide cart permission but connection with exception' => [
            $attendeeWithGuideCartPermission,
            $connectionWithException,
            AttendeeTokenNotFoundException::class
        ];

        $connectionWithValidResultButEmptyToken = $self->createMock(Connection::class);
        $result = $self->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchOne')->willReturn(null);
        $connectionWithValidResultButEmptyToken->expects(static::once())
            ->method('executeQuery')
            ->with(
                static::equalTo("SELECT `token` FROM `sales_channel_api_context` WHERE JSON_UNQUOTE(JSON_EXTRACT(payload, '$.attendeeId')) = :attendeeId"),
                static::equalTo([
                    'attendeeId' => 'test-attendee-id'
                ])
            )
            ->willReturn($result);
        yield 'attendee with guide cart permission but connection with valid result but empty token' => [
            $attendeeWithGuideCartPermission,
            $connectionWithValidResultButEmptyToken,
            AttendeeTokenNotFoundException::class
        ];
    }
}
