<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\JoinAsGuide;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide\JoinAsGuidePermissionException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class JoinAsGuidePermissionExceptionTest extends TestCase
{
    public function testExceptionMessage(): void
    {
        $exception = new JoinAsGuidePermissionException();
        static::assertEquals('Could not join as guide, permissions are missing', $exception->getMessage());
    }

    public function testExceptionStatusCode(): void
    {
        $exception = new JoinAsGuidePermissionException();
        static::assertEquals(Response::HTTP_FORBIDDEN, $exception->getStatusCode());
    }

    public function testExceptionErrorCode(): void
    {
        $exception = new JoinAsGuidePermissionException();
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__JOIN_AS_GUIDE_MISSING_PERMISSIONS, $exception->getErrorCode());
    }
}
