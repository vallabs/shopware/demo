<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Appointment\Api\JoinAsGuide;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide\JoinAsGuideViolationException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

class JoinAsGuideViolationExceptionTest extends TestCase
{
    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testExceptionMessage(ViolationList $violateCollection): void
    {
        $exception = new JoinAsGuideViolationException($violateCollection);

        $expectedMessage = "Could join as guide \n" . $violateCollection;
        static::assertEquals($expectedMessage, $exception->getMessage());
    }

    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testExceptionStatusCode(ViolationList $violateCollection): void
    {
        $exception = new JoinAsGuideViolationException($violateCollection);
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());
    }

    /**
     * @dataProvider getTestExceptionWithDifferentViolationCollections
     */
    public function testExceptionErrorCode(ViolationList $violateCollection): void
    {
        $exception = new JoinAsGuideViolationException($violateCollection);
        static::assertEquals(ErrorCode::GUIDED_SHOPPING__JOIN_AS_GUIDE_VIOLATION, $exception->getErrorCode());
    }

    public static function getTestExceptionWithDifferentViolationCollections(): \Generator
    {
        $emptyViolateCollection = new ViolationList(new ConstraintViolationList());
        yield 'empty-violate-collection' => [$emptyViolateCollection];

        $constraintViolationList = new ConstraintViolationList();
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList->add($violate);
        $nonEmptyCollection = new ViolationList($constraintViolationList);
        yield 'non-empty-violate-collection' => [$nonEmptyCollection];
    }
}
