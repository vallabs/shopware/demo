<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Content\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Content\Exception\InvalidParameterException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use SwagGuidedShopping\Service\Validator\ViolationList;

/**
 * @internal
 */
class InvalidParameterExceptionTest extends TestCase
{
    private ViolationList $violationList;

    public function setUp():void
    {
        $violate = new ConstraintViolation(
            'test-violate-message',
            '',
            [
                'value' => 'test-invalid-value',
            ],
            'test-invalid-value',
            'test-path',
            'test-invalid-value',
            null,
            'test-violate-code'
        );
        $constraintViolationList = new ConstraintViolationList();
        $constraintViolationList->add($violate);
        
        $this->violationList = new ViolationList($constraintViolationList);
    }

    public function testMessage(): void
    {
        $exception = new InvalidParameterException($this->violationList);

        static::assertEquals("The Request has the following errors \ntest-violate-message test-path\n", $exception->getMessage());
    }

    public function testErrorCode(): void
    {
        $exception = new InvalidParameterException($this->violationList);

        static::assertEquals(ErrorCode::GUIDED_SHOPPING__INVALID_PARAMETER, $exception->getErrorCode());
    }

    public function testStatusCode(): void
    {
        $exception = new InvalidParameterException($this->violationList);

        static::assertEquals(400, $exception->getStatusCode());
    }
}
