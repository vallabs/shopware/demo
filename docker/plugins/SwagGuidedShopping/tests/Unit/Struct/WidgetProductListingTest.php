<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Struct;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductEntity;
use SwagGuidedShopping\Struct\WidgetProductListing;

class WidgetProductListingTest extends TestCase
{
    public function testInitialize(): void
    {
        $product = new ProductEntity();
        $product->setId('test-product-id');

        $listing = new WidgetProductListing(
            [$product],
            1,
            1,
            10
        );

        static::assertSame([$product], $listing->getProducts());
        static::assertSame(1, $listing->getTotal());
        static::assertSame(1, $listing->getPage());
        static::assertSame(10, $listing->getLimit());
    }
}
