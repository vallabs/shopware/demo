<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Stub;

use SwagGuidedShopping\Content\PresentationState\Api\GetGuidePresentationStateStruct;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetGuidePresentationStateStructStub extends GetGuidePresentationStateStruct
{
    public function __construct(
        StateForAll $stateForAll,
        StateForGuides $stateForGuides,
        StateForMe $stateForMe
    ) {
        parent::__construct($stateForAll, $stateForGuides, $stateForMe);
    }

    /**
     * @return array<string, mixed>
     */
    public function getStateForAll(): array
    {
        return $this->stateForAll;
    }

    /**
     * @return array<string, mixed>
     */
    public function getStateForGuides(): array
    {
        return $this->stateForGuides;
    }

    /**
     * @return array<string, mixed>
     */
    public function getStateForMe(): array
    {
        return $this->stateForMe;
    }
}
