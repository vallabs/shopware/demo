<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Stub;

use SwagGuidedShopping\Content\PresentationState\SalesChannel\Get\GetClientPresentationStateStruct;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetClientPresentationStateStructStub extends GetClientPresentationStateStruct
{
    public function __construct(StateForAll $stateForAll, StateForClients $stateForClients, StateForMe $stateForMe)
    {
        parent::__construct($stateForAll, $stateForClients, $stateForMe);
    }

    /**
     * @return array<string, mixed>
     */
    public function getStateForAll(): array
    {
        return $this->stateForAll;
    }

    /**
     * @return array<string, mixed>
     */
    public function getStateForClients(): array
    {
        return $this->stateForClients;
    }

    /**
     * @return array<string, mixed>
     */
    public function getStateForMe(): array
    {
        return $this->stateForMe;
    }
}