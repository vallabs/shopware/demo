<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Framework\Routing;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceInterface;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceParameters;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Framework\Routing\GuideSalesChannelContextResolver;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\HttpFoundation\Request;

class GuidedSalesChannelContextResolverTest extends TestCase
{
    public function testResolveWithRequestHasNoAppointmentId(): void
    {
        $request = new Request();
        $resolver = $this->getResolver();
        $resolver->resolve($request);
        static::expectNotToPerformAssertions();
    }

    public function testResolveWithRequestHasAppointmentIdButFoundAppointmentHasNoSalesChannelDomain(): void
    {
        $context = new Context(new SystemSource());

        $request = new Request();
        $request->attributes->set('appointmentId', 'test-appointment-id');
        $request->attributes->set('sw-context', $context);

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setSalesChannelDomain(null);
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $salesChannelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $salesChannelContextPersister->expects(static::never())->method('load');
        $salesChannelContextPersister->expects(static::never())->method('save');

        $contextService = $this->createMock(SalesChannelContextServiceInterface::class);
        $contextService->expects(static::never())->method('get');

        $resolver = $this->getResolver($appointmentService, $contextService, $salesChannelContextPersister);

        static::expectException(NoSalesChannelDomainMappedException::class);
        $resolver->resolve($request);
    }

    public function testResolveWithRequestHasAppointmentIdAndContextTokenAliasAndValidAppointmentIsFound(): void
    {
        $context = new Context(new SystemSource());

        $request = new Request();
        $request->attributes->set('appointmentId', 'test-appointment-id');
        $request->attributes->set('sw-context', $context);
        $request->headers->set('sw-context-token-alias', 'test-context-token-alias');

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        /** @var SalesChannelDomainEntity $salesChannelDomain */
        $salesChannelDomain = $appointment->getSalesChannelDomain();
        $salesChannelDomainId = $salesChannelDomain->getId();
        $salesChannelId = $salesChannelDomain->getSalesChannelId();
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $salesChannelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $salesChannelContextPersister->expects(static::once())
            ->method('load')
            ->with(
                static::equalTo('test-context-token-alias'),
                static::equalTo($salesChannelId)
            )
            ->willReturn([
                'languageId' => 'test-language-id-from-context-token',
                'currencyId' => 'test-currency-id-from-context-token',
            ]);
        $salesChannelContextPersister->expects(static::once())
            ->method('save')
            ->with(
                static::equalTo('test-context-token-alias'),
                static::arrayHasKey('cacheKey'),
                static::equalTo($salesChannelId)
            );

        $contextService = $this->createMock(SalesChannelContextServiceInterface::class);
        $expectSalesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $contextService->expects(static::once())
            ->method('get')
            ->with(
                static::callback(
                    function (SalesChannelContextServiceParameters $parameters) use ($salesChannelId, $salesChannelDomainId) {
                    return $parameters->getLanguageId() === 'test-language-id-from-context-token'
                        && $parameters->getCurrencyId() === 'test-currency-id-from-context-token'
                        && $parameters->getSalesChannelId() === $salesChannelId
                        && $parameters->getDomainId() === $salesChannelDomainId
                        && $parameters->getToken() === 'test-context-token-alias';

                })
            )
            ->willReturn($expectSalesChannelContext);

        $resolver = $this->getResolver($appointmentService, $contextService, $salesChannelContextPersister);

        $resolver->resolve($request);
        static::assertTrue($context->hasExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME));
        static::assertSame($expectSalesChannelContext, $context->getExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME));
    }

    public function testResolveWithoutContextTokenAliasButWithRequestHasAppointmentIdAndValidAppointmentIsFound(): void
    {
        $context = new Context(new SystemSource());

        $request = new Request();
        $request->attributes->set('appointmentId', 'test-appointment-id');
        $request->attributes->set('sw-context', $context);

        $appointmentService = $this->createMock(AppointmentService::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        /** @var SalesChannelDomainEntity $salesChannelDomain */
        $salesChannelDomain = $appointment->getSalesChannelDomain();
        $salesChannelId = $salesChannelDomain->getSalesChannelId();
        $appointmentService->expects(static::once())
            ->method('getAppointment')
            ->willReturn($appointment);

        $salesChannelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $salesChannelContextPersister->expects(static::once())
            ->method('load')
            ->with(
                static::callback(
                    function (string $contextToken) {
                        return \mb_strlen($contextToken) === 32;
                    }
                ),
                static::equalTo($salesChannelId)
            )
            ->willReturn([]);
        $salesChannelContextPersister->expects(static::once())
            ->method('save')
            ->with(
                static::callback(
                    function (string $contextToken) {
                        return \mb_strlen($contextToken) === 32;
                    }
                ),
                static::arrayHasKey('cacheKey'),
                static::equalTo($salesChannelId)
            );

        $contextService = $this->createMock(SalesChannelContextServiceInterface::class);
        $expectSalesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $contextService->expects(static::once())
            ->method('get')
            ->with(
                static::callback(
                    function (SalesChannelContextServiceParameters $parameters) use ($salesChannelDomain) {
                        return $parameters->getLanguageId() === $salesChannelDomain->getLanguageId()
                            && $parameters->getCurrencyId() === $salesChannelDomain->getCurrencyId()
                            && $parameters->getSalesChannelId() === $salesChannelDomain->getSalesChannelId()
                            && $parameters->getDomainId() === $salesChannelDomain->getId()
                            && \mb_strlen($parameters->getToken()) === 32;
                    })
            )
            ->willReturn($expectSalesChannelContext);

        $resolver = $this->getResolver($appointmentService, $contextService, $salesChannelContextPersister);

        $resolver->resolve($request);
        static::assertTrue($context->hasExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME));
        static::assertSame($expectSalesChannelContext, $context->getExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME));
    }

    private function getResolver(
        ?MockObject $appointmentServiceMock = null,
        ?MockObject $contextServiceMock = null,
        ?MockObject $salesChannelContextPersisterMock = null
    ): GuideSalesChannelContextResolver
    {
        /** @var MockObject&AppointmentService $appointmentService */
        $appointmentService = $appointmentServiceMock ?: $this->createMock(AppointmentService::class);
        /** @var MockObject&SalesChannelContextServiceInterface $contextService */
        $contextService = $contextServiceMock ?: $this->createMock(SalesChannelContextServiceInterface::class);
        /** @var MockObject&SalesChannelContextPersister $salesChannelContextPersister */
        $salesChannelContextPersister = $salesChannelContextPersisterMock ?: $this->createMock(SalesChannelContextPersister::class);

        return new GuideSalesChannelContextResolver(
            $appointmentService,
            $contextService,
            $salesChannelContextPersister
        );
    }
}
