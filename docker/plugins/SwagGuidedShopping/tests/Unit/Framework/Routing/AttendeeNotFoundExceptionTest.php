<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Framework\Routing;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Framework\Routing\AttendeeNotFoundException;

class AttendeeNotFoundExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new AttendeeNotFoundException('test-attendee-id');

        static::assertEquals('The attendee with id (test-attendee-id) was not found', $exception->getMessage());
        static::assertEquals(404, $exception->getStatusCode());
        static::assertEquals('GUIDED_SHOPPING__ATTENDEE_NOT_FOUND', $exception->getErrorCode());
    }
}
