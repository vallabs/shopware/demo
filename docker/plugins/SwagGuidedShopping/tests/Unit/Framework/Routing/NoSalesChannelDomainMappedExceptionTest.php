<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Framework\Routing;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;

class NoSalesChannelDomainMappedExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new NoSalesChannelDomainMappedException('test-appointment-id');

        static::assertEquals('No SalesChannel domain mapped to the appointment (test-appointment-id)', $exception->getMessage());
        static::assertEquals(404, $exception->getStatusCode());
        static::assertEquals('GUIDED_SHOPPING__APPOINTMENT_NO_SALES_CHANNEL_DOMAIN_MAPPED', $exception->getErrorCode());
    }
}
