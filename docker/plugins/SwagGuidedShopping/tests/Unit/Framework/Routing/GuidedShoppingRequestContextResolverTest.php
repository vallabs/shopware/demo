<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Framework\Routing;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use SwagGuidedShopping\SalesChannel\Exception\NotAuthenticatedException;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\HttpFoundation\Request;

class GuidedShoppingRequestContextResolverTest extends TestCase
{
    /**
     * @dataProvider getTestDoNotResolveIfNoSalesChannelContextProviderData
     *
     * @param array<string, mixed> $attributes
     */
    public function testDoNotResolveIfNoSalesChannelContext(
        array $attributes
    ): void
    {
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::never())->method('load');
        $channelContextPersister->expects(static::never())->method('save');

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('search');

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        $resolver->resolve($request);
    }

    public static function getTestDoNotResolveIfNoSalesChannelContextProviderData(): \Generator
    {
        yield 'is join route' => [
            'attributes' => [
                PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => null,
                'join' => true,
                'attendee_required' => false
            ]
        ];

        yield 'is require attendee' => [
            'attributes' => [
                PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => null,
                'join' => false,
                'attendee_required' => true
            ]
        ];
    }

    public function testResolveNothingWithNoRequireAuthenticationAndIsNotJoinRoute(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid');
        $request = new Request([], [], [
            'attributes' => [
                PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => $salesChannelContext,
                PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT => null,
                'join' => false
            ]
        ]);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::never())->method('load');
        $channelContextPersister->expects(static::never())->method('save');

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('search');

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        $resolver->resolve($request);

        static::assertFalse($salesChannelContext->hasExtension('guidedShoppingAttendee'));
    }

    public function testResolveNothingWithJoinRouteButNoAttendeeIdInContext(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid');
        $attributes = [
            PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => $salesChannelContext,
            'join' => true
        ];
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::once())
            ->method('load')
            ->willReturn([]);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('search');

        $channelContextPersister->expects(static::never())->method('save');

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        $resolver->resolve($request);

        static::assertFalse($salesChannelContext->hasExtension('guidedShoppingAttendee'));
    }

    public function testThrowExceptionWithIsNotJoinRouteButNoAttendeeIdInContext(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid');
        $attributes = [
            PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => $salesChannelContext,
            'attendee_required' => true
        ];
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::once())
            ->method('load')
            ->willReturn([]);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::never())->method('search');

        $channelContextPersister->expects(static::never())->method('save');

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        static::expectException(NotAuthenticatedException::class);

        $resolver->resolve($request);
    }

    public function testThrowExceptionWithSalesChannelContextButAppointmentHasNoDomain(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid');
        $attributes = [
            PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => $salesChannelContext,
            'join' => true
        ];
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::once())
            ->method('load')
            ->willReturn(['attendeeId' => 'test-attendee-id']);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendee = new AttendeeEntity();
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setSalesChannelDomain(null);
        $attendee->assign([
            'id' => 'test-attendee-id',
            'appointment' => $appointment,
        ]);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AttendeeCollection([$attendee]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $channelContextPersister->expects(static::never())->method('save');

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        static::expectException(NoSalesChannelDomainMappedException::class);

        $resolver->resolve($request);
    }

    public function testResolveNothingWithSalesChannelContextButFoundNoAttendee(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid');
        $attributes = [
            PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => $salesChannelContext,
            'join' => true
        ];
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::once())
            ->method('load')
            ->willReturn(['attendeeId' => 'test-attendee-id']);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                0,
                new AttendeeCollection(),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $channelContextPersister->expects(static::never())->method('save');

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        $resolver->resolve($request);

        static::assertFalse($salesChannelContext->hasExtension('guidedShoppingAttendee'));
    }

    public function testResolveWithSalesChannelContextFromPlatformAttributeObject(): void
    {
        $customer = new CustomerEntity();
        $customer->setId('test-customer-id');
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid', $customer);

        $attributes = [
            PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => $salesChannelContext,
            'join' => true
        ];
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::once())
            ->method('load')
            ->willReturn(['attendeeId' => 'test-attendee-id']);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendee = new AttendeeEntity();
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->assign([
            'id' => 'test-attendee-id',
            'appointment' => $appointment,
        ]);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AttendeeCollection([$attendee]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $channelContextPersister->expects(static::once())
            ->method('save')
            ->with(
                static::equalTo($salesChannelContext->getToken()),
                static::equalTo([]),
                static::equalTo($salesChannelContext->getSalesChannelId()),
                static::equalTo('test-customer-id')
            );

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        $resolver->resolve($request);

        static::assertTrue($salesChannelContext->hasExtension('guidedShoppingAttendee'));
        static::assertSame($attendee, $salesChannelContext->getExtension('guidedShoppingAttendee'));
    }

    public function testResolveWithSalesChannelContextFromAdminContextExtension(): void
    {
        $customer = new CustomerEntity();
        $customer->setId('test-customer-id');
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext('test-uuid', $customer);

        $context = new Context(new SystemSource());
        $context->addExtension(GuidedShoppingRequestContextResolver::ADMIN_CONTEXT_EXTENSION_NAME, $salesChannelContext);

        $attributes = [
            PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT => null,
            PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT => $context,
            'join' => true
        ];
        $request = new Request([], [], $attributes);

        $channelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $channelContextPersister->expects(static::once())
            ->method('load')
            ->willReturn(['attendeeId' => 'test-attendee-id']);

        $attendeeRepository = $this->createMock(EntityRepository::class);
        $attendee = new AttendeeEntity();
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->assign([
            'id' => 'test-attendee-id',
            'appointment' => $appointment,
        ]);
        $attendeeRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AttendeeCollection([$attendee]),
                null,
                new Criteria(),
                $salesChannelContext->getContext()
            ));

        $channelContextPersister->expects(static::once())
            ->method('save')
            ->with(
                static::equalTo($salesChannelContext->getToken()),
                static::equalTo([]),
                static::equalTo($salesChannelContext->getSalesChannelId()),
                static::equalTo('test-customer-id')
            );

        $resolver = $this->getResolver($channelContextPersister, $attendeeRepository);

        $resolver->resolve($request);

        static::assertTrue($salesChannelContext->hasExtension('guidedShoppingAttendee'));
        static::assertSame($attendee, $salesChannelContext->getExtension('guidedShoppingAttendee'));
    }

    private function getResolver(
        ?MockObject $channelContextPersisterMock = null,
        ?MockObject $attendeeRepositoryMock = null
    ): GuidedShoppingRequestContextResolver
    {
        /** @var MockObject&SalesChannelContextPersister $channelContextPersister */
        $channelContextPersister = $channelContextPersisterMock ?: $this->createMock(SalesChannelContextPersister::class);
        /** @var MockObject&EntityRepository<AttendeeCollection> $attendeeRepository */
        $attendeeRepository = $attendeeRepositoryMock ?: $this->createMock(EntityRepository::class);

        return new GuidedShoppingRequestContextResolver(
            $channelContextPersister,
            $attendeeRepository
        );
    }
}
