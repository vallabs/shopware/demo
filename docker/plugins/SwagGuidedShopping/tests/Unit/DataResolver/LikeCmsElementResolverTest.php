<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\DataResolver;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\FieldConfig;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductCollection;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SalesChannel\SalesChannelDefinition;
use SwagGuidedShopping\DataResolver\LikeCmsElementResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CmsSlotMockHelper;
use Symfony\Component\HttpFoundation\Request;

class LikeCmsElementResolverTest extends TestCase
{
    private SalesChannelContext $salesChannelContext;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
    }

    public function testGetType(): void
    {
        $resolver = new LikeCmsElementResolver();
        static::assertSame('like', $resolver->getType());
    }

    public function testEnrichWithNoProductConfig(): void
    {
        $resolver = new LikeCmsElementResolver();

        $slot = (new CmsSlotMockHelper())->getCmsSlotEntity(
            'test-cms-slot-id',
            'test-cms-block-id',
            [],
            new FieldConfigCollection()
        );

        $resolveContext = new ResolverContext(
            $this->salesChannelContext,
            new Request()
        );

        $elementDataCollection = new ElementDataCollection();

        $resolver->enrich($slot, $resolveContext, $elementDataCollection);

        static::assertFalse($slot->getData() instanceof ProductEntity);
    }

    public function testEnrichWithStaticProductConfig(): void
    {
        $resolver = new LikeCmsElementResolver();

        $fieldConfig = new FieldConfig('product', 'static', 'test-product-id');
        $slot = (new CmsSlotMockHelper())->getCmsSlotEntity(
            'test-cms-slot-id',
            'test-cms-block-id',
            [],
            new FieldConfigCollection([$fieldConfig])
        );

        $resolveContext = new ResolverContext(
            $this->salesChannelContext,
            new Request()
        );

        $elementDataCollection = new ElementDataCollection();
        $product = new SalesChannelProductEntity();
        $product->setId('test-product-id');
        $elementDataCollection->add('product_test-cms-slot-id',
            new EntitySearchResult(
                SalesChannelDefinition::ENTITY_NAME,
                1,
                new EntityCollection([$product]),
                null,
                new Criteria(),
                $this->salesChannelContext->getContext()
            )
        );

        $resolver->enrich($slot, $resolveContext, $elementDataCollection);

        static::assertTrue($slot->getData() instanceof ProductEntity);
        static::assertSame($product, $slot->getData());
    }

    public function testEnrichWithMappedProductConfig(): void
    {
        $resolver = new LikeCmsElementResolver();

        $fieldConfig = new FieldConfig('product', 'mapped', 'test-product-id');
        $slot = (new CmsSlotMockHelper())->getCmsSlotEntity(
            'test-cms-slot-id',
            'test-cms-block-id',
            [],
            new FieldConfigCollection([$fieldConfig])
        );

        $product = new SalesChannelProductEntity();
        $product->setId('test-product-id');
        $resolveContext = new EntityResolverContext(
            $this->salesChannelContext,
            new Request(),
            new SalesChannelProductDefinition(),
            $product
        );

        $elementDataCollection = new ElementDataCollection();

        $resolver->enrich($slot, $resolveContext, $elementDataCollection);

        static::assertTrue($slot->getData() instanceof ProductEntity);
        static::assertSame($product, $slot->getData());
    }
}
