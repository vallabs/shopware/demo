<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Service\ScheduledTask;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Service\ScheduledTask\DeletePreviewPresentationTask;

/**
 * @internal
 */
class DeletePreviewPresentationTaskTest extends TestCase
{
    public function testGetTaskName(): void
    {
        $task = new DeletePreviewPresentationTask();
        static::assertEquals('shopware_guided_shopping.delete_preview_presentation', $task->getTaskName());
    }

    public function testGetDefaultInterval(): void
    {
        $task = new DeletePreviewPresentationTask();
        static::assertEquals(604800, $task->getDefaultInterval());
    }
}
