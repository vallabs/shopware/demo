<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Service\ScheduledTask;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Service\ScheduledTask\DeletePreviewPresentationTaskHandler;

/**
 * @internal
 */
class DeletePreviewPresentationTaskHandlerTest extends TestCase
{
    /**
     * @dataProvider getTestRunProviderData
     */
    public function testRun(
        MockObject $presentationMock = null,
        MockObject $presentationCmsPageMock = null,
        MockObject $appointmentMock = null
    ): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects(static::exactly(6))->method('info');
        $taskHandler = $this->getTaskHandler($presentationMock, $presentationCmsPageMock, $appointmentMock, $logger);
        $taskHandler->run();
    }

    public static function getTestRunProviderData(): \Generator
    {
        $self = new DeletePreviewPresentationTaskHandlerTest('test');

        yield 'no repositories run' => [];
        yield 'presentation repository run' => [$self->createPresentationRepositoryMock()];
        yield 'presentation cms page repository run' => [null, $self->createPresentationCmsPageRepositoryMock()];
        yield 'appointment repository run' => [null, null, $self->createAppointmentRepositoryMock()];
        yield 'presentation and presentation cms page repository run' => [$self->createPresentationRepositoryMock(), $self->createPresentationCmsPageRepositoryMock(), null];
        yield 'presentation and appointment repository run' => [$self->createPresentationRepositoryMock(), null, $self->createAppointmentRepositoryMock()];
        yield 'presentation cms page and appointment repository run' => [null, $self->createPresentationCmsPageRepositoryMock(), $self->createAppointmentRepositoryMock()];
        yield 'all repositories run' => [$self->createPresentationRepositoryMock(), $self->createPresentationCmsPageRepositoryMock(), $self->createAppointmentRepositoryMock()];
    }

    private function getTaskHandler(
        MockObject $presentationRepositoryMock = null,
        MockObject $presentationCmsPageRepositoryMock = null,
        MockObject $appointmentRepositoryMock = null,
        MockObject $loggerMock = null
    ): DeletePreviewPresentationTaskHandler
    {
        /** @var MockObject&EntityRepository<ScheduledTaskCollection> $scheduledTaskRepository */
        $scheduledTaskRepository = $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<PresentationCollection> $presentationRepository */
        $presentationRepository = $presentationRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository */
        $presentationCmsPageRepository = $presentationCmsPageRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&LoggerInterface $logger */
        $logger = $loggerMock ?: $this->createMock(LoggerInterface::class);

        return new DeletePreviewPresentationTaskHandler($scheduledTaskRepository, $presentationRepository, $presentationCmsPageRepository, $appointmentRepository, $logger);
    }

    private function createPresentationRepositoryMock(): MockObject
    {
        $presentationMock = $this->createMock(EntityRepository::class);
        $presentationMock->expects(static::once())->method('searchIds')
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['presentation-1'], 'primaryKey' => 'presentation-1']],
                new Criteria(),
                Context::createDefaultContext()
            ));
        $presentationMock->expects(static::once())->method('delete');

        return $presentationMock;
    }

    private function createPresentationCmsPageRepositoryMock(): MockObject
    {
        $presentationCmsPageMock = $this->createMock(EntityRepository::class);
        $presentationCmsPageMock->expects(static::once())->method('searchIds')
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['presentation-cms-page-1'], 'primaryKey' => 'presentation-cms-page-1']],
                new Criteria(),
                Context::createDefaultContext()
            ));
        $presentationCmsPageMock->expects(static::once())->method('delete');

        return $presentationCmsPageMock;
    }

    private function createAppointmentRepositoryMock(): MockObject
    {
        $appointmentMock = $this->createMock(EntityRepository::class);
        $appointmentMock->expects(static::once())->method('searchIds')
            ->willReturn(new IdSearchResult(
                1,
                [['data' => ['appointment-1'], 'primaryKey' => 'appointment-1']],
                new Criteria(),
                Context::createDefaultContext()
            ));
        $appointmentMock->expects(static::once())->method('delete');

        return $appointmentMock;
    }
}
