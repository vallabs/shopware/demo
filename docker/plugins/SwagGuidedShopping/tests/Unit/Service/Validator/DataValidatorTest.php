<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Service\Validator;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;

/**
 * @internal
 * @coversDefaultClass \SwagGuidedShopping\Service\Validator\DataValidator
 */
class DataValidatorTest extends TestCase
{
    public function testWithNoError(): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $dataBag = [
            'test-field' => 'test-value',
        ];
        $violations = $validator->validate($dataBag, new Collection([
            'test-field' => [new Type('string'), new Required(), new NotBlank()],
        ]));

        static::assertEquals(0, $violations->count());
    }

    public function testThatMissingParameterIsDetected(): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $dataBag = [];
        $constraint = new Collection(['firstName' => new Type('string')]);
        $violations = $validator->validate($dataBag, $constraint);

        static::assertEquals(1, $violations->count());
    }

    public function testThatWrongTypeisDetected(): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $dataBag = ['isBoolen' => 'wrongTypeAsString'];
        $constraint = new Collection(['isBoolen' => new Type('bool')]);
        $violations = $validator->validate($dataBag, $constraint);

        static::assertEquals(1, $violations->count());
    }

    public function testThatCorrectTypeisDetected(): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $dataBag = ['isBoolen' => true];
        $constraint = new Collection(['isBoolen' => new Type('bool')]);
        $violations = $validator->validate($dataBag, $constraint);

        static::assertEquals(0, $violations->count());
    }

    public function testThatViolationHasCorrectMessage(): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $dataBag = [];
        $constraint = new Collection(['firstName' => new Type('string')]);
        $violations = $validator->validate($dataBag, $constraint);

        static::assertStringContainsString('firstName', $violations->__toString());
    }

    public function testThatAdditionalFieldsAreAllowed(): void
    {
        $validator = new DataValidator(Validation::createValidator());
        $dataBag = ['additionalFieldTest' => 123];
        $constraint = new Collection([
            'fields' => [],
        ]);
        $violations = $validator->validate($dataBag, $constraint);

        static::assertEquals(0, $violations->count());
    }
}
