<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Service\Validator;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use SwagGuidedShopping\Service\Validator\ViolationList;

/**
 * @internal
 * @coversDefaultClass \SwagGuidedShopping\Service\Validator\ViolationList
 */
class ViolationListTest extends TestCase
{
    public function testViolationReturnMessage(): void
    {
        $constraintViolation = new ConstraintViolation(
            'This value should be of type string.',
            'This value should be of type {{ type }}.',
            [
                '{{ value }}' => '123123',
                '{{ type }}' => 'string'
            ],
            [
                'test' => '123123'
            ],
            '[test]',
            123123,
            null,
            null,
            new Type('string'),
            null
        );
        $constraintViolationList = new ConstraintViolationList();
        $constraintViolationList->add($constraintViolation);
        $violationList = new ViolationList($constraintViolationList);
        static::assertEquals(1, $violationList->count());

        $expectedViolationList = $violationList->get(0);
        static::assertInstanceOf(ConstraintViolation::class, $expectedViolationList);
        static::assertEquals('This value should be of type string.', $expectedViolationList->getMessage());
        static::assertEquals(["{{ value }}" => "123123", "{{ type }}" => "string"], $expectedViolationList->getParameters());
        static::assertEquals(["test" => "123123"], $expectedViolationList->getRoot());
        static::assertEquals("[test]", $expectedViolationList->getPropertyPath());
        static::assertEquals(123123, $expectedViolationList->getInvalidValue());
        static::assertNull($expectedViolationList->getPlural());
        static::assertNull($expectedViolationList->getCause());
        static::assertNull($expectedViolationList->getCode());
        $constraint = $expectedViolationList->getConstraint();
        static::assertInstanceOf(Type::class, $constraint);
        static::assertNull($constraint->payload);
        static::assertEquals("This value should be of type {{ type }}.", $constraint->message);
        static::assertEquals("string", $constraint->type);
        static::assertStringContainsString("This value should be of type string. [test]\n", $violationList->__toString());
    }
}
