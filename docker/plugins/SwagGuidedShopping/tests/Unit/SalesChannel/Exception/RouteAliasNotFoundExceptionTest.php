<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\SalesChannel\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\SalesChannel\Exception\RouteAliasNotFoundException;

class RouteAliasNotFoundExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new RouteAliasNotFoundException('test-alias');
        static::assertEquals('the alias (test-alias) could not be found for this route', $exception->getMessage());
        static::assertEquals(404, $exception->getStatusCode());
        static::assertEquals('GUIDED_SHOPPING__ROUTE_ALIAS_NOT_FOUND', $exception->getErrorCode());
    }
}
