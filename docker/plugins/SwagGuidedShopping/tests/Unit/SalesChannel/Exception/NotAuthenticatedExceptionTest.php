<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\SalesChannel\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\SalesChannel\Exception\NotAuthenticatedException;

class NotAuthenticatedExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new NotAuthenticatedException('test-message');
        static::assertEquals('test-message', $exception->getMessage());
        static::assertEquals('GUIDED_SHOPPING__NOT_AUTHENTICATED', $exception->getErrorCode());
        static::assertEquals(401, $exception->getStatusCode());
    }
}
