<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\SalesChannel\Exception;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\SalesChannel\Exception\RequiredRightsNotGrantedException;

class RequiredRightsNotGrantedExceptionTest extends TestCase
{
    public function testInitialize(): void
    {
        $exception = new RequiredRightsNotGrantedException('test-message');
        static::assertEquals('test-message', $exception->getMessage());
        static::assertEquals(403, $exception->getStatusCode());
        static::assertEquals('GUIDED_SHOPPING__REQUIRED_RIGHTS_NOT_GRANTED', $exception->getErrorCode());
    }
}
