<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\SalesChannel\Listener;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\SalesChannel\Listener\GuidedShoppingContextResolverListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class GuidedShoppingContextResolverListenerTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $events = GuidedShoppingContextResolverListener::getSubscribedEvents();
        static::assertArrayHasKey('kernel.controller', $events);
        static::assertIsArray($events['kernel.controller']);
        static::assertCount(1, $events['kernel.controller']);
        static::assertSame(['resolveContext', -100], $events['kernel.controller'][0]);
    }

    public function testResolveContext(): void
    {
        $contextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);
        $contextResolver->expects(static::once())->method('resolve');
        $listener = new GuidedShoppingContextResolverListener($contextResolver);
        $event = new ControllerEvent(
            $this->createMock(HttpKernelInterface::class),
            fn () => null,
            new Request(),
            null
        );
        $listener->resolveContext($event);
    }
}
