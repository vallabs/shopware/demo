<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\SalesChannel\Service;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\SalesChannel\Exception\RequiredRightsNotGrantedException;
use SwagGuidedShopping\SalesChannel\Service\AttendeeRequestDetection;
use SwagGuidedShopping\Tests\Unit\Fake\FakeRepository;

/**
 * @coversDefaultClass \SwagGuidedShopping\SalesChannel\Service\AttendeeRequestDetection
 *
 * @internal
 */
class AttendeeRequestDetectionTest extends TestCase
{
    public function testThatIdFromTokenIsUsedIfNoAttendeeIdIsGiven(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');
        $entities = [];

        $context = new Context(new SystemSource());
        $attendeeResult = new EntitySearchResult(AttendeeEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), $context);
        /** @var EntityRepository<AttendeeCollection>&FakeRepository $attendeeRepository */
        $attendeeRepository = new FakeRepository($attendeeResult);
        $detectionService = new AttendeeRequestDetection($attendeeRepository);

        $actingAttendee = new AttendeeEntity();
        $actingAttendee->setId('attendee-1');
        $actingAttendee->setAppointmentId('appointment-1');

        $attendeeStruct = $detectionService->detectAttendee($actingAttendee, $context);
        $this->assertEquals('attendee-1', $attendeeStruct->getId());
        $this->assertEquals('appointment-1', $attendeeStruct->getAppointmentId());
    }

    public function testWithGuideTokenForTheRightAppointment(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-client');
        $attendee->setType(AttendeeDefinition::TYPE_CLIENT);
        $attendee->setAppointmentId('appointment-1');
        $entities = [$attendee];

        $context = new Context(new SystemSource());
        $attendeeResult = new EntitySearchResult(AttendeeEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), $context);
        /** @var EntityRepository<AttendeeCollection>&FakeRepository $attendeeRepository */
        $attendeeRepository = new FakeRepository($attendeeResult);
        $detectionService = new AttendeeRequestDetection($attendeeRepository);

        $actingAttendee = new AttendeeEntity();
        $actingAttendee->setId('attendee-guide');
        $actingAttendee->setType(AttendeeDefinition::TYPE_GUIDE);
        $actingAttendee->setAppointmentId('appointment-1');

        $attendeeStruct = $detectionService->detectAttendee($actingAttendee, $context, 'attendee-client');
        $this->assertEquals('attendee-client', $attendeeStruct->getId());
        $this->assertEquals('appointment-1', $attendeeStruct->getAppointmentId());
    }

    public function testWithAGuideTokenButForAOtherAppointment(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-client');
        $attendee->setAppointmentId('appointment-2');
        $entities = [$attendee];

        $context = new Context(new SystemSource());
        $attendeeResult = new EntitySearchResult(AttendeeEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), $context);
        /** @var EntityRepository<AttendeeCollection>&FakeRepository $attendeeRepository */
        $attendeeRepository = new FakeRepository($attendeeResult);
        $detectionService = new AttendeeRequestDetection($attendeeRepository);

        $actingAttendee = new AttendeeEntity();
        $actingAttendee->setId('attendee-guide');
        $actingAttendee->setType(AttendeeDefinition::TYPE_GUIDE);
        $actingAttendee->setAppointmentId('appointment-1');

        $this->expectException(RequiredRightsNotGrantedException::class);
        $this->expectExceptionMessage('you need to be in the same appointment as your client to perform this operation');
        $detectionService->detectAttendee($actingAttendee, $context, 'attendee-client');
    }

    public function testWithRightAppointmentButAClientTokenIsUsed(): void
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-client');
        $attendee->setAppointmentId('appointment-2');
        $entities = [$attendee];

        $context = new Context(new SystemSource());
        $attendeeResult = new EntitySearchResult(AttendeeEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), $context);
        /** @var EntityRepository<AttendeeCollection>&FakeRepository $attendeeRepository */
        $attendeeRepository = new FakeRepository($attendeeResult);
        $detectionService = new AttendeeRequestDetection($attendeeRepository);

        $actingAttendee = new AttendeeEntity();
        $actingAttendee->setId('attendee-acting-client');
        $actingAttendee->setType(AttendeeDefinition::TYPE_CLIENT);
        $actingAttendee->setAppointmentId('appointment-2');

        $this->expectException(RequiredRightsNotGrantedException::class);
        $this->expectExceptionMessage('you need to be a guide to perform this operation');
        $detectionService->detectAttendee($actingAttendee, $context, 'attendee-client');
    }

    public function testWithNotExsistingAttendee(): void
    {
        $entities = [];
        $context = new Context(new SystemSource());
        $attendeeResult = new EntitySearchResult(AttendeeEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), $context);
        /** @var EntityRepository<AttendeeCollection>&FakeRepository $attendeeRepository */
        $attendeeRepository = new FakeRepository($attendeeResult);
        $detectionService = new AttendeeRequestDetection($attendeeRepository);

        $actingAttendee = new AttendeeEntity();
        $actingAttendee->setId('attendee-guide');
        $actingAttendee->setType(AttendeeDefinition::TYPE_GUIDE);
        $actingAttendee->setAppointmentId('appointment-2');

        $this->expectException(AttendeeNotFoundException::class);
        $detectionService->detectAttendee($actingAttendee, $context, 'attendee-unknown');
    }
}
