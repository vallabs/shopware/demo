<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Fake;

use Psr\Cache\CacheItemInterface;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\Cache\CacheItem;

class TagawareAdapterFake implements TagAwareAdapterInterface
{
    /**
     * @var array<int|string, mixed>
     */
    private array $items = [];

    public function fakeSetItem(CacheItemInterface $cacheItem, string $key): void
    {
        $this->setAsHit($cacheItem);
        $this->items[$key] = $cacheItem;
    }

    /**
     * @param mixed $key
     */
    public function getItem(mixed $key): CacheItem
    {
        if (\array_key_exists($key, $this->items)) {
            return $this->items[$key];
        }

        $item = new CacheItem();
        $class = new \ReflectionClass(CacheItem::class);
        $keyProperty = $class->getProperty('key');
        $keyProperty->setAccessible(true);
        $keyProperty->setValue($item, $key);

        return $item;
    }

    public function hasItem(string $key): bool
    {
        return \array_key_exists($key, $this->items);
    }

    /**
     * @param array<string> $keys
     */
    public function getItems(array $keys = []): iterable
    {
        foreach ($keys as $key) {
            yield $key => $this->items[$key];
        }
    }

    public function clear(string $prefix = ''): bool
    {
        $this->items = [];

        return true;
    }

    public function deleteItem(string $key): bool
    {
        unset($this->items[$key]);

        return true;
    }

    /**
     * @param array<string> $keys
     */
    public function deleteItems(array $keys): bool
    {
        foreach ($keys as $key) {
            unset($this->items[$key]);
        }

        return true;
    }

    public function save(CacheItemInterface $item): bool
    {
        $this->items[$item->getKey()] = $item;

        return true;
    }

    public function saveDeferred(CacheItemInterface $item): bool
    {
        return $this->save($item);
    }

    public function commit(): bool
    {
        return true;
    }

    /**
     * @param array<string> $tags
     */
    public function invalidateTags(array $tags): bool
    {
        return true;
    }

    private function setAsHit(CacheItemInterface $item): void
    {
        $class = new \ReflectionClass(CacheItem::class);
        $keyProperty = $class->getProperty('isHit');
        $keyProperty->setAccessible(true);
        $keyProperty->setValue($item, true);
    }
}
