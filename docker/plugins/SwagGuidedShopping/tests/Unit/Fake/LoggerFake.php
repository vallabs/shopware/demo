<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Fake;

use Psr\Log\LoggerInterface;

class LoggerFake implements LoggerInterface
{
    /** @var array<int, mixed> $criticals */
    public array $criticals = [];

    /** @var array<int, mixed> $infos */
    public array $infos = [];

    /**
     * @param array<int|string, mixed> $context
     */
    public function emergency(string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function alert(string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function critical(string|\Stringable $message, array $context = []): void
    {
        $this->criticals[] = ['message' => $message, 'context' => $context];
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function error(string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function warning(string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function notice(string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function info(string|\Stringable $message, array $context = []): void
    {
        $this->infos[] = ['message' => $message, 'context' => $context];
    }

    /**
     * @param array<int|string, mixed> $context
     */
    public function debug(string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }

    /**
     * @param mixed $level
     * @param array<int|string, mixed> $context
     */
    public function log(mixed $level, string|\Stringable $message, array $context = []): void
    {
        throw new \Exception('not implemented');
    }
}
