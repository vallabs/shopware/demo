<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Fake;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\AbstractInteractionHandler;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;

class FakeInteractionHandler extends AbstractInteractionHandler
{
    public function supports(Interaction $interaction): bool
    {
        return $interaction->getName() === 'fake_interaction';
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
    }
}
