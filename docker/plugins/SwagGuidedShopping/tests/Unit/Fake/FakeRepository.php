<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Fake;

use Shopware\Core\Checkout\Document\DocumentDefinition;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenContainerEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\AggregationResultCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Write\CloneBehavior;
use Shopware\Core\Framework\Event\NestedEventCollection;

class FakeRepository extends EntityRepository
{
    /**
     * @var array<string, mixed>
     */
    public array $data = [];

    /**
     * @var array<int, array<string, mixed>>
     */
    public array $writtenData = [];

    /**
     * @var array<int|string, mixed>
     */
    public array $upserts = [];

    /**
     * @var array<int|string, mixed>
     */
    public array $upsertsCurrent = [];

    /**
     * @var array<array<string, mixed>>
     */
    public array $delete = [];

    /**
     * @var array<string, mixed>|null
     */
    public ?array $searchIdsResult = null;

    public EntitySearchResult $searchResult;

    public function __construct(EntitySearchResult $searchResult)
    {
        $this->searchResult = $searchResult;
    }

    public function getDefinition(): EntityDefinition
    {
        return new DocumentDefinition();
    }

    public function searchIds(Criteria $criteria, Context $context): IdSearchResult
    {
        $data = [];
        foreach ($this->searchResult->getIds() as $id) {
            $data[] = ['primaryKey' => $id, 'data' => []];
        }

        return new IdSearchResult($this->searchResult->getTotal(), $data, $criteria, $context);
    }

    public function clone(string $id, Context $context, ?string $newId = null, ?CloneBehavior $behavior = null): EntityWrittenContainerEvent
    {
        return new EntityWrittenContainerEvent($context, new NestedEventCollection(), []);
    }

    public function search(Criteria $criteria, Context $context): EntitySearchResult
    {
        return $this->searchResult;
    }

    /**
     * @param array<string, mixed> $data
     */
    public function upsert(array $data, Context $context): EntityWrittenContainerEvent
    {
        foreach ($data as $dataset) {
            if (\array_key_exists('id', $dataset)) {
                $key = $dataset['id'];
            } else {
                $key = \json_encode($dataset);
                if ($key === false) {
                    throw new \Exception('need a valid array');
                }
                $key = \md5($key);
            }
            $this->upserts[$key][] = $dataset;
            $this->upsertsCurrent[$key] = $dataset;
            $this->writtenData[] = $dataset;
        }

        return new EntityWrittenContainerEvent($context, new NestedEventCollection([]), []);
    }

    /**
     * @param array<string, mixed> $data
     */
    public function create(array $data, Context $context): EntityWrittenContainerEvent
    {
        foreach ($data as $dataSet) {
            $this->writtenData[] = $dataSet;
        }

        return new EntityWrittenContainerEvent($context, new NestedEventCollection(), []);
    }

    /**
     * @param array<string, mixed> $ids
     */
    public function delete(array $ids, Context $context): EntityWrittenContainerEvent
    {
        $this->delete[] = $ids;
        return new EntityWrittenContainerEvent($context, new NestedEventCollection(), []);
    }

    public function createVersion(string $id, Context $context, ?string $name = null, ?string $versionId = null): string
    {
        return '';
    }

    public function merge(string $versionId, Context $context): void
    {
    }

    /**
     * @param array<string, mixed> $data
     */
    public function update(array $data, Context $context): EntityWrittenContainerEvent
    {
        $this->data = $data;

        return new EntityWrittenContainerEvent($context, new NestedEventCollection(), []);
    }

    public function aggregate(Criteria $criteria, Context $context): AggregationResultCollection
    {
        return new AggregationResultCollection([]);
    }

    /**
     * @return array<int|string, mixed>
     */
    public function getCurrentUpserts(): array
    {
        return $this->upsertsCurrent;
    }

    /**
     * @return array<int, mixed>
     */
    public function getDelete(): array
    {
        return $this->delete;
    }
}
