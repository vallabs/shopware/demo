<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Fake;

use SwagGuidedShopping\Content\PresentationState\State\AbstractPresentationState;

class FakeState extends AbstractPresentationState
{
    /** @gs\mercure-update */
    protected string $exposeMe = 'test';

    /**
     * @return array<string, mixed>
     */
    public function getMercureUpdatePayload(): array
    {
        return ['fake-state' => $this->getData()];
    }

    public function setExposeMe(string $exposeMe): void
    {
        $this->exposeMe = $exposeMe;
    }

    public function getExposeMe(): string
    {
        return $this->exposeMe;
    }
}
