<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Helpers;

use Shopware\Core\Framework\Api\Context\ContextSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\CashRoundingConfig;
use Shopware\Core\System\SalesChannel\SalesChannelEntity;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\Currency\CurrencyEntity;
use Shopware\Core\System\Country\CountryEntity;
use Shopware\Core\System\Tax\TaxCollection;
use Shopware\Core\Checkout\Customer\Aggregate\CustomerGroup\CustomerGroupEntity;
use Shopware\Core\Checkout\Cart\Delivery\Struct\ShippingLocation;
use Shopware\Core\Checkout\Shipping\ShippingMethodEntity;
use Shopware\Core\Checkout\Payment\PaymentMethodEntity;
use Shopware\Core\Checkout\Customer\CustomerEntity;

class SalesChannelContextHelper
{
    /**
     * @param array<int|string, mixed>|null $rulesIds
     */
    public function createSalesChannelContext(
        string $token = null,
        ?CustomerEntity $customer = null,
        ?ContextSource $contextSource = null,
        ?array $rulesIds = []
    ): SalesChannelContext
    {
        $context = new Context($contextSource ?? new SystemSource());
        if ($rulesIds) {
            $context->setRuleIds($rulesIds);
        }
        $rounding = new CashRoundingConfig(2, 1, false);
        $token = $token ?: Uuid::randomHex();

        $salesChannelEntity = new SalesChannelEntity();
        $salesChannelEntity->assign([
            'id' => $token,
            'name' => 'test-sales-channel-name',
            'translated' => [
                'name' => 'test-sales-channel-name',
            ],
        ]);

        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');

        return new SalesChannelContext(
            $context,
            $token,
            null,
            $salesChannelEntity,
            $currency,
            new CustomerGroupEntity(),
            new TaxCollection(),
            new PaymentMethodEntity(),
            new ShippingMethodEntity(),
            new ShippingLocation(new CountryEntity(), null, null),
            $customer,
            $rounding,
            $rounding
        );
    }
}
