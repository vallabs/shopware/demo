<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\System\SalesChannel\Aggregate\SalesChannelDomain;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainExtension;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class SalesChannelDomainExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $fieldCollection = new FieldCollection();
        $salesChannelDomainExtension = new SalesChannelDomainExtension();
        $salesChannelDomainExtension->extendFields($fieldCollection);
        // Check the fields count
        $elements = $fieldCollection->getElements();
        static::assertCount(1, $elements);
        /** @var AssociationField $element */
        $element = $elements[0];
        static::assertInstanceOf(OneToManyAssociationField::class, $element);
        static::assertEquals('guidedShoppingAppointmentGuideSalesChannelDomain', $element->getPropertyName());
        static::assertEquals(AppointmentDefinition::class, $element->getReferenceClass());
        static::assertEquals('sales_channel_domain_id', $element->getReferenceField());
        static::assertNotNull($element->getFlag(CascadeDelete::class));
    }

    public function testGetDefinitionClass(): void
    {
        $salesChannelDomainExtension = new SalesChannelDomainExtension();
        static::assertEquals(SalesChannelDomainDefinition::class, $salesChannelDomainExtension->getDefinitionClass());
    }
}
