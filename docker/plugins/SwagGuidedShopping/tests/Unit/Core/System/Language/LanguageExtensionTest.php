<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\System\Language;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\Language\LanguageDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Core\System\Language\LanguageExtension;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationDefinition;


class LanguageExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $fieldCollection = new FieldCollection();
        $languageExtension = new LanguageExtension();
        $languageExtension->extendFields($fieldCollection);
        // Check the fields count
        $elements = $fieldCollection->getElements();
        static::assertCount(1, $elements);
        /** @var AssociationField $element */
        $element = $elements[0];
        static::assertInstanceOf(OneToManyAssociationField::class, $element);
        static::assertEquals('presentationTranslation', $element->getPropertyName());
        static::assertEquals(PresentationTranslationDefinition::class, $element->getReferenceClass());
        static::assertEquals('language_id', $element->getReferenceField());
        static::assertNotNull($element->getFlag(CascadeDelete::class));
    }

    public function testGetDefinitionClass(): void
    {
        $languageExtension = new LanguageExtension();
        static::assertEquals(LanguageDefinition::class, $languageExtension->getDefinitionClass());
    }
}
