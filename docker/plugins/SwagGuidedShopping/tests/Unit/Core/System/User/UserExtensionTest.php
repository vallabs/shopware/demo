<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\System\User;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\User\UserDefinition;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use SwagGuidedShopping\Core\System\User\UserExtension;
use SwagGuidedShopping\Content\Presentation\PresentationDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class UserExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $fieldCollection = new FieldCollection();
        $userExtension = new UserExtension();
        $userExtension->extendFields($fieldCollection);
        // Check the fields count
        $elements = $fieldCollection->getElements();
        static::assertCount(6, $elements);

        /** @var AssociationField $firstElement */
        $firstElement = $elements[0];
        static::assertInstanceOf(OneToManyAssociationField::class, $firstElement);
        static::assertEquals('guidedShoppingAppointmentGuideUser', $firstElement->getPropertyName());
        static::assertEquals(AppointmentDefinition::class, $firstElement->getReferenceClass());
        static::assertEquals('guide_user_id', $firstElement->getReferenceField());
        static::assertNotNull($firstElement->getFlag(ApiAware::class));
        static::assertNotNull($firstElement->getFlag(CascadeDelete::class));

        /** @var AssociationField $secondElement */
        $secondElement = $elements[1];
        static::assertInstanceOf(OneToManyAssociationField::class, $secondElement);
        static::assertEquals('createdPresentations', $secondElement->getPropertyName());
        static::assertEquals(PresentationDefinition::class, $secondElement->getReferenceClass());
        static::assertEquals('created_by_id', $secondElement->getReferenceField());
        static::assertEquals('id', $secondElement->getLocalField());
        static::assertNotNull($secondElement->getFlag(ApiAware::class));
        static::assertNotNull($secondElement->getFlag(CascadeDelete::class));

        /** @var AssociationField $thirdElement */
        $thirdElement = $elements[2];
        static::assertInstanceOf(OneToManyAssociationField::class, $thirdElement);
        static::assertEquals('updatedPresentations', $thirdElement->getPropertyName());
        static::assertEquals(PresentationDefinition::class, $thirdElement->getReferenceClass());
        static::assertEquals('updated_by_id', $thirdElement->getReferenceField());
        static::assertEquals('id', $thirdElement->getLocalField());
        static::assertNotNull($thirdElement->getFlag(ApiAware::class));
        static::assertNotNull($thirdElement->getFlag(CascadeDelete::class));

        /** @var AssociationField $fourthElement */
        $fourthElement = $elements[3];
        static::assertInstanceOf(OneToManyAssociationField::class, $fourthElement);
        static::assertEquals('createdAppointments', $fourthElement->getPropertyName());
        static::assertEquals(AppointmentDefinition::class, $fourthElement->getReferenceClass());
        static::assertEquals('created_by_id', $fourthElement->getReferenceField());
        static::assertEquals('id', $fourthElement->getLocalField());
        static::assertNotNull($fourthElement->getFlag(ApiAware::class));
        static::assertNotNull($fourthElement->getFlag(CascadeDelete::class));

        /** @var AssociationField $fifthElement */
        $fifthElement = $elements[4];
        static::assertInstanceOf(OneToManyAssociationField::class, $fifthElement);
        static::assertEquals('updatedAppointments', $fifthElement->getPropertyName());
        static::assertEquals(AppointmentDefinition::class, $fifthElement->getReferenceClass());
        static::assertEquals('updated_by_id', $fifthElement->getReferenceField());
        static::assertEquals('id', $fifthElement->getLocalField());
        static::assertNotNull($fifthElement->getFlag(ApiAware::class));
        static::assertNotNull($fifthElement->getFlag(CascadeDelete::class));

        /** @var AssociationField $sixthElement */
        $sixthElement = $elements[5];
        static::assertInstanceOf(OneToManyAssociationField::class, $sixthElement);
        static::assertEquals('createdLayouts', $sixthElement->getPropertyName());
        static::assertEquals(CmsPageDefinition::class, $sixthElement->getReferenceClass());
        static::assertEquals('created_by_id', $sixthElement->getReferenceField());
        static::assertEquals('id', $sixthElement->getLocalField());
        static::assertNotNull($sixthElement->getFlag(ApiAware::class));
        static::assertNotNull($sixthElement->getFlag(CascadeDelete::class));
    }

    public function testGetDefinitionClass(): void
    {
        $userExtension = new UserExtension();
        static::assertEquals(UserDefinition::class, $userExtension->getDefinitionClass());
    }
}
