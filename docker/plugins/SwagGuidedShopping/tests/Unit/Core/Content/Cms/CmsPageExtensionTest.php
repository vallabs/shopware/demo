<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Content\Cms;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use SwagGuidedShopping\Core\Content\Cms\CmsPageExtension;
use Shopware\Core\System\User\UserDefinition;

class CmsPageExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $fieldCollection = new FieldCollection();
        $cmsPageExtension = new CmsPageExtension();
        $cmsPageExtension->extendFields($fieldCollection);
        // Check the fields count
        $elements = $fieldCollection->getElements();
        static::assertCount(2, $elements);

        $firstElement = $elements[0];
        static::assertInstanceOf(FkField::class, $firstElement);
        static::assertEquals('createdById', $firstElement->getPropertyName());
        static::assertEquals('created_by_id', $firstElement->getStorageName());
        static::assertEquals('id', $firstElement->getReferenceField());
        static::assertNotNull($firstElement->getFlag(ApiAware::class));
        static::assertNotNull($firstElement->getFlag(Inherited::class));

        /** @var AssociationField $secondElement */
        $secondElement = $elements[1];
        static::assertInstanceOf(ManyToOneAssociationField::class, $secondElement);
        static::assertEquals('createdBy', $secondElement->getPropertyName());
        static::assertEquals(UserDefinition::class, $secondElement->getReferenceClass());
        static::assertEquals('id', $secondElement->getReferenceField());
        static::assertNotNull($secondElement->getFlag(ApiAware::class));
    }

    public function testGetDefinitionClass(): void
    {
        $cmsPageExtension = new CmsPageExtension();
        static::assertEquals(CmsPageDefinition::class, $cmsPageExtension->getDefinitionClass());
    }
}
