<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Checkout\Cart\Struct;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\CashRoundingConfig;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class AttendeeCartTest extends TestCase
{
    public function testAddItem(): void
    {
        $attendeeCart = $this->getAttendeeCart();
        $item = new AttendeeCartLineItem('test-id', 1.3, 5);
        $attendeeCart->addItem($item);
        static::assertSame($item, $attendeeCart->getItems()['test-id']);
    }

    public function testGetAttendeeId(): void
    {
        $attendeeCart = $this->getAttendeeCart();
        static::assertEquals('test-attendee-id', $attendeeCart->getAttendeeId());
    }

    public function testGetPriceNet(): void
    {
        $item = new AttendeeCartLineItem('test-id', 1.24734, 5);

        $currency = new CurrencyEntity();
        $currency->setId('test-currency-id');
        $itemRoundingConfig = new CashRoundingConfig(2, 0.01, true);
        $currency->setItemRounding($itemRoundingConfig);
        $totalRoundingConfig = new CashRoundingConfig(1, 0.01, true);
        $currency->setTotalRounding($totalRoundingConfig);

        $cashRounding = $this->createMock(CashRounding::class);
        $cashRounding->expects(static::exactly(2))
            ->method('cashRound')
            ->willReturnOnConsecutiveCalls(1.25, 1.3);

        $attendeeCart = $this->getAttendeeCart($cashRounding);
        $attendeeCart->addItem($item);
        $priceNet = $attendeeCart->getPriceNet($currency);
        static::assertEquals(1.3, $priceNet);
    }

    public function testGetProductCount(): void
    {
        $item = new AttendeeCartLineItem('test-item-id', 1.24734, 5);
        $item2 = new AttendeeCartLineItem('test-item2-id', 1.24734, 6);
        $item3 = new AttendeeCartLineItem('test-item3-id', 1.24734, 2);

        $attendeeCart = $this->getAttendeeCart();
        $attendeeCart->addItem($item);
        $attendeeCart->addItem($item2);
        $attendeeCart->addItem($item3);

        $productCount = $attendeeCart->getProductCount();
        static::assertEquals(13, $productCount);
    }

    private function getAttendeeCart(
        ?MockObject $cashRoundingMock = null
    ): AttendeeCart
    {
        /** @var MockObject&CashRounding $cashRounding */
        $cashRounding = $cashRoundingMock ?: $this->createMock(CashRounding::class);
        return new AttendeeCart($cashRounding, 'test-attendee-id');
    }
}
