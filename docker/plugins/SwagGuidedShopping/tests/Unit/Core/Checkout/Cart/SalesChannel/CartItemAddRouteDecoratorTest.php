<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Checkout\Cart\SalesChannel;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\Checkout\Cart\SalesChannel\AbstractCartItemAddRoute;
use Shopware\Core\Checkout\Cart\LineItemFactoryRegistry;
use Shopware\Core\Checkout\Cart\SalesChannel\CartResponse;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Core\Checkout\Cart\SalesChannel\CartItemAddRouteDecorator;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class CartItemAddRouteDecoratorTest extends TestCase
{
    protected SalesChannelContext $salesChannelContext;
    protected CartResponse $cartResponse;

    protected function setUp(): void
    {
        parent::setUp();
        $this->salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $this->cartResponse = new CartResponse(new Cart('test'));
    }

    public function testGetDecorated(): void
    {
        $decorator = $this->getDecorator();
        $decorator->getDecorated();
        static::expectNotToPerformAssertions();
    }

    public function testAddPassAllConditionsWithItems(): void
    {
        $request = new Request();
        $items = [
            new LineItem('test1', 'product', 'test1', 1),
            new LineItem('test2', 'product', 'test2', 1),
        ];
        $attendee = $this->getValidAttendee();
        $this->salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        $guidedShoppingContextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);
        $guidedShoppingContextResolver
            ->expects(static::once())
            ->method('loadAttendee')
            ->with($this->salesChannelContext)
            ->willReturn($attendee);

        $lineItemFactoryRegistry = $this->createMock(LineItemFactoryRegistry::class);
        $lineItemFactoryRegistry
            ->expects(static::never())
            ->method('create');

        $decorated = $this->createMock(AbstractCartItemAddRoute::class);
        $decorated
            ->expects(static::once())
            ->method('add')
            ->willReturn($this->cartResponse);

        $decorator = $this->getDecorator($decorated, $guidedShoppingContextResolver, $lineItemFactoryRegistry);

        $decorator->add($request, new Cart('test'), $this->salesChannelContext, $items);

        static::assertEquals(
            true,
            $items[0]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER)
        );
        static::assertEquals(
            true,
            $items[1]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER)
        );
        static::assertEquals(
            $attendee->getAppointmentId(),
            $items[0]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_APPOINTMENT_ID)
        );
        static::assertEquals(
            $attendee->getAppointmentId(),
            $items[1]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_APPOINTMENT_ID)
        );

    }

    public function testAddPassAllConditionsWithoutItems(): void
    {
        $request = new Request([], [
            'items' => [
                ['type' => 'product', 'referencedId' => 'test1', 'quantity' => 1, 'id' => 'test1'],
                ['type' => 'product', 'referencedId' => 'test2', 'quantity' => 1, 'id' => 'test2']
            ]
        ]);
        $actualItems = null;
        $attendee = $this->getValidAttendee();
        $this->salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        $guidedShoppingContextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);
        $guidedShoppingContextResolver
            ->expects(static::once())
            ->method('loadAttendee')
            ->with($this->salesChannelContext)
            ->willReturn($attendee);

        $lineItemFactoryRegistry = $this->createMock(LineItemFactoryRegistry::class);
        $expectedItems = [
            new LineItem('test1', 'product', 'test1', 1),
            new LineItem('test2', 'product', 'test2', 1),
        ];
        $lineItemFactoryRegistry
            ->expects(static::exactly(2))
            ->method('create')
            ->willReturnCallback(function() use ($expectedItems) {
                static $index = 0;
                return $expectedItems[$index++];
            });

        $decorated = $this->createMock(AbstractCartItemAddRoute::class);
        $decorated
            ->expects(static::once())
            ->method('add')
            ->willReturn($this->cartResponse);

        $decorator = $this->getDecorator($decorated, $guidedShoppingContextResolver, $lineItemFactoryRegistry);

        $decorator->add($request, new Cart('test'), $this->salesChannelContext, $actualItems);

        static::assertEquals(
            true,
            $expectedItems[0]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER)
        );
        static::assertEquals(
            true,
            $expectedItems[1]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER)
        );
        static::assertEquals(
            $attendee->getAppointmentId(),
            $expectedItems[0]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_APPOINTMENT_ID)
        );
        static::assertEquals(
            $attendee->getAppointmentId(),
            $expectedItems[1]->getPayloadValue($decorator::GUIDED_SHOPPING_ADD_TO_CART_APPOINTMENT_ID)
        );

    }

    private function getValidAttendee(): AttendeeEntity
    {
        $now = new \DateTime();
        $attendee = new AttendeeEntity();
        $attendee->setId('attendee-1');

        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setPresentationId('test-presentation-id');
        $appointment->setActive(true);
        $appointment->setMode(AppointmentDefinition::MODE_SELF);
        $appointment->setAccessibleTo($now->modify('+1 day'));
        $attendee->setAppointment($appointment);

        return $attendee;
    }

    public function testAddWithExceptionWontReturnCartResponse(): void
    {
        $guidedShoppingContextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);
        $guidedShoppingContextResolver
            ->expects(static::once())
            ->method('loadAttendee')
            ->with($this->salesChannelContext)
            ->willThrowException(new \Exception());

        $decorated = $this->createMock(AbstractCartItemAddRoute::class);
        $decorated->expects(static::once())->method('add');

        $lineItemFactoryRegistry = $this->createMock(LineItemFactoryRegistry::class);
        $lineItemFactoryRegistry->expects(static::never())->method('create');

        $decorator = $this->getDecorator($decorated, $guidedShoppingContextResolver, $lineItemFactoryRegistry);
        $decorator->add(new Request(), new Cart('test'), $this->salesChannelContext, []);
    }

    /**
     * @dataProvider getAppointmentData
     *
     * @param AttendeeEntity $attendee
     */
    public function testAddReturnFalseWithSomeAppointmentConditions(AttendeeEntity $attendee): void
    {
        $items = [
            new LineItem('test1', 'product', 'test1', 1),
            new LineItem('test2', 'product', 'test2', 1),
        ];

        $this->salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        $guidedShoppingContextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);
        $guidedShoppingContextResolver
            ->expects(static::once())
            ->method('loadAttendee')
            ->with($this->salesChannelContext)
            ->willReturn($attendee);

        $decorated = $this->createMock(AbstractCartItemAddRoute::class);
        $decorated
            ->expects(static::once())
            ->method('add')
            ->willReturn($this->cartResponse);

        $lineItemFactoryRegistry = $this->createMock(LineItemFactoryRegistry::class);
        $lineItemFactoryRegistry->expects(static::never())->method('create');

        $decorator = $this->getDecorator($decorated, $guidedShoppingContextResolver, $lineItemFactoryRegistry);
        $decorator->add(new Request(), new Cart('test'), $this->salesChannelContext, $items);

        static::assertEmpty($items[0]->getPayload());
        static::assertEmpty($items[1]->getPayload());
    }

    public static function getAppointmentData(): \Generator
    {
        $attendee1 = new AttendeeEntity();
        $attendee1->setId('attendee-1');
        $appointment1 = (new AppointMentMockHelper())->getAppointEntity();
        $appointment1->setPresentationId('test-presentation-id');
        $appointment1->setActive(false);
        $attendee1->setAppointment($appointment1);
        yield 'appointment-is-not-active' => [
            'attendee' => $attendee1,
        ];

        $now = new \DateTime();
        $attendee2 = new AttendeeEntity();
        $attendee2->setId('attendee-2');
        $appointment2 = (new AppointMentMockHelper())->getAppointEntity();
        $appointment2->setPresentationId('test-presentation-id');
        $appointment2->setActive(true);
        $appointment2->setEndedAt($now->modify('-1 day'));
        $attendee2->setAppointment($appointment2);
        yield 'appointment-is-ended' => [
            'attendee' => $attendee2,
        ];

        $attendee3 = new AttendeeEntity();
        $attendee3->setId('atendee-3');
        $appointment3 = (new AppointMentMockHelper())->getAppointEntity();
        $appointment3->setPresentationId('test-presentation-id');
        $appointment3->setActive(true);
        $appointment3->setMode(AppointmentDefinition::MODE_SELF);
        $appointment3->setAccessibleTo($now->modify('-1 day'));
        $attendee3->setAppointment($appointment3);
        yield 'appointment-is-not-accessible' => [
            'attendee' => $attendee3,
        ];
    }

    private function getDecorator(
        ?MockObject $decoratedMock = null,
        ?MockObject $guidedShoppingContextResolverMock = null,
        ?MockObject $lineItemFactoryRegistryMock = null
    ): CartItemAddRouteDecorator
    {
        /** @var MockObject&AbstractCartItemAddRoute $decorated */
        $decorated = $decoratedMock ?: $this->createMock(AbstractCartItemAddRoute::class);
        /** @var MockObject&GuidedShoppingRequestContextResolver $guidedShoppingContextResolver */
        $guidedShoppingContextResolver = $guidedShoppingContextResolverMock ?: $this->createMock(GuidedShoppingRequestContextResolver::class);
        /** @var MockObject&LineItemFactoryRegistry $lineItemFactoryRegistry */
        $lineItemFactoryRegistry = $lineItemFactoryRegistryMock ?: $this->createMock(LineItemFactoryRegistry::class);

        return new CartItemAddRouteDecorator($decorated, $guidedShoppingContextResolver, $lineItemFactoryRegistry);
    }
}
