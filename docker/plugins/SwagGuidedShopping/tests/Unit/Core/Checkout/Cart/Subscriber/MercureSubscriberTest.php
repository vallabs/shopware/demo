<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Checkout\Cart\Subscriber;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\Event\CartChangedEvent;
use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedCriteriaEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Mercure\Service\AbstractPublisher;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Core\Checkout\Cart\Subscriber\MercureSubscriber;
use SwagGuidedShopping\Framework\Routing\AttendeeNotFoundException;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;

class MercureSubscriberTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $subscriber = $this->getSubscriber();
        $subscriberEvents = $subscriber::getSubscribedEvents();
        $expectSubscriberEvents = [
            CheckoutOrderPlacedCriteriaEvent::class => 'sendMercureEventForOrder',
            CartChangedEvent::class => 'sendMercureEventForLineItem'
        ];
        static::assertEquals($expectSubscriberEvents, $subscriberEvents);
    }

    /**
     * @dataProvider getTestSendMercureEventProviderData
     */
    public function testSendMercureEventForOrder(
        MockObject $contextResolver,
        MockObject $mercurePublisher,
        MockObject $jwtMercureTokenFactory,
        MockObject $topicGenerator,
        SalesChannelContext $salesChannelContext
    ): void
    {
        $subscriber = $this->getSubscriber(
            $contextResolver,
            $mercurePublisher,
            $jwtMercureTokenFactory,
            $topicGenerator
        );
        $criteria = new Criteria();
        $event = new CheckoutOrderPlacedCriteriaEvent($criteria, $salesChannelContext);
        $subscriber->sendMercureEventForOrder($event);
    }

    /**
     * @dataProvider getTestSendMercureEventProviderData
     */
    public function testSendMercureEventForLineItem(
        MockObject $contextResolver,
        MockObject $mercurePublisher,
        MockObject $jwtMercureTokenFactory,
        MockObject $topicGenerator,
        SalesChannelContext $salesChannelContext
    ): void
    {
        $subscriber = $this->getSubscriber(
            $contextResolver,
            $mercurePublisher,
            $jwtMercureTokenFactory,
            $topicGenerator
        );
        $event = new CartChangedEvent(new Cart('test-name'), $salesChannelContext);
        $subscriber->sendMercureEventForLineItem($event);
    }

    public static function getTestSendMercureEventProviderData(): \Generator
    {
        $self = new MercureSubscriberTest('test');

        $salesChannelContextHelper = new SalesChannelContextHelper();
        $salesChannelContext = $salesChannelContextHelper->createSalesChannelContext();

        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setType(AttendeeDefinition::TYPE_GUIDE);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $attendee->setAppointmentId($appointment->getId());
        $attendee->setAppointment($appointment);

        yield 'not-found-attendee' => [
            $self->createContextResolverMock(true, $attendee, $salesChannelContext),
            $self->createMercurePublisherMock(false),
            $self->createJWTMercureTokenFactoryMock(false),
            $self->createTopicGeneratorMock(false),
            $salesChannelContext
        ];

        yield 'found-attendee-but-generate-empty-topic' => [
            $self->createContextResolverMock(false, $attendee, $salesChannelContext),
            $self->createMercurePublisherMock(false),
            $self->createJWTMercureTokenFactoryMock(false),
            $self->createTopicGeneratorMock(true, $appointment->getId(), $appointment->getMode(), null),
            $salesChannelContext
        ];

        yield 'found-attendee-is-guide-and-generate-valid-topic' => [
            $self->createContextResolverMock(false, $attendee, $salesChannelContext),
            $self->createMercurePublisherMock(true, 'test-token', 'guide.changedCart', $attendee->getId(), 'test-topic'),
            $self->createJWTMercureTokenFactoryMock(true, 'test-topic', 'test-token'),
            $self->createTopicGeneratorMock(true, $appointment->getId(), $appointment->getMode(), 'test-topic'),
            $salesChannelContext
        ];

        $attendee2 = clone $attendee;
        $attendee2->setType('test-type');
        yield 'found-attendee-is-client-and-generate-valid-topic' => [
            $self->createContextResolverMock(false, $attendee2, $salesChannelContext),
            $self->createMercurePublisherMock(true, 'test-token',  'client.changedCart', $attendee2->getId(), 'test-topic'),
            $self->createJWTMercureTokenFactoryMock(true, 'test-topic', 'test-token'),
            $self->createTopicGeneratorMock(true, $appointment->getId(), $appointment->getMode(), 'test-topic'),
            $salesChannelContext
        ];
    }

    private function getSubscriber(
        ?MockObject $contextResolverMock = null,
        ?MockObject $mercurePublisherMock = null,
        ?MockObject $jwtMercureTokenFactoryMock = null,
        ?MockObject $topicGeneratorMock = null
    ): MercureSubscriber
    {
        /** @var MockObject&GuidedShoppingRequestContextResolver $contextResolver */
        $contextResolver = $contextResolverMock ?? $this->createMock(GuidedShoppingRequestContextResolver::class);
        /** @var MockObject&AbstractPublisher $mercurePublisher */
        $mercurePublisher = $mercurePublisherMock ?? $this->createMock(AbstractPublisher::class);
        /** @var MockObject&JWTMercureTokenFactory $jwtMercureTokenFactory */
        $jwtMercureTokenFactory = $jwtMercureTokenFactoryMock ?? $this->createMock(JWTMercureTokenFactory::class);
        /** @var MockObject&TopicGenerator $topicGenerator */
        $topicGenerator = $topicGeneratorMock ?? $this->createMock(TopicGenerator::class);

        return new MercureSubscriber(
            $contextResolver,
            $mercurePublisher,
            $jwtMercureTokenFactory,
            $topicGenerator
        );
    }

    private function createContextResolverMock(
        bool $isThrownException,
        AttendeeEntity $attendee,
        SalesChannelContext $salesChannelContext
    ): MockObject
    {
        $contextResolver = $this->createMock(GuidedShoppingRequestContextResolver::class);

        if ($isThrownException) {
            $contextResolver->expects(static::once())
                ->method('loadAttendee')
                ->with(
                    static::equalTo($salesChannelContext)
                )
                ->willThrowException(new AttendeeNotFoundException($attendee->getId()));
        } else {
            $contextResolver->expects(static::once())
                ->method('loadAttendee')
                ->with(
                    static::equalTo($salesChannelContext)
                )
                ->willReturn($attendee);
        }

        return $contextResolver;
    }

    private function createTopicGeneratorMock(
        bool $isRun,
        ?string $appointmentId = null,
        ?string $appointmentMode = null,
        ?string $expectTopic = null
    ): MockObject
    {
        $topicGenerator = $this->createMock(TopicGenerator::class);

        if ($isRun) {
            $topicGenerator->expects(static::once())
                ->method('generateGuidePublisherTopic')
                ->with(
                    static::equalTo($appointmentId),
                    static::equalTo($appointmentMode)
                )
                ->willReturn($expectTopic);
        } else {
            $topicGenerator->expects(static::never())->method('generateGuidePublisherTopic');
        }

        return $topicGenerator;
    }

    private function createJWTMercureTokenFactoryMock(
        bool $isRun,
        ?string $topic = null,
        ?string $expectToken = null
    ): MockObject
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService->expects(static::exactly(2))
            ->method('getString')
            ->willReturnOnConsecutiveCalls('test-publisher', 'test-subscriber');

        $jwtMercureTokenFactory = new JWTMercureTokenFactory($systemConfigService);

        $jwtMercureTokenFactoryMock = $this->createMock(\get_class($jwtMercureTokenFactory));

        if ($isRun && $topic && $expectToken) {
            $jwtMercureTokenFactoryMock->expects(static::once())
                ->method('createPublisherToken')
                ->with(
                    static::equalTo([$topic])
                )
                ->willReturn($expectToken);
        } else {
            $jwtMercureTokenFactoryMock->expects(static::never())->method('createSubscriberToken');
        }

        return $jwtMercureTokenFactoryMock;
    }

    private function createMercurePublisherMock(
        bool $isRun,
        ?string $token = null,
        ?string $eventName = null,
        ?string $attendeeId = null,
        ?string $topic = null
    ): MockObject
    {
        $mercurePublisher = $this->createMock(AbstractPublisher::class);
        if ($isRun && $token && $eventName && $attendeeId && $topic) {
            $mercurePublisher->expects(static::once())
                ->method('publish')
                ->with(
                    static::equalTo($token),
                    static::equalTo(['changedCart' => $attendeeId]),
                    static::equalTo($topic),
                );
        } else {
            $mercurePublisher->expects(static::never())->method('publish');
        }

        return $mercurePublisher;
    }
}
