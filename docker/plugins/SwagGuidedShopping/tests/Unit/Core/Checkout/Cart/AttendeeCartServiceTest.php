<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Checkout\Cart;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Core\Checkout\Cart\Service\AttendeeCartService;
use SwagGuidedShopping\Tests\Unit\MockBuilder\CartDataMockBuilder;

/**
 * @internal
 */
class AttendeeCartServiceTest extends TestCase
{
    protected string $sqlFindTokens;

    protected function setUp(): void
    {
        parent::setUp();
        $this->sqlFindTokens = "
            SELECT
                token
            FROM sales_channel_api_context
            WHERE JSON_UNQUOTE(JSON_EXTRACT(payload, '$.attendeeId')) IN (?)
        ";
    }

    /**
     * @param array<string, CurrencyEntity> $currencyData
     * @dataProvider getTestCreateAttendeeCartsWithoutAttendeeIdsProviderData
     */
    public function testCreateAttendeeCartsWithoutAttendeeIdsAndCanNotFindCurrency(
        Context $context,
        array $currencyData
    ): void
    {
        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationState = $this->createMock(PresentationStateService::class);
        $presentationState->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn(new StateForGuides('test-appointment-id', 'test-mercure-topic'));
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($presentationState);

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method('executeQuery');

        $service = $this->getService($connection, $presentationStateServiceFactory);

        $attendeeCarts = $service->createAttendeeCarts('test-appointment-id', $currencyData, $context);

        static::assertCount(0, $attendeeCarts);
    }

    public static function getTestCreateAttendeeCartsWithoutAttendeeIdsProviderData(): \Generator
    {
        $self = new AttendeeCartServiceTest('test');
        $context = new Context(new SystemSource());

        $currencyUsingIdDiffWithContextCurrencyId = new CurrencyEntity();
        $currencyUsingIdDiffWithContextCurrencyId->setId('test-currency-id');
        $currencyUsingIdDiffWithContextCurrencyId->setFactor(1.3);
        $invalidCurrencyData = ['test-currency-id' => $currencyUsingIdDiffWithContextCurrencyId];
        yield 'can-not-find-currency' => [
            $context,
            $invalidCurrencyData
        ];

        yield 'can-find-currency' => [
            $context,
            $self->createCurrencyDataWithCurrencyUsingIdSameToContextCurrencyId($context)
        ];
    }

    public function testCreateAttendeeCartsWithAttendeeIdsButCanNotFindTokens(): void
    {
        $context = new Context(new SystemSource());
        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationState = $this->createMock(PresentationStateService::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setVideoUserId(null);
        $attendee->setType('CLIENT');
        $stateForGuides->addClient($attendee);
        $presentationState->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($presentationState);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->with(
                static::equalTo($this->sqlFindTokens),
                static::equalTo([['test-attendee-id']]),
                static::equalTo([ArrayParameterType::STRING])
            )
            ->willReturn($result);

        $service = $this->getService($connection, $presentationStateServiceFactory);

        $currencyData = $this->createCurrencyDataWithCurrencyUsingIdSameToContextCurrencyId($context);

        $attendeeCarts = $service->createAttendeeCarts('test-appointment-id', $currencyData, $context);

        static::assertCount(0, $attendeeCarts);
    }

    public function testCreateAttendeeCartsWithAttendeeIdsAndCanFindTokensButCanNotFindCartData(): void
    {
        $context = new Context(new SystemSource());
        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationState = $this->createMock(PresentationStateService::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setVideoUserId(null);
        $attendee->setType('CLIENT');
        $stateForGuides->addClient($attendee);
        $presentationState->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($presentationState);

        $connection = $this->createMock(Connection::class);
        $resultFindTokens = $this->createMock(Result::class);
        $resultFindTokens->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn(['test-token' => []]);
        $resultFindCarts = $this->createMock(Result::class);
        $resultFindCarts->expects(static::once())
            ->method('fetchAllAssociative')
            ->willReturn([]);
        $connection->expects(static::exactly(2))
            ->method('executeQuery')
            ->willReturn(
                $resultFindTokens,
                $resultFindCarts
            );

        $service = $this->getService($connection, $presentationStateServiceFactory);

        $currency = new CurrencyEntity();
        $currency->setId($context->getCurrencyId());
        $currency->setFactor(1.3);
        $currencyData = [$context->getCurrencyId() => $currency];
        $cartData = $service->createAttendeeCarts('test-appointment-id', $currencyData, $context);

        static::assertCount(0, $cartData);
    }

    /**
     *
     * @dataProvider getTestCreateWithAttendeeIdsAndCanFindTokensAndCartData
     */
    public function testCreateAttendeeCartsWithAttendeeIdsAndCanFindTokensAndCartData(
        string $taxType,
        float $expectedTotalPriceNet
    ): void
    {
        $context = new Context(new SystemSource());
        $presentationStateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $presentationState = $this->createMock(PresentationStateService::class);
        $stateForGuides = new StateForGuides('test-appointment-id', 'test-mercure-topic');
        $attendee = new AttendeeEntity();
        $attendee->setId('test-attendee-id');
        $attendee->setVideoUserId(null);
        $attendee->setType('CLIENT');
        $stateForGuides->addClient($attendee);
        $presentationState->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $presentationStateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($presentationState);

        $connection = $this->createMock(Connection::class);
        $tokenResult = $this->createMock(Result::class);
        $tokenResult->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn(['test-token' => []]);
        $cartResult  = $this->createMock(Result::class);
        $cartResult->expects(static::once())
            ->method('fetchAllAssociative')
            ->willReturn((new CartDataMockBuilder())->createDemoCartData($taxType));
        $connection->expects(static::exactly(2))
            ->method('executeQuery')
            ->willReturn(
                $tokenResult,
                $cartResult
            );

        $service = $this->getService($connection, $presentationStateServiceFactory);

        $currency = new CurrencyEntity();
        $currency->setId($context->getCurrencyId());
        $currency->setFactor(1.3);

        $currency2 = new CurrencyEntity();
        $currency2->setId(\md5('currency_EUR'));
        $currency2->setFactor(25.56);
        $currencyData = [$context->getCurrencyId() => $currency, $currency2->getId() => $currency2];

        $attendeeCarts = $service->createAttendeeCarts('test-appointment-id', $currencyData, $context);

        static::assertCount(1, $attendeeCarts);
        static::assertArrayHasKey('test-token', $attendeeCarts);
        $cartDataOfTestToken = $attendeeCarts['test-token'];
        $items = $cartDataOfTestToken->getItems();
        static::assertArrayHasKey('item-4', $items);
        $item4 = $items['item-4'];
        static::assertEquals('item-4', $item4->getId());
        static::assertEquals(12, $item4->getQuantity());
        $actualTotalPriceNet = $item4->getTotalPriceNet();
        static::assertEquals($actualTotalPriceNet, $expectedTotalPriceNet);
    }

    private function getService(
        ?MockObject $connectionMock = null,
        ?MockObject $presentationStateServiceFactoryMock = null
    ): AttendeeCartService
    {
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ?: $this->createMock(Connection::class);
        $cashRounding = $this->createMock(CashRounding::class);
        /** @var MockObject&PresentationStateServiceFactory $presentationStateServiceFactory */
        $presentationStateServiceFactory = $presentationStateServiceFactoryMock ?: $this->createMock(PresentationStateServiceFactory::class);

        return new AttendeeCartService($connection, $cashRounding, $presentationStateServiceFactory);
    }

    public static function getTestCreateWithAttendeeIdsAndCanFindTokensAndCartData(): \Generator
    {
        yield  [
            'taxType' => CartPrice::TAX_STATE_GROSS,
            'expectedTotalPriceNet' => 6.215962441314554,
        ];

        yield  [
            'taxType' => CartPrice::TAX_STATE_NET,
            'expectedTotalPriceNet' => 6.215962441314554,
        ];

        yield  [
            'taxType' => CartPrice::TAX_STATE_FREE,
            'expectedTotalPriceNet' => 6.215962441314554,
        ];
    }

    /**
     * @return array<string, CurrencyEntity>
     */
    private function createCurrencyDataWithCurrencyUsingIdSameToContextCurrencyId(Context $context): array
    {
        $currency = new CurrencyEntity();
        $currency->setId($context->getCurrencyId());
        $currency->setFactor(1.3);

        return [$context->getCurrencyId() => $currency];
    }
}
