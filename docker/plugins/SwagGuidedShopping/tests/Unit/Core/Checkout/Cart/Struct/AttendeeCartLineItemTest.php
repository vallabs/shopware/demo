<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Checkout\Cart\Struct;

use PHPUnit\Framework\TestCase;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class AttendeeCartLineItemTest extends TestCase
{
    public function testGetAllProperties(): void
    {
        $attendeeCartLineItem = new AttendeeCartLineItem('test1', 1.23456, 2);
        static::assertEquals('test1', $attendeeCartLineItem->getId());
        static::assertEquals(1.23456, $attendeeCartLineItem->getTotalPriceNet());
        static::assertEquals(2, $attendeeCartLineItem->getQuantity());
    }
}
