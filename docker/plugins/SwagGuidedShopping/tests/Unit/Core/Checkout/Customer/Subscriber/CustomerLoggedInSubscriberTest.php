<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Checkout\Customer\Subscriber;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Customer\Event\CustomerBeforeLoginEvent;
use Shopware\Core\Checkout\Customer\Event\CustomerLoginEvent;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use SwagGuidedShopping\Core\Checkout\Customer\Subscriber\CustomerLoggedInSubscriber;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use SwagGuidedShopping\Content\Appointment\Service\ContextDataHelper;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class CustomerLoggedInSubscriberTest extends TestCase
{
    private function getProperty(object $object, string $property): mixed
    {
        $reflectedClass = new \ReflectionClass($object);
        $reflection = $reflectedClass->getProperty($property);
        $reflection->setAccessible(true);

        return $reflection->getValue($object);
    }

    public function testGetSubscribedEvents(): void
    {
        $customerLoggedInSubscriber = $this->getSubscriber();
        $actualSubscriberdEvents = $customerLoggedInSubscriber::getSubscribedEvents();
        $expectedSubscriberdEvents = [
            CustomerLoginEvent::class => 'updateAttendee',
            CustomerBeforeLoginEvent::class => 'findAttendee'
        ];
        static::assertEquals($expectedSubscriberdEvents, $actualSubscriberdEvents);
    }

    /**
     * @dataProvider getTestFindAttendeeData
     */
    public function testFindAttendee(
        ?AttendeeEntity $expectedAttendee
    ): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $contextDataHelper = $this->createMock(ContextDataHelper::class);
        $contextDataHelper->expects(static::once())
            ->method('getAttendeeFromContext')
            ->with(static::equalTo($salesChannelContext))
            ->willReturn($expectedAttendee);

        $customerLoggedInSubscriber = $this->getSubscriber(null, null, null, $contextDataHelper);
        $customerBeforeLoginEvent = new CustomerBeforeLoginEvent($salesChannelContext, 'test-email');
        $customerLoggedInSubscriber->findAttendee($customerBeforeLoginEvent);
        $actualAttendeeEntityBeforeLogin = $this->getProperty($customerLoggedInSubscriber, 'attendeeEntityBeforeLogin');
        static::assertEquals($expectedAttendee, $actualAttendeeEntityBeforeLogin);
    }

    public static function getTestFindAttendeeData(): \Generator
    {
        $attendee = new AttendeeEntity();
        $attendee->setId('test-id');

        yield 'found-attendee' => [
            'expectedAttendee' => $attendee
        ];

        yield 'found-no-attendee' => [
            'expectedAttendee' => null
        ];
    }

    public function testUpdateAttendee(): void
    {
        $salesChannelContextHelper = new SalesChannelContextHelper();
        $customer = new CustomerEntity();
        $customer->setId('test-id');
        $customer->setEmail('test@test.test');
        $token = 'test-token';
        $salesChannelContext = $salesChannelContextHelper->createSalesChannelContext($token, $customer);

        $attendee = new AttendeeEntity();
        $attendee->setId('test-id');
        $contextDataHelper = $this->createMock(ContextDataHelper::class);
        $contextDataHelper->expects(static::once())
                    ->method('getAttendeeFromContext')
                    ->with(static::equalTo($salesChannelContext))
                    ->willReturn($attendee);

        $customerLoginEvent = new CustomerLoginEvent($salesChannelContext, $customer, $token);
        $session = [];
        $salesChannelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $salesChannelContextPersister->expects(static::once())
                                     ->method('load')
                                     ->with(
                                        static::equalTo($customerLoginEvent->getContextToken()),
                                        static::equalTo($customerLoginEvent->getSalesChannelId())
                                    )
                                    ->willReturn($session);

        $abstractSalesChannelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $abstractSalesChannelContextFactory->expects(static::once())
                        ->method('create')
                        ->with(
                            static::equalTo($customerLoginEvent->getContextToken()),
                            static::equalTo($customerLoginEvent->getSalesChannelId()),
                            static::equalTo($session)
                        )
                        ->willReturn($salesChannelContext);

        $requestStack = new RequestStack();
        $requestStack->push(new Request());

        $salesChannelContextPersister->expects(static::once())
                        ->method('save');

        $joinAppointmentService = $this->createMock(JoinAppointmentService::class);
        $joinAppointmentService->expects(static::once())
                        ->method('joinMeeting');

        $customerLoggedInSubscriber = $this->getSubscriber($joinAppointmentService, $abstractSalesChannelContextFactory, $salesChannelContextPersister, $contextDataHelper, $requestStack);
        $customerBeforeLoginEvent = new CustomerBeforeLoginEvent($salesChannelContext, 'test@test.test');

        $customerLoggedInSubscriber->findAttendee($customerBeforeLoginEvent);
        $customerLoggedInSubscriber->updateAttendee($customerLoginEvent);
    }

    /**
     * @dataProvider getFailUpdateAttendee
     */
    public function testFailUpdateAttendee(
        ?AbstractSalesChannelContextFactory $abstractSalesChannelContextFactory,
        ?SalesChannelContextPersister $salesChannelContextPersister,
        ?ContextDataHelper $contextDataHelper,
        ?RequestStack $requestStack,
        SalesChannelContext $salesChannelContext,
        string $email,
        string $token,
        CustomerEntity $customer
    ): void
    {
        $customerLoggedInSubscriber = $this->getSubscriber(null, $abstractSalesChannelContextFactory, $salesChannelContextPersister, $contextDataHelper, $requestStack);
        $customerLoginEvent = new CustomerLoginEvent($salesChannelContext, $customer, $token);
        $customerBeforeLoginEvent = new CustomerBeforeLoginEvent($salesChannelContext, $email);

        $customerLoggedInSubscriber->findAttendee($customerBeforeLoginEvent);
        $customerLoggedInSubscriber->updateAttendee($customerLoginEvent);
    }

    public static function getFailUpdateAttendee(): \Generator
    {
        $self = new CustomerLoggedInSubscriberTest('test');

        $token = 'test-token';
        $requestStack = new RequestStack();
        $requestStack->push(new Request());

        $customer = new CustomerEntity();
        $customer->setId('test-id');
        $customer->setEmail('test@test.test');

        $attendee = new AttendeeEntity();
        $attendee->setId('test-id');

        $salesChannelContextHelper = new SalesChannelContextHelper();
        $salesChannelContextWithCustomer = $salesChannelContextHelper->createSalesChannelContext(null, $customer);
        $salesChannelContextWithoutCustomer = $salesChannelContextHelper->createSalesChannelContext();

        $contextDataHelper = $self->createMock(ContextDataHelper::class);
        $contextDataHelper->expects(static::once())
                    ->method('getAttendeeFromContext')
                    ->with(static::equalTo($salesChannelContextWithCustomer))
                    ->willReturn($attendee);

        $contextDataHelper1 = $self->createMock(ContextDataHelper::class);
        $contextDataHelper1->expects(static::once())
            ->method('getAttendeeFromContext')
            ->with(static::equalTo($salesChannelContextWithoutCustomer))
            ->willReturn(null);

        $salesChannelContextPersister = $self->createMock(SalesChannelContextPersister::class);
        $salesChannelContextPersister->expects(static::never())
                            ->method('load');

        $abstractSalesChannelContextFactory = $self->createMock(AbstractSalesChannelContextFactory::class);
        $abstractSalesChannelContextFactory->expects(static::never())
                            ->method('create');

        yield 'invalid-request' => [
            'abstractSalesChannelContextFactory' => $abstractSalesChannelContextFactory,
            'salesChannelContextPersister' => $salesChannelContextPersister,
            'contextDataHelper' => $contextDataHelper,
            'requestStack' => null,
            'salesChannelContext' => $salesChannelContextWithCustomer,
            'email' => 'test@test.test',
            'token' => $token,
            'customer' => $customer
        ];

        yield 'invalid-attendee' => [
            'abstractSalesChannelContextFactory' => $abstractSalesChannelContextFactory,
            'salesChannelContextPersister' => $salesChannelContextPersister,
            'contextDataHelper' => $contextDataHelper1,
            'requestStack' => $requestStack,
            'salesChannelContext' => $salesChannelContextWithoutCustomer,
            'email' => '',
            'token' => $token,
            'customer' => $customer
        ];
    }

    public function testFailUpdateAttendeeWithInvalidSaleChannelCustomer(): void
    {
        $token = 'test-token';
        $requestStack = new RequestStack();
        $requestStack->push(new Request());

        $attendee = new AttendeeEntity();
        $attendee->setId('test-id');

        $salesChannelContextHelper = new SalesChannelContextHelper();
        $salesChannelContext = $salesChannelContextHelper->createSalesChannelContext();

        $contextDataHelper = $this->createMock(ContextDataHelper::class);
        $contextDataHelper->expects(static::once())
                ->method('getAttendeeFromContext')
                ->with(static::equalTo($salesChannelContext))
                ->willReturn($attendee);

        $salesChannelContextPersister = $this->createMock(SalesChannelContextPersister::class);
        $salesChannelContextPersister->expects(static::once())
                        ->method('load')
                        ->willReturn([]);

        $abstractSalesChannelContextFactory = $this->createMock(AbstractSalesChannelContextFactory::class);
        $abstractSalesChannelContextFactory->expects(static::once())
                        ->method('create')
                        ->willReturn($this->createMock(SalesChannelContext::class));

        $customerLoggedInSubscriber = $this->getSubscriber(null, $abstractSalesChannelContextFactory, $salesChannelContextPersister, $contextDataHelper, $requestStack);
        $customer = new CustomerEntity();
        $customer->setId('test-id');
        $customerLoginEvent = new CustomerLoginEvent($salesChannelContext, $customer, $token);
        $customerBeforeLoginEvent = new CustomerBeforeLoginEvent($salesChannelContext, 'test@test.test');

        $customerLoggedInSubscriber->findAttendee($customerBeforeLoginEvent);
        $customerLoggedInSubscriber->updateAttendee($customerLoginEvent);
    }

    private function getSubscriber(
        ?JoinAppointmentService $joinAppointmentService = null,
        ?AbstractSalesChannelContextFactory $abstractSalesChannelContextFactory = null,
        ?SalesChannelContextPersister $salesChannelContextPersister = null,
        ?ContextDataHelper $contextDataHelper = null,
        ?RequestStack $requestStack = null
    ): CustomerLoggedInSubscriber
    {
        /** @var JoinAppointmentService $joinAppointmentService */
        $joinAppointmentService = $joinAppointmentService ?? $this->createMock(JoinAppointmentService::class);
        /** @var AbstractSalesChannelContextFactory $abstractSalesChannelContextFactory */
        $abstractSalesChannelContextFactory = $abstractSalesChannelContextFactory ?? $this->createMock(AbstractSalesChannelContextFactory::class);
        /** @var SalesChannelContextPersister $salesChannelContextPersister */
        $salesChannelContextPersister = $salesChannelContextPersister ?? $this->createMock(SalesChannelContextPersister::class);
        /** @var ContextDataHelper $contextDataHelper */
        $contextDataHelper = $contextDataHelper ?? $this->createMock(ContextDataHelper::class);
        /** @var RequestStack $requestStack */
        $requestStack = $requestStack ?? $this->createMock(RequestStack::class);

        return new CustomerLoggedInSubscriber(
            $joinAppointmentService,
            $abstractSalesChannelContextFactory,
            $salesChannelContextPersister,
            $contextDataHelper,
            $requestStack
        );
    }
}
