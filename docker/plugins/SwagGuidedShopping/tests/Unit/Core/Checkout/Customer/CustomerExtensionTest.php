<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Core\Customer;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\AssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Core\Checkout\Customer\CustomerExtension;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;

class CustomerExtensionTest extends TestCase
{
    public function testExtendFields(): void
    {
        $fieldCollection = new FieldCollection();
        $customerExtension = new CustomerExtension();
        $customerExtension->extendFields($fieldCollection);
        // Check the fields count
        $elements = $fieldCollection->getElements();
        static::assertCount(1, $elements);
        /** @var AssociationField $element */
        $element = $elements[0];
        static::assertInstanceOf(OneToManyAssociationField::class, $element);
        static::assertEquals('attendee', $element->getPropertyName());
        static::assertEquals(AttendeeDefinition::class, $element->getReferenceClass());
        static::assertEquals('customer_id', $element->getReferenceField());
        static::assertNotNull($element->getFlag(CascadeDelete::class));
    }

    public function testGetDefinitionClass(): void
    {
        $customerExtension = new CustomerExtension();
        static::assertEquals(CustomerDefinition::class, $customerExtension->getDefinitionClass());
    }
}
