<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Subscriber;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Result;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Event\AfterProductListingLoadedEvent;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Subscriber\AfterProductListingLoaderSubscriber;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class AfterProductListingLoaderSubscriberTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $subscriber = $this->getSubscriber();

        $events = $subscriber::getSubscribedEvents();

        static::assertCount(1, $events);
        static::assertArrayHasKey(AfterProductListingLoadedEvent::class, $events);
        static::assertSame('addLikesAndDisLikesToProduct', $events[AfterProductListingLoadedEvent::class]);
    }

    public function testAddLikesAndDisLikesToProductWithoutAttendeeInContext(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $searchResult = new EntitySearchResult(
            'test-entity',
            1,
            new ProductCollection(),
            null,
            new Criteria(),
            $salesChannelContext->getContext()
        );

        $event = new AfterProductListingLoadedEvent($searchResult, $salesChannelContext);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateServiceFactory->expects(static::never())->method(static::anything());

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method(static::anything());

        $subscriber = $this->getSubscriber($stateServiceFactory, $connection);

        $subscriber->addLikesAndDisLikesToProduct($event);
    }

    public function testAddLikesAndDisLikesToProductWithoutAttendeeInAppointment(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendeeId = \md5('test-attendee-id');
        $attendee->assign([
            'id' => $attendeeId,
            'attendeeName' => 'test-attendee-name',
            'appointmentId' => 'test-appointment-id',
            'type' => 'test-type'
        ]);
        $salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);
        $searchResult = new EntitySearchResult(
            'test-entity',
            1,
            new ProductCollection(),
            null,
            new Criteria(),
            $salesChannelContext->getContext()
        );

        $event = new AfterProductListingLoadedEvent($searchResult, $salesChannelContext);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides(
            'test-appointment-id',
            'test-mercure-topic',
        );
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method(static::anything());

        $subscriber = $this->getSubscriber($stateServiceFactory, $connection);

        $subscriber->addLikesAndDisLikesToProduct($event);
    }

    public function testAddLikesAndDisLikesToProductWithAttendee(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();
        $attendee = new AttendeeEntity();
        $attendeeId = \md5('test-attendee-id');
        $attendee->assign([
            'id' => $attendeeId,
            'attendeeName' => 'test-attendee-name',
            'appointmentId' => 'test-appointment-id',
            'type' => 'test-type'
        ]);
        $salesChannelContext->addExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME, $attendee);

        $product = new ProductEntity();
        $productId = \md5('test-product-id');
        $product->setId($productId);
        $searchResult = new EntitySearchResult(
            'test-entity',
            1,
            new ProductCollection([$product]),
            null,
            new Criteria(),
            $salesChannelContext->getContext()
        );

        $event = new AfterProductListingLoadedEvent($searchResult, $salesChannelContext);

        $stateServiceFactory = $this->createMock(PresentationStateServiceFactory::class);
        $stateForGuides = new StateForGuides(
            'test-appointment-id',
            'test-mercure-topic',
        );
        $stateForGuides->addAttendee($attendee);
        $stateService = $this->createMock(PresentationStateService::class);
        $stateService->expects(static::once())
            ->method('getStateForGuides')
            ->willReturn($stateForGuides);
        $stateServiceFactory->expects(static::once())
            ->method('build')
            ->willReturn($stateService);

        $connection = $this->createMock(Connection::class);
        $result = $this->createMock(Result::class);
        $result->expects(static::once())
            ->method('fetchAllAssociativeIndexed')
            ->willReturn([
                $productId  => [
                    'likes' => 3,
                    'dislikes' => 1,
                ],
            ]);
        $connection->expects(static::once())
            ->method('executeQuery')
            ->willReturn($result);

        $subscriber = $this->getSubscriber($stateServiceFactory, $connection);

        $subscriber->addLikesAndDisLikesToProduct($event);

        $product = $event->getSearchResult()->getEntities()->first();
        static::assertNotNull($product);
        static::assertTrue($product->hasExtension('attendeeProductCollections'));

        $extensionData = $product->getExtension('attendeeProductCollections');
        static::assertInstanceOf(ArrayStruct::class, $extensionData);
        static::assertTrue($extensionData->has('likes'));
        static::assertEquals(3, $extensionData->get('likes'));
        static::assertTrue($extensionData->has('dislikes'));
        static::assertEquals(1, $extensionData->get('dislikes'));
    }


    private function getSubscriber(
        MockObject $stateServiceFactoryMock = null,
        MockObject $connectionMock = null
    ): AfterProductListingLoaderSubscriber
    {
        /** @var MockObject&PresentationStateServiceFactory $stateServiceFactory */
        $stateServiceFactory = $stateServiceFactoryMock ?? $this->createMock(PresentationStateServiceFactory::class);
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ?? $this->createMock(Connection::class);

        return new AfterProductListingLoaderSubscriber(
            $stateServiceFactory,
            $connection
        );
    }
}
