<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Subscriber;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Document\DocumentDefinition;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\DefinitionInstanceRegistry;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityWriteResult;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityDeleteEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntitySearchedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Validation\RestrictDeleteViolationException;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Subscriber\EnrichCmsPageEvent;

class
EnrichCmsPageEventTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $event = $this->getEvent();

        $expectedData = [
            'cms_page.search' => 'addCriteria',
            'cms_page.written' => 'updateTheUserWhoCreateTheCmsPage',
            EntityDeleteEvent::class => 'checkIfTheLayoutIsUsed'
        ];
        $actualData = $event::getSubscribedEvents();
        static::assertEquals($expectedData, $actualData);
    }

    public function testAddCriteria(): void
    {
        $event = $this->getEvent();

        $criteria = new Criteria();
        $entitySearchedEvent = new EntitySearchedEvent(
            $criteria,
            new DocumentDefinition(),
            new Context(new SystemSource())
        );
        $event->addCriteria($entitySearchedEvent);
        static::assertTrue($criteria->hasAssociation('createdBy'));
    }

    /**
     * @dataProvider getTestUpdateTheUserWithInvalidEntityWrittenEventProviderData
     */
    public function testUpdateTheUserWithInvalidEntityWrittenEvent(
        EntityWrittenEvent $entityWrittenEvent
    ): void
    {
        $event = $this->getEvent();
        $event->updateTheUserWhoCreateTheCmsPage($entityWrittenEvent);
    }

    public static function getTestUpdateTheUserWithInvalidEntityWrittenEventProviderData(): \Generator
    {
        yield 'invalid-context-source' => [
            new EntityWrittenEvent(
                CmsPageDefinition::class,
                [],
                new Context(new SystemSource()),
                []
            )
        ];
        yield 'context-source-with-empty-user-id' => [
            new EntityWrittenEvent(
                CmsPageDefinition::class,
                [],
                new Context(new AdminApiSource(null)),
                []
            )
        ];
        yield 'context-source-with-empty-write-result' => [
            new EntityWrittenEvent(
                CmsPageDefinition::class,
                [],
                new Context(new AdminApiSource('test-user-id')),
                []
            )
        ];
        yield 'context-source-with-write-result-has-empty-payload' => [
            new EntityWrittenEvent(
                CmsPageDefinition::class,
                [],
                new Context(new AdminApiSource('test-user-id')),
                []
            )
        ];
        yield 'context-source-with-write-result-has-invalid-payload' => [
            new EntityWrittenEvent(
                CmsPageDefinition::class,
                [new EntityWriteResult(
                    'test-primary-key',
                    ['invalid-key' => 'value'],
                    CmsPageDefinition::ENTITY_NAME,
                    EntityWriteResult::OPERATION_INSERT
                )],
                new Context(new AdminApiSource('test-user-id')),
                []
            )
        ];
        yield 'context-source-with-write-result-has-invalid-payload-2' => [
            new EntityWrittenEvent(
                CmsPageDefinition::class,
                [new EntityWriteResult(
                    'test-primary-key',
                    ['id' => null],
                    CmsPageDefinition::ENTITY_NAME,
                    EntityWriteResult::OPERATION_INSERT
                )],
                new Context(new AdminApiSource('test-user-id')),
                []
            )
        ];
    }

    public function testUpdateTheUserWithValidEntityWrittenEvent(): void
    {
        $entityWrittenEvent = new EntityWrittenEvent(
            CmsPageDefinition::class,
            [new EntityWriteResult(
                'test-primary-key',
                ['id' => \md5('test-cms-page-id')],
                CmsPageDefinition::ENTITY_NAME,
                EntityWriteResult::OPERATION_INSERT
            )],
            new Context(new AdminApiSource(\md5('test-user-id'))),
            []
        );
        $connection = $this->createMock(Connection::class);
        $connection->expects(static::once())
            ->method('executeStatement')
            ->with(
                static::equalTo('UPDATE `cms_page` SET `created_by_id` = :createdById WHERE `id` = :id AND `created_by_id` IS NULL'),
                static::equalTo([
                    'createdById' => Uuid::fromHexToBytes(\md5('test-user-id')),
                    'id' => Uuid::fromHexToBytes(\md5('test-cms-page-id')),
                ])
            );

        $event = $this->getEvent($connection);
        $event->updateTheUserWhoCreateTheCmsPage($entityWrittenEvent);
    }

    public function testCheckIfTheLayoutIsUsedWithEmptyIds(): void
    {
        $EntityDeleteEvent = $this->createMock(EntityDeleteEvent::class);
        $EntityDeleteEvent->expects(static::once())
            ->method('getIds')
            ->with(static::equalTo(CmsPageDefinition::ENTITY_NAME))
            ->willReturn([]);

        $entityRepository = $this->createMock(EntityRepository::class);
        $entityRepository->expects(static::never())->method('searchIds');

        $registry = $this->createMock(DefinitionInstanceRegistry::class);
        $registry->expects(static::never())->method('register');

        $event = $this->getEvent(null, $entityRepository, $registry);
        $event->checkIfTheLayoutIsUsed($EntityDeleteEvent);
    }

    public function testCheckIfTheLayoutIsUsedWhenCmsPageIsNotUsedAnyWhere(): void
    {
        $cmsPageId = \md5('test-cms-page-id');
        $context = new Context(new SystemSource());

        $EntityDeleteEvent = $this->createMock(EntityDeleteEvent::class);
        $EntityDeleteEvent->expects(static::once())
            ->method('getIds')
            ->willReturn([$cmsPageId]);
        $EntityDeleteEvent->expects(static::once())
            ->method('getContext')
            ->willReturn($context);

        $entityRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('cmsPageId', [$cmsPageId]));
        $entityRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new IdSearchResult(0, [], $criteria, $context));

        $registry = $this->createMock(DefinitionInstanceRegistry::class);
        $registry->expects(static::never())->method('register');

        $event = $this->getEvent(null, $entityRepository, $registry);
        $event->checkIfTheLayoutIsUsed($EntityDeleteEvent);
    }

    public function testCheckIfTheLayoutIsUsedWhenCmsPageIsUsedSomeWhere(): void
    {
        $cmsPageId = \md5('test-cms-page-id');
        $context = new Context(new SystemSource());

        $EntityDeleteEvent = $this->createMock(EntityDeleteEvent::class);
        $EntityDeleteEvent->expects(static::once())
            ->method('getIds')
            ->willReturn([$cmsPageId]);
        $EntityDeleteEvent->expects(static::once())
            ->method('getContext')
            ->willReturn($context);

        $entityRepository = $this->createMock(EntityRepository::class);
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('cmsPageId', [$cmsPageId]));
        $entityRepository->expects(static::once())
            ->method('searchIds')
            ->with(
                static::equalTo($criteria),
                static::equalTo($context)
            )
            ->willReturn(new IdSearchResult(1, [['data' => [$cmsPageId], 'primaryKey' => $cmsPageId]], $criteria, $context));

        $registry = $this->createMock(DefinitionInstanceRegistry::class);
        $registry->expects(static::once())
            ->method('getByEntityName')
            ->with(static::equalTo(CmsPageDefinition::ENTITY_NAME))
            ->willReturn(new CmsPageDefinition());

        $event = $this->getEvent(null, $entityRepository, $registry);
        static::expectException(RestrictDeleteViolationException::class);
        $event->checkIfTheLayoutIsUsed($EntityDeleteEvent);
    }

    private function getEvent(
        ?MockObject $connectionMock = null,
        ?MockObject $entityRepositoryMock = null,
        ?MockObject $registryMock = null
    ): EnrichCmsPageEvent
    {
        $connection = $this->createMock(Connection::class);
        $connection->expects(static::never())->method('executeStatement');
        /** @var MockObject&Connection $connection */
        $connection = $connectionMock ?: $connection;

        $entityRepository = $this->createMock(EntityRepository::class);
        $entityRepository->expects(static::never())->method('searchIds');
        /** @var MockObject&EntityRepository<PresentationCmsPageCollection> $entityRepository */
        $entityRepository = $entityRepositoryMock ?: $entityRepository;

        $registry = $this->createMock(DefinitionInstanceRegistry::class);
        $registry->expects(static::never())->method('register');
        /** @var MockObject&DefinitionInstanceRegistry $registry */
        $registry = $registryMock ?: $registry;

        return new EnrichCmsPageEvent($connection, $entityRepository, $registry);
    }
}
