<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Subscriber;

use PHPUnit\Framework\TestCase;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Content\Product\Aggregate\ProductConfiguratorSetting\ProductConfiguratorSettingEntity;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionCollection;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Content\Property\PropertyGroupEntity;
use SwagGuidedShopping\Content\Presentation\Event\AfterLoadProductConfiguratorEvent;
use SwagGuidedShopping\Subscriber\AfterLoadProductConfiguratorSubscriber;
use SwagGuidedShopping\Tests\Unit\Helpers\SalesChannelContextHelper;

class AfterLoadProductConfiguratorSubscriberTest extends TestCase
{
    public function testGetSubscribedEvents(): void
    {
        $subscriber = new AfterLoadProductConfiguratorSubscriber();

        $events = $subscriber::getSubscribedEvents();

        static::assertCount(1, $events);
        static::assertArrayHasKey(AfterLoadProductConfiguratorEvent::class, $events);
        static::assertSame('assignConfiguratorMedia', $events[AfterLoadProductConfiguratorEvent::class]);
    }

    public function testAssignConfiguratorMediaWithoutGroups(): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $event = new AfterLoadProductConfiguratorEvent(null, $salesChannelContext);

        $subscriber = new AfterLoadProductConfiguratorSubscriber();

        $subscriber->assignConfiguratorMedia($event);

        static::assertNull($event->getGroups());
    }

    /**
     * @dataProvider getTestAssignConfiguratorMediaButDoNothingProviderData
     */
    public function testAssignConfiguratorMediaWithGroupContainsNoOptions(
        PropertyGroupCollection $groups
    ): void
    {
        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $event = new AfterLoadProductConfiguratorEvent($groups, $salesChannelContext);

        $subscriber = new AfterLoadProductConfiguratorSubscriber();

        $subscriber->assignConfiguratorMedia($event);

        static::assertSame($groups, $event->getGroups());
    }

    public static function getTestAssignConfiguratorMediaButDoNothingProviderData(): \Generator
    {
        $groupNoOptions = new PropertyGroupEntity();
        $groupNoOptions->setId('test-group-id');
        yield 'group contains no options' => [new PropertyGroupCollection([$groupNoOptions])];

        $groupContainsOptionHasNoProductConfiguratorSetting = clone $groupNoOptions;
        $optionNoConfiguratorSetting = new PropertyGroupOptionEntity();
        $optionNoConfiguratorSetting->setId('test-option-id');
        $groupContainsOptionHasNoProductConfiguratorSetting->setOptions(new PropertyGroupOptionCollection([$optionNoConfiguratorSetting]));
        yield 'group contains option with no product configurator setting' => [
            new PropertyGroupCollection([$groupContainsOptionHasNoProductConfiguratorSetting])
        ];

        $groupContainsOptionHasProductConfiguratorSettingButNoMedia = clone $groupNoOptions;
        $optionHasConfiguratorSetting = clone $optionNoConfiguratorSetting;
        $productConfiguratorSettingNoMedia = new ProductConfiguratorSettingEntity();
        $productConfiguratorSettingNoMedia->setId('test-product-configurator-setting-id');
        $optionHasConfiguratorSetting->setConfiguratorSetting($productConfiguratorSettingNoMedia);
        $groupContainsOptionHasProductConfiguratorSettingButNoMedia->setOptions(new PropertyGroupOptionCollection([$optionHasConfiguratorSetting]));
        yield 'group contains option with product configurator setting has no media' => [
            new PropertyGroupCollection([$groupContainsOptionHasProductConfiguratorSettingButNoMedia])
        ];
    }

    public function testAssign(): void
    {
        $group = new PropertyGroupEntity();
        $option = new PropertyGroupOptionEntity();
        $productConfiguratorSetting = new ProductConfiguratorSettingEntity();
        $media = new MediaEntity();
        $media->setId('test-media-id');
        $productConfiguratorSetting->setMedia($media);
        $option->setId('test-option-id');
        $option->setConfiguratorSetting($productConfiguratorSetting);
        $group->assign([
            'id' => 'test-group-id',
            'options' => new PropertyGroupOptionCollection([$option]),
        ]);
        $groups = new PropertyGroupCollection([$group]);

        $salesChannelContext = (new SalesChannelContextHelper())->createSalesChannelContext();

        $event = new AfterLoadProductConfiguratorEvent($groups, $salesChannelContext);

        $subscriber = new AfterLoadProductConfiguratorSubscriber();

        $subscriber->assignConfiguratorMedia($event);

        static::assertSame('test-media-id', $option->getMediaId());
        static::assertSame($media, $option->getMedia());
    }
}
