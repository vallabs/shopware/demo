<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Subscriber;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Checkout\Document\DocumentDefinition;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWriteEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\DeleteCommand;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\InsertCommand;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\UpdateCommand;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\WriteCommand;
use Shopware\Core\Framework\DataAbstractionLayer\Write\EntityExistence;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Validation\PreWriteValidationEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Write\WriteContext;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Validation\WriteConstraintViolationException;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Subscriber\AppointmentValidator;
use SwagGuidedShopping\Tests\Unit\MockBuilder\AppointMentMockHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class AppointmentValidatorTest extends TestCase
{
    private Context $context;

    protected function setUp(): void
    {
        parent::setUp();
        $this->context = new Context(new SystemSource());
    }

    public function testGetSubscriberEvents(): void
    {
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $subscriber = $this->getValidator($appointmentRepository);

        $expectSubscribers = [
            PreWriteValidationEvent::class => 'preValidate',
            EntityWriteEvent::class => 'beforeWrite'
        ];

        static::assertSame($expectSubscribers, $subscriber->getSubscribedEvents());
    }

    /**
     * @param list<WriteCommand> $commands
     *
     * @dataProvider validDataProvider
     */
    public function testPreValidateWithValidData(
        array $commands,
        ?MockObject $appointmentRepositoryMock = null
    ): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createAppointmentRepository();
        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $event = new PreWriteValidationEvent($writeContext, $commands);

        $subscriber->preValidate($event);

        /** @var array<int, mixed> $exceptions */
        $exceptions = $event->getExceptions()->getExceptions();
        static::assertCount(0, $exceptions);
    }

    public static function validDataProvider(): \Generator
    {
        $self = new self();

        yield 'without-command' => [[]];

        $entityExistence = $self->createMock(EntityExistence::class);
        $validPayload = [
            'presentation_path' => 'valid-presentation-path',
            'mode' => AppointmentDefinition::MODE_SELF,
            'sender' => null
        ];

        yield 'with-delete-command' => [
            [
                new DeleteCommand(
                    new AppointmentDefinition(),
                    [],
                    $entityExistence
                )
            ]
        ];

        yield 'with-insert-command-no-payload' => [
            [
                new InsertCommand(
                    new AppointmentDefinition(),
                    [],
                    [],
                    $entityExistence,
                    ''
                )
            ]
        ];

        yield 'with-insert-command-valid-payload' => [
            [
                new InsertCommand(
                    new AppointmentDefinition(),
                    $validPayload,
                    [],
                    $entityExistence,
                    ''
                )
            ],
            $self->createAppointmentRepository(true)
        ];

        yield 'with-insert-command-valid-payload-but-no-used-yet' => [
            [
                new InsertCommand(
                    new AppointmentDefinition(),
                    $validPayload,
                    [],
                    $entityExistence,
                    ''
                )
            ],
            $self->createAppointmentRepository(true)
        ];

        yield 'with-update-command-no-payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    [],
                    [],
                    $entityExistence,
                    ''
                )
            ]
        ];

        yield 'with-update-command-valid-payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    $validPayload,
                    [],
                    $entityExistence,
                    ''
                )
            ],
            $self->createAppointmentRepository(true)
        ];

        yield 'with-2-valid-commands' => [
            [
                new DeleteCommand(
                    new DocumentDefinition(),
                    [],
                    $entityExistence
                ),
                new DeleteCommand(
                    new AppointmentDefinition(),
                    [],
                    $entityExistence
                )
            ]
        ];
    }

    /**
     * @dataProvider invalidDataProvider
     *
     * @param list<WriteCommand> $commands
     * @param array<ConstraintViolation> $expectViolations
     */
    public function testPreValidateWithInvalidData(
        array $commands,
        int $violationsCount,
        array $expectViolations,
        ?MockObject $appointmentRepositoryMock = null
    ): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createAppointmentRepository();
        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $event = new PreWriteValidationEvent($writeContext, $commands);

        $subscriber->preValidate($event);

        /** @var array<int, mixed> $exceptions */
        $exceptions = $event->getExceptions()->getExceptions();
        static::assertCount(1, $exceptions);

        /** @var WriteConstraintViolationException $exception */
        $exception = $exceptions[0];
        static::assertEquals(Response::HTTP_BAD_REQUEST, $exception->getStatusCode());

        $violations = $exception->getViolations();
        static::assertEquals($violationsCount, $violations->count());
        foreach ($violations as $key => $value) {
            static::assertEquals($expectViolations[$key], $violations[$key]);
        }
    }

    public static function invalidDataProvider(): \Generator
    {
        $self = new AppointmentValidatorTest('test');

        $neverRunAppointmentRepository = $self->createMock(EntityRepository::class);
        $neverRunAppointmentRepository->expects(static::never())->method('searchIds');

        $entityExistence = $self->createMock(EntityExistence::class);

        $presentationPath = 'test-presentation-path';
        $presentationPathViolation = new ConstraintViolation(
            'This path already exists.',
            '',
            [
                'value' => $presentationPath,
            ],
            $presentationPath,
            '/0/presentationPath',
            $presentationPath,
            null,
            'DUPLICATED_URL'
        );
        yield 'with insert command invalid presentation path payload' => [
            [new InsertCommand(
                new AppointmentDefinition(),
                ['presentation_path' => $presentationPath],
                [],
                $entityExistence,
                ''
            )],
            1,
            [$presentationPathViolation],
            $self->createAppointmentRepository(true, true)
        ];
        yield 'with update command invalid presentation path payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    [
                        'presentation_path' => $presentationPath,
                        'guided_shopping_presentation_id' => 'test-presentation-id'
                    ],
                    ['id' => \md5('test-appointment-id')],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$presentationPathViolation],
            $self->createAppointmentRepository(true, true)
        ];

        $senderViolation = new ConstraintViolation(
            'The sender should be not empty.',
            '',
            [
                'value' => null,
            ],
            null,
            '/0/guideUserId',
            null,
            null,
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        );
        yield 'with insert command invalid sender payload' => [
            [new InsertCommand(
                new AppointmentDefinition(),
                [
                    'mode' => AppointmentDefinition::MODE_GUIDED,
                    'guide_user_id' => null,
                    'accessible_from' => "2023-07-13T12:42:00.000Z",
                    'accessible_to' => "2023-07-15T12:42:00.000Z"
                ],
                [],
                $entityExistence,
                ''
            )],
            1,
            [$senderViolation],
            $self->createAppointmentRepository()
        ];
        yield 'with update command invalid sender payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    [
                        'mode' => AppointmentDefinition::MODE_GUIDED,
                        'guide_user_id' => null,
                        'accessible_from' => "2023-07-13T12:42:00.000Z",
                        'accessible_to' => "2023-07-15T12:42:00.000Z"
                    ],
                    [],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$senderViolation],
            $self->createAppointmentRepository()
        ];

        $accessibleFromViolation = new ConstraintViolation(
            'The Accessible From should be not empty.',
            '',
            [
                'value' => null,
            ],
            null,
            '/0/accessibleFrom',
            null,
            null,
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        );
        yield 'with insert command invalid accessible from payload' => [
            [
                new InsertCommand(
                    new AppointmentDefinition(),
                    [
                        'mode' => AppointmentDefinition::MODE_GUIDED,
                        'guide_user_id' => \md5('test-user-id'),
                        'accessible_from' => null,
                        'accessible_to' => "2023-07-15T12:42:00.000Z"
                    ],
                    [],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$accessibleFromViolation],
            $self->createAppointmentRepository()
        ];
        yield 'with update command invalid accessible from payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    [
                        'mode' => AppointmentDefinition::MODE_GUIDED,
                        'guide_user_id' => \md5('test-user-id'),
                        'accessible_from' => null,
                        'accessible_to' => "2023-07-15T12:42:00.000Z"
                    ],
                    [],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$accessibleFromViolation],
            $self->createAppointmentRepository()
        ];

        $accessibleToViolation = new ConstraintViolation(
            'The Accessible To should be not empty.',
            '',
            [
                'value' => null,
            ],
            null,
            '/0/accessibleTo',
            null,
            null,
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        );
        yield 'with insert command invalid accessible to payload' => [
            [
                new InsertCommand(
                    new AppointmentDefinition(),
                    [
                        'mode' => AppointmentDefinition::MODE_GUIDED,
                        'guide_user_id' => \md5('test-user-id'),
                        'accessible_from' => "2023-07-15T12:42:00.000Z",
                        'accessible_to' => null
                    ],
                    [],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$accessibleToViolation],
            $self->createAppointmentRepository()
        ];
        yield 'with update command invalid accessible to payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    [
                        'mode' => AppointmentDefinition::MODE_GUIDED,
                        'guide_user_id' => \md5('test-user-id'),
                        'accessible_from' => "2023-07-15T12:42:00.000Z",
                        'accessible_to' => null
                    ],
                    [],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$accessibleToViolation],
            $self->createAppointmentRepository()
        ];

        $accessibleTimeViolation = new ConstraintViolation(
            'Accessible To should be greater than Accessible From.',
            '',
            [
                'value' => "2023-07-13T12:42:00.000Z",
            ],
            "2023-07-13T12:42:00.000Z",
            '/0/accessibleTo',
            "2023-07-13T12:42:00.000Z",
            null,
            'INVALID_ACCESSIBLE_TIME'
        );
        yield 'with insert command invalid accessible time payload' => [
            [new InsertCommand(
                new AppointmentDefinition(),
                ['accessible_from' => "2023-07-13T12:42:00.000Z", 'accessible_to' => "2023-07-13T12:42:00.000Z"],
                [],
                $entityExistence,
                ''
            )],
            1,
            [$accessibleTimeViolation],
            $self->createAppointmentRepository()
        ];
        yield 'with update command invalid accessible time payload' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    ['accessible_from' => "2023-07-13T12:42:00.000Z", 'accessible_to' => "2023-07-13T12:42:00.000Z"],
                    [],
                    $entityExistence,
                    ''
                )
            ],
            1,
            [$accessibleTimeViolation],
            $self->createAppointmentRepository()
        ];

        $presentationIdViolation = new ConstraintViolation(
            'The presentation should be not empty.',
            '',
            [
                'value' => null,
            ],
            null,
            '/0/presentationId',
            null,
            null,
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        );

        yield 'with insert command invalid request payload (duplicate url, empty sender and invalid accessible time)' => [
            [new InsertCommand(
                new AppointmentDefinition(),
                [
                    'presentation_path' => $presentationPath,
                    'mode' => AppointmentDefinition::MODE_GUIDED,
                    'sender' => null,
                    'accessible_from' => "2023-07-13T12:42:00.000Z",
                    'accessible_to' => "2023-07-13T12:42:00.000Z"
                ],
                [],
                $entityExistence,
                ''
            )],
            3,
            [$presentationPathViolation, $senderViolation, $accessibleTimeViolation],
            $self->createAppointmentRepository(true, true)
        ];

        yield 'with update command invalid request payload (empty presentation id, duplicate url, empty sender and invalid accessible time)' => [
            [
                new UpdateCommand(
                    new AppointmentDefinition(),
                    [
                        'presentation_path' => $presentationPath,
                        'mode' => AppointmentDefinition::MODE_GUIDED,
                        'sender' => null,
                        'accessible_from' => "2023-07-13T12:42:00.000Z",
                        'accessible_to' => "2023-07-13T12:42:00.000Z",
                    ],
                    ['id' => \md5('test-appointment-id')],
                    $entityExistence,
                    ''
                )
            ],
            4,
            [$presentationIdViolation, $presentationPathViolation, $senderViolation, $accessibleTimeViolation],
            $self->createAppointmentRepository(true, true)
        ];

        yield 'with insert command invalid accessible time payload (accessible_from wrong date time format)' => [
            [new InsertCommand(
                new AppointmentDefinition(),
                [
                    'accessible_from' => '1',
                    'accessible_to' => "2023-07-13T12:42:00.000Z"
                ],
                [],
                $entityExistence,
                ''
            )],
            1,
            [$accessibleFromViolation],
            $this->createAppointmentRepository()
        ];

        yield 'with insert command invalid accessible time payload (accessible_to wrong date time format)' => [
            [new InsertCommand(
                new AppointmentDefinition(),
                [
                    'accessible_from' => "2023-07-13T12:42:00.000Z",
                    'accessible_to' => '1'
                ],
                [],
                $entityExistence,
                ''
            )],
            1,
            [$accessibleToViolation],
            $this->createAppointmentRepository()
        ];
    }

    public function testPreValidateWithValidDataButNotFoundParentPresentation(): void
    {
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                $this->context
            ));

        $presentationRepository = $this->createMock(EntityRepository::class);
        $presentationRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                0,
                new AppointmentCollection([]),
                null,
                new Criteria(),
                $this->context
            ));

        $subscriber = $this->getValidator($appointmentRepository, $presentationRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $event = new PreWriteValidationEvent($writeContext, [
            new UpdateCommand(
                new AppointmentDefinition(),
                [
                    'presentation_path' => 'test-presentation-path-xyz',
                    'mode' => AppointmentDefinition::MODE_SELF,
                    'sender' => null
                ],
                ['id' => \md5('test-appointment-id')],
                $this->createMock(EntityExistence::class),
                ''
            )
        ]);

        $subscriber->preValidate($event);

        $exceptions = $event->getExceptions()->getExceptions();
        static::assertCount(1, $exceptions);

        $exception = $exceptions[0];
        static::assertInstanceOf(WriteConstraintViolationException::class, $exception);

        $violations = $exception->getViolations();
        static::assertEquals(1, $violations->count());
        static::assertEquals('The presentation should be not empty.', $violations[0]->getMessage());
        static::assertEquals('/0/presentationId', $violations[0]->getPropertyPath());
        static::assertEquals('c1051bb4-d103-4f74-8988-acbcafc7fdc3', $violations[0]->getCode());
    }

    /**
     * @param array<string, mixed> $payload
     *
     * @dataProvider getTestUpdateAppointmentWithGuidedModeAndDifferentAccessibleTimeAndExistingAppointmentProviderData
     */
    public function testUpdateAppointmentWithGuidedModeAndDifferentAccessibleTimeAndExistingGuidedAppointment(
        AppointmentEntity $appointment,
        array $payload,
        string $violateMessage,
        string $violatePropertyPath,
        string $violateCode
    ): void
    {
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                $this->context
            ));

        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $event = new PreWriteValidationEvent($writeContext, [
            new UpdateCommand(
                new AppointmentDefinition(),
                $payload,
                ['id' => \md5('test-appointment-id')],
                $this->createMock(EntityExistence::class),
                ''
            )
        ]);

        $subscriber->preValidate($event);

        $exceptions = $event->getExceptions()->getExceptions();
        static::assertCount(1, $exceptions);

        $exception = $exceptions[0];
        static::assertInstanceOf(WriteConstraintViolationException::class, $exception);

        $violations = $exception->getViolations();
        static::assertEquals(1, $violations->count());
        static::assertEquals($violateMessage, $violations[0]->getMessage());
        static::assertEquals($violatePropertyPath, $violations[0]->getPropertyPath());
        static::assertEquals($violateCode, $violations[0]->getCode());
    }

    public static function getTestUpdateAppointmentWithGuidedModeAndDifferentAccessibleTimeAndExistingAppointmentProviderData(): \Generator
    {
        $noAccessibleFromAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $noAccessibleFromAppointment->setAccessibleFrom(null);
        yield 'payload has guided mode, accessible_to and with existing guided appointment, but no accessible from' => [
            $noAccessibleFromAppointment,
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_GUIDED,
                'guided_shopping_presentation_id' => 'test-presentation-id',
                'guide_user_id' => \md5('test-user-id'),
                'accessible_to' => "2023-07-13T12:42:00.000Z",
            ],
            'The Accessible From should be not empty.',
            '/0/accessibleFrom',
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        ];
        yield 'payload has self mode, accessible_to and with existing guided appointment, but no accessible from' => [
            $noAccessibleFromAppointment,
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'guided_shopping_presentation_id' => 'test-presentation-id',
                'guide_user_id' => \md5('test-user-id'),
                'accessible_to' => "2023-07-13T12:42:00.000Z",
            ],
            'The Accessible From should be not empty.',
            '/0/accessibleFrom',
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        ];

        $noAccessibleToAppointment = (new AppointMentMockHelper())->getAppointEntity();
        $noAccessibleToAppointment->setAccessibleTo(null);
        yield 'payload has guided mode, accessible_from and with existing guided appointment, but no accessible to' => [
            $noAccessibleToAppointment,
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_GUIDED,
                'guide_user_id' => \md5('test-user-id'),
                'guided_shopping_presentation_id' => 'test-presentation-id',
                'accessible_from' => "2023-07-13T12:42:00.000Z",
            ],
            'The Accessible To should be not empty.',
            '/0/accessibleTo',
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        ];
        yield 'payload has self mode, accessible_from and with existing guided appointment, but no accessible to' => [
            $noAccessibleToAppointment,
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'guide_user_id' => \md5('test-user-id'),
                'guided_shopping_presentation_id' => 'test-presentation-id',
                'accessible_from' => "2023-07-13T12:42:00.000Z",
            ],
            'The Accessible To should be not empty.',
            '/0/accessibleTo',
            'c1051bb4-d103-4f74-8988-acbcafc7fdc3'
        ];

        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setAccessibleFrom(new \DateTime('2023-07-11T12:42:00.000Z'));
        $appointment->setAccessibleTo(new \DateTime('2023-07-13T12:42:00.000Z'));
        yield 'payload has accessible_from greater than appointment accessibleTo' => [
            $appointment,
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_GUIDED,
                'guide_user_id' => \md5('test-user-id'),
                'guided_shopping_presentation_id' => 'test-presentation-id',
                'accessible_from' => "2023-07-15T12:42:00.000Z",
            ],
            'Accessible To should be greater than Accessible From.',
            '/0/accessibleTo',
            'INVALID_ACCESSIBLE_TIME'
        ];
        yield 'payload has accessible_to lower than appointment accessibleFrom' => [
            $appointment,
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_GUIDED,
                'guide_user_id' => \md5('test-user-id'),
                'guided_shopping_presentation_id' => 'test-presentation-id',
                'accessible_to' => "2023-07-09T12:42:00.000Z",
            ],
            'Accessible To should be greater than Accessible From.',
            '/0/accessibleTo',
            'INVALID_ACCESSIBLE_TIME'
        ];
    }

    public function testBeforeWriteWithInsertCommand(): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::never())->method('search');
        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $command = new InsertCommand(
            new AppointmentDefinition(),
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'sender' => null
            ],
            [],
            $this->createMock(EntityExistence::class),
            ''
        );

        $event = EntityWriteEvent::create(
            $writeContext,
            [$command]
        );

        $subscriber->beforeWrite($event);

        static::assertArrayHasKey('guided_shopping_presentation_version_id', $command->getPayload());
        static::assertEquals(Defaults::LIVE_VERSION, Uuid::fromBytesToHex($command->getPayload()['guided_shopping_presentation_version_id']));
    }

    public function testBeforeWriteButNotAppointmentDefinition(): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointmentRepository->expects(static::never())->method(static::anything());
        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $command = new InsertCommand(
            new DocumentDefinition(),
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'sender' => null
            ],
            [],
            $this->createMock(EntityExistence::class),
            ''
        );

        $event = EntityWriteEvent::create(
            $writeContext,
            [$command]
        );

        $subscriber->beforeWrite($event);

        static::assertArrayNotHasKey('guided_shopping_presentation_version_id', $command->getPayload());
    }

    public function testBeforeWriteWithUpdateCommandHasValidPrimaryKey(): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setPresentationVersionId(null);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                $this->context
            ));
        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $command = new UpdateCommand(
            new AppointmentDefinition(),
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'sender' => null,
            ],
            ['id' => \md5('test-appointment-id')],
            $this->createMock(EntityExistence::class),
            ''
        );

        $event = EntityWriteEvent::create(
            $writeContext,
            [$command]
        );

        $subscriber->beforeWrite($event);

        static::assertArrayHasKey('guided_shopping_presentation_version_id', $command->getPayload());
        static::assertEquals(Defaults::LIVE_VERSION, Uuid::fromBytesToHex($command->getPayload()['guided_shopping_presentation_version_id']));
    }

    public function testBeforeWriteWithUpdateCommandHasPresentationIdInPayload(): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setPresentationVersionId(null);
        $appointmentRepository->expects(static::never())->method('search');

        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $command = new UpdateCommand(
            new AppointmentDefinition(),
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'sender' => null,
                'guided_shopping_presentation_id' => 'test-presentation-id'
            ],
            ['id' => \md5('test-appointment-id')],
            $this->createMock(EntityExistence::class),
            ''
        );

        $event = EntityWriteEvent::create(
            $writeContext,
            [$command]
        );

        $subscriber->beforeWrite($event);

        static::assertArrayHasKey('guided_shopping_presentation_version_id', $command->getPayload());
        static::assertEquals(Defaults::LIVE_VERSION, Uuid::fromBytesToHex($command->getPayload()['guided_shopping_presentation_version_id']));
    }

    public function testBeforeWriteWithUpdateCommandHasNoPresentationIdInPayload(): void
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $this->createMock(EntityRepository::class);
        $appointment = (new AppointMentMockHelper())->getAppointEntity();
        $appointment->setPresentationVersionId(null);
        $appointmentRepository->expects(static::once())
            ->method('search')
            ->willReturn(new EntitySearchResult(
                AppointmentDefinition::ENTITY_NAME,
                1,
                new AppointmentCollection([$appointment]),
                null,
                new Criteria(),
                $this->context
            ));
        $subscriber = $this->getValidator($appointmentRepository);

        $writeContext = WriteContext::createFromContext($this->context);
        $command = new UpdateCommand(
            new AppointmentDefinition(),
            [
                'presentation_path' => 'test-presentation-path-xyz',
                'mode' => AppointmentDefinition::MODE_SELF,
                'sender' => null
            ],
            ['id' => Uuid::randomBytes()],
            $this->createMock(EntityExistence::class),
            ''
        );

        $event = EntityWriteEvent::create(
            $writeContext,
            [$command]
        );

        $subscriber->beforeWrite($event);

        static::assertArrayHasKey('guided_shopping_presentation_version_id', $command->getPayload());
        static::assertEquals(Defaults::LIVE_VERSION, Uuid::fromBytesToHex($command->getPayload()['guided_shopping_presentation_version_id']));
    }

    public function getValidator(
        ?MockObject $appointmentRepositoryMock = null,
        ?MockObject $presentationRepositoryMock = null
    ): AppointmentValidator
    {
        /** @var MockObject&EntityRepository<AppointmentCollection> $appointmentRepository */
        $appointmentRepository = $appointmentRepositoryMock ?: $this->createMock(EntityRepository::class);
        /** @var MockObject&EntityRepository<PresentationCollection> $presentationRepository */
        $presentationRepository = $presentationRepositoryMock ?: $this->createMock(EntityRepository::class);

        return new AppointmentValidator($appointmentRepository, $presentationRepository);
    }

    private function createAppointmentRepository(?bool $isRun = false, ?bool $foundRecords = false): MockObject
    {
        $appointmentRepository = $this->createMock(EntityRepository::class);

        if ($isRun) {
            if ($foundRecords) {
               $result = new IdSearchResult(
                   1,
                   [['data' => ['test-appointment-id'], 'primaryKey' => 'test-appointment-id']],
                   new Criteria(),
                   new Context(new SystemSource())
               );
            } else {
                $result = new IdSearchResult(
                    0,
                    [],
                    new Criteria(),
                    new Context(new SystemSource())
                );
            }

            $appointmentRepository->expects(static::once())
                ->method('searchIds')
                ->willReturn($result);
        } else {
            $appointmentRepository->expects(static::never())
                ->method('searchIds');
        }

        return $appointmentRepository;
    }
}
