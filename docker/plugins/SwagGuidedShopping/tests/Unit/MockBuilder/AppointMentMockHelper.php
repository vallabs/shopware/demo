<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;

class AppointMentMockHelper
{
    public function getAppointEntity(?bool $videoChatNameIsNull = false): AppointmentEntity
    {
        $appointmentResult = new AppointmentEntity();
        $id = \md5('testAppointment');
        $appointmentResult->setId($id);
        $appointmentResult->setUniqueIdentifier($id);
        $appointmentResult->setName('test-appointment-name');
        $appointmentResult->setStartedAt(null);
        $appointmentResult->setEndedAt(null);
        $appointmentResult->setMode(AppointmentDefinition::MODE_GUIDED);
        $appointmentResult->setPresentationId('presentation_test_1');
        $appointmentResult->setAttendeeRestrictionType(AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_OPEN);
        $appointmentResult->setAccessibleTo(null);
        $appointmentResult->setAccessibleFrom(null);
        $salesChannelDomain = new SalesChannelDomainEntity();
        $salesChannelDomain->setId('test_sales_channel_domain_id');
        $salesChannelDomain->setUrl('http://test.com');
        $salesChannelDomain->setSalesChannelId('test_sales_channel_id');
        $salesChannelDomain->setLanguageId(\md5('language'));
        $appointmentResult->setSalesChannelDomainId('test_sales_channel_domain_id');
        $appointmentResult->setSalesChannelDomain($salesChannelDomain);
        $appointmentResult->setPresentationPath('test-presentation-path');

        $videoChat = new VideoChatEntity();
        $videoChat->setOwnerToken(null);
        $videoChat->setUrl(null);
        $videoChat->setUserToken(null);
        if ($videoChatNameIsNull) {
            $videoChat->setName(null);
        } else {
            $videoChat->setName('room_1');
        }
        $videoChat->setStartAsBroadcast(true);
        $videoChat->setAppointmentId($id);
        $videoChat->setId(\md5('testVideo'));

        $appointmentResult->setVideoChat($videoChat);
        $appointmentResult->setVideoAudioSettings(AppointmentDefinition::NONE_VIDEO_AUDIO);

        return $appointmentResult;
    }

    /**
     * @param array<Entity> $entities
     * @return EntitySearchResult<EntityCollection<Entity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(AppointmentEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }

    /**
     * @param array<Entity> $entities
     * @return EntitySearchResult<EntityCollection<Entity>>
     */
    public function createVideoChatSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(VideoChatEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }

    /**
     * @param array<Entity> $entities
     * @return EntitySearchResult<EntityCollection<Entity>>
     */
    public function createPresentationSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(PresentationEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }

    /**
     * @param array<PresentationCmsPageEntity> $entities
     * @return EntitySearchResult<EntityCollection<PresentationCmsPageEntity>>
     */
    public function createPresentationCmsPageSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(PresentationCmsPageEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }
}
