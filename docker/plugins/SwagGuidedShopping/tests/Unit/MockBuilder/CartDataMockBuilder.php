<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Tax\Struct\CalculatedTax;
use Shopware\Core\Checkout\Cart\Tax\Struct\CalculatedTaxCollection;
use Shopware\Core\Checkout\Cart\Tax\Struct\TaxRuleCollection;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagGuidedShopping\Core\Checkout\Cart\SalesChannel\CartItemAddRouteDecorator;

class CartDataMockBuilder
{
    /**
     * @return array<int, array<string, float|string>>
     */
    public function createDemoCartData(string $taxType = 'gross', string $token = 'test-token', bool $moreItems = false): array
    {
        $cart = $this->createCartWithTaxType($taxType, $token);

        $items = [
            [
                'number' => 1,
                'unitPrice' => 50,
                'quantity' => 4,
                'isGuidedShoppingContext' => true,
            ],
            [
                'number' => 2,
                'unitPrice' => 40.50,
                'quantity' => 3,
                'isGuidedShoppingContext' => true,
            ],
            [
                'number' => 3,
                'unitPrice' => 10,
                'quantity' => 40,
                'isGuidedShoppingContext' => true,
            ],
            [
                'number' => 4,
                'unitPrice' => 13.24,
                'quantity' => 12,
                'isGuidedShoppingContext' => true,
            ]
        ];

        if ($moreItems) {
            $items[] = [
                'number' => 5,
                'unitPrice' => 1.54,
                'quantity' => 54,
                'isGuidedShoppingContext' => true,
            ];

            $items[] = [
                'number' => 6,
                'unitPrice' => 70.4,
                'quantity' => 32,
                'isGuidedShoppingContext' => true,
            ];
        }

        $countItem = 1;
        foreach ($items as $item) {
            $lineItem = $this->getLineItem($item['number'], $item['unitPrice'], $item['quantity'], $item['isGuidedShoppingContext']);
            switch ($countItem) {
                case 1:
                    $lineItem->setReferencedId(null);
                    break;
                case 2:
                    $lineItem->setPrice(null);
                    break;
                case 3:
                    $lineItem->setPayloadValue(CartItemAddRouteDecorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER, false);
                    break;
            }

            $cart->add($lineItem);
            $countItem++;
        }

        $currencyEuroId = \md5('currency_EUR');
        $cartRow = [
            'payload' => \serialize($cart),
            'currency_id' => Uuid::fromHexToBytes($currencyEuroId),
            'attendee_id' => 'attendee_1',
        ];
        if ($cart->getPrice()->hasNetPrices()) {
            $cartRow['price'] = $cart->getPrice()->getNetPrice();
        }
        $cartData[] = $cartRow;

        return $cartData;
    }

    private function createCartWithTaxType(string $taxType, string $token): Cart
    {
        $cart = new Cart($token);

        if (\in_array($taxType, [CartPrice::TAX_STATE_NET, CartPrice::TAX_STATE_FREE], true)) {
            $cartPrice = new CartPrice(0, 0, 0, new CalculatedTaxCollection(), new TaxRuleCollection(), CartPrice::TAX_STATE_NET);
            $cart->setPrice($cartPrice);
        }

        return $cart;
    }

    private function getLineItem(int $number, float $grossUnitPrice, int $quantity, bool $isGuidedShoppingContext): LineItem
    {
        $lineItem = new LineItem('item-' . $number, 'product', 'item-' . $number, $quantity);
        $calculatedTax = new CalculatedTaxCollection();
        $totalPrice = $grossUnitPrice * $quantity;
        $tax = new CalculatedTax($totalPrice - ($totalPrice / 1.19), 19.0, 0.0);
        $calculatedTax->add($tax);
        $calculatedPrice = new CalculatedPrice($grossUnitPrice, ($grossUnitPrice * $quantity), $calculatedTax, new TaxRuleCollection(), $quantity);

        if ($isGuidedShoppingContext) {
            $lineItem->setPayloadValue(CartItemAddRouteDecorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER, true);
        }

        $lineItem->setPrice($calculatedPrice);

        return $lineItem;
    }
}
