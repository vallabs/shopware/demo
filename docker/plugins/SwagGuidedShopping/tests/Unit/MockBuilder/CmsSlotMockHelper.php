<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\FieldConfigCollection;
use Shopware\Core\Content\Cms\SalesChannel\Struct\TextStruct;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;

class CmsSlotMockHelper
{
    /**
     * @param string $id
     * @param string $blockId
     * @param array<string, mixed>|null $config
     * @param FieldConfigCollection $fieldConfig
     * @return CmsSlotEntity
     */
    public function getCmsSlotEntity(string $id, string $blockId, ?array $config, FieldConfigCollection $fieldConfig): CmsSlotEntity
    {
        $cmsSlot = new CmsSlotEntity();
        $cmsSlot->setId($id);
        $cmsSlot->setType('text');
        $cmsSlot->setSlot('content');
        $cmsSlot->setBlockId($blockId);
        $cmsSlot->setFieldConfig($fieldConfig);
        $cmsSlot->setTranslated([
            'config' => $config
        ]);
        $cmsSlot->setData(new TextStruct());
        $cmsSlot->setLocked(true);

        return $cmsSlot;
    }

    /**
     * @param array<int, CmsSlotEntity> $entities
     * @return EntitySearchResult<EntityCollection<CmsSlotEntity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(CmsSlotEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }
}
