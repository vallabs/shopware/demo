<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Test\Stub\DataAbstractionLayer\StaticEntityRepository;
use SwagGuidedShopping\Content\VideoChat\Service\Client\DailyClient;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;

class VideoManagerMockBuilder
{
    /**
     * @param StaticEntityRepository<VideoChatCollection>|null $videoChatRepositoryMock
     */
    public function build(?MockHandler $mockHandler = null, ?StaticEntityRepository $videoChatRepositoryMock = null): VideoRoomManager
    {
        $handlerStack = HandlerStack::create($mockHandler);

        $dailyClient = new DailyClient('', '', ['handler' => $handlerStack]);

        /** @var EntityRepository<VideoChatCollection>&StaticEntityRepository<VideoChatCollection> $videoChatRepository */
        $videoChatRepository = $videoChatRepositoryMock ?? new StaticEntityRepository([]);

        return new VideoRoomManager($dailyClient, $videoChatRepository);
    }
}
