<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

/**
 * @implements \Iterator<array-key, mixed>
 */
class IteratorMockHelper implements \Iterator
{
    /**
     * @var array<string|int, mixed>
     */
    private array $items;
    private int $pointer = 0;

    /**
     * @param array<string|int, mixed> $items
     */
    public function __construct(array $items) {
        $this->items = \array_values($items);
    }

    public function current(): mixed {
        return $this->items[$this->pointer];
    }

    public function key(): int {
        return $this->pointer;
    }

    public function next(): void {
        $this->pointer++;
    }

    public function rewind(): void {
        $this->pointer = 0;
    }

    public function valid(): bool
    {
        return $this->pointer < \count($this->items);
    }
}
