<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;

class CmsPageMockHelper
{
    public function getCmsPageEntity(string $id): CmsPageEntity
    {
        $cmsPage = new CmsPageEntity();
        $cmsPage->setId($id);
        $cmsPage->setName("testCmsPage{$id}");
        $cmsPage->setType('presentation_landingpage');

        return $cmsPage;
    }

    /**
     * @param array<int, CmsPageEntity> $entities
     * @return EntitySearchResult<EntityCollection<CmsPageEntity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(CmsPageEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }
}
