<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;

class CmsSectionMockHelper
{
    public function getCmsSectionEntity(string $id, string $pageId, ?int $position, ?CmsBlockCollection $blocks, bool $hasSectionName = true): CmsSectionEntity
    {
        $cmsSection = new CmsSectionEntity();
        $cmsSection->setId($id);
        $cmsSection->setPageId($pageId);
        $cmsSection->setType('default');
        $cmsSection->setPosition($position ?? 0);
        $cmsSection->setLocked(true);
        if ($hasSectionName) {
            $cmsSection->setName("testCmsSection{$id}");
        }
        if ($blocks) {
            $cmsSection->setBlocks($blocks);
        }

        return $cmsSection;
    }

    /**
     * @param array<int, CmsSectionEntity> $entities
     * @return EntitySearchResult<EntityCollection<CmsSectionEntity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(CmsSectionEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }
}
