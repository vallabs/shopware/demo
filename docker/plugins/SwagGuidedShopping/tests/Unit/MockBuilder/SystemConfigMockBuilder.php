<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\System\SystemConfig\SystemConfigLoader;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use Shopware\Core\System\SystemConfig\Util\ConfigReader;
use SwagGuidedShopping\Struct\ConfigKeys;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * @internal
 * @coversNothing
 */
class SystemConfigMockBuilder extends TestCase
{
    /**
     * @param array<string, mixed> $config
     */
    public function create(array $config = []): SystemConfigService
    {
        $defaultConfig = $this->getDefaultConfig();
        $config = $this->decorateConfig($defaultConfig, $config);

        $connection = $this->createMock(Connection::class);
        $loader = $this->createMock(SystemConfigLoader::class);

        $config = $this->buildTree($config);

        $loader->expects(static::any())->method('load')->willReturn($config);

        return new SystemConfigService(
            $connection,
            $this->createMock(ConfigReader::class),
            $loader,
            $this->createMock(EventDispatcher::class),
            true
        );
    }

    /**
     * @param array<string, mixed> $config
     * @return array<string, mixed>
     */
    private function buildTree(array $config): array
    {
        $array = [];
        $items = $config;
        foreach ($items as $key => $item) {
            $parts = \explode('.', $key);
            $last = \array_pop($parts);

            $ref = &$array;
            foreach ($parts as $part) {
                $ref = &$ref[$part];
            }

            $ref[$last] = $item;
        }

        return $array;
    }

    /**
     * @return array<string, mixed>
     */
    private function getDefaultConfig(): array
    {
        $defaultConfig[ConfigKeys::MERCURE_HUB_SUBSCRIBER_SECRET] = 'shopware-subscriber';
        $defaultConfig[ConfigKeys::MERCURE_HUB_PUBLISHER_SECRET] = 'shopware-publisher';
        $defaultConfig[ConfigKeys::MERCURE_HUB_URL] = 'http://test';
        $defaultConfig[ConfigKeys::MERCURE_HUB_PUBLIC_URL] = 'https://test';
        $defaultConfig[ConfigKeys::PRODUCT_DETAIL_DEFAULT_PAGE_ID] = 'bea211b5099241719830df8026624f7f';
        $defaultConfig[ConfigKeys::PRODUCT_LISTING_DEFAULT_PAGE_ID] = 'bea211b5099241719830df8026624f7f';
        $defaultConfig[ConfigKeys::PRESENTATION_ENDED_PAGE_ID] = 'bea211b5099241719830df8026624f7f';
        $defaultConfig[ConfigKeys::QUICK_VIEW_PAGE_ID] = 'bea211b5099241719830df8026624f7f';
        $defaultConfig[ConfigKeys::ALLOW_USER_ACTIONS_FOR_GUIDE] = true;

        return $defaultConfig;
    }

    /**
     * @param array<string, mixed> $defaultConfig
     * @param array<string, mixed> $config
     * @return array<string, mixed>
     */
    private function decorateConfig(array $defaultConfig, array $config): array
    {
        foreach ($config as $key => $value) {
            $defaultConfig[$key] = $value;
        }

        return $defaultConfig;
    }
}
