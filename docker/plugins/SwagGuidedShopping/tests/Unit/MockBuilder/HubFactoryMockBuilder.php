<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use SwagGuidedShopping\Content\Mercure\Factory\HubFactory;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;

class HubFactoryMockBuilder
{
    /**
     * @param array<string, mixed> $config
     */
    public function create(array $config = []): HubFactory
    {
        $systemConfigMockBuilder = new SystemConfigMockBuilder('test');

        $configSystemMock = $systemConfigMockBuilder->create($config);
        $JwtMercureTokenFactory = new JWTMercureTokenFactory($configSystemMock);

        return new HubFactory($configSystemMock, $JwtMercureTokenFactory);
    }
}
