<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;

class PresentationMockHelper
{
    public function getPresentationEntity(string $id): PresentationEntity
    {
        $presentation = new PresentationEntity();
        $presentation->setId(\md5($id));
        $presentation->setName('test-presentation');
        $presentation->setActive(true);

        return $presentation;
    }

    /**
     * @param array<int, PresentationEntity> $entities
     * @return EntitySearchResult<EntityCollection<PresentationEntity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(
            PresentationEntity::class,
            \count($entities), (new EntityCollection($entities)),
            null,
            new Criteria(),
            new Context(new SystemSource())
        );
    }
}
