<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotCollection;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;

class CmsBlockMockHelper
{
    public function getCmsBlockEntity(?string $id, string $sectionId, int $position, ?CmsSlotCollection $slots = null): CmsBlockEntity
    {
        $cmsBlock = new CmsBlockEntity();
        $cmsBlock->setId($id ?? 'testCmsBlock');
        $cmsBlock->setType('text');
        $cmsBlock->setSectionId($sectionId);
        if ($slots) {
            $cmsBlock->setSlots($slots);
        }
        $cmsBlock->setPosition($position);
        $cmsBlock->setName("testCmsBlock{$id}");
        $cmsBlock->setSectionPosition('main');
        $cmsBlock->setLocked(true);

        return $cmsBlock;
    }

    /**
     * @param array<int, CmsBlockEntity> $entities
     * @return EntitySearchResult<EntityCollection<CmsBlockEntity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(CmsBlockEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }
}
