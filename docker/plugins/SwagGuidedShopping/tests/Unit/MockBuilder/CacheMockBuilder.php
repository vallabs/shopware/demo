<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\CacheItem;

/**
 * @internal
 * @coversNothing
 */
class CacheMockBuilder extends TestCase
{
    /**
     * @param array<string, mixed> $cacheValue
     */
    public function generateCacheItem(string $key, $cacheValue, bool $isHit = true): CacheItem
    {
        $item = new CacheItem();
        $item->set($cacheValue);
        $class = new \ReflectionClass(CacheItem::class);
        $itemKey = $class->getProperty('key');
        $itemKey->setAccessible(true);
        $itemKey->setValue($item, $key);
        $isHitProp = $class->getProperty('isHit');
        $isHitProp->setAccessible(true);
        $isHitProp->setValue($item, $isHit);

        return $item;
    }
}
