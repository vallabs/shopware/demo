<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;

class VideoChatMockHelper
{
    public function getVideoChatEntity(): VideoChatEntity
    {
        $videoChat = new VideoChatEntity();
        $videoChat->setId('test-video-chat-id');
        $videoChat->setName('test-video-chat-name');
        $videoChat->setUrl('test-video-chat-url');
        $videoChat->setAppointmentId('test-appointment-id');
        $videoChat->setOwnerToken('test-owner-token');
        $videoChat->setUserToken('test-user-token');
        $videoChat->setStartAsBroadcast(true);

        return $videoChat;
    }

    /**
     * @param array<int, VideoChatEntity> $entities
     * @return EntitySearchResult<VideoChatCollection>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(
            VideoChatEntity::class,
            \count($entities),
            new VideoChatCollection($entities),
            null,
            new Criteria(),
            new Context(new SystemSource())
        );
    }
}
