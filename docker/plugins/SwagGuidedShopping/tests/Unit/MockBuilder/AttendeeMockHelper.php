<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\MockBuilder;

use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;

class AttendeeMockHelper
{
    public function getAttendeeEntity(string $appointmentId, string $type): AttendeeEntity
    {
        $attendee = new AttendeeEntity();

        $attendee->setId('test-attendee-id');
        $attendee->setType($type);
        $attendee->setAppointmentId($appointmentId);
        $attendee->setAttendeeName('test-attendee-name');
        $attendee->setAttendeeEmail('test-attendee-email');
        $attendee->setAttendeeSubmittedAt(new \DateTimeImmutable());

        return $attendee;
    }

    /**
     * @param array<Entity> $entities
     * @return EntitySearchResult<EntityCollection<Entity>>
     */
    public function createSearchResult(array $entities = []): EntitySearchResult
    {
        return new EntitySearchResult(AttendeeEntity::class, \count($entities), (new EntityCollection($entities)), null, new Criteria(), new Context(new SystemSource()));
    }
}
