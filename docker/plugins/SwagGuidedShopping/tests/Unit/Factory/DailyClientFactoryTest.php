<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Unit\Factory;

use PHPUnit\Framework\TestCase;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Factory\DailyClientFactory;

class DailyClientFactoryTest extends TestCase
{
    public function testCreateClient(): void
    {
        $systemConfigService = $this->createMock(SystemConfigService::class);
        $systemConfigService
            ->expects(static::exactly(2))
            ->method('getString')
            ->willReturnOnConsecutiveCalls('test-base-url', 'test-api-key');

        $dailyClientFactory = new DailyClientFactory($systemConfigService);
        $dailyClientFactory->createClient();
    }
}
