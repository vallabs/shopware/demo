<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Tests\Migration\MigrationTestTrait;
use SwagGuidedShopping\Migration\Migration1648897694CreatedBy;

class Migration1648897694CreatedByTest extends TestCase
{
    use MigrationTestTrait;

    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    /**
     * @before
     */
    public function initialise(): void
    {
        $connection = KernelLifecycleManager::getConnection();
        $migration = new Migration1648897694CreatedBy();

        $connection->rollBack();
        $migration->update($connection);

        if ($this->hasColumn($connection, 'guided_shopping_presentation', 'created_by_id')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP FOREIGN KEY `fk.guided_shopping_presentation.created_by_id`;');
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP COLUMN `created_by_id`;');
        }

        if ($this->hasColumn($connection, 'guided_shopping_presentation', 'updated_by_id')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP FOREIGN KEY `fk.guided_shopping_presentation.updated_by_id`;');
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP COLUMN `updated_by_id`;');
        }

        if ($this->hasColumn($connection, 'guided_shopping_appointment', 'created_by_id')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment` DROP FOREIGN KEY `fk.guided_shopping_appointment.created_by_id`;');
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment` DROP COLUMN `created_by_id`;');
        }

        if ($this->hasColumn($connection, 'guided_shopping_appointment', 'updated_by_id')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment` DROP FOREIGN KEY `fk.guided_shopping_appointment.updated_by_id`;');
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment` DROP COLUMN `updated_by_id`;');
        }

        $migration->update($connection);
        $migration->updateDestructive($connection);

        $connection->beginTransaction();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1648897694;
        $migration = new Migration1648897694CreatedBy();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $this->expectConstraints('guided_shopping_presentation');
        $this->expectConstraints('guided_shopping_appointment');
    }

    private function hasColumn(Connection $connection, string $table, string $columnName): bool
    {
        return \count(\array_filter(
                $connection->createSchemaManager()->listTableColumns($table),
                static fn (Column $column): bool => $column->getName() === $columnName
            )) > 0;
    }

    private function expectConstraints(string $tableName): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, $tableName, 'created_by_id'));
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, $tableName, 'updated_by_id'));
    }
}
