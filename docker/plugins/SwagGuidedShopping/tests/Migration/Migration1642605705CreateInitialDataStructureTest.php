<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use SwagGuidedShopping\Migration\Migration1642605705CreateInitialDataStructure;

class Migration1642605705CreateInitialDataStructureTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1642605705;
        $migration = new Migration1642605705CreateInitialDataStructure();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1642605705CreateInitialDataStructure();

        $migration->update($this->connection);
        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        $this->expectTableHasColumns('guided_shopping_appointment');
        $this->expectTableHasColumns('guided_shopping_appointment_attendee');
        $this->expectTableHasColumns('guided_shopping_appointment_video_chat');
        $this->expectTableHasColumns('guided_shopping_attendee_product_collection');
        $this->expectTableHasColumns('guided_shopping_interaction');
        $this->expectTableHasColumns('guided_shopping_presentation');
        $this->expectTableHasColumns('guided_shopping_presentation_translation');
        $this->expectTableHasColumns('guided_shopping_presentation_cms_page');
        $this->expectTableHasColumns('guided_shopping_presentation_cms_page_translation');
    }

    private function expectTableHasColumns(string $tableName): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::tableExists($this->connection, $tableName));
        $exists = $this->connection->fetchOne("SELECT COUNT(*) FROM `{$tableName}`") !== false;
        static::assertTrue($exists);
    }
}
