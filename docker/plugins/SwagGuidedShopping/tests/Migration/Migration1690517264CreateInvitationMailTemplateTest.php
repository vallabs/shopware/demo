<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use SwagGuidedShopping\Migration\Migration1690517264CreateInvitationMailTemplate;

class Migration1690517264CreateInvitationMailTemplateTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1690517264;
        $migration = new Migration1690517264CreateInvitationMailTemplate();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1690517264CreateInvitationMailTemplate();
        $migration->update($this->connection);

        $this->connection->delete('mail_template_type', [
            'id' => '0A7ACFDA21AD4052869219EE884AC069',
        ]);
        $this->connection->delete('mail_template_type', [
            'id' => 'E7AFC9E22B2B42F4A015A8389AF245B5',
        ]);
        $this->connection->delete('mail_template', [
            'id' => '54682107D9924907A4D524760B17AECF',
        ]);
        $this->connection->delete('mail_template', [
            'id' => 'EEFEAC3598F84A23AFCB4485384032DE',
        ]);

        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        static::assertTrue($this->checkMailTemplateTypeIsExists('0A7ACFDA21AD4052869219EE884AC069'));
        static::assertTrue($this->checkMailTemplateTypeIsExists('E7AFC9E22B2B42F4A015A8389AF245B5'));
        static::assertTrue($this->checkMailTemplateIsExists('54682107D9924907A4D524760B17AECF'));
        static::assertTrue($this->checkMailTemplateIsExists('EEFEAC3598F84A23AFCB4485384032DE'));
    }

    private function checkMailTemplateTypeIsExists(string $mailTemplateTypeId): bool
    {
        $record = $this->connection->fetchOne("
            SELECT COUNT(*)
            FROM `mail_template_type`
            WHERE `id` = UNHEX('{$mailTemplateTypeId}');
        ");
        return $record !== false;
    }

    private function checkMailTemplateIsExists(string $mailTemplateId): bool
    {
        $record = $this->connection->fetchOne("
            SELECT COUNT(*)
            FROM `mail_template`
            WHERE `id` = UNHEX('{$mailTemplateId}');
        ");
        return $record !== false;
    }
}
