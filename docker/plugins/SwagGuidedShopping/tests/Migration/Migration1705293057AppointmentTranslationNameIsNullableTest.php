<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use SwagGuidedShopping\Migration\Migration1705293057AppointmentTranslationNameIsNullable;

class Migration1705293057AppointmentTranslationNameIsNullableTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1705293057;
        $migration = new Migration1705293057AppointmentTranslationNameIsNullable();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1705293057AppointmentTranslationNameIsNullable();
        $migration->update($this->connection);

        if (!$this->checkIfNameColumnExists()) {
            $this->connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `name` VARCHAR(255) NOT NULL AFTER `id`;
            ');
        }

        $migration->update($this->connection);
        $this->checkIfTheNameColumnIsNullable();

        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);
        $this->checkIfTheNameColumnIsNullable();
    }

    private function checkIfNameColumnExists(): bool
    {
        $result = $this->connection->fetchOne("
            SHOW COLUMNS FROM `guided_shopping_appointment` LIKE 'name';
        ");

        return $result !== false;
    }

    private function checkIfTheNameColumnIsNullable(): void
    {
        $result = $this->connection->fetchAllAssociative("SHOW COLUMNS FROM `guided_shopping_appointment_translation` LIKE 'name'");

        static::assertCount(1, $result);
        static::assertArrayHasKey('Null', $result[0]);
        static::assertEquals('YES', $result[0]['Null']);
    }
}
