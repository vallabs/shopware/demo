<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagGuidedShopping\Migration\Migration1701771598ModifyDefaultLayoutsInShoppingExperience;

class Migration1701771598ModifyDefaultLayoutsInShoppingExperienceTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1701771598;
        $migration = new Migration1701771598ModifyDefaultLayoutsInShoppingExperience();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1701771598ModifyDefaultLayoutsInShoppingExperience();
        $migration->update($this->connection);

        $defaultProductListingPageId = '33E88C7994FA4CF79A1265E5105B93B2';
        $defaultProductDetailPageId = 'BEA211B5099241719830DF8026624F7F';
        $defaultEndPresentationPageId = '8EA80092FAA744559409F3E9F7ADCC6B';
        $defaultQuickviewPageId = '182D3F7F988044ADBBA449B70C8BC472';

        $pageIds = [
            $defaultProductListingPageId,
            $defaultProductDetailPageId,
            $defaultEndPresentationPageId,
            $defaultQuickviewPageId,
        ];

        static::assertTrue($this->checkDSRLayoutsIsLocked($pageIds));
        static::assertTrue($this->checkDSRLayoutTranslations($pageIds));

        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        static::assertTrue($this->checkDSRLayoutsIsLocked($pageIds));
        static::assertTrue($this->checkDSRLayoutTranslations($pageIds));
    }

    /**
     * @param array<string> $pageIds
     */
    private function checkDSRLayoutsIsLocked(array $pageIds): bool
    {
        $sql = "
            SELECT COUNT(`id`) FROM `cms_page` 
            WHERE `locked` = 1 
                AND `id` IN (:ids)
                AND `version_id` = :versionId
        ";

        $result = $this->connection->fetchOne(
            $sql,
            [
                'ids' => Uuid::fromHexToBytesList($pageIds),
                'versionId' => Uuid::fromHexToBytes(Defaults::LIVE_VERSION),
            ],
            [
                'ids' => ArrayParameterType::STRING,
            ]
        );

        return $result !== false;
    }

    /**
     * @param array<string> $pageIds
     */
    private function checkDSRLayoutTranslations(array $pageIds): bool
    {
        $sql = "
            SELECT COUNT(*) FROM `cms_page_translation` 
            WHERE `cms_page_id` IN (:ids)
        ";

        $result = $this->connection->fetchNumeric(
            $sql,
            [
                'ids' => Uuid::fromHexToBytesList($pageIds),
            ],
            [
                'ids' => ArrayParameterType::STRING,
            ]
        );

        return $result !== false;
    }
}
