<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Tests\Migration\MigrationTestTrait;
use SwagGuidedShopping\Migration\Migration1666081431AddPreviewPresentationIntoPresentationTable;

class Migration1666081431AddPreviewPresentationIntoPresentationTableTest extends TestCase
{
    use MigrationTestTrait;

    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    /**
     * @before
     */
    public function initialise(): void
    {
        $connection = KernelLifecycleManager::getConnection();
        $migration = new Migration1666081431AddPreviewPresentationIntoPresentationTable();

        $connection->rollBack();
        $migration->update($connection);

        if ($this->hasColumn($connection, 'parent_id') && $this->hasColumn($connection, 'parent_version_id')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP FOREIGN KEY `fk.guided_shopping_presentation.parent_id`;');
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP COLUMN `parent_id`;');
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation` DROP COLUMN `parent_version_id`;');
        }

        $migration->update($connection);
        $migration->updateDestructive($connection);

        $connection->beginTransaction();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1666081431;
        $migration = new Migration1666081431AddPreviewPresentationIntoPresentationTable();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'guided_shopping_presentation', 'parent_id'));
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'guided_shopping_presentation', 'parent_version_id'));
    }

    private function hasColumn(Connection $connection, string $columnName): bool
    {
        return \count(\array_filter(
                $connection->createSchemaManager()->listTableColumns('guided_shopping_presentation'),
                static fn (Column $column): bool => $column->getName() === $columnName
            )) > 0;
    }
}
