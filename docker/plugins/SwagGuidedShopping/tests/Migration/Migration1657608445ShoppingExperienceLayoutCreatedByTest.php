<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Tests\Migration\MigrationTestTrait;
use SwagGuidedShopping\Migration\Migration1657608445ShoppingExperienceLayoutCreatedBy;

class Migration1657608445ShoppingExperienceLayoutCreatedByTest extends TestCase
{
    use MigrationTestTrait;

    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    /**
     * @before
     */
    public function initialise(): void
    {
        $connection = KernelLifecycleManager::getConnection();
        $migration = new Migration1657608445ShoppingExperienceLayoutCreatedBy();

        $connection->rollBack();
        $migration->update($connection);

        if ($this->hasColumn($connection, 'created_by_id')) {
            $connection->executeStatement('ALTER TABLE `cms_page` DROP FOREIGN KEY `fk.cms_page.created_by_id`;');
            $connection->executeStatement('ALTER TABLE `cms_page` DROP COLUMN `created_by_id`;');
        }

        $migration->update($connection);
        $migration->updateDestructive($connection);

        $connection->beginTransaction();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1657608445;
        $migration = new Migration1657608445ShoppingExperienceLayoutCreatedBy();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'cms_page', 'created_by_id'));
    }

    private function hasColumn(Connection $connection, string $columnName): bool
    {
        return \count(\array_filter(
                $connection->createSchemaManager()->listTableColumns('cms_page'),
                static fn (Column $column): bool => $column->getName() === $columnName
            )) > 0;
    }
}
