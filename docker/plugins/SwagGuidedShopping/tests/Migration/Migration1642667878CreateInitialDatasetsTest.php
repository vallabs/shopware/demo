<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use SwagGuidedShopping\Migration\Migration1642667878CreateInitialDatasets;

class Migration1642667878CreateInitialDatasetsTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1642667878;
        $migration = new Migration1642667878CreateInitialDatasets();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1642667878CreateInitialDatasets();
        $migration->update($this->connection);

        $this->connection->delete('cms_page', [
            'id' => '33E88C7994FA4CF79A1265E5105B93B2',
        ]);
        $this->connection->delete('cms_page', [
            'id' => 'BEA211B5099241719830DF8026624F7F',
        ]);
        $this->connection->delete('cms_page', [
            'id' => '8EA80092FAA744559409F3E9F7ADCC6B',
        ]);
        $this->connection->delete('cms_page', [
            'id' => '182D3F7F988044ADBBA449B70C8BC472',
        ]);

        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        static::assertTrue($this->checkCmsPageIsExists('33E88C7994FA4CF79A1265E5105B93B2', 'presentation_product_list'));
        static::assertTrue($this->checkCmsPageIsExists('BEA211B5099241719830DF8026624F7F', 'presentation_product_detail'));
        static::assertTrue($this->checkCmsPageIsExists('8EA80092FAA744559409F3E9F7ADCC6B', 'presentation_product_list'));
        static::assertTrue($this->checkCmsPageIsExists('182D3F7F988044ADBBA449B70C8BC472', 'product_detail'));
    }

    private function checkCmsPageIsExists(string $cmsPageId, string $type): bool
    {
        $record = $this->connection->fetchOne("
            SELECT COUNT(*)
            FROM `cms_page`
            WHERE `id` = UNHEX('{$cmsPageId}') AND `type` = '{$type}';
        ");
        return $record !== false;
    }
}
