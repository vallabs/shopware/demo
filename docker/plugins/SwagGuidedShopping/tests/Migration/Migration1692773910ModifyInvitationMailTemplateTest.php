<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use SwagGuidedShopping\Migration\Migration1692773910ModifyInvitationMailTemplate;

class Migration1692773910ModifyInvitationMailTemplateTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1692773910;
        $migration = new Migration1692773910ModifyInvitationMailTemplate();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1692773910ModifyInvitationMailTemplate();
        $migration->update($this->connection);

        $this->connection->delete('mail_template_translation', [
            'mail_template_id' => '54682107D9924907A4D524760B17AECF'
        ]);

        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        static::assertTrue($this->checkMailTemplateTranslationIsExists('54682107D9924907A4D524760B17AECF'));
    }

    private function checkMailTemplateTranslationIsExists(string $mailTemplateTypeId): bool
    {
        $record = $this->connection->fetchOne("
            SELECT COUNT(*)
            FROM `mail_template_translation`
            WHERE `mail_template_id` = UNHEX('{$mailTemplateTypeId}');
        ");
        return $record !== false;
    }
}
