<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Tests\Migration\MigrationTestTrait;
use SwagGuidedShopping\Migration\Migration1662540955AddIsInstantListingToPresentationCmsPageTable;

class Migration1662540955AddIsInstantListingToPresentationCmsPageTableTest extends TestCase
{
    use MigrationTestTrait;

    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    /**
     * @before
     */
    public function initialise(): void
    {
        $connection = KernelLifecycleManager::getConnection();
        $migration = new Migration1662540955AddIsInstantListingToPresentationCmsPageTable();

        $connection->rollBack();
        $migration->update($connection);

        if ($this->hasColumn($connection, 'is_instant_listing')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_presentation_cms_page` DROP COLUMN `is_instant_listing`;');
        }

        $migration->update($connection);
        $migration->updateDestructive($connection);

        $connection->beginTransaction();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1662540955;
        $migration = new Migration1662540955AddIsInstantListingToPresentationCmsPageTable();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'guided_shopping_presentation_cms_page', 'is_instant_listing'));
    }

    private function hasColumn(Connection $connection, string $columnName): bool
    {
        return \count(\array_filter(
                $connection->createSchemaManager()->listTableColumns('guided_shopping_presentation_cms_page'),
                static fn (Column $column): bool => $column->getName() === $columnName
            )) > 0;
    }
}
