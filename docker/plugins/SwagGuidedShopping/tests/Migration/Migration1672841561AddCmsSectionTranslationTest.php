<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use SwagGuidedShopping\Migration\Migration1672841561AddCmsSectionTranslation;

class Migration1672841561AddCmsSectionTranslationTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1672841561;
        $migration = new Migration1672841561AddCmsSectionTranslation();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1672841561AddCmsSectionTranslation();

        $migration->update($this->connection);
        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        $this->expectTableExistsAndColumnCount('cms_section_translation');
    }

    private function expectTableExistsAndColumnCount(string $tableName): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::tableExists($this->connection, $tableName));
        $exists = $this->connection->fetchOne("SELECT COUNT(*) FROM `{$tableName}`") !== false;
        static::assertTrue($exists);
    }
}
