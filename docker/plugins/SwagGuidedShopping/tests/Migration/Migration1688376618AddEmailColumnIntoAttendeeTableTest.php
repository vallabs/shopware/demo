<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Tests\Migration\MigrationTestTrait;
use SwagGuidedShopping\Migration\Migration1688376618AddEmailColumnIntoAttendeeTable;

class Migration1688376618AddEmailColumnIntoAttendeeTableTest extends TestCase
{
    use MigrationTestTrait;

    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    /**
     * @before
     */
    public function initialise(): void
    {
        $connection = KernelLifecycleManager::getConnection();
        $migration = new Migration1688376618AddEmailColumnIntoAttendeeTable();

        $connection->rollBack();
        $migration->update($connection);

        if ($this->hasColumn($connection, 'attendee_email')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment_attendee` DROP COLUMN `attendee_email`;');
        }

        $migration->update($connection);
        $migration->updateDestructive($connection);

        $connection->beginTransaction();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1688376618;
        $migration = new Migration1688376618AddEmailColumnIntoAttendeeTable();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'guided_shopping_appointment_attendee', 'attendee_email'));
    }

    private function hasColumn(Connection $connection, string $columnName): bool
    {
        return \count(\array_filter(
                $connection->createSchemaManager()->listTableColumns('guided_shopping_appointment_attendee'),
                static fn (Column $column): bool => $column->getName() === $columnName
            )) > 0;
    }
}
