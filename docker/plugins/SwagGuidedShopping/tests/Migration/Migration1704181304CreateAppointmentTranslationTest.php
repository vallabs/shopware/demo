<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagGuidedShopping\Migration\Migration1704181304CreateAppointmentTranslation;

class Migration1704181304CreateAppointmentTranslationTest extends TestCase
{
    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1704181304;
        $migration = new Migration1704181304CreateAppointmentTranslation();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        $migration = new Migration1704181304CreateAppointmentTranslation();
        $migration->update($this->connection);

        if (!$this->checkIfColumnExists('name')) {
            $this->connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `name` VARCHAR(255) NOT NULL AFTER `id`;
            ');
        }

        if (!$this->checkIfColumnExists('message')) {
            $this->connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `message` LONGTEXT NULL AFTER `name`;
            ');
        }

        $appointmentId = '018CCD3E90A97D0BA9CC61CC10D4644C';
        $this->connection->executeStatement('
            INSERT IGNORE INTO `guided_shopping_appointment` (`id`, `name`, `mode`, `message`, `video_audio_settings`, `created_at`)
            VALUES (:id, :name, :mode, :message, :videoAudioSettings, :createdAt);
        ', [
            'id' => Uuid::fromHexToBytes($appointmentId),
            'name' => 'test',
            'mode' => 'self',
            'message' => 'test',
            'videoAudioSettings' => 'none',
            'createdAt' => (new \DateTime())->format('Y-m-d H:i:s'),
        ]);

        $migration->update($this->connection);
        $migration->update($this->connection);
        $migration->updateDestructive($this->connection);

        static::assertTrue($this->checkAppointmentTranslationTableIsExists());
        static::assertTrue($this->checkTransferredTranslationData($appointmentId));
        static::assertTrue($this->checkAppointmentTranslatedFieldsIsDeleted());
    }

    private function checkIfColumnExists(string $columnName): bool
    {
        $result = $this->connection->fetchOne("
            SHOW COLUMNS FROM `guided_shopping_appointment` LIKE '{$columnName}';
        ");

        return $result !== false;
    }

    private function checkAppointmentTranslationTableIsExists(): bool
    {
        $record = $this->connection->fetchOne("
            SHOW TABLES LIKE 'guided_shopping_appointment_translation';
        ");

        return $record !== false;
    }

    private function checkTransferredTranslationData(string $appointmentId): bool
    {
        $result = $this->connection->executeStatement("
            SELECT * FROM `guided_shopping_appointment_translation`
            WHERE `guided_shopping_appointment_id` = :appointmentId;
        ", ['appointmentId' => Uuid::fromHexToBytes($appointmentId)]);

        return $result === 2;
    }

    private function checkAppointmentTranslatedFieldsIsDeleted(): bool
    {
        $result = $this->connection->fetchAllNumeric("
            SHOW COLUMNS FROM `guided_shopping_appointment`
            WHERE Field IN ('name', 'message');
        ");

        return \count($result) === 0;
    }
}
