<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use PHPUnit\Framework\TestCase;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Tests\Migration\MigrationTestTrait;
use SwagGuidedShopping\Migration\Migration1688024751AppointmentInvitation;

class Migration1688024751AppointmentInvitationTest extends TestCase
{
    use MigrationTestTrait;

    protected Connection $connection;

    protected function setUp(): void
    {
        parent::setUp();
        $this->connection = KernelLifecycleManager::getConnection();
    }

    /**
     * @before
     */
    public function initialise(): void
    {
        $connection = KernelLifecycleManager::getConnection();
        $migration = new Migration1688024751AppointmentInvitation();

        $connection->rollBack();
        $migration->update($connection);

        if ($this->hasColumn($connection, 'guided_shopping_appointment', 'message')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment` DROP COLUMN `message`;');
        }

        if ($this->hasColumn($connection, 'guided_shopping_appointment_attendee', 'invitation_status')) {
            $connection->executeStatement('ALTER TABLE `guided_shopping_appointment_attendee` DROP COLUMN `invitation_status`;');
        }

        $migration->update($connection);
        $migration->updateDestructive($connection);

        $connection->beginTransaction();
    }

    public function testGetCreationTimestamp(): void
    {
        $expectedTimestamp = 1688024751;
        $migration = new Migration1688024751AppointmentInvitation();

        static::assertEquals($expectedTimestamp, $migration->getCreationTimestamp());
    }

    public function testUpdate(): void
    {
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'guided_shopping_appointment', 'message'));
        static::assertTrue(EntityDefinitionQueryHelper::columnExists($this->connection, 'guided_shopping_appointment_attendee', 'invitation_status'));
    }

    private function hasColumn(Connection $connection, string $table, string $columnName): bool
    {
        return \count(\array_filter(
                $connection->createSchemaManager()->listTableColumns($table),
                static fn (Column $column): bool => $column->getName() === $columnName
            )) > 0;
    }
}
