<?php declare(strict_types=1);

namespace SwagGuidedShopping\Tests;

use Shopware\Core\Framework\Test\TestCaseBase\KernelLifecycleManager;
use Shopware\Core\TestBootstrapper as CoreBootstrapper;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpKernel\KernelInterface;

if (\is_readable(__DIR__ . '/../vendor/shopware/platform/src/Core/TestBootstrapper.php')) {
    require __DIR__ . '/../vendor/shopware/platform/src/Core/TestBootstrapper.php';
} else {
    require __DIR__ . '/../vendor/shopware/core/TestBootstrapper.php';
}

class TestBootstrapper extends CoreBootstrapper
{
    /**
     * @var string[]
     */
    private array $activePlugins = [];

    public function bootstrap(): CoreBootstrapper
    {
        $result = CoreBootstrapper::bootstrap();

        $this->installPlugins();

        return $result;
    }

    public function addActivePlugins(string ...$activePlugins): CoreBootstrapper
    {
        $this->activePlugins = \array_unique(\array_merge($this->activePlugins, $activePlugins));

        return $this;
    }

    private function getKernel(): KernelInterface
    {
        return KernelLifecycleManager::getKernel();
    }

    private function installPlugins(): void
    {
        $application = new Application($this->getKernel());
        $refreshCommand = $application->find('plugin:refresh');
        $refreshCommand->run(new ArrayInput([], $refreshCommand->getDefinition()), $this->getOutput());

        foreach ($this->activePlugins as $activePlugin) {
            $args = [
                '--activate' => true,
                '--reinstall' => true,
                'plugins' => [$activePlugin],
            ];

            $kernel = KernelLifecycleManager::bootKernel();
            $application = new Application($kernel);
            $installCommand = $application->find('plugin:install');
            $returnCode = $installCommand->run(
                new ArrayInput($args, $installCommand->getDefinition()),
                $this->getOutput()
            );

            if ($returnCode !== 0) {
                throw new \RuntimeException('system:install failed');
            }
        }

        KernelLifecycleManager::bootKernel();
    }



    /**
     * @param string|null $pathToComposerJson The composer.json to determine the plugin name. In most cases it's possible to find it automatically.
     *
     * Adds the calling plugin to the plugin list that is installed and activated
     */
    public function addCallingPlugin(?string $pathToComposerJson = null): CoreBootstrapper
    {
        if (!$pathToComposerJson) {
            $trace = \debug_backtrace(\DEBUG_BACKTRACE_IGNORE_ARGS);
            $callerFile = $trace[0]['file'] ?? '';

            $dir = \dirname($callerFile);
            $max = 10;
            while ($max-- > 0 && !\is_file($dir . '/composer.json')) {
                $dir = \dirname($dir);
            }

            if ($max <= 0) {
                throw new \RuntimeException('Failed to find plugin composer.json. Starting point ' . $callerFile);
            }

            $pathToComposerJson = $dir . '/composer.json';
        }

        if (!\is_file($pathToComposerJson)) {
            throw new \RuntimeException('Could not auto detect plugin name via composer.json. Path: ' . $pathToComposerJson);
        }

        $composer = \json_decode((string) \file_get_contents($pathToComposerJson), true);
        $baseClass = $composer['extra']['shopware-plugin-class'] ?? '';
        if ($baseClass === '') {
            throw new \RuntimeException('composer.json does not contain `extra.shopware-plugin-class`. Path: ' . $pathToComposerJson);
        }

        $parts = \explode('\\', $baseClass);
        $pluginName = \end($parts);

        $this->addActivePlugins($pluginName);

        return $this;
    }
}
