<?php declare(strict_types=1);

use Shopware\Core\DevOps\StaticAnalyze\StaticAnalyzeKernel;
use Shopware\Core\Framework\Adapter\Kernel\KernelFactory;
use Shopware\Core\Framework\Plugin\KernelPluginLoader\StaticKernelPluginLoader;
use SwagGuidedShopping\Tests\TestBootstrapper;

require __DIR__ . '/TestBootstrapper.php';

$bootstrapper = (new TestBootstrapper());
$_SERVER['PROJECT_ROOT'] = $_ENV['PROJECT_ROOT'] = $bootstrapper->getProjectDir();
if (!defined('TEST_PROJECT_DIR')) {
    define('TEST_PROJECT_DIR', $_SERVER['PROJECT_ROOT']);
}

$classLoader = $bootstrapper->getClassLoader();
$classLoader->addPsr4('SwagGuidedShopping\\Tests\\', __DIR__);

$plugins = [
    [
        'name' => 'SwagShopwarePwa',
        'baseClass' => 'SwagShopwarePwa\SwagShopwarePwa',
        'active' => true,
        'path' => 'custom/plugins/SwagShopwarePwa',
        'version' => 'dev-master',
        'autoload' => ['psr-4' => ['SwagShopwarePwa\\' => 'src/']],
        'managedByComposer' => false,
        'composerName' => 'shopware-pwa/shopware-pwa'
    ],
    [
        'name' => 'SwagGuidedShopping',
        'baseClass' => 'SwagGuidedShopping\SwagGuidedShopping',
        'active' => true,
        'path' => 'custom/plugins/SwagGuidedShopping',
        'version' => '0.3.0',
        'autoload' => ['psr-4' => ['SwagGuidedShopping\\' => 'src/']],
        'managedByComposer' => false,
        'composerName' => 'swag/guided-shopping'
    ]
];


try {
    $bootstrapper->addActivePlugins('SwagShopwarePwa');
    $bootstrapper->addCallingPlugin();
    $bootstrapper->bootstrap();

} catch (\Throwable $e) {
}

$pluginLoader = new StaticKernelPluginLoader($classLoader, null, $plugins);


KernelFactory::$kernelClass = StaticAnalyzeKernel::class;

/** @var StaticAnalyzeKernel $kernel */
$kernel = KernelFactory::create(
    environment: 'teste',
    debug: true,
    classLoader: $classLoader,
    pluginLoader: $pluginLoader
);

$kernel->boot();

return $kernel;
