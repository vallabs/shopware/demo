# Checklist

## Requirements for default basic stack

<aside>
💡 This checklist does not cover the project specific configuration like cms pages, specific presentation. The only intention of the checklist is to provide plugin fully working at every stack and make it ready to be used in terms of administration stuff and end-user experience.

</aside>

### Shopware fundamentals
- [ ] Node env, pnpm, make commands are working.
- [ ] 🌐Shopware 6 is available on the web over https.
- [ ] PWA plugin for Shopware 6 is installed.

### Required external services
- [ ] [Mercure.rocks](http://Mercure.rocks) service is available on the web over https with required settings
- [ ] [Daily.co](http://Daily.co) service is available on the web with default settings

### Setup
- [ ] The plugin is installed with no errors and it’s available in the “Marketing” section
  ![Untitled](assets/dsr-menu-item.png)
- [ ] The plugin is set up with settings based on the configuration of external services

### Run Shopware Frontends App
- [ ] Install dependencies in `templates/guided-shopping`.
- [ ] Run dev command.

**👌🏼 Great! Now it’s time to setup the presentations and prepare the appointments in order to start using the Digital Sales Rooms.**
