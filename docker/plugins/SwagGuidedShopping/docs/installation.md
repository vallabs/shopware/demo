# Digital Sales Rooms installation

## Get the SwagGuidedShopping plugin for Shopware 6
1. Visit **[URL](https://gitlab.com/shopware/shopware/shopware-6/services/swagguidedshopping)** to clone or download the repository
2. Make sure the plugin has a PHP package structure (containing _`composer.json`_ file, _`src/`_ folder and so on)
3. Prepare a zip file containing the plugin in followed structure

```bash
# SwagGuidedShopping.zip

**SwagGuidedShopping**/
├── bin
├── composer.json
├── composer.lock
├── makefile
├── phpstan.neon
├── phpunit.xml
├── [README.md](http://readme.md/)
├── src
└── tests
```
## Installation
### 1. Install dependencies
- Add necessary 3rd libraries
```bash
# install 3rd libraries
composer require symfony/mercure:^0.6.2 spatie/icalendar-generator:^2.5
```
- Install [SwagShopwarePwa plugin (supported versions > 0.4.*)](https://github.com/shopware/SwagShopwarePwa)

### 2. Install SwagGuidedShopping plugin
Install it like you installed other plugins into Shopware CMS. You can read the [official instruction here](https://developer.shopware.com/docs/guides/plugins/plugins/plugin-base-guide.html#install-your-plugin).

Or, you can follow our steps:
#### Install via admin panel using zip package
1. Log-in into admin panel
2. Go to Extensions > My extensions

   ![Untitled](assets/extensionsMenu.png)

3. Click on “Upload extension” button and choose the zip file containing the plugin from your device.

   ![Untitled](assets/uploadExtension.png)

4. Once it’s uploaded and listed, click “Install app”

![Untitled](assets/swagExtensionOnList.png)

5. Once it’s installed, activate the plugin by clicking on the switch button on the left

#### Install via terminal on the server
1. Extract the zip file onto the `<shopware-root-dir>/custom/plugins` directory
2. Run available symfony commands:

```bash
# refresh list of available plugins
bin/console plugin:refresh
# find the plugin **name** (first column on the list), in this case it's "**SwagGuidedShopping"**
# and use it in the next command:
bin/console plugin:install **SwagGuidedShopping** --activate
# clear the cache afterwards
bin/console cache:clear
# it's ready to use :) 🏁
```

### 3. Setup realtime service - Mercure
- [Stack hero](https://www.stackhero.io/en/services/Mercure-Hub/benefits) (recommended) 


> We tested the service provided by [StackHero](https://www.stackhero.io/en/services/Mercure-Hub/pricing)
> Depending on the expected traffic, you can easily switch between the plans. For a small demo between few people at the same time, the “Hobby” plan is enough.


- [Self hosted](./mercure/self-host.md)
- [Mercure cloud](https://mercure.rocks/) (service publicly accessible in further configuration - see Configuration section)

After finish setup Mercure service, it will provide some important keys:
- **Mercure domain name** (1)
- **Publisher JWT key** (2)
- **Subscriber JWT key** (3)

![mercureConfig](assets/mercureConfig.png)

### 4. Setup realtime voice & video call - Daily
The service responsible for streaming a video between the attendees.

1. Go to the dashboard at: [https://dashboard.daily.co/](https://dashboard.daily.co/)
2. Visit the “developers” section on the left
3. Get the **API KEY** (4)
4. Daily is also provide each account an **API BASE URL** based on your team name (5).

### 5. Fill configuration of plugin
Open Shopware CMS, select `Marketing > Digital Sales Rooms > Configuration`

![Untitled](assets/configuration.png)

Then, fill the information for 2 important sections:
#### Video and Audio / Daily.co
 - **API base url:** fill data from (5)
 - **API key:** fill data from (4)

#### Realtime service / Mercure
 - **Hub url:** fill data from (1)
 - **Hub public url:** fill data from (1)
 - **Hub subscriber secret:** fill data from (2)
 - **Hub publisher secret:** fill data from (3)

You will see another sections also, but all the inputs already assigned with default values, so you don't need to setup it.

### 6. Run Shopware Frontends App
- From Shopware root folder, go to the folder of DSR plugin.
```
cd ./custom/plugins/SwagGuidedShopping
```

- Install dependecies:
```
make fe-init
```

- Develop:
```
make fe-dev
```

- Build:
```
make fe-build
```
