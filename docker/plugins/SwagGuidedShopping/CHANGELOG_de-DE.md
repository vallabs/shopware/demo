# Änderungsprotokoll

Alle nennenswerten Änderungen an diesem Projekt werden in dieser Datei dokumentiert.

Das Format basiert auf [Ein Changelog führen](https://keepachangelog.com/en/1.0.0/),
und dieses Projekt hält sich an die [Semantische Versionierung](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2023-10-31
### Stellen Sie sicher, dass das Plugin mit Shopware 6.5 kompatibel ist.

## [1.2.1] - 2023-09-12
### Wir stellen diese Nebenversion zur Verfügung für:
- Verbessern Sie die Leistung beim Öffnen des Produktlisting-Modals auf der PWA-Seite
- Behebung des Fehlers beim Laden von CMS-Seiten von der Storefront-Seite

## [1.2.0] - 2023-08-31
### Added
**Eine der wichtigsten Neuerungen ist die Integration von Unit-Tests im gesamten Code. Es enthält außerdem:**
* GS-88 – Termineinladungen & Self - Guided Präsentationen können jetzt direkt über Digital Sales Rooms aus dem Admin heraus an Kunden versendet werden.
* GS-585 - Digital Sales Rooms können im anonymen Modus mit `Mercure.rocks` verbunden werden.
* GS-652 - Im Instant Listing kann ein Guide mehrere Produkte aus- bzw. abwählen und diese entsprechend ein Instant Listing  hinzufügen bzw. bearbeiten.
* GS-707 - Erweiterung der API - Dokumentation um notwendige Entitäten.
* GS-759 - Instant Listing: Erfolgt während der Bearbeitung eines Instant Listings ein Browser - Refresh werden die Produkte zwischengespeichert und gehen nicht verloren.
* GS-763 - Erweiterung der API - Dokumentation um notwendige Entitäten.

### Fixed
* GS-751 - Fehlerbehebung im Instant Listing beim hinzufügen/entfernen von Produkten.
* GS-766 - Im Admin war es zeitweise nicht möglic,h die Reihenfolge von Präsentationsseiten zu ändern. Dieses Problem wurde nun behoben.
* GS-775 - Validierungsfehler beim Aktualisieren eines vorhandenen Termins behoben.

### Changed
* GS-706 - Erstellen Sie Termine, ohne `Daily.co` zu verwenden.
* GS-708 - Verbessern Sie das Auswählen/Abwählen aller Produkte beim Erstellen/Aktualisieren einer Sofortliste.
* GS-753 - Verbessern Sie die Mehrfachauswahl beim Filtern im Sofortlisten-Modal.
* GS-758 - Für den Versand der Einladung benötigt die Selbstständigkeit keine festen Zeiten.
* GS-761 - Refaktorieren Sie Routenpfade und aktualisieren Sie sie in API-Dokumenten
* GS-762 - Ändern Sie die E-Mail-Einladungsvorlage.
* GS-764 - Ändern Sie Dienste so, dass sie mit dem CMS-Erweiterungs-Plugin kompatibel sind.
* GS-789 - Entfernen Sie redundante Standardfehlermeldungen, wenn Sie eine irgendwo verwendete CMS-Seite löschen.

## [1.1.0] - 2023-04-27
### Hinzugefügt
* GS-355 - Unit-Tests für `useMediaDevices` hinzufügen.
* GS-642 - Das CMS-Seitenlayout konfigurierbar machen.
* GS-626 - Digital Sales Rooms wurde dahingehend optimiert, dass es besser je nach Kundenwunsch angepasst werden kann.
* GS-586 - Verkäufer kann einen Verkaufstermin ohne zuvor erstellte Präsentation durchführen. Basierend auf Kundenanforderungen die ein Kunde während des Termins mitteilt, kann mit dem Instant Listing ein Slide mit passenden Produkten erstellt werden.
* GS-623 - Zusätzliche Slots für `SwGsLayer` hinzufügen & die Auswahl eines Produkts durch Anklicken einer beliebigen Stelle der Produktbox ermöglichen.
* GS-619 - Das Layout austauschbar machen.
* GS-641 - Hinzufügen benutzerdefinierter Felder für die Termine und Präsentationen.
* GS-622 - Guide kann im Vorfeld eines Kundentermins Produkte zur Merkliste für einen Kunden hinzufügen.
* GS-645 - Händler können sehen, ob Kunden an einem geführten Termin teilgenommen haben oder eine ungeführte Präsentation geöffnet haben.
* GS-653 - Präsentationen können im Admin kopiert werden.
* GS-652 - Im Instant Listing kann ein Guide mehrere Produkte aus- bzw. abwählen und diese entsprechend ein Instant Listing  hinzufügen bzw. bearbeiten.

### Behoben
* GS-485 - Die Funktion **Go to Guide** konnte nicht angewendet werden, wenn auf einer Produktdetailseite mehrere  Cross - Selling Elemente eingebunden wurden.
* GS-454 - Behebung des Fehlers, dass User die CMS-Seite in den Erlebniswelten nicht duplizieren können.
* GS-499 - Fix Admin-Suche mit dem Präsentations- und Terminbereich.
* GS-630 - Korrektur des Pfeil-Navigationsstils in Safari.
* GS-631 - Platzhalter der Like-Liste korrigieren.
* GS-632 - Die PDP zeigt falsche Inhalte an.
* GS-648 - Fixierung der Mengeneingabe von Produkten im Warenkorb.
* GS-627 - Den Warenkorbpreis beheben.
* GS-639 - Gruppenartikel in der Produktauswahl repariert.
* GS-447 - Kunden die zu einem geschlossenen Termin eingeladen werden, können jetzt bei der Anmeldung zum Termin auswählen, dass Verkäufer Produkte zum Warenkorb/Merkliste für den Kunden hinzufügen dürfen.
* GS-615 - User können Präsentationen aktiv/inaktiv setzen. Ist eine Präsentation inaktiv, kann kein Kunde auf die Präsentation zugreifen. Der Verkäufer kann immer auf eine Präsentation zugreifen.
* GS-663 - Behebung eines Fehlers, der dafür sorgte, dass der allgemeine Warenkorb Produkte/ Umsatz angezeigt.
* GS-469 - **Guide Folgen** - Funktion für Nachzügler zu einer Präsentation, die bereits gestartet wurde wieder hergestellt.
* GS-649 - Optimierung der Mengeneingabe im Warenkorb. Die Anzahl der Mengeneingab kann manuell und oder per Pfeilnavigation erhöht oder reduziert werden.  Hier wurde im speziellen die UX optimiert, damit der Kunde die Anzahl der Produkte besser festlegen kann.
* GS-678 - Darstellung des Burgermenüs wurde angepasst.B
* GS-613 - Behebung der modalen Anzeige der Produktzuordnung, die je nach Art des Layouts nicht korrekt angezeigt wurde.
* GS-681 - Behebung der Vorschaufunktion für Präsentationen.

### Geändert
* GS-624 - Aufteilung der Navigationskomponenten.
* GS-618 - Optimierung des Scroll-Verhaltens.
* GS-614 - Behebung des Fehlers, der verhindert hat, dass die Vorschaufunktion für Präsentationen im Admin über Ipads funktionierte.
* GS-669 - Änderung der Position des Währungssymbolsam Preis.
* GS-638 - Anpassung der Teilnehmertabelle in den Termindetails.
* GS-621 - Bereinigung gespeicherter Präsentationen in der Live-Sitzung.
* GS-620 - Verbesserung der Speicherleistung.
* GS-645 - Händler können sehen, ob Kunden  an einem geführten Termin teilgenommen haben oder eine ungeführte Präsentation geöffnet haben (Zusätzliche Änderungen).
* GS-676 - Anpassung des deutschen Übersetzung in der Daily.co checkbox im Anmelde - Modal.
* GS-635 - Produkt-Hover-Verhalten im Warenkorb optimiert.
* GS-625 - z-index refaktoriert.
* GS-675 - Aktualisierung der Vorschaufunktion für Präsentationen.
* GS-670 - Ton einschalten, wenn Video deaktiviert ist.

## [1.0.0] - 2023-01-31
### **Erste Veröffentlichung**
