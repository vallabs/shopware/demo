#!/usr/bin/env bash

# Run this command from plugin root

rm -rf build && mkdir build && mkdir build/SwagGuidedShopping

cp composer.json build/SwagGuidedShopping/composer.json && cp -r src build/SwagGuidedShopping/src

cd build/SwagGuidedShopping

composer install --no-dev -n
rm -rf vendor/symfony/service-contracts

cd ..

zip -q -r SwagGuidedShopping.zip SwagGuidedShopping

echo "Package Build Finished"
