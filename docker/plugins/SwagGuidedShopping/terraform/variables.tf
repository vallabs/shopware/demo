variable "APP_NAME" {
  type        = string
}

variable "APP_SUBNET" {
  type        = string
  validation {
    condition     = can(regex("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\/[0-9]{1,3}$", var.APP_SUBNET))
    error_message = "APP_SUBNET should be a valid subnet."
  }
}

variable "SHOPWARE_ENDPOINT" {
  type      = string
}

variable "SHOPWARE_ACCESS_TOKEN" {
  type      = string
}