provider "aws" {
  region              = "eu-central-1"
  allowed_account_ids = ["968359503715"]
  assume_role {
    role_arn = "arn:aws:iam::968359503715:role/TerraformServiceRole"
  }
  default_tags {
    tags = {
      Environment = "production"
    }
  }
}

terraform {
  backend "s3" {
    role_arn       = "arn:aws:iam::968359503715:role/TerraformServiceRole"
    bucket         = "shopware-app-hosting-production-tfstate"
    key            = "production/apps/dsr-frontends/terraform.tfstate"
    region         = "eu-central-1"
    session_name   = "terraform"
    encrypt        = true
    dynamodb_table = "terraform-locks"
  }
}

data "terraform_remote_state" "static_infra" {
  backend = "s3"
  config = {
    bucket   = "shopware-app-hosting-production-tfstate"
    key      = "production/static-infrastructure/terraform.tfstate"
    role_arn = "arn:aws:iam::968359503715:role/TerraformServiceRole"
    region   = "eu-central-1"
  }
}

data "terraform_remote_state" "core_infra" {
  backend = "s3"
  config = {
    bucket   = "shopware-app-hosting-production-tfstate"
    key      = "production/core-infrastructure/terraform.tfstate"
    role_arn = "arn:aws:iam::968359503715:role/TerraformServiceRole"
    region   = "eu-central-1"
  }
}
