module "app" {
    source = "git::ssh://git@gitlab.shopware.com/shopware-cloud/app-hosting/infrastructure/modules.git//app"
  
    name              = var.APP_NAME
    server_port       = 3000
    health_check_path = "/_healthcheck"
    subnet_cidr       = var.APP_SUBNET
  
    # resources
    server_memory = 2048
  
    custom_environment = {
        NUXT_PUBLIC_SHOPWARE_SHOPWARE_ENDPOINT=var.SHOPWARE_ENDPOINT,
        NUXT_PUBLIC_SHOPWARE_SHOPWARE_ACCESS_TOKEN=var.SHOPWARE_ACCESS_TOKEN
    }

    # optional
    enable_mysql              = false
    enable_s3                 = false
    autoscaling_request_count = 0 // 0 = disabled
  
    infrastructure = {
      // DNS + loadbalancing
      external_domain   = data.terraform_remote_state.static_infra.outputs.external_domain
      loadbalancer_name = data.terraform_remote_state.core_infra.outputs.public_alb_name
  
      // task definition
      ecs_execution_role          = data.terraform_remote_state.core_infra.outputs.ecs_task_execution_role_name
      datadog_api_key_secret_name = data.terraform_remote_state.core_infra.outputs.datadog_api_key_secret_name
    }
  }
  