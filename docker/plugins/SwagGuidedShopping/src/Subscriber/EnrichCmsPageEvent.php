<?php declare(strict_types=1);

namespace SwagGuidedShopping\Subscriber;

use Doctrine\DBAL\Connection;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\DefinitionInstanceRegistry;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityDeleteEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Validation\RestrictDeleteViolation;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Validation\RestrictDeleteViolationException;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Content\Cms\CmsPageEvents;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntitySearchedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWrittenEvent;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EnrichCmsPageEvent implements EventSubscriberInterface
{
    /**
     * @param EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository
     */
    public function __construct(
        private readonly Connection $connection,
        private readonly EntityRepository $presentationCmsPageRepository,
        private readonly DefinitionInstanceRegistry $registry
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'cms_page.search' => 'addCriteria',
            CmsPageEvents::PAGE_WRITTEN_EVENT => 'updateTheUserWhoCreateTheCmsPage',
            EntityDeleteEvent::class => 'checkIfTheLayoutIsUsed'
        ];
    }

    public function addCriteria(EntitySearchedEvent $event): void
    {
        $criteria = $event->getCriteria();
        $criteria->addAssociation('createdBy');
    }

    public function updateTheUserWhoCreateTheCmsPage(EntityWrittenEvent $event): void
    {
        $contextSource = $event->getContext()->getSource();
        if (!$contextSource instanceof AdminApiSource) {
            return;
        }

        $createdById = $contextSource->getUserId();
        if (!$createdById) {
             return;
        }

        $payload = $event->getPayloads();
        if (!isset($payload[0]) || !isset($payload[0]['id']) || !$payload[0]['id']) {
            return;
        }

        $this->updateCreatedById($createdById, $payload[0]['id']);
    }

    public function checkIfTheLayoutIsUsed(EntityDeleteEvent $event): void
    {
        /** @var string[] $cmsPageIds */
        $cmsPageIds = $event->getIds(CmsPageDefinition::ENTITY_NAME);
        $context = $event->getContext();

        if (\count($cmsPageIds)) {
            $criteria = new Criteria();
            $criteria->addFilter(new EqualsAnyFilter('cmsPageId', $cmsPageIds));
            $result = $this->presentationCmsPageRepository->searchIds($criteria, $context);

            if (\count($result->getData())) {
                $definition = $this->registry->getByEntityName(CmsPageDefinition::ENTITY_NAME);
                $restriction = new RestrictDeleteViolation(['guided_shopping_presentation' => $result->getData()]);
                throw new RestrictDeleteViolationException($definition, [$restriction]);
            }
        }
    }

    private function updateCreatedById(string $createdById, string $cmsPageId): void
    {
        $this->connection->executeStatement(
            'UPDATE `cms_page` SET `created_by_id` = :createdById WHERE `id` = :id AND `created_by_id` IS NULL',
            [
                'createdById' => Uuid::fromHexToBytes($createdById),
                'id' => Uuid::fromHexToBytes($cmsPageId),
            ]
        );
    }
}
