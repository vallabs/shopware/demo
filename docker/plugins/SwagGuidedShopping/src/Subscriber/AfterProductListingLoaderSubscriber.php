<?php declare(strict_types=1);

namespace SwagGuidedShopping\Subscriber;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Event\AfterProductListingLoadedEvent;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AfterProductListingLoaderSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory,
        private readonly Connection $connection
    )   {
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            AfterProductListingLoadedEvent::class => 'addLikesAndDisLikesToProduct'
        ];
    }

    public function addLikesAndDisLikesToProduct(AfterProductListingLoadedEvent $event): void
    {
        $salesChannelContext = $event->getSalesChannelContext();

        /** @var AttendeeEntity|null $attendee */
        $attendee = $salesChannelContext->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);

        if (!$attendee) {
            return;
        }

        $stateService = $this->presentationStateServiceFactory->build(
            $attendee->getAppointmentId(),
            $salesChannelContext->getContext()
        );

        $attendeeIds = $stateService->getStateForGuides()->getAttendeeIdsForAllKindOfClients();
        if (!\count($attendeeIds)) {
            return;
        }

        $result = $event->getSearchResult();

        $params = [
            'productIds' => Uuid::fromHexToBytesList($result->getIds()),
            'attendeeIds' => Uuid::fromHexToBytesList($attendeeIds)
        ];

        $sql = "
            SELECT
                LOWER(HEX(`product_id`)) as productId,
                COUNT(IF(`alias` = 'liked', 1, NULL)) as likes,
                COUNT(IF(`alias` = 'disliked', 1, NULL)) as dislikes
            FROM `guided_shopping_attendee_product_collection`
            WHERE `product_id` IN (:productIds)
                AND `attendee_id` IN (:attendeeIds)
            GROUP BY `productId`
        ";

        $data =  $this->connection->executeQuery(
            $sql,
            $params,
            [
                'productIds' => ArrayParameterType::STRING,
                'attendeeIds' => ArrayParameterType::STRING
            ]
        )->fetchAllAssociativeIndexed();

        /** @var ProductEntity $entity */
        foreach ($result->getEntities() as $entity) {
            $entity->addArrayExtension(
                'attendeeProductCollections',
                [
                    'likes' => $data[$entity->getId()]['likes'] ?? 0,
                    'dislikes' => $data[$entity->getId()]['dislikes'] ?? 0
                ]
            );
        }
    }
}
