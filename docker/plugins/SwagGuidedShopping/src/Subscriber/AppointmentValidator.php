<?php declare(strict_types=1);

namespace SwagGuidedShopping\Subscriber;

use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityWriteEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\WriteCommand;
use Shopware\Core\Framework\Uuid\Uuid;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Validation\PreWriteValidationEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\InsertCommand;
use Shopware\Core\Framework\DataAbstractionLayer\Write\Command\UpdateCommand;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Shopware\Core\Framework\Validation\WriteConstraintViolationException;

class AppointmentValidator implements EventSubscriberInterface
{
    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     * @param EntityRepository<PresentationCollection> $presentationRepository
     */
    public function __construct(
        private readonly EntityRepository $appointmentRepository,
        private readonly EntityRepository $presentationRepository,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            PreWriteValidationEvent::class => 'preValidate',
            EntityWriteEvent::class => 'beforeWrite',
        ];
    }

    public function preValidate(PreWriteValidationEvent $event): void
    {
        $violationList = new ConstraintViolationList();

        foreach ($event->getCommands() as $index => $command) {
            if (
                !$command->getDefinition() instanceof AppointmentDefinition
                || (!$command instanceof InsertCommand && !$command instanceof UpdateCommand)
            ) {
                continue;
            }

            $payload = $command->getPayload();
            $context = $event->getContext();
            $appointmentId = null;
            $appointment = null;

            /**
             * Check if the appointment has presentationId,
             * then check if the presentation is existed with live version (parent presentation)
             */
            if ($command instanceof UpdateCommand) {
                $primaryKey = $command->getPrimaryKey();
                if (isset($primaryKey['id'])) {
                    $id = $primaryKey['id'];
                    $appointmentId = !Uuid::isValid($id) ? Uuid::fromBytesToHex($id) : $id;
                    $criteria = new Criteria([$appointmentId]);
                    $appointment = $this->appointmentRepository->search($criteria, $context)->first();

                    $emptyPresentationError = $this->buildViolation(
                        'The presentation should be not empty.',
                        null,
                        'presentationId',
                        'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                        $index
                    );

                    if (!isset($payload['guided_shopping_presentation_id'])) {
                        if (!$appointment instanceof AppointmentEntity) {
                            $violationList->add($emptyPresentationError);
                        } else {
                            $presentationId = $appointment->getPresentationId();
                            $criteria = new Criteria([$presentationId]);
                            $criteria->addFilter(new EqualsFilter('versionId', Defaults::LIVE_VERSION));
                            $presentation = $this->presentationRepository->search($criteria, $context)->first();

                            if (!$presentation) {
                                $violationList->add($emptyPresentationError);
                            }
                        }
                    }
                }
            }

            /**
             * Check if presentation path is existed
             */
            if (isset($payload['presentation_path'])) {
                $criteria = new Criteria();
                $criteria->addFilter(new EqualsFilter('presentationPath', $payload['presentation_path']));
                if ($appointmentId) {
                    $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [
                        new EqualsFilter('id', $appointmentId)
                    ]));
                }

                $records = $this->appointmentRepository->searchIds($criteria, $context);
                if ($records->getTotal() > 0) {
                    $violationList->add($this->buildViolation(
                        'This path already exists.',
                        $payload['presentation_path'],
                        'presentationPath',
                        'DUPLICATED_URL',
                        $index
                    ));
                }
            }

            // Check if it's GUIDED appointment
            if (isset($payload['mode']) && $payload['mode'] === AppointmentDefinition::MODE_GUIDED) {
                // Check the sender is empty
                if (!isset($payload['guide_user_id'])
                    && (!$appointment instanceof AppointmentEntity || !$appointment->getGuideUserId())
                ) {
                    $violationList->add($this->buildViolation(
                        'The sender should be not empty.',
                        null,
                        'guideUserId',
                        'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                        $index
                    ));
                }

                // Check the accessible from is empty
                if (
                    !isset($payload['accessible_from'])
                    && (!$appointment instanceof AppointmentEntity || !$appointment->getAccessibleFrom())
                ) {
                    $violationList->add($this->buildViolation(
                        'The Accessible From should be not empty.',
                        null,
                        'accessibleFrom',
                        'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                        $index
                    ));

                    continue;
                }

                // Check the accessible to is empty
                if (
                    !isset($payload['accessible_to'])
                    && (!$appointment instanceof AppointmentEntity || !$appointment->getAccessibleTo())
                ) {
                    $violationList->add($this->buildViolation(
                        'The Accessible To should be not empty.',
                        null,
                        'accessibleTo',
                        'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                        $index
                    ));

                    continue;
                }
            }

            /**
             * Check the accessible time is valid
             */
            if (isset($payload['accessible_from']) && isset($payload['accessible_to'])) {
                $accessibleFrom = \strtotime($payload['accessible_from']);
                $accessibleTo = \strtotime($payload['accessible_to']);

                if (!$accessibleFrom) {
                    $violationList->add($this->buildViolation(
                        'The Accessible From should be not empty.',
                        null,
                        'accessibleFrom',
                        'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                        $index
                    ));
                    continue;
                }

                if (!$accessibleTo) {
                    $violationList->add($this->buildViolation(
                        'The Accessible To should be not empty.',
                        null,
                        'accessibleTo',
                        'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                        $index
                    ));
                    continue;
                }

                if ($accessibleFrom >= $accessibleTo) {
                    $violationList->add($this->buildViolation(
                        'Accessible To should be greater than Accessible From.',
                        $payload['accessible_to'],
                        'accessibleTo',
                        'INVALID_ACCESSIBLE_TIME',
                        $index
                    ));
                }
            } else {
                if ($appointment instanceof AppointmentEntity) {
                    if (isset($payload['accessible_from']) || isset($payload['accessible_to'])) {
                        $isGuidedMode = $appointment->getMode() === AppointmentDefinition::MODE_GUIDED;

                        if (isset($payload['accessible_from'])) {
                            $accessibleFrom = \strtotime($payload['accessible_from']);
                            $accessibleTo = $appointment->getAccessibleTo()?->getTimestamp();
                        } else {
                            $accessibleFrom = $appointment->getAccessibleFrom()?->getTimestamp();
                            $accessibleTo = \strtotime($payload['accessible_to']);
                        }

                        if (!$accessibleFrom && $isGuidedMode) {
                            $violationList->add($this->buildViolation(
                                'The Accessible From should be not empty.',
                                null,
                                'accessibleFrom',
                                'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                                $index
                            ));
                            continue;
                        }

                        if (!$accessibleTo && $isGuidedMode) {
                            $violationList->add($this->buildViolation(
                                'The Accessible To should be not empty.',
                                null,
                                'accessibleTo',
                                'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                                $index
                            ));
                            continue;
                        }

                        if ($accessibleFrom && $accessibleTo && $accessibleFrom >= $accessibleTo) {
                            $violationList->add($this->buildViolation(
                                'Accessible To should be greater than Accessible From.',
                                $payload['accessible_to'] ?? null,
                                'accessibleTo',
                                'INVALID_ACCESSIBLE_TIME',
                                $index
                            ));
                        }
                    }
                }
            }
        }

        if ($violationList->count()) {
            $event->getExceptions()->add(new WriteConstraintViolationException($violationList));
        }
    }

    public function beforeWrite(EntityWriteEvent $event): void
    {
        foreach ($event->getCommands() as $command) {
            if ($command->getDefinition() instanceof AppointmentDefinition) {
                if ($command instanceof InsertCommand) {
                    $this->addDefaultLiveVersionId($command);
                    continue;
                }

                if ($command instanceof UpdateCommand) {
                    $primaryKey = $command->getPrimaryKey();
                    
                    if (!isset($primaryKey['id'])) continue;

                    $id = $primaryKey['id'];

                    if (!Uuid::isValid($id)) {
                        $id = Uuid::fromBytesToHex($id);
                    }

                    $payload = $command->getPayload();
                    if (isset($payload['guided_shopping_presentation_id'])) {
                        $this->addDefaultLiveVersionId($command);
                        continue;
                    }

                    $appointment = $this->appointmentRepository->search(new Criteria([$id]), $event->getContext())->first();

                    if ($appointment instanceof AppointmentEntity
                        && !$appointment->getPresentationVersionId()
                    ) {
                        $this->addDefaultLiveVersionId($command);
                    }
                }
            }
        }
    }

    private function buildViolation(string $message, mixed $invalidValue, string $propertyPath, string $code, int $index): ConstraintViolationInterface
    {
        $formattedPath = "/{$index}/{$propertyPath}";

        return new ConstraintViolation(
            $message,
            '',
            [
                'value' => $invalidValue,
            ],
            $invalidValue,
            $formattedPath,
            $invalidValue,
            null,
            $code
        );
    }

    /**
     *  Add default live version id
     *  to display the appointment's presentation in the Administration's appointment list
     */
    private function addDefaultLiveVersionId(WriteCommand $command): void
    {
        $command->addPayload('guided_shopping_presentation_version_id', Uuid::fromHexToBytes(Defaults::LIVE_VERSION));
    }
}
