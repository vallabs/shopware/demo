<?php declare(strict_types=1);

namespace SwagGuidedShopping\Subscriber;

use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Content\Product\Aggregate\ProductConfiguratorSetting\ProductConfiguratorSettingEntity;
use Shopware\Core\Content\Property\Aggregate\PropertyGroupOption\PropertyGroupOptionCollection;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use SwagGuidedShopping\Content\Presentation\Event\AfterLoadProductConfiguratorEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AfterLoadProductConfiguratorSubscriber implements EventSubscriberInterface
{
    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            AfterLoadProductConfiguratorEvent::class => 'assignConfiguratorMedia'
        ];
    }

    public function assignConfiguratorMedia(AfterLoadProductConfiguratorEvent $event): void
    {
        $groups = $event->getGroups();

        if (!$groups instanceof PropertyGroupCollection) {
            return;
        }

        foreach ($groups as $group) {
            $options = $group->getOptions();

            if (!$options instanceof PropertyGroupOptionCollection) {
                continue;
            }

            foreach ($options as $option) {
                $configuratorSetting = $option->getConfiguratorSetting();

                if (!$configuratorSetting instanceof ProductConfiguratorSettingEntity) {
                    continue;
                }

                $media = $configuratorSetting->getMedia();

                if (!$media instanceof MediaEntity) {
                    continue;
                }

                $option->setMediaId($media->getId());
                $option->setMedia($media);
            }
        }
    }
}
