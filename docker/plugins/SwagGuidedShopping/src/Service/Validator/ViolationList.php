<?php declare(strict_types=1);

namespace SwagGuidedShopping\Service\Validator;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ViolationList
{
    public function __construct(
        private readonly ConstraintViolationListInterface $violationList
    ) {
    }

    public function __toString(): string
    {
        $message = '';
        /** @var ConstraintViolation $violation */
        foreach ($this->violationList as $violation) {
            $message .= $violation->getMessage() . ' ' . $violation->getPropertyPath() . "\n";
        }

        return $message;
    }

    public function count(): int
    {
        return $this->violationList->count();
    }

    public function get(int $offset): ConstraintViolationInterface
    {
        return $this->violationList->get($offset);
    }
}
