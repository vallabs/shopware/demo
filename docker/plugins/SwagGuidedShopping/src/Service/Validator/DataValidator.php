<?php declare(strict_types=1);

namespace SwagGuidedShopping\Service\Validator;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DataValidator
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    /**
     * @param array<string, mixed> $data
     */
    public function validate(array $data, Collection $validationCollection): ViolationList
    {
        $validationCollection->allowExtraFields = true;
        $violations = $this->validator->validate($data, $validationCollection);

        return new ViolationList($violations);
    }
}
