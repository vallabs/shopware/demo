<?php declare(strict_types=1);

namespace SwagGuidedShopping\Service\ScheduledTask;

use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\IdSearchResult;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskCollection;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler(handles: DeletePreviewPresentationTask::class)]
class DeletePreviewPresentationTaskHandler extends ScheduledTaskHandler
{
    protected Context $context;

    /**
     * @param EntityRepository<ScheduledTaskCollection> $scheduledTaskRepository
     * @param EntityRepository<PresentationCollection> $presentationRepository
     * @param EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        protected EntityRepository $scheduledTaskRepository,
        private readonly EntityRepository $presentationRepository,
        private readonly EntityRepository $presentationCmsPageRepository,
        private readonly EntityRepository $appointmentRepository,
        private readonly LoggerInterface $logger
    ) {
        parent::__construct($scheduledTaskRepository);
    }

    public function run(): void
    {
        $this->context = Context::createDefaultContext();

        $this->logger->info('Deleting preview PRESENTATION CMS PAGES(s)');
        $this->deleteAllPreviewPresentationCmsPage();
        $this->logger->info('Deleted preview PRESENTATION CMS PAGE(s)');

        $this->logger->info('Deleting preview PRESENTATION(s)');
        $this->deleteAllPreviewPresentation();
        $this->logger->info('Deleted preview PRESENTATION');

        $this->logger->info('Deleting preview APPOINTMENT(s)');
        $this->deleteAllPreviewAppointment();
        $this->logger->info('Deleted preview APPOINTMENT(s)');
    }

    private function deleteAllPreviewPresentationCmsPage(): void
    {
        $criteria = new Criteria();
        $criteria->addFilter(new NotFilter(
            MultiFilter::CONNECTION_AND,
            [new EqualsFilter('presentation.parentId', null)]
        ));
        $previewPresentationCmsPages = $this->presentationCmsPageRepository->searchIds($criteria, $this->context);
        $this->deleteRecords($previewPresentationCmsPages, $this->presentationCmsPageRepository);
    }

    private function deleteAllPreviewPresentation(): void
    {
        $criteria = new Criteria();
        $criteria->addFilter(new NotFilter(
            MultiFilter::CONNECTION_AND,
            [new EqualsFilter('parentId', null)]
        ));
        $previewPresentations = $this->presentationRepository->searchIds($criteria, $this->context);
        $this->deleteRecords($previewPresentations, $this->presentationRepository);
    }

    private function deleteAllPreviewAppointment(): void
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('isPreview', true));
        $previewAppointments = $this->appointmentRepository->searchIds($criteria, $this->context);
        $this->deleteRecords($previewAppointments, $this->appointmentRepository);
    }

    /**
     * @param EntityRepository<PresentationCmsPageCollection>|EntityRepository<PresentationCollection>|EntityRepository<AppointmentCollection> $repository
     */
    private function deleteRecords(IdSearchResult $idSearchResult, EntityRepository $repository): void
    {
        if ($idSearchResult->getTotal()) {
            $data = [];
            foreach ($idSearchResult->getIds() as $id) {
                $data[]['id'] = $id;
            }

            $repository->delete($data, $this->context);
        }
    }
}
