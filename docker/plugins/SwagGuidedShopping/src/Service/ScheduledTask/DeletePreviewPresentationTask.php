<?php declare(strict_types=1);

namespace SwagGuidedShopping\Service\ScheduledTask;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class DeletePreviewPresentationTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'shopware_guided_shopping.delete_preview_presentation';
    }

    public static function getDefaultInterval(): int
    {
        return 604800; // a week
    }
}
