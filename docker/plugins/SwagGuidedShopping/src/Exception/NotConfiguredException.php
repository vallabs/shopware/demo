<?php declare(strict_types=1);

namespace SwagGuidedShopping\Exception;

class NotConfiguredException extends \Exception
{
    public function __construct(string $message = '')
    {
        parent::__construct($message);
    }
}
