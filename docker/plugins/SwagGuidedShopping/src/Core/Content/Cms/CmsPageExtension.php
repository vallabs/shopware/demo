<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Content\Cms;

use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\System\User\UserDefinition;

class CmsPageExtension extends EntityExtension
{
    public function getDefinitionClass(): string
    {
        return CmsPageDefinition::class;
    }

    public function extendFields(FieldCollection $collection): void
    {
        $collection->add((new FkField('created_by_id', 'createdById', UserDefinition::class))->addFlags(new Inherited()));
        $collection->add((new ManyToOneAssociationField('createdBy', 'created_by_id', UserDefinition::class, 'id', true)));
    }
}
