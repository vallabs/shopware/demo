<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\System\SalesChannel\Aggregate\SalesChannelDomain;

use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class SalesChannelDomainExtension extends EntityExtension
{
    public function getDefinitionClass(): string
    {
        return SalesChannelDomainDefinition::class;
    }

    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToManyAssociationField(
                'guidedShoppingAppointmentGuideSalesChannelDomain',
                AppointmentDefinition::class,
                'sales_channel_domain_id')
            )->addFlags(new CascadeDelete())
        );
    }
}
