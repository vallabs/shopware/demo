<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\System\User;

use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\User\UserDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Presentation\PresentationDefinition;
use Shopware\Core\Content\Cms\CmsPageDefinition;

class UserExtension extends EntityExtension
{
    public function getDefinitionClass(): string
    {
        return UserDefinition::class;
    }

    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToManyAssociationField(
                'guidedShoppingAppointmentGuideUser',
                AppointmentDefinition::class,
                'guide_user_id')
            )->addFlags(new CascadeDelete())
        );

        $collection->add(
            (new OneToManyAssociationField(
                'createdPresentations',
                PresentationDefinition::class,
                'created_by_id',
                'id')
            )->addFlags(new CascadeDelete())

        );
        $collection->add(
            (new OneToManyAssociationField(
                'updatedPresentations',
                PresentationDefinition::class,
                'updated_by_id',
                'id')
            )->addFlags(new CascadeDelete())
        );

        $collection->add(
            (new OneToManyAssociationField(
                'createdAppointments',
                AppointmentDefinition::class,
                'created_by_id',
                'id')
            )->addFlags(new CascadeDelete())
        );

        $collection->add(
            (new OneToManyAssociationField(
                'updatedAppointments',
                AppointmentDefinition::class,
                'updated_by_id',
                'id')
            )->addFlags(new CascadeDelete())
        );

        $collection->add(
            (new OneToManyAssociationField(
                'createdLayouts',
                CmsPageDefinition::class,
                'created_by_id',
                'id')
            )->addFlags(new CascadeDelete())
        );
    }
}
