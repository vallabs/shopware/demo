<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Customer\Subscriber;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Checkout\Customer\Event\CustomerBeforeLoginEvent;
use Shopware\Core\Checkout\Customer\Event\CustomerLoginEvent;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Service\ContextDataHelper;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CustomerLoggedInSubscriber implements EventSubscriberInterface
{
    private ?AttendeeEntity $attendeeEntityBeforeLogin = null;

    public function __construct(
        private readonly JoinAppointmentService $joinAppointmentService,
        private readonly AbstractSalesChannelContextFactory $contextFactory,
        private readonly SalesChannelContextPersister $contextPersister,
        private readonly ContextDataHelper $contextDataHelper,
        private readonly RequestStack $requestStack
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            CustomerLoginEvent::class => 'updateAttendee',
            CustomerBeforeLoginEvent::class => 'findAttendee',
        ];
    }

    public function findAttendee(CustomerBeforeLoginEvent $event): void
    {
        $this->attendeeEntityBeforeLogin = null;
        $this->attendeeEntityBeforeLogin = $this->contextDataHelper->getAttendeeFromContext($event->getSalesChannelContext());
    }

    public function updateAttendee(CustomerLoginEvent $event): void
    {
        $request = $this->requestStack->getMainRequest();

        if (!$request) {
            return;
        }

        if (!$this->attendeeEntityBeforeLogin) {
            return;
        }

        $afterLoginToken = $event->getContextToken();
        $session = $this->contextPersister->load($afterLoginToken, $event->getSalesChannelId());
        $salesChannelContext = $this->contextFactory->create($afterLoginToken, $event->getSalesChannelId(), $session);
        if (!$salesChannelContext->getCustomer() instanceof CustomerEntity) {
            return;
        }

        $this->contextPersister->save(
            $afterLoginToken,
            ['attendeeId' => $this->attendeeEntityBeforeLogin->getId()],
            $salesChannelContext->getSalesChannelId(),
            $salesChannelContext->getCustomer()->getId()
        );

        $this->joinAppointmentService->joinMeeting(
            $this->attendeeEntityBeforeLogin->getAppointmentId(),
            $salesChannelContext,
            false
        );
    }
}
