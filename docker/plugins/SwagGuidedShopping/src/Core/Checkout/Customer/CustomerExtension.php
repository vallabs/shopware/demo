<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Customer;

use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;

class CustomerExtension extends EntityExtension
{
    public function getDefinitionClass(): string
    {
        return CustomerDefinition::class;
    }

    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToManyAssociationField(
                'attendee',
                AttendeeDefinition::class,
                'customer_id')
            )->addFlags(new CascadeDelete())
        );
    }
}
