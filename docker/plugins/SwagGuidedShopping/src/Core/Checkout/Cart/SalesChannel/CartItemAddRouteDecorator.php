<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Cart\SalesChannel;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItemFactoryRegistry;
use Shopware\Core\Checkout\Cart\SalesChannel\AbstractCartItemAddRoute;
use Shopware\Core\Checkout\Cart\SalesChannel\CartResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class CartItemAddRouteDecorator extends AbstractCartItemAddRoute
{
    public const GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER = 'GUIDED_SHOPPING_ADD_TO_CART';
    public const GUIDED_SHOPPING_ADD_TO_CART_APPOINTMENT_ID = 'GUIDED_SHOPPING_APPOINTMENT_ID';

    public function __construct(
        private readonly AbstractCartItemAddRoute $decorated,
        private readonly GuidedShoppingRequestContextResolver $guidedShoppingContextResolver,
        private readonly LineItemFactoryRegistry $lineItemFactory
    ) {
    }

    public function getDecorated(): AbstractCartItemAddRoute
    {
        return $this->decorated;
    }

    /**
     * @param LineItem[]|null $items
     */
    public function add(Request $request, Cart $cart, SalesChannelContext $context, ?array $items): CartResponse
    {
        try {
            $attendee = $this->guidedShoppingContextResolver->loadAttendee($context);
        } catch (\Exception $e) {
            return $this->decorated->add($request, $cart, $context, $items);
        }

        if (!$items) {
            $items = [];

            /** @var array<string|int, mixed> $item */
            foreach ($request->request->all('items') as $item) {
                $items[] = $this->lineItemFactory->create($item, $context);
            }
        }

        if ($this->isAddToCartInAppointmentContext($attendee)) {
            $appointmentId = $attendee->getAppointmentId();

            foreach ($items as $item) {
                $item->setPayloadValue(self::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER, true);
                $item->setPayloadValue(self::GUIDED_SHOPPING_ADD_TO_CART_APPOINTMENT_ID, $appointmentId);
            }
        }

        return $this->decorated->add($request, $cart, $context, $items);
    }

    private function isAddToCartInAppointmentContext(AttendeeEntity $attendee): bool
    {
        $appointment = $attendee->getAppointment();
        if (!$appointment->isActive()) {
            return false;
        }

        if ($appointment->getEndedAt()) {
            return false;
        }

        $now = new \DateTime();
        if (
            $appointment->getMode() === AppointmentDefinition::MODE_SELF
            && $appointment->getAccessibleTo()
            && $appointment->getAccessibleTo()->getTimestamp() < $now->getTimestamp()
        ) {
            return false;
        }

        return true;
    }
}
