<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Cart\Struct;

use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\System\Currency\CurrencyEntity;

class AttendeeCart
{
    /** @var AttendeeCartLineItem[] */
    private array $items = [];

    public function __construct(
        private readonly CashRounding $cashRounding,
        private readonly string $attendeeId
    ) {
    }

    public function addItem(AttendeeCartLineItem $cartInsightLineItem): void
    {
        $this->items[$cartInsightLineItem->getId()] = $cartInsightLineItem;
    }

    /**
     * @return AttendeeCartLineItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    public function getPriceNet(CurrencyEntity $currencyEntity): float
    {
        $priceNet = 0.0;
        foreach ($this->items as $item) {
            $priceNet += $this->cashRounding->cashRound($item->getTotalPriceNet(), $currencyEntity->getItemRounding());
        }

        return $this->cashRounding->cashRound($priceNet, $currencyEntity->getTotalRounding());
    }

    public function getAttendeeId(): string
    {
        return $this->attendeeId;
    }

    public function getProductCount(): int
    {
        $productCount = 0;
        foreach ($this->items as $item) {
            $productCount += $item->getQuantity();
        }

        return $productCount;
    }
}
