<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Cart\Struct;

use Shopware\Core\Framework\Struct\Struct;

class AttendeeCartLineItem extends Struct
{
    public function __construct(
        protected string $id,
        protected float $totalPriceNet,
        protected int $quantity
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTotalPriceNet(): float
    {
        return $this->totalPriceNet;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
