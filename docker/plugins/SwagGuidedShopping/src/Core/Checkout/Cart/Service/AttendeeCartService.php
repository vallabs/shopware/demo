<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Cart\Service;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Tax\Struct\CalculatedTax;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Core\Checkout\Cart\SalesChannel\CartItemAddRouteDecorator;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCartLineItem;

class AttendeeCartService
{
    public function __construct(
        private readonly Connection $dbalConnection,
        private readonly CashRounding $cashRounding,
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    /**
     * @param CurrencyEntity[] $currencyData
     * @return AttendeeCart[]
     * @throws \Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function createAttendeeCarts(string $appointmentId, array $currencyData, Context $context): array
    {
        $presentationStateService = $this->presentationStateServiceFactory->build($appointmentId, $context);
        $stateForGuides =  $presentationStateService->getStateForGuides();
        $cartData = $this->getCartData($stateForGuides->getAttendeeIdsOfClients());
        return $this->enrichCartData($cartData, $currencyData, $context->getCurrencyId());
    }

    /**
     * @param array<string> $attendeeIds
     * @return array<int, array<string, mixed>>
     * @throws Exception
     */
    public function getCartData(array $attendeeIds): array
    {
        if (!\count($attendeeIds)) {
            return [];
        }

        $tokens = $this->getTokensForAttendeeIds($attendeeIds);
        if (!\count($tokens)) {
            return [];
        }

        $sql = "
            SELECT
                cart.currency_id,
                cart.payload as payload,
                JSON_UNQUOTE(JSON_EXTRACT(sales_channel_api_context.payload, '$.attendeeId')) as attendee_id
            FROM cart
            INNER JOIN sales_channel_api_context ON cart.token = sales_channel_api_context.token
            WHERE cart.token IN (?)
        ";

        $resultStatement = $this->dbalConnection->executeQuery($sql, [$tokens], [ArrayParameterType::STRING]);

        return $resultStatement->fetchAllAssociative();
    }

    /**
     * @param array<int, array<string, mixed>> $cartData
     * @param CurrencyEntity[] $currencyData
     * @return AttendeeCart[]
     */
    private function enrichCartData(array $cartData, array $currencyData, string $defaultCurrencyId): array
    {
        if (!isset($currencyData[$defaultCurrencyId])) {
            return [];
        }

        $defaultCurrency = $currencyData[$defaultCurrencyId];

        $carts = [];
        foreach ($cartData as &$cartRow) {
            /** @var Cart $cart */
            $cart = \unserialize($cartRow['payload']);
            $cartCurrency = $currencyData[Uuid::fromBytesToHex($cartRow['currency_id'])] ?? null;

            $cartRow['price'] = $cart->getPrice()->getTotalPrice();

            $cartInsightCollection = new AttendeeCart($this->cashRounding, $cartRow['attendee_id']);

            /** @var LineItem $lineItem */
            foreach ($cart->getLineItems() as $lineItem) {
                $productId = $lineItem->getReferencedId();
                if (!$productId) {
                    continue;
                }

                if (!$lineItem->getPrice() instanceof CalculatedPrice) {
                    continue;
                }

                if (!$lineItem->getPayloadValue(CartItemAddRouteDecorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER)) {
                    continue;
                }

                $totalPrice = $this->calculatePriceInDefaultCurrency(
                    $defaultCurrency,
                    $cartCurrency,
                    $lineItem->getPrice()->getTotalPrice()
                );

                $item = new AttendeeCartLineItem($productId, $totalPrice, $lineItem->getQuantity());

                $cartInsightCollection->addItem($item);
            }

            $carts[$cart->getToken()] = $cartInsightCollection;
        }

        return $carts;
    }

    private function calculatePriceInDefaultCurrency(CurrencyEntity $defaultCurrency, ?CurrencyEntity $cartCurrency, float $price): float
    {
        if ($cartCurrency && $cartCurrency->getId() !== $defaultCurrency->getId()) {
            $price = $price / $cartCurrency->getFactor();
        }

        return $price;
    }

    /**
     * @param array<int, string> $attendeeIds
     * @return array<int, int|string>
     * @throws Exception
     */
    private function getTokensForAttendeeIds(array $attendeeIds): array
    {
        $sql = "
            SELECT
                token
            FROM sales_channel_api_context
            WHERE JSON_UNQUOTE(JSON_EXTRACT(payload, '$.attendeeId')) IN (?)
        ";

        $resultStatement = $this->dbalConnection->executeQuery($sql, [$attendeeIds], [ArrayParameterType::STRING]);

        return \array_keys($resultStatement->fetchAllAssociativeIndexed());
    }
}
