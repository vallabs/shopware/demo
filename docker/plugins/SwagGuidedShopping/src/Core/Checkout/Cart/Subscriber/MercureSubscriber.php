<?php declare(strict_types=1);

namespace SwagGuidedShopping\Core\Checkout\Cart\Subscriber;

use Shopware\Core\Checkout\Cart\Event\CartChangedEvent;
use Shopware\Core\Checkout\Cart\Event\CheckoutOrderPlacedCriteriaEvent;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Mercure\Service\AbstractPublisher;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MercureSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly GuidedShoppingRequestContextResolver $guidedShoppingContextResolver,
        private readonly AbstractPublisher $mercurePublisher,
        private readonly JWTMercureTokenFactory $JWTMercureTokenFactory,
        private readonly TopicGenerator $topicGenerator
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            CheckoutOrderPlacedCriteriaEvent::class => 'sendMercureEventForOrder',
            CartChangedEvent::class => 'sendMercureEventForLineItem',
        ];
    }

    public function sendMercureEventForOrder(CheckoutOrderPlacedCriteriaEvent $event): void
    {
        $this->sendMercureEvent($event->getSalesChannelContext());
    }

    public function sendMercureEventForLineItem(CartChangedEvent $event): void
    {
        $this->sendMercureEvent($event->getContext());
    }

    private function sendMercureEvent(SalesChannelContext $context): void
    {
        try {
            $attendee = $this->guidedShoppingContextResolver->loadAttendee($context);

            $topic = $this->topicGenerator->generateGuidePublisherTopic($attendee->getAppointmentId(), $attendee->getAppointment()->getMode());
            if ($topic) {
                $token = $this->JWTMercureTokenFactory->createPublisherToken([$topic]);
                $this->mercurePublisher->publish($token, ['changedCart' => $attendee->getId()], $topic);
            }
        } catch (\Exception $e) {
            // do nothing to be sure that the checkout flow is not interrupted
        }
    }
}
