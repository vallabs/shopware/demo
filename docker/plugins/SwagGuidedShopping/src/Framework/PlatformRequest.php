<?php declare(strict_types=1);

namespace SwagGuidedShopping\Framework;

final class PlatformRequest
{
    public const GUIDED_SHOPPING_MERCURE_TOKEN = 'gs-jwt-mercure-token';
}
