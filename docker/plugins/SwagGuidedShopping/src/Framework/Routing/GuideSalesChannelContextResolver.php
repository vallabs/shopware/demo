<?php declare(strict_types=1);

namespace SwagGuidedShopping\Framework\Routing;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Util\Random;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceInterface;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextServiceParameters;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use Symfony\Component\HttpFoundation\Request;

class GuideSalesChannelContextResolver
{
    public const CONTEXT_EXTENSION_NAME = 'guidedShoppingAttendee';
    public const TOKEN_FIELD_ATTENDEE_ID = 'attendeeId';
    public const ADMIN_CONTEXT_EXTENSION_NAME = 'salesChannelContextForGuidedShopping';
    public const HEADER_CONTEXT_TOKEN_ALIAS = PlatformRequest::HEADER_CONTEXT_TOKEN . '-alias';

    public function __construct(
        private readonly AppointmentService $appointmentService,
        private readonly SalesChannelContextServiceInterface $contextService,
        private readonly SalesChannelContextPersister $salesChannelContextPersister
    ) {
    }

    public function resolve(Request $request, ?string $appointmentId = null): void
    {
        $appointmentId = (string) $request->get('appointmentId', $appointmentId);

        if ($appointmentId === '') {
            return;
        }

        $context = $request->attributes->get(PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT);

        $appointment = $this->appointmentService->getAppointment($appointmentId, $context);

        $salesChannelContext = $this->fetchSalesChannelContext($request, $appointment, $context);

        $context->addExtension(self::ADMIN_CONTEXT_EXTENSION_NAME, $salesChannelContext);
    }

    private function fetchSalesChannelContext(Request $request, AppointmentEntity $appointment, Context $originalContext): SalesChannelContext
    {
        $salesChannelDomain = $appointment->getSalesChannelDomain();

        if (!$salesChannelDomain) {
            throw new NoSalesChannelDomainMappedException($appointment->getId());
        }

        $contextToken = $this->getContextToken($request);
        $salesChannelId = $salesChannelDomain->getSalesChannelId();
        $salesChannelLanguageId = $salesChannelDomain->getLanguageId();
        $salesChannelCurrencyId = $salesChannelDomain->getCurrencyId();
        $salesChannelDomainId = $appointment->getSalesChannelDomainId();

        // get language and currency from the sw-context-token
        $contextPayload = $this->salesChannelContextPersister->load($contextToken, $salesChannelId);

        $this->salesChannelContextPersister->save($contextToken, ['cacheKey' => Uuid::randomHex()], $salesChannelId);

        $parameters = new SalesChannelContextServiceParameters(
            $salesChannelId,
            $contextToken,
            $contextPayload['languageId'] ?? $salesChannelLanguageId,
            $contextPayload['currencyId'] ?? $salesChannelCurrencyId,
            $salesChannelDomainId,
            $originalContext
        );

        return $this->contextService->get($parameters);
    }

    private function getContextToken(Request $request): string
    {
        $contextToken = $request->headers->get(self::HEADER_CONTEXT_TOKEN_ALIAS);

        return $contextToken ?: Random::getAlphanumericString(32);
    }
}
