<?php declare(strict_types=1);

namespace SwagGuidedShopping\Framework\Routing;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Routing\RequestContextResolverInterface;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\SalesChannel\Exception\NotAuthenticatedException;
use Symfony\Component\HttpFoundation\Request;

class GuidedShoppingRequestContextResolver implements RequestContextResolverInterface
{
    public const CONTEXT_EXTENSION_NAME = 'guidedShoppingAttendee';
    public const TOKEN_FIELD_ATTENDEE_ID = 'attendeeId';
    public const ADMIN_CONTEXT_EXTENSION_NAME = 'salesChannelContextForGuidedShopping';

    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly SalesChannelContextPersister $channelContextPersister,
        private readonly EntityRepository $attendeeRepository
    ) {
    }

    public function resolve(Request $request): void
    {
        /** @var SalesChannelContext|null $salesChannelContext */
        $salesChannelContext = $request->attributes->get(PlatformRequest::ATTRIBUTE_SALES_CHANNEL_CONTEXT_OBJECT);

        /** @var Context|null $context */
        $context = $request->attributes->get(PlatformRequest::ATTRIBUTE_CONTEXT_OBJECT);

        if (!$this->isAuthenticationRequired($request, $context) && !$this->isJoinRoute($request)) {
            return;
        }

        if ($context && $context->hasExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME)) {
            /** @var SalesChannelContext|null $salesChannelContext */
            $salesChannelContext = $context->getExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME);
        }

        if (!$salesChannelContext) {
            return;
        }

        try {
            $attendee = $this->loadAttendee($salesChannelContext);
        } catch (AttendeeNotFoundException $e) {
            // do not add missing attendee information to context - a new one will be created
            return;
        } catch (NotAuthenticatedException $e) {
            if ($this->isJoinRoute($request)) {
                return;
            }
            throw $e;
        }

        $appointment = $attendee->getAppointment();
        if (!$appointment->getSalesChannelDomain()) {
            throw new NoSalesChannelDomainMappedException($appointment->getId());
        }

        $salesChannelContext->addExtension(self::CONTEXT_EXTENSION_NAME, $attendee);

        $this->updateSession($salesChannelContext);
    }

    private function isAuthenticationRequired(Request $request, ?Context $context): bool
    {
        $guideRequest = $context && $context->hasExtension(GuideSalesChannelContextResolver::ADMIN_CONTEXT_EXTENSION_NAME);

        return $this->isAttendeeRequired($request) || $guideRequest;
    }

    private function isAttendeeRequired(Request $request): bool
    {
        return $request->attributes->get('attendee_required', false);
    }

    private function isJoinRoute(Request $request): bool
    {
        return $request->attributes->get('join', false);
    }

    public function loadAttendee(SalesChannelContext $salesChannelContext): AttendeeEntity
    {
        $customer = $salesChannelContext->getCustomer();

        $customerId = null;
        if ($customer instanceof CustomerEntity) {
            $customerId = $customer->getId();
        }

        $salesChannelContextPayload = $this->channelContextPersister->load($salesChannelContext->getToken(),
            $salesChannelContext->getSalesChannelId(), $customerId);
        $attendeeId = $this->searchAttendeeIdInContext($salesChannelContextPayload);

        /** @var AttendeeEntity|null $attendee */
        $attendee = $this->attendeeRepository->search(new Criteria([$attendeeId]),
            $salesChannelContext->getContext())->first();

        if (!$attendee) {
            throw new AttendeeNotFoundException($attendeeId);
        }

        return $attendee;
    }

    private function updateSession(SalesChannelContext $salesChannelContext): void
    {
        $customerId = null;
        if ($salesChannelContext->getCustomer()) {
            $customerId = $salesChannelContext->getCustomer()->getId();
        }
        $this->channelContextPersister->save($salesChannelContext->getToken(), [],
            $salesChannelContext->getSalesChannelId(), $customerId);
    }

    /**
     * Check if already attendee is set in the payload (by join routes)
     *
     * @param array<string, mixed> $salesChannelContextPayload
     * @return string
     */
    private function searchAttendeeIdInContext(array $salesChannelContextPayload): string
    {
        if (!\array_key_exists(self::TOKEN_FIELD_ATTENDEE_ID, $salesChannelContextPayload)) {
            throw new NotAuthenticatedException('you need to join this appointment, before you can access this route');
        }

        return $salesChannelContextPayload[self::TOKEN_FIELD_ATTENDEE_ID];
    }
}
