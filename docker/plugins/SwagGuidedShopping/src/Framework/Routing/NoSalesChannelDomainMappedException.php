<?php declare(strict_types=1);

namespace SwagGuidedShopping\Framework\Routing;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class NoSalesChannelDomainMappedException extends ShopwareHttpException
{
    public function __construct(string $appointmentId)
    {
        parent::__construct(\sprintf('No SalesChannel domain mapped to the appointment (' . $appointmentId . ')'));
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__APPOINTMENT_NO_SALES_CHANNEL_DOMAIN_MAPPED;
    }
}
