<?php declare(strict_types=1);

namespace SwagGuidedShopping\Framework\Api\EventListener;

use SwagGuidedShopping\Framework\Routing\GuideSalesChannelContextResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CorsListener implements EventSubscriberInterface
{
    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse', 9998],
        ];
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $response = $event->getResponse();

        $corsHeaders = \explode(',', (string) $response->headers->get('Access-Control-Allow-Headers'));
        $corsHeaders[] = GuideSalesChannelContextResolver::HEADER_CONTEXT_TOKEN_ALIAS;

        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Methods', 'GET,POST,PUT,PATCH,DELETE');
        $response->headers->set('Access-Control-Allow-Headers', \implode(',', $corsHeaders));
        $response->headers->set('Access-Control-Expose-Headers', \implode(',', $corsHeaders));
    }
}
