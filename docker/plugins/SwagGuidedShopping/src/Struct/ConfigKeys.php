<?php declare(strict_types=1);

namespace SwagGuidedShopping\Struct;

class ConfigKeys
{
    public const BASE_URL = self::GS_CONFIG_BASE . 'baseUrl';
    public const MERCURE_HUB_SUBSCRIBER_SECRET = self::GS_CONFIG_BASE . 'mercureHubSubscriberSecret';
    public const MERCURE_HUB_PUBLISHER_SECRET = self::GS_CONFIG_BASE . 'mercureHubPublisherSecret';
    public const PRODUCT_LISTING_DEFAULT_PAGE_ID = self::GS_CONFIG_BASE . 'productListingDefaultPageId';
    public const PRODUCT_DETAIL_DEFAULT_PAGE_ID = self::GS_CONFIG_BASE . 'productDetailDefaultPageId';
    public const PRESENTATION_ENDED_PAGE_ID = self::GS_CONFIG_BASE . 'presentationEndedPageId';
    public const MERCURE_HUB_PUBLIC_URL = self::GS_CONFIG_BASE . 'mercureHubPublicUrl';
    public const MERCURE_HUB_URL = self::GS_CONFIG_BASE . 'mercureHubUrl';
    public const API_KEY = self::GS_CONFIG_BASE . 'apiKey';
    public const QUICK_VIEW_PAGE_ID = self::GS_CONFIG_BASE . 'quickviewPageId';
    public const ALLOW_USER_ACTIONS_FOR_GUIDE = self::GS_CONFIG_BASE . 'allowUserActionsForGuide';
    public const MAX_HOURS_BEFORE_CLOSE_APPOINTMENT = self::GS_CONFIG_BASE . 'maxAppointmentHoursWithoutInteractionBeforeClose';
    public const SEND_INVITATION_MAIL_TEMPLATE = self::GS_CONFIG_BASE . 'sendInvitationMailTemplate';
    public const CANCEL_INVITATION_MAIL_TEMPLATE = self::GS_CONFIG_BASE . 'cancelInvitationMailTemplate';
    private const GS_CONFIG_BASE = 'SwagGuidedShopping.config.';
}
