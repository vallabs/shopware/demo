<?php declare(strict_types=1);

namespace SwagGuidedShopping\Struct;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Struct\Struct;

class WidgetProductListing extends Struct
{
    /**
     * @param ProductEntity[] $products
     */
    public function __construct(
        protected array $products,
        protected int $total,
        protected int $page,
        protected int $limit
    ) {
        $this->products = \array_values($products);
    }

    /**
     * @return ProductEntity[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }
}
