<?php declare(strict_types=1);

namespace SwagGuidedShopping;

if (\file_exists(\dirname(__DIR__) . '/vendor/autoload.php')) {
    require_once \dirname(__DIR__) . '/vendor/autoload.php';
}

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\EntityDefinitionQueryHelper;
use Shopware\Core\Framework\Plugin;
use Shopware\Core\Framework\Plugin\Context\UninstallContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SwagGuidedShopping extends Plugin
{
    public function uninstall(UninstallContext $uninstallContext): void
    {
        parent::uninstall($uninstallContext);
        \assert($this->container instanceof ContainerInterface, 'Container is not set yet, please call setContainer() before calling boot(), see `src/Core/Kernel.php:186`.');

        if ($uninstallContext->keepUserData()) {
            return;
        }

        /** @var Connection $connection */
        $connection = $this->container->get(Connection::class);

        $tables = [
            'guided_shopping_appointment',
            'guided_shopping_appointment_attendee',
            'guided_shopping_appointment_video_chat',
            'guided_shopping_attendee_product_collection',
            'guided_shopping_interaction',
            'guided_shopping_presentation',
            'guided_shopping_presentation_cms_page',
            'guided_shopping_presentation_cms_page_translation',
            'guided_shopping_presentation_translation',
            'cms_section_translation'
        ];

        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($tables as $tableName) {
            $connection->executeStatement('DROP TABLE IF EXISTS ' . $tableName);
        }

        // Drop column created_by_id from cms_page table
        if (EntityDefinitionQueryHelper::columnExists($connection, 'cms_page', 'created_by_id')) {
            $connection->executeStatement('
                ALTER TABLE `cms_page`
                    DROP FOREIGN KEY `fk.cms_page.created_by_id`,
                    DROP COLUMN `created_by_id`
            ');
        }

        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1;');

        try {
            $connection->beginTransaction();
            foreach ($this->getRowsToDelete() as $tableName => $ids) {
                foreach ($ids as $id) {
                    $queryBuilder = $connection->createQueryBuilder();
                    $queryBuilder->delete($tableName)
                        ->where($queryBuilder->expr()->eq('id', $id))
                    ;
                    $connection->executeStatement($queryBuilder->getSQL());
                }
            }

            $connection->commit();
        } catch (\Exception $e) {
            $connection->rollBack();
            throw $e;
        }
    }

    /**
     * @return array<string, array<string>>
     */
    private function getRowsToDelete(): array
    {
        $toDelete['cms_page'] = [
            "X'33E88C7994FA4CF79A1265E5105B93B2'",
            "X'BEA211B5099241719830DF8026624F7F'",
            "X'182D3F7F988044ADBBA449B70C8BC472'",
            "X'8EA80092FAA744559409F3E9F7ADCC6B'",
        ];

        $toDelete['cms_section'] = [
            "X'2229E1F1208A4B8086BAF7AEC84F5E2C'",
            "X'F4AAA27972314E3C9B7F3BB35C7F8A10'",
            "X'14D1FA87BBAA489CA3034C62FB405F46'",
            "X'0A5E89814463470684A734B975ADD4ED'",
        ];

        $toDelete['cms_block'] = [
            "X'96EA8B9676A5461C9149D205D792ECF2'",
            "X'095CD9A4EB49493AA95EA1E7A84A9503'",
            "X'47CC4A3919794162982EA83F64A836FE'",
            "X'2D69EDD4FAAB493BAA056C81EA8D131F'",
            "X'C0D3DABA2E244122947438C28F776D41'",
            "X'9D2B5DCCB60F4682A2278BEE0B1EDB0D'",
            "X'A4659A44F27B44DCAE16CC8CAACA0534'",
            "X'0A13F5DAA9BE4E7B8ED5E3ECABA79FE6'",
            "X'C49B01E9C6624973B9CDD1992D3C009A'",
        ];

        $toDelete['cms_slot'] = [
            "X'80CF45856D9040E9A27666F4F86EF269'",
            "X'8EF2167945544E5DBF38426AF128C9D9'",
            "X'4853A97BBE30412BAC92E7E8526627C8'",
            "X'9AA4697356904C039098FBBAE29FC429'",
            "X'7946BD3200064451BBC2102ACEE52773'",
            "X'22EAFAB4CA954F0687E210FB766D8EDE'",
            "X'7A7F86DA1CAC49F5855497AEC03F99AA'",
            "X'2401E9E007BE4BE4B2386658A03A82FB'",
            "X'D5518ED1A3C24C9A88F94220ED8F28D7'",
            "X'5B39433F79BB4E2D93531242CFF2078A'",
            "X'CB3C1E3D528A414EAF0C805CF00AB605'",
            "X'870C93719C9445C3A6708EC5AE7215E4'",
            "X'31C877709A294DBEB89366EFE9F32EFE'",
        ];

        $toDelete['mail_template_type'] = [
            "X'0A7ACFDA21AD4052869219EE884AC069'",
            "X'E7AFC9E22B2B42F4A015A8389AF245B5'",
        ];

        $toDelete['mail_template'] = [
            "X'54682107D9924907A4D524760B17AECF'",
            "X'EEFEAC3598F84A23AFCB4485384032DE'",
        ];

        return $toDelete;
    }
}
