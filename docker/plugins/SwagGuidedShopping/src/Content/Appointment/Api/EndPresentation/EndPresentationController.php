<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\EndPresentation;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class EndPresentationController extends AbstractController
{
    public function __construct(
        private readonly AppointmentService $appointmentService
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/end', name: 'api.action.guided-shopping.appointment.end', methods: ['POST'])]
    public function end(string $appointmentId, Context $context): JsonResponse
    {
        $this->appointmentService->endPresentation($appointmentId, $context);

        return new JsonResponse();
    }
}
