<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide;

use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Uuid\Exception\InvalidUuidException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Framework\Routing\GuideSalesChannelContextResolver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class JoinAsGuideController extends AbstractController
{
    public function __construct(
        private readonly GuideSalesChannelContextResolver $salesChannelContextResolver,
        private readonly JoinAsGuideRequestHandler $requestHandler
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/join-as-guide', name: 'api.action.guided-shopping.appointment.join-as-guide', defaults: ['join' => true], methods: ['POST'])]
    public function join(string $appointmentId, Context $context, Request $request): JsonResponse
    {
        $adminApiSource = $context->getSource();
        if (!$adminApiSource instanceof AdminApiSource) {
            throw new \Exception('No admin api Source given');
        }

        try {
            $this->salesChannelContextResolver->resolve($request);
        } catch (InvalidUuidException $e) {
            throw new AppointmentNotFoundException();
        }

        return $this->requestHandler->handleRequest($appointmentId, $context, $adminApiSource);
    }
}
