<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class JoinAsGuidePermissionException extends ShopwareHttpException
{
    public function __construct()
    {
        $message = 'Could not join as guide, permissions are missing';
        parent::__construct($message);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_FORBIDDEN;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__JOIN_AS_GUIDE_MISSING_PERMISSIONS;
    }
}
