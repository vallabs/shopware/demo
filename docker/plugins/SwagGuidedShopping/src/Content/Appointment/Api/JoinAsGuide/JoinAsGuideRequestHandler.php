<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide;

use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\HttpFoundation\JsonResponse;

class JoinAsGuideRequestHandler
{
    final public const REQUIRED_GUIDE_PERMISSIONS = [
        'guided_shopping_appointment:update',
        'guided_shopping_presentation:read',
        'guided_shopping_appointment:read',
        'guided_shopping_appointment_attendee:read',
        'guided_shopping_appointment_attendee:create',
        'guided_shopping_appointment_attendee:update',
        'guided_shopping_appointment_video_chat:read',
        'guided_shopping_appointment_video_chat:update',
        'guided_shopping_appointment_video_chat:delete',
        'guided_shopping_attendee_product_collection:read',
        'guided_shopping_attendee_product_collection:create',
        'guided_shopping_attendee_product_collection:delete',
        'guided_shopping_presentation_cms_page:read',
        'guided_shopping_presentation_cms_page:update',
        'guided_shopping_presentation_cms_page:create',
        'guided_shopping_presentation_cms_page_translation:read',
        'guided_shopping_presentation_cms_page_translation:update',
        'guided_shopping_presentation_cms_page_translation:create',
        'guided_shopping_presentation_translation:read',
        'customer:read',
        'customer_address:read',
        'country:read',
        'order_customer:read',
        'order:read',
        'order_line_item:read',
        'state_machine_state:read',
        'product_stream:read',
        'product:read',
        'system_config:read',
    ];

    public function __construct(
        private readonly AppointmentService $appointmentService,
        private readonly JoinAppointmentService $joinAppointmentService
    ) {
    }

    /**
     * @throws \Exception
     */
    public function handleRequest(string $appointmentId, Context $context, AdminApiSource $adminApiSource): JsonResponse
    {
        if (!$this->isAllowedToGuide($adminApiSource)) {
            throw new JoinAsGuidePermissionException();
        }

        /** @var SalesChannelContext|null $salesChannelContext */
        $salesChannelContext = $context->getExtension(GuidedShoppingRequestContextResolver::ADMIN_CONTEXT_EXTENSION_NAME);
        if (!$salesChannelContext) {
            throw new \Exception('Sales channel context is not found.');
        }

        $appointment = $this->appointmentService->getAppointment($appointmentId, $context);

        $appointmentJoinStruct = $this->joinAppointmentService->joinMeetingAsGuide($appointment->getId(), $salesChannelContext, $appointment->getMode());

        $response = new JsonResponse($appointmentJoinStruct->getVars());

        $response->headers->set(PlatformRequest::HEADER_CONTEXT_TOKEN, $appointmentJoinStruct->getNewContextToken());

        return $response;
    }

    private function isAllowedToGuide(AdminApiSource $adminApiSource): bool
    {
        if ($adminApiSource->isAdmin()) {
            return true;
        }

        foreach (self::REQUIRED_GUIDE_PERMISSIONS as $permissionRequired) {
            if (!$adminApiSource->isAllowed($permissionRequired)) {
                return false;
            }
        }

        return true;
    }
}
