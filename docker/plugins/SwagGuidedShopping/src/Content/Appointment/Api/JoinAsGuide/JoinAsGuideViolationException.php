<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\JoinAsGuide;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class JoinAsGuideViolationException extends ShopwareHttpException
{
    public function __construct(ViolationList $violationsCollection)
    {
        $message = "Could join as guide \n";
        $message .= $violationsCollection;
        parent::__construct($message);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__JOIN_AS_GUIDE_VIOLATION;
    }
}
