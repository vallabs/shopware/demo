<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\StartPresentation;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class StartPresentationController extends AbstractController
{
    public function __construct(
        private readonly AppointmentService $appointmentService
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/start', name: 'api.action.guided-shopping.appointment.start', methods: ['POST'])]
    public function start(string $appointmentId, Context $context): JsonResponse
    {
        $this->appointmentService->startPresentation($appointmentId, $context);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
