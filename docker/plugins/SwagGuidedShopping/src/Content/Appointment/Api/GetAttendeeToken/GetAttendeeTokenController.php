<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Api\GetAttendeeToken;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeTokenNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\CartPermissionsNotGrantedException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class GetAttendeeTokenController extends AbstractController
{
    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly Connection $connection,
        private readonly EntityRepository $attendeeRepository
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/attendee/{attendeeId}/sw-context-token', name: 'api.action.guided-shopping.attendee.context-token', methods: ['GET'])]
    public function getToken(string $attendeeId, Context $context): JsonResponse
    {
        /** @var AttendeeEntity|null $attendee */
        $attendee = $this->attendeeRepository->search(new Criteria([$attendeeId]), $context)->first();
        if (!$attendee) {
            throw new AttendeeNotFoundException($attendeeId);
        }

        if (!$attendee->isGuideCartPermissionsGranted()) {
            throw new CartPermissionsNotGrantedException();
        }

        $token = $this->findToken($attendeeId);
        if (!$token) {
            throw new AttendeeTokenNotFoundException();
        }

        return new JsonResponse(['attendee-sw-context-token' => $token]);
    }

    private function findToken(string $attendeeId): ?string
    {
        try {
            $result = $this->connection->executeQuery(
                "SELECT `token` FROM `sales_channel_api_context` WHERE JSON_UNQUOTE(JSON_EXTRACT(payload, '$.attendeeId')) = :attendeeId",
                [
                    'attendeeId' => $attendeeId
                ]
            );

            return $result->fetchOne();
        } catch (\Exception) {
            return null;
        }
    }
}
