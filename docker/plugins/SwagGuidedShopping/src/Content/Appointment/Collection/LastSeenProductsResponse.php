<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Collection;

use Shopware\Core\Framework\Struct\Struct;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class LastSeenProductsResponse extends StoreApiResponse
{
    public function __construct(Struct $data)
    {
        parent::__construct($data);
    }
}
