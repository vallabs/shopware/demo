<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection;

use Shopware\Core\Framework\Struct\Struct;

class AttendeeProductCollectionStruct extends Struct
{
    /**
     * @param array<int|string, mixed> $collection
     */
    public function __construct(
        protected array $collection
    ) {
    }

    /**
     * @return array<int|string, mixed>
     */
    public function getCollection(): array
    {
        return $this->collection;
    }
}
