<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<AttendeeProductCollectionEntity>
 */
class AttendeeProductCollectionCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AttendeeProductCollectionEntity::class;
    }
}
