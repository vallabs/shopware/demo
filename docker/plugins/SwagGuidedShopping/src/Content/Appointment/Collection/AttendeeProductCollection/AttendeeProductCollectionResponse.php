<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection;

use Shopware\Core\System\SalesChannel\StoreApiResponse;

class AttendeeProductCollectionResponse extends StoreApiResponse
{
    public function __construct(AttendeeProductCollectionStruct $data)
    {
        parent::__construct($data);
    }
}
