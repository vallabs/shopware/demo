<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection;

use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;

class AttendeeProductCollectionDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_attendee_product_collection';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return AttendeeProductCollectionEntity::class;
    }

    public function getCollectionClass(): string
    {
        return AttendeeProductCollectionCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new FkField('attendee_id', 'attendeeId', AttendeeDefinition::class))->addFlags(new Required(), new ApiAware()),
                (new FkField('product_id', 'productId', ProductDefinition::class))->addFlags(new Required(), new ApiAware()),
                (new ReferenceVersionField(ProductDefinition::class))->addFlags(new Required()),
                (new StringField('alias', 'alias'))->addFlags(new Required()),

                new ManyToOneAssociationField('attendee', 'attendee_id', AttendeeDefinition::class),
                new ManyToOneAssociationField('product', 'product_id', ProductDefinition::class),
            ]
        );
    }
}
