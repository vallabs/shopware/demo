<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection;

use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;

class AttendeeProductCollectionEntity extends Entity
{
    use EntityIdTrait;

    protected string $attendeeId;

    protected ?AttendeeEntity $attendee;

    protected string $salesChannelContextToken;

    protected string $productId;

    protected ?ProductEntity $product;

    protected string $alias;

    public function getAttendeeId(): string
    {
        return $this->attendeeId;
    }

    public function setAttendeeId(string $attendeeId): void
    {
        $this->attendeeId = $attendeeId;
    }

    public function getAttendee(): ?AttendeeEntity
    {
        return $this->attendee;
    }

    public function setAttendee(?AttendeeEntity $attendee): void
    {
        $this->attendee = $attendee;
    }

    public function getSalesChannelContextToken(): string
    {
        return $this->salesChannelContextToken;
    }

    public function setSalesChannelContextToken(string $salesChannelContextToken): void
    {
        $this->salesChannelContextToken = $salesChannelContextToken;
    }

    public function getProductId(): string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    public function getProduct(): ?ProductEntity
    {
        return $this->product;
    }

    public function setProduct(?ProductEntity $product): void
    {
        $this->product = $product;
    }

    public function getAlias(): string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): void
    {
        $this->alias = $alias;
    }
}
