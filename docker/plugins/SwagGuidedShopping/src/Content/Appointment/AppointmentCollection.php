<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<AppointmentEntity>
 */
class AppointmentCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AppointmentEntity::class;
    }
}
