<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\Common\IterableQuery;
use Shopware\Core\Framework\DataAbstractionLayer\Dbal\Common\IteratorFactory;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Log\Package;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Elasticsearch\Admin\Indexer\AbstractAdminIndexer;

#[Package('administration')]
class AppointmentAdminSearchIndexer extends AbstractAdminIndexer
{
    /**
     * @param EntityRepository<AppointmentCollection> $repository
     */
    public function __construct(
        private readonly IteratorFactory $iteratorFactory,
        private readonly Connection $connection,
        private readonly EntityRepository $repository,
        private readonly int $indexingBatchSize
    ) {
    }

    public function getDecorated(): AbstractAdminIndexer
    {
        throw new DecorationPatternException(self::class);
    }

    public function getName(): string
    {
        return 'guided_shopping_appointment-listing';
    }

    public function getEntity(): string
    {
        return AppointmentDefinition::ENTITY_NAME;
    }

    public function getIterator(): IterableQuery
    {
        return $this->iteratorFactory->createIterator($this->getEntity(), null, $this->indexingBatchSize);
    }

    /**
     * @param array<string> $ids
     * @return array<int|string, array<string, mixed>>
     * @throws Exception
     */
    public function fetch(array $ids): array
    {
        $sql = "
            SELECT 
                LOWER(HEX(`guided_shopping_appointment`.`id`)) as id,
                `guided_shopping_appointment`.`name`,
                `guided_shopping_appointment`.`presentation_path`
            FROM `guided_shopping_appointment`
            WHERE `guided_shopping_appointment`.`id` IN (:ids)
                AND `guided_shopping_appointment`.`is_preview` = 0
                AND `guided_shopping_appointment`.`active` = 1
            GROUP BY `guided_shopping_appointment`.`id`
        ";

        $params = [
            'ids' => Uuid::fromHexToBytesList($ids),
        ];

        $data = $this->connection->executeQuery(
            $sql,
            $params,
            [
                'ids' => ArrayParameterType::STRING
            ]
        )->fetchAllAssociative();

        $mapped = [];
        foreach ($data as $item) {
            $id = $item['id'];
            $text = \implode(' ', \array_filter(\array_unique(\array_values($item))));
            $mapped[$id] = ['id' => $id, 'text' => \mb_strtolower($text)];
        }

        return $mapped;
    }

    /**
     * @inheritDoc
     */
    public function globalData(array $result, Context $context): array
    {
        $hits = \array_key_exists('hits', $result) && \is_array($result['hits']) ? $result['hits'] : [];
        $ids = !empty(\array_column($hits, 'id')) ? \array_column($hits, 'id') : null;

        /** @var EntitySearchResult<AppointmentCollection> $entities */
        $entities = $this->repository->search(new Criteria($ids), $context)->getEntities();

        return [
            'total' => \array_key_exists('total', $result) && \is_numeric($result['total']) ? (int) $result['total'] : 0,
            'data' => $entities
        ];
    }
}
