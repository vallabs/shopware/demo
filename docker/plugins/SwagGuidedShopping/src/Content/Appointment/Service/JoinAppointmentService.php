<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Util\Random;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use Shopware\Core\System\SalesChannel\Context\SalesChannelContextPersister;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Struct\AppointmentJoinStruct;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use SwagGuidedShopping\Struct\ConfigKeys;

class JoinAppointmentService
{
    public function __construct(
        private readonly TopicGenerator $topicGenerator,
        private readonly JWTMercureTokenFactory $JWTMercureTokenFactory,
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory,
        private readonly SalesChannelContextPersister $channelContextPersister,
        private readonly AttendeeService $attendeeService,
        private readonly SystemConfigService $systemConfigService,
        private readonly ContextDataHelper $contextDataReader
    ) {
    }

    public function joinMeetingAsClient(
        string $appointmentId,
        SalesChannelContext $context,
        string $appointmentMode,
        ?string $attendeeName = null
    ): AppointmentJoinStruct
    {
        $hubUrl = $this->getMercureHubPublicUrl();
        $struct = $this->joinMeeting($appointmentId, $context, false, $attendeeName);
        $clientSubscriberTopics = $this->topicGenerator->generateClientSubscriberTopics($appointmentId, $appointmentMode);
        $clientPublisherTopic = $this->topicGenerator->generateClientPublisherTopic($appointmentId, $appointmentMode);

        $this->addMercureDataToStruct($struct, $appointmentMode, $clientSubscriberTopics, $clientPublisherTopic, $hubUrl);

        return $struct;
    }

    public function joinMeetingAsGuide(
        string $appointmentId,
        SalesChannelContext $context,
        string $appointmentMode
    ): AppointmentJoinStruct
    {
        $hubUrl = $this->getMercureHubPublicUrl();
        $struct = $this->joinMeeting($appointmentId, $context, true);
        $guideSubscriberTopics = $this->topicGenerator->generateGuideSubscriberTopics($appointmentId, $appointmentMode);
        $guidePublisherTopic = $this->topicGenerator->generateGuidePublisherTopic($appointmentId, $appointmentMode);

        $this->addMercureDataToStruct($struct, $appointmentMode, $guideSubscriberTopics, $guidePublisherTopic, $hubUrl);

        return $struct;
    }

    public function joinMeeting(
        string $appointmentId,
        SalesChannelContext $salesChannelContext,
        bool $asGuide,
        ?string $attendeeName = null
    ): AppointmentJoinStruct
    {
        $attendeeType = $asGuide ? AttendeeDefinition::TYPE_GUIDE : AttendeeDefinition::TYPE_CLIENT;
        $contextToken = $salesChannelContext->getToken();

        // clear up customer login if the user wants to log in as guide
        // and always revoke storefront context for guide (because it's cached without using original context fully for cache key)
        $customerId = $this->contextDataReader->getCustomerIdFromContext($salesChannelContext);
        if ($attendeeType === AttendeeDefinition::TYPE_GUIDE) {
            $contextToken = $this->revokeContext($salesChannelContext);
            $customerId = null;
        }

        // load attendee from context if already joined
        $attendee = $this->contextDataReader->getAttendeeFromCredentials($salesChannelContext, $appointmentId, $attendeeType);
        $attendee = $attendee ?? $this->contextDataReader->getAttendeeFromContext($salesChannelContext);

        // revoke invalid attendee and create new context token if user wants to join as guide or guide wants to join as client or appointment is wrong
        if ($this->shouldRevokeInvalidAttendee($attendee, $appointmentId, $attendeeType)) {
            if ($attendee instanceof AttendeeEntity) {
                $this->instantlyRemoveAttendeeFromOldAppointment($attendee, $salesChannelContext, $customerId);
            }
            $attendee = null;
        }

        // create new attendee of nothing was found
        if (!$attendee) {
            $attendee = $this->attendeeService->createAttendee(
                $appointmentId,
                $salesChannelContext,
                $attendeeType,
                $customerId,
                $this->contextDataReader->getUserIdFromContext($salesChannelContext),
                $this->getAttendeeName($attendeeName, $salesChannelContext)
            );
        }

        // Appointment Validation
        $appointment = $attendee->getAppointment();
        $salesChannelDomain = $appointment->getSalesChannelDomain();

        if (!$salesChannelDomain instanceof SalesChannelDomainEntity) {
            throw new NoSalesChannelDomainMappedException($appointmentId);
        }

        // for appointment switches with same user / context
        $this->channelContextPersister->save(
            $contextToken,
            [
                GuidedShoppingRequestContextResolver::TOKEN_FIELD_ATTENDEE_ID => $attendee->getId(),
                'languageId' => $salesChannelContext->getLanguageId(),
                'currencyId' => $salesChannelContext->getCurrencyId(),
            ],
            $salesChannelContext->getSalesChannelId(),
            $customerId
        );

        // attendee was invited via admin but has not joined yet
        if (!$attendee->getJoinedAt() || !$attendee->getAttendeeName()) {
            $attendee->setAttendeeName($attendee->getAttendeeName() ?? $this->getAttendeeName($attendeeName, $salesChannelContext));
            if ($appointment->getMode() === AppointmentDefinition::MODE_GUIDED) {
                $attendee->setJoinedAt($appointment->getStartedAt() ? new \DateTime() : NULL);
            } else { // self mode
                $attendee->setJoinedAt(new \DateTime());
            }
            $attendee->setLastActive(new \DateTime());
        } else {
            $attendee->setLastActive(new \DateTime());
            $attendee->setCustomerId($customerId);
            if ($salesChannelContext->getCustomer() instanceof CustomerEntity) {
                //make sure the customer name is set instead of the nickname
                $attendee->setAttendeeName($this->getAttendeeName($attendee->getAttendeeName(), $salesChannelContext));
            }
        }

        $this->attendeeService->updateAttendee($attendee->getId(),
            [
                'attendeeName' => $attendee->getAttendeeName(),
                'joinedAt' => $attendee->getJoinedAt(),
                'lastActive' => $attendee->getLastActive(),
                'customerId' => $attendee->getCustomerId(),
            ],
            $salesChannelContext->getContext()
        );
        // prepare state for update guides "act as attendees action list
        $this->publishAttendeeToState($attendee, $salesChannelContext);

        $struct = new AppointmentJoinStruct(
            $appointmentId,
            $contextToken,
            $attendee->getId(),
            $salesChannelDomain->getSalesChannelId(),
            $appointment->getMode(),
            $appointment->isPreview()
        );

        $struct->setSalesChannelName($salesChannelContext->getSalesChannel()->getTranslation('name'));
        $struct->setAppointmentName($appointment->getTranslation('name'));

        return $struct;
    }

    private function instantlyRemoveAttendeeFromOldAppointment(
        AttendeeEntity $attendee,
        SalesChannelContext $salesChannelContext,
        ?string $customerId
    ): void
    {
        $stateService = $this->presentationStateServiceFactory->build($attendee->getAppointmentId(), $salesChannelContext->getContext());
        $stateForGuide = $stateService->getStateForGuides();
        $stateForGuide->removeAttendee($attendee->getId());
        $stateService->publishStateForGuides();

        $joinedAt = new \DateTime();
        $joinedAt->sub(new \DateInterval('PT5M'));
        $attendeeData = [
            'id' => $attendee->getId(),
            'lastActive' => new \DateTime($joinedAt->format('Y-m-d H:i:s')),
        ];
        $this->attendeeService->updateAttendee(
            $attendee->getId(),
            $attendeeData,
            $salesChannelContext->getContext()
        );

        // for appointment switches with same user / context
        $this->channelContextPersister->save(
            $salesChannelContext->getToken(),
            [GuidedShoppingRequestContextResolver::TOKEN_FIELD_ATTENDEE_ID => null],
            $salesChannelContext->getSalesChannelId(),
            $customerId
        );
    }

    private function getAttendeeName(?string $fallbackName, SalesChannelContext $salesChannelContext): ?string
    {
        return $this->contextDataReader->getAttendeeNameFromContext($salesChannelContext) ?? $fallbackName;
    }

    private function shouldRevokeInvalidAttendee(
        ?AttendeeEntity $attendee,
        string $appointmentId,
        string $attendeeType
    ): bool
    {
        if (!$attendee) {
            return false;
        }

        if ($attendee->getType() !== $attendeeType) {
            return true;
        }

        if ($attendee->getAppointmentId() !== $appointmentId) {
            return true;
        }

        return false;
    }

    private function revokeContext(SalesChannelContext $context): string
    {
        $this->channelContextPersister->delete($context->getToken(), $context->getSalesChannelId());

        return Random::getAlphanumericString(32);
    }

    private function publishAttendeeToState(AttendeeEntity $attendee, SalesChannelContext $salesChannelContext): void
    {
        $presentationSateService = $this->presentationStateServiceFactory->build($attendee->getAppointmentId(), $salesChannelContext->getContext());

        $stateForGuides = $presentationSateService->getStateForGuides();
        $stateForGuides->addAttendee($attendee);
        $presentationSateService->publishStateForGuides();

        $stateForClients = $presentationSateService->getStateForClients();
        $stateForClients->storeParticipants($stateForGuides);
        $presentationSateService->publishStateForClients();
    }

    /**
     * @param array<string> $subscriberTopics
     */
    private function addMercureDataToStruct(
        AppointmentJoinStruct $struct,
        string $appointmentMode,
        array $subscriberTopics,
        ?string $publisherTopic,
        string $hubUrl
    ): void
    {
        if ($appointmentMode === AppointmentDefinition::MODE_SELF) {
            return;
        }

        if (\count($subscriberTopics)) {
            $struct->setJWTMercureSubscriberToken($this->JWTMercureTokenFactory->createSubscriberToken($subscriberTopics));
        }

        if ($publisherTopic) {
            $struct->setJWTMercurePublisherToken($this->JWTMercureTokenFactory->createPublisherToken([$publisherTopic]));
        }

        $struct->setMercureSubscriberTopics($subscriberTopics);
        $struct->setMercurePublisherTopic($publisherTopic);
        $struct->setMercureHubPublicUrl($hubUrl);
    }

    private function getMercureHubPublicUrl(): string
    {
        $hubUrl = $this->systemConfigService->getString(ConfigKeys::MERCURE_HUB_PUBLIC_URL);
        if ($hubUrl === '') {
            throw new \Exception('Wrong mercure hub public url parameter configuration');
        }

        return $hubUrl;
    }
}
