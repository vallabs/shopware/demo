<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;

class LastSeenProductsService
{
    /**
     * @param EntityRepository<ProductCollection> $productRepository
     */
    public function __construct(
        private readonly InteractionService $interactionService,
        private readonly EntityRepository $productRepository
    ) {
    }

    /**
     * @return array<string>
     */
    public function getProducts(AttendeeEntity $attendee, SalesChannelContext $context): array
    {
        $interactions = $this->interactionService->getInteractions(
            [InteractionName::PRODUCT_VIEWED, InteractionName::QUICKVIEW_OPENED],
            $context->getContext(),
            $attendee->getId()
        );

        if (!\count($interactions)) {
            return [];
        }

        $productIdsSorted = $this->getProductIdsFromInteractions($interactions);

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsAnyFilter('id', $productIdsSorted));
        $products = $this->productRepository->search($criteria, $context->getContext());

        $products->sortByIdArray($productIdsSorted);

        $productIdsSortedAndGroupedByParent = [];
        /** @var ProductEntity $product */
        foreach ($products->getElements() as $product) {
            $key = $product->getParentId() ?? $product->getId();
            if (\array_key_exists($key, $productIdsSortedAndGroupedByParent)) {
                continue;
            }

            $productIdsSortedAndGroupedByParent[$key] = $product->getId();
        }

        return \array_values($productIdsSortedAndGroupedByParent);
    }

    /**
     * @param array<string, Interaction> $interactions
     * @return array<string>
     */
    private function getProductIdsFromInteractions(array $interactions): array
    {
        $productIds = [];
        foreach ($interactions as $interaction) {
            /** @var ProductPayload $payload */
            $payload = $interaction->getPayload();
            if (\in_array($payload->getProductId(), $productIds)) {
                continue;
            }

            $productIds[] = $payload->getProductId();
        }

        return $productIds;
    }
}
