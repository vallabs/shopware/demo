<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAlreadyEndedException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAlreadyStartedException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsPreviewException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsSelfModeException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetStartedException;
use SwagGuidedShopping\Content\Cms\Service\CmsPageCreatorService;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatCreateStruct;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;
use SwagGuidedShopping\Exception\NotConfiguredException;
use SwagGuidedShopping\Struct\ConfigKeys;

class AppointmentService
{
    protected const PRESENTATION_ENDED_PAGE_NAME = 'THE END';

    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly SystemConfigService $systemConfigService,
        private readonly EntityRepository $appointmentRepository,
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory,
        private readonly VideoRoomManager $videoRoomManager,
        private readonly CmsPageCreatorService $cmsPageCreatorService,
        private readonly InteractionService $interactionService,
        private readonly PresentationVersionHandler $presentationVersionHandler,
        private readonly EntityRepository $attendeeRepository
    ) {
    }

    /**
     * @param array<string> $associations
     */
    public function getAppointment(
        string $appointmentId,
        Context $context,
        array $associations = []
    ): AppointmentEntity
    {
        $criteria = new Criteria([$appointmentId]);

        if (\count($associations)) {
            $criteria->addAssociations($associations);
        }

        if (\in_array('presentation.cmsPages', $associations)) {
            $criteria->getAssociation('presentation.cmsPages')
                ->addSorting(new FieldSorting('position'));
        }

        /** @var AppointmentEntity|null $appointment */
        $appointment = $this->appointmentRepository->search($criteria, $context)->first();
        if (!$appointment) {
            throw new EntityNotFoundException(AppointmentEntity::class, $appointmentId);
        }

        return $appointment;
    }

    public function findAppointmentByUrl(string $presentationPath, Context $context): ?AppointmentEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationPath', $presentationPath));
        $criteria->addAssociation('attendees');
        $criteria->addAssociation('presentation');

        $appointments = $this->appointmentRepository->search($criteria, $context);

        if ($appointments->getTotal()) {
            /** @var AppointmentEntity|null $appointment */
            $appointment = $appointments->first();

            return $appointment;
        }

        // if there is no path specified the slug is the appointmentId
        try {
            return $this->getAppointment($presentationPath, $context, ['attendees']);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function startPresentation(string $appointmentId, Context $context): void
    {
        $appointment = $this->getAppointment($appointmentId, $context, ['presentation.cmsPages']);
        if ($appointment->isPreview()) {
            throw new AppointmentIsPreviewException();
        }

        if ($appointment->getStartedAt()) {
            throw new AppointmentAlreadyStartedException();
        }

        if ($appointment->getEndedAt()) {
            throw new AppointmentAlreadyEndedException();
        }

        if ($appointment->getMode() === AppointmentDefinition::MODE_SELF) {
            throw new AppointmentIsSelfModeException();
        }

        if ($appointment->getVideoAudioSettings() !== AppointmentDefinition::NONE_VIDEO_AUDIO) {
            if (!$appointment->getVideoChat() instanceof VideoChatEntity) {
                throw new VideoChatEntityDoesNotExist($appointmentId);
            }

            $roomData = $this->videoRoomManager->createRoom($appointment->getVideoChat()->isStartAsBroadcast());
            $this->videoRoomManager->saveVideoChatData($appointment->getVideoChat()->getId(), $roomData, $context);
        }

        $presentationVersionId = $this->presentationVersionHandler->createPresentationVersion($appointment, $context);
        $this->presentationVersionHandler->updatePresentationCmsPages($appointment, $presentationVersionId, $context);

        $startedAt = new \DateTime();
        $this->appointmentRepository->upsert([['id' => $appointmentId, 'startedAt' => $startedAt]], $context);
        $this->setPresentationStateForStarted($appointment, $startedAt, $roomData ?? null, $context);
    }

    public function isPresentationEnable(AppointmentEntity $appointment): bool
    {
        return !(
            !$appointment->getPresentationVersionId()
            || $appointment->getMode() !== AppointmentDefinition::MODE_GUIDED
        );
    }

    public function endPresentation(string $appointmentId, Context $context): void
    {
        $appointment = $this->getAppointment($appointmentId, $context, ['presentation']);

        if ($appointment->getEndedAt()) {
            throw new AppointmentAlreadyEndedException();
        }

        if ($appointment->getMode() !== AppointmentDefinition::MODE_GUIDED) {
            throw new AppointmentIsSelfModeException();
        }

        if (!$appointment->getStartedAt() || !$appointment->getPresentationVersionId()) {
            throw new AppointmentNotYetStartedException();
        }

        $listingPageLayoutId = $this->systemConfigService->getString(ConfigKeys::PRESENTATION_ENDED_PAGE_ID);
        if (\trim($listingPageLayoutId) === '') {
            throw new NotConfiguredException('presentationEndedPageId need to be configured');
        }

        $productViewedInteractions = $this->interactionService->getInteractions([InteractionName::PRODUCT_VIEWED], $context, null, $appointmentId, [], ['payload'], AttendeeDefinition::TYPE_GUIDE);

        $productIdsViewed = [];
        foreach ($productViewedInteractions as $productViewed) {
            /** @var ProductPayload $productPayload */
            $productPayload = $productViewed->getPayload();
            $productIdsViewed[] = $productPayload->getProductId();
        }

        $position = $this->cmsPageCreatorService->getPositionForLastSlide($appointment->getPresentationId(), $appointment->getPresentationVersionId(), $context) + 1;
        $this->cmsPageCreatorService->createPresentationPage($listingPageLayoutId, $productIdsViewed, $appointment->getPresentationId(), $appointment->getPresentationVersionId(), $position, false, self::PRESENTATION_ENDED_PAGE_NAME, $context);

        if ($appointment->getVideoChat()) {
            if (!$appointment->getVideoChat()->getName()) {
                $this->videoRoomManager->removeRoomFromAppointment($appointment->getVideoChat()->getId(), $context);
            } else {
                $this->videoRoomManager->deleteRoom($appointment->getVideoChat()->getName(), $context);
            }
        }

        $ended = new \DateTime();
        $this->appointmentRepository->upsert([
            [
                'id' => $appointment->getId(),
                'mode' => AppointmentDefinition::MODE_SELF,
                'endedAt' => $ended,
                'videoAudioSettings' => AppointmentDefinition::NONE_VIDEO_AUDIO,
            ],
        ], $context);

        $presentationStateService = $this->presentationStateServiceFactory->build($appointment->getId(), $context);
        $presentationStateAll = $presentationStateService->getStateForAll();
        $presentationStateAll->setEndedAt($ended);
        $presentationStateAll->setVideoAudioSettings(AppointmentDefinition::NONE_VIDEO_AUDIO);
        $presentationStateAll->setAppointmentMode(AppointmentDefinition::MODE_SELF);
        $presentationStateAll->setDynamicPageClosed();
        $presentationStateService->publishStateForAll();
    }

    private function setPresentationStateForStarted(
        AppointmentEntity $appointment,
        \DateTime $startedAt,
        ?VideoChatCreateStruct $roomData,
        Context $context
    ): void
    {
        $presentationStateService = $this->presentationStateServiceFactory->build($appointment->getId(), $context);

        $presentationStateAll = $presentationStateService->getStateForAll();
        $presentationStateClients = $presentationStateService->getStateForClients();
        $presentationStateGuides = $presentationStateService->getStateForGuides();

        $activeAttendeeIds = $presentationStateGuides->getAttendeeIdsOfClients();
        $guideIds = $presentationStateGuides->getAttendeeIdsOfGuides();
        // update time attendee joined presentation
        $this->updateAttendee(\array_merge($activeAttendeeIds, $guideIds), $context);

        $presentationStateAll->setAppointmentMode($appointment->getMode());
        $presentationStateAll->setAttendeeRestrictionType($appointment->getAttendeeRestrictionType());
        $presentationStateAll->setStartedAt($startedAt);
        $presentationStateAll->setAccessibleFrom($appointment->getAccessibleFrom());
        $presentationStateAll->setAccessibleTo($appointment->getAccessibleTo());
        $presentationStateAll->setVideoAudioSettings($appointment->getVideoAudioSettings());

        if ($roomData) {
            $presentationStateAll->setVideoRoomUrl($roomData->getRoomUrl());
            $presentationStateAll->setBroadcastMode($roomData->startAsBroadcast());
            $presentationStateClients->setVideoClientToken($roomData->getUserToken());
            $presentationStateGuides->setVideoGuideToken($roomData->getOwnerToken());
        }

        $presentationStateService->publishStateForAll();
        $presentationStateService->publishStateForGuides();
        $presentationStateService->publishStateForClients();
    }

    public function getAppointmentInvitationMailTemplateId(string $invitationAction): string
    {
        $mailTemplateKey = AppointmentDefinition::INVITATION_MAIL_TEMPLATE_TYPE[$invitationAction];
        return $this->systemConfigService->getString($mailTemplateKey);
    }

    /**
     * @param array<string> $attendeeIds
     */
    private function updateAttendee(array $attendeeIds, Context $context): void
    {
        $joinedAt = new \DateTime();
        $updateData = [];

        foreach ($attendeeIds as $attendeeId) {
            $updateData[] = [
                'id' => $attendeeId,
                'joinedAt' => $joinedAt
            ];
        }

        $this->attendeeRepository->update($updateData, $context);
    }
}
