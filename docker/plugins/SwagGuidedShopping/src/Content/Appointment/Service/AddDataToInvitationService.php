<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Psr\Log\LoggerInterface;
use Shopware\Core\Content\Mail\Service\AbstractMailService;
use Shopware\Core\Content\MailTemplate\MailTemplateCollection;
use Shopware\Core\Content\MailTemplate\MailTemplateEntity;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\User\UserCollection;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationCouldNotSendException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentInvitationMissingStartOrEndTimeException;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;
use Symfony\Component\Mime\Email;
use SwagGuidedShopping\Content\Appointment\Traits\IcsTrait;
use Symfony\Component\Mime\Part\TextPart;

class AddDataToInvitationService extends AbstractMailService
{
    use IcsTrait;

    /**
     * @param EntityRepository<MailTemplateCollection> $mailTemplateRepository
     * @param EntityRepository<UserCollection> $userRepository
     */
    public function __construct(
        private readonly AbstractMailService $mailService,
        private readonly AppointmentService $appointmentService,
        private readonly EntityRepository $mailTemplateRepository,
        private readonly EntityRepository $userRepository,
        private readonly AttendeeService $attendeeService,
        private readonly LoggerInterface $logger
    ) {
    }

    public function getDecorated(): AbstractMailService
    {
        return $this->mailService;
    }

    /**
     * @param mixed[] $data
     * @param mixed[] $templateData
     */
    public function send(array $data, Context $context, array $templateData = []): ?Email
    {
        /**
         * Check invitationAction parameters
         * to check if there's an appointment invitation action
         */
        if (
            isset($data['invitationAction'])
            && \in_array($data['invitationAction'], AppointmentDefinition::INVITATION_ACTIONS)
        ) {
            if (!isset($data['appointmentId']) || !Uuid::isValid($data['appointmentId'])) {
                throw new AppointmentInvitationCouldNotSendException('Invalid appointment id.');
            }

            $appointmentId = $data['appointmentId'];
            $appointment = $this->appointmentService->getAppointment(
                $appointmentId,
                $context,
                ['guideUser', 'attendees.customer.defaultShippingAddress.country', 'salesChannelDomain.salesChannel']
            );

            $salesChannelDomain = $appointment->getSalesChannelDomain();
            if (!$salesChannelDomain) {
                throw new AppointmentInvitationCouldNotSendException('Appointment sales channel domain is empty.');
            }

            if (!$appointment->getAttendees()->count()) {
                throw new AppointmentInvitationCouldNotSendException('No attendees to send mail.');
            }

            $dateFrom = $appointment->getAccessibleFrom();
            $dateTo = $appointment->getAccessibleTo();
            $isGuideAppointment = $appointment->getMode() === AppointmentDefinition::MODE_GUIDED;

            if ($isGuideAppointment && (!$dateFrom || !$dateTo)) {
                throw new AppointmentInvitationMissingStartOrEndTimeException();
            }

            $invitationAction = $data['invitationAction'];
            $isCancelAction = \in_array($invitationAction, [AppointmentDefinition::CANCEL_ACTION]);

            $content = $this->getMailTemplateContent($invitationAction, $context);
            $data = \array_merge($data, $content);

            /** @var string $appointmentName */
            $appointmentName = $appointment->getTranslation('name');
            $customFields = []; // the additional fields to render mail template
            $timeZone = $this->getTimeZone($context);

            if ($isCancelAction) {
                $data['subject'] = "CANCELLED: {$appointmentName}";
                $customFields['cancelMessage'] = $data['cancelMessage'];
            } else {
                $data['subject'] = \ucfirst($appointmentName);

                if ($dateFrom && $dateTo) {
                    $customFields['accessibleFrom'] = $this->extractAccessibleTimeInfo($dateFrom, $timeZone);
                    $customFields['accessibleTo'] = $this->extractAccessibleTimeInfo($dateTo, $timeZone);
                    $customFields['isSameDay'] = $customFields['accessibleFrom']['date'] === $customFields['accessibleTo']['date'];
                }
            }

            $guide = $appointment->getGuideUser();
            $data['senderName'] = $guide ? "{$guide->getFirstName()} {$guide->getLastName()}" : "Shopware Digital Sales Room";
            $data['salesChannelId'] = $salesChannelDomain->getSalesChannelId();

            $recipients = $appointment->getAttendees()->getAppointmentRecipients();
            if ($isGuideAppointment && $guide) {
                $data['binAttachments'][] = $this->generateIcsFile(
                    $appointment,
                    $recipients,
                    $invitationAction
                );

                $recipients[$guide->getEmail()] = [
                    'name' => $appointment->getGuideName(),
                    'isGuide' => true,
                    'invitationStatus' => null
                ];
            }

            $failedMails = [];
            $succeedMails = [];
            /**
             * It's about counting the total size of all the mail sent successfully
             * @var Email|null $mailResult
             */
            $mailResult = null;

            /**
             * @var string $emailAddress
             * @var array<string, mixed> $info
             */
            foreach ($recipients as $emailAddress => $info) {
                // Only send the new attendee if there's a regular invite action
                if ($invitationAction === AppointmentDefinition::INVITE_ACTION && $info['invitationStatus']) {
                    continue;
                }

                // Cancel appointment with individual recipient
                if ($isCancelAction && isset($data['recipientEmail']) && \mb_strlen($data['recipientEmail']) && $emailAddress !== $data['recipientEmail']) {
                    continue;
                }

                // Re-assign recipient info to send mail
                $data['recipients'] = [];
                $data['recipients'][$emailAddress] = $info['name'];

                //  Add necessary info into invitation mail with each recipient
                if (!$isCancelAction) {
                    $customFields['isGuide'] = isset($info['isGuide']) && $info['isGuide'] === true;
                    $customFields['token'] = EncryptHelper::encryptString($emailAddress);
                }

                // switch timezone if the customer exists in Shopware system
                if (isset($info['timeZone']) && $info['timeZone']) {
                    $customerTimeZone = $info['timeZone'];

                    if ($dateFrom && $dateTo) {
                        $customFields['accessibleFrom'] = $this->extractAccessibleTimeInfo($dateFrom, $customerTimeZone);
                        $customFields['accessibleTo'] = $this->extractAccessibleTimeInfo($dateTo, $customerTimeZone);
                    }
                }

                // Add template data to render mail content
                $appointment->setCustomFields($customFields);
                $templateData['appointment'] = $appointment;

                $mail = $this->mailService->send($data, $context, $templateData);
                if ($mail) {
                    $succeedMails[] = $emailAddress;

                    if (!$mailResult) {
                        $mailResult = $mail;
                    } else {
                        $currentMailContent = $mailResult->toString();

                        $mailResult->setBody(new TextPart($currentMailContent . $mail->toString()));
                    }
                } else {
                    $failedMails[] = $emailAddress;
                }
            }

            // only update status if there's an invitation mail of guide appointment
            if (!$isCancelAction && $isGuideAppointment) {
                $this->attendeeService->updateInvitationStatus($succeedMails, $appointmentId, 'pending', $context);
            }

            $this->logSendMail($succeedMails, $failedMails);

            return $mailResult;
        }

        return $this->mailService->send($data, $context, $templateData);
    }

    /**
     * @return array<string>
     */
    private function getMailTemplateContent(string $invitationAction, Context $context): array
    {
        $mailTemplateId = $this->appointmentService->getAppointmentInvitationMailTemplateId($invitationAction);
        if ($mailTemplateId === '') {
            throw new AppointmentInvitationCouldNotSendException('The mail template is not set in the Plugin configuration.');
        }

        /** @var MailTemplateEntity|null $mailTemplate */
        $mailTemplate = $this->mailTemplateRepository->search(new Criteria([$mailTemplateId]), $context)->first();
        if (!$mailTemplate) {
            throw new AppointmentInvitationCouldNotSendException('Mail template not found.');
        }

        if (!$mailTemplate->getContentHtml()) {
            throw new AppointmentInvitationCouldNotSendException('Mail template content in html is blank.');
        }

        if (!$mailTemplate->getContentPlain()) {
            throw new AppointmentInvitationCouldNotSendException('Mail template content in plain text is blank.');
        }

        $data['contentHtml'] = $mailTemplate->getContentHtml();
        $data['contentPlain'] = $mailTemplate->getContentPlain();

        return $data;
    }

    private function getTimeZone(Context $context): string
    {
        $contextSource = $context->getSource();

        if ($contextSource instanceof AdminApiSource) { // Get the timezone of the admin user who sends the mail
            $userId = $contextSource->getUserId();

            if ($userId) {
                $adminUser = $this->userRepository->search(new Criteria([$userId]), $context)->first();

                if ($adminUser instanceof UserEntity) {
                    return $adminUser->getTimeZone();
                }
            }
        }

        return 'UTC';
    }

    /**
     * @return array<string>
     */
    private function extractAccessibleTimeInfo(\DateTimeInterface $dateTime, string $timezone): array
    {
        $formattedDateTime = \DateTime::createFromInterface($dateTime)->setTimezone(new \DateTimeZone($timezone));

        return [
            'date' => $formattedDateTime->format('d M Y'),
            'time' => $formattedDateTime->format('H:i'),
            'timezone' => $timezone
        ];
    }

    /**
     * @param array<string> $succeedMails
     * @param array<string> $failedMails
     */
    private function logSendMail(array $succeedMails, array $failedMails): void
    {
        $this->logger->info(\sprintf("[APPOINTMENT INVITATION MAIL] Succeed at sending mail to %s", @\json_encode($succeedMails)));
        $this->logger->warning(\sprintf("[APPOINTMENT INVITATION MAIL] Failed at sending mail to %s", @\json_encode($failedMails)));
    }
}
