<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAuthModeNotSetException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentAuthRuleNotSetException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentForbiddenException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotYetAccessibleException;
use SwagGuidedShopping\Content\Presentation\Exception\PresentationNotFoundException;

class ClientJoinAppointmentValidator
{
    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly EntityRepository $attendeeRepository
    ) {
    }

    public function validate(AppointmentEntity $appointmentEntity, SalesChannelContext $salesChannelContext): void
    {
        if (!$appointmentEntity->isActive()) {
            throw new AppointmentNotFoundException();
        }

        $presentation = $appointmentEntity->getPresentation();
        if (!$presentation || !$presentation->isActive()) {
            throw new PresentationNotFoundException();
        }

        // the accessible times only apply to self-appointments
        if ($appointmentEntity->getMode() === AppointmentDefinition::MODE_SELF && !$appointmentEntity->getEndedAt()) {
            if ($this->isOutdated($appointmentEntity)) {
                throw new AppointmentNotFoundException();
            }

            if (!$this->isAppointmentAccessible($appointmentEntity)) {
                throw new AppointmentNotYetAccessibleException();
            }
        }

        if (!$this->isUserAllowed($appointmentEntity, $salesChannelContext)) {
            throw new AppointmentForbiddenException();
        }
    }

    private function isUserAllowed(AppointmentEntity $appointmentEntity, SalesChannelContext $salesChannelContext): bool
    {
        switch ($appointmentEntity->getAttendeeRestrictionType()) {
            case AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_CUSTOMER:
                return $this->isAllowedCustomer($appointmentEntity, $salesChannelContext);
            case AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_RULES:
                return $this->isAllowedByRule($appointmentEntity, $salesChannelContext);
            case AppointmentDefinition::ATTENDEE_RESTRICTION_TYPE_OPEN:
                return true;
        }

        throw new AppointmentAuthModeNotSetException();
    }

    private function isAllowedCustomer(AppointmentEntity $appointmentEntity, SalesChannelContext $salesChannelContext): bool
    {
        $customer = $salesChannelContext->getCustomer();
        if (!$customer) {
            return false;
        }

        $customerId = $customer->getId();
        $customerEmail = $customer->getEmail();
        $attendees = $appointmentEntity->getAttendees();

        $attendee = $attendees->filter(function (AttendeeEntity $attendee) use ($customerId, $customerEmail) {
            return $attendee->getCustomerId() === $customerId || $attendee->getAttendeeEmail() === $customerEmail;
        })->first();

        if (!$attendee) {
            return false;
        }

        if (!$attendee->getCustomerId()) { // update attendee's customerId by customer email
            $this->attendeeRepository->update([
                [
                    'id' => $attendee->getId(),
                    'customerId' => $customerId,
                ],
            ], $salesChannelContext->getContext());
        }

        return true;
    }

    private function isAllowedByRule(AppointmentEntity $appointmentEntity, SalesChannelContext $salesChannelContext): bool
    {
        if (empty($appointmentEntity->getAttendeeRuleIds())) {
            throw new AppointmentAuthRuleNotSetException();
        }

        foreach ($appointmentEntity->getAttendeeRuleIds() as $ruleId) {
            if (\in_array($ruleId, $salesChannelContext->getRuleIds())) {
                // @codeCoverageIgnoreStart
                return true;
                // @codeCoverageIgnoreEnd
            }
        }

        return false;
    }

    private function isOutdated(AppointmentEntity $appointmentEntity): bool
    {
        if (!$appointmentEntity->getAccessibleTo()) {
            return false;
        }

        return new \DateTime() > $appointmentEntity->getAccessibleTo();
    }

    private function isAppointmentAccessible(AppointmentEntity $appointmentEntity): bool
    {
        if (!$appointmentEntity->getAccessibleFrom()) {
            // @codeCoverageIgnoreStart
            return true;
            // @codeCoverageIgnoreEnd
        }

        return !(new \DateTime() < $appointmentEntity->getAccessibleFrom());
    }
}
