<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Api\Context\AdminSalesChannelApiSource;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\User\UserCollection;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;

class ContextDataHelper
{
    /**
     * @param EntityRepository<UserCollection> $userRepository
     */
    public function __construct(
        private readonly AttendeeService $attendeeService,
        private readonly EntityRepository $userRepository,
        private readonly GuidedShoppingRequestContextResolver $contextResolver
    ) {
    }

    public function getAttendeeNameFromContext(SalesChannelContext $salesChannelContext): ?string
    {
        if ($salesChannelContext->getContext()->getSource() instanceof AdminSalesChannelApiSource) {
            /** @var AdminSalesChannelApiSource $adminScSource */
            $adminScSource = $salesChannelContext->getContext()->getSource();
            /** @var AdminApiSource $adminSource */
            $adminSource = $adminScSource->getOriginalContext()->getSource();

            if (!$adminSource->getUserId()) {
                return null;
            }

            $user = $this->userRepository->search(new Criteria([$adminSource->getUserId()]), $salesChannelContext->getContext())->first();
            if ($user instanceof UserEntity) {
                return $user->getFirstName() . ' ' . $user->getLastName();
            }
        }

        if ($salesChannelContext->getCustomer() instanceof CustomerEntity) {
            $customer = $salesChannelContext->getCustomer();
            return $customer->getFirstName() . ' ' . $customer->getLastName();
        }

        return null;
    }

    public function getCustomerIdFromContext(SalesChannelContext $salesChannelContext): ?string
    {
        if ($salesChannelContext->getCustomer() instanceof CustomerEntity) {
            return $salesChannelContext->getCustomer()->getId();
        }

        return null;
    }

    public function getUserIdFromContext(SalesChannelContext $salesChannelContext): ?string
    {
        if ($salesChannelContext->getContext()->getSource() instanceof AdminSalesChannelApiSource) {
            /** @var AdminSalesChannelApiSource $adminScSource */
            $adminScSource = $salesChannelContext->getContext()->getSource();
            /** @var AdminApiSource $adminSource */
            $adminSource = $adminScSource->getOriginalContext()->getSource();

            return $adminSource->getUserId();
        }

        return null;
    }

    public function getAttendeeFromContext(SalesChannelContext $salesChannelContext): ?AttendeeEntity
    {
        try {
            return $this->contextResolver->loadAttendee($salesChannelContext);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function getAttendeeFromCredentials(SalesChannelContext $salesChannelContext, string $appointmentId, string $attendeeType): ?AttendeeEntity
    {
        if ($attendeeType === AttendeeDefinition::TYPE_CLIENT) {
            $customerId = $this->getCustomerIdFromContext($salesChannelContext);
            /**
             *  if you are logged in and not already joined
             *  we will search a prepared attendee which admin has added to a customer restricted appointment
             */
            if ($customerId) {
                return $this->attendeeService->findAttendeeByCustomer($customerId, $appointmentId, $salesChannelContext->getContext());
            }
        }

        if ($attendeeType === AttendeeDefinition::TYPE_GUIDE) {
            $userId = $this->getUserIdFromContext($salesChannelContext);
            if ($userId) {
                return $this->attendeeService->findAttendeeByUser($userId, $appointmentId, $salesChannelContext->getContext());
            }
        }

        return null;
    }
}
