<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Content\Product\Aggregate\ProductVisibility\ProductVisibilityDefinition;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\ProductAvailableFilter;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionCollection;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionStruct;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeProductCollectionAliasNotFoundException;

class AttendeeProductCollectionService
{
    public const ALIAS_LIKED = 'liked';
    public const ALIAS_DISLIKED = 'disliked';
    public const ALIAS = [
        self::ALIAS_DISLIKED,
        self::ALIAS_LIKED,
    ];

    /**
     * @param EntityRepository<AttendeeProductCollectionCollection> $attendeeProductCollectionRepository
     * @param EntityRepository<ProductCollection> $productRepository
     */
    public function __construct(
        private readonly EntityRepository $attendeeProductCollectionRepository,
        private readonly EntityRepository $productRepository
    ) {
    }

    public function remove(
        string $alias,
        string $attendeeId,
        string $productId,
        Context $context
    ): void
    {
        $toDelete = $this->getListEntry($attendeeId, $productId, $context, [$alias]);
        if ($toDelete) {
            $this->attendeeProductCollectionRepository->delete([['id' => $toDelete->getId()]], $context);
        }
    }

    public function getProducts(
        ?string $alias,
        string $attendeeId,
        Context $context,
        string $salesChannelId
    ): AttendeeProductCollectionStruct
    {
        $criteria = new Criteria();
        $collection = [];

        if ($alias) {
            $this->validateCollectionAlias($alias);
            $criteria->addFilter(new EqualsFilter('alias', $alias));
            $collection[$alias] = [];
        } else {
            foreach (self::ALIAS as $_alias) {
                $collection[$_alias] = [];
            }
        }

        $criteria->addFilter(
            new EqualsFilter('attendeeId', $attendeeId)
        );

        $criteria->getAssociation('product')->addFilter(
            new ProductAvailableFilter($salesChannelId, ProductVisibilityDefinition::VISIBILITY_ALL)
        );

        $searchResult = $this->attendeeProductCollectionRepository->search($criteria, $context);

        /** @var AttendeeProductCollectionEntity $element */
        foreach ($searchResult->getElements() as $element) {
            $collection[$element->getAlias()][] = $element->getProductId();
        }

        return new AttendeeProductCollectionStruct($collection);
    }

    public function addProduct(
        string $alias,
        string $attendeeId,
        string $productId,
        Context $context
    ): void
    {
        $this->validateCollectionAlias($alias);

        $productIds = $this->productRepository->searchIds(new Criteria([$productId]), $context);
        if (!$productIds->getTotal()) {
            throw new EntityNotFoundException(ProductEntity::class, $productId);
        }

        $data = $this->loadExistingData($attendeeId, $productId, $context);
        $data['alias'] = $alias;
        $data['productVersionId'] = Defaults::LIVE_VERSION;

        $this->attendeeProductCollectionRepository->upsert([$data], $context);
    }

    private function validateCollectionAlias(string $alias): void
    {
        if (!\in_array($alias, self::ALIAS)) {
            throw new AttendeeProductCollectionAliasNotFoundException();
        }
    }

    /**
     * @param array<string>|null $aliases
     */
    private function getListEntry(
        string $attendeeId,
        string $productId,
        Context $context,
        ?array $aliases = []
    ): ?AttendeeProductCollectionEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('attendeeId', $attendeeId));
        $criteria->addFilter(new EqualsFilter('productId', $productId));
        if (\is_array($aliases) && \count($aliases)) {
            $criteria->addFilter(new EqualsAnyFilter('alias', $aliases));
        }

        /** @var AttendeeProductCollectionEntity|null $attendeeProductCollection */
        $attendeeProductCollection = $this->attendeeProductCollectionRepository->search($criteria, $context)->first();

        return $attendeeProductCollection;
    }

    /**
     * @return array<string>
     */
    private function loadExistingData(string $attendeeId, string $productId, Context $context): array
    {
        $entry = $this->getListEntry($attendeeId, $productId, $context, [self::ALIAS_DISLIKED, self::ALIAS_LIKED]);
        if ($entry) {
            $data['id'] = $entry->getId();
        } else {
            $data['productId'] = $productId;
            $data['attendeeId'] = $attendeeId;
        }

        return $data;
    }
}
