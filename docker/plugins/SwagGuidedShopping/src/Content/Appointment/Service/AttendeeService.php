<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Service;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Event\BeforeAttendeeCreatedEvent;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeCouldNotBeCreatedException;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AttendeeService
{
    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly EntityRepository $attendeeRepository,
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory,
        private readonly EventDispatcherInterface $eventDispatcher
    ) {
    }

    /**
     * @param array<string, mixed> $attendeeData
     */
    public function updateAttendee(string $attendeeId, array $attendeeData, Context $context): void
    {
        $attendeeData['id'] = $attendeeId;
        $this->attendeeRepository->upsert([$attendeeData], $context);

        if (!(
            isset($attendeeData['attendeeName'])
            || isset($attendeeData['videoUserId'])
            || isset($attendeeData['guideCartPermissionsGranted'])
        )) {
            return;
        }

        /** @var AttendeeEntity|null $attendee */
        $attendee = $this->attendeeRepository->search(new Criteria([$attendeeId]), $context)->first();

        if (!$attendee instanceof AttendeeEntity) {
            throw new AttendeeNotFoundException($attendeeId);
        }

        $presentationStateService = $this->presentationStateServiceFactory->build($attendee->getAppointmentId(), $context);
        $stateForGuides = $presentationStateService->getStateForGuides();
        if (isset($attendeeData['attendeeName'])) {
            if ($attendee->isGuide()) {
                $stateForGuides->setGuideName($attendeeId, $attendeeData['attendeeName']);
            } else {
                $stateForGuides->setClientName($attendeeId, $attendeeData['attendeeName']);
            }
        }

        if (isset($attendeeData['videoUserId'])) {
            if ($attendee->isGuide()) {
                $stateForGuides->setGuideVideoUserId($attendeeId, $attendeeData['videoUserId']);
            } else {
                $stateForGuides->setClientVideoUserId($attendeeId, $attendeeData['videoUserId']);
            }
        }

        if (isset($attendeeData['guideCartPermissionsGranted'])) {
            $stateForGuides->setGuideCartPermissionsGranted($attendeeId, $attendeeData['guideCartPermissionsGranted']);
        }

        $stateForMe = $presentationStateService->getStateForMe();
        if (isset($attendeeData['attendeeSubmittedAt'])) {
            $stateForMe->setAttendee($attendee);
        }

        $presentationStateService->publishStateForGuides();
        $stateForClients = $presentationStateService->getStateForClients();
        $stateForClients->storeParticipants($stateForGuides);
        $presentationStateService->publishStateForClients();
        $presentationStateService->publishStateForMe();
    }

    public function findAttendeeByCustomer(string $customerId, string $appointmentId, Context $context): ?AttendeeEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('customerId', $customerId));
        $criteria->addFilter(new EqualsFilter('appointmentId', $appointmentId));
        $criteria->addFilter(new EqualsFilter('type', AttendeeDefinition::TYPE_CLIENT));

        /** @var AttendeeEntity|null $attendee */
        $attendee = $this->attendeeRepository->search($criteria, $context)->first();

        return $attendee;
    }

    public function findAttendeeByUser(string $userId, string $appointmentId, Context $context): ?AttendeeEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('userId', $userId));
        $criteria->addFilter(new EqualsFilter('appointmentId', $appointmentId));
        $criteria->addFilter(new EqualsFilter('type', AttendeeDefinition::TYPE_GUIDE));
        $criteria->addAssociation('user');

        /** @var AttendeeEntity|null $attendee */
        $attendee = $this->attendeeRepository->search($criteria, $context)->first();

        return $attendee;
    }

    public function createAttendee(
        string $appointmentId,
        SalesChannelContext $context,
        string $attendeeType,
        ?string $customerId,
        ?string $userId,
        ?string $attendeeName
    ): AttendeeEntity
    {
        $attendeeId = Uuid::randomHex();

        /** @var array<string, mixed> $attendeeData */
        $attendeeData = [
            'id' => $attendeeId,
            'appointmentId' => $appointmentId,
            'type' => $attendeeType,
            'attendeeName' => $attendeeName,
            'lastActive' => new \DateTime(),
        ];

        if ($attendeeType === AttendeeDefinition::TYPE_GUIDE) {
            $attendeeData['userId'] = $userId;
        } elseif ($attendeeType === AttendeeDefinition::TYPE_CLIENT) {
            $attendeeData['customerId'] = $customerId;
        }

        /** @var BeforeAttendeeCreatedEvent $event */
        $event = $this->eventDispatcher->dispatch(new BeforeAttendeeCreatedEvent($attendeeData, $context));
        $attendeeData = $event->getAttendeeData();

        $this->attendeeRepository->create([$attendeeData], $context->getContext());

        $attendee = $this->attendeeRepository->search(new Criteria([$attendeeId]), $context->getContext())->first();

        if ($attendee instanceof AttendeeEntity) {
            return $attendee;
        }

        throw new AttendeeCouldNotBeCreatedException();
    }

    /**
     * @param array<string> $attendeeEmails
     */
    public function updateInvitationStatus(
        array $attendeeEmails,
        string $appointmentId,
        string $invitationStatus,
        Context $context
    ): bool
    {
        if (!\count($attendeeEmails)) {
            return false;
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', $appointmentId));
        $criteria->addFilter(new EqualsAnyFilter('attendeeEmail', $attendeeEmails));

        $result = $this->attendeeRepository->searchIds($criteria, $context);
        if (!$result->getTotal()) return false;

        $updateData = [];
        foreach ($result->getIds() as $id) {
            $updateData[] = [
                'id' => $id,
                'invitationStatus' => $invitationStatus,
            ];
        }

        try {
            $this->attendeeRepository->update($updateData, $context);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
