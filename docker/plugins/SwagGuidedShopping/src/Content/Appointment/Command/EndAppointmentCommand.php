<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Command;

use SwagGuidedShopping\Content\Appointment\ScheduledTask\EndAppointmentTaskHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EndAppointmentCommand extends Command
{
    protected static $defaultName = 'guidedshopping:end-outdated-appointments';

    public function __construct(
        private readonly EndAppointmentTaskHandler $endAppointmentTaskHandler
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Closes all outdated appointments which were not ended by user');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->endAppointmentTaskHandler->run();
        return 0;
    }
}
