<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment;

use Shopware\Core\Content\Rule\RuleEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainEntity;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;

class AppointmentEntity extends Entity
{
    use EntityIdTrait;
    use EntityCustomFieldsTrait;

    protected ?string $name;

    protected bool $active;

    protected ?\DateTimeInterface $accessibleFrom;

    protected ?\DateTimeInterface $accessibleTo;

    protected ?string $guideUserId;

    protected ?UserEntity $guideUser = null;

    protected ?string $salesChannelDomainId;

    protected ?SalesChannelDomainEntity $salesChannelDomain;

    protected ?string $presentationPath;

    protected string $mode;

    protected bool $isPreview = false;

    protected string $videoAudioSettings = AppointmentDefinition::NONE_VIDEO_AUDIO;

    protected string $presentationId;

    protected ?PresentationEntity $presentation = null;

    protected AttendeeCollection $attendees;

    protected ?VideoChatEntity $videoChat;

    /**
     * @var array<string>|null
     */
    protected ?array $attendeeRuleIds;

    protected ?RuleEntity $attendeeRule;

    protected ?string $attendeeRestrictionType;

    protected ?\DateTimeInterface $startedAt = null;

    protected ?\DateTimeInterface $endedAt = null;

    protected bool $default = false;

    protected ?string $guidedShoppingPresentationVersionId = null;

    protected ?string $createdById;

    protected ?UserEntity $createdBy;

    protected ?string $updatedById;

    protected ?UserEntity $updatedBy;

    protected ?string $message = null;

    public function __construct()
    {
        $this->attendees = new AttendeeCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getAccessibleFrom(): ?\DateTimeInterface
    {
        return $this->accessibleFrom;
    }

    public function setAccessibleFrom(?\DateTimeInterface $accessibleFrom): void
    {
        $this->accessibleFrom = $accessibleFrom;
    }

    public function getAccessibleTo(): ?\DateTimeInterface
    {
        return $this->accessibleTo;
    }

    public function setAccessibleTo(?\DateTimeInterface $accessibleTo): void
    {
        $this->accessibleTo = $accessibleTo;
    }

    public function getGuideUserId(): ?string
    {
        return $this->guideUserId;
    }

    public function setGuideUserId(?string $guideUserId): void
    {
        $this->guideUserId = $guideUserId;
    }

    public function getGuideUser(): ?UserEntity
    {
        return $this->guideUser;
    }

    public function setGuideUser(?UserEntity $guideUser): void
    {
        $this->guideUser = $guideUser;
    }

    public function getSalesChannelDomainId(): ?string
    {
        return $this->salesChannelDomainId;
    }

    public function setSalesChannelDomainId(?string $salesChannelDomainId): void
    {
        $this->salesChannelDomainId = $salesChannelDomainId;
    }

    public function getSalesChannelDomain(): ?SalesChannelDomainEntity
    {
        return $this->salesChannelDomain;
    }

    public function setSalesChannelDomain(?SalesChannelDomainEntity $salesChannelDomain): void
    {
        $this->salesChannelDomain = $salesChannelDomain;
    }

    public function getPresentationPath(): ?string
    {
        return $this->presentationPath;
    }

    public function setPresentationPath(?string $presentationPath): void
    {
        $this->presentationPath = $presentationPath;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function setMode(string $mode): void
    {
        $this->mode = $mode;
    }

    public function isPreview(): bool
    {
        return $this->isPreview;
    }

    public function setPreview(bool $isPreview): void
    {
        $this->isPreview = $isPreview;
    }

    public function getVideoAudioSettings(): string
    {
        return $this->videoAudioSettings;
    }

    public function setVideoAudioSettings(string $videoAudioSettings): void
    {
        $this->videoAudioSettings = $videoAudioSettings;
    }

    public function getPresentationId(): string
    {
        return $this->presentationId;
    }

    public function setPresentationId(string $presentationId): void
    {
        $this->presentationId = $presentationId;
    }

    public function getPresentation(): ?PresentationEntity
    {
        return $this->presentation;
    }

    public function setPresentation(PresentationEntity $presentation): void
    {
        $this->presentation = $presentation;
    }

    public function getAttendees(): AttendeeCollection
    {
        return $this->attendees;
    }

    public function setAttendees(AttendeeCollection $attendees): void
    {
        $this->attendees = $attendees;
    }

    public function getVideoChat(): ?VideoChatEntity
    {
        return $this->videoChat;
    }

    public function setVideoChat(?VideoChatEntity $videoChat): void
    {
        $this->videoChat = $videoChat;
    }

    /**
     * @return array<string>|null
     */
    public function getAttendeeRuleIds(): ?array
    {
        return $this->attendeeRuleIds;
    }

    /**
     * @param array<string>|null $attendeeRuleIds
     */
    public function setAttendeeRuleIds(?array $attendeeRuleIds): void
    {
        $this->attendeeRuleIds = $attendeeRuleIds;
    }

    public function getAttendeeRule(): ?RuleEntity
    {
        return $this->attendeeRule;
    }

    public function setAttendeeRule(?RuleEntity $attendeeRule): void
    {
        $this->attendeeRule = $attendeeRule;
    }

    public function getAttendeeRestrictionType(): ?string
    {
        return $this->attendeeRestrictionType;
    }

    public function setAttendeeRestrictionType(?string $attendeeRestrictionType): void
    {
        $this->attendeeRestrictionType = $attendeeRestrictionType;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): void
    {
        $this->startedAt = $startedAt;
    }

    public function getEndedAt(): ?\DateTimeInterface
    {
        return $this->endedAt;
    }

    public function setEndedAt(?\DateTimeInterface $endedAt): void
    {
        $this->endedAt = $endedAt;
    }

    public function isDefault(): bool
    {
        return $this->default;
    }

    public function setDefault(bool $default): void
    {
        $this->default = $default;
    }

    public function getPresentationVersionId(): ?string
    {
        return $this->guidedShoppingPresentationVersionId;
    }

    public function setPresentationVersionId(?string $presentationVersionId): void
    {
        $this->guidedShoppingPresentationVersionId = $presentationVersionId;
    }

    public function getCreatedById(): ?string
    {
        return $this->createdById;
    }

    public function setCreatedById(?string $createdById): void
    {
        $this->createdById = $createdById;
    }

    public function getCreatedBy(): ?UserEntity
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserEntity $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedById(): ?string
    {
        return $this->updatedById;
    }

    public function setUpdatedById(?string $updatedById): void
    {
        $this->updatedById = $updatedById;
    }

    public function getUpdatedBy(): ?UserEntity
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?UserEntity $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    public function getPresentationUrl(): ?string
    {
        return $this->salesChannelDomain && $this->presentationPath
            ? "{$this->salesChannelDomain->getUrl()}/presentation/{$this->presentationPath}"
            : null;
    }

    public function getGuideName(): string
    {
        return $this->guideUser
            ? "{$this->guideUser->getFirstName()} {$this->guideUser->getLastName()}"
            : 'The guide';
    }


    /**
     * @return array<string>|null
     */
    public function getGuideInfo(): ?array
    {
        if ($this->guideUser) {
            return [
                'email' => $this->guideUser->getEmail(),
                'name' => $this->getGuideName()
            ];
        } else {
            return null;
        }
    }
}
