<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedByField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\AllowHtml;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\SearchRanking;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedByField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\SalesChannel\Aggregate\SalesChannelDomain\SalesChannelDomainDefinition;
use Shopware\Core\System\User\UserDefinition;
use SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation\AppointmentTranslationDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Presentation\PresentationDefinition;
use SwagGuidedShopping\Content\VideoChat\VideoChatDefinition;
use SwagGuidedShopping\Struct\ConfigKeys;

class AppointmentDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_appointment';
    public const MODE_SELF = 'self';
    public const MODE_GUIDED = 'guided';
    public const NONE_VIDEO_AUDIO = 'none';
    public const BOTH_VIDEO_AUDIO = 'both';
    public const AUDIO_ONLY = 'audio_only';
    public const ATTENDEE_RESTRICTION_TYPE_OPEN = 'open';
    public const ATTENDEE_RESTRICTION_TYPE_CUSTOMER = 'customer';
    public const ATTENDEE_RESTRICTION_TYPE_RULES = 'rules';
    public const INVITE_ACTION = 'invite';
    public const INVITE_ALL_ACTION = 'inviteAll';
    public const CANCEL_ACTION = 'cancel';
    public const INVITATION_ACTIONS = [
        self::INVITE_ACTION,
        self::INVITE_ALL_ACTION,
        self::CANCEL_ACTION
    ];

    public const INVITATION_MAIL_TEMPLATE_TYPE = [
        self::INVITE_ACTION => ConfigKeys::SEND_INVITATION_MAIL_TEMPLATE,
        self::INVITE_ALL_ACTION => ConfigKeys::SEND_INVITATION_MAIL_TEMPLATE,
        self::CANCEL_ACTION => ConfigKeys::CANCEL_INVITATION_MAIL_TEMPLATE
    ];

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return AppointmentEntity::class;
    }

    public function getCollectionClass(): string
    {
        return AppointmentCollection::class;
    }

    /**
     * @return array<string, mixed>
     */
    public function getDefaults(): array
    {
        return [
            'active' => true,
            'videoAudioSettings' => static::NONE_VIDEO_AUDIO,
            'mode' => self::MODE_GUIDED,
            'attendeeRestrictionType' => self::ATTENDEE_RESTRICTION_TYPE_OPEN,
        ];
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new FkField('guided_shopping_presentation_id', 'presentationId', PresentationDefinition::class))->addFlags(new ApiAware(), new Required()),
                (new ReferenceVersionField(PresentationDefinition::class))->addFlags(new ApiAware()),

                (new BoolField('active', 'active'))->addFlags(new ApiAware()),
                (new FkField('sales_channel_domain_id', 'salesChannelDomainId', SalesChannelDomainDefinition::class))->addFlags(new ApiAware(), new Required()),
                (new StringField('presentation_path', 'presentationPath'))->addFlags(new Required(), new SearchRanking(SearchRanking::ASSOCIATION_SEARCH_RANKING)),
                (new DateTimeField('accessible_from', 'accessibleFrom'))->addFlags(new ApiAware()),
                (new DateTimeField('accessible_to', 'accessibleTo'))->addFlags(new ApiAware()),
                (new JsonField('attendee_rule_ids', 'attendeeRuleIds'))->addFlags(new ApiAware()),
                (new FkField('guide_user_id', 'guideUserId', UserDefinition::class))->addFlags(new ApiAware()),
                (new StringField('mode', 'mode'))->addFlags(new Required()),
                (new TranslatedField('message'))->addFlags(new AllowHtml(false)),
                (new StringField('attendee_restriction_type', 'attendeeRestrictionType')),
                (new BoolField('is_preview', 'isPreview'))->addFlags(new ApiAware()),
                (new StringField('video_audio_settings', 'videoAudioSettings'))->addFlags(new ApiAware()),
                (new TranslatedField('name'))->addFlags(new ApiAware(), new Required(), new SearchRanking(SearchRanking::HIGH_SEARCH_RANKING)),
                (new DateTimeField('started_at', 'startedAt'))->addFlags(new ApiAware()),
                (new DateTimeField('ended_at', 'endedAt'))->addFlags(new ApiAware()),
                (new BoolField('default', 'default'))->addFlags(new ApiAware()),
                (new CustomFields())->addFlags(new ApiAware()),
                (new CreatedByField([Context::CRUD_API_SCOPE, Context::SYSTEM_SCOPE]))->addFlags(new ApiAware(), new Required()),
                (new UpdatedByField([Context::CRUD_API_SCOPE, Context::SYSTEM_SCOPE]))->addFlags(new ApiAware()),
                (new ManyToOneAssociationField('presentation', 'guided_shopping_presentation_id', PresentationDefinition::class, 'id', false))->addFlags(new SearchRanking(SearchRanking::ASSOCIATION_SEARCH_RANKING)),
                (new OneToOneAssociationField('videoChat', 'id', 'appointment_id', VideoChatDefinition::class, true))->addFlags(new ApiAware()), //don't add a cascade delete, this has to be removed first in the remote video tool, therefore is a guided_shopping_appointment.deleted Listener implemented
                (new ManyToOneAssociationField('guideUser', 'guide_user_id', UserDefinition::class, 'id', true))->addFlags(new SearchRanking(SearchRanking::MIDDLE_SEARCH_RANKING)),
                (new ManyToOneAssociationField('salesChannelDomain', 'sales_channel_domain_id', SalesChannelDomainDefinition::class, 'id', true)),
                (new OneToManyAssociationField('attendees', AttendeeDefinition::class, 'appointment_id'))->addFlags(new CascadeDelete()),
                (new ManyToOneAssociationField('createdBy', 'created_by_id', UserDefinition::class, 'id', true))->addFlags(new SearchRanking(SearchRanking::HIGH_SEARCH_RANKING)),
                (new ManyToOneAssociationField('updatedBy', 'updated_by_id', UserDefinition::class, 'id', true)),
                (new TranslationsAssociationField(
                    AppointmentTranslationDefinition::class,
                    'guided_shopping_appointment_id'
                ))->addFlags(new ApiAware(), new Required())
            ]
        );
    }
}
