<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Attendee;

use Shopware\Core\Checkout\Customer\CustomerEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionCollection;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;

class AttendeeEntity extends Entity
{
    use EntityIdTrait;

    protected string $appointmentId = '';

    protected AppointmentEntity $appointment;

    protected ?string $customerId = null;

    protected ?CustomerEntity $customer = null;

    protected ?string $userId = null;

    protected ?string $videoUserId = null;

    protected ?UserEntity $user = null;

    protected ?\DateTimeInterface $joinedAt = null;

    protected ?\DateTimeInterface $lastActive = null;

    protected ?InteractionCollection $interactions = null;

    protected ?AttendeeProductCollectionCollection $productCollections = null;

    protected string $type;

    protected ?string $attendeeName = null;

    protected ?string $attendeeEmail = null;

    protected bool $guideCartPermissionsGranted = false;

    protected ?\DateTimeInterface $attendeeSubmittedAt = null;

    protected ?string $invitationStatus = null;

    public function __constructor(): void
    {
        $this->interactions = new InteractionCollection();
        $this->productCollections = new AttendeeProductCollectionCollection();
    }

    public function getAppointmentId(): string
    {
        return $this->appointmentId;
    }

    public function setAppointmentId(string $appointmentId): void
    {
        $this->appointmentId = $appointmentId;
    }

    public function getCustomerId(): ?string
    {
        return $this->customerId;
    }

    public function setCustomerId(?string $customerId): void
    {
        $this->customerId = $customerId;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(?string $userId): void
    {
        $this->userId = $userId;
    }

    public function getUser(): ?UserEntity
    {
        return $this->user;
    }

    public function setUser(?UserEntity $user): void
    {
        $this->user = $user;
    }

    public function getGuideName(): string
    {
        if ($this->user instanceof UserEntity) {
            return $this->user->getFirstName() . ' ' . $this->user->getLastName();
        }

        throw new \Exception('No guide set');
    }

    public function getAppointment(): AppointmentEntity
    {
        return $this->appointment;
    }

    public function setAppointment(AppointmentEntity $appointment): void
    {
        $this->appointment = $appointment;
    }

    public function getCustomer(): ?CustomerEntity
    {
        return $this->customer;
    }

    public function setCustomer(?CustomerEntity $customer): void
    {
        $this->customer = $customer;
    }

    public function getInteractions(): InteractionCollection
    {
        return $this->interactions ?: new InteractionCollection();
    }

    public function setInteractions(InteractionCollection $interactions): void
    {
        $this->interactions = $interactions;
    }

    public function getProductCollections(): AttendeeProductCollectionCollection
    {
        return $this->productCollections ?: new AttendeeProductCollectionCollection();
    }

    public function setProductCollections(AttendeeProductCollectionCollection $productCollections): void
    {
        $this->productCollections = $productCollections;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function isGuide(): bool
    {
        return $this->getType() === AttendeeDefinition::TYPE_GUIDE;
    }

    public function isClient(): bool
    {
        return $this->getType() === AttendeeDefinition::TYPE_CLIENT;
    }

    public function getJoinedAt(): ?\DateTimeInterface
    {
        return $this->joinedAt;
    }

    public function setJoinedAt(?\DateTimeInterface $joinedAt): void
    {
        $this->joinedAt = $joinedAt;
    }

    public function getLastActive(): ?\DateTimeInterface
    {
        return $this->lastActive;
    }

    public function setLastActive(?\DateTimeInterface $lastActive): void
    {
        $this->lastActive = $lastActive;
    }

    public function getAttendeeName(): ?string
    {
        return $this->attendeeName;
    }

    public function setAttendeeName(?string $attendeeName): void
    {
        $this->attendeeName = $attendeeName;
    }

    public function getAttendeeEmail(): ?string
    {
        return $this->attendeeEmail;
    }

    public function setAttendeeEmail(?string $attendeeEmail): void
    {
        $this->attendeeEmail = $attendeeEmail;
    }

    public function isGuideCartPermissionsGranted(): bool
    {
        return $this->guideCartPermissionsGranted;
    }

    public function setGuideCartPermissionsGranted(bool $guideCartPermissionsGranted): void
    {
        $this->guideCartPermissionsGranted = $guideCartPermissionsGranted;
    }

    public function getAttendeeSubmittedAt(): ?\DateTimeInterface
    {
        return $this->attendeeSubmittedAt;
    }

    public function setAttendeeSubmittedAt(?\DateTimeInterface $attendeeSubmittedAt): void
    {
        $this->attendeeSubmittedAt = $attendeeSubmittedAt;
    }

    public function getVideoUserId(): ?string
    {
        return $this->videoUserId;
    }

    public function setVideoUserId(?string $videoUserId): void
    {
        $this->videoUserId = $videoUserId;
    }

    public function getInvitationStatus(): ?string
    {
        return $this->invitationStatus;
    }

    public function setInvitationStatus(?string $invitationStatus): void
    {
        $this->invitationStatus = $invitationStatus;
    }
}
