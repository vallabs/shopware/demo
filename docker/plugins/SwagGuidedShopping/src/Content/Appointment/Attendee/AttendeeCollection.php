<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Attendee;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<AttendeeEntity>
 */
class AttendeeCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AttendeeEntity::class;
    }

    /**
     * @return array<string, array<string, mixed>>
     *
     * /**
     * Example data format
     * recipients[<string>'test@gmail.com'] = [
     *    'name' => <string>'test',
     *    'invitationStatus' => <string|null>'pending'
     *    'timeZone' => <string|null>'Europe/Berlin'
     * ]
     */
    public function getAppointmentRecipients(): array
    {
        $recipients = [];

        /** @var AttendeeEntity $attendee */
        foreach ($this->getElements() as $attendee) {
            $customer = $attendee->getCustomer();

            if ($customer) { // if attendee is existed in Shopware system
                $name = "{$customer->getFirstName()} {$customer->getLastName()}";
                $countryISO = $customer->getDefaultShippingAddress()?->getCountry()?->getIso();
                $timeZones = $countryISO ? \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $countryISO) : [];

                $recipients[$customer->getEmail()] = [
                    'name' => $name,
                    'invitationStatus' => $attendee->getInvitationStatus(),
                    'timeZone' => \count($timeZones) ? $timeZones[0] : null,
                ];
            } else { // if attendee is not existed in Shopware system
                if ($attendee->getAttendeeEmail()) {
                    $attendeeEmail = $attendee->getAttendeeEmail();
                    $recipients[$attendeeEmail] = [
                        'name' => $attendee->getAttendeeName() ?: \explode('@', $attendeeEmail)[0],
                        'invitationStatus' => $attendee->getInvitationStatus(),
                        'timeZone' => null,
                    ];
                }
            }
        }

        return $recipients;
    }
}
