<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Attendee;

use Shopware\Core\Checkout\Customer\CustomerDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\User\UserDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionDefinition;
use SwagGuidedShopping\Content\Interaction\InteractionDefinition;

class AttendeeDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_appointment_attendee';
    public const TYPE_GUIDE = 'GUIDE';
    public const TYPE_CLIENT = 'CLIENT';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return AttendeeEntity::class;
    }

    public function getCollectionClass(): string
    {
        return AttendeeCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new FkField('appointment_id', 'appointmentId', AppointmentDefinition::class))->addFlags(new Required()),
                (new BoolField('guide_cart_permissions_granted', 'guideCartPermissionsGranted')),
                (new DateTimeField('attendee_submitted_at', 'attendeeSubmittedAt'))->addFlags(new ApiAware()),
                new FkField('customer_id', 'customerId', CustomerDefinition::class),
                new FkField('user_id', 'userId', UserDefinition::class),
                new StringField('video_user_id', 'videoUserId', 50),
                new StringField('attendee_name', 'attendeeName', 255),
                new StringField('attendee_email', 'attendeeEmail', 255),
                (new StringField('type', 'type', 10))->addFlags(new Required()),
                (new StringField('invitation_status', 'invitationStatus')),
                (new DateTimeField('joined_at', 'joinedAt'))->addFlags(new ApiAware()),
                (new DateTimeField('last_active', 'lastActive'))->addFlags(new ApiAware()),

                new ManyToOneAssociationField('appointment', 'appointment_id', AppointmentDefinition::class, 'id', true),
                new ManyToOneAssociationField('customer', 'customer_id', CustomerDefinition::class),
                new ManyToOneAssociationField('user', 'user_id', UserDefinition::class),
                (new OneToManyAssociationField('interactions', InteractionDefinition::class, 'attendee_id'))->addFlags(new CascadeDelete()),
                (new OneToManyAssociationField('productCollections', AttendeeProductCollectionDefinition::class, 'attendee_id'))->addFlags(new CascadeDelete()),
            ]
        );
    }
}
