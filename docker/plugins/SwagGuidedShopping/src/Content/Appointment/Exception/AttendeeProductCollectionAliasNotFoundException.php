<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

class AttendeeProductCollectionAliasNotFoundException extends \Exception
{
}
