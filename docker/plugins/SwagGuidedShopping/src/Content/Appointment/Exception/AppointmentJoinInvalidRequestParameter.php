<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class AppointmentJoinInvalidRequestParameter extends ShopwareHttpException
{
    public function __construct(ViolationList $violationsCollection)
    {
        $message = "There are the following parameter errors in the request body \n";
        $message .= $violationsCollection;
        parent::__construct($message);
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__APPOINTMENT_JOIN_INVALID_REQUEST_PATAMETER;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
