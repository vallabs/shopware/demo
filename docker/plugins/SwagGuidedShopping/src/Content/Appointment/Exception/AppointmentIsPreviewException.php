<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AppointmentIsPreviewException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('The preview appointment can not started.');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__APPOINTMENT_IS_PREVIEW;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_METHOD_NOT_ALLOWED;
    }
}
