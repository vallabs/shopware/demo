<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AppointmentPresentationIsNotSet extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('The appointment needs to have a presentation set.');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__APPOINTMENT_PRESENTATION_NOT_SET;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
