<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AppointmentInvitationMissingStartOrEndTimeException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('Invitation email can not be sent because of missing start/end time.');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::INVITATION_EMAIL_SENDING_TIME_ERROR;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
