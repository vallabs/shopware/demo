<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class GuideNotIdentifiedAsAdminException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('Guide can not be identified with admin user id');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__GUIDE_NOT_IDENTIFIED_BY_USER_ID;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_METHOD_NOT_ALLOWED;
    }
}
