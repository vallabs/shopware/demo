<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AttendeeTokenNotFoundException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('No token for attendee found');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__TOKEN_FOR_ATTENDEE_NOT_FOUND;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }
}
