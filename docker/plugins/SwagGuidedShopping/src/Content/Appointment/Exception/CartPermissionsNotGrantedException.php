<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class CartPermissionsNotGrantedException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('Guide permissions for cart not granted');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__GUIDE_CART_PERMISSIONS_NOT_GRANTED;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_METHOD_NOT_ALLOWED;
    }
}
