<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AppointmentNotYetAccessibleException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('Appointment not yet accessible');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__APPOINTMENT_NOT_YET_ACCESSIBLE;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_TEMPORARY_REDIRECT;
    }
}
