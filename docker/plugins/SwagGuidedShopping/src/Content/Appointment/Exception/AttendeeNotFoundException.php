<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AttendeeNotFoundException extends ShopwareHttpException
{
    public function __construct(string $attendeeId)
    {
        parent::__construct(
            \sprintf('Attendee with id %s not found', $attendeeId)
        );
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__ATTENDEE_NOT_FOUND;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }
}
