<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\AllowHtml;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\SearchRanking;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class AppointmentTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'guided_shopping_appointment_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return AppointmentTranslationCollection::class;
    }

    public function getEntityClass(): string
    {
        return AppointmentTranslationEntity::class;
    }

    protected function getParentDefinitionClass(): string
    {
        return AppointmentDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('name', 'name'))->addFlags(new ApiAware(), new Required(), new SearchRanking(SearchRanking::HIGH_SEARCH_RANKING)),
            (new LongTextField('message', 'message'))->addFlags(new AllowHtml(false)),
        ]);
    }
}
