<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<AppointmentTranslationEntity>
 */
class AppointmentTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AppointmentTranslationEntity::class;
    }
}
