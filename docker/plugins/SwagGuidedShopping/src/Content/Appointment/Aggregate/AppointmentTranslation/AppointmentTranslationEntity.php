<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;

class AppointmentTranslationEntity extends TranslationEntity
{
    protected string $appointmentId;

    protected ?string $name;

    protected ?string $message = null;

    public function getAppointmentId(): string
    {
        return $this->appointmentId;
    }

    public function setAppointmentId(string $appointmentId): void
    {
        $this->appointmentId = $appointmentId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

}
