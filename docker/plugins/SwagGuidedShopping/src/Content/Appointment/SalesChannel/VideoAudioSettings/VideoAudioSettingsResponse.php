<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\VideoAudioSettings;

use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\StoreApiResponse;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;

class VideoAudioSettingsResponse extends StoreApiResponse
{
    /**
     * @var ArrayStruct<string, mixed>
     */
    protected $object;

    public function __construct(
        ?AppointmentEntity $appointment
    ) {
        parent::__construct(new ArrayStruct([
            'videoAudioSettings' => $appointment?->getVideoAudioSettings()
        ], 'appointment_video_audio_settings'));
    }

    /**
     * @return ArrayStruct<string, mixed>
     */
    public function getResult(): ArrayStruct
    {
        return $this->object;
    }

    public function getVideoAudioSettings(): ?string
    {
        return $this->object->get('videoAudioSettings');
    }
}
