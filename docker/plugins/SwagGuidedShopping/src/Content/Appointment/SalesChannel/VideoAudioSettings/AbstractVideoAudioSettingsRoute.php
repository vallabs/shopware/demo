<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\VideoAudioSettings;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractVideoAudioSettingsRoute
{
    abstract public function getDecorated(): AbstractVideoAudioSettingsRoute;

    abstract public function getVideoAudioSettings(
        string $presentationPath,
        SalesChannelContext $context
    ): VideoAudioSettingsResponse;
}
