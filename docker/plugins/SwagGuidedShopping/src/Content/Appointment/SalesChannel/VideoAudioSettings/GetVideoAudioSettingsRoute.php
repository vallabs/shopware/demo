<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\VideoAudioSettings;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class GetVideoAudioSettingsRoute extends AbstractVideoAudioSettingsRoute
{
    public function __construct(
        private readonly AppointmentService $appointmentService,
    ) {
    }

    public function getDecorated(): AbstractVideoAudioSettingsRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/{presentationPath}/video-audio-settings', name: 'store-api.guided-shopping.appointment.video-audio-settings', methods: ['GET'])]
    public function getVideoAudioSettings(string $presentationPath, SalesChannelContext $context): VideoAudioSettingsResponse
    {
        $appointment = $this->appointmentService->findAppointmentByUrl($presentationPath, $context->getContext());

        return new VideoAudioSettingsResponse($appointment);
    }
}
