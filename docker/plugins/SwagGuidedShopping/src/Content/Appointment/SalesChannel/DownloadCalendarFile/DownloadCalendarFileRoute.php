<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\DownloadCalendarFile;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeInvitationInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Appointment\Traits\IcsTrait;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class DownloadCalendarFileRoute extends AbstractDownloadCalendarFileRoute
{
    use IcsTrait;

    public function __construct(
        private readonly DataValidator $dataValidator,
        private readonly AppointmentService $appointmentService
    ) {
    }

    public function getDecorated(): AbstractDownloadCalendarFileRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/{appointmentId}/download-ics', name: 'store-api.guided-shopping.appointment.download-ics', defaults: [], methods: ['POST'])]
    public function downloadIcs(string $appointmentId, Request $request, SalesChannelContext $salesChannelContext): Response
    {
        $this->validateRequestBody($request->request->all());
        $context = $salesChannelContext->getContext();

        /** @var string $token  */
        $token = $request->request->get('token', '');
        $decryptResult = EncryptHelper::decryptString($token);
        if(!$decryptResult['valid'] || !isset($decryptResult['email'])) {
            return new Response($decryptResult['message'] ?? 'Invalid token', 401);
        }

        $appointment = $this->appointmentService->getAppointment(
            $appointmentId,
            $context,
            ['guideUser', 'attendees.customer', 'salesChannelDomain.salesChannel']
        );

        if (!$appointment->getSalesChannelDomain()) {
            return new Response('Appointment sales channel domain is not set yet.', 400);
        }

        if ($appointment->getMode() !== AppointmentDefinition::MODE_GUIDED) {
            return new Response('Appointment is self mode.', 400);
        }

        $icsFile = $this->generateIcsFile($appointment);

        $appointmentName = $appointment->getTranslation('name') ?? $appointment->getName();

        $fileName = $appointmentName ?? 'dsr-appointment';

        return $this->createResponse("{$fileName}.ics", $icsFile['content']);
    }

    private function createResponse(string $filename, string $content): Response
    {
        $response = new Response($content);

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $filename,
            // only printable ascii
            \preg_replace('/[\x00-\x1F\x7F-\xFF]/', '_', $filename) ?? ''
        );

        $response->headers->set('Content-Type', 'text/calendar');
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * @param array<string, mixed> $jsonRequestPayload
     */
    private function validateRequestBody(array $jsonRequestPayload): void
    {
        $constraint = new Collection(
            [
                'token' => [new Type('string'), new Required(), new NotBlank()],
            ]
        );

        $violations = $this->dataValidator->validate($jsonRequestPayload, $constraint);
        if ($violations->count()) {
            throw new AttendeeInvitationInvalidRequestParameter($violations);
        }
    }
}
