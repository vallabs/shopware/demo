<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\DownloadCalendarFile;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractDownloadCalendarFileRoute
{
    abstract public function getDecorated(): AbstractDownloadCalendarFileRoute;

    abstract public function downloadIcs(string $appointmentId, Request $request, SalesChannelContext $salesChannelContext): Response;
}
