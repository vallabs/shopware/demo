<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\JoinAsClient;

use Shopware\Core\PlatformRequest;
use Shopware\Core\System\SalesChannel\StoreApiResponse;
use SwagGuidedShopping\Content\Appointment\Struct\AppointmentJoinStruct;

class AppointmentJoinAsClientResponse extends StoreApiResponse
{
    public function __construct(AppointmentJoinStruct $data)
    {
        parent::__construct($data);
        $this->headers->set(PlatformRequest::HEADER_CONTEXT_TOKEN, $data->getNewContextToken());
    }
}
