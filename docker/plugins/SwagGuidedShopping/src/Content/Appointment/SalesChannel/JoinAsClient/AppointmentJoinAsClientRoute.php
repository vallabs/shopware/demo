<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\JoinAsClient;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentNotFoundException;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentJoinInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Appointment\Service\ClientJoinAppointmentValidator;
use SwagGuidedShopping\Content\Appointment\Service\JoinAppointmentService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Collection;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class AppointmentJoinAsClientRoute extends AbstractAppointmentJoinAsClientRoute
{
    public function __construct(
        private readonly AppointmentService $appointmentService,
        private readonly ClientJoinAppointmentValidator $clientJoinAppointmentValidator,
        private readonly JoinAppointmentService $joinAppointmentService,
        private readonly DataValidator $dataValidator
    ) {
    }

    public function getDecorated(): AbstractAppointmentJoinAsClientRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/{presentationPath}/join-as-client', name: 'store-api.guided-shopping.appointment-join-as-client', requirements: ['presentationPath' => '.+'], defaults: ['join' => true], methods: ['POST'])]
    public function join(string $presentationPath, RequestDataBag $dataBag, Request $request, SalesChannelContext $context): AppointmentJoinAsClientResponse
    {
        $this->validateRequestBody($request->request->all());

        $appointment = $this->appointmentService->findAppointmentByUrl($presentationPath, $context->getContext());
        if (!$appointment) {
            throw new AppointmentNotFoundException();
        }

        $this->clientJoinAppointmentValidator->validate($appointment, $context);

        $fallbackAttendeeName = $request->get('attendeeName');

        $appointmentJoinStruct = $this->joinAppointmentService->joinMeetingAsClient(
            $appointment->getId(),
            $context,
            $appointment->getMode(),
            $fallbackAttendeeName
        );

        return new AppointmentJoinAsClientResponse($appointmentJoinStruct);
    }

    /**
     * @param array<string, mixed> $jsonRequestPayload
     */
    private function validateRequestBody(array $jsonRequestPayload): void
    {
        $constraint = new Collection(
            [
                'attendeeName' => [new Type('string')],
            ]
        );
        $constraint->allowMissingFields = true;
        $violations = $this->dataValidator->validate($jsonRequestPayload, $constraint);
        if ($violations->count()) {
            throw new AppointmentJoinInvalidRequestParameter($violations);
        }
    }
}
