<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\JoinAsClient;

use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractAppointmentJoinAsClientRoute
{
    abstract public function getDecorated(): AbstractAppointmentJoinAsClientRoute;

    abstract public function join(
        string $presentationPath,
        RequestDataBag $dataBag,
        Request $request,
        SalesChannelContext $context
    ): AppointmentJoinAsClientResponse;
}
