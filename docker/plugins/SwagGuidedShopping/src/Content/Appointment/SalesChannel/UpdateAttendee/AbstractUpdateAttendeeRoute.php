<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\UpdateAttendee;

use Shopware\Core\System\SalesChannel\NoContentResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractUpdateAttendeeRoute
{
    abstract public function getDecorated(): AbstractUpdateAttendeeRoute;

    abstract public function updateAttendee(
        Request $request,
        SalesChannelContext $context
    ): UpdateAttendeeResponse;
}
