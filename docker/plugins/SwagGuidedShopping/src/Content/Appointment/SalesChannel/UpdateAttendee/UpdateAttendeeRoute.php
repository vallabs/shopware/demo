<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\UpdateAttendee;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentJoinInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Type;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class UpdateAttendeeRoute extends AbstractUpdateAttendeeRoute
{
    public function __construct(
        private readonly AttendeeService $attendeeService,
        private readonly DataValidator $dataValidator
    ) {
    }

    public function getDecorated(): AbstractUpdateAttendeeRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/attendee', name: 'store-api.guided-shopping.appointment.update-attendee', defaults: ['attendee_required' => true], methods: ['PATCH'])]
    public function updateAttendee(Request $request, SalesChannelContext $context): UpdateAttendeeResponse
    {
        $this->validateRequestBody($request->request->all());

        /** @var array<string, mixed> $update */
        $update = [];
        /** @var string $attendeeName */
        $attendeeName = $request->request->get('attendeeName');
        if ($attendeeName) {
            $update['attendeeName'] = $attendeeName;
        }

        /** @var string $videoUserId */
        $videoUserId = $request->request->get('videoUserId');
        if ($videoUserId) {
            $update['videoUserId'] = $videoUserId;
        }

        /** @var bool $guideCartPermissionsGranted */
        $guideCartPermissionsGranted = $request->request->get('guideCartPermissionsGranted', false);
        if ($guideCartPermissionsGranted) {
            $update['guideCartPermissionsGranted'] = $guideCartPermissionsGranted;
        }

        /** @var bool $attendeeSubmitted */
        $attendeeSubmitted = $request->request->get('attendeeSubmitted', false);
        if ($attendeeSubmitted) {
            $update['attendeeSubmittedAt'] = new \DateTimeImmutable();
        }

        if (!\count($update)) {
            return new UpdateAttendeeResponse($update);
        }

        /** @var AttendeeEntity $attendee */
        $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);

        $this->attendeeService->updateAttendee($attendee->getId(), $update, $context->getContext());

        return new UpdateAttendeeResponse($update);
    }

    /**
     * @param array<string, mixed> $jsonRequestPayload
     */
    private function validateRequestBody(array $jsonRequestPayload): void
    {
        $constraint = new Collection(
            [
                'attendeeName' => [new Type('string')],
                'videoUserId' => [new Type('string')],
                'guideCartPermissionsGranted' => [new Type('bool')],
                'attendeeSubmitted' => [new Type('bool')],
            ]
        );
        $constraint->allowMissingFields = true;
        $violations = $this->dataValidator->validate($jsonRequestPayload, $constraint);
        if ($violations->count()) {
            throw new AppointmentJoinInvalidRequestParameter($violations);
        }
    }
}
