<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\UpdateAttendee;

use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class UpdateAttendeeResponse extends StoreApiResponse
{
    /**
     * @var ArrayStruct<string, mixed>
     */
    protected $object;

    /**
     * @param array<string, mixed> $updatedData
     */
    public function __construct(array $updatedData)
    {
        parent::__construct(new ArrayStruct([
            'data' => $updatedData,
        ], 'guided-shopping.appointment.update-attendee'));
    }

    /**
     * @return ArrayStruct<string, mixed>
     */
    public function getResult(): ArrayStruct
    {
        return $this->object;
    }

    /**
     * @return array<string, mixed>
     */
    public function getData(): array
    {
        return $this->object->get('data');
    }
}
