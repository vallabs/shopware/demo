<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Get;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionResponse;

abstract class AbstractAttendeeProductCollectionGetRoute
{
    abstract public function getDecorated(): AbstractAttendeeProductCollectionGetRoute;

    abstract public function getProducts(
        ?string $alias,
        SalesChannelContext $context
    ): AttendeeProductCollectionResponse;
}
