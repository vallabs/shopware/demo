<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\LastSeenProducts;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Collection\LastSeenProductsResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractLastSeenProductsRoute
{
    abstract public function getDecorated(): AbstractLastSeenProductsRoute;

    abstract public function getList(
        Request $request,
        SalesChannelContext $context
    ): LastSeenProductsResponse;
}
