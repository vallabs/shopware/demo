<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Add;

use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\NoContentResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

abstract class AbstractAttendeeProductCollectionAddRoute
{
    abstract public function getDecorated(): AbstractAttendeeProductCollectionAddRoute;

    abstract public function add(
        string $alias,
        string $productId,
        SalesChannelContext $context,
        Criteria $criteria
    ): NoContentResponse;
}
