<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Remove;

use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\NoContentResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

abstract class AbstractAttendeeProductCollectionRemoveRoute
{
    abstract public function getDecorated(): AbstractAttendeeProductCollectionRemoveRoute;

    abstract public function removeEntry(
        string $alias,
        string $productId,
        SalesChannelContext $context,
        Criteria $criteria
    ): NoContentResponse;
}
