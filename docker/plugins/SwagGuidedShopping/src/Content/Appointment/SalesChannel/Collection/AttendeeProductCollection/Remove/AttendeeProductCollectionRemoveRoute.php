<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Remove;

use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\NoContentResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeProductCollectionService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class AttendeeProductCollectionRemoveRoute extends AbstractAttendeeProductCollectionRemoveRoute
{
    public function __construct(
        private readonly AttendeeProductCollectionService $attendeeProductCollectionService
    ) {
    }

    public function getDecorated(): AbstractAttendeeProductCollectionRemoveRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/collection/{alias}/{productId}', name: 'store-api.guided-shopping.appointment.collection.attendee-product-collection-remove-product', defaults: ['attendee_required' => true, '_entity' => 'product'], methods: ['DELETE'])]
    public function removeEntry(string $alias, string $productId, SalesChannelContext $context, Criteria $criteria): NoContentResponse
    {
        /** @var AttendeeEntity $attendee */
        $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);

        $this->attendeeProductCollectionService->remove($alias, $attendee->getId(), $productId, $context->getContext());

        return new NoContentResponse();
    }
}
