<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\AttendeeProductCollection\Get;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionResponse;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeProductCollectionAliasNotFoundException;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeProductCollectionService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\SalesChannel\Exception\RouteAliasNotFoundException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class AttendeeProductCollectionGetRoute extends AbstractAttendeeProductCollectionGetRoute
{
    public function __construct(
        private readonly AttendeeProductCollectionService $attendeeProductCollectionService
    ) {
    }

    public function getDecorated(): AbstractAttendeeProductCollectionGetRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/collection/{alias}', name: 'store-api.guided-shopping.appointment.collection.get-attendee-product-collection', defaults: ['attendee_required' => true, '_entity' => 'product', 'alias' => null], methods: ['GET'])]
    public function getProducts(?string $alias, SalesChannelContext $context): AttendeeProductCollectionResponse
    {
        /** @var AttendeeEntity $attendee */
        $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);

        try {
            return new AttendeeProductCollectionResponse($this->attendeeProductCollectionService->getProducts($alias, $attendee->getId(), $context->getContext(), $context->getSalesChannelId()));
        } catch (AttendeeProductCollectionAliasNotFoundException $e) {
            throw new RouteAliasNotFoundException($alias);
        }
    }
}
