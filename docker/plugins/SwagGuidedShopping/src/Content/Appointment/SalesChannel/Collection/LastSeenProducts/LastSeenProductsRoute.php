<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\Collection\LastSeenProducts;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionStruct;
use SwagGuidedShopping\Content\Appointment\Collection\LastSeenProductsResponse;
use SwagGuidedShopping\Content\Appointment\Service\LastSeenProductsService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class LastSeenProductsRoute extends AbstractLastSeenProductsRoute
{
    public function __construct(
        private readonly LastSeenProductsService $lastSeenProductsService
    ) {
    }

    public function getDecorated(): AbstractLastSeenProductsRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/collection/last-seen', name: 'store-api.guided-shopping.appointment.get-last-seen-products', defaults: ['attendee_required' => true, '_entity' => 'product'], methods: ['GET'], priority: 2)]
    public function getList(Request $request, SalesChannelContext $context): LastSeenProductsResponse
    {
        /** @var AttendeeEntity $attendee */
        $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);
        $lastSeenProducts = new AttendeeProductCollectionStruct(['lastSeen' => $this->lastSeenProductsService->getProducts($attendee, $context)]);

        return new LastSeenProductsResponse($lastSeenProducts);
    }
}
