<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation;

use Symfony\Component\HttpFoundation\JsonResponse;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractUpdateInvitationStatusRoute
{
    abstract public function getDecorated(): AbstractUpdateInvitationStatusRoute;

    abstract public function updateInvitationStatus(
        string $appointmentId,
        Request $request,
        SalesChannelContext $context
    ): JsonResponse;
}
