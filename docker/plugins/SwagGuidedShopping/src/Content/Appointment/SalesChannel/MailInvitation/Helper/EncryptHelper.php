<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper;

class EncryptHelper
{
    protected const CIPHER_METHOD = "AES-256-CBC";
    protected const HASH_ALGORITHM = 'sha256';
    protected const INIT_VECTOR_LENGTH = 16;
    protected const INVITATION_SECRET_KEY = 'invitation-secret-key';

    // Encrypt a string with a validity period of 7 days
    public static function encryptString(string $string, ?int $lifeTimeInDays = 7): string
    {
        $validUntil = \time() + ($lifeTimeInDays * 24 * 60 * 60); // Calculate expiration timestamp with lifetime in days
        $dataJson = \json_encode(['string' => $string, 'valid_until' => $validUntil]);
        $key = \hash(static::HASH_ALGORITHM, self::INVITATION_SECRET_KEY, true);
        $iv = \openssl_random_pseudo_bytes(static::INIT_VECTOR_LENGTH);

        /** @phpstan-ignore-next-line */
        $encrypted = \openssl_encrypt($dataJson, static::CIPHER_METHOD, $key, OPENSSL_RAW_DATA, $iv);
        /** @phpstan-ignore-next-line  */
        $hmac = \hash_hmac(static::HASH_ALGORITHM, $encrypted, $key, true);

        return \base64_encode($iv . $hmac . $encrypted);
    }

    /**
     * @return array<string, mixed>
     */
    public static function decryptString(string $encryptedString): array
    {
        $key = \hash(static::HASH_ALGORITHM, self::INVITATION_SECRET_KEY, true);

        // replace all the white spaces by character "+" before decoding the key
        $data = \base64_decode(\strtr($encryptedString, ' ', '+'));
        $iv = \mb_substr($data, 0, static::INIT_VECTOR_LENGTH, '8bit');
        $hmac = \mb_substr($data, static::INIT_VECTOR_LENGTH, 32, '8bit');
        $encrypted = \mb_substr($data, static::INIT_VECTOR_LENGTH + 32, null, '8bit');

        /** @var string $decrypted  */
        $decrypted = \openssl_decrypt($encrypted, static::CIPHER_METHOD, $key, OPENSSL_RAW_DATA, $iv);
        $calculatedHmac = \hash_hmac(static::HASH_ALGORITHM, $encrypted, $key, true);

        if (\hash_equals($hmac, $calculatedHmac)) {
            $dataJson = \json_decode($decrypted, true);
            $validUntil = $dataJson['valid_until'];

            if (\time() <= $validUntil) {
                return ['valid' => true, 'email' => $dataJson['string']];
            } else {
                // Encrypted string has expired
                return ['valid' => false, 'error' => 'EXPIRED_TOKEN', 'message' => 'Token already expired.'];
            }
        } else {
            // Invalid HMAC, the data may have been tampered with
            return ['valid' => false, 'error' => 'INVALID_TOKEN', 'message' => 'Invalid token.'];
        }
    }
}
