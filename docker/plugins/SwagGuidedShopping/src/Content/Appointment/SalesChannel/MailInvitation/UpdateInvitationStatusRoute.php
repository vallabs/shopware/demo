<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeInvitationInvalidRequestParameter;
use SwagGuidedShopping\Content\Appointment\Service\AttendeeService;
use SwagGuidedShopping\Content\Appointment\SalesChannel\MailInvitation\Helper\EncryptHelper;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Choice;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class UpdateInvitationStatusRoute extends AbstractUpdateInvitationStatusRoute
{
    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly AttendeeService $attendeeService,
        private readonly DataValidator $dataValidator,
        private readonly EntityRepository $appointmentRepository
    ) {
    }

    public function getDecorated(): AbstractUpdateInvitationStatusRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/{appointmentId}/attendee/respond-invitation', name: 'store-api.guided-shopping.appointment.attendee-respond-invitation', methods: ['PATCH'])]
    public function updateInvitationStatus(string $appointmentId, Request $request, SalesChannelContext $salesChannelContext): JsonResponse
    {
        $this->validateRequestBody($request->request->all());
        $context = $salesChannelContext->getContext();

        /** @var string $token  */
        $token = $request->request->get('token', '');
        $decryptResult = EncryptHelper::decryptString($token);
        if(!$decryptResult['valid'] || !isset($decryptResult['email'])) {
            return new JsonResponse($decryptResult, 401);
        }

        /** @var AppointmentEntity|null $appointment */
        $appointment = $this->appointmentRepository->search(new Criteria([$appointmentId]), $context)->first();
        if (!$appointment) {
            return new JsonResponse([
                'error' => "APPOINTMENT_NOT_FOUND",
                'message' => 'Appointment is not found.'
            ], 400);
        }

        /** @var string $answer  */
        $answer = $request->request->get('answer');
        $isUpdated = $this->attendeeService->updateInvitationStatus([$decryptResult['email']], $appointmentId, $answer, $context);
        if (!$isUpdated) {
            return new JsonResponse([
                'error' => "ATTENDEE_NOT_FOUND",
                'message' => 'Attendee is not found.'
            ], 400);
        }

        $appointmentStatus = null;
        if ($appointment->getStartedAt()) {
            $appointmentStatus = $appointment->getEndedAt() ? 'ended' : 'started';
        }

        return new JsonResponse([
            'appointment' => [
                'id' => $appointment->getId(),
                'accessibleFrom' => $appointment->getAccessibleFrom(),
                'accessibleTo' => $appointment->getAccessibleTo(),
                'status' => $appointmentStatus
            ],
            "answer" => $answer
        ]);
    }

    /**
     * @param array<string, mixed> $jsonRequestPayload
     */
    private function validateRequestBody(array $jsonRequestPayload): void
    {
        $constraint = new Collection(
            [
                'token' => [new Type('string'), new Required(), new NotBlank()],
                'answer' => [new Type('string'), new Required(), new NotBlank(), new Choice(['maybe', 'declined', 'accepted'])],
            ]
        );

        $violations = $this->dataValidator->validate($jsonRequestPayload, $constraint);
        if ($violations->count()) {
            throw new AttendeeInvitationInvalidRequestParameter($violations);
        }
    }
}
