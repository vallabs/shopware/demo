<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Traits;

use Spatie\IcalendarGenerator\Components\Calendar;
use Spatie\IcalendarGenerator\Components\Event;
use Spatie\IcalendarGenerator\Enums\EventStatus;
use Spatie\IcalendarGenerator\Properties\TextProperty;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;

trait IcsTrait
{
    /**
     * @param array<string, array<string>>|null $recipients
     * @return array<string>
     */
    public function generateIcsFile(
        AppointmentEntity $appointment,
        ?array $recipients = null,
        ?string $invitationAction = AppointmentDefinition::INVITE_ACTION
    ): array
    {
        /** @var string $address */
        $address = $appointment->getPresentationUrl();
        $eventStatus = $invitationAction === AppointmentDefinition::CANCEL_ACTION
            ? EventStatus::cancelled()
            : EventStatus::confirmed();
        $appointmentName = $appointment->getTranslation('name') ?? 'Digital Sales Rooms';
        /** @var array<string> $organizer */
        $organizer = $appointment->getGuideInfo();
        $serverName = $_SERVER['SERVER_NAME'] ?? 'shopware.com';

        /* build the ics file*/
        $icsEvent = Event::create()
            ->uniqueIdentifier("{$appointment->getId()}@{$serverName}")
            ->name($appointmentName)
            ->description($appointment->getMessage() ?? '')
            ->createdAt(new \DateTimeImmutable('now'));

        if ($appointment->getAccessibleFrom() && $appointment->getAccessibleTo()) {
            $icsEvent
                ->startsAt($appointment->getAccessibleFrom())
                ->endsAt($appointment->getAccessibleTo());
        }

        $icsEvent
            ->address($address)
            ->status($eventStatus)
            ->transparent();

        // send to the guide as well, but considered as an attendee
        $icsEvent->attendee($organizer['email'], $organizer['name']);

        foreach ($recipients ?? $appointment->getAttendees()->getAppointmentRecipients() as $email => $recipient) {
            $icsEvent->attendee($email, $recipient['name']);
        }

        /** @phpstan-ignore-next-line  */
        $content = Calendar::create()
                ->event($icsEvent)
                ->appendProperty(TextProperty::create('CALSCALE', 'GREGORIAN'))
                ->appendProperty(TextProperty::create('METHOD', 'REQUEST'))
                ->get();

        // replace PRODID created by library
        $content = \str_replace('spatie/icalendar-generator', '-//DIGITAL SALES ROOMS//Calendar//EN', $content);

        return [
            'content' => $content,
            'fileName' => "invite.ics",
            'mimeType' => 'text/calendar'
        ];
    }
}
