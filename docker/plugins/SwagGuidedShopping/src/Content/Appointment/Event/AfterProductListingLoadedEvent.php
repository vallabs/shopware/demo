<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Event;

use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Event\ShopwareSalesChannelEvent;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class AfterProductListingLoadedEvent implements ShopwareSalesChannelEvent
{
    /**
     * @param EntitySearchResult<ProductCollection> $searchResult
     */
    public function __construct(
        protected EntitySearchResult $searchResult,
        protected SalesChannelContext $salesChannelContext
    ) {
    }

    public function getContext(): Context
    {
        return $this->salesChannelContext->getContext();
    }

    public function getSalesChannelContext(): SalesChannelContext
    {
        return $this->salesChannelContext;
    }

    /**
     * @return EntitySearchResult<ProductCollection>
     */
    public function getSearchResult(): EntitySearchResult
    {
        return $this->searchResult;
    }
}
