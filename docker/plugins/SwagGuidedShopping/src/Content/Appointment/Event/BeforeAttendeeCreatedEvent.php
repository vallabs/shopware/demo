<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Event;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\ShopwareSalesChannelEvent;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class BeforeAttendeeCreatedEvent implements ShopwareSalesChannelEvent
{
    /**
     * @param array<string> $attendeeData
     */
    public function __construct(
        protected array $attendeeData,
        protected SalesChannelContext $salesChannelContext
    ) {
    }

    /**
     * @return array<string>
     */
    public function getAttendeeData(): array
    {
        return $this->attendeeData;
    }

    /**
     * @param array<string> $attendeeData
     */
    public function setAttendeeData(array $attendeeData): void
    {
        $this->attendeeData = $attendeeData;
    }

    public function getContext(): Context
    {
        return $this->salesChannelContext->getContext();
    }

    public function getSalesChannelContext(): SalesChannelContext
    {
        return $this->salesChannelContext;
    }
}
