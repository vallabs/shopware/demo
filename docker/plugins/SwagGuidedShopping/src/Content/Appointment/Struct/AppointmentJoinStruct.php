<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\Struct;

use Shopware\Core\Framework\Struct\Struct;

class AppointmentJoinStruct extends Struct
{
    /**
     * @var array<string>
     */
    protected array $mercureSubscriberTopics = [];

    protected ?string $mercurePublisherTopic = null;

    protected ?string $JWTMercureSubscriberToken = null;

    protected ?string $mercureHubPublicUrl = null;

    protected ?string $JWTMercurePublisherToken = null;

    protected string $salesChannelName;

    protected ?string $appointmentName = null;

    public function __construct(
        protected string $id,
        protected string $newContextToken,
        protected string $attendeeId,
        protected string $salesChannelId,
        protected string $presentationGuideMode,
        protected ?bool $isPreview = false
    ) {
    }

    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array<string>
     */
    public function getMercureSubscriberTopics(): array
    {
        return $this->mercureSubscriberTopics;
    }

    public function getMercurePublisherTopic(): ?string
    {
        return $this->mercurePublisherTopic;
    }

    public function getAttendeeId(): string
    {
        return $this->attendeeId;
    }

    /**
     * @param array<string> $mercureSubscriberTopics
     */
    public function setMercureSubscriberTopics(array $mercureSubscriberTopics): void
    {
        $this->mercureSubscriberTopics = $mercureSubscriberTopics;
    }

    public function setMercurePublisherTopic(?string $mercurePublisherTopic): void
    {
        $this->mercurePublisherTopic = $mercurePublisherTopic;
    }

    public function setJWTMercureSubscriberToken(?string $JWTMercureSubscriberToken): void
    {
        $this->JWTMercureSubscriberToken = $JWTMercureSubscriberToken;
    }

    public function setMercureHubPublicUrl(?string $mercureHubPublicUrl): void
    {
        $this->mercureHubPublicUrl = $mercureHubPublicUrl;
    }

    public function setJWTMercurePublisherToken(?string $JWTMercurePublisherToken): void
    {
        $this->JWTMercurePublisherToken = $JWTMercurePublisherToken;
    }

    public function getNewContextToken(): string
    {
        return $this->newContextToken;
    }

    public function setSalesChannelName(string $salesChannelName): void
    {
        $this->salesChannelName = $salesChannelName;
    }

    public function setAppointmentName(?string $appointmentName): void
    {
        $this->appointmentName = $appointmentName;
    }
}
