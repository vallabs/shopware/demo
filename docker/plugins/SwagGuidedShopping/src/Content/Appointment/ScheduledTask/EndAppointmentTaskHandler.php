<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\ScheduledTask;

use Psr\Log\LoggerInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskCollection;
use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTaskHandler;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Exception\CouldNotEndAppointmentException;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use SwagGuidedShopping\Struct\ConfigKeys;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler(handles: EndAppointmentTask::class)]
class EndAppointmentTaskHandler extends ScheduledTaskHandler
{
    protected const MAX_HOURS_BEFORE_CLOSE_APPOINTMENT_DEFAULT = 4;

    /**
     * @param EntityRepository<ScheduledTaskCollection> $scheduledTaskRepository
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly LoggerInterface    $logger,
        protected EntityRepository          $scheduledTaskRepository,
        private readonly EntityRepository   $appointmentRepository,
        private readonly AppointmentService $appointmentService,
        private readonly InteractionService $interactionService,
        private readonly SystemConfigService $configService
    ) {
        parent::__construct($scheduledTaskRepository);
    }

    public function run(): void
    {
        $maxMinutesAfterLastInteractionConfigurationValue = $this->configService->getInt(ConfigKeys::MAX_HOURS_BEFORE_CLOSE_APPOINTMENT);
        if ($maxMinutesAfterLastInteractionConfigurationValue > 0) {
            $maxMinutesAfterLastInteraction = $maxMinutesAfterLastInteractionConfigurationValue * 60;
        } else {
            $maxMinutesAfterLastInteraction = 60 * self::MAX_HOURS_BEFORE_CLOSE_APPOINTMENT_DEFAULT;
        }

        $now = new \DateTime('now', new \DateTimeZone('UTC'));
        $context = Context::createDefaultContext();
        /** @var array<AppointmentEntity> $appointments */
        $appointments = $this->loadAppointments($context);
        foreach ($appointments as $appointment) {
            try {
                $lastInteraction = $this->interactionService->getLastInteractionDateTime($appointment->getId(), $context);
                if (!$lastInteraction) {
                    $lastInteraction = $appointment->getStartedAt();
                }

                if (!$lastInteraction) {
                    throw new CouldNotEndAppointmentException('could not end appointment as there is no interaction and appointment has no started date');
                }

                $timeDifferenceMinutes = ($now->getTimestamp() - $lastInteraction->getTimestamp()) / 60;
                if ($timeDifferenceMinutes > $maxMinutesAfterLastInteraction && $this->appointmentService->isPresentationEnable($appointment)) {
                    // end presentation and reinitialize state
                    $context->addExtension(PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION, new ArrayStruct([true]));
                    $this->appointmentService->endPresentation($appointment->getId(), $context);
                } else {
                    throw new CouldNotEndAppointmentException(
                        'could not end appointment as there is a presentation not enabled yet and the time interval from the last interaction to now not greater than the max hours to close appointment you configure'
                    );
                }
            } catch (\Exception $e) {
                $this->logger->critical('could not end appointment automatically', ['appointmentId' => $appointment->getId(), 'error' => $e]);
            }
        }
    }

    /**
     * @return array<Entity>
     */
    private function loadAppointments(Context $context): array
    {
        $criteria = new Criteria();
        //make sure it is started
        $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('startedAt', null)]));
        $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('presentationId', null)]));
        $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('guidedShoppingPresentationVersionId', null)]));
        //make sure it is NOT self mode
        $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('mode', AppointmentDefinition::MODE_SELF)]));
        //make sure it is NOT ended yet
        $criteria->addFilter(new EqualsFilter('endedAt', null));

        return $this->appointmentRepository->search($criteria, $context)->getElements();
    }
}
