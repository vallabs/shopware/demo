<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Appointment\ScheduledTask;

use Shopware\Core\Framework\MessageQueue\ScheduledTask\ScheduledTask;

class EndAppointmentTask extends ScheduledTask
{
    public static function getTaskName(): string
    {
        return 'shopware_guided_shopping.end_appointment_task';
    }

    public static function getDefaultInterval(): int
    {
        return 3600;
    }
}
