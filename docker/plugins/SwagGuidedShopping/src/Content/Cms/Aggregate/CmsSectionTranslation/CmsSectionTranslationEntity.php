<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;

class CmsSectionTranslationEntity extends TranslationEntity
{
    protected string $cmsSectionId;

    protected ?string $name;

    protected ?CmsSectionEntity $cmsSection;

    public function getCmsSectionId(): string
    {
        return $this->cmsSectionId;
    }

    public function setCmsSectionId(string $cmsSectionId): void
    {
        $this->cmsSectionId = $cmsSectionId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getCmsSection(): ?CmsSectionEntity
    {
        return $this->cmsSection;
    }

    public function setCmsSection(?CmsSectionEntity $cmsSection): void
    {
        $this->cmsSection = $cmsSection;
    }
}
