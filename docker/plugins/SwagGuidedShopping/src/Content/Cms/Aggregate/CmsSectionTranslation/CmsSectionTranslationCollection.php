<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<CmsSectionTranslationEntity>
 */
class CmsSectionTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return CmsSectionTranslationEntity::class;
    }
}
