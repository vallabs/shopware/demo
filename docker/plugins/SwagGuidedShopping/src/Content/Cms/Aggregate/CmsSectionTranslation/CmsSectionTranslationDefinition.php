<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class CmsSectionTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'cms_section_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getParentDefinitionClass(): string
    {
        return CmsSectionDefinition::class;
    }

    public function getEntityClass(): string
    {
        return CmsSectionTranslationEntity::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('name', 'name'))->addFlags(new Required()),
        ]);
    }
}
