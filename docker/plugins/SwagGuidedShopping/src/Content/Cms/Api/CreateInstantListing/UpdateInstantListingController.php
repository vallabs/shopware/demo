<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Api\CreateInstantListing;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Cms\Service\InstantListingRequestHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class UpdateInstantListingController extends AbstractController
{
    public function __construct(
        private readonly InstantListingRequestHandler $instantListingRequestHandler
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/instant-listing', name: 'api.action.guided-shopping.appointment.update-instant-listing', methods: ['PATCH'])]
    public function update(string $appointmentId, Context $context, Request $request): JsonResponse
    {
        $updatedData = $this->instantListingRequestHandler->handleUpdateInstantListingRequest($appointmentId, $request->request->all(), $context);

        return new JsonResponse($updatedData, Response::HTTP_OK);
    }
}
