<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Service;

use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;

class NotesElementHandler
{
    public const UNIQUE_IDENTIFIER_FIELD = '_uniqueIdentifier';

    /**
     * @return array<CmsSlotEntity>
     */
    public function removeNotes(CmsSectionEntity $section): array
    {
        $section->setCustomFields([self::UNIQUE_IDENTIFIER_FIELD => $section->getId()]);

        /** @var array<CmsSlotEntity> $notes */
        $notes = [];

        /** @var CmsBlockCollection|null $blocks */
        $blocks = $section->getBlocks();
        if (!$blocks || !$blocks->count()) {
            return [];
        }

        /** @var array<CmsBlockEntity> $newBlocks */
        $newBlocks = [];
        /** @var CmsBlockEntity $block */
        foreach ($blocks as $block) {
            $block->setCustomFields([self::UNIQUE_IDENTIFIER_FIELD => $block->getId()]);

            /** @var array<CmsSlotEntity> $newSlots */
            $newSlots = [];

            /** @var CmsSlotCollection|null $slots */
            $slots = $block->getSlots();
            if (!$slots || !$slots->count()) {
                continue;
            }

            /** @var CmsSlotEntity $slot */
            foreach ($slots as $slot) {
                $slot->setCustomFields([self::UNIQUE_IDENTIFIER_FIELD => $slot->getId()]);

                if ($slot->getType() === 'notes') {
                    $notes[] = $slot;
                    continue;
                }

                $newSlots[] = $slot;
            }

            if (\count($newSlots)) {
                $newBlocks[] = $block;
                $block->setSlots(new CmsSlotCollection($newSlots));
            }
        }

        $section->setBlocks(new CmsBlockCollection($newBlocks));

        return $notes;
    }
}
