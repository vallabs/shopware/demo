<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Service;

use Shopware\Core\Defaults;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Aggregation\Metric\MaxAggregation;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\Metric\MaxResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;

class CmsPageCreatorService
{
    /**
     * @param EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository
     */
    public function __construct(
        private readonly EntityRepository $presentationCmsPageRepository
    ) {
    }

    /**
     * @param array<int, string> $productIds
     */
    public function createPresentationPage(
        string $layoutId,
        array $productIds,
        string $presentationId,
        string $presentationVersionId,
        int $startPosition,
        bool $isInstantListing,
        ?string $pageName,
        Context $context
    ): int
    {
        $currentIndex = $this->updateSlidePositions($startPosition, $presentationId, $presentationVersionId, $context);

        $this->presentationCmsPageRepository->upsert([
            [
                'cmsPageId' => $layoutId,
                'cmsPageVersionId' => Defaults::LIVE_VERSION,
                'pickedProductIds' => $productIds,
                'presentationId' => $presentationId,
                'title' => $pageName,
                'guidedShoppingPresentationVersionId' => $presentationVersionId,
                'position' => $startPosition,
                'isInstantListing' => $isInstantListing
            ],
        ], $context);

        return $currentIndex;
    }

    /**
     * @param array<int, string> $addProductIds
     * @param array<int, string> $removeProductIds
     *
     * @return array<string, mixed>
     */
    public function updatePresentationPage(
        array $addProductIds,
        array $removeProductIds,
        string $presentationPageId,
        ?string $pageName,
        Context $context
    ): array
    {
        $criteria = new Criteria([$presentationPageId]);

        /** @var PresentationCmsPageEntity|null $presentationCmsPage */
        $presentationCmsPage = $this->presentationCmsPageRepository->search($criteria, $context)->first();

        /** @var array<int, string> $pickedProductIds */
        $pickedProductIds = $presentationCmsPage && $presentationCmsPage->getPickedProductIds() ? $presentationCmsPage->getPickedProductIds() : [];

        if ($addProductIds) {
            $pickedProductIds = \array_merge($addProductIds, $pickedProductIds);
        }

        if ($removeProductIds) {
            $pickedProductIds = \array_diff($pickedProductIds, $removeProductIds);
        }

        $updateData = [
            'id' => $presentationPageId,
            'pickedProductIds' => \array_values(\array_unique($pickedProductIds)),
            'title' => $pageName,
        ];

        $this->presentationCmsPageRepository->upsert([$updateData], $context);

        return $updateData;
    }

    public function getPositionForSlide(string $presentationPageId, Context $context): int
    {
        $page = $this->presentationCmsPageRepository->search(new Criteria([$presentationPageId]), $context)->first();
        if (!$page instanceof PresentationCmsPageEntity) {
            throw new \Exception('page could not be found');
        }

        return $page->getPosition();
    }

    public function getPositionForLastSlide(string $presentationId, string $presentationVersionId, Context $context): int
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationId', $presentationId));
        $criteria->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', $presentationVersionId));
        $criteria->addAggregation(new MaxAggregation('maxPosition', 'position'));
        $pages = $this->presentationCmsPageRepository->search($criteria, $context);

        /** @var MaxResult|null $currentMaxPosition */
        $currentMaxPosition = $pages->getAggregations()->get('maxPosition');
        if ($currentMaxPosition === null || $currentMaxPosition->getMax() === null) {
            return 0;
        }

        return (int) $currentMaxPosition->getMax();
    }

    public function updateSlidePositions(
        int $startAtPosition,
        string $presentationId,
        string $presentationVersionId,
        Context $context
    ): int
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationId', $presentationId));
        $criteria->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', $presentationVersionId));
        $criteria->addSorting(new FieldSorting('position'));
        $criteria->addAssociation('cmsPage.sections');

        $pagesToUpdate = $this->presentationCmsPageRepository->search($criteria, $context);

        $upserts = [];
        $index = 0;
        /** @var PresentationCmsPageEntity $cmsPage */
        foreach ($pagesToUpdate->getElements() as $cmsPage) {
            if ($cmsPage->getPosition() >= $startAtPosition) {
                $upserts[] = [
                    'id' => $cmsPage->getId(),
                    'position' => $cmsPage->getPosition() + 1,
                ];
            } else {
                $index += $cmsPage->getCmsPage()?->getSections()?->count() ?? 0;
            }
        }

        $this->presentationCmsPageRepository->upsert($upserts, $context);

        return $index + 1;
    }
}
