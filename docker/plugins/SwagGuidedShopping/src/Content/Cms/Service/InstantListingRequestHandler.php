<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Service;

use Shopware\Core\Framework\Context;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Exception\AppointmentIsSelfModeException;
use SwagGuidedShopping\Content\Appointment\Service\AppointmentService;
use SwagGuidedShopping\Content\Exception\InvalidParameterException;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Exception\NotConfiguredException;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagGuidedShopping\Struct\ConfigKeys;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Constraints\Type;

class InstantListingRequestHandler
{
    public function __construct(
        private readonly CmsPageCreatorService $cmsPageCreatorService,
        private readonly SystemConfigService $configService,
        private readonly DataValidator $dataValidator,
        private readonly AppointmentService $appointmentService,
        private readonly PresentationVersionHandler $presentationVersionHandler
    ) {
    }

    /**
     * @param array<string, mixed> $requestParameters
     */
    public function handleCreateInstantListingRequest(string $appointmentId, array $requestParameters, Context $context): int
    {
        $constraint = new Collection(
            [
                'productIds' => [new Type('array'), new Required()],
                'currentPageGroupId' => [new Type('string')],
            ]
        );

        $appointment = $this->appointmentService->getAppointment($appointmentId, $context);

        if ($appointment->getMode() === AppointmentDefinition::MODE_SELF) {
            throw new AppointmentIsSelfModeException();
        }

        $presentationVersionId = $this->presentationVersionHandler->createPresentationVersion($appointment, $context);

        $violations = $this->dataValidator->validate($requestParameters, $constraint);
        if ($violations->count()) {
            throw new InvalidParameterException($violations);
        }

        $listingLayoutId = $this->configService->getString(ConfigKeys::PRODUCT_LISTING_DEFAULT_PAGE_ID);
        if (\trim($listingLayoutId) === '') {
            throw new NotConfiguredException('instantListingLayoutId needs to be configured');
        }

        if (isset($requestParameters['currentPageGroupId']) && $requestParameters['currentPageGroupId'] !== '') {
            $currentPosition = $this->cmsPageCreatorService->getPositionForSlide(
                $requestParameters['currentPageGroupId'],
                $this->presentationVersionHandler->getContextForAppointment($presentationVersionId, $context)
            );
        } else {
            $currentPosition = 0; // will be first slide of presentation if no current page group id is passed
        }

        return $this->cmsPageCreatorService->createPresentationPage(
            $listingLayoutId,
            $requestParameters['productIds'],
            $appointment->getPresentationId(),
            $presentationVersionId,
            $currentPosition + 1,
            true,
            $requestParameters['pageName'] ?? null,
            $context
        );
    }

    /**
     * @param array<string, mixed> $requestParameters
     * @return array<string, mixed>
     */
    public function handleUpdateInstantListingRequest(string $appointmentId, array $requestParameters, Context $context): array
    {
        $constraint = new Collection(
            [
                'addProductIds' => [new Type('array'), new Required()],
                'removeProductIds' => [new Type('array'), new Required()],
                'currentPageGroupId' => [new Type('string'), new Required(), new NotBlank()],
            ]
        );

        $this->appointmentService->getAppointment($appointmentId, $context);

        $violations = $this->dataValidator->validate($requestParameters, $constraint);
        if ($violations->count()) {
            throw new InvalidParameterException($violations);
        }

        return $this->cmsPageCreatorService->updatePresentationPage(
            $requestParameters['addProductIds'],
            $requestParameters['removeProductIds'],
            $requestParameters['currentPageGroupId'],
            $requestParameters['pageName'] ?? null,
            $context
        );
    }
}
