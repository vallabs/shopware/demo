<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms\Service;

use Shopware\Core\Content\Cms\Aggregate\CmsBlock\CmsBlockEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\DataResolver\CmsSlotsDataResolver;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;

/**
 * @internal
 */
class PresentationCmsPageLoader implements SalesChannelCmsPageLoaderInterface
{
    /**
     * @internal
     */
    public function __construct(
        private readonly SalesChannelCmsPageLoaderInterface $inner,
        private readonly CmsSlotsDataResolver $slotDataResolver
    ) {
    }

    /**
     * @param array<string, mixed> $config
     */
    public function load(
        Request $request,
        Criteria $criteria,
        SalesChannelContext $context,
        ?array $config = null,
        ?ResolverContext $resolverContext = null
    ): EntitySearchResult
    {
        $pages = $this->inner->load(
            $request,
            $criteria,
            $context,
            $config,
            $resolverContext
        );

        /** @var CmsPageEntity $page */
        foreach ($pages as $page) {
            if (!$page->getSections() || !\count($page->getSections())) {
                continue;
            }

            $page->getSections()->sort(function (CmsSectionEntity $a, CmsSectionEntity $b) {
                return $a->getPosition() <=> $b->getPosition();
            });

            if (!$resolverContext) {
                $resolverContext = new ResolverContext($context, $request);
            }

            // step 2, sort blocks into sectionPositions
            /** @var CmsSectionEntity $section */
            foreach ($page->getSections() as $section) {
                $blocks = $section->getBlocks();

                if ($blocks) {
                    $blocks->sort(function (CmsBlockEntity $a, CmsBlockEntity $b) {
                        return $a->getPosition() <=> $b->getPosition();
                    });

                    /** @var CmsBlockEntity $block */
                    foreach ($blocks as $block) {
                        if ($block->getSlots()) {
                            $block->getSlots()->sort(function (CmsSlotEntity $a, CmsSlotEntity $b) {
                                return $a->getSlot() <=> $b->getSlot();
                            });
                        }
                    }
                }
            }

            // step 3, find config overwrite
            $overwrite = $config[$page->getId()] ?? $config;

            // step 4, overwrite slot config
            $this->overwriteSlotConfig($page, $overwrite);

            // step 5, resolve slot data
            $this->loadSlotData($page, $resolverContext);
        }

        return $pages;
    }

    private function loadSlotData(CmsPageEntity $page, ResolverContext $resolverContext): void
    {
        if ($page->getSections()) {
            $slots = $this->slotDataResolver->resolve($page->getSections()->getBlocks()->getSlots(), $resolverContext);
            $page->getSections()->getBlocks()->setSlots($slots);
        }
    }

    /**
     * @param CmsPageEntity $page
     * @param array<string, mixed> $config
     * @return void
     */
    private function overwriteSlotConfig(CmsPageEntity $page, ?array $config = []): void
    {
        if ($page->getSections()) {
            /** @var CmsSlotEntity $slot */
            foreach ($page->getSections()->getBlocks()->getSlots() as $slot) {
                if (!$slot->getConfig() && $slot->getTranslation('config')) {
                    $slot->setConfig($slot->getTranslation('config'));
                }

                if (empty($config)) {
                    continue;
                }

                if (!isset($config[$slot->getId()])) {
                    continue;
                }

                $slot->setConfig($config[$slot->getId()]);
                $slot->addTranslated('config', $config[$slot->getId()]);
            }
        }
    }
}
