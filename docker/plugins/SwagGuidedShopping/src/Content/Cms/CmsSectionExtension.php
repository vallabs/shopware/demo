<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Cms;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Runtime;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationDefinition;

class CmsSectionExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new TranslatedField('name'))->addFlags(new Runtime())
        );
        $collection->add(
            (new TranslationsAssociationField(
                CmsSectionTranslationDefinition::class,
                'cms_section_id'
            ))->addFlags(new ApiAware())
        );
    }

    public function getDefinitionClass(): string
    {
        return CmsSectionDefinition::class;
    }
}
