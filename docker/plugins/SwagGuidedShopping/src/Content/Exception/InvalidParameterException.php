<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class InvalidParameterException extends ShopwareHttpException
{
    public function __construct(ViolationList $violationList)
    {
        $message = "The Request has the following errors \n";
        $message .= $violationList;
        parent::__construct($message);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__INVALID_PARAMETER;
    }
}
