<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\State;

use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;

class StateForMe extends AbstractPresentationState
{
    /** @gs\mercure-update */
    protected ?string $attendeeId = null;

    /** @gs\mercure-update */
    protected ?string $attendeeName = null;

    /** @gs\mercure-update */
    protected ?string $videoUserId = null;

    /** @gs\mercure-update */
    protected ?bool $guideCartPermissionsGranted = false;

    /** @gs\mercure-update */
    protected ?\DateTimeInterface $attendeeSubmittedAt = null;

    public function setAttendee(AttendeeEntity $attendee): void
    {
        $this->setAttendeeId($attendee->getId());
        $this->setAttendeeName($attendee->getAttendeeName());
        $this->setVideoUserId($attendee->getVideoUserId());
        $this->setGuideCartPermissionsGranted($attendee->isGuideCartPermissionsGranted());
        $this->setAttendeeSubmittedAt($attendee->getAttendeeSubmittedAt());
    }

    public function setAttendeeId(string $attendeeId): void
    {
        $this->attendeeId = $attendeeId;
    }

    public function setAttendeeName(?string $attendeeName): void
    {
        $this->attendeeName = $attendeeName;
    }

    public function setVideoUserId(?string $videoUserId): void
    {
        $this->videoUserId = $videoUserId;
    }

    public function setGuideCartPermissionsGranted(?bool $guideCartPermissionsGranted): void
    {
        $this->guideCartPermissionsGranted = $guideCartPermissionsGranted;
    }

    public function setAttendeeSubmittedAt(?\DateTimeInterface $attendeeSubmittedAt): void
    {
        $this->attendeeSubmittedAt = $attendeeSubmittedAt;
    }

    /**
     * @return array<string, mixed>
     */
    public function getMercureUpdatePayload(): array
    {
        return ['state-for-me' => $this->getData()];
    }
}
