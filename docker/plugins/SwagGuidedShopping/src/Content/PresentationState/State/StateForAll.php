<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\State;

use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractDynamicPageOpenedPayload;

class StateForAll extends AbstractPresentationState
{
    /** @gs\mercure-update */
    protected ?string $currentGuideProductId = null;

    /** @gs\mercure-update */
    protected ?string $lastActiveGuideSection = null;

    /** @gs\mercure-update */
    protected ?string $currentPageId = null;

    /** @gs\mercure-update */
    protected ?string $currentSectionId = null;

    /** @gs\mercure-update */
    protected ?int $currentSlideAlias = 0;

    /** @gs\mercure-update */
    protected ?AbstractDynamicPageOpenedPayload $currentDynamicPage = null;

    /** @gs\mercure-update */
    protected bool $started = false;

    /** @gs\mercure-update */
    protected bool $running = false;

    /** @gs\mercure-update */
    protected bool $ended = false;

    /** @gs\mercure-update */
    protected ?\DateTimeInterface $startedAt = null;

    /** @gs\mercure-update */
    protected ?\DateTimeInterface $endedAt = null;

    /** @gs\mercure-update */
    protected ?\DateTimeInterface $accessibleFrom = null;

    /** @gs\mercure-update */
    protected ?\DateTimeInterface $accessibleTo = null;

    /** @gs\mercure-update */
    protected ?string $appointmentMode = null;

    /** @gs\mercure-update */
    protected string $videoAudioSettings = 'none';

    /** @gs\mercure-update */
    protected string $videoRoomUrl = '';

    /** @gs\mercure-update */
    protected ?string $attendeeRestrictionType = null;

    /** @gs\mercure-update */
    protected ?string $productDetailDefaultPageId = null;

    /** @gs\mercure-update */
    protected ?string $quickviewPageId = null;

    /** @gs\mercure-update */
    protected ?string $productListingDefaultPageId = null;

    /** @gs\mercure-update */
    protected bool $allowUserActionsForGuide = false;

    /** @gs\mercure-update */
    protected bool $broadcastMode = false;

    public function setCurrentGuideProductId(?string $currentGuideProductId): void
    {
        $this->currentGuideProductId = $currentGuideProductId;
    }

    public function setBroadcastMode(bool $active): void
    {
       $this->broadcastMode = $active;
    }

    public function setLastActiveGuideSection(?string $lastActiveGuideSection): void
    {
        $this->lastActiveGuideSection = $lastActiveGuideSection;
    }

    public function setCurrentSlide(?string $currentPageId, ?string $currentSectionId, ?int $currentSlideAlias): void
    {
        $this->currentPageId = $currentPageId;
        $this->currentSectionId = $currentSectionId;
        $this->lastActiveGuideSection = null;
        $this->currentSlideAlias = $currentSlideAlias;
    }

    /**
     * @return array<string, mixed>
     */
    public function getMercureUpdatePayload(): array
    {
        return ['state-for-all' => $this->getData()];
    }

    public function setDynamicPageOpen(AbstractDynamicPageOpenedPayload $dynamicPageOpenedPayload): void
    {
        $this->currentDynamicPage = $dynamicPageOpenedPayload;
    }

    public function setDynamicPageClosed(): void
    {
        $this->currentDynamicPage = null;
    }

    public function setAppointmentState(AppointmentEntity $appointment): void
    {
        $this->setStartedAt($appointment->getStartedAt());
        $this->setEndedAt($appointment->getEndedAt());
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): void
    {
        $this->startedAt = $startedAt;
        $this->started = (bool) $startedAt;
        $this->setRunningState();
    }

    public function setEndedAt(?\DateTimeInterface $endedAt): void
    {
        $this->ended = $this->started && $endedAt;
        $this->endedAt = $this->ended ? $endedAt : null;
        $this->setRunningState();
    }

    public function setAccessibleFrom(?\DateTimeInterface $accessibleFrom): void
    {
        $this->accessibleFrom = $accessibleFrom;
    }

    public function setAccessibleTo(?\DateTimeInterface $accessibleTo): void
    {
        $this->accessibleTo = $accessibleTo;
    }

    public function setVideoAudioSettings(string $videoAudioSettings): void
    {
        $this->videoAudioSettings = $videoAudioSettings;
    }

    public function setVideoRoomUrl(string $videoRoomUrl): void
    {
        $this->videoRoomUrl = $videoRoomUrl;
    }

    public function setAppointmentMode(string $appointmentMode): void
    {
        $this->appointmentMode = $appointmentMode;
    }

    public function setAttendeeRestrictionType(?string $attendeeRestrictionType): void
    {
        $this->attendeeRestrictionType = $attendeeRestrictionType;
    }

    public function getProductDetailDefaultPageId(): ?string
    {
        return $this->productDetailDefaultPageId;
    }

    public function setProductDetailDefaultPageId(?string $productDetailDefaultPageId): void
    {
        $this->productDetailDefaultPageId = $productDetailDefaultPageId;
    }

    public function getQuickviewPageId(): ?string
    {
        return $this->quickviewPageId;
    }

    public function setQuickviewPageId(?string $quickviewPageId): void
    {
        $this->quickviewPageId = $quickviewPageId;
    }

    public function getProductListingDefaultPageId(): ?string
    {
        return $this->productListingDefaultPageId;
    }

    public function setProductListingDefaultPageId(?string $productListingDefaultPageId): void
    {
        $this->productListingDefaultPageId = $productListingDefaultPageId;
    }

    public function getAllowUserActionsForGuide(): bool
    {
        return $this->allowUserActionsForGuide;
    }

    public function setAllowUserActionsForGuide(bool $allowUserActionsForGuide): void
    {
        $this->allowUserActionsForGuide = $allowUserActionsForGuide;
    }

    private function setRunningState(): void
    {
        $this->running = $this->started && !$this->ended;
    }
}
