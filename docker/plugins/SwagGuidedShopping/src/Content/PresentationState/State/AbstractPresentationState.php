<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\State;

use Shopware\Core\Framework\Struct\Struct;

abstract class AbstractPresentationState extends Struct
{
    protected const EXPOSABLE_ANNOTATION = '@gs\mercure-update';

    protected string $hash;

    /**
     * @param array<string, mixed> $data
     */
    public function __construct(
        protected string $appointmentId,
        protected string $mercureTopic,
        array $data = []
    ) {
        $this->setFromArray($data);
        if (!\count($data)) {
            $this->hash = '';
        } else {
            $this->hash = $this->calculateHash();
        }
    }

    /**
     * @return array<string, mixed>
     */
    abstract public function getMercureUpdatePayload(): array;

    public function getAppointmentId(): string
    {
        return $this->appointmentId;
    }

    public function getMercureTopic(): string
    {
        return $this->mercureTopic;
    }

    /**
     * @return array<string, mixed>
     */
    final public function getData(): array
    {
        $data = [];
        $reflectionClass = new \ReflectionClass($this);
        $properties = $reflectionClass->getProperties();

        foreach ($properties as $property) {
            $propertyName = $property->getName();
            $key = $propertyName;

            if ($propertyName === 'extensions') {
                $extensions = $property->getValue($this);

                $data[$key] = [];
                if (\is_array($extensions)) {
                    foreach ($extensions as $extensionName => $extensionData) {
                        $data[$key][$extensionName] = $extensionData;
                    }
                    continue;
                }
            }

            if (
                !\is_string($property->getDocComment())
                || !\str_contains($property->getDocComment(), self::EXPOSABLE_ANNOTATION)
            ) {
                continue;
            }

            $value = $property->getValue($this);
            $data[$key] = $value;
        }

        return $data;
    }

    public function resetHash(): void
    {
        $this->hash = $this->calculateHash();
    }

    public function hasChanges(): bool
    {
        if ($this->hash !== $this->calculateHash()) {
            return true;
        }

        return false;
    }

    protected function calculateHash(): string
    {
        $json = \json_encode($this->getData(), \JSON_THROW_ON_ERROR);
        return \md5($json);
    }

    /**
     * @param array<string, mixed> $data
     */
    private function setFromArray(array $data): void
    {
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties();

        foreach ($properties as $property) {
            $propertyName = $property->getName();

            if (\array_key_exists($propertyName, $data)) {
                $this->{$propertyName} = $data[$propertyName];
            }
        }
    }
}
