<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\State;

class StateCollection
{
    public function __construct(
        private readonly StateForAll $stateForAll,
        private readonly StateForClients $stateForClients,
        private readonly StateForGuides $stateForGuides,
        private readonly StateForMe $stateForMe
    ) {
    }

    public function getStateForAll(): StateForAll
    {
        return $this->stateForAll;
    }

    public function getStateForClients(): StateForClients
    {
        return $this->stateForClients;
    }

    public function getStateForGuides(): StateForGuides
    {
        return $this->stateForGuides;
    }

    public function getStateForMe(): StateForMe
    {
        return $this->stateForMe;
    }
}
