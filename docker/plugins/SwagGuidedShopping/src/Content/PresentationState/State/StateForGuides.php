<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\State;

use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;

class StateForGuides extends AbstractPresentationState
{
    /**
     * @gs\mercure-update
     * @var array<string, array<string, mixed>>
     */
    protected array $clients = [];

    /**
     * @gs\mercure-update
     * @var array<string, array<string, mixed>>
     */
    protected array $inactiveClients = [];

    /**
     * @gs\mercure-update
     * @var array<string, array<string, mixed>>
     */
    protected array $guides = [];

    /** @gs\mercure-update */
    protected ?string $videoGuideToken = null;

    /**
     * @gs\mercure-update
     * @var array<string, mixed>
     */
    protected array $quickViewState = [];

    /** @gs\mercure-update */
    protected ?string $salesChannelId = null;

    public function resetClients(): void
    {
        $this->clients = [];
        $this->inactiveClients = [];
    }

    public function addClient(AttendeeEntity $attendee): void
    {
        $this->clients[$attendee->getId()] = $this->getAttendeeArray($attendee);
    }

    public function addInactiveClient(AttendeeEntity $attendee): void
    {
        $this->inactiveClients[$attendee->getId()] = $this->getAttendeeArray($attendee);
    }

    public function addGuide(AttendeeEntity $attendee): void
    {
        $this->guides[$attendee->getId()] = $this->getAttendeeArray($attendee);
    }

    public function setClientName(string $attendeeId, string $attendeeName): void
    {
        $this->clients[$attendeeId]['attendeeName'] = $attendeeName;
    }

    public function setClientVideoUserId(string $attendeeId, ?string $videoUserId): void
    {
        $this->clients[$attendeeId]['videoUserId'] = $videoUserId;
    }

    public function setGuideVideoUserId(string $attendeeId, ?string $videoUserId): void
    {
        $this->guides[$attendeeId]['videoUserId'] = $videoUserId;
    }

    public function setGuideCartPermissionsGranted(string $attendeeId, ?bool $granted): void
    {
        $this->clients[$attendeeId]['guideCartPermissionsGranted'] = $granted;
    }

    public function setGuideName(string $attendeeId, string $attendeeName): void
    {
        $this->guides[$attendeeId]['attendeeName'] = $attendeeName;
    }

    public function setVideoGuideToken(?string $videoGuideToken): void
    {
        $this->videoGuideToken = $videoGuideToken;
    }

    /**
     * @return array<string, mixed>
     */
    public function getMercureUpdatePayload(): array
    {
        return ['state-for-guides' => $this->getData()];
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public function getClients(): array
    {
        return $this->clients;
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public function getInactiveClients(): array
    {
        return $this->inactiveClients;
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public function getAllClients(): array
    {
        return \array_merge($this->clients, $this->inactiveClients);
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public function getGuides(): array
    {
        return $this->guides;
    }

    /**
     * @return array<string>
     */
    public function getAttendeeIdsOfClients(): array
    {
        return \array_keys($this->clients);
    }

    /**
     * @return array<string>
     */
    public function getAttendeeIdsOfInactiveClients(): array
    {
        return \array_keys($this->inactiveClients);
    }

    /**
     * @return array<string>
     */
    public function getAttendeeIdsForAllKindOfClients(): array
    {
        return \array_keys($this->getAllClients());
    }

    /**
     * @return array<string>
     */
    public function getAttendeeIdsOfGuides(): array
    {
        return \array_column($this->guides, 'attendeeId');
    }

    /**
     * @return array<string, array<string, mixed>>
     */
    public function setQuickViewStateForClient(string $productId, string $attendeeId, bool $state): array
    {
        if (!\array_key_exists($productId, $this->quickViewState)) {
            $this->quickViewState[$productId] = [];
        }

        $this->removeAttendeeFromAllQuickviews($attendeeId);
        if ($state) {
            $this->quickViewState[$productId][$attendeeId] = $attendeeId;

            return $this->quickViewState;
        }

        return $this->quickViewState;
    }

    public function removeAttendee(string $attendeeId): void
    {
        if (\array_key_exists($attendeeId, $this->clients)) {
            $this->inactiveClients[$attendeeId] = $this->clients[$attendeeId];
            unset($this->clients[$attendeeId]);
        }
    }

    public function removeInactiveAttendee(string $attendeeId): void
    {
        if (\array_key_exists($attendeeId, $this->inactiveClients)) {
            unset($this->inactiveClients[$attendeeId]);
        }
    }

    public function addAttendee(AttendeeEntity $attendee): void
    {
        if ($attendee->getType() === AttendeeDefinition::TYPE_GUIDE) {
            $this->addGuide($attendee);
            return;
        }

        $this->removeInactiveAttendee($attendee->getId());
        $this->addClient($attendee);
    }

    public function addInactiveAttendee(AttendeeEntity $attendee): void
    {
        if ($attendee->getType() === AttendeeDefinition::TYPE_GUIDE) {
            return;
        }

        $this->removeAttendee($attendee->getId());
        $this->addInactiveClient($attendee);
    }

    private function removeAttendeeFromAllQuickviews(string $attendeeeId): void
    {
        foreach ($this->quickViewState as $key => $quickview) {
            if (\array_key_exists($attendeeeId, $quickview)) {
                unset($this->quickViewState[$key][$attendeeeId]);
            }
        }
    }

    public function setSalesChannelId(?string $salesChannelId): void
    {
        $this->salesChannelId = $salesChannelId;
    }

    public function getSalesChannelId(): ?string
    {
        return $this->salesChannelId;
    }

    /**
     * @return array<string, mixed>
     */
    private function getAttendeeArray(AttendeeEntity $attendee): array
    {
        $attendeeData = [];
        $attendeeData['attendeeId'] = $attendee->getId();
        $attendeeData['attendeeName'] = $attendee->getAttendeeName() ?? $attendee->getAttendeeEmail();
        $attendeeData['videoUserId'] = $attendee->getVideoUserId();
        $attendeeData['guideCartPermissionsGranted'] = $attendee->isGuideCartPermissionsGranted();
        $attendeeData['hasJoined'] = $attendee->getType() === AttendeeDefinition::TYPE_CLIENT && $attendee->getAttendeeSubmittedAt();

        return $attendeeData;
    }
}
