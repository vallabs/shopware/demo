<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\State;

class StateForClients extends AbstractPresentationState
{
    /** @gs\mercure-update */
    protected ?string $videoClientToken = null;

    /** @gs\mercure-update */
    protected ?string $hoveredElementId = null;

    /**
     * @gs\mercure-update
     * @var array<string, array<string, string|null>>
     */
    protected array $clients = [];

    /**
     * @gs\mercure-update
     * @var array<string, array<string, string|null>>
     */
    protected array $guides = [];

    public function setVideoClientToken(?string $videoClientToken): void
    {
        $this->videoClientToken = $videoClientToken;
    }

    public function setHoveredElementId(?string $elementId): void
    {
        $this->hoveredElementId = $elementId;
    }

    public function storeParticipants(StateForGuides $stateForGuides): void
    {
        $data = $stateForGuides->getData();
        $this->clients = $data['clients'];
        $this->guides = $data['guides'];
    }

    public function getMercureUpdatePayload(): array
    {
        return ['state-for-clients' => $this->getData()];
    }
}
