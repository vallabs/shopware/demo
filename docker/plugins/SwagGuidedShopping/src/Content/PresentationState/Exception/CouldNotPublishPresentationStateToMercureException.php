<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Exception;

class CouldNotPublishPresentationStateToMercureException extends \Exception
{
}
