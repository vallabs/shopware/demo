<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class AttendeeMissingAppointmentException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct(
            \sprintf('Attendee missing the appointment.')
        );
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__ATTENDEE_MISSING_APPOINTMENT;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
