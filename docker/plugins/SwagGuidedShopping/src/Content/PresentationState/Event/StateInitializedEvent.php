<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Event;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\PresentationState\State\StateCollection;
use Symfony\Contracts\EventDispatcher\Event;

class StateInitializedEvent extends Event
{
    public function __construct(
        protected StateCollection $stateCollection,
        protected AppointmentEntity $appointment,
        protected Context $context
    ) {
    }

    public function getStateCollection(): StateCollection
    {
        return $this->stateCollection;
    }

    public function getAppointment(): AppointmentEntity
    {
        return $this->appointment;
    }

    public function getContext(): Context
    {
        return $this->context;
    }
}
