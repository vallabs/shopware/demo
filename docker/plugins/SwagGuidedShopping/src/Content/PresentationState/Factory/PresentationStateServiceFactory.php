<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Factory;

use Psr\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Mercure\Service\AbstractPublisher;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

class PresentationStateServiceFactory
{
    private ?PresentationStateService $presentationStateService = null;

    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly TagAwareAdapterInterface $cache,
        private readonly JWTMercureTokenFactory $JWTMercureTokenFactory,
        private readonly AbstractPublisher $mercurePublisher,
        private readonly TopicGenerator $topicGenerator,
        private readonly EntityRepository $appointmentRepository,
        private readonly SystemConfigService $config,
        private readonly EventDispatcherInterface $eventDispatcher
    ) {
    }

    public function build(string $appointmentId, Context $context): PresentationStateService
    {
        if (!$this->presentationStateService) {
            $topics = [
                $this->topicGenerator->generatePresentationStateForClientsPublisherTopic($appointmentId),
                $this->topicGenerator->generatePresentationStateForAllPublisherTopic($appointmentId),
                $this->topicGenerator->generatePresentationStateForGuidesPublisherTopic($appointmentId),
                $this->topicGenerator->generatePresentationStateForMePublisherTopic($appointmentId),
            ];

            $JWTMercureToken = $this->JWTMercureTokenFactory->createPublisherToken($topics);
            $this->presentationStateService = new PresentationStateService(
                $this->cache,
                $appointmentId,
                $this->mercurePublisher,
                $JWTMercureToken,
                $this->topicGenerator,
                $this->appointmentRepository,
                $context,
                $this->config,
                $this->eventDispatcher
            );
        }

        return $this->presentationStateService;
    }
}
