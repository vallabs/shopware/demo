<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Api;

use Shopware\Core\Framework\Api\Context\AdminApiSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Struct\ArrayStruct;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Content\PresentationState\Service\PresentationStateService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class GetGuidePresentationStateController extends AbstractController
{
    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly EntityRepository $attendeeRepository,
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/presentation/state', name: 'api.action.guided-shopping.presentation.state-guide', methods: ['GET'])]
    public function getState(string $appointmentId, Context $context): GetGuidePresentationStateResponse
    {
        $context->addExtension(PresentationStateService::SKIP_CACHE_CONTEXT_EXTENSION, new ArrayStruct([true]));
        $presentationStateService = $this->presentationStateServiceFactory->build($appointmentId, $context);

        $stateForMe = $presentationStateService->getStateForMe();
        $attendee = $this->getAttendee($appointmentId, $context);
        if ($attendee) {
            $stateForMe->setAttendee($attendee);
        }

        $state = new GetGuidePresentationStateStruct(
            $presentationStateService->getStateForAll(),
            $presentationStateService->getStateForGuides(),
            $stateForMe
        );

        return new GetGuidePresentationStateResponse($state);
    }

    private function getAttendee(string $appointmentId, Context $context): ?AttendeeEntity
    {
        /** @var AdminApiSource  $contextSource */
        $contextSource = $context->getSource();
        $userId = $contextSource->getUserId();

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('userId', $userId));
        $criteria->addFilter(new EqualsFilter('appointmentId', $appointmentId));

        /** @var AttendeeEntity|null $attendee */
        $attendee = $this->attendeeRepository->search($criteria, $context)->first();

        return $attendee;
    }
}
