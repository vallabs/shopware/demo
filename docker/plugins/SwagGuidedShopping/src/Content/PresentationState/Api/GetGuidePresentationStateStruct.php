<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Api;

use Shopware\Core\Framework\Struct\Struct;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetGuidePresentationStateStruct extends Struct
{
    /**
     * @var array<string, mixed>
     */
    protected array $stateForAll;

    /**
     * @var array<string, mixed>
     */
    protected array $stateForGuides;

    /**
     * @var array<string, mixed>
     */
    protected array $stateForMe;


    public function __construct(
        StateForAll $stateForAll,
        StateForGuides $stateForGuides,
        StateForMe $stateForMe)
    {
        $this->stateForAll = $stateForAll->getData();
        $this->stateForGuides = $stateForGuides->getData();
        $this->stateForMe = $stateForMe->getData();
    }
}
