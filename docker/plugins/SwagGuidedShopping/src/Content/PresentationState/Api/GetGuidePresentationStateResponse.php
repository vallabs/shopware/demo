<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Api;

use Shopware\Core\Framework\Api\Response\JsonApiResponse;

class GetGuidePresentationStateResponse extends JsonApiResponse
{
    public function __construct(GetGuidePresentationStateStruct $data)
    {
        parent::__construct($data);
    }
}
