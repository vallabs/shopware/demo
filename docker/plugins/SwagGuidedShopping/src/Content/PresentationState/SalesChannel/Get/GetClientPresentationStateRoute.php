<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\SalesChannel\Get;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Content\PresentationState\Exception\AttendeeMissingAppointmentException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class GetClientPresentationStateRoute extends AbstractGetClientPresentationStateRoute
{
    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    public function getDecorated(): AbstractGetClientPresentationStateRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/presentation/state', name: 'store-api.guided-shopping.presentation-state-client', defaults: ['attendee_required' => true], methods: ['GET'])]
    public function getState(SalesChannelContext $context): GetClientPresentationStateResponse
    {
        /** @var AttendeeEntity|null $attendee */
        $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);
        if (!$attendee) {
            throw new SalesChannelContextMissingAttendeeException();
        }

        /** @var string|null $appointmentId */
        $appointmentId = $attendee->getAppointmentId();
        if (!$appointmentId) {
            throw new AttendeeMissingAppointmentException();
        }

        $presentationStateService = $this->presentationStateServiceFactory->build($appointmentId, $context->getContext());
        $stateForMe = $presentationStateService->getStateForMe();
        $stateForMe->setAttendee($attendee);

        $state = new GetClientPresentationStateStruct(
            $presentationStateService->getStateForAll(),
            $presentationStateService->getStateForClients(),
            $stateForMe
        );

        return new GetClientPresentationStateResponse($state);
    }
}
