<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\SalesChannel\Get;

use Shopware\Core\System\SalesChannel\StoreApiResponse;

class GetClientPresentationStateResponse extends StoreApiResponse
{
    public function __construct(GetClientPresentationStateStruct $data)
    {
        parent::__construct($data);
    }
}
