<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\SalesChannel\Get;

use Shopware\Core\Framework\Struct\Struct;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;

class GetClientPresentationStateStruct extends Struct
{
    /**
     * @var array<string, mixed>
     */
    protected array $stateForAll;

    /**
     * @var array<string, mixed>
     */
    protected array $stateForClients;

    /**
     * @var array<string, mixed>
     */
    protected array $stateForMe;

    public function __construct(StateForAll $stateForAll, StateForClients $stateForClients, StateForMe $stateForMe)
    {
        $this->stateForAll = $stateForAll->getData();
        $this->stateForClients = $stateForClients->getData();
        $this->stateForMe = $stateForMe->getData();
    }
}
