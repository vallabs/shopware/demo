<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\SalesChannel\Get;

use Shopware\Core\System\SalesChannel\SalesChannelContext;

abstract class AbstractGetClientPresentationStateRoute
{
    abstract public function getDecorated(): AbstractGetClientPresentationStateRoute;

    abstract public function getState(
        SalesChannelContext $context
    ): GetClientPresentationStateResponse;
}
