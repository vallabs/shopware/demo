<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\PresentationState\Service;

use Psr\EventDispatcher\EventDispatcherInterface;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Mercure\Service\AbstractPublisher;
use SwagGuidedShopping\Content\Mercure\Service\TopicGenerator;
use SwagGuidedShopping\Content\PresentationState\Event\StateInitializedEvent;
use SwagGuidedShopping\Content\PresentationState\Event\StateReInitializedEvent;
use SwagGuidedShopping\Content\PresentationState\Exception\CouldNotPublishPresentationStateToMercureException;
use SwagGuidedShopping\Content\PresentationState\State\AbstractPresentationState;
use SwagGuidedShopping\Content\PresentationState\State\StateCollection;
use SwagGuidedShopping\Content\PresentationState\State\StateForAll;
use SwagGuidedShopping\Content\PresentationState\State\StateForClients;
use SwagGuidedShopping\Content\PresentationState\State\StateForGuides;
use SwagGuidedShopping\Content\PresentationState\State\StateForMe;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;
use SwagGuidedShopping\Struct\ConfigKeys;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;

class PresentationStateService
{
    public const CACHE_KEY_PREFIX = 'GS_APPOINTMENT_STATE_';
    public const SKIP_CACHE_CONTEXT_EXTENSION = 'SGS_SKIP_STATE_CACHE';
    public const OUTDATED_CLIENTS_INTERVAL = 'PT2M';

    protected ?StateCollection $stateCollection = null;

    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly TagAwareAdapterInterface $cache,
        private readonly string $appointmentId,
        private readonly AbstractPublisher $mercurePublisher,
        private readonly string $mercurePublisherToken,
        private readonly TopicGenerator $topicGenerator,
        private readonly EntityRepository $appointmentRepository,
        private readonly Context $context,
        private readonly SystemConfigService $config,
        private readonly EventDispatcherInterface $eventDispatcher
    ) {
        $this->loadStates($appointmentId, $context);
    }

    public function getStateForAll(): StateForAll
    {
        if (!$this->stateCollection) {
            throw new \Exception('States are not initialized.');
        }

        return $this->stateCollection->getStateForAll();
    }

    public function getStateForClients(): StateForClients
    {
        if (!$this->stateCollection) {
            throw new \Exception('States are not initialized.');
        }

        return $this->stateCollection->getStateForClients();
    }

    public function getStateForGuides(): StateForGuides
    {
        if (!$this->stateCollection) {
            throw new \Exception('States are not initialized.');
        }

        return $this->stateCollection->getStateForGuides();
    }

    public function getStateForMe(): StateForMe
    {
        if (!$this->stateCollection) {
            throw new \Exception('States are not initialized.');
        }

        return $this->stateCollection->getStateForMe();
    }

    public function publishStateForAll(): void
    {
        $state = $this->getStateForAll();
        if (!$state->hasChanges()) {
            return;
        }

        $this->save($state);
        $this->publishState($state);
    }

    public function publishStateForGuides(): void
    {
        $state = $this->getStateForGuides();
        if (!$state->hasChanges()) {
            return;
        }

        $this->save($state);
        $this->publishState($state);
    }

    public function publishStateForClients(): void
    {
        $state = $this->getStateForClients();
        if (!$state->hasChanges()) {
            return;
        }

        $this->save($state);
        $this->publishState($state);
    }

    public function publishStateForMe(): void
    {
        $state = $this->getStateForMe();
        if (!$state->hasChanges()) {
            return;
        }

        $this->save($state);
        $this->publishState($state);
    }

    public function loadStates(string $appointmentId, Context $context): void
    {
        if ($this->stateCollection) {
            throw new \Exception('States are already loaded.');
        }

        $appointment = null;

        if (!$this->stateExistsInCache() || $context->hasExtension(self::SKIP_CACHE_CONTEXT_EXTENSION)) {
            $criteria = new Criteria([$appointmentId]);
            $criteria->addAssociation('attendees');
            $criteria->addAssociation('attendees.user');
            $criteria->addAssociation('attendees.customer');

            /** @var AppointmentEntity|null $appointment */
            $appointment = $this->appointmentRepository->search($criteria, $context)->first();
            if (!$appointment) {
                throw new EntityNotFoundException(AppointmentEntity::class, $appointmentId);
            }

            if (!$this->stateExistsInCache()) {
                $this->initializeStates($appointment);
                return;
            }
        }

        $stateForAll = new StateForAll($appointmentId, $this->topicGenerator->generatePresentationStateForAllPublisherTopic($appointmentId), $this->readCacheData($this->getCacheKey(StateForAll::class)));
        $stateForGuides = new StateForGuides($appointmentId, $this->topicGenerator->generatePresentationStateForGuidesPublisherTopic($appointmentId), $this->readCacheData($this->getCacheKey(StateForGuides::class)));
        $stateForClients = new StateForClients($appointmentId, $this->topicGenerator->generatePresentationStateForClientsPublisherTopic($appointmentId), $this->readCacheData($this->getCacheKey(StateForClients::class)));
        $stateForMe = new StateForMe($appointmentId, $this->topicGenerator->generatePresentationStateForMePublisherTopic($appointmentId), $this->readCacheData($this->getCacheKey(StateForMe::class)));

        $this->stateCollection = new StateCollection($stateForAll, $stateForClients, $stateForGuides, $stateForMe);

        if ($context->hasExtension(self::SKIP_CACHE_CONTEXT_EXTENSION)) {
            /**
             * no need to check if the appointment is null here
             * because the appointment is loaded from the database if the cache is not exist or skipped.
             *
             * @phpstan-ignore-next-line
             */
            $this->reInitializeStates($appointment);
        }
    }

    public function initializeStates(AppointmentEntity $appointment): void
    {
        if ($appointment->getId() !== $this->appointmentId) {
            throw new \Exception('Appointment id from service must match the appointment id from the entity.');
        }

        $appointmentId = $appointment->getId();
        $stateForAll = new StateForAll($appointmentId, $this->topicGenerator->generatePresentationStateForAllPublisherTopic($appointmentId), []);
        $stateForGuides = new StateForGuides($appointmentId, $this->topicGenerator->generatePresentationStateForGuidesPublisherTopic($appointmentId), []);
        $stateForClients = new StateForClients($appointmentId, $this->topicGenerator->generatePresentationStateForClientsPublisherTopic($appointmentId), []);
        $stateForMe = new StateForMe($appointmentId, $this->topicGenerator->generatePresentationStateForMePublisherTopic($appointmentId), []);

        $stateForAll->setAppointmentState($appointment);
        $stateForAll->setAttendeeRestrictionType($appointment->getAttendeeRestrictionType());
        $stateForAll->setAppointmentMode($appointment->getMode());
        $stateForAll->setAccessibleTo($appointment->getAccessibleTo());
        $stateForAll->setAccessibleFrom($appointment->getAccessibleFrom());
        $stateForAll->setProductDetailDefaultPageId($this->config->getString(ConfigKeys::PRODUCT_DETAIL_DEFAULT_PAGE_ID));
        $stateForAll->setProductListingDefaultPageId($this->config->getString(ConfigKeys::PRODUCT_LISTING_DEFAULT_PAGE_ID));
        $stateForAll->setQuickviewPageId($this->config->getString(ConfigKeys::QUICK_VIEW_PAGE_ID));
        $stateForAll->setAllowUserActionsForGuide($this->config->getBool(ConfigKeys::ALLOW_USER_ACTIONS_FOR_GUIDE));
        $stateForAll->setVideoAudioSettings($appointment->getVideoAudioSettings());
        $stateForGuides->setSalesChannelId($appointment->getSalesChannelDomain()?->getSalesChannelId());

        if (
            !$appointment->isPreview()
            && $appointment->getMode() === AppointmentDefinition::MODE_GUIDED
            && $appointment->getVideoAudioSettings() !== AppointmentDefinition::NONE_VIDEO_AUDIO
        ) {
            if (!$appointment->getVideoChat() instanceof VideoChatEntity) {
                throw new VideoChatEntityDoesNotExist($appointment->getId());
            }

            $videoChat = $appointment->getVideoChat();
            if ($videoChat->getUrl()) {
                $stateForAll->setVideoRoomUrl($videoChat->getUrl());
                $stateForAll->setBroadcastMode($videoChat->isStartAsBroadcast());
                $stateForClients->setVideoClientToken($videoChat->getUserToken());
                $stateForGuides->setVideoGuideToken($videoChat->getOwnerToken());
            }
        }

        $this->stateCollection = new StateCollection($stateForAll, $stateForClients, $stateForGuides, $stateForMe);
        $this->updateAttendees($appointment->getAttendees(), $stateForGuides, $stateForClients);

        if ($this->stateCollection) {
            $this->eventDispatcher->dispatch(new StateInitializedEvent($this->stateCollection, $appointment, $this->context));
        }

        $this->publishStateForAll();
        $this->publishStateForClients();
        $this->publishStateForGuides();
        $this->publishStateForMe();
    }

    public function reInitializeStates(AppointmentEntity $appointment): void
    {
        if ($appointment->getId() !== $this->appointmentId) {
            throw new \Exception('Appointment id from service must match the appointment id from the entity.');
        }

        $stateForGuides = $this->getStateForGuides();
        $stateForClients = $this->getStateForClients();
        $this->updateAttendees($appointment->getAttendees(), $stateForGuides, $stateForClients);

        if ($this->stateCollection) {
            $this->eventDispatcher->dispatch(new StateReInitializedEvent($this->stateCollection, $appointment, $this->context));
        }

        $this->publishStateForAll();
        $this->publishStateForClients();
        $this->publishStateForGuides();
        $this->publishStateForMe();
    }

    public function updateAttendees(AttendeeCollection $attendees, StateForGuides $stateForGuides, StateForClients $stateForClients): void
    {
        $stateForGuides->resetClients();

        $diffDate = new \DateTime();
        $diffDate->sub(new \DateInterval(self::OUTDATED_CLIENTS_INTERVAL));

        /** @var AttendeeEntity $attendee */
        foreach ($attendees as $attendee) {
            if (
                $attendee->getType() === AttendeeDefinition::TYPE_GUIDE
                || ($attendee->getLastActive() && $attendee->getLastActive()->getTimestamp() > $diffDate->getTimestamp())
            ) {
                $stateForGuides->addAttendee($attendee);
                $stateForClients->storeParticipants($stateForGuides);
            } else {
                $stateForGuides->addInactiveClient($attendee);
            }
        }
    }

    private function save(AbstractPresentationState $state): void
    {
        $this->writeToCache($this->getCacheKey(\get_class($state)), $state->getData());
        $state->resetHash();
    }

    private function publishState(AbstractPresentationState $state): void
    {
        try {
            $this->mercurePublisher->publish($this->mercurePublisherToken, $state->getMercureUpdatePayload(), $state->getMercureTopic());
        } catch (\Exception $e) {
            throw new CouldNotPublishPresentationStateToMercureException($e->getMessage());
        }
    }

    private function stateExistsInCache(): bool
    {
        $cacheForAll = $this->readCacheData($this->getCacheKey(StateForAll::class));
        $cacheForGuides = $this->readCacheData($this->getCacheKey(StateForGuides::class));
        $cacheForClients = $this->readCacheData($this->getCacheKey(StateForClients::class));
        $cacheForMe = $this->readCacheData($this->getCacheKey(StateForMe::class));
        if (!\count($cacheForAll)
            && !\count($cacheForClients)
            && !\count($cacheForGuides)
            && !\count($cacheForMe)
        ) {
            return false;
        }

        return true;
    }

    private function getCacheKey(string $state): string
    {
        return self::CACHE_KEY_PREFIX . '_' . \md5($state) . '_' . $this->appointmentId;
    }

    /**
     * @return array<string, mixed>
     */
    private function readCacheData(string $cacheKey): array
    {
        $item = $this->cache->getItem($cacheKey);
        if ($item->isHit()) {
            $data = $item->get();
        } else {
            $data = [];
        }

        return $data;
    }

    /**
     * @param array<int|string, mixed> $data
     */
    private function writeToCache(string $itemKey, array $data): void
    {
        $item = $this->cache->getItem($itemKey);
        $item->set($data);
        $item->expiresAfter(new \DateInterval('P10D'));
        $this->cache->save($item);
    }
}
