<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Token;

use Firebase\JWT\JWT;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Mercure\Exception\JwtMercureInvalidTopicInTokenException;
use SwagGuidedShopping\Exception\NotConfiguredException;
use SwagGuidedShopping\Struct\ConfigKeys;
use Symfony\Component\Mercure\Jwt\TokenFactoryInterface;

class JWTMercureTokenFactory implements TokenFactoryInterface
{
    private ?string $privatePublisherSecret = null;

    private ?string $privateSubscriberSecret = null;

    public function __construct(SystemConfigService $configService)
    {
        $publisherSecret = $configService->getString(ConfigKeys::MERCURE_HUB_PUBLISHER_SECRET);
        $subscriberSecret = $configService->getString(ConfigKeys::MERCURE_HUB_SUBSCRIBER_SECRET);

        if (\mb_strlen(\trim($publisherSecret))) {
            $this->privatePublisherSecret = $publisherSecret;
        }

        if (\mb_strlen(\trim($subscriberSecret))) {
            $this->privateSubscriberSecret = $subscriberSecret;
        }
    }

    /**
     * @param array<string>|null $subscribe
     * @param array<string>|null $publish
     * @param array<int|string, mixed> $additionalClaims
     */
    public function create(?array $subscribe = [], ?array $publish = [], array $additionalClaims = []): string
    {
        /**
         * @var array<string> $subscribe
         * @var array<string> $publish
         */
        if (\count($subscribe) && \count($publish)) {
            throw new JwtMercureInvalidTopicInTokenException('You are not allowed to define subscriber and publisher topics in one token.');
        }

        if (\count($subscribe)) {
            return $this->createSubscriberToken($subscribe);
        }

        if (\count($publish)) {
            return $this->createPublisherToken($publish);
        }

        throw new JwtMercureInvalidTopicInTokenException('You must set at least one subscriber or publisher topic.');
    }

    /**
     * @param array<string> $publish
     */
    public function createPublisherToken(array $publish): string
    {
        if (!$this->privatePublisherSecret) {
            throw new NotConfiguredException('No mercure hub Publisher secret configured.');
        }

        $payload = $this->generatePayload([], $publish);

        return JWT::encode($payload, $this->privatePublisherSecret, 'HS256');
    }

    /**
     * @param array<string> $subscribe
     */
    public function createSubscriberToken(array $subscribe): string
    {
        if (!$this->privateSubscriberSecret) {
            throw new NotConfiguredException('No mercure hub Subscriber secret configured.');
        }
        
        $payload = $this->generatePayload($subscribe, []);

        return JWT::encode($payload, $this->privateSubscriberSecret, 'HS256');
    }

    /**
     * @param array<string> $subscriberTopics
     * @param array<string> $publisherTopics
     * @return array<string, mixed>
     */
    private function generatePayload(array $subscriberTopics, array $publisherTopics): array
    {
        $payload = [];
        $payload['mercure']['subscribe'] = $subscriberTopics;
        $payload['mercure']['publish'] = $publisherTopics;

        return $payload;
    }
}
