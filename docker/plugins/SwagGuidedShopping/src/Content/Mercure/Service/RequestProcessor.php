<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Service;

use SwagGuidedShopping\Content\Mercure\Exception\MercurePublishViolationException;
use SwagGuidedShopping\Framework\PlatformRequest;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class RequestProcessor
{
    public function __construct(
        private readonly AbstractPublisher $publisher,
        private readonly DataValidator $validator
    ) {
    }

    public function process(Request $request): string
    {
        $requestBody = $request->getContent();
        $parameters = \json_decode($requestBody, true);
        $mercureToken = (string) $request->headers->get(PlatformRequest::GUIDED_SHOPPING_MERCURE_TOKEN);
        $parameters[PlatformRequest::GUIDED_SHOPPING_MERCURE_TOKEN] = $mercureToken;
        $this->validateRequestParameters($parameters);

        $update = $parameters['update'];
        $topic = $parameters['topic'];

        return $this->publisher->publish($mercureToken, $update, $topic);
    }

    /**
     * @param array<string, mixed> $parameters
     */
    protected function validateRequestParameters(array $parameters): void
    {
        $constraint = new Collection(
            [
                'update' => new Type('array'),
                'topic' => [new Type('string'), new NotBlank()],
                PlatformRequest::GUIDED_SHOPPING_MERCURE_TOKEN => [new Type('string'), new NotBlank()],
            ]
        );

        $constraint->allowExtraFields = true;
        $violations = $this->validator->validate($parameters, $constraint);

        if ($violations->count()) {
            throw new MercurePublishViolationException($violations);
        }
    }
}
