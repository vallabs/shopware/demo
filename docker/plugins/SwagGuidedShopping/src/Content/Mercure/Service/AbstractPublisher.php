<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Service;

abstract class AbstractPublisher
{
    /**
     * @param array<int|string, mixed> $updateData
     */
    abstract public function publish(string $mercurePublisherToken, array $updateData, string $topic): string;
}
