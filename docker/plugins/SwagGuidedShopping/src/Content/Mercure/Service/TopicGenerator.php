<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Service;

use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class TopicGenerator
{
    /**
     * @return array<string>
     */
    public function generateGuideSubscriberTopics(string $appointmentId, string $appointmentMode): array
    {
        if ($appointmentMode === AppointmentDefinition::MODE_SELF) {
            return [];
        }

        return [
            'gs-client-actions-' . $appointmentId,
            'gs-guide-actions-' . $appointmentId,
            $this->generatePresentationStateForGuidesPublisherTopic($appointmentId),
            $this->generatePresentationStateForAllPublisherTopic($appointmentId),
        ];
    }

    public function generateGuidePublisherTopic(string $appointmentId, string $appointmentMode): ?string
    {
        if ($appointmentMode === AppointmentDefinition::MODE_SELF) {
            return null;
        }

        return 'gs-guide-actions-' . $appointmentId;
    }

    /**
     * @return array<string>
     */
    public function generateClientSubscriberTopics(string $appointmentId, string $appointmentMode): array
    {
        if ($appointmentMode === AppointmentDefinition::MODE_SELF) {
            return [];
        }

        return [
            'gs-guide-actions-' . $appointmentId,
            $this->generatePresentationStateForClientsPublisherTopic($appointmentId),
            $this->generatePresentationStateForAllPublisherTopic($appointmentId),
        ];
    }

    public function generateClientPublisherTopic(string $appointmentId, string $appointmentMode): ?string
    {
        if ($appointmentMode === AppointmentDefinition::MODE_SELF) {
            return null;
        }

        return 'gs-client-actions-' . $appointmentId;
    }

    public function generatePresentationStateForGuidesPublisherTopic(string $appointmentId): string
    {
        return 'gs-presentation-state-for-guide-' . $appointmentId;
    }

    public function generatePresentationStateForAllPublisherTopic(string $appointmentId): string
    {
        return 'gs-presentation-state-for-all-' . $appointmentId;
    }

    public function generatePresentationStateForClientsPublisherTopic(string $appointmentId): string
    {
        return 'gs-presentation-state-for-client-' . $appointmentId;
    }

    public function generatePresentationStateForMePublisherTopic(string $appointmentId): string
    {
        return 'gs-presentation-state-for-me-' . $appointmentId;
    }
}
