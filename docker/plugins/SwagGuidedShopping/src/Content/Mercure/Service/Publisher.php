<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Service;

use SwagGuidedShopping\Content\Mercure\Factory\HubFactory;
use Symfony\Component\Mercure\Update;

class Publisher extends AbstractPublisher
{
    public function __construct(
        private readonly HubFactory $hubFactory
    ) {
    }

    /**
     * @param array<int|string, mixed> $updateData
     */
    public function publish(string $mercurePublisherToken, array $updateData, string $topic): string
    {
        $hub = $this->hubFactory->build($mercurePublisherToken)->getHub();

        $updateData = \json_encode($updateData);
        if (!\is_string($updateData)) {
            throw new \Exception('Invalid update payload.');
        }

        $update = new Update($topic, $updateData, true);

        return $hub->publish($update);
    }
}
