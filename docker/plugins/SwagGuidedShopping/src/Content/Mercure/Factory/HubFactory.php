<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Factory;

use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Mercure\Struct\JwtProvider;
use SwagGuidedShopping\Content\Mercure\Token\JWTMercureTokenFactory;
use SwagGuidedShopping\Exception\NotConfiguredException;
use SwagGuidedShopping\Struct\ConfigKeys;
use Symfony\Component\Mercure\Hub;
use Symfony\Component\Mercure\HubRegistry;

class HubFactory
{
    private ?string $hubUrl = null;

    private ?string $hubUrlPublic = null;

    public function __construct(
        SystemConfigService $systemConfigService,
        private readonly JWTMercureTokenFactory $jwtMercureTokenFactory
    ) {
        $this->setVariablesFromSystemConfig($systemConfigService);
    }

    public function build(string $jwtToken): HubRegistry
    {
        if (!$this->hubUrl) {
            throw new NotConfiguredException('No mercure hub url configured.');
        }

        if (!$this->hubUrlPublic) {
            throw new NotConfiguredException('No mercure hub public url configured.');
        }

        $hub = new Hub($this->hubUrl, new JwtProvider($jwtToken), $this->jwtMercureTokenFactory, $this->hubUrlPublic);

        return new HubRegistry($hub);
    }

    private function setVariablesFromSystemConfig(SystemConfigService $systemConfigService): void
    {
        $hubUrl = $systemConfigService->getString(ConfigKeys::MERCURE_HUB_URL);
        $hubPublicUrl = $systemConfigService->getString(ConfigKeys::MERCURE_HUB_PUBLIC_URL);

        if (\mb_strlen(\trim($hubUrl))) {
            $this->hubUrl = $hubUrl;
        }

        if (\mb_strlen(\trim($hubPublicUrl))) {
            $this->hubUrlPublic = $hubPublicUrl;
        }
    }
}
