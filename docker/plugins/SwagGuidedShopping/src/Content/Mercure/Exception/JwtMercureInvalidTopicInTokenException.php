<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Exception;

class JwtMercureInvalidTopicInTokenException extends \Exception
{
}
