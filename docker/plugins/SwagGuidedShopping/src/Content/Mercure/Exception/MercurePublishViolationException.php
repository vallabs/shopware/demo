<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class MercurePublishViolationException extends ShopwareHttpException
{
    public function __construct(ViolationList $violationsCollection)
    {
        $message = "Could not publish mercure update \n";
        $message .= $violationsCollection;
        parent::__construct($message);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__MERCURE_PUBLISH_VIOLATION;
    }
}
