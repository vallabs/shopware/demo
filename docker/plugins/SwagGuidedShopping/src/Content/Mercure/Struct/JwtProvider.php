<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Mercure\Struct;

use Symfony\Component\Mercure\Jwt\TokenProviderInterface;

final class JwtProvider implements TokenProviderInterface
{
    public function __construct(
        private readonly string $token
    ) {
    }

    public function getJwt(): string
    {
        return $this->token;
    }
}
