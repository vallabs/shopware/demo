<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class ProductPageLoadInvalidRequestParameter extends ShopwareHttpException
{
    public function __construct(ViolationList $violationsCollection)
    {
        $message = "There are the following parameter errors in the request body \n";
        $message .= $violationsCollection;
        parent::__construct($message);
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__PRODUCT_PAGE_LOAD_INVALID_REQUEST_PARAMETER;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
