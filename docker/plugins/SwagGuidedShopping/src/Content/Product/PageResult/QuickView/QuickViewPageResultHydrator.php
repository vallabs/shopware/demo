<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\PageResult\QuickView;

use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use SwagGuidedShopping\Content\Product\PageResult\ProductPageResult;
use SwagShopwarePwa\Pwa\PageResult\AbstractPageResultHydrator;

/**
 * This is a helper class which strips down fields in the response and assembles the product page result.
 * It's really more of a preprocessor than a hydrator to be exact.
 *
 * It seems reasonable to create an interface for hydrators, however there is no common input format for them other than a custom struct.
 * Don't want to over-engineer here.
 */
class QuickViewPageResultHydrator extends AbstractPageResultHydrator
{
    public function hydrate(SalesChannelProductEntity $product, ?PropertyGroupCollection $configurator): ProductPageResult
    {
        $pageResult = new ProductPageResult();

        $pageResult->setProduct($product);

        if ($configurator) {
            $pageResult->setConfigurator($configurator);
        }

        $pageResult->setCmsPage($product->getCmsPage());

        // As cmsPage is already part of the response, reset the one attached to the product.
        $pageResult->getProduct()->setCmsPage(new CmsPageEntity());

        return $pageResult;
    }
}
