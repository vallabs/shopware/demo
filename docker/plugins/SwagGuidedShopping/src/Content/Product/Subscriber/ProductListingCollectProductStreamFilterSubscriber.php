<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\Subscriber;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\ProductStream\Service\ProductStreamBuilder;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

class ProductListingCollectProductStreamFilterSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly ProductStreamBuilder $productStreamBuilder
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ProductListingCriteriaEvent::class => 'addStreamFilters',
        ];
    }

    public function addStreamFilters(ProductListingCriteriaEvent $event): void
    {
        $criteria = $event->getCriteria();
        $streamIds = $this->getProductStreamIds($event->getRequest());

        if (!\count($streamIds)) {
            return;
        }

        $streamFilters = [];
        foreach ($streamIds as $streamId) {
            $streamFilters = \array_merge($streamFilters, $this->productStreamBuilder->buildFilters(
                $streamId,
                $event->getContext()
            ));
        }

        if (!\count($streamFilters)) {
            return;
        }

        $streamFilter = new MultiFilter(MultiFilter::CONNECTION_OR, $streamFilters);
        $criteria->addFilter($streamFilter);
    }

    /**
     * @param Request $request
     * @return array<int|string, mixed>
     */
    private function getProductStreamIds(Request $request): array
    {
        $ids = $request->query->get('streams', '');
        if ($request->isMethod(Request::METHOD_POST)) {
            $ids = $request->request->get('streams', '');
        }

        if (\is_string($ids)) {
            $ids = \explode('|', $ids);
        }

        return \array_filter((array) $ids);
    }
}
