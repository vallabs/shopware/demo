<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel\QuickView;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Product\SalesChannel\PageLoaderService;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagShopwarePwa\Pwa\Response\CmsPageRouteResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class QuickViewRoute extends AbstractQuickViewRoute
{
    public function __construct(
        private readonly SystemConfigService $config,
        private readonly PageLoaderService $pageLoaderService
    ) {
    }

    public function getDecorated(): AbstractQuickViewRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/quickview/{productId}/{cmsPageLayoutId}', name: 'store-api.guided-shopping.quickview', defaults: ['attendee_required' => true], methods: ['GET'])]
    public function load(string $productId, string $cmsPageLayoutId, Request $request, SalesChannelContext $context): CmsPageRouteResponse
    {
        /** @var string $configuredLayoutId */
        $configuredLayoutId = $this->config->get(ConfigKeys::QUICK_VIEW_PAGE_ID);

        $request->request->set('productId', $productId);
        $request->request->set('cmsPageLayoutId', $cmsPageLayoutId);

        $pageResult = $this->pageLoaderService->load($configuredLayoutId, $request, $context);

        return new CmsPageRouteResponse($pageResult);
    }
}
