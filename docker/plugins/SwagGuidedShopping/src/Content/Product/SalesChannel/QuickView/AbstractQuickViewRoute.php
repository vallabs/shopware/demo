<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel\QuickView;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagShopwarePwa\Pwa\Response\CmsPageRouteResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractQuickViewRoute
{
    abstract public function getDecorated(): AbstractQuickViewRoute;

    abstract public function load(string $productId, string $cmsPageLayoutId, Request $request, SalesChannelContext $context): CmsPageRouteResponse;
}
