<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel\Listing;

use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingRouteResponse;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractProductListingRoute
{
    abstract public function getDecorated(): AbstractProductListingRoute;

    abstract public function load(Request $request, SalesChannelContext $context, Criteria $criteria): ProductListingRouteResponse;
}
