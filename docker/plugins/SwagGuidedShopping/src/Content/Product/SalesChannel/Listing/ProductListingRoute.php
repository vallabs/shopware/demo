<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel\Listing;

use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\Product\Events\ProductListingResultEvent;
use Shopware\Core\Content\Product\Events\ProductSearchCriteriaEvent;
use Shopware\Core\Content\Product\Events\ProductSuggestCriteriaEvent;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEvents;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingLoader;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingResult;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingRouteResponse;
use Shopware\Core\Content\Product\SalesChannel\ProductAvailableFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Grouping\FieldGrouping;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Event\AfterProductListingLoadedEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class ProductListingRoute extends AbstractProductListingRoute
{
    /**
     * @param SalesChannelRepository<ProductCollection> $productRepository
     */
    public function __construct(
        private readonly ProductListingLoader     $listingLoader,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly SalesChannelRepository   $productRepository
    )
    {
    }

    public function getDecorated(): AbstractProductListingRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/product-listing', name: 'store-api.guided-shopping.product.listing', defaults: ['_entity' => 'product', 'attendee_required' => true], methods: ['POST'])]
    public function load(Request $request, SalesChannelContext $context, Criteria $criteria): ProductListingRouteResponse
    {
        $this->eventDispatcher->dispatch(
            new ProductListingCriteriaEvent($request, $criteria, $context)
        );

        $this->eventDispatcher->dispatch(
            new ProductSuggestCriteriaEvent($request, $criteria, $context),
            ProductEvents::PRODUCT_SUGGEST_CRITERIA
        );

        $criteria->addFilter(new ProductAvailableFilter($context->getSalesChannel()->getId()));

        $this->addFilterAggregations($request, $criteria, $context);

        $originalIds = $criteria->getIds(); // save original ids for later use (e.g. for pagination)

        $useIdSorting = $request->request->get('useIdSorting', false);

        if ($useIdSorting && \count($originalIds)) {
            $criteria->resetSorting();

            $page = $request->request->getInt('p', 1);

            $this->chunkSortedIds($criteria, $page);
        }

        $loadVariants = $request->request->get('loadVariants', false);

        if ($loadVariants) {
            $criteria->addAssociation('options.group');

            $entities = $this->productRepository->search($criteria, $context);
        } else {
            $entities = $this->listingLoader->load($criteria, $context);
        }

        if ($request->request->get('interaction', false)) {
            $this->eventDispatcher->dispatch(
                new AfterProductListingLoadedEvent($entities, $context)
            );
        }

        /**
         * Turn back to the original ids to get the correct total count
         * in case the original ids is chunked for pagination.
         */
        if ($useIdSorting && \count($originalIds)) {
            $returnedEntities = new EntitySearchResult(
                $entities->getEntity(),
                \count($originalIds),
                $entities->getEntities(),
                $entities->getAggregations(),
                $entities->getCriteria()->setIds($originalIds),
                $entities->getContext()
            );
        }

        $result = ProductListingResult::createFrom($returnedEntities ?? $entities);
        $result->addState(...$entities->getStates());

        if ($request->request->get('loadAllIds', false)) {
            $allIds = $this->getAllIds($criteria, $context, $loadVariants);
            $result->addArrayExtension('allIds', ['total' => $result->getTotal(), 'data' => $allIds]);
        }

        $this->eventDispatcher->dispatch(
            new ProductListingResultEvent($request, $result, $context)
        );

        return new ProductListingRouteResponse($result);
    }

    private function addFilterAggregations(Request $request, Criteria $criteria, SalesChannelContext $context): void
    {
        $this->eventDispatcher->dispatch(
            new ProductSearchCriteriaEvent($request, $criteria, $context),
            ProductEvents::PRODUCT_SEARCH_CRITERIA
        );
    }

    private function addGroupings(Criteria $criteria): void
    {
        $criteria->addGroupField(new FieldGrouping('displayGroup'));

        $criteria->addFilter(
            new NotFilter(
                MultiFilter::CONNECTION_AND,
                [new EqualsFilter('displayGroup', null)]
            )
        );
    }

    /**
     * @return list<string>|list<array<string, string>>
     */
    private function getAllIds(Criteria $origin, SalesChannelContext $context, mixed $loadVariants): array
    {
        $criteria = clone $origin;

        if ($loadVariants === false) {
            $this->addGroupings($criteria);
        }

        $criteria->setLimit(null);

        $result = $this->productRepository->searchIds($criteria, $context);

        return $result->getIds();
    }

    /**
     * Divide the sorted ids into chunks and return the chunk for the current page.
     */
    private function chunkSortedIds(Criteria $criteria, int $page): void
    {
        $ids = $criteria->getIds();
        $limit = $criteria->getLimit();

        $offset = $limit * ($page - 1);
        $filteredProductIds = \array_splice($ids, $offset, $limit);

        $criteria->setIds($filteredProductIds);

        // reset offset to 0 to get the correct total count
        $criteria->setOffset(0);
    }
}
