<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel\ProductDetailPage;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagShopwarePwa\Pwa\Response\CmsPageRouteResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractProductDetailPageRoute
{
    abstract public function getDecorated(): AbstractProductDetailPageRoute;

    abstract public function load(string $productId, Request $request, SalesChannelContext $context): CmsPageRouteResponse;
}
