<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel\ProductDetailPage;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Product\SalesChannel\PageLoaderService;
use SwagGuidedShopping\Struct\ConfigKeys;
use SwagShopwarePwa\Pwa\Response\CmsPageRouteResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class ProductDetailPageRoute extends AbstractProductDetailPageRoute
{
    public function __construct(
        private readonly SystemConfigService $config,
        private readonly PageLoaderService $pageLoaderService
    ) {
    }

    public function getDecorated(): AbstractProductDetailPageRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/product/{productId}', name: 'store-api.guided-shopping.product-detail-page', defaults: ['attendee_required' => true], methods: ['GET'])]
    public function load(string $productId, Request $request, SalesChannelContext $context): CmsPageRouteResponse
    {
        /** @var string $configuredLayoutId */
        $configuredLayoutId = $this->config->get(ConfigKeys::PRODUCT_DETAIL_DEFAULT_PAGE_ID);

        $request->request->set('productId', $productId);
        // add param useProductLayout to request for using product layout if configured
        $request->request->set('useProductLayout', true);

        $pageResult = $this->pageLoaderService->load($configuredLayoutId, $request, $context);

        return new CmsPageRouteResponse($pageResult);
    }
}
