<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\SalesChannel;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\Detail\AbstractProductDetailRoute;
use Shopware\Core\Content\Product\SalesChannel\Detail\ProductDetailRouteResponse;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Exception\InvalidParameterException;
use SwagGuidedShopping\Content\Presentation\Event\AfterLoadProductConfiguratorEvent;
use SwagGuidedShopping\Content\Product\Exception\ProductPageLoadInvalidRequestParameter;
use SwagGuidedShopping\Content\Product\PageResult\ProductPageResult;
use SwagGuidedShopping\Content\Product\PageResult\QuickView\QuickViewPageResultHydrator;
use SwagGuidedShopping\Service\Validator\DataValidator;
use SwagShopwarePwa\Pwa\PageResult\AbstractPageResult;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class PageLoaderService
{
    public function __construct(
        private readonly DataValidator                      $dataValidator,
        private readonly AbstractProductDetailRoute         $productDetailRoute,
        private readonly ProductDefinition                  $productDefinition,
        private readonly SalesChannelCmsPageLoaderInterface $cmsPageLoader,
        private readonly NotesElementHandler                $notesElementHandler,
        private readonly EventDispatcherInterface           $eventDispatcher,
        private readonly QuickViewPageResultHydrator        $resultHydrator,
    ) {
    }

    public function load(
        string $configuredLayoutId,
        Request $request,
        SalesChannelContext $context
    ): ProductPageResult
    {
        $this->validateRequestBody($request->request->all());

        $result = $this->loadProduct($request, $context);

        $product = $result->getProduct();

        $resolverContext = new EntityResolverContext($context, $request, $this->productDefinition, $product);

        $cmsPageLayoutId = $request->request->get('cmsPageLayoutId');

        $useProductLayout = $request->request->get('useProductLayout', false);

        /**
         * Use the product layout if the product has a cms page id and the useProductLayout flag is set
         * or use the configured layout id if the cmsPageLayoutId is not provided
         *
         * @var array<string> $ids
         */
        $ids = $useProductLayout && $product->getCmsPageId() ? [$product->getCmsPageId()] : [$cmsPageLayoutId ?: $configuredLayoutId];

        /** @var CmsPageEntity $cmsPage */
        $cmsPage = $this->cmsPageLoader->load(
            $request,
            new Criteria($ids),
            $context,
            null,
            $resolverContext
        )->first();

        $sections = $cmsPage->getSections();

        if ($sections instanceof CmsSectionCollection) {
            /** @var CmsSectionEntity $section */
            foreach ($sections as $section) {
                $this->notesElementHandler->removeNotes($section);
            }
        }

        $result->getProduct()->setCmsPage($cmsPage);

        $this->eventDispatcher->dispatch(
            new AfterLoadProductConfiguratorEvent($result->getConfigurator(), $context)
        );

        return $this->resultHydrator->hydrate($result->getProduct(), $result->getConfigurator());
    }

    private function loadProduct(Request $request, SalesChannelContext $context): ProductDetailRouteResponse
    {
        /** @var string|null $productId */
        $productId = $request->request->get('productId');

        if ($productId === null) {
            throw new RoutingException(
                Response::HTTP_BAD_REQUEST,
                'MISSING_REQUIRED_PARAMETER',
                'productId is required'
            );
        }

        $criteria = new Criteria();
        $criteria->addAssociation('media');
        $criteria->addAssociation('properties.group');
        $criteria->addAssociation('productReviews');
        $criteria->addAssociation('manufacturer.media');

        return $this->productDetailRoute->load($productId, $request, $context, $criteria);
    }

    /**
     * @param array<string, mixed> $payload
     */
    private function validateRequestBody(array $payload): void
    {
        $constraint = new Collection(
            [
                'productId' => [new Type('string')],
                'cmsPageLayoutId' => [new Type('string')],
            ]
        );

        $constraint->allowMissingFields = true;

        $violations = $this->dataValidator->validate($payload, $constraint);

        if ($violations->count()) {
            throw new ProductPageLoadInvalidRequestParameter($violations);
        }
    }
}
