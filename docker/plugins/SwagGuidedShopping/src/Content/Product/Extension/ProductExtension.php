<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\Extension;

use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityExtension;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\Collection\AttendeeProductCollection\AttendeeProductCollectionDefinition;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;

class ProductExtension extends EntityExtension
{
    public function extendFields(FieldCollection $collection): void
    {
        $collection->add(
            (new OneToManyAssociationField(
                'attendeeProductCollections',
                AttendeeProductCollectionDefinition::class,
                'product_id')
            )->addFlags(new CascadeDelete(), new ApiAware()),
        );

        $collection->add(
            (new OneToManyAssociationField(
                'presentationCmsPages',
                PresentationCmsPageDefinition::class,
                'product_id')
            )->addFlags(new CascadeDelete()),
        );
    }

    public function getDefinitionClass(): string
    {
        return ProductDefinition::class;
    }
}
