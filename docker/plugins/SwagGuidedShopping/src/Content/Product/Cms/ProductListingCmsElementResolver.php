<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Product\Cms;

use Shopware\Core\Content\Cms\Aggregate\CmsSlot\CmsSlotEntity;
use Shopware\Core\Content\Cms\DataResolver\CriteriaCollection;
use Shopware\Core\Content\Cms\DataResolver\Element\AbstractCmsElementResolver;
use Shopware\Core\Content\Cms\DataResolver\Element\ElementDataCollection;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\ResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\Struct\ProductListingStruct;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Request;
use Shopware\Core\Content\Product\Cms\ProductListingCmsElementResolver as ShopwareProductListingCmsElementResolver;

class ProductListingCmsElementResolver extends AbstractCmsElementResolver
{
    public const CONTEXT_EXTENSION = 'swagGuidedShoppingPresentation';

    public function __construct(
        private readonly ShopwareProductListingCmsElementResolver $original,
    ) {
    }

    public function getType(): string
    {
        return 'product-listing';
    }

    public function collect(CmsSlotEntity $slot, ResolverContext $resolverContext): ?CriteriaCollection
    {
        return $this->original->collect($slot, $resolverContext);
    }

    public function enrich(CmsSlotEntity $slot, ResolverContext $resolverContext, ElementDataCollection $result): void
    {
        $context = $resolverContext->getSalesChannelContext();

        if (!$context->hasExtension(self::CONTEXT_EXTENSION)) {
            $this->original->enrich($slot, $resolverContext, $result);
            return;
        }

        $data = new ProductListingStruct();
        $slot->setData($data);

        $request = $resolverContext->getRequest();

        $this->restrictFilters($slot, $request);

        if ($this->isCustomSorting($slot)) {
            $this->restrictSortings($request, $slot);
            $this->addDefaultSorting($request, $slot);
        }

        $criteria = new Criteria();
        $criteria->setTitle('cms::product-listing');

        // add empty listing with placeholder product
        $searchResult = new EntitySearchResult(
            ProductDefinition::ENTITY_NAME,
            0,
            new ProductCollection(),
            null,
            $criteria,
            $context->getContext()
        );
        $product = new ProductEntity();
        $product->setId(Uuid::randomHex());
        $searchResult->add($product);

        $data->setListing($searchResult);
    }

    private function isCustomSorting(CmsSlotEntity $slot): bool
    {
        $config = $slot->getTranslation('config');

        if ($config && isset($config['useCustomSorting'], $config['useCustomSorting']['value'])) {
            return $config['useCustomSorting']['value'];
        }

        return false;
    }

    private function addDefaultSorting(Request $request, CmsSlotEntity $slot): void
    {
        if ($request->get('order')) {
            return;
        }

        $config = $slot->getTranslation('config');

        if ($config
            && isset($config['defaultSorting'], $config['defaultSorting']['value'])
            && $config['defaultSorting']['value']
        ) {
            $request->request->set('order', $config['defaultSorting']['value']);
            return;
        }

        // if we have no specific order given at this point, set the order to be the highest priority available sorting
        if ($request->get('availableSortings')) {
            $availableSortings = $request->get('availableSortings');
            \arsort($availableSortings, \SORT_DESC | \SORT_NUMERIC);
            $request->request->set('order', \array_key_first($availableSortings));
        }
    }

    private function restrictSortings(Request $request, CmsSlotEntity $slot): void
    {
        $config = $slot->getTranslation('config');

        if (!$config || !isset($config['availableSortings']) || !isset($config['availableSortings']['value'])) {
            return;
        }

        $request->request->set('availableSortings', $config['availableSortings']['value']);
    }

    private function restrictFilters(CmsSlotEntity $slot, Request $request): void
    {
        // set up the default behavior
        $defaults = ['manufacturer-filter', 'rating-filter', 'shipping-free-filter', 'price-filter', 'property-filter'];

        $request->request->set('property-whitelist', null);

        $config = $slot->get('config');

        if (isset($config['propertyWhitelist']['value']) && \count($config['propertyWhitelist']['value'])) {
            $request->request->set('property-whitelist', $config['propertyWhitelist']['value']);
        }

        if (!isset($config['filters']['value'])) {
            return;
        }

        // apply config settings
        $config = \explode(',', $config['filters']['value']);
        foreach ($defaults as $filter) {
            if (\in_array($filter, $config, true)) {
                continue;
            }

            $request->request->set($filter, false);
        }
    }
}
