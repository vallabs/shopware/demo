<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\LastSeenInsights\Api;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Widgets\LastSeenInsights\Service\LastSeenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class GetLastSeenProductInsightsController extends AbstractController
{
    public function __construct(
        private readonly LastSeenService $lastSeenService
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/widgets/last-seen', name: 'api.guided-shopping.appointment.widgets.get-last-seen-insights', methods: ['GET'])]
    public function getInsights(string $appointmentId, Request $request, Context $context): JsonResponse
    {
        $products = $this->lastSeenService->getLastSeenProducts($appointmentId, $request, $context);

        return new JsonResponse($products);
    }
}
