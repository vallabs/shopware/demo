<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\LastSeenInsights\Service;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingLoader;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use SwagGuidedShopping\Struct\WidgetProductListing;
use Symfony\Component\HttpFoundation\Request;

class LastSeenService
{
    public function __construct(
        private readonly PresentationStateServiceFactory $stateServiceFactory,
        private readonly Connection $connection,
        private readonly AbstractSalesChannelContextFactory $channelContextFactory,
        private readonly ProductListingLoader $productListingLoader
    ) {
    }

    /**
     * @throws Exception
     */
    public function getLastSeenProducts(
        string $appointmentId,
        Request $request,
        Context $context
    ): WidgetProductListing
    {
        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);

        $stateService = $this->stateServiceFactory->build($appointmentId, $context);
        $stateForGuides = $stateService->getStateForGuides();

        $salesChannelId = $stateForGuides->getSalesChannelId();
        if (!$salesChannelId) {
            throw new NoSalesChannelDomainMappedException($stateForGuides->getAppointmentId());
        }

        $attendeeId = $request->query->get('attendeeId');
        // load data of a particular attendee
        $attendeeIds = !\is_null($attendeeId) ? [$attendeeId] : $stateForGuides->getAttendeeIdsForAllKindOfClients();

        if (!\count($attendeeIds)) {
            return new WidgetProductListing([], 0, $page, $limit);
        }

        $sql = "
            SELECT 
                DISTINCT(JSON_UNQUOTE(JSON_EXTRACT(`payload`, '$.productId'))) AS `productId`
            FROM `guided_shopping_interaction`
            WHERE `name` IN (:interactions)
                AND `attendee_id` IN (:attendeeIds)
            ORDER BY `triggered_at` DESC
        ";

        $data = $this->connection->executeQuery(
            $sql,
            [
                'interactions' => [InteractionName::PRODUCT_VIEWED, InteractionName::QUICKVIEW_OPENED],
                'attendeeIds' => Uuid::fromHexToBytesList($attendeeIds)
            ],
            [
                'interactions' => ArrayParameterType::STRING,
                'attendeeIds' => ArrayParameterType::STRING
            ]
        )->fetchAllAssociativeIndexed();

        $total = \count($data);

        /** @var array<string> $productIds */
        $productIds = \array_keys($data);
        if (!\count($productIds)) {
            return new WidgetProductListing([], $total, $page, $limit);
        }

        $offset = $limit * ($page - 1);

        $filteredProductIds = \array_splice($productIds, $offset, $limit);
        if (!\count($filteredProductIds)) {
            return new WidgetProductListing([], $total, $page, $limit);
        }

        $salesChannelContext = $this->channelContextFactory->create(Uuid::randomHex(), $salesChannelId);

        $criteria = new Criteria($filteredProductIds);
        $criteria->resetSorting();

        /** @var EntitySearchResult<ProductCollection> $result */
        $result = $this->productListingLoader->load($criteria, $salesChannelContext);

        /** @var ProductEntity[] $products */
        $products = $result->getElements();

        return new WidgetProductListing(
            $products,
            $total,
            $page,
            $limit
        );
    }
}
