<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\CartInsights\Struct;

use Shopware\Core\Framework\Struct\Struct;
use Shopware\Core\System\Currency\CurrencyEntity;

class CartInsightsStruct extends Struct
{
    protected float $cartSum = 0.0;

    protected int $productCount = 0;

    protected string $currencyId;

    protected string $currencySymbol;

    /**
     * @var array<string, array<int, mixed>>
     */
    protected array $topProducts = [];

    /**
     * @param array<string, array<string, mixed>> $topProducts
     */
    public function __construct(float $cartSum, array $topProducts, int $productCount, CurrencyEntity $currency)
    {
        $this->cartSum = $cartSum;
        foreach ($topProducts as $groupName => $groupedProducts) {
            foreach ($groupedProducts as $productId => $value) {
                $this->topProducts[$groupName][] = ['productId' => $productId, 'value' => $value];
            }
        }
        $this->productCount = $productCount;
        $this->currencySymbol = $currency->getSymbol();
        $this->currencyId = $currency->getId();
    }

    public function getCartSum(): float
    {
        return $this->cartSum;
    }

    public function getProductCount(): int
    {
        return $this->productCount;
    }

    public function getCurrencyId(): string
    {
        return $this->currencyId;
    }

    public function getCurrencySymbol(): string
    {
        return $this->currencySymbol;
    }

    /**
     * @return array<string, array<int, mixed>>
     */
    public function getTopProducts(): array
    {
        return $this->topProducts;
    }
}
