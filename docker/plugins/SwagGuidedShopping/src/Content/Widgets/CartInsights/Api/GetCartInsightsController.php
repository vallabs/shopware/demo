<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\CartInsights\Api;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\Currency\CurrencyCollection;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\CartInsights\Service\CartInsightsService;
use SwagGuidedShopping\Core\Checkout\Cart\Service\AttendeeCartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class GetCartInsightsController extends AbstractController
{
    /**
     * @param EntityRepository<CurrencyCollection> $currencyRepository
     */
    public function __construct(
        private readonly CartInsightsService $cartInsightsService,
        private readonly EntityRepository $currencyRepository,
        private readonly AttendeeCartService $attendeeCartService
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/widgets/cart-insights', name: 'api.guided-shopping.appointment.widgets.get-cart-insights', methods: ['GET'])]
    public function getInsights(string $appointmentId, Context $context): JsonResponse
    {
        /** @var CurrencyEntity[] $currencyData */
        $currencyData = $this->currencyRepository->search(new Criteria(), $context)->getElements();

        $attendeeCarts = $this->attendeeCartService->createAttendeeCarts($appointmentId, $currencyData, $context);

        $cartInsights = $this->cartInsightsService->getCartInsights($attendeeCarts, $currencyData[$context->getCurrencyId()]);

        return new JsonResponse($cartInsights);
    }
}
