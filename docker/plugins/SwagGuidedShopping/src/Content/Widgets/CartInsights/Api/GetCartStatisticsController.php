<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\CartInsights\Api;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Widgets\CartInsights\Service\CartStatisticsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class GetCartStatisticsController extends AbstractController
{
    public function __construct(
        private readonly CartStatisticsService $cartStatisticsService
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/widgets/cart-statistics', name: 'api.guided-shopping.appointment.widgets.get-cart-statistics', methods: ['GET'])]
    public function getCartStatistics(string $appointmentId, Request $request, Context $context): JsonResponse
    {
        $cart = $this->cartStatisticsService->create($appointmentId, $request, $context);

        return new JsonResponse($cart);
    }
}
