<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\CartInsights\Service;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\CartCalculator;
use Shopware\Core\Checkout\Cart\LineItem\LineItem;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Core\Checkout\Cart\SalesChannel\CartItemAddRouteDecorator;
use SwagGuidedShopping\Core\Checkout\Cart\Service\AttendeeCartService;
use Symfony\Component\HttpFoundation\Request;

class CartStatisticsService
{
    protected const CART_NAME = 'gs-cart-statistics';

    private const METADATA_KEY = 'meta';

    private const METADATA_ATTENDEES_KEY = 'attendees';

    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly EntityRepository $appointmentRepository,
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory,
        private readonly AttendeeCartService $attendeeCartService,
        private readonly AbstractSalesChannelContextFactory $channelContextFactory,
        private readonly CartCalculator $cartCalculator
    ) {
    }

    public function create(string $appointmentId, Request $request, Context $context): Cart
    {
        /** @var AppointmentEntity|null $appointment */
        $appointment = $this->appointmentRepository->search(new Criteria([$appointmentId]), $context)->first();
        if (!$appointment || !$appointment->getSalesChannelDomain()) {
            throw new EntityNotFoundException(AppointmentDefinition::ENTITY_NAME, $appointmentId);
        }

        $presentationStateService = $this->presentationStateServiceFactory->build($appointmentId, $context);
        $stateForGuides = $presentationStateService->getStateForGuides();

        $attendees = $stateForGuides->getAllClients();
        $attendeeIds = \array_keys($attendees);

        $cartData = $this->attendeeCartService->getCartData($attendeeIds);
        $tempCart = new Cart(Uuid::randomHex());

        foreach ($cartData as $cartRow) {
            /** @var Cart $cart */
            $cart = \unserialize($cartRow['payload']);
            $attendeeId = $cartRow['attendee_id'];

            /** @var LineItem $lineItem */
            foreach ($cart->getLineItems() as $lineItem) {
                $productId = $lineItem->getReferencedId();

                if (!$productId) {
                    continue;
                }

                if (!$lineItem->getPrice() instanceof CalculatedPrice) {
                    continue;
                }

                if (!$lineItem->getPayloadValue(CartItemAddRouteDecorator::GUIDED_SHOPPING_ADD_TO_CART_CONTEXT_IDENTIFIER)) {
                    continue;
                }

                $lineItemKey = self::CART_NAME . '-' . $productId;

                $existedLineItem = $tempCart->get($lineItemKey);
                if ($existedLineItem) {
                    $existedLineItem->setStackable(true);
                    $existedLineItem->setQuantity($existedLineItem->getQuantity() + $lineItem->getQuantity());

                    $metaData = $existedLineItem->getExtension(self::METADATA_KEY);
                    if ($metaData instanceof ArrayStruct) {
                        $currentAttendees = $metaData->get(self::METADATA_ATTENDEES_KEY);
                        $currentAttendees[] =  [
                            'id' => $attendeeId,
                            'name' => $attendees[$attendeeId]['attendeeName'] ?? NULL
                        ];

                        $metaData->set(self::METADATA_ATTENDEES_KEY, $currentAttendees);
                    }
                } else {
                    $lineItem = new LineItem(
                        $lineItemKey,
                        $lineItem->getType(),
                        $lineItem->getReferencedId(),
                        $lineItem->getQuantity()
                    );

                    $metaData = [];
                    $metaData[self::METADATA_ATTENDEES_KEY][] = [
                        'id' => $attendeeId,
                        'name' => $attendees[$attendeeId]['attendeeName'] ?? NULL
                    ];
                    $lineItem->addExtension(self::METADATA_KEY, new ArrayStruct($metaData));

                    $tempCart->add($lineItem);
                }
            }
        }

        $salesChannelContext = $this->channelContextFactory->create(
            Uuid::randomHex(),
            $appointment->getSalesChannelDomain()->getSalesChannelId()
        );

        $cart = $this->cartCalculator->calculate($tempCart, $salesChannelContext);

        $this->sort($request, $cart->getLineItems());

        return $cart;
    }

    private  function sort(Request $request, LineItemCollection $lineItems): void
    {
        $orderBy = $request->query->get('order', 'quantity');
        $sortBy = $request->query->get('sort', 'desc');

        $lineItems->sort(function(LineItem $a, LineItem $b) use ($orderBy, $sortBy) {
            switch ($orderBy) {
                case "name":
                    return ($sortBy === 'asc') ? $a->getLabel() <=> $b->getLabel() : $b->getLabel() <=> $a->getLabel();
                case "revenue":
                    $aPrice = $a->getPrice() ? $a->getPrice()->getTotalPrice() : 0;
                    $bPrice = $b->getPrice() ? $b->getPrice()->getTotalPrice() : 0;
                    return ($sortBy === 'asc') ? $aPrice - $bPrice : $bPrice - $aPrice;
                case "quantity":
                    return ($sortBy === 'asc') ? $a->getQuantity() - $b->getQuantity() : $b->getQuantity() - $a->getQuantity();
                default:
                    return 0;
            }
        });
    }
}
