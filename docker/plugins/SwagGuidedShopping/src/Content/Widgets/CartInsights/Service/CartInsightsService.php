<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\CartInsights\Service;

use Shopware\Core\Checkout\Cart\Price\CashRounding;
use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\CartInsights\Struct\CartInsightsStruct;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;

class CartInsightsService
{
    public function __construct(
        private readonly CashRounding $cashRounding
    ) {
    }

    /**
     * @param AttendeeCart[] $attendeeCarts
     */
    public function getCartInsights(array $attendeeCarts, CurrencyEntity $currency): CartInsightsStruct
    {
        $cartSum = $this->calculateCartSum($attendeeCarts, $currency);
        $productCount = $this->calculateProductCount($attendeeCarts);
        $topProducts = $this->getTopProducts($attendeeCarts, $currency);

        return new CartInsightsStruct($cartSum, $topProducts, $productCount, $currency);
    }

    /**
     * @param AttendeeCart[] $attendeeCarts
     * @return array<string, array<string, mixed>>
     */
    private function getTopProducts(array $attendeeCarts, CurrencyEntity $currencyEntity, int $maxProducts = 5): array
    {
        $productCount = [];
        $productSum = [];

        foreach ($attendeeCarts as $attendeeCart) {
            foreach ($attendeeCart->getItems() as $cartInsightLineItem) {
                $productId = $cartInsightLineItem->getId();

                if (!\array_key_exists($productId, $productCount)) {
                    $productCount[$cartInsightLineItem->getId()] = 0;
                }

                if (!\array_key_exists($productId, $productSum)) {
                    $productSum[$productId] = 0.0;
                }

                $productCount[$productId] += $cartInsightLineItem->getQuantity();
                $productSum[$productId] += $cartInsightLineItem->getTotalPriceNet();
            }
        }

        \arsort($productCount);
        \arsort($productSum);

        $sorted['byQuantity'] = \array_slice($productCount, 0, $maxProducts);
        $sorted['byRevenue'] = \array_slice($productSum, 0, $maxProducts);

        foreach ($sorted['byRevenue'] as $key => $revenue) {
            $sorted['byRevenue'][$key] = $this->cashRounding->cashRound($revenue, $currencyEntity->getTotalRounding());
        }

        return $sorted;
    }

    /**
     * @param AttendeeCart[] $attendeeCarts
     */
    private function calculateCartSum(array $attendeeCarts, CurrencyEntity $currency): float
    {
        $cartSum = 0.0;

        foreach ($attendeeCarts as $attendeeCart) {
            $cartSum += $attendeeCart->getPriceNet($currency);
        }

        return $cartSum;
    }

    /**
     * @param AttendeeCart[] $attendeeCarts
     */
    private function calculateProductCount(array $attendeeCarts): int
    {
        $productCount = 0;
        foreach ($attendeeCarts as $attendeeCart) {
            foreach ($attendeeCart->getItems() as $item) {
                $productCount += $item->getQuantity();
            }
        }

        return $productCount;
    }
}
