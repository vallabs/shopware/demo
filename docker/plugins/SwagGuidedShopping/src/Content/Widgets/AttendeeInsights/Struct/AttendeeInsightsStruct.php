<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct;

use Shopware\Core\Framework\Struct\Struct;

class AttendeeInsightsStruct extends Struct
{
    public function __construct(
        protected string $id,
        protected float $cartSum,
        protected int $productCount,
        protected int $lineItemCount
    ) {
    }

    public function getCartSum(): float
    {
        return $this->cartSum;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getProductCount(): int
    {
        return $this->productCount;
    }

    public function getLineItemCount(): int
    {
        return $this->lineItemCount;
    }
}
