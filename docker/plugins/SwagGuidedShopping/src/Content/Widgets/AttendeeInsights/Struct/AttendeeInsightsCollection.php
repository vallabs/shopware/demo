<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct;

use Shopware\Core\Framework\Struct\Struct;
use Shopware\Core\System\Currency\CurrencyEntity;

class AttendeeInsightsCollection extends Struct
{
    /**
     * @var array<string, AttendeeInsightsStruct>
     */
    protected array $attendees = [];

    protected string $currencyId;

    protected string $currencySymbol;

    public function __construct(CurrencyEntity $currency)
    {
        $this->currencyId = $currency->getId();
        $this->currencySymbol = $currency->getSymbol();
    }

    public function getCurrencyId(): string
    {
        return $this->currencyId;
    }

    public function getCurrencySymbol(): string
    {
        return $this->currencySymbol;
    }

    public function addAttendee(AttendeeInsightsStruct $attendeeInsights): void
    {
        $this->attendees[$attendeeInsights->getId()] = $attendeeInsights;
    }

    /**
     * @return array<string, AttendeeInsightsStruct>
     */
    public function getAttendees(): array
    {
        return $this->attendees;
    }
}
