<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\AttendeeInsights\Service;

use Shopware\Core\System\Currency\CurrencyEntity;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsCollection;
use SwagGuidedShopping\Content\Widgets\AttendeeInsights\Struct\AttendeeInsightsStruct;
use SwagGuidedShopping\Core\Checkout\Cart\Struct\AttendeeCart;

class AttendeeInsightsService
{
    /**
     * @param AttendeeCart[] $attendeeCarts
     */
    public function getInsights(array $attendeeCarts, CurrencyEntity $currencyEntity): AttendeeInsightsCollection
    {
        $collection = new AttendeeInsightsCollection($currencyEntity);

        foreach ($attendeeCarts as $attendeeCart) {
            $collection->addAttendee(
                new AttendeeInsightsStruct(
                    $attendeeCart->getAttendeeId(),
                    $attendeeCart->getPriceNet($currencyEntity),
                    $attendeeCart->getProductCount(),
                    \count($attendeeCart->getItems())
                )
            );
        }

        return $collection;
    }
}
