<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\WishlistInsights\Api;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Widgets\WishlistInsights\Service\WishlistService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class GetWishlistInsightsController extends AbstractController
{
    public function __construct(
        private readonly WishlistService $wishlistService
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/widgets/wishlist', name: 'api.guided-shopping.appointment.widgets.get-wishlist', methods: ['GET'])]
    public function getWishlist(string $appointmentId, Request $request, Context $context): JsonResponse
    {
        $result = $this->wishlistService->loadLikedProducts($appointmentId, $request, $context);

        return new JsonResponse($result);
    }
}
