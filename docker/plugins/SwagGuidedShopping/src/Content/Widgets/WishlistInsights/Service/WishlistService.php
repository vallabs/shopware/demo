<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Widgets\WishlistInsights\Service;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Shopware\Core\Content\Product\ProductCollection;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Context\AbstractSalesChannelContextFactory;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;
use SwagGuidedShopping\Framework\Routing\NoSalesChannelDomainMappedException;
use SwagGuidedShopping\Struct\WidgetProductListing;
use Symfony\Component\HttpFoundation\Request;

class WishlistService
{
    /**
     * @param SalesChannelRepository<ProductCollection> $productRepository
     */
    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory,
        private readonly Connection $connection,
        private readonly AbstractSalesChannelContextFactory $channelContextFactory,
        private readonly SalesChannelRepository $productRepository
    ) {
    }

    /**
     * @throws \Exception
     */
    public function loadLikedProducts(
        string $appointmentId,
        Request $request,
        Context $context
    ): WidgetProductListing
    {
        $stateService = $this->presentationStateServiceFactory->build($appointmentId, $context);
        $stateForGuides = $stateService->getStateForGuides();

        $limit = $request->query->getInt('limit', 10);
        $page = $request->query->getInt('page', 1);

        $salesChannelId = $stateForGuides->getSalesChannelId();
        if (!$salesChannelId) {
            throw new NoSalesChannelDomainMappedException($stateForGuides->getAppointmentId());
        }

        $attendees = $stateForGuides->getAllClients();
        $attendeeIds = \array_keys($attendees);
        if (!\count($attendeeIds)) {
           return new WidgetProductListing([], 0, $page, $limit);
        }

        $orderBy = $request->query->get('order', 'quantity');
        $direction = $request->query->get('sort', 'desc');

        $likedProducts = $this->getAllLikedProductsByAttendees($attendeeIds, $orderBy, $direction);
        if (!\count($likedProducts)) {
            return new WidgetProductListing([], 0, $page, $limit);
        }

        /**
         * get the product ids for the current page (pagination)
         * @var array<string> $productIds
         */
        $productIds = \array_keys($likedProducts);
        $offset = $limit * ($page - 1);
        $filteredProductIds = \array_splice($productIds, $offset, $limit);
        if (!\count($filteredProductIds)) {
            return new WidgetProductListing([], \count($likedProducts), $page, $limit);
        }

        $criteria = new Criteria($filteredProductIds);
        if ($orderBy === 'name') {
            $criteria->addSorting(new FieldSorting('product.name', $direction));
        } else {
            $criteria->resetSorting(); // use id sorting if order by quantity or revenue
        }

        $criteria->addAssociation('options.group');

        $salesChannelContext = $this->channelContextFactory->create(Uuid::randomHex(), $salesChannelId);

        /** @var EntitySearchResult<ProductCollection> $result */
        $result = $this->productRepository->search($criteria, $salesChannelContext);

        foreach ($result->getEntities() as $product) {
            $productId = $product->getId();

            $wishlistData = $this->fetchingExtensionData($likedProducts[$productId] ?? [], $attendees);

            $product->addArrayExtension('wishlist', $wishlistData);
        }

        /** @var ProductEntity[] $products */
        $products = $result->getElements();

        return new WidgetProductListing(
            $products,
            \count($likedProducts),
            $page,
            $limit
        );
    }

    /**
     * @param array<string> $attendeeIds
     * @return array<array<string, mixed>>
     * @throws Exception
     */
    private function getAllLikedProductsByAttendees(array $attendeeIds, string $orderBy, string $direction): array
    {
        $sql = "
            SELECT 
                LOWER(HEX(`product_id`)) as productId,
                COUNT(`product_id`) as count,
                GROUP_CONCAT(LOWER(HEX(`attendee_id`))) as attendeeIds
            FROM `guided_shopping_attendee_product_collection`
            WHERE `attendee_id` IN (:attendeeIds)
                AND `alias` = 'liked'
            GROUP BY `productId`
            ORDER BY `count` {$direction}
        ";

        $data = $this->connection->executeQuery(
            $sql,
            ['attendeeIds' => Uuid::fromHexToBytesList($attendeeIds)],
            ['attendeeIds' => ArrayParameterType::STRING]
        )->fetchAllAssociativeIndexed();


        if ($orderBy !== 'revenue' || !\count($data)) {
            return $data;
        }

        /** @var string[] $productIds */
        $productIds = \array_keys($data);

        // get parentId if product has variants; otherwise use productId as parentId
        $mapping = $this->mappingWithParentId($productIds);

        $parentIds = \array_column($mapping, 'parentId');

        $cheapestPrices = $this->getCheapestPrices($parentIds);

        $data = $this->addCheapestPriceToLikedProducts($data, $mapping, $cheapestPrices);

        return $this->sortLikedProductsByRevenue($data, $direction);
    }

    /**
     * @param array<string> $productIds
     * @return array<array<string, string>>
     * @throws Exception
     */
    private function mappingWithParentId(array $productIds): array
    {
        return $this->connection->executeQuery(
            "
                SELECT  LOWER(HEX(`id`)) as `id`, 
                        IFNULL(LOWER(HEX(`parent_id`)), LOWER(HEX(`id`))) AS `parentId`
                    FROM `product`
                    WHERE `id` IN (:productIds)
                    AND `product`.`cheapest_price_accessor` IS NOT NULL
                ",
            [
                'productIds' => Uuid::fromHexToBytesList($productIds)
            ],
            ['productIds' => ArrayParameterType::STRING]
        )->fetchAllAssociativeIndexed();
    }

    /**
     * @param array<string> $productIds
     * @return array<array<string, mixed>>
     * @throws Exception
     */
    private function getCheapestPrices(array $productIds): array
    {
        $sql = "
                SELECT 
                    LOWER(HEX(`product`.`id`)) AS `id`, 
                    IFNULL(LOWER(HEX(`parent_id`)), LOWER(HEX(`id`))) AS `parentId`,
                    GROUP_CONCAT(
                        REPLACE(
                            REPLACE(JSON_UNQUOTE(JSON_EXTRACT(`cheapest_price_accessor`, '$.*.*.gross')), '[', ''),
                            ']', ''
                        )
                    ) AS `grossValues`
                FROM
                    `product`
                WHERE
                    (`id` IN (:productIds) OR `parent_id` IN (:productIds))
                    AND `product`.`cheapest_price_accessor` IS NOT NULL
                GROUP BY `parentId`
            ";

        return $this->connection->executeQuery(
            $sql,
            ['productIds' => Uuid::fromHexToBytesList($productIds)],
            ['productIds' => ArrayParameterType::STRING]
        )->fetchAllAssociativeIndexed();
    }

    /**
     * @param array<array<string, mixed>> $likedProducts
     * @param array<array<string, mixed>> $mapping
     * @param array<array<string, mixed>> $cheapestPrices     
     * @return array<array<string, mixed>>
     */
    private function addCheapestPriceToLikedProducts(array $likedProducts, array $mapping,  array $cheapestPrices): array
    {
        foreach ($likedProducts as $productId => $item) {
            $parentId = $mapping[$productId]['parentId'];

            if (isset($cheapestPrices[$parentId])) {
                $grossValues = $cheapestPrices[$parentId]['grossValues'];
            } else {
                $filteredPrice = \array_values(\array_filter($cheapestPrices, function ($item) use ($parentId) {
                    return $item['parentId'] === $parentId;
                }));
                $grossValues = $filteredPrice[0]['grossValues'];
            }

            $cheapestPrice = (float) \min(\explode(',', $grossValues));

            $likedProducts[$productId]['cheapestPrice'] = $cheapestPrice;
            $likedProducts[$productId]['totalPrice'] = (int) $item['count'] * $cheapestPrice;
        }

        return $likedProducts;
    }

    /**
     * @param array<array<string, mixed>> $likedProducts
     * @return array<array<string, mixed>>
     */
    private function sortLikedProductsByRevenue(array $likedProducts, string $direction): array
    {
        \uasort($likedProducts, static function ($a, $b) use ($direction) {
            $aTotalPrice = (int) $a['count'] * (float) $a['cheapestPrice'];
            $bTotalPrice = (int) $b['count'] * (float) $b['cheapestPrice'];

            return $direction === 'asc' ? $aTotalPrice <=> $bTotalPrice : $bTotalPrice <=> $aTotalPrice;
        });

        return $likedProducts;
    }

    /**
     * @param array<string, mixed> $likedProduct
     * @param array<string, array<string, mixed>> $attendee
     * @return array<string, mixed>
     */
    private function fetchingExtensionData(array $likedProduct, array $attendee): array
    {
        $likedAttendees = [];

        if (!\array_key_exists('attendeeIds', $likedProduct)) {
            return [
                'count' => 0,
                'attendees' => []
            ];
        }

        $attendeeIds = \explode(',', $likedProduct['attendeeIds']);
        foreach ($attendeeIds as $attendeeId) {
            if (!\array_key_exists($attendeeId, $attendee)) {
                continue;
            }

            $likedAttendees[] = [
                'id' => $attendeeId,
                'name' => $attendee[$attendeeId]['attendeeName'] ?? NULL
            ];
        }

        return [
            'count' => $likedProduct['count'],
            'attendees' => $likedAttendees
        ];
    }
}
