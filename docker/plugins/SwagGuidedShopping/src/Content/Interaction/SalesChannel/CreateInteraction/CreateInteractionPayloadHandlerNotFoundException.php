<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class CreateInteractionPayloadHandlerNotFoundException extends ShopwareHttpException
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__CREATE_INTERACTION_PAYLOAD_HANDLER_NOT_FOUND;
    }
}
