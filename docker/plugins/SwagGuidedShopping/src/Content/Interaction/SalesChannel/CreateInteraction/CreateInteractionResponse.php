<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction;

use Shopware\Core\System\SalesChannel\StoreApiResponse;

class CreateInteractionResponse extends StoreApiResponse
{
    public function __construct(CreateInteractionStruct $data)
    {
        parent::__construct($data);
    }
}
