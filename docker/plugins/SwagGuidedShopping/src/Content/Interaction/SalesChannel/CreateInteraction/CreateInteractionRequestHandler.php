<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction;

use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadException;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagGuidedShopping\Service\Validator\DataValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Type;

class CreateInteractionRequestHandler
{
    public function __construct(
        private readonly DataValidator $validator,
        private readonly InteractionService $interactionService
    ) {
    }

    public function handleRequest(Request $request, RequestDataBag $dataBag, SalesChannelContext $context): void
    {
        $interactionName = $dataBag->get('name');
        if (!$interactionName) {
            throw new \Exception('Interaction name is empty.');
        }

        $payload = $this->handlePayload($dataBag);
        $triggeredAt = $this->createTriggeredAt($dataBag->get('triggeredAt'));

        $timeToLive = $dataBag->get('lifeTimeInSeconds', -1);

        try {
            /** @var AttendeeEntity $attendee */
            $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);
            $this->interactionService->handle($interactionName, $triggeredAt, $timeToLive, $attendee, $payload, $context);
        } catch (InteractionPayloadException $exception) {
            throw new CreateInteractionPayloadViolationException($exception->getViolationsCollection());
        }
    }

    public function validateRequest(Request $request): void
    {
        $constraint = new Collection(
            [
                'name' => [new Type('string')],
                'lifeTimeInSeconds' => new Optional([new Type('integer')]),
                'payload' => new Optional([new Type('array')]),
                'triggeredAt' => new Optional([new DateTime('Y-m-d H:i:s')]),
            ]
        );

        $violations = $this->validator->validate($request->request->all(), $constraint);
        if ($violations->count()) {
            throw new CreateInteractionViolationException($violations);
        }
    }

    /**
     * @return array<string, mixed>
     */
    private function handlePayload(RequestDataBag $dataBag): array
    {
        $payload = $dataBag->get('payload');

        return $payload ? $payload->all() : [];
    }

    private function createTriggeredAt(?string $triggeredAt): \DateTimeImmutable
    {
        $triggeredAt = $triggeredAt ? \DateTimeImmutable::createFromFormat('Y-m-d H:i:s', $triggeredAt) : new \DateTimeImmutable('now');

        if (!$triggeredAt) {
            throw new \Exception('Could not generate a triggered at date.');
        }

        return $triggeredAt;
    }
}
