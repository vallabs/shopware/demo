<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction;

use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractCreateInteractionRoute
{
    abstract public function getDecorated(): AbstractCreateInteractionRoute;

    abstract public function create(
        RequestDataBag $dataBag,
        Request $request,
        SalesChannelContext $context
    ): CreateInteractionResponse;
}
