<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class CreateInteractionPayloadViolationException extends ShopwareHttpException
{
    public function __construct(ViolationList $violationsCollection)
    {
        $message = "Could not add interaction, there are errors with the payload \n";
        $message .= $violationsCollection;
        parent::__construct($message);
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__INTERACTION_ADD_PAYLOAD_VIOLATION;
    }
}
