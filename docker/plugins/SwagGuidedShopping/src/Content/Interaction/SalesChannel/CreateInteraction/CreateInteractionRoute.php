<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\SalesChannel\CreateInteraction;

use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class CreateInteractionRoute extends AbstractCreateInteractionRoute
{
    public function __construct(
        private readonly CreateInteractionRequestHandler $requestHandler
    ) {
    }

    public function getDecorated(): AbstractCreateInteractionRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/interaction', name: 'store-api.guided-shopping.create-interaction', defaults: ['attendee_required' => true], methods: ['POST'])]
    public function create(RequestDataBag $dataBag, Request $request, SalesChannelContext $context): CreateInteractionResponse
    {
        $this->requestHandler->validateRequest($request);

        $this->requestHandler->handleRequest($request, $dataBag, $context);

        return new CreateInteractionResponse(new CreateInteractionStruct());
    }
}
