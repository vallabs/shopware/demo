<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction;

use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadHandlerNotFound;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler\AbstractInteractionPayloadHandler;

class InteractionPayloadHandlerCollection
{
    /**
     * @var array<string, AbstractInteractionPayloadHandler>
     */
    private array $handlers = [];

    /**
     * @param iterable<AbstractInteractionPayloadHandler> $interactionPayloadHandlers
     */
    public function __construct(iterable $interactionPayloadHandlers)
    {
        /** @var AbstractInteractionPayloadHandler $interactionPayloadHandler */
        foreach ($interactionPayloadHandlers as $interactionPayloadHandler) {
            $names = $interactionPayloadHandler::getSupportedInteractions();
            foreach ($names as $name) {
                $this->handlers[$name] = $interactionPayloadHandler;
            }
        }
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(string $interactionName, array $payload): AbstractPayload
    {
        $handler = $this->getHandler($interactionName);

        return $handler->createPayload($payload);
    }

    public function getHandler(string $interactionName): AbstractInteractionPayloadHandler
    {
        if (!\array_key_exists($interactionName, $this->handlers)) {
            throw new InteractionPayloadHandlerNotFound($interactionName, \array_keys($this->handlers));
        }

        return $this->handlers[$interactionName];
    }
}
