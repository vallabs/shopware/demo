<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;

class QuickViewHandler extends AbstractInteractionHandler
{
    private const INTERACTIONS = [
        InteractionName::QUICKVIEW_OPENED,
        InteractionName::QUICKVIEW_CLOSED
    ];

    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    public function supports(Interaction $interaction): bool
    {
        return \in_array($interaction->getName(), self::INTERACTIONS);
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        $this->updatePresentationStates($interaction, $context->getContext());
    }

    private function updatePresentationStates(Interaction $interaction, Context $context): void
    {
        $state = $interaction->getName() === InteractionName::QUICKVIEW_OPENED;

        $presentationService = $this->presentationStateServiceFactory->build($interaction->getAppointmentId(), $context);
        $stateForGuides = $presentationService->getStateForGuides();
        /** @var ProductPayload $quickViewState */
        $quickViewState = $interaction->getPayload();
        $stateForGuides->setQuickViewStateForClient($quickViewState->getProductId(), $interaction->getAttendeeId(), $state);

        $presentationService->publishStateForGuides();
    }
}
