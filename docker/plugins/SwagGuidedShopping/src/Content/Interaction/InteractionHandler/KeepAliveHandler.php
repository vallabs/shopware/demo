<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;

class KeepAliveHandler extends AbstractInteractionHandler
{
    protected const INTERACTIONS = [
        InteractionName::KEEP_ALIVE,
    ];

    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly EntityRepository $attendeeRepository
    ) {
    }

    public function supports(Interaction $interaction): bool
    {
        return \in_array($interaction->getName(), self::INTERACTIONS);
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        $this->attendeeRepository->update([
            [
                'id' => $interaction->getAttendeeId(),
                'lastActive' => (new \DateTime())->format(Defaults::STORAGE_DATE_TIME_FORMAT),
            ],
        ], $context->getContext());
    }
}
