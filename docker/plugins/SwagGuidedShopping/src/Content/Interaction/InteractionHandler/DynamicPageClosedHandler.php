<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;

class DynamicPageClosedHandler extends AbstractInteractionHandler
{
    protected const INTERACTIONS = [
        InteractionName::DYNAMIC_PAGE_CLOSED,
    ];

    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    public function supports(Interaction $interaction): bool
    {
        return \in_array($interaction->getName(), self::INTERACTIONS);
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        $this->updatePresentationStatesClosed($interaction, $context->getContext());
    }

    private function updatePresentationStatesClosed(Interaction $interaction, Context $context): void
    {
        $presentationService = $this->presentationStateServiceFactory->build($interaction->getAppointmentId(), $context);
        $stateForAll = $presentationService->getStateForAll();
        $stateForAll->setDynamicPageClosed();

        $presentationService->publishStateForAll();
    }
}
