<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Framework\Struct\Struct;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractPayload;

class Interaction
{
    private string $attendeeId;

    private string $appointmentId;

    public function __construct(
        private readonly string $name,
        private readonly \DateTimeImmutable $triggeredAt,
        private readonly int $lifeTimeInSeconds,
        private readonly AttendeeEntity $attendee,
        private readonly AbstractPayload $payload
    ) {
        $this->attendeeId = $attendee->getId();
        $this->appointmentId = $attendee->getAppointmentId();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTriggeredAt(): \DateTimeImmutable
    {
        return $this->triggeredAt;
    }

    public function getLifeTimeInSeconds(): int
    {
        return $this->lifeTimeInSeconds;
    }

    public function getAttendee(): AttendeeEntity
    {
        return $this->attendee;
    }

    public function getAttendeeId(): string
    {
        return $this->attendeeId;
    }

    public function getPayload(): Struct
    {
        return $this->payload;
    }

    public function getAppointmentId(): string
    {
        return $this->appointmentId;
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        if ($this->lifeTimeInSeconds === -1) {
            return null;
        }

        return $this->triggeredAt->add(new \DateInterval('PT' . $this->lifeTimeInSeconds . 'S'));
    }

    /**
     * @return array<string, mixed>
     */
    public function getPayloadAsArray(): array
    {
        $payload = $this->getPayload()->jsonSerialize();
        unset($payload['extensions']);

        return $payload;
    }
}
