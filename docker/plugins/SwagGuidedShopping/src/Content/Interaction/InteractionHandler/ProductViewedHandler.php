<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;

class ProductViewedHandler extends AbstractInteractionHandler
{
    private const INTERACTIONS = [InteractionName::PRODUCT_VIEWED];

    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    public function supports(Interaction $interaction): bool
    {
        return \in_array($interaction->getName(), self::INTERACTIONS);
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        $this->updatePresentationStates($interaction, $context->getContext());
    }

    private function updatePresentationStates(Interaction $interaction, Context $context): void
    {
        $presentationService = $this->presentationStateServiceFactory->build($interaction->getAppointmentId(), $context);
        $stateForAll = $presentationService->getStateForAll();
        /** @var ProductPayload $productViewedPayload */
        $productViewedPayload = $interaction->getPayload();
        $stateForAll->setCurrentGuideProductId($productViewedPayload->getProductId());

        $presentationService->publishStateForAll();
    }
}
