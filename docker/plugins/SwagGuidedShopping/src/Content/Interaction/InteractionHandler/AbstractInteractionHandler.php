<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\System\SalesChannel\SalesChannelContext;

abstract class AbstractInteractionHandler
{
    abstract public function supports(Interaction $interaction): bool;

    abstract public function handle(Interaction $interaction, SalesChannelContext $context): void;
}
