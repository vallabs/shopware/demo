<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;

class InteractionDbSaveHandler extends AbstractInteractionHandler
{
    /**
     * @param EntityRepository<InteractionCollection> $interactionRepository
     */
    public function __construct(
        private readonly EntityRepository $interactionRepository
    ) {
    }

    public function supports(Interaction $interaction): bool
    {
        if (\in_array($interaction->getName(), InteractionName::NOT_STORED_INTERACTIONS)) {
            return false;
        }

        //we only want to store interactions that have a time to live greater than 0 or -1 (-1 would mean they have to stay there forever)
        return $interaction->getLifeTimeInSeconds() > 0 || $interaction->getLifeTimeInSeconds() === -1;
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        $this->add($interaction, $context->getContext());
    }

    private function add(Interaction $interaction, Context $context): void
    {
        $upsert[] = [
            'name' => $interaction->getName(),
            'attendeeId' => $interaction->getAttendeeId(),
            'triggeredAt' => $interaction->getTriggeredAt(),
            'expiresAt' => $interaction->getExpiresAt(),
            'payload' => $interaction->getPayloadAsArray(),
        ];

        $this->interactionRepository->create($upsert, $context);
    }
}
