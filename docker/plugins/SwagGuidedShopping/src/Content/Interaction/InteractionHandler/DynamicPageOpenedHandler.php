<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionHandler;

use Shopware\Core\Framework\Context;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractDynamicPageOpenedPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Content\PresentationState\Factory\PresentationStateServiceFactory;

class DynamicPageOpenedHandler extends AbstractInteractionHandler
{
    protected const INTERACTIONS = [
        InteractionName::DYNAMIC_PAGE_OPENED,
        InteractionName::DYNAMIC_PRODUCT_PAGE_OPENED,
        InteractionName::DYNAMIC_PRODUCT_LISTING_PAGE_OPENED,
    ];

    public function __construct(
        private readonly PresentationStateServiceFactory $presentationStateServiceFactory
    ) {
    }

    public function supports(Interaction $interaction): bool
    {
        return \in_array($interaction->getName(), self::INTERACTIONS);
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        $this->updatePresentationStatesOpen($interaction, $context->getContext());
    }

    private function updatePresentationStatesOpen(Interaction $interaction, Context $context): void
    {
        $presentationService = $this->presentationStateServiceFactory->build($interaction->getAppointmentId(), $context);
        $stateForAll = $presentationService->getStateForAll();
        /** @var AbstractDynamicPageOpenedPayload $dynamicPagePayload */
        $dynamicPagePayload = $interaction->getPayload();
        $stateForAll->setDynamicPageOpen($dynamicPagePayload);

        $presentationService->publishStateForAll();
    }
}
