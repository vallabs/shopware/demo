<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;

class InteractionEntity extends Entity
{
    use EntityIdTrait;

    protected string $name;

    protected \DateTimeImmutable $expiresAt;

    protected \DateTimeImmutable $triggeredAt;

    /** @var array<string, mixed>|null  */
    protected ?array $payload = null;

    protected string $attendeeId;

    protected AttendeeEntity $attendee;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getExpiresAt(): ?\DateTimeImmutable
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(\DateTimeImmutable $expiresAt): void
    {
        $this->expiresAt = $expiresAt;
    }

    public function getTriggeredAt(): \DateTimeImmutable
    {
        return $this->triggeredAt;
    }

    public function setTriggeredAt(\DateTimeImmutable $triggeredAt): void
    {
        $this->triggeredAt = $triggeredAt;
    }

    /**
     * @return array<string, mixed>|null
     */
    public function getPayload(): ?array
    {
        return $this->payload;
    }

    /**
     * @param array<string, mixed>|null $payload
     */
    public function setPayload(?array $payload): void
    {
        $this->payload = $payload;
    }

    public function getAttendeeId(): string
    {
        return $this->attendeeId;
    }

    public function setAttendeeId(string $attendeeId): void
    {
        $this->attendeeId = $attendeeId;
    }

    public function getAttendee(): AttendeeEntity
    {
        return $this->attendee;
    }

    public function setAttendee(AttendeeEntity $attendee): void
    {
        $this->attendee = $attendee;
    }
}
