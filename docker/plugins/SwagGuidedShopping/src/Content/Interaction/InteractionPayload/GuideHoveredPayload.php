<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class GuideHoveredPayload extends AbstractPayload
{
    public function __construct(
        protected ?string $hoveredElementId = null
    ) {
    }

    public function getHoveredElementId(): ?string
    {
        return $this->hoveredElementId;
    }
}
