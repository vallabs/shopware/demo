<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class ProductPayload extends AbstractPayload
{
    public function __construct(
        protected string $productId
    ) {
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
