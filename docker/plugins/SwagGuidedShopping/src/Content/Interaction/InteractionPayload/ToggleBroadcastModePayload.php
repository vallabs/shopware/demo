<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class ToggleBroadcastModePayload extends AbstractPayload
{
    public function __construct(
        protected bool $active
    ) {
    }

    public function getActive(): bool
    {
        return $this->active;
    }
}
