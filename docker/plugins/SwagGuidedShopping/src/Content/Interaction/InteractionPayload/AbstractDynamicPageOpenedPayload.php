<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

abstract class AbstractDynamicPageOpenedPayload extends AbstractPayload
{
    protected string $type;

    protected bool $opened = true;

    public function getType(): string
    {
        return $this->type;
    }

    public function isOpened(): bool
    {
        return $this->opened;
    }
}
