<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class DynamicPageOpenedPayload extends AbstractDynamicPageOpenedPayload
{
    public function __construct(
        protected string $type
    ) {
    }
}
