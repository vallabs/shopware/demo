<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class DynamicProductListingPageOpenedPayload extends AbstractDynamicPageOpenedPayload
{
    public function __construct(
        protected string $type,
        protected int $page
    ) {
    }

    public function getPage(): int
    {
        return $this->page;
    }
}
