<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class EmptyPayload extends AbstractPayload
{
}
