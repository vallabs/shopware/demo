<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class PageViewedPayload extends AbstractPayload
{
    public function __construct(
        protected string $pageId,
        protected string $sectionId,
        protected int $slideAlias
    ) {
    }

    public function getPageId(): string
    {
        return $this->pageId;
    }

    public function getSectionId(): string
    {
        return $this->sectionId;
    }

    public function getSlideAlias(): int
    {
        return $this->slideAlias;
    }
}
