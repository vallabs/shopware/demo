<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

class DynamicProductPageOpenedPayload extends AbstractDynamicPageOpenedPayload
{
    public function __construct(
        protected string $type,
        protected string $productId
    ) {
    }

    public function getProductId(): string
    {
        return $this->productId;
    }
}
