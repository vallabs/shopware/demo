<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayload;

use Shopware\Core\Framework\Struct\Struct;

abstract class AbstractPayload extends Struct
{
}
