<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<InteractionEntity>
 */
class InteractionCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return InteractionEntity::class;
    }
}
