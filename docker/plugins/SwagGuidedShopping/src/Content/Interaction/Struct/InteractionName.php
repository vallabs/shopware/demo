<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Struct;

class InteractionName
{
    public const KEEP_ALIVE = 'keep.alive';
    public const PRODUCT_VIEWED = 'product.viewed';
    public const QUICKVIEW_OPENED = 'quickview.opened';
    public const QUICKVIEW_CLOSED = 'quickview.closed';
    public const DYNAMIC_PAGE_OPENED = 'dynamicPage.opened';
    public const DYNAMIC_PRODUCT_PAGE_OPENED = 'dynamicProductPage.opened';
    public const DYNAMIC_PRODUCT_LISTING_PAGE_OPENED = 'dynamicProductListingPage.opened';
    public const DYNAMIC_PRODUCT_LISTING_PAGE_LOADED_MORE = 'dynamicProductListingPage.loadedMore';
    public const DYNAMIC_PAGE_CLOSED = 'dynamicPage.closed';
    public const PAGE_VIEWED = 'page.viewed';

    public const GUIDE_HOVERED = 'guide.hovered';
    public const ATTENDEE_PRODUCT_COLLECTION_LIKED = 'attendee.product.collection.liked';
    public const ATTENDEE_PRODUCT_COLLECTION_DISLIKED = 'attendee.product.collection.disliked';
    public const ATTENDEE_PRODUCT_COLLECTION_REMOVED = 'attendee.product.collection.removed';
    public const REMOTE_CHEKOUT_ACCEPTED = 'remote.checkout.accepted';
    public const REMOTE_CHEKOUT_DENIED = 'remote.checkout.denied';
    public const NOT_STORED_INTERACTIONS = [
        self::KEEP_ALIVE,
    ];
    public const BROADCAST_MODE_TOGGLED = 'broadcastMode.toggled';
    public const SECURITY_ONLY_ALLOWED_FOR_GUIDES = [
        self::DYNAMIC_PAGE_OPENED,
        self::DYNAMIC_PAGE_CLOSED,
        self::DYNAMIC_PRODUCT_PAGE_OPENED,
        self::DYNAMIC_PRODUCT_LISTING_PAGE_OPENED,
        self::DYNAMIC_PRODUCT_LISTING_PAGE_LOADED_MORE,
        self::BROADCAST_MODE_TOGGLED,
    ];
    public const SECURITY_ONLY_ALLOWED_FOR_CLIENTS = [];
}
