<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\GuideHoveredPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Type;

class GuideHoveredPayloadHandler extends AbstractInteractionPayloadHandler
{
    private const INTERACTIONS = [
        InteractionName::GUIDE_HOVERED,
    ];

    /**
     * @return array<int, string>
     */
    public static function getSupportedInteractions(): array
    {
        return self::INTERACTIONS;
    }

    public function getPayloadValidationConstraint(): Collection
    {
        return new Collection(
            [
                'hoveredElementId' => [new Type('string')],
            ]
        );
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(array $payload): GuideHoveredPayload
    {
        return new GuideHoveredPayload($payload['hoveredElementId']);
    }
}
