<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicProductPageOpenedPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DynamicProductPageOpenedPayloadHandler extends AbstractInteractionPayloadHandler
{
    private const INTERACTIONS = [
        InteractionName::DYNAMIC_PRODUCT_PAGE_OPENED,
    ];

    /**
     * @return array<string>
     */
    public static function getSupportedInteractions(): array
    {
        return self::INTERACTIONS;
    }

    public function getPayloadValidationConstraint(): Collection
    {
        return new Collection(
            [
                'type' => [new Type('string'), new NotBlank()],
                'productId' => [new Type('string'), new NotBlank()],
            ]
        );
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(array $payload): DynamicProductPageOpenedPayload
    {
        if (!isset($payload['type']) || !isset($payload['productId'])) {
            throw new \Exception('Invalid payload.');
        }

        return new DynamicProductPageOpenedPayload($payload['type'], $payload['productId']);
    }
}
