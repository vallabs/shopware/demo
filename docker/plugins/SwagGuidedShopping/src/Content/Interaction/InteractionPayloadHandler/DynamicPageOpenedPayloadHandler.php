<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\DynamicPageOpenedPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class DynamicPageOpenedPayloadHandler extends AbstractInteractionPayloadHandler
{
    private const INTERACTIONS = [
        InteractionName::DYNAMIC_PAGE_OPENED,
    ];

    /**
     * @return array<string>
     */
    public static function getSupportedInteractions(): array
    {
        return self::INTERACTIONS;
    }

    public function getPayloadValidationConstraint(): Collection
    {
        return new Collection(
            [
                'type' => [new Type('string'), new NotBlank()],
            ]
        );
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(array $payload): DynamicPageOpenedPayload
    {
        if (!isset($payload['type'])) {
            throw new \Exception('Invalid payload.');
        }

        return new DynamicPageOpenedPayload($payload['type']);
    }
}
