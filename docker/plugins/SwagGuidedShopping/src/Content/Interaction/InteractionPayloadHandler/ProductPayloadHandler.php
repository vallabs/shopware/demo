<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\ProductPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class ProductPayloadHandler extends AbstractInteractionPayloadHandler
{
    private const INTERACTIONS = [
        InteractionName::PRODUCT_VIEWED,
        InteractionName::QUICKVIEW_OPENED,
        InteractionName::QUICKVIEW_CLOSED,
        InteractionName::ATTENDEE_PRODUCT_COLLECTION_DISLIKED,
        InteractionName::ATTENDEE_PRODUCT_COLLECTION_LIKED,
        InteractionName::ATTENDEE_PRODUCT_COLLECTION_REMOVED,
    ];

    /**
     * @return array<string>
     */
    public static function getSupportedInteractions(): array
    {
        return self::INTERACTIONS;
    }

    public function getPayloadValidationConstraint(): Collection
    {
        return new Collection(
            [
                'productId' => [new Type('string'), new NotBlank()],
            ]
        );
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(array $payload): ProductPayload
    {
        if (!isset($payload['productId'])) {
            throw new \Exception('Invalid payload.');
        }

        return new ProductPayload($payload['productId']);
    }
}
