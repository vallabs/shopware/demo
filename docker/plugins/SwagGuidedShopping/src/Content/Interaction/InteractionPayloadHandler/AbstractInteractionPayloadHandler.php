<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractPayload;
use Symfony\Component\Validator\Constraints\Collection;

abstract class AbstractInteractionPayloadHandler
{
    /**
     * @return array<string>
     */
    abstract public static function getSupportedInteractions(): array;

    /**
     * @param array<string, mixed> $payload
     */
    abstract public function createPayload(array $payload): AbstractPayload;

    abstract public function getPayloadValidationConstraint(): Collection;
}
