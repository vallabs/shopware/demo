<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\EmptyPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;

class EmptyPayloadHandler extends AbstractInteractionPayloadHandler
{
    private const INTERACTIONS = [
        InteractionName::REMOTE_CHEKOUT_ACCEPTED,
        InteractionName::REMOTE_CHEKOUT_DENIED,
        InteractionName::DYNAMIC_PAGE_CLOSED,
        InteractionName::KEEP_ALIVE,
    ];

    /**
     * @return array<string>
     */
    public static function getSupportedInteractions(): array
    {
        return self::INTERACTIONS;
    }

    public function getPayloadValidationConstraint(): Collection
    {
        return new Collection([
            'fields' => [],
        ]);
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(array $payload): EmptyPayload
    {
        return new EmptyPayload();
    }
}
