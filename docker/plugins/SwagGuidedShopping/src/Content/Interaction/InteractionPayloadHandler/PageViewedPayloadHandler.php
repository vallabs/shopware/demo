<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\InteractionPayloadHandler;

use SwagGuidedShopping\Content\Interaction\InteractionPayload\PageViewedPayload;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class PageViewedPayloadHandler extends AbstractInteractionPayloadHandler
{
    private const INTERACTIONS = [
        InteractionName::PAGE_VIEWED,
    ];

    /**
     * @return array<string>
     */
    public static function getSupportedInteractions(): array
    {
        return self::INTERACTIONS;
    }

    public function getPayloadValidationConstraint(): Collection
    {
        return new Collection(
            [
                'pageId' => [new Type('string'), new NotBlank()],
                'sectionId' => [new Type('string'), new NotBlank()],
                'slideAlias' => [new Type('int'), new NotBlank()],
            ]
        );
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function createPayload(array $payload): PageViewedPayload
    {
        if (!isset($payload['pageId']) || !isset($payload['sectionId']) || !isset($payload['slideAlias'])) {
            throw new \Exception('Invalid payload.');
        }

        return new PageViewedPayload($payload['pageId'], $payload['sectionId'], $payload['slideAlias']);
    }
}
