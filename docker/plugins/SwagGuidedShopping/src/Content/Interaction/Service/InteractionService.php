<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Service;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Aggregation\Metric\MaxAggregation;
use Shopware\Core\Framework\DataAbstractionLayer\Search\AggregationResult\Metric\MaxResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\NotFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\RangeFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Grouping\FieldGrouping;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionNotAllowedException;
use SwagGuidedShopping\Content\Interaction\InteractionCollection;
use SwagGuidedShopping\Content\Interaction\Exception\InteractionPayloadException;
use SwagGuidedShopping\Content\Interaction\InteractionEntity;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;
use SwagGuidedShopping\Content\Interaction\InteractionHandlerCollection;
use SwagGuidedShopping\Content\Interaction\InteractionPayload\AbstractPayload;
use SwagGuidedShopping\Content\Interaction\InteractionPayloadHandlerCollection;
use SwagGuidedShopping\Content\Interaction\Struct\InteractionName;
use SwagGuidedShopping\Service\Validator\DataValidator;

class InteractionService
{
    /**
     * @param EntityRepository<InteractionCollection> $interactionRepository
     */
    public function __construct(
        private readonly EntityRepository $interactionRepository,
        private readonly DataValidator $validator,
        private readonly InteractionHandlerCollection $interactionHandlers,
        private readonly InteractionPayloadHandlerCollection $payloadHandlers
    ) {
    }

    /**
     * @param array<string, mixed> $payload
     */
    public function handle(
        string $interactionName,
        \DateTimeImmutable $triggeredAt,
        int $timeToLive,
        AttendeeEntity $attendee,
        array $payload,
        SalesChannelContext $salesChannelContext
    ): void
    {
        if (!$this->isInteractionAllowed($interactionName, $attendee->getType(), $attendee->getAppointment()->getMode())) {
            throw new InteractionNotAllowedException();
        }

        $payloadHandler = $this->payloadHandlers->getHandler($interactionName);

        $constraintCollection = $payloadHandler->getPayloadValidationConstraint();

        $violations = $this->validator->validate($payload, $constraintCollection);
        if ($violations->count()) {
            throw new InteractionPayloadException($violations);
        }

        $interaction = $this->createInteraction($interactionName, $triggeredAt, $timeToLive, $attendee, $payloadHandler->createPayload($payload));

        $this->interactionHandlers->handle($interaction, $salesChannelContext);
    }

    public function isInteractionAllowed(string $interactionName, string $attendeeType, string $mode): bool
    {
        if ($mode === AppointmentDefinition::MODE_SELF) {
            return true;
        }

        if ($attendeeType === AttendeeDefinition::TYPE_GUIDE) {
            return !(\in_array($interactionName, InteractionName::SECURITY_ONLY_ALLOWED_FOR_CLIENTS));
        }

        return !(\in_array($interactionName, InteractionName::SECURITY_ONLY_ALLOWED_FOR_GUIDES));
    }

    public function cleanupInteractions(Context $context): int
    {
        $criteria = new Criteria();
        $now = new \DateTime('now');

        $criteria->addFilter(new RangeFilter('expiresAt', [RangeFilter::LTE => $now->format('Y-m-d H:i:s')]));
        $criteria->addFilter(new NotFilter(MultiFilter::CONNECTION_AND, [new EqualsFilter('expiresAt', null)]));

        $outdatedInteractions = $this->interactionRepository->searchIds($criteria, $context);

        $cleanableIds = [];
        foreach ($outdatedInteractions->getIds() as $id) {
            $cleanableIds[] = ['id' => $id];
        }

        $cleanableIdsCount = \count($cleanableIds);
        if (!$cleanableIdsCount) {
            return $cleanableIdsCount;
        }

        $this->interactionRepository->delete($cleanableIds, $context);

        return $cleanableIdsCount;
    }

    public function getLastInteractionDateTime(string $appointmentId, Context $context): ?\DateTime
    {
        $criteria = new Criteria();
        $criteria->addAggregation(new MaxAggregation('lastInteractionTriggered', 'triggeredAt'));
        $criteria->addFilter(new EqualsFilter('attendee.appointmentId', $appointmentId));
        $criteria->setLimit(1);

        $res = $this->interactionRepository->search($criteria, $context);

        /** @var MaxResult|null $lastInteractionResult */
        $lastInteractionResult = $res->getAggregations()->get('lastInteractionTriggered');
        if (!$lastInteractionResult || !$lastInteractionResult->getMax()) {
            return null;
        }

        $lastInteractionTriggeredTime = (string) $lastInteractionResult->getMax();

        return new \DateTime($lastInteractionTriggeredTime);
    }

    /**
     * @param array<string> $interactionNames
     * @param array<FieldSorting> $sorting
     * @param array<string> $groupFields
     *
     * @return Interaction[]
     */
    public function getInteractions(
        array $interactionNames,
        Context $context,
        ?string $attendeeId,
        ?string $appointmentId = null,
        array $sorting = [],
        array $groupFields = [],
        ?string $attendeeType = null
    ): array
    {
        $criteria = new Criteria();
        $nameFilter = new EqualsAnyFilter('guided_shopping_interaction.name', $interactionNames);

        if ($attendeeId) {
            $criteria->addFilter(
                new MultiFilter(
                    MultiFilter::CONNECTION_AND, [
                        $nameFilter,
                        new EqualsFilter('attendeeId', $attendeeId),
                    ]
                )
            );
        } else {
            $criteria->addFilter($nameFilter);
        }

        if ($appointmentId) {
            $criteria->addFilter(
                new MultiFilter(
                    MultiFilter::CONNECTION_AND, [
                        $nameFilter,
                        new EqualsFilter('attendee.appointmentId', $appointmentId),
                    ]
                )
            );
        }

        foreach ($groupFields as $field) {
            $criteria->addGroupField(new FieldGrouping($field));
        }

        if ($attendeeType) {
            $criteria->addFilter(new EqualsFilter('attendee.type', $attendeeType));
        }

        if (\count($sorting)) {
            $criteria->resetSorting();

            foreach ($sorting as $sort) {
                $criteria->addSorting($sort);
            }
        } else {
            $criteria->addSorting(new FieldSorting('triggeredAt', FieldSorting::DESCENDING));
        }

        $interactionData = $this->interactionRepository->search($criteria, $context)->getElements();

        $interactions = [];
        /** @var InteractionEntity $interaction */
        foreach ($interactionData as $interaction) {
            $payload = $interaction->getPayload();
            if (!$payload) {
                $payload = [];
            }

            $payloadHandler = $this->payloadHandlers->getHandler($interaction->getName());

            $constraintCollection = $payloadHandler->getPayloadValidationConstraint();

            $violations = $this->validator->validate($payload, $constraintCollection);
            if ($violations->count()) {
                continue;
            }

            $interactions[] = $this->createInteraction(
                $interaction->getName(),
                $interaction->getTriggeredAt(),
                -1,
                $interaction->getAttendee(),
                $payloadHandler->createPayload($payload)
            );
        }

        return $interactions;
    }

    private function createInteraction(
        string $interactionName,
        \DateTimeImmutable $triggeredAt,
        int $timeToLive,
        AttendeeEntity $attendee,
        AbstractPayload $payload
    ): Interaction
    {
        return new Interaction($interactionName, $triggeredAt, $timeToLive, $attendee, $payload);
    }
}
