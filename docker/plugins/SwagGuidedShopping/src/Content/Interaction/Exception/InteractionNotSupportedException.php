<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class InteractionNotSupportedException extends ShopwareHttpException
{
    public function __construct(string $handler, string $interaction)
    {
        parent::__construct(\sprintf('The interaction %s is not support by the handler %s', $interaction, $handler));
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__INTERACTION_NOT_SUPPORTED;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
