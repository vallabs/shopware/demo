<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use SwagGuidedShopping\Service\Validator\ViolationList;
use Symfony\Component\HttpFoundation\Response;

class InteractionPayloadException extends ShopwareHttpException
{
    public function __construct(
        private readonly ViolationList $violationsCollection
    ) {
        $message = "The provided interaction payload is not valid \n";
        $message .= $violationsCollection;
        parent::__construct($message);
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__INTERACTION_ADD_PAYLOAD_VIOLATION;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getViolationsCollection(): ViolationList
    {
        return $this->violationsCollection;
    }
}
