<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class InteractionNotAllowedException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct('This interaction is not allowed.');
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__INTERACTION_NOT_ALLOWED;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_METHOD_NOT_ALLOWED;
    }
}
