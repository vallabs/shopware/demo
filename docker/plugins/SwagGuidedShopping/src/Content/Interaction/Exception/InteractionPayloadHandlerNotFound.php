<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class InteractionPayloadHandlerNotFound extends ShopwareHttpException
{
    /**
     * @param array<string> $possibleHandlers
     */
    public function __construct(string $interaction, array $possibleHandlers)
    {
        $possibleInteractionString = \implode(', ', $possibleHandlers);
        parent::__construct(\sprintf('There is no interaction payload handler for the interaction with the name %s. Possible interactions are (%s)', $interaction, $possibleInteractionString));
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__CREATE_INTERACTION_PAYLOAD_HANDLER_NOT_FOUND;
    }
}
