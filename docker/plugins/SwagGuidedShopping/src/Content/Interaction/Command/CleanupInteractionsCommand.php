<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction\Command;

use Shopware\Core\Framework\Context;
use SwagGuidedShopping\Content\Interaction\Service\InteractionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanupInteractionsCommand extends Command
{
    protected static $defaultName = 'guidedshopping:cleanup:interactions';

    public function __construct(
        private readonly InteractionService $interactionService
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Start deleting outdated interactions.');
        $deleteCount = $this->interactionService->cleanupInteractions(Context::createDefaultContext());
        $output->writeln(\sprintf('Deleted %s interactions.', $deleteCount));

        return 0;
    }
}
