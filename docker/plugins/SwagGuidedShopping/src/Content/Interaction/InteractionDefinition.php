<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeDefinition;

class InteractionDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_interaction';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return InteractionEntity::class;
    }

    public function getCollectionClass(): string
    {
        return InteractionCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new StringField('name', 'name'))->addFlags(new Required()),
                (new DateTimeField('expires_at', 'expiresAt')),
                (new DateTimeField('triggered_at', 'triggeredAt'))->addFlags(new Required()),
                new JsonField('payload', 'payload'),
                (new FkField('attendee_id', 'attendeeId', AttendeeDefinition::class))->addFlags(new Required()),

                new ManyToOneAssociationField('attendee', 'attendee_id', AttendeeDefinition::class, 'id', true),
            ]
        );
    }
}
