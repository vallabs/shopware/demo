<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Interaction;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\AbstractInteractionHandler;
use SwagGuidedShopping\Content\Interaction\InteractionHandler\Interaction;

class InteractionHandlerCollection
{
    /**
     * @var array<AbstractInteractionHandler>
     */
    private array $handlers = [];

    /**
     * @param iterable<AbstractInteractionHandler> $interactionHandlers
     */
    public function __construct(iterable $interactionHandlers)
    {
        foreach ($interactionHandlers as $interactionHandler) {
            $this->handlers[] = $interactionHandler;
        }
    }

    public function handle(Interaction $interaction, SalesChannelContext $context): void
    {
        /** @var AbstractInteractionHandler $handler */
        foreach ($this->handlers as $handler) {
            if ($handler->supports($interaction)) {
                $handler->handle($interaction, $context);
            }
        }
    }
}
