<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class PresentationNotFoundException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct(
            \sprintf('Presentation not found.')
        );
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__PRESENTATION_NOT_FOUND;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }
}
