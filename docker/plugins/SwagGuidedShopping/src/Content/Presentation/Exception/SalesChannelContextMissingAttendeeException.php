<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class SalesChannelContextMissingAttendeeException extends ShopwareHttpException
{
    public function __construct()
    {
        parent::__construct(
            \sprintf('Sales channel context missing attendee.')
        );
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__SALES_CHANNEL_CONTEXT_MISSING_ATTENDEE;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }
}
