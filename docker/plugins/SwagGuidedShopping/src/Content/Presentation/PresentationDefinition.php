<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CreatedByField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\SearchRanking;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ParentAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ParentFkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\UpdatedByField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\VersionField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\User\UserDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationDefinition;

class PresentationDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_presentation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return PresentationEntity::class;
    }

    public function getCollectionClass(): string
    {
        return PresentationCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new ApiAware(), new Required(), new PrimaryKey()),
                (new VersionField())->addFlags(new ApiAware()),
                (new BoolField('active', 'active'))->addFlags(new ApiAware()),
                (new ParentFkField(self::class))->addFlags(new ApiAware()),
                (new ReferenceVersionField(self::class, 'parent_version_id'))->addFlags(new ApiAware(), new Required()),
                (new TranslatedField('name'))->addFlags(new ApiAware(), new SearchRanking(SearchRanking::HIGH_SEARCH_RANKING)),
                (new TranslatedField('customFields'))->addFlags(new ApiAware(), new Inherited()),
                (new CreatedByField([Context::CRUD_API_SCOPE, Context::SYSTEM_SCOPE]))->addFlags(new ApiAware(), new Required()),
                (new UpdatedByField([Context::CRUD_API_SCOPE, Context::SYSTEM_SCOPE]))->addFlags(new ApiAware()),
                (new TranslationsAssociationField(PresentationTranslationDefinition::class, 'guided_shopping_presentation_id'))->addFlags(new ApiAware(), new Required()),
                (new ParentAssociationField(self::class, 'id'))->addFlags(new ApiAware()),
                (new OneToManyAssociationField('appointments', AppointmentDefinition::class, 'guided_shopping_presentation_id'))->addFlags(new ApiAware(), new CascadeDelete(false)),
                (new OneToManyAssociationField('cmsPages', PresentationCmsPageDefinition::class, 'presentation_id'))->addFlags(new ApiAware(), new CascadeDelete(false)),
                (new ManyToOneAssociationField('createdBy', 'created_by_id', UserDefinition::class, 'id', true))->addFlags(new SearchRanking(SearchRanking::HIGH_SEARCH_RANKING)),
                (new ManyToOneAssociationField('updatedBy', 'updated_by_id', UserDefinition::class, 'id', true)),
            ]
        );
    }
}
