<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\User\UserEntity;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation\PresentationTranslationCollection;

class PresentationEntity extends Entity
{
    use EntityIdTrait;
    use EntityCustomFieldsTrait;

    protected bool $active;

    protected string $name;

    protected PresentationTranslationCollection $translations;

    protected AppointmentCollection $appointments;

    protected PresentationCmsPageCollection $cmsPages;

    protected ?string $parentId = null;

    protected ?PresentationEntity $parent = null;

    protected ?string $createdById;

    protected ?UserEntity $createdBy;

    protected ?string $updatedById;

    protected ?UserEntity $updatedBy;

    public function __construct()
    {
        $this->appointments = new AppointmentCollection();
        $this->translations = new PresentationTranslationCollection();
        $this->cmsPages = new PresentationCmsPageCollection();
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function getTranslations(): PresentationTranslationCollection
    {
        return $this->translations;
    }

    public function setTranslations(PresentationTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    public function getAppointments(): AppointmentCollection
    {
        return $this->appointments;
    }

    public function setAppointments(AppointmentCollection $appointments): void
    {
        $this->appointments = $appointments;
    }

    /**
     * @return PresentationCmsPageCollection
     */
    public function getCmsPages(): PresentationCmsPageCollection
    {
        return $this->cmsPages;
    }

    public function setCmsPages(PresentationCmsPageCollection $cmsPages): void
    {
        $this->cmsPages = $cmsPages;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getParentId(): ?string
    {
        return $this->parentId;
    }

    public function setParentId(?string $parentId): void
    {
        $this->parentId = $parentId;
    }

    public function getParent(): ?PresentationEntity
    {
        return $this->parent;
    }

    public function setParent(?PresentationEntity $parent): void
    {
        $this->parent = $parent;
    }

    public function getCreatedById(): ?string
    {
        return $this->createdById;
    }

    public function setCreatedById(?string $createdById): void
    {
        $this->createdById = $createdById;
    }

    public function getCreatedBy(): ?UserEntity
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserEntity $createdBy): void
    {
        $this->createdBy = $createdBy;
    }

    public function getUpdatedById(): ?string
    {
        return $this->updatedById;
    }

    public function setUpdatedById(?string $updatedById): void
    {
        $this->updatedById = $updatedById;
    }

    public function getUpdatedBy(): ?UserEntity
    {
        return $this->updatedBy;
    }

    public function setUpdatedBy(?UserEntity $updatedBy): void
    {
        $this->updatedBy = $updatedBy;
    }
}
