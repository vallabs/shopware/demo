<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;

class PresentationTranslationEntity extends TranslationEntity
{
    use EntityCustomFieldsTrait;

    protected ?string $name = null;

    protected string $guidedShoppingPresentationId;

    protected PresentationEntity $guidedShoppingPresentation;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getGuidedShoppingPresentationId(): string
    {
        return $this->guidedShoppingPresentationId;
    }

    public function setGuidedShoppingPresentationId(string $guidedShoppingPresentationId): void
    {
        $this->guidedShoppingPresentationId = $guidedShoppingPresentationId;
    }

    public function getGuidedShoppingPresentation(): PresentationEntity
    {
        return $this->guidedShoppingPresentation;
    }

    public function setGuidedShoppingPresentation(PresentationEntity $guidedShoppingPresentation): void
    {
        $this->guidedShoppingPresentation = $guidedShoppingPresentation;
    }
}
