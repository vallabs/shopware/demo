<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPageTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\TranslationEntity;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;

class PresentationCmsPageTranslationEntity extends TranslationEntity
{
    use EntityCustomFieldsTrait;

    protected ?string $name = null;

    protected string $guidedShoppingPresentationCmsPageId;

    protected PresentationCmsPageEntity $guidedShoppingPresentationCmsPage;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getGuidedShoppingPresentationCmsPageId(): string
    {
        return $this->guidedShoppingPresentationCmsPageId;
    }

    public function setGuidedShoppingPresentationCmsPageId(string $guidedShoppingPresentationCmsPageId): void
    {
        $this->guidedShoppingPresentationCmsPageId = $guidedShoppingPresentationCmsPageId;
    }

    public function getGuidedShoppingPresentationCmsPage(): PresentationCmsPageEntity
    {
        return $this->guidedShoppingPresentationCmsPage;
    }

    public function setGuidedShoppingPresentationCmsPage(PresentationCmsPageEntity $guidedShoppingPresentationCmsPage): void
    {
        $this->guidedShoppingPresentationCmsPage = $guidedShoppingPresentationCmsPage;
    }
}
