<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPageTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;

class PresentationCmsPageTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'guided_shopping_presentation_cms_page_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return PresentationCmsPageTranslationCollection::class;
    }

    public function getEntityClass(): string
    {
        return PresentationCmsPageTranslationEntity::class;
    }

    protected function getParentDefinitionClass(): string
    {
        return PresentationCmsPageDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('title', 'title', ))->addFlags(new ApiAware()),
            (new CustomFields())->addFlags(new ApiAware()),
            (new JsonField('slot_config', 'slotConfig'))->addFlags(new ApiAware()),
        ]);
    }
}
