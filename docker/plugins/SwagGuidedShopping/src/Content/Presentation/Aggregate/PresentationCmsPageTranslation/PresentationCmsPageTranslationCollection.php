<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPageTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<PresentationCmsPageTranslationEntity>
 */
class PresentationCmsPageTranslationCollection extends EntityCollection
{
    public function filterByLanguageId(string $id): self
    {
        return $this->filter(function (PresentationCmsPageTranslationEntity $customTranslationEntity) use ($id) {
            return $customTranslationEntity->getLanguageId() === $id;
        });
    }

    protected function getExpectedClass(): string
    {
        return PresentationCmsPageTranslationEntity::class;
    }
}
