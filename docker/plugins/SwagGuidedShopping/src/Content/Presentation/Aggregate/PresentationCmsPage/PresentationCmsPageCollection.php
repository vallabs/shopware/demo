<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage;

use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<PresentationCmsPageEntity>
 */
class PresentationCmsPageCollection extends EntityCollection
{
    /**
     * @return array<string>
     */
    public function getPresentationIds(): array
    {
        return $this->fmap(function (PresentationCmsPageEntity $presentationCmsPage) {
            return $presentationCmsPage->getPresentationId();
        });
    }

    public function filterByPresentationId(string $id): self
    {
        return $this->filter(function (PresentationCmsPageEntity $presentationCmsPage) use ($id) {
            return $presentationCmsPage->getPresentationId() === $id;
        });
    }

    public function filterByPresentationVersionId(string $presentationVersionId, ?bool $isInstantListing = null): self
    {
        return $this->filter(function (PresentationCmsPageEntity $presentationCmsPage) use ($presentationVersionId, $isInstantListing) {
            return $presentationCmsPage->getGuidedShoppingPresentationVersionId() === $presentationVersionId
                && (\is_null($isInstantListing) || $presentationCmsPage->isInstantListing() === $isInstantListing);
        });
    }

    /**
     * @return array<string>
     */
    public function getCmsPageIds(): array
    {
        return $this->fmap(function (PresentationCmsPageEntity $presentationCmsPage) {
            return $presentationCmsPage->getCmsPageId();
        });
    }

    public function filterByCmsPageId(string $id): self
    {
        return $this->filter(function (PresentationCmsPageEntity $presentationCmsPage) use ($id) {
            return $presentationCmsPage->getCmsPageId() === $id;
        });
    }

    public function getCmsPage(): CmsPageCollection
    {
        return new CmsPageCollection(
            $this->fmap(function (PresentationCmsPageEntity $presentationCmsPage) {
                return $presentationCmsPage->getCmsPage();
            })
        );
    }

    /**
     * @return array<string, int>
     */
    public function getPosition(): array
    {
        return $this->fmap(function (PresentationCmsPageEntity $presentationCmsPage) {
            return $presentationCmsPage->getPosition();
        });
    }

    public function sortByPosition(): void
    {
        $this->sort(function (PresentationCmsPageEntity $pageA, PresentationCmsPageEntity $pageB) {
            return ($pageA->getPosition() > $pageB->getPosition()) ? 1 : -1;
        });
    }

    public function getApiAlias(): string
    {
        return 'guided_shopping_presentation_cms_page_collection';
    }

    protected function getExpectedClass(): string
    {
        return PresentationCmsPageEntity::class;
    }
}
