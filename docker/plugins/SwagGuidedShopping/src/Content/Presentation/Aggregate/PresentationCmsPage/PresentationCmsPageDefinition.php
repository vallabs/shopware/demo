<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage;

use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\ProductStream\ProductStreamDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IntField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\JsonField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ReferenceVersionField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPageTranslation\PresentationCmsPageTranslationDefinition;
use SwagGuidedShopping\Content\Presentation\PresentationDefinition;

class PresentationCmsPageDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_presentation_cms_page';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return PresentationCmsPageCollection::class;
    }

    public function getEntityClass(): string
    {
        return PresentationCmsPageEntity::class;
    }

    public function since(): ?string
    {
        return '6.0.0.0';
    }

    protected function getParentDefinitionClass(): ?string
    {
        return PresentationDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new IdField('id', 'id'))->addFlags(new ApiAware(), new PrimaryKey(), new Required()),

            (new FkField('presentation_id', 'presentationId', PresentationDefinition::class))->addFlags(new ApiAware(), new Required()),
            (new ReferenceVersionField(PresentationDefinition::class))->addFlags(new ApiAware()),
            (new ManyToOneAssociationField('presentation', 'presentation_id', PresentationDefinition::class, 'id', false)),

            (new FkField('cms_page_id', 'cmsPageId', CmsPageDefinition::class))->addFlags(new ApiAware(), new Required()),
            (new ReferenceVersionField(CmsPageDefinition::class))->addFlags(new ApiAware()),
            (new ManyToOneAssociationField('cmsPage', 'cms_page_id', CmsPageDefinition::class, 'id', true))->addFlags(new ApiAware()),

            (new TranslatedField('title'))->addFlags(new ApiAware()),
            (new TranslatedField('customFields'))->addFlags(new ApiAware(), new Inherited()),
            (new TranslatedField('slotConfig'))->addFlags(new Inherited()),
            (new TranslationsAssociationField(PresentationCmsPageTranslationDefinition::class, 'guided_shopping_presentation_cms_page_id'))->addFlags(new ApiAware(), new Required()),

            (new FkField('product_id', 'productId', ProductDefinition::class))->addFlags(new ApiAware()),
            (new ReferenceVersionField(ProductDefinition::class))->addFlags(new ApiAware()),
            (new ManyToOneAssociationField('product', 'product_id', ProductDefinition::class, 'id', false)),

            (new FkField('product_stream_id', 'productStreamId', ProductStreamDefinition::class))->addFlags(new ApiAware()),
            (new ManyToOneAssociationField('productStream', 'product_stream_id', ProductStreamDefinition::class, 'id', false)),

            (new JsonField('picked_product_ids', 'pickedProductIds'))->addFlags(new ApiAware()),

            (new IntField('position', 'position'))->addFlags(new ApiAware()),
            (new BoolField('is_instant_listing', 'isInstantListing'))->addFlags(new ApiAware()),
            (new CustomFields())->addFlags(new ApiAware()),
        ]);
    }
}
