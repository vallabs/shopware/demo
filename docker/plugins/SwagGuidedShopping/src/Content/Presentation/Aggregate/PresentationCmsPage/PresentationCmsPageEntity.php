<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage;

use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Product\ProductEntity;
use Shopware\Core\Content\ProductStream\ProductStreamEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;

class PresentationCmsPageEntity extends Entity
{
    use EntityIdTrait;
    use EntityCustomFieldsTrait;

    protected string $presentationId;

    protected string $cmsPageId;

    protected ?string $title = null;

    protected ?string $productId = null;

    protected ?string $productStreamId = null;

    protected string $productVersionId;

    protected int $position;

    protected bool $isInstantListing = false;

    protected ?CmsPageEntity $cmsPage = null;

    protected ?PresentationEntity $presentation = null;

    protected ?ProductEntity $product = null;

    protected ?ProductStreamEntity $productStream = null;

    /**
     * @var array<string>|null
     */
    protected ?array $pickedProductIds = null;

    protected ?string $guidedShoppingPresentationVersionId = null;

    public function getPresentationId(): string
    {
        return $this->presentationId;
    }

    public function setPresentationId(string $presentationId): void
    {
        $this->presentationId = $presentationId;
    }

    public function getCmsPageId(): string
    {
        return $this->cmsPageId;
    }

    public function setCmsPageId(string $cmsPageId): void
    {
        $this->cmsPageId = $cmsPageId;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function isInstantListing(): bool
    {
        return $this->isInstantListing;
    }

    public function setInstantListing(bool $isInstantListing): void
    {
        $this->isInstantListing = $isInstantListing;
    }

    public function getCmsPage(): ?CmsPageEntity
    {
        return $this->cmsPage;
    }

    public function setCmsPage(?CmsPageEntity $cmsPage): void
    {
        $this->cmsPage = $cmsPage;
    }

    public function getPresentation(): ?PresentationEntity
    {
        return $this->presentation;
    }

    public function setPresentation(?PresentationEntity $presentation): void
    {
        $this->presentation = $presentation;
    }

    public function getProductId(): ?string
    {
        return $this->productId;
    }

    public function setProductId(string $productId): void
    {
        $this->productId = $productId;
    }

    public function getProductVersionId(): string
    {
        return $this->productVersionId;
    }

    public function setProductVersionId(string $productVersionId): void
    {
        $this->productVersionId = $productVersionId;
    }

    public function getProduct(): ?ProductEntity
    {
        return $this->product;
    }

    public function setProduct(?ProductEntity $product): void
    {
        $this->product = $product;
    }

    public function getProductStreamId(): ?string
    {
        return $this->productStreamId;
    }

    public function setProductStreamId(string $productStreamId): void
    {
        $this->productStreamId = $productStreamId;
    }

    public function getProductStream(): ?ProductStreamEntity
    {
        return $this->productStream;
    }

    public function setProductStream(ProductStreamEntity $productStream): void
    {
        $this->productStream = $productStream;
    }

    /**
     * @return array<string>|null
     */
    public function getPickedProductIds(): ?array
    {
        return $this->pickedProductIds;
    }

    /**
     * @param array<string>|null $pickedProductIds
     */
    public function setPickedProductIds(?array $pickedProductIds): void
    {
        $this->pickedProductIds = $pickedProductIds;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getGuidedShoppingPresentationVersionId(): ?string
    {
        return $this->guidedShoppingPresentationVersionId;
    }

    public function setGuidedShoppingPresentationVersionId(?string $version): void
    {
        $this->guidedShoppingPresentationVersionId = $version;
    }
}
