<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure;

use Shopware\Core\Framework\Struct\Collection;

/**
 * @extends Collection<PresentationPageResult>
 */
class PresentationPageResultCollection extends Collection
{
}
