<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure;

use Shopware\Core\System\SalesChannel\StoreApiResponse;

class PresentationStructureResponse extends StoreApiResponse
{
    public function __construct(PresentationPageResults $page)
    {
        parent::__construct($page);
    }
}
