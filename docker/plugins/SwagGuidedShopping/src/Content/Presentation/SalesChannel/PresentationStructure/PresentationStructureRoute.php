<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Cms\Aggregate\CmsSectionTranslation\CmsSectionTranslationCollection;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Presentation\Exception\PresentationNotFoundException;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\Presentation\Service\PresentationVersionHandler;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class PresentationStructureRoute extends AbstractPresentationStructureRoute
{
    public const PRESENTATION_PAGE_ROUTE = 'frontend.presentation.page';

    /**
     * @param EntityRepository<PresentationCollection> $presentationRepository
     * @param EntityRepository<CmsPageCollection> $cmsPageRepository
     */
    public function __construct(
        private readonly EntityRepository $presentationRepository,
        private readonly EntityRepository $cmsPageRepository,
        private readonly PresentationVersionHandler $presentationVersionHandler,
        private readonly NotesElementHandler $notesElementHandler
    ) {
    }

    public function getDecorated(): AbstractPresentationStructureRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/presentation', name: 'store-api.guided-shopping.presentation-structure', defaults: ['attendee_required' => true], methods: ['GET'])]
    public function structure(Request $request, SalesChannelContext $salesChannelContext): PresentationStructureResponse
    {
        /** @var AttendeeEntity|null $attendee */
        $attendee = $salesChannelContext->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);

        if (!$attendee) {
            throw new SalesChannelContextMissingAttendeeException();
        }

        $appointment = $attendee->getAppointment();
        $presentationId = $appointment->getPresentationId();
        $context = $salesChannelContext->getContext();

        /** @var array<string> $ids */
        $ids = [$presentationId];
        $criteria = new Criteria($ids);

        $mappedPresentationVersionId = $this->presentationVersionHandler->createPresentationVersion($appointment, $context);
        if ($mappedPresentationVersionId !== Defaults::LIVE_VERSION) {
            $this->presentationVersionHandler->updatePresentationCmsPages($appointment, $mappedPresentationVersionId, $context);
        }

        $criteria->getAssociation('cmsPages')
            ->addSorting(new FieldSorting('position'))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', $mappedPresentationVersionId));

        /** @var PresentationEntity|null $presentation */
        $presentation = $this->presentationRepository->search($criteria, $context)->first();
        if (!$presentation) {
            throw new PresentationNotFoundException();
        }

        $navigation = [];
        $index = 1;
        $results = new PresentationPageResults();

        foreach ($presentation->getCmsPages() as $cmsPageRelation) {
            $criteria = new Criteria([$cmsPageRelation->getCmsPageId()]);
            $criteria->addAssociation('sections.blocks.slots');
            $criteria->addAssociation('sections.translations');
            $criteria->getAssociation('sections')->addSorting(new FieldSorting('position'));
            $criteria->getAssociation('sections.blocks')->addSorting(new FieldSorting('position'));

            /** @var CmsPageEntity|null $cmsPage */
            $cmsPage = $this->cmsPageRepository->search($criteria, $context)->first();
            if (!$cmsPage) {
                continue;
            }

            $languageId = $context->getLanguageId();
            foreach ($cmsPage->getSections() ?? [] as $section) {
                /**
                 * Remove note slot from section
                 */
                $notes = [];
                $sectionNotes = $this->notesElementHandler->removeNotes($section);
                foreach ($sectionNotes as $note) {
                    $notes[] = $note;
                }

                $cmsPageSectionClone = clone $cmsPage;
                $cmsPageSectionClone->setSections(new CmsSectionCollection());

                foreach ($section->getBlocks()?->getSlots() ?? [] as $slot) {
                    if (isset($cmsPageRelation->getTranslation('slotConfig')[$slot->getId()])) {
                        $cmsPageRelationSlotConfigTrans = $cmsPageRelation->getTranslation('slotConfig')[$slot->getId()];

                        if (isset($cmsPageRelationSlotConfigTrans['products'])) {
                            $slotConfig = $slot->getConfig();
                            $slotConfigTranslated  = $slot->getTranslated()['config'] ?? [];
                            $slotConfig['products']['value'] = $slotConfigTranslated['products']['value'] = $cmsPageRelationSlotConfigTrans['products']['value'];

                            $slot->setConfig($slotConfig);
                            $slot->setTranslated($slotConfigTranslated);
                        }
                    }
                }

                $sectionsNew = $cmsPageSectionClone->getSections();
                $sectionsNew?->add($section);

                $pageResult = new PresentationPageResult();
                $pageResult->setResourceType(self::PRESENTATION_PAGE_ROUTE);
                $pageResult->setResourceIdentifier($presentation->getId());
                $pageResult->setCmsPage($cmsPageSectionClone);

                $results->getCmsPageResults()->add($pageResult);

                $relationTitle = $cmsPageRelation->getTranslation('title') ?? $cmsPageRelation->getTitle();

                $sectionNavigation = [
                    'index' => $index,
                    'sectionId' => $section->getId(),
                    'sectionName' => $this->getSectionName($section, $languageId),
                    'groupId' => $cmsPageRelation->getId(), // presentationCmsPage id
                    'groupName' => $relationTitle ?: $cmsPage->getTranslation('name') ?? $cmsPage->getName(), // presentationCmsPage/cmsPage name
                    'cmsPageId' => $cmsPage->getId(),
                    'isInstantListing' => $cmsPageRelation->isInstantListing(),
                    'pickedProductsCount' => $cmsPageRelation->isInstantListing() && $cmsPageRelation->getPickedProductIds() ? \count($cmsPageRelation->getPickedProductIds()) : 0,
                ];

                if ($attendee->isGuide()) {
                    $sectionNavigation['notes'] = $notes;
                }

                $navigation[] = $sectionNavigation;

                ++$index;
            }
        }

        $results->setNavigation($navigation);

        return new PresentationStructureResponse($results);
    }

    private function getSectionName(CmsSectionEntity $section, string $languageId): ?string
    {
        /** @var CmsSectionTranslationCollection|null $sectionTranslations */
        $sectionTranslations = $section->getExtension('translations');
        $sectionTranslation = $sectionTranslations ? $sectionTranslations->get("{$section->getId()}-{$languageId}") : null;

        return $sectionTranslation ? $sectionTranslation->getName() : $section->getName();
    }
}
