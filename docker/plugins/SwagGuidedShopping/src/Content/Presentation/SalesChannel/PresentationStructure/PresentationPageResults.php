<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure;

use SwagShopwarePwa\Pwa\PageResult\AbstractPageResult;

class PresentationPageResults extends AbstractPageResult
{
    protected PresentationPageResultCollection $cmsPageResults;

    /**
     * @var iterable<int, array<int|string, mixed>>
     */
    protected iterable $navigation;

    public function __construct()
    {
        $this->cmsPageResults = new PresentationPageResultCollection();
    }

    public function getCmsPageResults(): PresentationPageResultCollection
    {
        return $this->cmsPageResults;
    }

    public function setCmsPageResults(PresentationPageResultCollection $cmsPageResults): void
    {
        $this->cmsPageResults = $cmsPageResults;
    }

    /**
     * @return iterable<int, array<int|string, mixed>>
     */
    public function getNavigation(): iterable
    {
        return $this->navigation;
    }

    /**
     * @param iterable<int, array<int|string, mixed>>  $navigation
     */
    public function setNavigation(iterable $navigation): void
    {
        $this->navigation = $navigation;
    }
}
