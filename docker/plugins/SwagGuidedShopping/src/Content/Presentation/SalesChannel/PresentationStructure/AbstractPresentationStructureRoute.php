<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractPresentationStructureRoute
{
    abstract public function getDecorated(): AbstractPresentationStructureRoute;

    abstract public function structure(Request $request, SalesChannelContext $context): PresentationStructureResponse;
}
