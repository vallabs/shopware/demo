<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationStructure;

use SwagShopwarePwa\Pwa\PageResult\AbstractPageResult;

class PresentationPageResult extends AbstractPageResult
{
}
