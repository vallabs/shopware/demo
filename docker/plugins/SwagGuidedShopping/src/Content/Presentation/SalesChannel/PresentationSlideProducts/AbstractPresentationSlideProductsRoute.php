<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideProducts;

use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingRouteResponse;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractPresentationSlideProductsRoute
{
    abstract public function getDecorated(): AbstractPresentationSlideProductsRoute;

    abstract public function products(Request $request, SalesChannelContext $context, Criteria $criteria): ProductListingRouteResponse;
}
