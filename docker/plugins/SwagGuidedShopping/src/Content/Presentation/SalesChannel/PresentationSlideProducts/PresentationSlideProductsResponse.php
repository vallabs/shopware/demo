<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideProducts;

use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingResult;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class PresentationSlideProductsResponse extends StoreApiResponse
{
    public function __construct(ProductListingResult $listingResult)
    {
        parent::__construct($listingResult);
    }
}
