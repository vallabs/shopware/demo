<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideProducts;

use Shopware\Core\Content\Product\Aggregate\ProductVisibility\ProductVisibilityDefinition;
use Shopware\Core\Content\Product\Events\ProductListingCriteriaEvent;
use Shopware\Core\Content\Product\Events\ProductListingResultEvent;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingLoader;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingResult;
use Shopware\Core\Content\Product\SalesChannel\Listing\ProductListingRouteResponse;
use Shopware\Core\Content\Product\SalesChannel\ProductAvailableFilter;
use Shopware\Core\Content\ProductStream\Service\ProductStreamBuilder;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\EntitySearchResult;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsAnyFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\MultiFilter;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagGuidedShopping\Content\Appointment\Event\AfterProductListingLoadedEvent;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class PresentationSlideProductsRoute extends AbstractPresentationSlideProductsRoute
{
    /**
     * @param EntityRepository<PresentationCmsPageCollection> $presentationCmsRepository
     */
    public function __construct(
        private readonly EntityRepository $presentationCmsRepository,
        private readonly ProductListingLoader $listingLoader,
        private readonly ProductStreamBuilder $productStreamBuilder,
        private readonly EventDispatcherInterface $eventDispatcher
    ) {
    }

    public function getDecorated(): AbstractPresentationSlideProductsRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/presentation/{presentationCmsPageId}/slide/{sectionId}/products', name: 'store-api.guided-shopping.presentation-products', defaults: ['attendee_required' => true, '_entity' => 'product'], methods: ['POST'])]
    public function products(Request $request, SalesChannelContext $context, Criteria $criteria): ProductListingRouteResponse
    {
        $this->eventDispatcher->dispatch(
            new ProductListingCriteriaEvent($request, $criteria, $context)
        );

        $presentationCmsPageId = (string) $request->get('presentationCmsPageId');
        $sectionId = (string) $request->get('sectionId');

        if ($sectionId === '') {
            throw RoutingException::missingRequestParameter('sectionId');
        }
        if ($presentationCmsPageId === '') {
            throw RoutingException::missingRequestParameter('presentationCmsPageId');
        }

        $cmsCriteria = new Criteria([$presentationCmsPageId]);
        /** @var PresentationCmsPageEntity|null $cmsPageRelation */
        $cmsPageRelation = $this->presentationCmsRepository->search($cmsCriteria, $context->getContext())->first();

        $criteria->addFilter(
            new ProductAvailableFilter($context->getSalesChannel()->getId(), ProductVisibilityDefinition::VISIBILITY_ALL)
        );

        try {
            $streamId = $cmsPageRelation ? $this->extendCriteria($context, $criteria, $cmsPageRelation) : null;
        } catch (\Exception $e) {
            $entities = new EntitySearchResult(ProductDefinition::ENTITY_NAME, 0, new EntityCollection([]), null, new Criteria(), $context->getContext());
            $result = ProductListingResult::createFrom($entities);

            return new ProductListingRouteResponse($result);
        }

        $criteria->resetAggregations();

        if (!$streamId) { // if no stream is given, we need to filter by the ids
            /** @var string[] $ids */
            $ids = $criteria->getIds();

            $criteria->addFilter(
                new MultiFilter(
                    MultiFilter::CONNECTION_OR,
                    [
                        new EqualsAnyFilter('parentId', $ids),
                        new EqualsAnyFilter('id', $ids)
                    ]
                ));

            $criteria->setIds([]);
        }

        $entities = $this->listingLoader->load($criteria, $context);

        $this->eventDispatcher->dispatch(
            new AfterProductListingLoadedEvent($entities, $context)
        );

        $result = ProductListingResult::createFrom($entities);
        $result->addState(...$entities->getStates());

        $this->eventDispatcher->dispatch(
            new ProductListingResultEvent($request, $result, $context)
        );

        $result->setStreamId($streamId);

        return new ProductListingRouteResponse($result);
    }

    private function extendCriteria(SalesChannelContext $salesChannelContext, Criteria $criteria, PresentationCmsPageEntity $cmsPageRelation): ?string
    {
        if ($cmsPageRelation->getPickedProductIds()) {
            $criteria->setIds($cmsPageRelation->getPickedProductIds());

            return null;
        }

        if ($cmsPageRelation->getProductStreamId()) {
            $filters = $this->productStreamBuilder->buildFilters(
                $cmsPageRelation->getProductStreamId(),
                $salesChannelContext->getContext()
            );

            $criteria->addFilter(...$filters);

            return $cmsPageRelation->getProductStreamId();
        }

        throw new \Exception('No product information given.');
    }
}
