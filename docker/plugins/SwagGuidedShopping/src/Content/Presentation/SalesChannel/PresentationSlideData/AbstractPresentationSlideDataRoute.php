<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideData;

use Shopware\Core\System\SalesChannel\SalesChannelContext;
use SwagShopwarePwa\Pwa\Response\CmsPageRouteResponse;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractPresentationSlideDataRoute
{
    abstract public function getDecorated(): AbstractPresentationSlideDataRoute;

    abstract public function data(Request $request, SalesChannelContext $context): CmsPageRouteResponse;
}
