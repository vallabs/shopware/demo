<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideData;

use Shopware\Core\Content\Category\CategoryDefinition;
use Shopware\Core\Content\Category\CategoryEntity;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionEntity;
use Shopware\Core\Content\Cms\CmsPageDefinition;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Content\Cms\DataResolver\ResolverContext\EntityResolverContext;
use Shopware\Core\Content\Cms\SalesChannel\SalesChannelCmsPageLoaderInterface;
use Shopware\Core\Content\Product\ProductDefinition;
use Shopware\Core\Content\Product\SalesChannel\Detail\ProductConfiguratorLoader;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductCollection;
use Shopware\Core\Content\Product\SalesChannel\SalesChannelProductEntity;
use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\InconsistentCriteriaIdsException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\Framework\Routing\RoutingException;
use Shopware\Core\Framework\Struct\ArrayStruct;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\System\SalesChannel\Entity\SalesChannelRepository;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\Product\ProductPageCriteriaEvent;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageDefinition;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\Event\AfterLoadProductConfiguratorEvent;
use SwagGuidedShopping\Content\Presentation\Exception\SalesChannelContextMissingAttendeeException;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;
use SwagGuidedShopping\Content\Presentation\PresentationEntity;
use SwagGuidedShopping\Content\Product\Cms\ProductListingCmsElementResolver;
use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use SwagShopwarePwa\Pwa\PageResult\Landing\LandingPageResult;
use SwagShopwarePwa\Pwa\PageResult\Navigation\NavigationPageResult;
use SwagShopwarePwa\Pwa\PageResult\Product\ProductPageResult;
use SwagShopwarePwa\Pwa\Response\CmsPageRouteResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class PresentationSlideDataRoute extends AbstractPresentationSlideDataRoute
{
    /**
     * @param EntityRepository<PresentationCollection> $presentationRepository
     * @param SalesChannelRepository<SalesChannelProductCollection> $productRepository
     */
    public function __construct(
        private readonly EntityRepository $presentationRepository,
        private readonly SalesChannelRepository $productRepository,
        private readonly ProductConfiguratorLoader $configuratorLoader,
        private readonly SalesChannelCmsPageLoaderInterface $cmsPageLoader,
        private readonly CategoryDefinition $categoryDefinition,
        private readonly ProductDefinition $productDefinition,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly NotesElementHandler $notesElementHandler
    ) {
    }

    public function getDecorated(): AbstractPresentationSlideDataRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/appointment/presentation/{presentationCmsPageId}/slide/{sectionId}', name: 'store-api.guided-shopping.presentation-data', defaults: ['attendee_required' => true], methods: ['GET'])]
    public function data(Request $request, SalesChannelContext $context): CmsPageRouteResponse
    {
        $presentationCmsPageId = (string) $request->get('presentationCmsPageId');
        if ($presentationCmsPageId === '') {
            throw RoutingException::missingRequestParameter('presentationCmsPageId');
        }

        $sectionId = (string) $request->get('sectionId');
        if ($sectionId === '') {
            throw RoutingException::missingRequestParameter('sectionId');
        }

        /** @var AttendeeEntity|null $attendee */
        $attendee = $context->getExtension(GuidedShoppingRequestContextResolver::CONTEXT_EXTENSION_NAME);
        if (!$attendee) {
            throw new SalesChannelContextMissingAttendeeException();
        }

        $presentationId = $attendee->getAppointment()->getPresentationId();

        /** @var array<string> $ids */
        $ids = [$presentationId];
        $criteria = new Criteria($ids);
        $criteria->getAssociation('cmsPages')->addFilter(new EqualsFilter('id', $presentationCmsPageId));
        $criteria->addAssociation('cmsPages.cmsPage.sections.blocks.slots');
        $criteria->getAssociation('cmsPages')->addSorting(new FieldSorting('position'));
        $criteria->setLimit(1);

        /** @var PresentationEntity|null $presentation */
        $presentation = $this->presentationRepository->search($criteria, $context->getContext())->first();
        if (!$presentation) {
            throw new EntityNotFoundException(PresentationCmsPageDefinition::ENTITY_NAME, $presentationCmsPageId);
        }

        /** @var PresentationCmsPageEntity|null $cmsPageRelation */
        $cmsPageRelation = $presentation->getCmsPages()->first();
        if (!$cmsPageRelation) {
            throw new EntityNotFoundException(PresentationCmsPageDefinition::ENTITY_NAME, $presentationCmsPageId);
        }

        /** @var CmsPageEntity|null $cmsPage */
        $cmsPage = $cmsPageRelation->getCmsPage();
        if (!$cmsPage) {
            throw new EntityNotFoundException(PresentationCmsPageDefinition::ENTITY_NAME, $presentationCmsPageId);
        }

        $slotConfig = $cmsPageRelation->getTranslation('slotConfig');
        $cmsPageId = $cmsPage->getId();
        $criteria = new Criteria([$cmsPageId]);
        $criteria->getAssociation('sections')->addFilter(new EqualsFilter('id', $sectionId));
        $criteria->setLimit(1);

        $context->addExtension(
            ProductListingCmsElementResolver::CONTEXT_EXTENSION,
            new ArrayStruct([
                'presentationCmsPageId' => $presentationCmsPageId,
                'sectionId' => $sectionId,
            ])
        );

        $resolverContext = $this->getResolverContext($cmsPageRelation, $context, $request);

        $pages = $this->cmsPageLoader->load(
            $request,
            $criteria,
            $context,
            $slotConfig ?: [],
            $resolverContext
        );

        /** @var CmsPageEntity|null $page */
        $page = $pages->first();
        if (!$page) {
            throw new EntityNotFoundException(CmsPageDefinition::ENTITY_NAME, $cmsPageId);
        }

        $pageResult = new LandingPageResult();

        if ($resolverContext && $resolverContext->getDefinition() instanceof ProductDefinition) {
            /** @var SalesChannelProductEntity $product */
            $product = $resolverContext->getEntity();
            /** @var PropertyGroupCollection $configurator */
            $configurator = $resolverContext->getEntity()->getExtension('configurator');
            $pageResult = new ProductPageResult();
            $pageResult->setProduct($product);
            $pageResult->setConfigurator($configurator);
        } elseif ($resolverContext && $resolverContext->getDefinition() instanceof CategoryDefinition) {
            $pageResult = new NavigationPageResult();
            /** @var CategoryEntity $category */
            $category = $resolverContext->getEntity();
            $pageResult->setCategory($category);
        }

        $this->removeNotes($page);
        $pageResult->setCmsPage($page);
        $cmsPageRelation->setCmsPage($page);

        // remove duplicates from picked product ids if there are any
        if ($cmsPageRelation->getPickedProductIds()) {
            $cmsPageRelation->setPickedProductIds(\array_unique($cmsPageRelation->getPickedProductIds()));
        }

        $pageResult->addExtension('cmsPageRelation', $cmsPageRelation);

        return new CmsPageRouteResponse($pageResult);
    }

    private function removeNotes(CmsPageEntity $page): void
    {
        $sections = $page->getSections();

        if ($sections instanceof CmsSectionCollection) {
            /** @var CmsSectionEntity $section */
            foreach ($sections as $section) {
                $this->notesElementHandler->removeNotes($section);
            }
        }
    }

    private function getResolverContext(PresentationCmsPageEntity $cmsPageRelation, SalesChannelContext $context, Request $request): ?EntityResolverContext
    {
        /** @var CmsPageEntity $page */
        $page = $cmsPageRelation->getCmsPage();
        if (\mb_strpos($page->getType(), 'product_list') !== false) {
            $category = new CategoryEntity();
            $category->setId(Uuid::randomHex());

            return new EntityResolverContext($context, $request, $this->categoryDefinition, $category);
        }

        if (\mb_strpos($page->getType(), 'product_detail') !== false) {
            if (!$cmsPageRelation->getProductId()) {
                throw new \Exception('No productId mapped to page');
            }

            $productId = $this->findBestVariant($cmsPageRelation->getProductId(), $context);
            $criteria = new Criteria([$productId]);
            $criteria->addAssociation('manufacturer.media')
                ->addAssociation('options.group')
                ->addAssociation('properties.group')
                ->addAssociation('mainCategories.category')
                ->addAssociation('productReviews')
                ->addAssociation('media')
                ->addAssociation('crossSellings');

            $this->eventDispatcher->dispatch(new ProductPageCriteriaEvent($productId, $criteria, $context));

            /** @var SalesChannelProductEntity|null $product */
            $product = $this->productRepository->search($criteria, $context)->first();
            if (!$product) {
                throw new EntityNotFoundException(ProductDefinition::ENTITY_NAME, $productId);
            }

            $groups = $this->configuratorLoader->load($product, $context);

            $this->eventDispatcher->dispatch(new AfterLoadProductConfiguratorEvent($groups, $context));

            $product->addExtension('configurator', $groups);

            return new EntityResolverContext($context, $request, $this->productDefinition, $product);
        }

        return null;
    }

    /**
     * @throws InconsistentCriteriaIdsException
     */
    private function findBestVariant(string $productId, SalesChannelContext $context): string
    {
        /**
         * we have to make sure that on every request for each attendee leads to the same result
         * otherwise, some attendees would see another variant depending on price etc.
         */
        $criteria = (new Criteria())
            ->addFilter(new EqualsFilter('product.parentId', $productId))
            ->addSorting(new FieldSorting('product.id', FieldSorting::ASCENDING))
            ->setLimit(1);

        $variantId = $this->productRepository->searchIds($criteria, $context);

        return $variantId->firstId() ?? $productId;
    }
}
