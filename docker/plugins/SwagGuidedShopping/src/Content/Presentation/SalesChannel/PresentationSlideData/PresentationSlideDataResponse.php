<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\PresentationSlideData;

use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class PresentationSlideDataResponse extends StoreApiResponse
{
    public function __construct(CmsPageEntity $page)
    {
        parent::__construct($page);
    }
}
