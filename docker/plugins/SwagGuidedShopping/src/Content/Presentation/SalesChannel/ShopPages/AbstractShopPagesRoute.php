<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\ShopPages;

use Shopware\Core\System\SalesChannel\SalesChannelContext;

abstract class AbstractShopPagesRoute
{
    abstract public function getDecorated(): AbstractShopPagesRoute;

    abstract public function getShopPage(string $pageName, SalesChannelContext $context): ShopPagesResponse;
}
