<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\ShopPages;

use Shopware\Core\Framework\Struct\Struct;
use Shopware\Core\System\SalesChannel\StoreApiResponse;

class ShopPagesResponse extends StoreApiResponse
{
    public function __construct(Struct $data)
    {
        parent::__construct($data);
    }
}
