<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\SalesChannel\ShopPages;

use Shopware\Core\Content\Cms\Aggregate\CmsSection\CmsSectionCollection;
use Shopware\Core\Content\Cms\CmsPageCollection;
use Shopware\Core\Content\Cms\CmsPageEntity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Plugin\Exception\DecorationPatternException;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Core\System\SystemConfig\Exception\InvalidKeyException;
use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\Cms\Service\NotesElementHandler;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['store-api']])]
class ShopPagesRoute extends AbstractShopPagesRoute
{
    /**
     * @param EntityRepository<CmsPageCollection> $cmsPageRepository
     */
    public function __construct(
        private readonly SystemConfigService $systemConfigService,
        private readonly EntityRepository $cmsPageRepository,
        private readonly NotesElementHandler $notesElementHandler
    ) {
    }

    public function getDecorated(): AbstractShopPagesRoute
    {
        throw new DecorationPatternException(self::class);
    }

    #[Route(path: '/store-api/guided-shopping/shop-pages/{pageName}', name: 'store-api.system-config.shop-pages', methods: ['GET'])]
    public function getShopPage(string $pageName, SalesChannelContext $context): ShopPagesResponse
    {
        $key = "core.basicInformation.{$pageName}";

        /** @var string|null $pageId */
        $pageId = $this->systemConfigService->get($key);

        if ($pageId === null) {
            throw new InvalidKeyException($key);
        }

        $criteria = new Criteria([$pageId]);
        $criteria->addAssociation('sections.blocks.slots');
        $criteria->addAssociation('sections.backgroundMedia');
        $criteria->addAssociation('sections.blocks.backgroundMedia');

        $page = $this->cmsPageRepository->search($criteria, $context->getContext())->first();

        if (!$page instanceof CmsPageEntity || !$page->getSections() instanceof CmsSectionCollection) {
            throw new EntityNotFoundException(CmsPageEntity::class, $pageId);
        }

        foreach ($page->getSections() as $section) {
            $this->notesElementHandler->removeNotes($section);
        }

        return new ShopPagesResponse($page);
    }
}
