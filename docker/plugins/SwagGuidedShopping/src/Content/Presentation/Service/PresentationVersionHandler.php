<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Service;

use Shopware\Core\Defaults;
use Shopware\Core\Framework\Api\Context\SystemSource;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Sorting\FieldSorting;
use Shopware\Core\Framework\DataAbstractionLayer\Write\CloneBehavior;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\Cms\Service\CmsPageCreatorService;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageCollection;
use SwagGuidedShopping\Content\Presentation\Aggregate\PresentationCmsPage\PresentationCmsPageEntity;
use SwagGuidedShopping\Content\Presentation\PresentationCollection;

class PresentationVersionHandler
{
    /**
     * @param EntityRepository<PresentationCollection> $presentationRepository
     * @param EntityRepository<PresentationCmsPageCollection> $presentationCmsPageRepository
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly EntityRepository $presentationRepository,
        private readonly EntityRepository $presentationCmsPageRepository,
        private readonly EntityRepository $appointmentRepository,
        private readonly CmsPageCreatorService $cmsPageCreatorService
    ) {
    }

    public function createPresentationVersion(AppointmentEntity $appointment, Context $context): string
    {
        $presentationId = $appointment->getPresentationId();

        if ($appointment->isDefault() && $appointment->getMode() === AppointmentDefinition::MODE_SELF) {
            return Defaults::LIVE_VERSION;
        }

        if ($appointment->getPresentationVersionId() !== Defaults::LIVE_VERSION && $appointment->getPresentationVersionId()) {
            return $appointment->getPresentationVersionId();
        } else {
            $presentationVersionId = $this->presentationRepository->createVersion($presentationId, $context, 'version for appointment ' . $appointment->getId());
            $this->appointmentRepository->update([
                [
                    'id' => $appointment->getId(),
                    'guidedShoppingPresentationVersionId' => $presentationVersionId,
                ],
            ], $context);
        }

        return $presentationVersionId;
    }

    public function getContextForAppointment(string $presentationVersionId, Context $context): Context
    {
        return new Context(
            new SystemSource(),
            $context->getRuleIds(),
            $context->getCurrencyId(),
            $context->getLanguageIdChain(),
            $presentationVersionId,
            $context->getCurrencyFactor(),
            $context->considerInheritance(),
            $context->getTaxState(),
            $context->getRounding()
        );
    }

    public function updatePresentationCmsPages(AppointmentEntity $appointment, string $presentationVersionId, Context $context): void
    {
        $presentationId = $appointment->getPresentationId();

        if (!$appointment->getStartedAt() && !$appointment->getEndedAt()) {
            if ($appointment->getPresentation()) { // called from AppointmentService
                $existingCmsPages = $appointment->getPresentation()->getCmsPages()->filterByPresentationVersionId($presentationVersionId);
            } else {  // called from PresentationStructureRoute
                $existingCmsPages = $this->getExistingPresentationCmsPages($presentationId, $presentationVersionId, $context);
            }

            /**
             * Get list of current cms pages are NOT instant listing
             * And delete all of them to update the new original cms pages
             */
            $existingCmsPagesIsNotInstantListing = $existingCmsPages->filterByPresentationVersionId($presentationVersionId, false);
            $removeIds = \array_map(function (string $id) {
                return ['id' => $id];
            }, \array_values($existingCmsPagesIsNotInstantListing->getIds()));
            $this->presentationCmsPageRepository->delete($removeIds, $context);

            // Get the list of positions are occupied by current cms pages are instant listing
            $existingCmsPagesIsInstantListing = $existingCmsPages->filterByPresentationVersionId($presentationVersionId, true);
            $existingCmsPagesIsInstantListing->sortByPosition();
            $occupiedPositions = $existingCmsPagesIsInstantListing->getPosition();


            // Get original cms pages
            $originalCmsPages = $this->getOriginalPresentationCmsPages($presentationId, $context);
            $originalCmsPagesCount = \count($originalCmsPages);
            if (!$originalCmsPagesCount) {
                return;
            }

            // clone the original cms pages with unoccupied positions
            $totalPages = $originalCmsPagesCount + \count($occupiedPositions);
            for ($i = 1; $i <= $totalPages; $i++) {
                if (!\in_array($i, $occupiedPositions)) {
                    /** @var PresentationCmsPageEntity $cloneCmsPage */
                    $cloneCmsPage = \array_shift($originalCmsPages);
                    $this->clonePresentationCmsPage($cloneCmsPage, $presentationVersionId, $i, $context);

                    if (!\count($originalCmsPages)) {
                        break;
                    }
                }
            }

            // re-arrange the positions of presentation cms pages
            $this->cmsPageCreatorService->updateSlidePositions(1, $presentationVersionId, $presentationId, $context);
        }
    }

    private function getExistingPresentationCmsPages(string $presentationId, string $versionId, Context $context): PresentationCmsPageCollection
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationId', $presentationId))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', $versionId))
            ->addSorting(new FieldSorting('position'));

        /** @var PresentationCmsPageCollection $presentationCmsPages */
        $presentationCmsPages = $this->presentationCmsPageRepository->search($criteria, $context)->getEntities();

        return $presentationCmsPages;
    }


    /**
     * @return array<Entity>
     */
    private function getOriginalPresentationCmsPages(string $presentationId, Context $context): array
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('presentationId', $presentationId))
            ->addFilter(new EqualsFilter('guidedShoppingPresentationVersionId', Defaults::LIVE_VERSION))
            ->addFilter(new EqualsFilter('isInstantListing', false))
            ->addSorting(new FieldSorting('position'));

        return $this->presentationCmsPageRepository->search($criteria, $context)->getElements();
    }


    private function clonePresentationCmsPage(PresentationCmsPageEntity $cmsPage, string $newVersionId, ?int $position, Context $context): void
    {
        $modifyData = [
            'guidedShoppingPresentationVersionId' => $newVersionId,
            'cmsPageVersionId' => $cmsPage->getVersionId(),
            'createdAt' => $cmsPage->getCreatedAt(),
        ];

        if ($position) {
            $modifyData['position'] = $position;
        }

        $cloneBehavior = new CloneBehavior($modifyData);

        $this->presentationCmsPageRepository->clone($cmsPage->getId(), $context, null, $cloneBehavior);
    }
}
