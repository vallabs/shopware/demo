<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation\Event;

use Shopware\Core\Content\Property\PropertyGroupCollection;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\Event\ShopwareSalesChannelEvent;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class AfterLoadProductConfiguratorEvent implements ShopwareSalesChannelEvent
{
    public function __construct(
        protected ?PropertyGroupCollection $groups,
        protected SalesChannelContext $salesChannelContext
    ) {
    }

    public function getGroups(): ?PropertyGroupCollection
    {
        return $this->groups;
    }

    public function getSalesChannelContext(): SalesChannelContext
    {
        return $this->salesChannelContext;
    }

    public function getContext(): Context
    {
        return $this->salesChannelContext->getContext();
    }
}
