<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\Presentation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<PresentationEntity>
 */
class PresentationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return PresentationEntity::class;
    }
}
