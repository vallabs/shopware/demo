<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Service;

use GuzzleHttp\Psr7\Response;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoChatCreateStruct;
use SwagGuidedShopping\Content\VideoChat\Api\Create\VideoRoomCreateException;
use SwagGuidedShopping\Content\VideoChat\Service\Client\DailyClient;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;

class VideoRoomManager
{
    /**
     * @param EntityRepository<VideoChatCollection> $videoChatRepository
     */
    public function __construct(
        private readonly DailyClient $client,
        private readonly EntityRepository $videoChatRepository
    ) {
    }

    /**
     * creates a secret room in daily.co
     */
    public function createRoom(bool $startAsBroadcast): VideoChatCreateStruct
    {
        $roomOptions = [
            'privacy' => 'private',
        ];

        try {
            /** @var Response $response */
            $response = $this->client->post('rooms', ['body' => \json_encode($roomOptions)]);
        } catch (\Exception $e) {
            throw new VideoRoomCreateException($e->getMessage());
        }

        $responseBody = $this->client->getResponseBody($response);

        $userToken = $this->createToken($responseBody['name'], false);
        $ownerToken = $this->createToken($responseBody['name'], true);

        return new VideoChatCreateStruct($responseBody['url'], $responseBody['name'], $userToken, $ownerToken, $startAsBroadcast);
    }

    public function saveVideoChatData(string $videoChatId, VideoChatCreateStruct $roomData, Context $context): void
    {
        $this->videoChatRepository->upsert([
            [
                'id' => $videoChatId,
                'name' => $roomData->getRoomName(),
                'url' => $roomData->getRoomUrl(),
                'userToken' => $roomData->getUserToken(),
                'ownerToken' => $roomData->getOwnerToken(),
            ]
        ], $context);
    }

    public function deleteRoom(string $roomName, Context $context): void
    {
        try {
            $this->client->delete('rooms/' . $roomName);
        } catch (\Exception $e) {
            if ($e->getCode() !== \Symfony\Component\HttpFoundation\Response::HTTP_NOT_FOUND) {
                throw $e;
            }
        }

        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('name', $roomName));
        $roomId = $this->videoChatRepository->searchIds($criteria, $context)->firstId();
        if ($roomId) {
            $this->removeRoomFromAppointment($roomId, $context);
        }
    }

    public function removeRoomFromAppointment(string $roomId, Context $context): void
    {
        $this->videoChatRepository->delete([['id' => $roomId]], $context);
    }

    public function removeRoomDataFromAppointment(string $videoChatId, Context $context): void
    {
        $this->videoChatRepository->upsert([
            [
                'id' => $videoChatId,
                'name' => null,
                'url' => null,
                'userToken' => null,
                'ownerToken' => null,
            ]
        ], $context);
    }

    /**
     * Create a token on daily.co
     * without a token you can't join a room which is marked as private
     */
    private function createToken(
        string $roomName,
        bool $ownerToken
    ): string {
        $tokenOptions = [
            'properties' => [
                'room_name' => $roomName,
                'is_owner' => $ownerToken,
            ],
        ];

        try {
            /** @var Response $response */
            $response = $this->client->post('meeting-tokens', ['body' => \json_encode($tokenOptions)]);
        } catch (\Exception $e) {
            throw new VideoRoomCreateException($e->getMessage());
        }

        $responseBody = $this->client->getResponseBody($response);

        return $responseBody['token'];
    }
}
