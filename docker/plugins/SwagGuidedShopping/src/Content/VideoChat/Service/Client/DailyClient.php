<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Service\Client;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

class DailyClient extends Client
{
    /**
     * @param array<string, mixed> $additionalConfig
     */
    public function __construct(string $baseUrl, string $apiKey, array $additionalConfig = [])
    {
        $config = [
            'base_uri' => $baseUrl,
            'headers' => [
                'Authorization' => 'Bearer ' . $apiKey,
                'Content-Type' => 'application/json',
            ],
            'timeout' => 10.0,
        ];

        $config = \array_merge_recursive($config, $additionalConfig);

        parent::__construct($config);
    }

    /**
     * @param Response $response
     * @return array<string, mixed>
     */
    public function getResponseBody(Response $response): array
    {
        $body = $response->getBody();
        $body = (string) $body;

        return \json_decode($body, true);
    }
}
