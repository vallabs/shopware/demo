<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Api\Create;

use Shopware\Core\Framework\Context;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class VideoChatRoomCreateController extends AbstractController
{
    public function __construct(
        private readonly VideoChatRoomCreateFacade $videoChatRoomCreateFacade
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/video-room', name: 'api.action.guided-shopping.create-video-room', methods: ['POST'])]
    public function createVideoChatRoom(string $appointmentId, Context $context): JsonResponse
    {
        $roomCreateStruct = $this->videoChatRoomCreateFacade->handleRoomCreation($appointmentId, $context);

        return new JsonResponse($roomCreateStruct->getVars());
    }
}
