<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Api\Create;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;

class VideoChatRoomCreateFacade
{
    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly VideoRoomManager $videoRoomManager,
        private readonly EntityRepository $appointmentRepository
    ) {
    }

    public function handleRoomCreation(string $appointmentId, Context $context): VideoChatCreateStruct
    {
        /** @var AppointmentEntity|null $appointment */
        $appointment = $this->appointmentRepository->search(new Criteria([$appointmentId]), $context)->first();

        if (!$appointment instanceof AppointmentEntity) {
            throw new EntityNotFoundException(AppointmentDefinition::ENTITY_NAME, $appointmentId);
        }

        $videoChat = $appointment->getVideoChat();

        if (!$videoChat) {
            throw new VideoChatEntityDoesNotExist($appointmentId);
        }

        if ($videoChat->getName()) {
            return $this->createVideoChatStruct($videoChat);
        }

        try {
            $roomData = $this->videoRoomManager->createRoom($videoChat->isStartAsBroadcast());
        } catch (\Exception $e) {
            throw new VideoRoomCreateException($e->getMessage());
        }

        $this->videoRoomManager->saveVideoChatData($videoChat->getId(), $roomData, $context);

        return $roomData;
    }

    private function createVideoChatStruct(VideoChatEntity $videoChat): VideoChatCreateStruct
    {
        if (!$videoChat->getUrl()
            || !$videoChat->getName()
            || !$videoChat->getOwnerToken()
            || !$videoChat->getUserToken()
        ) {
            throw new \Exception(\sprintf('Video chat (%s) is not complete', $videoChat->getId()));
        }

        return new VideoChatCreateStruct(
            $videoChat->getUrl(),
            $videoChat->getName(),
            $videoChat->getUserToken(),
            $videoChat->getOwnerToken(),
            $videoChat->isStartAsBroadcast()
        );
    }
}
