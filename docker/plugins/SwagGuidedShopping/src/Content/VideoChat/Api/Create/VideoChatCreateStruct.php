<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Api\Create;

use Shopware\Core\Framework\Struct\Struct;

class VideoChatCreateStruct extends Struct
{
    public function __construct(
        protected string $roomUrl,
        protected string $roomName,
        protected string $userToken,
        protected string $ownerToken,
        protected bool $startAsBroadcast
    ) {
    }

    public function getRoomUrl(): string
    {
        return $this->roomUrl;
    }

    public function getUserToken(): string
    {
        return $this->userToken;
    }

    public function getOwnerToken(): string
    {
        return $this->ownerToken;
    }

    public function getRoomName(): string
    {
        return $this->roomName;
    }

    public function startAsBroadcast(): bool
    {
        return $this->startAsBroadcast;
    }
}
