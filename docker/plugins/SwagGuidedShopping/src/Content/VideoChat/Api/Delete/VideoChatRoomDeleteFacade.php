<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Api\Delete;

use GuzzleHttp\Exception\RequestException;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Exception\EntityNotFoundException;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use SwagGuidedShopping\Content\Appointment\AppointmentCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntityDoesNotExist;

class VideoChatRoomDeleteFacade
{
    /**
     * @param EntityRepository<AppointmentCollection> $appointmentRepository
     */
    public function __construct(
        private readonly VideoRoomManager $videoRoomManager,
        private readonly EntityRepository $appointmentRepository
    ) {
    }

    public function handleRoomDeletion(string $appointmentId, Context $context): bool
    {
        /** @var AppointmentEntity|null $appointment */
        $appointment = $this->appointmentRepository->search(new Criteria([$appointmentId]), $context)->first();

        if (!$appointment instanceof AppointmentEntity) {
            throw new EntityNotFoundException(AppointmentDefinition::ENTITY_NAME, $appointmentId);
        }

        $videoChat = $appointment->getVideoChat();
        if (!$videoChat) {
            throw new VideoChatEntityDoesNotExist($appointmentId);
        }

        $roomName = $videoChat->getName();
        if ($roomName) {
            try {
                $this->videoRoomManager->deleteRoom($roomName, $context);
            } catch (RequestException $e) {
                // 404 would be ok as this means the room is already closed
                if ($e->getResponse() && $e->getResponse()->getStatusCode() !== 404) {
                    throw new VideoRoomDeleteException($e->getMessage());
                }
            }
        }

        $this->videoRoomManager->removeRoomDataFromAppointment($videoChat->getId(), $context);

        return true;
    }
}
