<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Api\Delete;

use Shopware\Core\Framework\Context;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(defaults: ['_routeScope' => ['api']])]
class VideoChatRoomDeleteController extends AbstractController
{
    public function __construct(
        private readonly VideoChatRoomDeleteFacade $videoChatRoomDeleteFacade
    ) {
    }

    #[Route(path: '/api/_action/guided-shopping/appointment/{appointmentId}/video-room', name: 'api.action.guided-shopping.delete-video-room', methods: ['DELETE'])]
    public function deleteVideoChatRoom(string $appointmentId, Context $context): JsonResponse
    {
        $this->videoChatRoomDeleteFacade->handleRoomDeletion($appointmentId, $context);

        return new JsonResponse();
    }
}
