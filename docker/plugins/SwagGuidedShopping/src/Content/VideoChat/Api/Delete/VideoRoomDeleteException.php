<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\Api\Delete;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class VideoRoomDeleteException extends ShopwareHttpException
{
    public function getStatusCode(): int
    {
        return Response::HTTP_BAD_REQUEST;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__ROOM_DELETE_EXCEPTION;
    }
}
