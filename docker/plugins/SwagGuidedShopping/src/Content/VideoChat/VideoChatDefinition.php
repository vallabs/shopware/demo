<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat;

use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\CustomFields;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\ApiAware;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class VideoChatDefinition extends EntityDefinition
{
    public const ENTITY_NAME = 'guided_shopping_appointment_video_chat';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getEntityClass(): string
    {
        return VideoChatEntity::class;
    }

    public function getCollectionClass(): string
    {
        return VideoChatCollection::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new FkField('appointment_id', 'appointmentId', AppointmentDefinition::class))->addFlags(new Required()),
                (new StringField('name', 'name'))->addFlags(new ApiAware()),
                (new StringField('owner_token', 'ownerToken')),
                (new StringField('user_token', 'userToken')),
                (new StringField('url', 'url'))->addFlags(new ApiAware()),
                (new BoolField('start_as_broadcast', 'startAsBroadcast'))->addFlags(new ApiAware()),
                (new CustomFields())->addFlags(new ApiAware()),

                new OneToOneAssociationField('appointment', 'appointment_id', 'id', AppointmentDefinition::class, false),
            ]
        );
    }
}
