<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @extends EntityCollection<VideoChatEntity>
 */
class VideoChatCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return VideoChatEntity::class;
    }
}
