<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat\EventListener;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\EntityWriteResult;
use Shopware\Core\Framework\DataAbstractionLayer\Event\EntityDeletedEvent;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use SwagGuidedShopping\Content\VideoChat\Service\VideoRoomManager;
use SwagGuidedShopping\Content\VideoChat\VideoChatCollection;
use SwagGuidedShopping\Content\VideoChat\VideoChatEntity;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class VideoChatDeletedListener implements EventSubscriberInterface
{
    /**
     * @param EntityRepository<VideoChatCollection> $videoChatRepository
     */
    public function __construct(
        private readonly VideoRoomManager $videoRoomManager,
        private readonly EntityRepository $videoChatRepository
    ) {
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            AppointmentDefinition::ENTITY_NAME . '.deleted' => ['onEntityDeleted'],
        ];
    }

    public function onEntityDeleted(EntityDeletedEvent $entityDeletedEvent): void
    {
        $writeResults = $entityDeletedEvent->getWriteResults();

        foreach ($writeResults as $writeResult) {
            if ($this->isAppointmentDeletedEvent($writeResult)) {
                $appointmentId = $writeResult->getPrimaryKey();
                if (!\is_string($appointmentId)) {
                    continue;
                }

                $videoChat = $this->getVideoChatForAppointment($appointmentId, $entityDeletedEvent->getContext());

                if ($videoChat instanceof VideoChatEntity && $videoChat->getName()) {
                    $this->videoRoomManager->deleteRoom($videoChat->getName(), $entityDeletedEvent->getContext());
                }
            }
        }
    }

    private function isAppointmentDeletedEvent(EntityWriteResult $writeResult): bool
    {
        return $writeResult->getEntityName() === AppointmentDefinition::ENTITY_NAME
            && $writeResult->getOperation() === EntityWriteResult::OPERATION_DELETE;
    }

    private function getVideoChatForAppointment(string $deletedAppointmentId, Context $context): ?VideoChatEntity
    {
        $criteria = new Criteria();
        $criteria->addFilter(new EqualsFilter('appointmentId', $deletedAppointmentId));

        /** @var VideoChatEntity|null $videoChat */
        $videoChat = $this->videoChatRepository->search($criteria, $context)->first();

        return $videoChat;
    }
}
