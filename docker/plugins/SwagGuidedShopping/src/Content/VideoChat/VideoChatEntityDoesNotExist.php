<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class VideoChatEntityDoesNotExist extends ShopwareHttpException
{
    public function __construct(string $appointmentId)
    {
        parent::__construct(
            \sprintf('There is no video entity found for the appointment (%s), this has to be created before creating or modifying a room. Maybe the appointment has no video chat enabled.', $appointmentId)
        );
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__VIDEO_CHAT_ENTITY_NOT_FOUND;
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }
}
