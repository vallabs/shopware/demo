<?php declare(strict_types=1);

namespace SwagGuidedShopping\Content\VideoChat;

use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityCustomFieldsTrait;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use SwagGuidedShopping\Content\Appointment\AppointmentEntity;

class VideoChatEntity extends Entity
{
    use EntityIdTrait;
    use EntityCustomFieldsTrait;

    protected string $appointmentId;

    protected ?AppointmentEntity $appointment = null;

    protected ?string $name;

    protected ?string $ownerToken;

    protected ?string $userToken;

    protected ?string $url;

    protected bool $startAsBroadcast;

    public function getAppointmentId(): string
    {
        return $this->appointmentId;
    }

    public function setAppointmentId(string $appointmentId): void
    {
        $this->appointmentId = $appointmentId;
    }

    public function getAppointment(): ?AppointmentEntity
    {
        return $this->appointment;
    }

    public function setAppointment(AppointmentEntity $appointment): void
    {
        $this->appointment = $appointment;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getOwnerToken(): ?string
    {
        return $this->ownerToken;
    }

    public function setOwnerToken(?string $ownerToken): void
    {
        $this->ownerToken = $ownerToken;
    }

    public function getUserToken(): ?string
    {
        return $this->userToken;
    }

    public function setUserToken(?string $userToken): void
    {
        $this->userToken = $userToken;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function isStartAsBroadcast(): bool
    {
        return $this->startAsBroadcast;
    }

    public function setStartAsBroadcast(bool $startAsBroadcast): void
    {
        $this->startAsBroadcast = $startAsBroadcast;
    }
}
