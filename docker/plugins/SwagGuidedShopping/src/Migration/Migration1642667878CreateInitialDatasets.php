<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1642667878CreateInitialDatasets extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1642667878;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement($this->getPDpAndListingSql());
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function getPDpAndListingSql(): string
    {
        return <<<'SQL'
                INSERT IGNORE INTO `cms_page` (`id`, `version_id`, `type`, `entity`, `preview_media_id`, `locked`, `config`, `created_at`)
                VALUES
                    (X'33E88C7994FA4CF79A1265E5105B93B2', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'presentation_product_list', NULL, NULL, 0, NULL, now()),
                    (X'BEA211B5099241719830DF8026624F7F', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'presentation_product_detail', NULL, NULL, 0, NULL, now()),
                	(X'8EA80092FAA744559409F3E9F7ADCC6B', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'presentation_product_list', NULL, NULL, 0, NULL, now()),
                	(X'182D3F7F988044ADBBA449B70C8BC472', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'product_detail', NULL, NULL, 1, NULL, now());
                INSERT IGNORE INTO `cms_page_translation` (`cms_page_id`, `cms_page_version_id`, `language_id`, `name`, `custom_fields`, `created_at`)
                VALUES
                    (X'33E88C7994FA4CF79A1265E5105B93B2', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', 'Default Guided Shopping product listing page', NULL, now()),
                    (X'BEA211B5099241719830DF8026624F7F', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', 'Default Guided Shopping product detail page', NULL, now()),
                    (X'8EA80092FAA744559409F3E9F7ADCC6B', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', 'Ended presentation page', NULL, now()),
                    (X'182D3F7F988044ADBBA449B70C8BC472', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', 'Guided Shopping Quickview', NULL, now());
                INSERT IGNORE INTO `cms_section` (`id`, `version_id`, `cms_page_id`, `cms_page_version_id`, `position`, `type`, `name`, `locked`, `sizing_mode`, `mobile_behavior`, `background_color`, `background_media_id`, `background_media_mode`, `css_class`, `custom_fields`, `created_at`)
                VALUES
                    (X'2229E1F1208A4B8086BAF7AEC84F5E2C', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'33E88C7994FA4CF79A1265E5105B93B2', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'default', NULL, 0, 'boxed', 'wrap', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'F4AAA27972314E3C9B7F3BB35C7F8A10', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'BEA211B5099241719830DF8026624F7F', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'default', NULL, 0, 'boxed', 'wrap', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'0A5E89814463470684A734B975ADD4ED', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'8EA80092FAA744559409F3E9F7ADCC6B', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'default', NULL, 0, 'boxed', 'wrap', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'14D1FA87BBAA489CA3034C62FB405F46', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'182D3F7F988044ADBBA449B70C8BC472', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'default', NULL, 0, 'boxed', 'wrap', NULL, NULL, 'cover', NULL, NULL, now());
                INSERT IGNORE INTO `cms_block` (`id`, `version_id`, `cms_section_id`, `cms_section_version_id`, `position`, `section_position`, `type`, `name`, `locked`, `margin_top`, `margin_bottom`, `margin_left`, `margin_right`, `background_color`, `background_media_id`, `background_media_mode`, `css_class`, `custom_fields`, `created_at`)
                VALUES
                    (X'96EA8B9676A5461C9149D205D792ECF2', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2229E1F1208A4B8086BAF7AEC84F5E2C', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'main', 'product-listing', NULL, 0, '20px', '20px', '20px', '20px', NULL, NULL, 'cover', NULL, NULL,now()),
                    (X'095CD9A4EB49493AA95EA1E7A84A9503', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'F4AAA27972314E3C9B7F3BB35C7F8A10', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 2, 'main', 'text-two-column', NULL, 0, '20px', '20px', '20px', '20px', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'47CC4A3919794162982EA83F64A836FE', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'F4AAA27972314E3C9B7F3BB35C7F8A10', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 1, 'main', 'image-gallery-big', NULL, 0, '20px', '20px', '20px', '20px', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'2D69EDD4FAAB493BAA056C81EA8D131F', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'F4AAA27972314E3C9B7F3BB35C7F8A10', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 3, 'main', 'cross-selling', NULL, 0, '0', '0', '0', '0', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'C0D3DABA2E244122947438C28F776D41', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'F4AAA27972314E3C9B7F3BB35C7F8A10', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'main', 'product-heading', NULL, 0, '0', '20px', '0', '0', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'0A13F5DAA9BE4E7B8ED5E3ECABA79FE6', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'0A5E89814463470684A734B975ADD4ED', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 1, 'main', 'product-listing', NULL, 0, '20px', '20px', '20px', '20px', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'C49B01E9C6624973B9CDD1992D3C009A', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'0A5E89814463470684A734B975ADD4ED', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'main', 'text-hero', NULL, 0, '20px', '20px', '20px', '20px', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'9D2B5DCCB60F4682A2278BEE0B1EDB0D', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'14D1FA87BBAA489CA3034C62FB405F46', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 1, 'main', 'gallery-buybox', NULL, 0, '20px', '0', '0', '0', NULL, NULL, 'cover', NULL, NULL, now()),
                    (X'A4659A44F27B44DCAE16CC8CAACA0534', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'14D1FA87BBAA489CA3034C62FB405F46', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 0, 'main', 'product-heading', NULL, 0, '0', '20px', '0', '0', NULL, NULL, 'cover', NULL, NULL, now());
                INSERT IGNORE INTO `cms_slot` (`id`, `version_id`, `cms_block_id`, `cms_block_version_id`, `type`, `slot`, `locked`, `created_at`)
                VALUES
                    (X'80CF45856D9040E9A27666F4F86EF269', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'96EA8B9676A5461C9149D205D792ECF2', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'product-listing', 'content', 0, now()),
                    (X'8EF2167945544E5DBF38426AF128C9D9', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'095CD9A4EB49493AA95EA1E7A84A9503', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'buy-box', 'right', 0, now()),
                    (X'4853A97BBE30412BAC92E7E8526627C8', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'47CC4A3919794162982EA83F64A836FE', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'image-gallery', 'imageGallery', 0, now()),
                    (X'9AA4697356904C039098FBBAE29FC429', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'095CD9A4EB49493AA95EA1E7A84A9503', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'product-description-reviews', 'left', 0, now()),
                    (X'7946BD3200064451BBC2102ACEE52773', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2D69EDD4FAAB493BAA056C81EA8D131F', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'cross-selling', 'content', 0, now()),
                    (X'22EAFAB4CA954F0687E210FB766D8EDE', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'C0D3DABA2E244122947438C28F776D41', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'like', 'right', 0, now()),
                    (X'7A7F86DA1CAC49F5855497AEC03F99AA', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'C0D3DABA2E244122947438C28F776D41', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'product-name', 'left', 0, now()),
                    (X'870C93719C9445C3A6708EC5AE7215E4', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'0A13F5DAA9BE4E7B8ED5E3ECABA79FE6', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'product-listing', 'content', 0, now()),
                    (X'31C877709A294DBEB89366EFE9F32EFE', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'C49B01E9C6624973B9CDD1992D3C009A', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'text', 'content', 0, now()),
                    (X'2401E9E007BE4BE4B2386658A03A82FB', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'9D2B5DCCB60F4682A2278BEE0B1EDB0D', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'image-gallery', 'left', 0, now()),
                    (X'D5518ED1A3C24C9A88F94220ED8F28D7', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'9D2B5DCCB60F4682A2278BEE0B1EDB0D', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'buy-box', 'right', 0, now()),
                    (X'5B39433F79BB4E2D93531242CFF2078A', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'A4659A44F27B44DCAE16CC8CAACA0534', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'manufacturer-logo', 'right', 0, now()),
                    (X'CB3C1E3D528A414EAF0C805CF00AB605', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'A4659A44F27B44DCAE16CC8CAACA0534', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', 'product-name', 'left', 0, now());
                INSERT IGNORE INTO `cms_slot_translation` (`cms_slot_id`, `cms_slot_version_id`, `language_id`, `config`, `custom_fields`, `created_at`)
                VALUES
                    (X'80CF45856D9040E9A27666F4F86EF269', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"filters\": {\"value\": \"manufacturer-filter,rating-filter,price-filter,shipping-free-filter,property-filter\", \"source\": \"static\"}, \"boxLayout\": {\"value\": \"standard\", \"source\": \"static\"}, \"showSorting\": {\"value\": true, \"source\": \"static\"}, \"defaultSorting\": {\"value\": \"\", \"source\": \"static\"}, \"useCustomSorting\": {\"value\": false, \"source\": \"static\"}, \"availableSortings\": {\"value\": [], \"source\": \"static\"}, \"propertyWhitelist\": {\"value\": [], \"source\": \"static\"}}', NULL, now()),
                    (X'8EF2167945544E5DBF38426AF128C9D9', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"product\": {\"value\": null, \"source\": \"static\"}, \"alignment\": {\"value\": null, \"source\": \"static\"}}', NULL, now()),
                    (X'9AA4697356904C039098FBBAE29FC429', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"product\": {\"value\": null, \"source\": \"static\"}, \"alignment\": {\"value\": null, \"source\": \"static\"}}', NULL, now()),
                    (X'4853A97BBE30412BAC92E7E8526627C8', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"zoom\": {\"value\": true, \"source\": \"static\"}, \"minHeight\": {\"value\": \"430px\", \"source\": \"static\"}, \"fullScreen\": {\"value\": true, \"source\": \"static\"}, \"displayMode\": {\"value\": \"contain\", \"source\": \"static\"}, \"sliderItems\": {\"value\": \"product.media\", \"source\": \"mapped\"}, \"bigImageMode\": {\"value\": true, \"source\": \"static\"}, \"verticalAlign\": {\"value\": null, \"source\": \"static\"}, \"navigationDots\": {\"value\": \"inside\", \"source\": \"static\"}, \"galleryPosition\": {\"value\": \"left\", \"source\": \"static\"}, \"navigationArrows\": {\"value\": \"inside\", \"source\": \"static\"}}', NULL, now()),
                    (X'7946BD3200064451BBC2102ACEE52773', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"product\": {\"value\": null, \"source\": \"static\"}, \"boxLayout\": {\"value\": \"standard\", \"source\": \"static\"}, \"elMinWidth\": {\"value\": \"300px\", \"source\": \"static\"}, \"displayMode\": {\"value\": \"standard\", \"source\": \"static\"}}', NULL, now()),
                    (X'22EAFAB4CA954F0687E210FB766D8EDE', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '[]', NULL, now()),
                    (X'7A7F86DA1CAC49F5855497AEC03F99AA', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"content\": {\"value\": \"product.name\", \"source\": \"mapped\"}, \"verticalAlign\": {\"value\": null, \"source\": \"static\"}}', NULL, now()),
                    (X'870C93719C9445C3A6708EC5AE7215E4', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"filters\": {\"value\": \"manufacturer-filter,rating-filter,price-filter,shipping-free-filter,property-filter\", \"source\": \"static\"}, \"boxLayout\": {\"value\": \"standard\", \"source\": \"static\"}, \"showSorting\": {\"value\": true, \"source\": \"static\"}, \"defaultSorting\": {\"value\": \"\", \"source\": \"static\"}, \"useCustomSorting\": {\"value\": false, \"source\": \"static\"}, \"availableSortings\": {\"value\": [], \"source\": \"static\"}, \"propertyWhitelist\": {\"value\": [], \"source\": \"static\"}}', NULL, now()),
                    (X'31C877709A294DBEB89366EFE9F32EFE', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"content\": {\"value\": \"<h2 style=\\\"text-align: center;\\\">The live presentation ended</h2>\\n                        <hr>\\n                        <p style=\\\"text-align: center;\\\">Thank you for your attention!</p><p style=\\\"text-align: center;\\\">Below you can find all products wich were presented to you.</p>\", \"source\": \"static\"}, \"verticalAlign\": {\"value\": null, \"source\": \"static\"}}', NULL, now()),
                    (X'2401E9E007BE4BE4B2386658A03A82FB', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"zoom\": {\"value\": false, \"source\": \"static\"}, \"minHeight\": {\"value\": \"340px\", \"source\": \"static\"}, \"fullScreen\": {\"value\": false, \"source\": \"static\"}, \"displayMode\": {\"value\": \"standard\", \"source\": \"static\"}, \"sliderItems\": {\"value\": \"product.media\", \"source\": \"mapped\"}, \"verticalAlign\": {\"value\": null, \"source\": \"static\"}, \"navigationDots\": {\"value\": null, \"source\": \"static\"}, \"galleryPosition\": {\"value\": \"left\", \"source\": \"static\"}, \"navigationArrows\": {\"value\": \"inside\", \"source\": \"static\"}}', NULL, now()),
                    (X'D5518ED1A3C24C9A88F94220ED8F28D7', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"product\": {\"value\": null, \"source\": \"static\"}, \"alignment\": {\"value\": null, \"source\": \"static\"}}', NULL, now()),
                    (X'5B39433F79BB4E2D93531242CFF2078A', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"url\": {\"value\": null, \"source\": \"static\"}, \"media\": {\"value\": \"product.manufacturer.media\", \"source\": \"mapped\"}, \"newTab\": {\"value\": true, \"source\": \"static\"}, \"minHeight\": {\"value\": null, \"source\": \"static\"}, \"displayMode\": {\"value\": \"standard\", \"source\": \"static\"}, \"verticalAlign\": {\"value\": null, \"source\": \"static\"}}', NULL, now()),
                    (X'CB3C1E3D528A414EAF0C805CF00AB605', X'0FA91CE3E96A4BC2BE4BD9CE752C3425', X'2FBB5FE2E29A4D70AA5854CE7CE3E20B', '{\"content\": {\"value\": \"product.name\", \"source\": \"mapped\"}, \"verticalAlign\": {\"value\": null, \"source\": \"static\"}}', NULL, now());
                SQL;
    }
}
