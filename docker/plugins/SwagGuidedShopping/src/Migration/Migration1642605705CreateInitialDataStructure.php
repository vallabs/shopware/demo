<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1642605705CreateInitialDataStructure extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1642605705;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=0;');
        foreach ($this->getCreateTableStatements() as $sql) {
            $connection->executeStatement($sql);
        }
        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1;');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    /**
     * @return array<string>
     */
    private function getCreateTableStatements(): array
    {
        $sql_guided_shopping_appointment = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_appointment` (
              `id` binary(16) NOT NULL,
              `guided_shopping_presentation_id` binary(16) DEFAULT NULL,
              `guided_shopping_presentation_version_id` binary(16) DEFAULT NULL,
              `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
              `name` varchar(255) DEFAULT NULL,
              `sales_channel_domain_id` binary(16) DEFAULT NULL,
              `presentation_path` varchar(255) DEFAULT NULL,
              `accessible_from` datetime(3) DEFAULT NULL,
              `accessible_to` datetime(3) DEFAULT NULL,
              `guide_user_id` binary(16) DEFAULT NULL,
              `mode` varchar(255) NOT NULL,
              `video_chat_enabled` tinyint(1) NOT NULL DEFAULT '1',
              `attendee_restriction_type` varchar(255)  DEFAULT NULL,
              `started_at` datetime DEFAULT NULL,
              `attendee_rule_ids` json DEFAULT NULL,
              `ended_at` datetime DEFAULT NULL,
              `custom_fields` json DEFAULT NULL,
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`id`),
              UNIQUE KEY `presentation_path` (`presentation_path`),
              KEY `guided_shopping_presentation_id` (`guided_shopping_presentation_id`,`guided_shopping_presentation_version_id`),
              CONSTRAINT `guided_shopping_appointment_ibfk_1`
              FOREIGN KEY (`guided_shopping_presentation_id`, `guided_shopping_presentation_version_id`) REFERENCES `guided_shopping_presentation` (`id`, `version_id`) ON DELETE SET NULL ON UPDATE CASCADE
            );
SQL;

        $sql_guided_shopping_appointment_attendee = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_appointment_attendee` (
              `id` binary(16) NOT NULL,
              `customer_id` binary(16) DEFAULT NULL,
              `user_id` binary(16) DEFAULT NULL,
              `video_user_id` varchar(50) DEFAULT NULL,
              `attendee_name` varchar(255) DEFAULT NULL,
              `appointment_id` binary(16) NOT NULL,
              `guide_cart_permissions_granted` tinyint(1) DEFAULT '0',
              `joined_at` datetime(3) DEFAULT NULL,
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              `last_active` datetime DEFAULT NULL,
              `type` varchar(10) NOT NULL,
              PRIMARY KEY (`id`),
              KEY `fk.guided_shopping_appointment_attendee.appointment_id` (`appointment_id`),
              CONSTRAINT `fk.guided_shopping_appointment_attendee.appointment_id` FOREIGN KEY (`appointment_id`) REFERENCES `guided_shopping_appointment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            );
SQL;

        $sql_guided_shopping_appointment_video_chat = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_appointment_video_chat` (
              `id` binary(16) NOT NULL,
              `appointment_id` binary(16) NOT NULL,
              `url` varchar(255) DEFAULT NULL,
              `name` varchar(100) DEFAULT NULL,
              `owner_token` varchar(255) DEFAULT NULL,
              `user_token` varchar(255) DEFAULT NULL,
              `start_as_broadcast` tinyint(1) NOT NULL DEFAULT '0',
              `custom_fields` json DEFAULT NULL,
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            );
SQL;

        $sql_guided_shopping_attendee_product_collection = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_attendee_product_collection` (
              `id` binary(16) NOT NULL,
              `attendee_id` binary(16) NOT NULL,
              `product_id` binary(16) NOT NULL,
              `product_version_id` binary(16) NOT NULL,
              `alias` varchar(50) NOT NULL,
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`id`)
            );
SQL;

        $sql_guided_shopping_interaction = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_interaction` (
              `id` binary(16) NOT NULL,
              `attendee_id` binary(16) DEFAULT NULL,
              `name` varchar(100) NOT NULL,
              `expires_at` datetime DEFAULT NULL,
              `payload` json DEFAULT NULL,
              `triggered_at` datetime NOT NULL,
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk.guided_shopping_interaction.attendee_id` (`attendee_id`),
              CONSTRAINT `fk.guided_shopping_interaction.attendee_id` FOREIGN KEY (`attendee_id`) REFERENCES `guided_shopping_appointment_attendee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            );
SQL;

        $sql_guided_shopping_presentation = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_presentation` (
              `id` binary(16) NOT NULL,
              `version_id` binary(16) NOT NULL,
              `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
              `created_at` datetime NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`id`,`version_id`)
            );
SQL;

        $sql_guided_shopping_presentation_translation = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_presentation_translation` (
              `guided_shopping_presentation_id` binary(16) NOT NULL,
              `guided_shopping_presentation_version_id` binary(16) NOT NULL,
              `language_id` binary(16) NOT NULL,
              `name` varchar(255) DEFAULT NULL,
              `custom_fields` json DEFAULT NULL,
              `created_at` datetime(3) NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`guided_shopping_presentation_id`,`guided_shopping_presentation_version_id`,`language_id`),
              KEY `fk.presentation_translation.language_id` (`language_id`),
              CONSTRAINT `fk.presentation_translation.language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.presentation_translation.presentation_id` FOREIGN KEY (`guided_shopping_presentation_id`, `guided_shopping_presentation_version_id`) REFERENCES `guided_shopping_presentation` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            );

SQL;

        $sql_guided_shopping_presentation_cms_page = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_presentation_cms_page` (
              `id` binary(16) NOT NULL,
              `presentation_id` binary(16) NOT NULL,
              `guided_shopping_presentation_version_id` binary(16) NOT NULL,
              `cms_page_id` binary(16) NOT NULL,
              `cms_page_version_id` binary(16) NOT NULL,
              `product_id` binary(16) DEFAULT NULL,
              `product_version_id` binary(16) DEFAULT NULL,
              `product_stream_id` binary(16) DEFAULT NULL,
              `picked_product_ids` json DEFAULT NULL,
              `custom_fields` json DEFAULT NULL,
              `position` int(8) NOT NULL DEFAULT '0',
              `created_at` datetime(3) NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`id`),
              KEY `fk.presentation_cms_page.cms_page_id` (`cms_page_id`),
              KEY `fk.presentation_cms_page.product_id` (`product_id`,`product_version_id`),
              KEY `fk.presentation_cms_page.product_stream_id` (`product_stream_id`),
              KEY `fk.guided_shopping_presentation.cms_page_presentation_id` (`presentation_id`),
              CONSTRAINT `fk.guided_shopping_presentation.cms_page_presentation_id` FOREIGN KEY (`presentation_id`) REFERENCES `guided_shopping_presentation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.presentation_cms_page.cms_page_id` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.presentation_cms_page.product_id` FOREIGN KEY (`product_id`, `product_version_id`) REFERENCES `product` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.presentation_cms_page.product_stream_id` FOREIGN KEY (`product_stream_id`) REFERENCES `product_stream` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            );
SQL;

        $sql_guided_shopping_presentation_cms_page_translation = <<<'SQL'
            CREATE TABLE IF NOT EXISTS `guided_shopping_presentation_cms_page_translation` (
              `guided_shopping_presentation_cms_page_id` binary(16) NOT NULL,
              `language_id` binary(16) NOT NULL,
              `title` varchar(255) DEFAULT NULL,
              `custom_fields` json DEFAULT NULL,
              `slot_config` json DEFAULT NULL,
              `created_at` datetime(3) NOT NULL,
              `updated_at` datetime DEFAULT NULL,
              PRIMARY KEY (`guided_shopping_presentation_cms_page_id`,`language_id`),
              KEY `fk.presentation_cms_page_translation.language_id` (`language_id`),
              CONSTRAINT `fk.presentation_cms_page_translation.language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.presentation_cms_page_translation.presentation_id` FOREIGN KEY (`guided_shopping_presentation_cms_page_id`) REFERENCES `guided_shopping_presentation_cms_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            );
SQL;

        $statements[] = $sql_guided_shopping_appointment;
        $statements[] = $sql_guided_shopping_appointment_attendee;
        $statements[] = $sql_guided_shopping_appointment_video_chat;
        $statements[] = $sql_guided_shopping_attendee_product_collection;
        $statements[] = $sql_guided_shopping_interaction;
        $statements[] = $sql_guided_shopping_presentation;
        $statements[] = $sql_guided_shopping_presentation_translation;
        $statements[] = $sql_guided_shopping_presentation_cms_page;
        $statements[] = $sql_guided_shopping_presentation_cms_page_translation;

        return $statements;
    }
}
