<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1688024751AppointmentInvitation extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1688024751;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'message')) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `message` longtext DEFAULT NULL AFTER `mode`;
            ');
        }

        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment_attendee', 'invitation_status')) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment_attendee`
                ADD COLUMN `invitation_status` varchar(100) DEFAULT NULL AFTER `appointment_id`;
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
