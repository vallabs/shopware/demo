<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1648897694CreatedBy extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1648897694;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_presentation', 'created_by_id')) {
            $connection->executeStatement("
                ALTER TABLE `guided_shopping_presentation`
                ADD COLUMN `created_by_id` BINARY(16) NULL AFTER `created_at`,
                ADD CONSTRAINT `fk.guided_shopping_presentation.created_by_id`
                    FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
            ");
        }

        if (!MigrationStep::columnExists($connection, 'guided_shopping_presentation', 'updated_by_id')) {
            $connection->executeStatement("
                ALTER TABLE `guided_shopping_presentation`
                ADD COLUMN `updated_by_id` BINARY(16) NULL AFTER `updated_at`,
                ADD CONSTRAINT `fk.guided_shopping_presentation.updated_by_id`
                    FOREIGN KEY (`updated_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
            ");
        }

        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'created_by_id')) {
            $connection->executeStatement("
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `created_by_id` BINARY(16) NULL AFTER `created_at`,
                ADD CONSTRAINT `fk.guided_shopping_appointment.created_by_id`
                    FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
            ");
        }

        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'updated_by_id')) {
            $connection->executeStatement("
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `updated_by_id` BINARY(16) NULL AFTER `updated_at`,
                ADD CONSTRAINT `fk.guided_shopping_appointment.updated_by_id`
                    FOREIGN KEY (`updated_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
            ");
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
