<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1666081431AddPreviewPresentationIntoPresentationTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1666081431;
    }

    public function update(Connection $connection): void
    {
        if (
            !MigrationStep::columnExists($connection, 'guided_shopping_presentation', 'parent_id')
            && !MigrationStep::columnExists($connection, 'guided_shopping_presentation', 'parent_version_id')
        ) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_presentation`
                ADD COLUMN `parent_id` BINARY(16) NULL AFTER `active`,
                ADD COLUMN `parent_version_id` BINARY(16) NULL AFTER `parent_id`,
                ADD CONSTRAINT `fk.guided_shopping_presentation.parent_id`
                FOREIGN KEY (`parent_id`, `parent_version_id`) REFERENCES `guided_shopping_presentation` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
