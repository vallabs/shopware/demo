<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1705293057AppointmentTranslationNameIsNullable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1705293057;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement('
            ALTER TABLE `guided_shopping_appointment_translation` 
            CHANGE `name` `name` VARCHAR(255) NULL;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
