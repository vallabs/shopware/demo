<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Content\MailTemplate\Aggregate\MailTemplateTranslation\MailTemplateTranslationDefinition;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Migration\Traits\ImportTranslationsTrait;
use Shopware\Core\Migration\Traits\Translations;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class Migration1692773910ModifyInvitationMailTemplate extends MigrationStep
{
    use ImportTranslationsTrait;

    public function getCreationTimestamp(): int
    {
        return 1692773910;
    }

    public function update(Connection $connection): void
    {
        $inviteMailTemplateId = '54682107D9924907A4D524760B17AECF';

        // delete old invite mail template translations
        $connection->executeStatement("
            DELETE FROM `mail_template_translation` 
            WHERE `mail_template_id` = UNHEX('54682107D9924907A4D524760B17AECF')
        ");

        $this->createMailTemplateTranslation(
            $connection,
            $inviteMailTemplateId,
            ['de-DE' => 'EINGELADEN', 'en-GB' => 'INVITED'],
            ['de-DE' => 'Versenden Sie die Termineinladung', 'en-GB' => 'Send the appointment invitation'],
            $this->createInviteContentHtml(),
            $this->createInviteContentPlain()
        );

    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    /**
     * @param array<string> $prefixSubject
     * @param array<string> $description
     */
    private function createMailTemplateTranslation(
        Connection $connection,
        string $mailTemplateId,
        array $prefixSubject,
        array $description,
        string $contentHtml,
        string $contentPlain
    ): void
    {
        $id = Uuid::fromHexToBytes($mailTemplateId);
        $senderName = '{{ appointment.guideUser.firstName }} {{ appointment.guideUser.lastName }}';

        $translations = new Translations(
            [
                'mail_template_id' => $id,
                'sender_name' => $senderName,
                'subject' => "{$prefixSubject['de-DE']}: {{ appointment.name }}" ,
                'description' => $description['de-DE'],
                'content_html' => $contentHtml,
                'content_plain' => $contentPlain
            ],
            [
                'mail_template_id' => $id,
                'sender_name' => $senderName,
                'subject' => "{$prefixSubject['en-GB']}: {{ appointment.name }}",
                'description' => $description['en-GB'],
                'content_html' => $contentHtml,
                'content_plain' => $contentPlain
            ]
        );

        $this->importTranslation(MailTemplateTranslationDefinition::ENTITY_NAME, $translations, $connection);
    }

    private function createInviteContentHtml(): string
    {
        return '
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="width: 100%">
              <tr>
                <td class="">
                 <div class="greeting">
                    {% if appointment.message %}
                      <div style="margin-bottom: 20px;">
                        {{ appointment.message|raw }}
                      </div>
                    {% endif %}
                    <div class="invitation-content" style="padding: 32px; background-color: #F9FAFB;">
                      <table role="presentation">
                        <tr>
                          <td>
                            <h3 style="margin: 0; font-size: 20px; font-weight: 700px; color: #1F262E">
                            {% if appointment.customFields.isGuide %}
                                You created successfully an appointment with Digital Sales Rooms.
                            {% else %}
                                You are invited to a Digital Sales Rooms experience.
                            {% endif %}
                            </h3>
                          </td>
                        </tr>
                        {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 16px; font-size: 16px; color: #1F262E">
                                {% if appointment.customFields.isGuide %}
                                  Date: 
                                  {% if appointment.customFields.isSameDay %}
                                    <b>{{ appointment.customFields.accessibleFrom.date }}</b> at <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> - <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                {% else %}
                                  <b style="text-transform: capitalize; font-weight: 700; color: #0870FF">{{ appointment.salesChannelDomain.salesChannel.translated.name }}</b>
                                  invited you to a Digital Sales Rooms presentation that will take place
                                  {% if appointment.customFields.isSameDay %}
                                    on <b>{{ appointment.customFields.accessibleFrom.date }}</b> at <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    from <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> to <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                {% endif %}
                                </p>
                              </td>
                            </tr>
                        {% endif %}
                        {% if appointment.mode === "'. AppointmentDefinition::MODE_GUIDED .'" and appointment.customFields.isGuide === false %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  Are you going to participate in this presentation?
                                </p>
                              </td>
                            </tr>
                            <tr style="margin: 0">
                              <td style="padding-top: 12px">
                                <table border="0" cellpadding="0px" cellspacing="0" style="width: auto; min-width: unset">
                                  <tr>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}">Yes</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}">Maybe</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}" >No</a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  You can join the presentation here:
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td style="padding-top: 12px">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Join presentation
                                </a>
                              </td>
                            </tr>
                        {% else %}
                            <tr>
                              <td style="margin: 0; padding-top: 40px;">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Get started
                                </a>
                              </td>
                            </tr>
                        {% endif %}
                      </table>
                    </div>
                </td>
              </tr>
            </table>
        ';
    }

    private function createInviteContentPlain(): string
    {
        return '
            {% if appointment.message %}
                {{ appointment.message|raw }}
            {% endif %}
            {% if appointment.customFields.isGuide %}
                You created successfully an appointment with Digital Sales Rooms.
            {% else %}
                You are invited to a Digital Sales Rooms experience.
            {% endif %}
            {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                {% if appointment.customFields.isGuide %}
                    Date: 
                    {% if appointment.customFields.isSameDay %}
                      {{ appointment.customFields.accessibleFrom.date }} at {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                    {% else %}
                      {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
                {% else %}
                    {{ appointment.salesChannelDomain.salesChannel.translated.name }} invited you to a Digital Sales Rooms presentation that will take place
                    {% if appointment.customFields.isSameDay %}
                        on {{ appointment.customFields.accessibleFrom.date }} at {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                    {% else %}
                        from {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} to {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
                {% endif %}
            {% endif %}
            {% if appointment.mode === "' . AppointmentDefinition::MODE_GUIDED . '" and appointment.customFields.isGuide === false %}
                Are you going to participate in this presentation?
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}"Yes
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}"Maybe
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}"No
                You can join the presentation here.
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Join presentation
            {% else %}
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Get started
            {% endif %}
        ';
    }
}
