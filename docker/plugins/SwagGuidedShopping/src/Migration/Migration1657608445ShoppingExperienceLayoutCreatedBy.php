<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1657608445ShoppingExperienceLayoutCreatedBy extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1657608445;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'cms_page', 'created_by_id')) {
            $connection->executeStatement('
                ALTER TABLE `cms_page`
                ADD COLUMN `created_by_id` BINARY(16) NULL AFTER `created_at`,
                ADD CONSTRAINT `fk.cms_page.created_by_id`
                    FOREIGN KEY (`created_by_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
