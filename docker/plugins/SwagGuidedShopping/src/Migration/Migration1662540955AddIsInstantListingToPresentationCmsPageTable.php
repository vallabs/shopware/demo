<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1662540955AddIsInstantListingToPresentationCmsPageTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1662540955;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_presentation_cms_page', 'is_instant_listing')) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_presentation_cms_page`
                ADD COLUMN `is_instant_listing` tinyint(1) NOT NULL DEFAULT 0 AFTER `position`
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
