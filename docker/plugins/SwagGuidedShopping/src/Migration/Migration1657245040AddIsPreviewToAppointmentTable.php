<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1657245040AddIsPreviewToAppointmentTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1657245040;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'is_preview')) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `is_preview` tinyint(1) NOT NULL DEFAULT 0 AFTER `mode`
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
