<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1686213125ModifyAppointmentVideoAudioSettings extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1686213125;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'video_audio_settings')) {
            // Add new column to apply new changes
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `video_audio_settings` varchar(100) NOT NULL AFTER `is_preview`;
            ');
        }

        if (MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'video_chat_enabled')) {
            // Update the old appointments
            $connection->executeStatement('
                UPDATE `guided_shopping_appointment` SET `video_audio_settings` = CASE
                WHEN `video_chat_enabled` = 1 THEN "both"
                WHEN `video_chat_enabled` = 0 THEN "none"
                ELSE "none"
                END;
            ');

            // Remove the old column after updating the old appointments
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment`
                DROP COLUMN `video_chat_enabled`;
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
