<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1688376618AddEmailColumnIntoAttendeeTable extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1688376618;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment_attendee', 'attendee_email')) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment_attendee`
                ADD COLUMN `attendee_email` varchar(100) DEFAULT NULL AFTER `attendee_name`;
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
