<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1649691126DefaultAppointmentField extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1649691126;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment', 'default')) {
            $connection->executeStatement("
                ALTER TABLE `guided_shopping_appointment`
                ADD COLUMN `default` tinyint(1) unsigned NOT NULL DEFAULT 0 AFTER `ended_at`;
            ");
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
