<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Migration\Traits\ImportTranslationsTrait;
use Shopware\Core\Migration\Traits\Translations;
use SwagGuidedShopping\Content\Appointment\Aggregate\AppointmentTranslation\AppointmentTranslationDefinition;

class Migration1704181304CreateAppointmentTranslation extends MigrationStep
{
    use ImportTranslationsTrait;

    public function getCreationTimestamp(): int
    {
        return 1704181304;
    }

    public function update(Connection $connection): void
    {
        $this->createTranslationTable($connection);

        $nameColumnExists = $this->checkIfColumnExists('name', $connection);
        $messageColumnExists = $this->checkIfColumnExists('message', $connection);

        if ($nameColumnExists && $messageColumnExists) {
            $this->addTranslationWithExistingAppointment($connection);
        }

        if ($nameColumnExists) {
            $this->dropColumn('name', $connection);
        }

        if ($messageColumnExists) {
            $this->dropColumn('message', $connection);
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function createTranslationTable(Connection $connection): void
    {
        $connection->executeStatement('
            CREATE TABLE IF NOT EXISTS `guided_shopping_appointment_translation` (
                `guided_shopping_appointment_id` BINARY(16) NOT NULL,
                `language_id` BINARY(16) NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                `message` LONGTEXT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`guided_shopping_appointment_id`, `language_id`),
                CONSTRAINT `fk.appointment_translation.appointment_id` FOREIGN KEY (`guided_shopping_appointment_id`)
                  REFERENCES `guided_shopping_appointment` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.appointment_translation.language_id` FOREIGN KEY (`language_id`)
                  REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    private function addTranslationWithExistingAppointment(Connection $connection): void
    {
        $appointments = $connection->fetchAllAssociative('
            SELECT id, name, message FROM guided_shopping_appointment;
        ');

        foreach ($appointments as $appointment) {
            $translations = new Translations(
                [
                    'guided_shopping_appointment_id' => $appointment['id'],
                    'name' => $appointment['name'],
                    'message' => $appointment['message'],
                ],
                [
                    'guided_shopping_appointment_id' => $appointment['id'],
                    'name' => $appointment['name'],
                    'message' => $appointment['message'],
                ]
            );

            $this->importTranslation(AppointmentTranslationDefinition::ENTITY_NAME, $translations, $connection);
        }
    }

    private function dropColumn(string $columnName, Connection $connection): void
    {
        $connection->executeStatement("
            ALTER TABLE `guided_shopping_appointment`
            DROP COLUMN `{$columnName}`;
        ");
    }

    private function checkIfColumnExists(string $columnName, Connection $connection): bool
    {
        $result = $connection->fetchOne("
            SHOW COLUMNS FROM `guided_shopping_appointment` LIKE '{$columnName}';
        ");

        return $result !== false;
    }
}
