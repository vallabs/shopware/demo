<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1672841561AddCmsSectionTranslation extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1672841561;
    }

    public function update(Connection $connection): void
    {
        $connection->executeStatement('
            CREATE TABLE IF NOT EXISTS `cms_section_translation` (
              `cms_section_id` BINARY(16) NOT NULL,
              `cms_section_version_id` BINARY(16) NOT NULL,
              `language_id` BINARY(16) NOT NULL,
              `name` VARCHAR(255) COLLATE utf8mb4_unicode_ci NULL,
              `created_at` DATETIME(3) NOT NULL,
              `updated_at` DATETIME(3) NULL,
              PRIMARY KEY (`cms_section_id`, `cms_section_version_id`, `language_id`),
              CONSTRAINT `fk.cms_section_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
              CONSTRAINT `fk.cms_section_translation.cms_section_id` FOREIGN KEY (`cms_section_id`, `cms_section_version_id`)
                REFERENCES `cms_section` (`id`, `version_id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
