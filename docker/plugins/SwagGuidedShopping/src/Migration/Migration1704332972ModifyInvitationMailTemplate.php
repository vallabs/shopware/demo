<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Content\MailTemplate\Aggregate\MailTemplateTranslation\MailTemplateTranslationDefinition;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Migration\Traits\ImportTranslationsTrait;
use Shopware\Core\Migration\Traits\Translations;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;

class Migration1704332972ModifyInvitationMailTemplate extends MigrationStep
{
    use ImportTranslationsTrait;

    public function getCreationTimestamp(): int
    {
        return 1704332972;
    }

    public function update(Connection $connection): void
    {
        $inviteMailTemplateId = '54682107D9924907A4D524760B17AECF';
        $cancelMailTemplateId = 'EEFEAC3598F84A23AFCB4485384032DE';

        // invite mail template
        $this->deleteOldMailTemplateTranslations($connection, $inviteMailTemplateId);
        $this->createMailTemplateTranslation(
            $connection,
            $inviteMailTemplateId,
            ['de-DE' => 'Sie sind zur Teilnahme eingeladen', 'en-GB' => 'You are invited to join'],
            ['de-DE' => 'Versenden Sie die Termineinladung', 'en-GB' => 'Send the appointment invitation'],
            ['de-DE' => $this->createInviteContentHtmlInGerman(), 'en-GB' => $this->createInviteContentHtmlInEnglish()],
            ['de-DE' => $this->createInviteContentPlainInGerman(), 'en-GB' => $this->createInviteContentPlainInEnglish()]
        );

        // cancel mail template
        $this->deleteOldMailTemplateTranslations($connection, $cancelMailTemplateId);
        $this->createMailTemplateTranslation(
            $connection,
            $cancelMailTemplateId,
            ['de-DE' => 'Präsentation abgesag', 'en-GB' => 'Presentation cancelled'],
            ['de-DE' => 'Stornieren Sie die Termineinladung', 'en-GB' => 'Cancel the appointment invitation'],
            ['de-DE' => $this->createCancelContentHtmlInGerman(), 'en-GB' => $this->createCancelContentHtmlInEnglish()],
            ['de-DE' => $this->createCancelContentPlainInGerman(), 'en-GB' => $this->createCancelContentPlainInEnglish()]
        );
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function deleteOldMailTemplateTranslations(Connection $connection, string $mailTemplateId): void
    {
        $connection->executeStatement("
            DELETE FROM `mail_template_translation` 
            WHERE `mail_template_id` = UNHEX('{$mailTemplateId}')
        ");
    }

    /**
     * @param array<string> $prefixSubject
     * @param array<string> $description
     * @param array<string> $contentHtml
     * @param array<string> $contentPlain
     */
    private function createMailTemplateTranslation(
        Connection $connection,
        string $mailTemplateId,
        array $prefixSubject,
        array $description,
        array $contentHtml,
        array $contentPlain
    ): void
    {
        $id = Uuid::fromHexToBytes($mailTemplateId);
        $senderName = '{{ appointment.guideUser.firstName }} {{ appointment.guideUser.lastName }}';

        $translations = new Translations(
            [
                'mail_template_id' => $id,
                'sender_name' => $senderName,
                'subject' => "{$prefixSubject['de-DE']}: {{ appointment.name }}" ,
                'description' => $description['de-DE'],
                'content_html' => $contentHtml['de-DE'],
                'content_plain' => $contentPlain['de-DE']
            ],
            [
                'mail_template_id' => $id,
                'sender_name' => $senderName,
                'subject' => "{$prefixSubject['en-GB']}: {{ appointment.name }}",
                'description' => $description['en-GB'],
                'content_html' => $contentHtml['en-GB'],
                'content_plain' => $contentPlain['en-GB']
            ]
        );

        $this->importTranslation(MailTemplateTranslationDefinition::ENTITY_NAME, $translations, $connection);
    }

    private function createInviteContentHtmlInEnglish(): string
    {
        return '
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="width: 100%">
              <tr>
                <td class="">
                 <div class="greeting">
                    {% if appointment.message %}
                      <div style="margin-bottom: 20px;">
                        {{ appointment.message|raw }}
                      </div>
                    {% endif %}
                    <div class="invitation-content" style="padding: 32px; background-color: #F9FAFB;">
                      <table role="presentation">
                        <tr>
                          <td>
                            <h3 style="margin: 0; font-size: 20px; font-weight: 700px; color: #1F262E">
                            {% if appointment.customFields.isGuide %}
                                You have successfully created an appointment with Digital Sales Rooms.
                            {% else %}
                                You are invited to a Digital Sales Rooms experience.
                            {% endif %}
                            </h3>
                          </td>
                        </tr>
                        {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 16px; font-size: 16px; color: #1F262E">
                                {% if appointment.customFields.isGuide %}
                                  Date: 
                                  {% if appointment.customFields.isSameDay %}
                                    <b>{{ appointment.customFields.accessibleFrom.date }}</b> at <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> - <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                {% else %}
                                  <b style="text-transform: capitalize; font-weight: 700; color: #0870FF">{{ appointment.salesChannelDomain.salesChannel.translated.name }}</b>
                                  invited you to a Digital Sales Rooms presentation that will take place
                                  {% if appointment.customFields.isSameDay %}
                                    on <b>{{ appointment.customFields.accessibleFrom.date }}</b> at <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    from <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> to <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                {% endif %}
                                </p>
                              </td>
                            </tr>
                        {% endif %}
                        {% if appointment.mode === "'. AppointmentDefinition::MODE_GUIDED .'" and appointment.customFields.isGuide === false %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  Are you going to participate in this presentation?
                                </p>
                              </td>
                            </tr>
                            <tr style="margin: 0">
                              <td style="padding-top: 12px">
                                <table border="0" cellpadding="0px" cellspacing="0" style="width: auto; min-width: unset">
                                  <tr>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}">Yes</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}">Maybe</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}" >No</a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  You can join the presentation here:
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td style="padding-top: 12px">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Join presentation
                                </a>
                              </td>
                            </tr>
                        {% else %}
                            <tr>
                              <td style="margin: 0; padding-top: 40px;">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Get started
                                </a>
                              </td>
                            </tr>
                        {% endif %}
                      </table>
                    </div>
                </td>
              </tr>
            </table>
        ';
    }

    private function createInviteContentHtmlInGerman(): string
    {
        return '
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="width: 100%">
              <tr>
                <td class="">
                 <div class="greeting">
                    {% if appointment.message %}
                      <div style="margin-bottom: 20px;">
                        {{ appointment.message|raw }}
                      </div>
                    {% endif %}
                    <div class="invitation-content" style="padding: 32px; background-color: #F9FAFB;">
                      <table role="presentation">
                        <tr>
                          <td>
                            <h3 style="margin: 0; font-size: 20px; font-weight: 700px; color: #1F262E">
                            {% if appointment.customFields.isGuide %}
                                Sie haben erfolgreich einen Termin mit Digital Sales Rooms erstellt.
                            {% else %}
                                Sie wurden zu einem Digital Sales Room eingeladen.
                            {% endif %}
                            </h3>
                          </td>
                        </tr>
                        {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 16px; font-size: 16px; color: #1F262E">
                                {% if appointment.customFields.isGuide %}
                                  Datum: 
                                  {% if appointment.customFields.isSameDay %}
                                    <b>{{ appointment.customFields.accessibleFrom.date }}</b> zwischen <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> - <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                {% else %}
                                  <b style="text-transform: capitalize; font-weight: 700; color: #0870FF">{{ appointment.salesChannelDomain.salesChannel.translated.name }}</b>
                                  hat Sie zu einer Präsentation in Digital Sales Rooms eingeladen
                                  {% if appointment.customFields.isSameDay %}
                                    am <b>{{ appointment.customFields.accessibleFrom.date }}</b> zwischen <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    von <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> bis <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                {% endif %}
                                </p>
                              </td>
                            </tr>
                        {% endif %}
                        {% if appointment.mode === "'. AppointmentDefinition::MODE_GUIDED .'" and appointment.customFields.isGuide === false %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  Können Sie an dieser Präsentation teilnehmen?
                                </p>
                              </td>
                            </tr>
                            <tr style="margin: 0">
                              <td style="padding-top: 12px">
                                <table border="0" cellpadding="0px" cellspacing="0" style="width: auto; min-width: unset">
                                  <tr>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}">Ja</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}">Vielleicht</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}" >Nein</a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  Hierüber können Sie der Präsentation beitreten.
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td style="padding-top: 12px">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Präsentation beitreten
                                </a>
                              </td>
                            </tr>
                        {% else %}
                            <tr>
                              <td style="margin: 0; padding-top: 40px;">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Los geht’s
                                </a>
                              </td>
                            </tr>
                        {% endif %}
                      </table>
                    </div>
                </td>
              </tr>
            </table>
        ';
    }

    private function createInviteContentPlainInEnglish(): string
    {
        return '
            {% if appointment.message %}
                {{ appointment.message|raw }}
            {% endif %}
            {% if appointment.customFields.isGuide %}
                You have successfully created an appointment with Digital Sales Rooms.
            {% else %}
                You are invited to a Digital Sales Rooms experience.
            {% endif %}
            {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                {% if appointment.customFields.isGuide %}
                    Date: 
                    {% if appointment.customFields.isSameDay %}
                      {{ appointment.customFields.accessibleFrom.date }} at {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                    {% else %}
                      {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
                {% else %}
                    {{ appointment.salesChannelDomain.salesChannel.translated.name }} invited you to a Digital Sales Rooms presentation that will take place
                    {% if appointment.customFields.isSameDay %}
                        on {{ appointment.customFields.accessibleFrom.date }} at {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                    {% else %}
                        from {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} to {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
                {% endif %}
            {% endif %}
            {% if appointment.mode === "' . AppointmentDefinition::MODE_GUIDED . '" and appointment.customFields.isGuide === false %}
                Are you going to participate in this presentation?
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}"Yes
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}"Maybe
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}"No
                You can join the presentation here.
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Join presentation
            {% else %}
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Get started
            {% endif %}
        ';
    }

    private function createInviteContentPlainInGerman(): string
    {
        return '
            {% if appointment.message %}
                {{ appointment.message|raw }}
            {% endif %}
            {% if appointment.customFields.isGuide %}
                Sie haben erfolgreich einen Termin mit Digital Sales Rooms erstellt.
            {% else %}
                Sie wurden zu einem Digital Sales Room eingeladen.
            {% endif %}
            {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                {% if appointment.customFields.isGuide %}
                    Datum: 
                    {% if appointment.customFields.isSameDay %}
                      {{ appointment.customFields.accessibleFrom.date }} zwischen {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                    {% else %}
                      {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
                {% else %}
                    {{ appointment.salesChannelDomain.salesChannel.translated.name }} hat Sie zu einer Präsentation in Digital Sales Rooms eingeladen
                    {% if appointment.customFields.isSameDay %}
                        am {{ appointment.customFields.accessibleFrom.date }} zwischen {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                    {% else %}
                        von {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} bis {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
                {% endif %}
            {% endif %}
            {% if appointment.mode === "' . AppointmentDefinition::MODE_GUIDED . '" and appointment.customFields.isGuide === false %}
                Können Sie an dieser Präsentation teilnehmen?
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}"Ja
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}"Vielleicht
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}"Nein
                Hierüber können Sie der Präsentation beitreten.
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Präsentation beitreten
            {% else %}
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Los geht’s
            {% endif %}
        ';
    }

    private function createCancelContentHtmlInEnglish(): string
    {
        return '
            The appointment {{appointment.name}} you have been invited to has unfortunately been cancelled.
            {% if appointment.customFields.cancelMessage %}
                Comment:
                {{ appointment.customFields.cancelMessage|raw }}
            {% endif %}
        ';
    }

    private function createCancelContentHtmlInGerman(): string
    {
        return '
            Die Termin {{appointment.name}} zur der Sie eingeladen sind, wurde leider abgesagt.
            {% if appointment.customFields.cancelMessage %}
                Kommentar:
                {{ appointment.customFields.cancelMessage|raw }}
            {% endif %}
        ';
    }

    private function createCancelContentPlainInEnglish(): string
    {
        return '
            The appointment {{appointment.name}} you have been invited to has unfortunately been cancelled.
            {% if appointment.customFields.cancelMessage %}
                Comment:
                {{ appointment.customFields.cancelMessage|raw }}
            {% endif %}
        ';
    }

    private function createCancelContentPlainInGerman(): string
    {
        return '
            Die Termin {{appointment.name}} zur der Sie eingeladen sind, wurde leider abgesagt.
            {% if appointment.customFields.cancelMessage %}
                Kommentar:
                {{ appointment.customFields.cancelMessage|raw }}
            {% endif %}
        ';
    }
}
