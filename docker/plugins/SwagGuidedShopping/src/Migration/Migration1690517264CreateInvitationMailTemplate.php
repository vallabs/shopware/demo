<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Content\MailTemplate\Aggregate\MailTemplateTranslation\MailTemplateTranslationDefinition;
use Shopware\Core\Content\MailTemplate\Aggregate\MailTemplateTypeTranslation\MailTemplateTypeTranslationDefinition;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Migration\Traits\Translations;
use SwagGuidedShopping\Content\Appointment\AppointmentDefinition;
use Shopware\Core\Migration\Traits\ImportTranslationsTrait;

class Migration1690517264CreateInvitationMailTemplate extends MigrationStep
{
    use ImportTranslationsTrait;

    public function getCreationTimestamp(): int
    {
        return 1690517264;
    }

    public function update(Connection $connection): void
    {
        $inviteMailTemplateTypeId = '0A7ACFDA21AD4052869219EE884AC069';
        $cancelMailTemplateTypeId = 'E7AFC9E22B2B42F4A015A8389AF245B5';
        $inviteMailTemplateId = '54682107D9924907A4D524760B17AECF';
        $cancelMailTemplateId = 'EEFEAC3598F84A23AFCB4485384032DE';

        $this->createMailTemplateType(
            $connection,
            $inviteMailTemplateTypeId,
            $cancelMailTemplateTypeId
        );

        $this->createMailTemplate(
            $connection,
            $inviteMailTemplateTypeId,
            $cancelMailTemplateTypeId,
            $inviteMailTemplateId,
            $cancelMailTemplateId
        );

        $this->createMailTemplateTypeTranslation(
            $connection,
            $inviteMailTemplateTypeId,
            ['de-DE' => 'Termin: Einladung verschicken', 'en-GB' => 'Appointment: Send the invitation']
        );

        $this->createMailTemplateTypeTranslation(
            $connection,
            $cancelMailTemplateTypeId,
            ['de-DE' => 'Termin: Einladung absagen', 'en-GB' => 'Appointment: Cancel the invitation']
        );

        $this->createMailTemplateTranslation(
            $connection,
            $inviteMailTemplateId,
            ['de-DE' => 'EINGELADEN', 'en-GB' => 'INVITED'],
            ['de-DE' => 'Versenden Sie die Termineinladung', 'en-GB' => 'Send the appointment invitation'],
            $this->createInviteContentHtml(),
            $this->createInviteContentPlain()
        );

        $this->createMailTemplateTranslation(
            $connection,
            $cancelMailTemplateId,
            ['de-DE' => 'ABGESAGT', 'en-GB' => 'CANCELLED'],
            ['de-DE' => 'Stornieren Sie die Termineinladung', 'en-GB' => 'Cancel the appointment invitation'],
            $this->createCancelContentHtml(),
            $this->createCancelContentPlain()
        );
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    private function createMailTemplateType(
        Connection $connection,
        string $inviteMailTemplateTypeId,
        string $cancelMailTemplateTypeId
    ): void
    {
        $availableEntities = \json_encode([
            'appointment' => 'guided_shopping_appointment'
        ]);

        $connection->executeStatement("
            INSERT IGNORE INTO `mail_template_type` (`id`, `technical_name`, `available_entities`, `template_data`, `created_at`)
            VALUES
                (UNHEX(:inviteMailTemplateTypeId), 'appointment_invitation.send', :availableEntities, :inviteMailTemplateData, now()),
                (UNHEX(:cancelMailTemplateTypeId), 'appointment_invitation.cancel', :availableEntities, :cancelMailTemplateData, now())
        ", [
            'inviteMailTemplateTypeId' => $inviteMailTemplateTypeId,
            'cancelMailTemplateTypeId' => $cancelMailTemplateTypeId,
            'availableEntities' => $availableEntities,
            'inviteMailTemplateData' => \json_encode($this->createInviteMailTemplateData()),
            'cancelMailTemplateData' => \json_encode($this->createCancelMailTemplateData())
        ]);
    }

    private function createMailTemplate(
        Connection $connection,
        string $inviteMailTemplateTypeId,
        string $cancelMailTemplateTypeId,
        string $inviteMailTemplateId,
        string $cancelMailTemplateId
    ): void
    {
        $connection->executeStatement("
            INSERT IGNORE INTO `mail_template` (`id`, `mail_template_type_id`, `created_at`)
            VALUES
                (UNHEX(:inviteMailTemplateId), UNHEX(:inviteMailTemplateTypeId), now()),
                (UNHEX(:cancelMailTemplateId), UNHEX(:cancelMailTemplateTypeId), now())
        ", [
            'inviteMailTemplateTypeId' => $inviteMailTemplateTypeId,
            'cancelMailTemplateTypeId' => $cancelMailTemplateTypeId,
            'inviteMailTemplateId' => $inviteMailTemplateId,
            'cancelMailTemplateId' => $cancelMailTemplateId
        ]);
    }

    /**
     * @param array<string> $name
     */
    private function createMailTemplateTypeTranslation(
        Connection $connection,
        string $mailTemplateTypeId,
        array $name
    ): void
    {
        $id = Uuid::fromHexToBytes($mailTemplateTypeId);

        $translations = new Translations(
            [
                'mail_template_type_id' => $id,
                'name' => $name['de-DE']
            ],
            [
                'mail_template_type_id' => $id,
                'name' => $name['en-GB']
            ]
        );

        $this->importTranslation(MailTemplateTypeTranslationDefinition::ENTITY_NAME, $translations, $connection);
    }

    /**
     * @param array<string> $prefixSubject
     * @param array<string> $description
     */
    private function createMailTemplateTranslation(
        Connection $connection,
        string $mailTemplateId,
        array $prefixSubject,
        array $description,
        string $contentHtml,
        string $contentPlain
    ): void
    {
        $id = Uuid::fromHexToBytes($mailTemplateId);
        $senderName = '{{ appointment.guideUser.firstName }} {{ appointment.guideUser.lastName }}';

        $translations = new Translations(
            [
                'mail_template_id' => $id,
                'sender_name' => $senderName,
                'subject' => "{$prefixSubject['de-DE']}: {{ appointment.name }}" ,
                'description' => $description['de-DE'],
                'content_html' => $contentHtml,
                'content_plain' => $contentPlain
            ],
            [
                'mail_template_id' => $id,
                'sender_name' => $senderName,
                'subject' => "{$prefixSubject['en-GB']}: {{ appointment.name }}",
                'description' => $description['en-GB'],
                'content_html' => $contentHtml,
                'content_plain' => $contentPlain
            ]
        );

        $this->importTranslation(MailTemplateTranslationDefinition::ENTITY_NAME, $translations, $connection);
    }

    private function createInviteContentHtml(): string
    {
        return '
            <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="width: 100%">
              <tr>
                <td class="">
                 <div class="greeting">
                    {% if appointment.message %}
                      <div style="margin-bottom: 20px;">
                        {{ appointment.message|raw }}
                      </div>
                    {% endif %}
                    <div class="invitation-content" style="padding: 32px; background-color: #F9FAFB;">
                      <table role="presentation">
                        <tr>
                          <td>
                            <h3 style="margin: 0; font-size: 20px; font-weight: 700px; color: #1F262E">You are invited to a Digital Sales Room experience.</h3>
                          </td>
                        </tr>
                        {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 16px; font-size: 16px; color: #1F262E">
                                  <b style="text-transform: capitalize; font-weight: 700; color: #0870FF">{{ appointment.salesChannelDomain.salesChannel.translated.name }}</b>
                                  invited you to a Digital Sales Room presentation that will take place
                                  {% if appointment.customFields.isSameDay %}
                                    on <b>{{ appointment.customFields.accessibleFrom.date }}</b> at <b>{{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleFrom.timezone }}).
                                  {% else %}
                                    from <b>{{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }}</b> to <b>{{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }}</b> ({{ appointment.customFields.accessibleTo.timezone }}).
                                  {% endif %}
                                </p>
                              </td>
                            </tr>
                        {% endif %}
                        {% if appointment.mode === "'. AppointmentDefinition::MODE_GUIDED .'" and appointment.customFields.isGuide === false %}
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  Are you going to participate in this presentation?
                                </p>
                              </td>
                            </tr>
                            <tr style="margin: 0">
                              <td style="padding-top: 12px">
                                <table border="0" cellpadding="0px" cellspacing="0" style="width: auto; min-width: unset">
                                  <tr>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}">Yes</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}">Maybe</a>
                                    </td>
                                    <td style="padding-right: 8px">
                                        <a style="background-color: #E5E7EB; color: #1F262E; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none; display: block;" href="{{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}" >No</a>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <p style="margin: 0; padding-top: 40px; font-size: 14px; font-weight: 700; color: #1F262E">
                                  You can join the presentation here:
                                </p>
                              </td>
                            </tr>
                            <tr>
                              <td style="padding-top: 12px">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Join presentation
                                </a>
                              </td>
                            </tr>
                        {% else %}
                            <tr>
                              <td style="margin: 0; padding-top: 40px;">
                                <a href="{{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}"
                                    style="display: inline-block; background-color: #0870FF; color: white; border: 0; padding: 8px 16px; font-size: 14px; font-weight: 500; text-decoration: none"
                                >
                                  Get started
                                </a>
                              </td>
                            </tr>
                        {% endif %}
                      </table>
                    </div>
                </td>
              </tr>
            </table>
        ';
    }

    private function createInviteContentPlain(): string
    {
        return '
            {% if appointment.message %}
                {{ appointment.message|raw }}
            {% endif %}
            You are invited to a Digital Sales Room experience.
            {% if appointment.customFields.accessibleFrom is defined and appointment.customFields.accessibleTo is defined and appointment.customFields.accessibleFrom is not null and appointment.customFields.accessibleTo is not null %}
                {{ appointment.salesChannelDomain.salesChannel.translated.name }} invited you to a Digital Sales Room presentation that will take place
                    {% if appointment.customFields.isSameDay %}
                        on {{ appointment.customFields.accessibleFrom.date }} at {{ appointment.customFields.accessibleFrom.time }} - {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleFrom.timezone }}).
                      {% else %}
                        from {{ appointment.customFields.accessibleFrom.date }} {{ appointment.customFields.accessibleFrom.time }} to {{ appointment.customFields.accessibleTo.date }} {{ appointment.customFields.accessibleTo.time }} ({{ appointment.customFields.accessibleTo.timezone }}).
                    {% endif %}
            {% endif %}
            {% if appointment.mode === "' . AppointmentDefinition::MODE_GUIDED . '" and appointment.customFields.isGuide === false %}
                Are you going to participate in this presentation?
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=accepted&token={{ appointment.customFields.token }}"Yes
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=maybe&token={{ appointment.customFields.token }}"Maybe
                {{ appointment.salesChannelDomain.url }}/invitation/{{ appointment.id }}?answer=declined&token={{ appointment.customFields.token }}"No
                You can join the presentation here.
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Join presentation
            {% else %}
                {{ appointment.salesChannelDomain.url }}/presentation/{{ appointment.presentationPath }}Get started
            {% endif %}
        ';
    }

    private function createCancelContentHtml(): string
    {
        return '
            {% if appointment.customFields.cancelMessage %}
                <div class="message">
                    {{ appointment.customFields.cancelMessage|raw }}
                </div>
            {% endif %}
        ';
    }

    private function createCancelContentPlain(): string
    {
        return '
            {% if appointment.customFields.cancelMessage %}
                {{ appointment.customFields.cancelMessage|raw }}
            {% endif %}
        ';
    }

    /**
     * @return array<string, mixed>
     */
    private function createInviteMailTemplateData(): array
    {
        return [
            'appointment' => [
                'id' => 'XYZidXYZ',
                'name' => 'Summer 2023 Collection appointment',
                'mode' => AppointmentDefinition::MODE_GUIDED,
                'presentationPath' => 'XYZpathZYX',
                'message' => 'Hi folks,
                              I’m happy to invite you to our <b>Summer 2023 collection</b> presentation. I hope that a lot of you are able to attend!
                              See you then and best wishes
                              Jennifer',
                'salesChannelDomain' => [
                    'salesChannel' => [
                        'translated' => [
                            'name' => 'Demo shop'
                        ]
                    ],
                    'url' => \getenv('APP_URL'),
                ],
                'customFields' => [
                    'accessibleFrom' => [
                        'date' => "23 Jun 2023",
                        'time' => "12:00",
                        'timezone' => "UTC",
                    ],
                    'accessibleTo' => [
                        'date' => "24 Jun 2023",
                        'time' => "20:13",
                        'timezone' => "UTC",
                    ],
                    'isSameDay' => false,
                    'token' => 'XYZtokenZYX',
                    'isGuide' => false
                ]
            ]
        ];
    }

    /**
     * @return array<string, mixed>
     */
    private function createCancelMailTemplateData(): array
    {
        return [
            'appointment' => [
                'name' => 'Summer 2023 Collection appointment',
                'customFields' => [
                    'cancelMessage' => 'So sorry about this inconvenience.'
                ]
            ]
        ];
    }
}
