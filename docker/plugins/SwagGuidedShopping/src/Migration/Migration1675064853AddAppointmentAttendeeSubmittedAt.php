<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\Connection;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1675064853AddAppointmentAttendeeSubmittedAt extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1675064853;
    }

    public function update(Connection $connection): void
    {
        if (!MigrationStep::columnExists($connection, 'guided_shopping_appointment_attendee', 'attendee_submitted_at')) {
            $connection->executeStatement('
                ALTER TABLE `guided_shopping_appointment_attendee`
                ADD COLUMN `attendee_submitted_at` datetime(3) DEFAULT NULL AFTER `guide_cart_permissions_granted`;
            ');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }
}
