<?php declare(strict_types=1);

namespace SwagGuidedShopping\Migration;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Connection;
use Shopware\Core\Content\Cms\Aggregate\CmsPageTranslation\CmsPageTranslationDefinition;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\Migration\MigrationStep;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Migration\Traits\ImportTranslationsTrait;
use Shopware\Core\Migration\Traits\Translations;

class Migration1701771598ModifyDefaultLayoutsInShoppingExperience extends MigrationStep
{
    use ImportTranslationsTrait;

    public function getCreationTimestamp(): int
    {
        return 1701771598;
    }

    public function update(Connection $connection): void
    {
       $defaultProductListingPageId = Uuid::fromHexToBytes('33E88C7994FA4CF79A1265E5105B93B2');
       $defaultProductDetailPageId = Uuid::fromHexToBytes('BEA211B5099241719830DF8026624F7F');
       $defaultEndPresentationPageId = Uuid::fromHexToBytes('8EA80092FAA744559409F3E9F7ADCC6B');
       $defaultQuickviewPageId = Uuid::fromHexToBytes('182D3F7F988044ADBBA449B70C8BC472');

       $pageIds = [
           $defaultProductListingPageId,
           $defaultProductDetailPageId,
           $defaultEndPresentationPageId,
           $defaultQuickviewPageId,
       ];

       $sql = "
            UPDATE `cms_page` SET `locked` = 1
            WHERE id IN (:ids) AND version_id = :versionId
       "; // prevent modifying the default layouts

       $connection->executeStatement($sql, [
           'ids' => $pageIds,
           'versionId' => Uuid::fromHexToBytes(Defaults::LIVE_VERSION),
       ],
       [
           'ids' => ArrayParameterType::STRING,
       ]);

       $translations = [
            $defaultProductListingPageId => new Translations(
                [
                    'cms_page_id' => $defaultProductListingPageId,
                    'name' => 'Standard Digital Sales Rooms Produkt-Listing Seite',
                ],
                [
                    'cms_page_id' => $defaultProductListingPageId,
                    'name' => 'Default Digital Sales Rooms product listing page',
                ]
            ),
            $defaultProductDetailPageId => new Translations(
                [
                    'cms_page_id' => $defaultProductDetailPageId,
                    'name' => 'Standard Digital Sales Rooms Produkt-Detail Seite',
                ],
                [
                    'cms_page_id' => $defaultProductDetailPageId,
                    'name' => 'Default Digital Sales Rooms product detail page',
                ]
            ),
            $defaultEndPresentationPageId => new Translations(
                [
                    'cms_page_id' => $defaultEndPresentationPageId,
                    'name' => 'Standard Digital Sales Rooms Endpräsentations-Seite',
                ],
                [
                    'cms_page_id' => $defaultEndPresentationPageId,
                    'name' => 'Default Digital Sales Rooms end presentation page',
                ]
            ),
            $defaultQuickviewPageId => new Translations(
                [
                    'cms_page_id' => $defaultQuickviewPageId,
                    'name' => 'Standard Digital Sales Rooms Quickview-Seite',
                ],
                [
                    'cms_page_id' => $defaultQuickviewPageId,
                    'name' => 'Default Digital Sales Rooms Quickview page',
                ]
            ),
        ];

        $existingDefaultLayoutIds = $this->getAllDefaultLayoutIds($connection, $pageIds);
        
        foreach ($translations as $cmsPageId => $translation) {
            if (\in_array(Uuid::fromBytesToHex($cmsPageId), $existingDefaultLayoutIds)) {
                $this->importTranslation(CmsPageTranslationDefinition::ENTITY_NAME, $translation, $connection);
            }
        }
    }

    public function updateDestructive(Connection $connection): void
    {
        // implement update destructive
    }

    /**
     * @param array<string> $pageIds
     * @return array<int, int|string>
     */
    private function getAllDefaultLayoutIds(Connection $connection, array $pageIds): array
    {
        $sql = "
            SELECT LOWER(HEX(`id`)) AS `id` FROM `cms_page` 
            WHERE `id` IN (:ids)
        ";

        $result = $connection->fetchAllAssociativeIndexed(
            $sql,
            [
                'ids' => $pageIds,
                'versionId' => Uuid::fromHexToBytes(Defaults::LIVE_VERSION),
            ],
            [
                'ids' => ArrayParameterType::STRING,
            ]
        );

        return \array_keys($result);
    }
}
