<template>
  <div class="sw-gs-product-listing-pagination">
    <div v-if="getCurrentPage < getTotalPagesCount" class="load-more">
      <SfLoader :loading="isLoading" />
      <button class="sf-button--outline sw-gs-product-listing-pagination-load-more-button"
              ref="loadMoreButton"
              @click="loadMore">
        {{ $t("load more") }}...
      </button>
    </div>
  </div>
</template>

<script lang="ts">
  import {SfButton, SfLoader} from '@storefront-ui/vue'
  import {useListing, useSharedState, useVueContext} from '@shopware-pwa/composables'
  import {computed, ref, nextTick, inject, onMounted, onUnmounted, watch} from '@vue/composition-api'
  import {usePresentation} from '../logic/composables/usePresentation';
  import {ListingResult} from '@shopware-pwa/commons/interfaces/response/ListingResult';
  import {Product} from '@shopware-pwa/commons/interfaces/models/content/product/Product';
  import {useDynamicPage} from '../logic/composables/useDynamicPage';
  import {useAppointment} from '../logic/composables/useAppointment';
  import {useMercure} from '../logic/composables/useMercure';
  import {useMercureObserver} from '../logic/composables/useMercureObserver';
  import { useState } from '../logic/composables/useState';

  export default {
    name: 'SwGsProductListingPagination',
    components: {
      SfButton,
      SfLoader,
    },
    props: {
      /**
       * after how many pages infinity loading should stop automatically
       */
      pageLoadThreshold: {
        type: Number,
        default: Infinity,
      },
    },
    data() {
      return {
        pagesLoaded: 0,
      }
    },
    setup(props) {
      const listingType = props?.listingType || 'categoryListing';
      const COMPOSABLE_NAME = 'createListingComposable';
      const contextName = COMPOSABLE_NAME;

      const { openDynamicPage } = useDynamicPage();
      const { isGuide, isSelfService } = useAppointment();
      const { mercurePublish } = useMercure();
      const { currentLoadedInfo } = useMercureObserver();
      const { sharedRef } = useSharedState();
      const { stateForAll  } = useState();
      const { getElements, getCurrentPage, getTotalPagesCount } = useListing({ listingType: listingType });
      const {getSlideProducts, currentSlideIndex } = usePresentation();
      const loadMoreButton = ref(null);

      // get initial listing shared ref
      const {isVueComponent} = useVueContext();
      const cmsContext = isVueComponent && inject('swCmsContext', null);
      const cacheKey = cmsContext ? `${contextName}(cms-${cmsContext})` : contextName;

      const _storeInitialListing = sharedRef<Partial<ListingResult<Product>>>(
        `${cacheKey}-initialListing-${listingType}`
      );

      const _isLoading = ref<Boolean>(false);
      const _initiallyLoaded = ref<Boolean>(false);
      const _pagesLoaded = ref<number>(0);
      const controller = ref<AbortController>(new AbortController());

      let intersectionObserver: IntersectionObserver;

      const sleep = (ms) => {
        return new Promise(resolve => setTimeout(resolve, ms));
      };

      const changePage = async (pageNumber) => {
        if (pageNumber !== 1 && pageNumber > getTotalPagesCount.value) return;

        _isLoading.value = true;
        const newPage = await getSlideProducts(currentSlideIndex.value, {
          p: pageNumber,
          limit: 40
        }, controller.value);

        if (!newPage) {
          _isLoading.value = false;
          return false;
        }

        const oldElements = getElements.value;
        _storeInitialListing.value = newPage;

        if (pageNumber === 1) {
          _storeInitialListing.value.elements = newPage?.elements;
        } else {
          _storeInitialListing.value.elements = [...oldElements, ...newPage?.elements];
        }

        _storeInitialListing.value.page = pageNumber;

        if (!isSelfService.value && isGuide.value) {
          openDynamicPage({
            type: 'productList',
            opened: true,
            page: pageNumber,
          });
        }

        _pagesLoaded.value += 1;
        _initiallyLoaded.value = true;
        await sleep(500);
        _isLoading.value = false;
        if (pageNumber === 1) {
          destroyObserver();
          createIntersectionObserver();
        }
        return true;
      };

      const loadMore = async (initialFetch: Boolean = true) => {
        if (_isLoading.value) return;

        if (initialFetch) {
          let newPage = 0;
          if (typeof getCurrentPage.value === 'number') newPage = getCurrentPage.value + 1;
          if (typeof getCurrentPage.value === 'string') newPage = parseInt(getCurrentPage.value) + 1;
          if (isSelfService.value) {
            mercurePublish.local('dynamicProductListingPage.loadedMore', {
              type: 'productList',
              page: newPage
            });
          } else {
            mercurePublish.all('dynamicProductListingPage.loadedMore', {
              type: 'productList',
              page: newPage
            });
          }
        }
        return;
      };

      const onIntersect = (entries: IntersectionObserverEntry[]) => {
        if (_isLoading.value) return;
        if (entries && entries[0] && entries[0].isIntersecting) {
          if (props.pageLoadThreshold !== -1 && _pagesLoaded.value >= props.pageLoadThreshold) return;
          loadMore();
        }
      };

      const createIntersectionObserver = () => {
        nextTick(() => {
          const button = loadMoreButton.value;
          if (!button) return;

          intersectionObserver = new IntersectionObserver(onIntersect, {
            threshold: 1
          });
          intersectionObserver.observe(button);
        });
      };

      const destroyObserver = () => {
        if (intersectionObserver) intersectionObserver.disconnect();
      };
      
      onMounted(async () => {
        currentLoadedInfo.value = null;
        await changePage(getCurrentPage.value);
        if (!isGuide.value && stateForAll.value && !currentLoadedInfo.value && stateForAll.value.currentDynamicPage?.type === 'productList' && stateForAll.value.currentDynamicPage?.page) {
          currentLoadedInfo.value = {
            type: stateForAll.value.currentDynamicPage.type,
            page: stateForAll.value.currentDynamicPage.page
          }
        }
      });

      onUnmounted(() => {
        controller.value.abort();
        _storeInitialListing.value = null;
        destroyObserver();
      });

      watch(() => currentLoadedInfo.value, async (value) => {
        if (value && value.page) {
          const latestPage = value.page;
          const currentPage = _storeInitialListing.value.page;
          if (isGuide.value) {
            mercurePublish.guide('dynamicProductListingPage.loadedMore', {
              type: 'productList',
              page: latestPage
            });
          }
          if (currentPage < latestPage) {
            for (let i = currentPage + 1; i <= latestPage; i++) {
              await changePage(i);
            }
          }
        }
      });

      return {
        changePage,
        getTotalPagesCount,
        getElements,
        getCurrentPage,
        loadMore,
        isLoading: computed(() => _isLoading.value),
        loadMoreButton
      }
    },
  }
</script>

<style lang="scss" scoped>
  .sw-gs-product-listing-pagination {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 5px 0;
    &-load-more-button {
      opacity: 0;
    }
    .load-more {
      margin-top: 30px;
    }
    .sf-loader {
      height: 38px;
      width: 38px;
    }
  }
</style>
