import SwGsIconBars from './SwGsIconBars.vue';
import SwGsIconBookmarkOn from './SwGsIconBookmarkOn.vue';
import SwGsIconBookmarkOff from './SwGsIconBookmarkOff.vue';
import SwGsIconBroadcast from './SwGsIconBroadcast.vue';
import SwGsIconCameraOff from './SwGsIconCameraOff.vue';
import SwGsIconCameraOn from './SwGsIconCameraOn.vue';
import SwGsIconCart from './SwGsIconCart.vue';
import SwGsIconChevronLeft from './SwGsIconChevronLeft.vue';
import SwGsIconChevronRight from './SwGsIconChevronRight.vue';
import SwGsIconCircleLeft from './SwGsIconCircleLeft.vue';
import SwGsIconCircleRight from './SwGsIconCircleRight.vue';
import SwGsIconClient from './SwGsIconClient.vue';
import SwGsIconClients from './SwGsIconClients.vue';
import SwGsIconClose from './SwGsIconClose.vue';
import SwGsIconCog from './SwGsIconCog.vue';
import SwGsIconCompress from './SwGsIconCompress.vue';
import SwGsIconCrossselling from './SwGsIconCrossselling.vue';
import SwGsIconDotDotDot from './SwGsIconDotDotDot.vue';
import SwGsIconEdit from './SwGsIconEdit.vue';
import SwGsIconExpand from './SwGsIconExpand.vue';
import SwGsIconEye from './SwGsIconEye.vue';
import SwGsIconFilter from './SwGsIconFilter.vue';
import SwGsIconGetClients from './SwGsIconGetClients.vue';
import SwGsIconHighlight from './SwGsIconHighlight.vue';
import SwGsIconInfo from './SwGsIconInfo.vue';
import SwGsIconJumpTo from './SwGsIconJumpTo.vue';
import SwGsIconLastSeen from './SwGsIconLastSeen.vue';
import SwGsIconLayerFixed from './SwGsIconLayerFixed.vue';
import SwGsIconLayerStatic from './SwGsIconLayerStatic.vue';
import SwGsIconListing from './SwGsIconListing.vue';
import SwGsIconLongArrowLeft from './SwGsIconLongArrowLeft.vue';
import SwGsIconLongArrowRight from './SwGsIconLongArrowRight.vue';
import SwGsIconMicOff from './SwGsIconMicOff.vue';
import SwGsIconMicOn from './SwGsIconMicOn.vue';
import SwGsIconNotes from './SwGsIconNotes.vue';
import SwGsIconPhoneOff from './SwGsIconPhoneOff.vue';
import SwGsIconPhoneOn from './SwGsIconPhoneOn.vue';
import SwGsIconPieChart from './SwGsIconPieChart.vue';
import SwGsIconPlus from './SwGsIconPlus.vue';
import SwGsIconProductVariants from './SwGsIconProductVariants.vue';
import SwGsIconSave from './SwGsIconSave.vue';
import SwGsIconSoundOff from './SwGsIconSoundOff.vue';
import SwGsIconSoundOn from './SwGsIconSoundOn.vue';
import SwGsIconSoundSettings from './SwGsIconSoundSettings.vue';
import SwGsIconThumbsDown from './SwGsIconThumbsDown.vue';
import SwGsIconThumbsDownSmall from './SwGsIconThumbsDownSmall.vue';
import SwGsIconThumbsUp from './SwGsIconThumbsUp.vue';
import SwGsIconThumbsUpSmall from './SwGsIconThumbsUpSmall.vue';
import SwGsIconTrash from './SwGsIconTrash.vue';
import SwGsIconTrayUp from './SwGsIconTrayUp.vue';

export {
  SwGsIconBars,
  SwGsIconBookmarkOn,
  SwGsIconBookmarkOff,
  SwGsIconBroadcast,
  SwGsIconCameraOff,
  SwGsIconCameraOn,
  SwGsIconCart,
  SwGsIconChevronLeft,
  SwGsIconChevronRight,
  SwGsIconCircleLeft,
  SwGsIconCircleRight,
  SwGsIconClient,
  SwGsIconClients,
  SwGsIconClose,
  SwGsIconCog,
  SwGsIconCompress,
  SwGsIconCrossselling,
  SwGsIconDotDotDot,
  SwGsIconEdit,
  SwGsIconExpand,
  SwGsIconEye,
  SwGsIconFilter,
  SwGsIconGetClients,
  SwGsIconHighlight,
  SwGsIconInfo,
  SwGsIconJumpTo,
  SwGsIconLastSeen,
  SwGsIconLayerFixed,
  SwGsIconLayerStatic,
  SwGsIconListing,
  SwGsIconLongArrowLeft,
  SwGsIconLongArrowRight,
  SwGsIconMicOff,
  SwGsIconMicOn,
  SwGsIconNotes,
  SwGsIconPhoneOff,
  SwGsIconPhoneOn,
  SwGsIconPieChart,
  SwGsIconPlus,
  SwGsIconProductVariants,
  SwGsIconSave,
  SwGsIconSoundOff,
  SwGsIconSoundOn,
  SwGsIconSoundSettings,
  SwGsIconThumbsDown,
  SwGsIconThumbsDownSmall,
  SwGsIconThumbsUp,
  SwGsIconThumbsUpSmall,
  SwGsIconTrash,
  SwGsIconTrayUp
}
