import { ref } from "vue-demi";
import {
  CrossSelling,
  SearchFilterType
} from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useCrossSelling} from '../widgets/useCrossSelling';

import {useAdminApiInstance} from '../useAdminApiInstance';
jest.mock('../useAdminApiInstance');
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<typeof useAdminApiInstance>;

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

describe("Composables - useCrossSelling", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const getOrderLineItemsMock = jest.fn();
  const getIncludesConfigMock = jest.fn();
 
  const _topMatchesMock = ref<any>(null);
  const _crossSellCollectionMock = ref<CrossSelling[]>([]);
  const adminApiInstanceMock = ref(null);
  const productAssociationsMock = ref<CrossSelling[]>([]);

  const loadAssociationsMock = jest.fn();

  beforeEach(() => {
    jest
      .useFakeTimers()
      .setSystemTime(new Date('2023-01-01'));
    jest.resetAllMocks();
    _topMatchesMock.value = null;
    _crossSellCollectionMock.value = [];
    
    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("topMatches"))
            return _topMatchesMock;
          else if (contextName.includes("crossSellCollection"))
            return _crossSellCollectionMock;
        },
      } as any;
    });

    mockedComposables.useDefaults.mockImplementation(() => {
      return {
        getIncludesConfig: getIncludesConfigMock
      } as any;
    });

    mockedComposables.useProductAssociations.mockImplementation(() => {
      return {
        loadAssociations: loadAssociationsMock,
        productAssociations: productAssociationsMock
      } as any;
    });

    (mockedUseAdminApiInstance as any).mockImplementation(() => ({
      adminApiInstance: adminApiInstanceMock,
    } as any));

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedGSApiClient.OrderService = {
      getOrderLineItems: getOrderLineItemsMock,
    } as any;

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("loadCrossSellCollection", () => {
      it("should not call loadCrossSells when product invalid", async() => {
        // process
        const { loadCrossSellCollection } = useCrossSelling();
        await loadCrossSellCollection();

        // expect
        expect(loadAssociationsMock).not.toHaveBeenCalled();
      });

      it("should work correctly when product valid", async () => {
        // mock data
        const product = {
          id: '1'
        } as any;

        loadAssociationsMock.mockImplementationOnce(() => {
          productAssociationsMock.value = [
            {
              name: '1111',
              id: 1
            }
          ] as any
        });

        // process
        const { loadCrossSellCollection, crossSellCollection } = useCrossSelling(product);
        await loadCrossSellCollection();

        // expect
        expect(loadAssociationsMock).toHaveBeenCalledWith({
          method: "post",
          params: {
            associations: {
              seoUrls: {},
            },
          },
        });
        expect(crossSellCollection.value).toEqual([
          {
            name: '1111',
            id: 1
          }
        ])
      });
    });

    describe("loadCrossSellTopMatches", () => {
      it("should not call getOrderLineItems when flattenedCrossSellProducts empty", async() => {
        // mock data
        _crossSellCollectionMock.value = [];

        // process
        const { loadCrossSellTopMatches } = useCrossSelling();
        await loadCrossSellTopMatches();
 
        // expect
        expect(getOrderLineItemsMock).not.toHaveBeenCalled();
      });

      it("should work correctly when flattenedCrossSellProducts valid", async() => {
        // mock data
        _crossSellCollectionMock.value = [
          {
            name: '1111',
            id: 1,
            products: [
              { id: 1 }, { id: 2 }
            ]
          }
        ] as any;

        getOrderLineItemsMock.mockResolvedValueOnce([
          {
            id: 1,
            productId: 1
          },
          {
            id: 2,
            productId: 2
          },
          {
            id: 2,
            productId: 2
          },
        ]);

        // process
        const { loadCrossSellTopMatches, crossSellTopMatches, crossSellCount } = useCrossSelling();
        await loadCrossSellTopMatches();
       
        // expect
        expect(getOrderLineItemsMock).toHaveBeenCalledWith({
          includes: {
            order_line_item: ["type", "id", "productId"],
          },
          filter: [
            {
              type: SearchFilterType.MULTI,
              operator: "or",
              queries: [
                {
                  type: SearchFilterType.EQUALS_ANY,
                  field: "product.parentId",
                  value: [1, 2],
                },
                {
                  type: SearchFilterType.EQUALS_ANY,
                  field: "product.id",
                  value: [1, 2],
                },
              ],
            },
            {
              type: SearchFilterType.RANGE,
              field: "order.orderDate",
              parameters: {
                gte: '2022-01-01',
                lte: '2023-01-01',
              },
            },
            {
              type: SearchFilterType.EQUALS,
              field: "order.stateMachineState.technicalName",
              value: "completed",
            },
          ],
          aggregations: [
            {
              name: "sum",
              type: "sum",
              field: "quantity",
            },
          ],
          limit: 10,
        }, adminApiInstanceMock);

        expect(crossSellTopMatches.value).toEqual([
          {
            id: 2
          },
          {
            id: 1
          }
        ]);

        expect(crossSellCount.value).toEqual(2);
      });

      it("should handle error when api failed", async() => {
        // mock data
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        _crossSellCollectionMock.value = [
          {
            name: '1111',
            id: 1,
            products: [
              { id: 1 }, { id: 2 }
            ]
          }
        ] as any;

        getOrderLineItemsMock.mockRejectedValueOnce(res);

        // process
        const { loadCrossSellTopMatches, crossSellTopMatches } = useCrossSelling();
        await loadCrossSellTopMatches();
       
        // expect
        expect(getOrderLineItemsMock).toHaveBeenCalledWith({
          includes: {
            order_line_item: ["type", "id", "productId"],
          },
          filter: [
            {
              type: SearchFilterType.MULTI,
              operator: "or",
              queries: [
                {
                  type: SearchFilterType.EQUALS_ANY,
                  field: "product.parentId",
                  value: [1, 2],
                },
                {
                  type: SearchFilterType.EQUALS_ANY,
                  field: "product.id",
                  value: [1, 2],
                },
              ],
            },
            {
              type: SearchFilterType.RANGE,
              field: "order.orderDate",
              parameters: {
                gte: '2022-01-01',
                lte: '2023-01-01',
              },
            },
            {
              type: SearchFilterType.EQUALS,
              field: "order.stateMachineState.technicalName",
              value: "completed",
            },
          ],
          aggregations: [
            {
              name: "sum",
              type: "sum",
              field: "quantity",
            },
          ],
          limit: 10,
        }, adminApiInstanceMock);

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useCrossSelling",
            method: "loadCrossSellTopMatches"
          }
        );

        expect(crossSellTopMatches.value).toEqual(null);
      });
    });
  });
});
