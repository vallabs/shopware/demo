import {
  getOrderLineItems,
  getOrderCustomer,
} from "../orderService";

describe("Services - orderService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
      },
    };
  });

  describe("getOrderLineItems", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [
              {
                orderId: 'dummyOrderId'
              }
            ]
          }
        });
      });
      const res = await getOrderLineItems({}, apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/search/order-line-item`, {});
      expect(res).toEqual([
        {
          orderId: 'dummyOrderId'
        }
      ])
    })
  });

  describe("getOrderCustomer", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [
              {
                orderId: 'dummyOrderId'
              }
            ]
          }
        });
      });
      const res = await getOrderCustomer({}, apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/search/order-customer`, {});
      expect(res).toEqual([
        {
          orderId: 'dummyOrderId'
        }
      ])
    })
  });

});
