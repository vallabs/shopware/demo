import { ref } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { useTabsUIState } from "../useTabsUIState";

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

describe("Composables - useTabsUIState", () => {
  const handleErrorMock = jest.fn();
  const rootContextMock = prepareRootContextMock();
  const _activeTabMock = ref<number>(0);
  const stateNameMock = ref<string>("test");

  beforeEach(() => {
    jest.resetAllMocks();
    _activeTabMock.value = 0;
    stateNameMock.value = "test";

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes(stateNameMock.value))
            return _activeTabMock;
        }
      } as any;
    });

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("switchState", () => {
      it("should return correctly params value of switchState", () => {
        const { switchState } = useTabsUIState({ stateName: "test" });
        switchState(1);
        expect(_activeTabMock.value).toEqual(1);
      });

      it("should retur correctly activeTab value when _activeTab valid and params undefined", () => {
        _activeTabMock.value = 2;
        const { switchState } = useTabsUIState({ stateName: "test" });
        switchState(undefined);
        expect(_activeTabMock.value).toEqual(2);
      });

      it("should return correctly activeTab value when _activeTab valid and stateName invalid", () => {
        _activeTabMock.value = 2;
        const { switchState } = useTabsUIState({});
        switchState(1);
        expect(_activeTabMock.value).toEqual(2);
      });
    });
  });

  describe("computed - activeTab", () => {
    it("should return activeTab", () => {
      const { activeTab } = useTabsUIState({ stateName: "test" });
      expect(activeTab.value).toEqual(0);
    });
  });
});
