import {
  getPage,
  getSlideData,
  getSlideProducts,
  getStateForGuides,
  getStateForClients,
  getCmsPageById
} from "../presentationService";

describe("Services - presentationService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
      },
    };
  });

  describe("getPage", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            cmsPageResults: {},
            navigation: []
          }
        });
      });
      const res = await getPage(apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/presentation`);
      expect(res).toEqual({
        cmsPageResults: {},
        navigation: []
      })
    })
  });

  describe("getSlideData", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {}
        });
      });
      const res = await getSlideData('dummyPageId', 'dummySlideId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/presentation/dummyPageId/slide/dummySlideId`);
      expect(res).toEqual({});
    })
  });

  describe("getSlideProducts", () => {
    it('should work correctly with correct response without any controller', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {}
        });
      });
      const res = await getSlideProducts('dummyPageId', 'dummySlideId', {}, apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/presentation/dummyPageId/slide/dummySlideId/products`, {}, {});
      expect(res).toEqual({});
    });

    it('should work correctly with correct response with controller', async () => {
      const control = new AbortController();
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {}
        });
      });
      const res = await getSlideProducts('dummyPageId', 'dummySlideId', {}, apiInstanceMock, control);
      expect(postMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/presentation/dummyPageId/slide/dummySlideId/products`, {}, {signal: control.signal});
      expect(res).toEqual({});
    })
  });

  describe("getStateForGuides", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {}
        });
      });
      const res = await getStateForGuides('dummyAppointmentId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/dummyAppointmentId/presentation/state`);
      expect(res).toEqual({});
    })
  });

  describe("getStateForClients", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {}
        });
      });
      const res = await getStateForClients(apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/presentation/state`);
      expect(res).toEqual({});
    })
  });

  describe("getCmsPageById", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            id: 'dummyPageId'
          }
        });
      });
      const res = await getCmsPageById('dummyPageId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/store-api/cms/dummyPageId`);
      expect(res).toEqual({
        id: 'dummyPageId'
      });
    })
  });

});
