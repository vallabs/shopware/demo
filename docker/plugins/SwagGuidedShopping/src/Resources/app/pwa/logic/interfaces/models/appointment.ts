export interface AppointmentModel {
  id: string,
  JWTMercurePublisherToken: string,
  JWTMercureSubscriberToken: string,
  attendeeId: string,
  mercureHubPublicUrl: string,
  mercurePublisherTopic: string,
  mercureSubscriberTopics: [string],
  presentationGuideMode: string,
  salesChannelId: string,
  newContextToken: string,
  isPreview: boolean,
}

export interface AttendeeData {
  attendeeName?: string,
  videoUserId?: string,
  guideCartPermissionsGranted?: boolean
}
