import { ref } from "vue-demi";
import { Product } from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import { useAdminApiInstance } from "../useAdminApiInstance";
jest.mock("../useAdminApiInstance");
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<
  typeof useAdminApiInstance
>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

import { useProductInsights } from "../widgets/useProductInsights";

describe("Composables - useProductInsights", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const getOrderLineItemsMock = jest.fn();

  const _storeProductOrderItemsMock = ref<any>({});
  const _storeSortedGroupPropertiesMock = ref<any>({});
  const _storeFilteredGroupPropertiesMock = ref<any>({});
  const _storeFilteredPropertiesMock = ref<any>([]);
  const adminApiInstanceMock = ref({});

  beforeEach(() => {
    jest.resetAllMocks();

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("productOrdersCount"))
            return _storeProductOrderItemsMock;
          if (contextName.includes("productSortedGroupProperties"))
            return _storeSortedGroupPropertiesMock;
          if (contextName.includes("productFilteredGroupProperties"))
            return _storeFilteredGroupPropertiesMock;
          if (contextName.includes("productFilteredProperties"))
            return _storeFilteredPropertiesMock;
        },
      } as any;
    });

    (mockedUseAdminApiInstance as any).mockImplementation(
      () =>
        ({
          adminApiInstance: adminApiInstanceMock,
        } as any)
    );

    mockedGSApiClient.OrderService = {
      getOrderLineItems: getOrderLineItemsMock,
    } as any;

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    _storeProductOrderItemsMock.value = {};
    _storeSortedGroupPropertiesMock.value = {};
    _storeFilteredGroupPropertiesMock.value = {};
    _storeFilteredPropertiesMock.value = [];
  
    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("loadProductOrdersCount", () => {
      it("should be executed correctly", async () => {
        const { productOrdersCount, loadProductOrdersCount } = useProductInsights({
          id: 'productId'
        } as Product);
        getOrderLineItemsMock.mockResolvedValue(
          [
            {
              apiAlias: 'apiAlias1',
              id: 'id1',
              productId: 'productId1',
              type: 'type1'
            },
            {
              apiAlias: 'apiAlias2',
              id: 'id2',
              productId: 'productId2',
              type: 'type2'
            }
          ]
        );
        await loadProductOrdersCount();

        expect(productOrdersCount.value).toEqual(2);
      });

      it("should handle error correctly", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getOrderLineItemsMock.mockRejectedValueOnce(res);
        const { loadProductOrdersCount } = useProductInsights({
          id: 'productId'
        } as Product);
        await loadProductOrdersCount();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useProductInsights",
            method: "loadProductOrdersCount"
          }
        );
      });
    });

    describe("getSortedGroupProperties", () => {
      it("should be executed correctly", async () => {
        const { sortedGroupProperties, getSortedGroupProperties } = useProductInsights({
          id: 'productId',
          properties: [
            {
                "groupId": "c19105a35554404984e91610a4d7a517",
                "name": "black",
                "position": 1,
                "colorHexCode": "#000000",
                "mediaId": null,
                "group": {
                    "name": "Color",
                    "displayType": "color",
                    "sortingType": "alphanumeric",
                    "description": null,
                    "position": 1,
                    "filterable": true,
                    "visibleOnProductDetailPage": true,
                    "options": null,
                    "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
                    "versionId": null,
                    "translated": {
                        "name": "Color",
                        "description": null,
                        "position": 1,
                        "customFields": {}
                    },
                    "createdAt": "2020-11-02T09:22:49.405+00:00",
                    "updatedAt": null,
                    "extensions": {
                        "foreignKeys": {
                            "apiAlias": "array_struct"
                        },
                        "internal_mapping_storage": {
                            "apiAlias": "array_struct"
                        }
                    },
                    "id": "c19105a35554404984e91610a4d7a517",
                    "customFields": null,
                    "apiAlias": "property_group"
                },
                "media": null,
                "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                "versionId": null,
                "translated": {
                    "name": "black",
                    "position": 1,
                    "customFields": {}
                },
                "createdAt": "2020-11-02T09:23:15.908+00:00",
                "updatedAt": null,
                "extensions": {
                    "foreignKeys": {
                        "apiAlias": "array_struct"
                    }
                },
                "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                "customFields": null,
                "apiAlias": "property_group_option"
            },
            {
                "groupId": "f9e765cbcd224fe4a8c1c6f73dd36bba",
                "name": "42",
                "position": 1,
                "colorHexCode": null,
                "mediaId": null,
                "group": {
                    "name": "Size",
                    "displayType": "text",
                    "sortingType": "alphanumeric",
                    "description": null,
                    "position": 1,
                    "filterable": true,
                    "visibleOnProductDetailPage": true,
                    "options": null,
                    "_uniqueIdentifier": "f9e765cbcd224fe4a8c1c6f73dd36bba",
                    "versionId": null,
                    "translated": {
                        "name": "Size",
                        "description": null,
                        "position": 1,
                        "customFields": {}
                    },
                    "createdAt": "2020-09-16T06:19:12.437+00:00",
                    "updatedAt": null,
                    "extensions": {
                        "foreignKeys": {
                            "apiAlias": "array_struct"
                        },
                        "internal_mapping_storage": {
                            "apiAlias": "array_struct"
                        }
                    },
                    "id": "f9e765cbcd224fe4a8c1c6f73dd36bba",
                    "customFields": null,
                    "apiAlias": "property_group"
                },
                "media": null,
                "_uniqueIdentifier": "4eb64a13b81b44449c1371f28f3bc305",
                "versionId": null,
                "translated": {
                    "name": "42",
                    "position": 1,
                    "customFields": {}
                },
                "createdAt": "2022-05-10T08:51:11.451+00:00",
                "updatedAt": null,
                "extensions": {
                    "foreignKeys": {
                        "apiAlias": "array_struct"
                    }
                },
                "id": "4eb64a13b81b44449c1371f28f3bc305",
                "customFields": null,
                "apiAlias": "property_group_option"
            }
          ]
        } as any);
       
        await getSortedGroupProperties();

        expect(sortedGroupProperties.value).toEqual({
          "Color": {
              "14f6273f49c9400a8fcfbd9e89cc2c3f": {
                  "groupId": "c19105a35554404984e91610a4d7a517",
                  "name": "black",
                  "position": 1,
                  "colorHexCode": "#000000",
                  "mediaId": null,
                  "group": {
                      "name": "Color",
                      "displayType": "color",
                      "sortingType": "alphanumeric",
                      "description": null,
                      "position": 1,
                      "filterable": true,
                      "visibleOnProductDetailPage": true,
                      "options": null,
                      "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
                      "versionId": null,
                      "translated": {
                          "name": "Color",
                          "description": null,
                          "position": 1,
                          "customFields": {}
                      },
                      "createdAt": "2020-11-02T09:22:49.405+00:00",
                      "updatedAt": null,
                      "extensions": {
                          "foreignKeys": {
                              "apiAlias": "array_struct"
                          },
                          "internal_mapping_storage": {
                              "apiAlias": "array_struct"
                          }
                      },
                      "id": "c19105a35554404984e91610a4d7a517",
                      "customFields": null,
                      "apiAlias": "property_group"
                  },
                  "media": null,
                  "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                  "versionId": null,
                  "translated": {
                      "name": "black",
                      "position": 1,
                      "customFields": {}
                  },
                  "createdAt": "2020-11-02T09:23:15.908+00:00",
                  "updatedAt": null,
                  "extensions": {
                      "foreignKeys": {
                          "apiAlias": "array_struct"
                      }
                  },
                  "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                  "customFields": null,
                  "apiAlias": "property_group_option"
              }
          },
          "Size": {
              "4eb64a13b81b44449c1371f28f3bc305": {
                  "groupId": "f9e765cbcd224fe4a8c1c6f73dd36bba",
                  "name": "42",
                  "position": 1,
                  "colorHexCode": null,
                  "mediaId": null,
                  "group": {
                      "name": "Size",
                      "displayType": "text",
                      "sortingType": "alphanumeric",
                      "description": null,
                      "position": 1,
                      "filterable": true,
                      "visibleOnProductDetailPage": true,
                      "options": null,
                      "_uniqueIdentifier": "f9e765cbcd224fe4a8c1c6f73dd36bba",
                      "versionId": null,
                      "translated": {
                          "name": "Size",
                          "description": null,
                          "position": 1,
                          "customFields": {}
                      },
                      "createdAt": "2020-09-16T06:19:12.437+00:00",
                      "updatedAt": null,
                      "extensions": {
                          "foreignKeys": {
                              "apiAlias": "array_struct"
                          },
                          "internal_mapping_storage": {
                              "apiAlias": "array_struct"
                          }
                      },
                      "id": "f9e765cbcd224fe4a8c1c6f73dd36bba",
                      "customFields": null,
                      "apiAlias": "property_group"
                  },
                  "media": null,
                  "_uniqueIdentifier": "4eb64a13b81b44449c1371f28f3bc305",
                  "versionId": null,
                  "translated": {
                      "name": "42",
                      "position": 1,
                      "customFields": {}
                  },
                  "createdAt": "2022-05-10T08:51:11.451+00:00",
                  "updatedAt": null,
                  "extensions": {
                      "foreignKeys": {
                          "apiAlias": "array_struct"
                      }
                  },
                  "id": "4eb64a13b81b44449c1371f28f3bc305",
                  "customFields": null,
                  "apiAlias": "property_group_option"
              }
          }
        });
      });

      it("should handle error correctly", async () => {
        const { getSortedGroupProperties } = useProductInsights({
          id: 'productId',
          properties: [
            {
              "group": {},
            },
          ]
        } as any);
       
        await getSortedGroupProperties();
        expect(handleErrorMock).toHaveBeenCalledWith(
          expect.anything(),
          {
            context: "useProductInsights",
            method: "getSortedGroupProperties"
          }
        );
      });
    });

    describe("togglePropertyFilter", () => {
      it("should be executed correctly when property is not filtered", async () => {
        const property1 = {
          "groupId": "c19105a35554404984e91610a4d7a517",
          "name": "black",
          "position": 1,
          "colorHexCode": "#000000",
          "mediaId": null,
          "group": {
              "name": "Color",
              "displayType": "color",
              "sortingType": "alphanumeric",
              "description": null,
              "position": 1,
              "filterable": true,
              "visibleOnProductDetailPage": true,
              "options": null,
              "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
              "versionId": null,
              "translated": {
                  "name": "Color",
                  "description": null,
                  "position": 1,
                  "customFields": {}
              },
              "createdAt": "2020-11-02T09:22:49.405+00:00",
              "updatedAt": null,
              "extensions": {
                  "foreignKeys": {
                      "apiAlias": "array_struct"
                  },
                  "internal_mapping_storage": {
                      "apiAlias": "array_struct"
                  }
              },
              "id": "c19105a35554404984e91610a4d7a517",
              "customFields": null,
              "apiAlias": "property_group"
          },
          "media": null,
          "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
          "versionId": null,
          "translated": {
              "name": "black",
              "position": 1,
              "customFields": {}
          },
          "createdAt": "2020-11-02T09:23:15.908+00:00",
          "updatedAt": null,
          "extensions": {
              "foreignKeys": {
                  "apiAlias": "array_struct"
              }
          },
          "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
          "customFields": null,
          "apiAlias": "property_group_option"
      };
        const { filteredProperties, sortedGroupProperties, getSortedGroupProperties, togglePropertyFilter } = useProductInsights({
          id: 'productId',
          properties: [
            property1
          ]
        } as any);
       
        await getSortedGroupProperties();

        togglePropertyFilter(property1);
        expect(filteredProperties.value[0]).toEqual({
          ...property1,
          isFilter: true
        });

        expect(sortedGroupProperties.value).toEqual({
          "Color": {
              "14f6273f49c9400a8fcfbd9e89cc2c3f": {
                  "groupId": "c19105a35554404984e91610a4d7a517",
                  "name": "black",
                  "position": 1,
                  "colorHexCode": "#000000",
                  "mediaId": null,
                  "group": {
                      "name": "Color",
                      "displayType": "color",
                      "sortingType": "alphanumeric",
                      "description": null,
                      "position": 1,
                      "filterable": true,
                      "visibleOnProductDetailPage": true,
                      "options": null,
                      "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
                      "versionId": null,
                      "translated": {
                          "name": "Color",
                          "description": null,
                          "position": 1,
                          "customFields": {}
                      },
                      "createdAt": "2020-11-02T09:22:49.405+00:00",
                      "updatedAt": null,
                      "extensions": {
                          "foreignKeys": {
                              "apiAlias": "array_struct"
                          },
                          "internal_mapping_storage": {
                              "apiAlias": "array_struct"
                          }
                      },
                      "id": "c19105a35554404984e91610a4d7a517",
                      "customFields": null,
                      "apiAlias": "property_group"
                  },
                  "media": null,
                  "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                  "versionId": null,
                  "translated": {
                      "name": "black",
                      "position": 1,
                      "customFields": {}
                  },
                  "createdAt": "2020-11-02T09:23:15.908+00:00",
                  "updatedAt": null,
                  "extensions": {
                      "foreignKeys": {
                          "apiAlias": "array_struct"
                      }
                  },
                  "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                  "customFields": null,
                  "apiAlias": "property_group_option",
                  "isActive": true,
                  "isFilter": true,
              }
          },
        });
      });

      it("should be executed correctly when property is filtered", async () => {
        const property1 = {
          "groupId": "c19105a35554404984e91610a4d7a517",
          "name": "black",
          "position": 1,
          "colorHexCode": "#000000",
          "mediaId": null,
          "group": {
              "name": "Color",
              "displayType": "color",
              "sortingType": "alphanumeric",
              "description": null,
              "position": 1,
              "filterable": true,
              "visibleOnProductDetailPage": true,
              "options": null,
              "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
              "versionId": null,
              "translated": {
                  "name": "Color",
                  "description": null,
                  "position": 1,
                  "customFields": {}
              },
              "createdAt": "2020-11-02T09:22:49.405+00:00",
              "updatedAt": null,
              "extensions": {
                  "foreignKeys": {
                      "apiAlias": "array_struct"
                  },
                  "internal_mapping_storage": {
                      "apiAlias": "array_struct"
                  }
              },
              "id": "c19105a35554404984e91610a4d7a517",
              "customFields": null,
              "apiAlias": "property_group"
          },
          "media": null,
          "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
          "versionId": null,
          "translated": {
              "name": "black",
              "position": 1,
              "customFields": {}
          },
          "createdAt": "2020-11-02T09:23:15.908+00:00",
          "updatedAt": null,
          "extensions": {
              "foreignKeys": {
                  "apiAlias": "array_struct"
              }
          },
          "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
          "customFields": null,
          "apiAlias": "property_group_option"
        };
        const { filteredProperties, sortedGroupProperties, getSortedGroupProperties, togglePropertyFilter } = useProductInsights({
          id: 'productId',
          properties: [
            property1
          ]
        } as any);
        
        await getSortedGroupProperties();
        // enable filter
        togglePropertyFilter(property1);

        // remove filter
        togglePropertyFilter(property1);

        expect(filteredProperties.value.find(x => x.id === property1.id)).toBeUndefined();
        expect(sortedGroupProperties.value).toEqual({
          "Color": {
              "14f6273f49c9400a8fcfbd9e89cc2c3f": {
                  "groupId": "c19105a35554404984e91610a4d7a517",
                  "name": "black",
                  "position": 1,
                  "colorHexCode": "#000000",
                  "mediaId": null,
                  "group": {
                      "name": "Color",
                      "displayType": "color",
                      "sortingType": "alphanumeric",
                      "description": null,
                      "position": 1,
                      "filterable": true,
                      "visibleOnProductDetailPage": true,
                      "options": null,
                      "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
                      "versionId": null,
                      "translated": {
                          "name": "Color",
                          "description": null,
                          "position": 1,
                          "customFields": {}
                      },
                      "createdAt": "2020-11-02T09:22:49.405+00:00",
                      "updatedAt": null,
                      "extensions": {
                          "foreignKeys": {
                              "apiAlias": "array_struct"
                          },
                          "internal_mapping_storage": {
                              "apiAlias": "array_struct"
                          }
                      },
                      "id": "c19105a35554404984e91610a4d7a517",
                      "customFields": null,
                      "apiAlias": "property_group"
                  },
                  "media": null,
                  "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                  "versionId": null,
                  "translated": {
                      "name": "black",
                      "position": 1,
                      "customFields": {}
                  },
                  "createdAt": "2020-11-02T09:23:15.908+00:00",
                  "updatedAt": null,
                  "extensions": {
                      "foreignKeys": {
                          "apiAlias": "array_struct"
                      }
                  },
                  "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
                  "customFields": null,
                  "apiAlias": "property_group_option",
                  "isActive": false,
                  "isFilter": false,
              }
          },
        });
      });
    });

    describe("resetFilteredProperties", () => {
      it("should be executed correctly", async () => {
        const property1 = {
          "groupId": "c19105a35554404984e91610a4d7a517",
          "name": "black",
          "position": 1,
          "colorHexCode": "#000000",
          "mediaId": null,
          "group": {
              "name": "Color",
              "displayType": "color",
              "sortingType": "alphanumeric",
              "description": null,
              "position": 1,
              "filterable": true,
              "visibleOnProductDetailPage": true,
              "options": null,
              "_uniqueIdentifier": "c19105a35554404984e91610a4d7a517",
              "versionId": null,
              "translated": {
                  "name": "Color",
                  "description": null,
                  "position": 1,
                  "customFields": {}
              },
              "createdAt": "2020-11-02T09:22:49.405+00:00",
              "updatedAt": null,
              "extensions": {
                  "foreignKeys": {
                      "apiAlias": "array_struct"
                  },
                  "internal_mapping_storage": {
                      "apiAlias": "array_struct"
                  }
              },
              "id": "c19105a35554404984e91610a4d7a517",
              "customFields": null,
              "apiAlias": "property_group"
          },
          "media": null,
          "_uniqueIdentifier": "14f6273f49c9400a8fcfbd9e89cc2c3f",
          "versionId": null,
          "translated": {
              "name": "black",
              "position": 1,
              "customFields": {}
          },
          "createdAt": "2020-11-02T09:23:15.908+00:00",
          "updatedAt": null,
          "extensions": {
              "foreignKeys": {
                  "apiAlias": "array_struct"
              }
          },
          "id": "14f6273f49c9400a8fcfbd9e89cc2c3f",
          "customFields": null,
          "apiAlias": "property_group_option"
        };
        const { filteredProperties, togglePropertyFilter, sortedGroupProperties, filteredGroupProperties, resetFilteredProperties, getSortedGroupProperties } = useProductInsights({
          id: 'productId',
          properties: [
            property1
          ]
        } as any);
        
        await getSortedGroupProperties();
        togglePropertyFilter(property1);

        resetFilteredProperties();
        expect(filteredProperties.value).toEqual([]);
        expect(filteredGroupProperties.value).toEqual([]);
        expect(sortedGroupProperties.value).toEqual({});
      });
    });

  });
});
