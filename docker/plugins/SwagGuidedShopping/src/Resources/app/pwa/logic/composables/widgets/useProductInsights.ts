import { useSharedState } from "@shopware-pwa/composables";
import { computed, ref } from "@vue/composition-api";
import { OrderService } from "../../api-client";
import { useAdminApiInstance } from "../useAdminApiInstance";
import { useErrorHandler } from "../useErrorHandler";
import {
  Product,
  ShopwareSearchParams,
  SearchFilterType,
} from "@shopware-pwa/commons";
import { ClientApiError, OrderLineItem } from "../../interfaces";
import dayjs from "dayjs";

const COMPOSABLE_NAME = "useProductInsights";

export const useProductInsights = (product?: Product) => {
  const contextName = COMPOSABLE_NAME;
  const productId = ref<string>(product?.id);
  const { adminApiInstance } = useAdminApiInstance();
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const _storeProductOrderItems = sharedRef<OrderLineItem[]>(
    `${contextName}-productOrdersCount`,
    null
  );
  const _storeSortedGroupProperties = sharedRef<any>(
    `${contextName}-productSortedGroupProperties`,
    {}
  );
  const _storeFilteredGroupProperties = sharedRef<any>(
    `${contextName}-productFilteredGroupProperties`,
    {}
  );
  const _storeFilteredProperties = sharedRef<any>(
    `${contextName}-productFilteredProperties`,
    []
  );

  const DATE_FORMAT: string = "YYYY-MM-DD";
  const currentDate = dayjs().format(DATE_FORMAT);
  const pastDate = dayjs().subtract(1, "year").format(DATE_FORMAT);

  const orderCountCriteria: ShopwareSearchParams = {
    includes: {
      order_line_item: ["type"],
    },
    filter: [
      {
        type: SearchFilterType.MULTI,
        operator: "or",
        queries: [
          {
            type: SearchFilterType.EQUALS,
            field: "product.parentId",
            value: productId.value,
          },
          {
            type: SearchFilterType.EQUALS,
            field: "product.id",
            value: productId.value,
          },
        ],
      },
      {
        type: SearchFilterType.RANGE,
        field: "order.orderDate",
        parameters: {
          gte: pastDate, // yyyy-mm-dd
          lte: currentDate, // yyyy-mm-dd
        },
      },
      {
        type: SearchFilterType.EQUALS,
        field: "order.stateMachineState.technicalName",
        value: "completed",
      },
    ],
    aggregations: [
      {
        name: "sum",
        type: "sum",
        field: "quantity",
      },
    ],
    // total_count_mode: 0,
  };

  function resetFilteredProperties() {
    _storeFilteredProperties.value.forEach((property) => {
      if (property.isFilter) {
        togglePropertyFilter(property);
      }
    });

    _storeFilteredProperties.value = [];
    _storeFilteredGroupProperties.value = [];
    _storeSortedGroupProperties.value = {};
  }

  async function loadProductOrdersCount() {
    try {
      _storeProductOrderItems.value = await OrderService.getOrderLineItems(
        orderCountCriteria,
        adminApiInstance
      );
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadProductOrdersCount",
      });
    }
  }

  async function getSortedGroupProperties() {
    try {
      const properties = product?.properties;
      _storeSortedGroupProperties.value = sortProperties(properties);
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "getSortedGroupProperties",
      });
    }
  }

  function sortProperties(properties) {
    let sortedProperties = {};

    if (properties.length > 0) {
      // create object with group names
      properties.forEach((property) => {
        sortedProperties[property.group.translated.name] = {};
      });

      // sort properties in group objects
      properties.forEach((property) => {
        if (property.group.translated.name in sortedProperties) {
          sortedProperties[property.group.translated.name][property.id] =
            property;
        }
      });
    }

    return sortedProperties;
  }

  function togglePropertyFilter(property) {
    if (!property.isFilter) {
      addFilter(property);
    } else {
      removeFilter(property);
    }

    updateFilter(property);
    _storeFilteredGroupProperties.value = sortProperties(
      _storeFilteredProperties.value
    );
  }

  function addFilter(property) {
    property.isFilter = true;
    _storeFilteredProperties.value.push(property);
  }

  function removeFilter(property) {
    property.isFilter = false;
    _storeFilteredProperties.value = _storeFilteredProperties.value.filter(
      (filterProperty) => filterProperty.id != property.id
    );
  }

  function updateFilter(property) {
    const activeProperties = Object.assign(
      {},
      _storeSortedGroupProperties.value
    );
    activeProperties[property.group.translated.name][property.id].isActive =
      property.isFilter;
    _storeSortedGroupProperties.value = activeProperties;
  }

  return {
    loadProductOrdersCount,
    getSortedGroupProperties,
    togglePropertyFilter,
    resetFilteredProperties,
    productOrdersCount: computed(() => _storeProductOrderItems.value?.length),
    sortedGroupProperties: computed(() => _storeSortedGroupProperties.value),
    filteredGroupProperties: computed(
      () => _storeFilteredGroupProperties.value
    ),
    filteredProperties: computed(() => _storeFilteredProperties.value),
  };
};
