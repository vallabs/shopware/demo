import { ref } from '@vue/composition-api';
import { useMercure } from './useMercure';
import { useMercureObserver } from './useMercureObserver';
import VueScrollTo from 'vue-scrollto';
import {useHoverElement} from "./useHoverElement";
import {useAppointment} from "./useAppointment";

const HIGHLIGHT_DURATION: number = 5000;
const SCROLL_DURATION: number = 500;
const SCROLL_OPTIONS: object = {
  container: '.sw-gs-layout',
  offset: -200,
};

type SlotContext = {
  type: string,
}

export const useScrollToElement = (elementId: string, slotContextOptions: SlotContext = null, defaultScrollOptions = {}) => {
  const scrollOptions = ref({
    ...SCROLL_OPTIONS,
    ...defaultScrollOptions
  });
  const { mercurePublish } = useMercure();
  const { lastClientHoveredElement, lastGuideHoveredElement } = useMercureObserver();
  const { publishHover } = useHoverElement(elementId);
  const scrollToIsLoading = ref<boolean>(false);
  const slotContext = ref<SlotContext>(slotContextOptions);
  const { appointment } = useAppointment();

  function setSlotContext(value: SlotContext) {
    slotContext.value = value;
  }

  /**
   * forces client to scroll to guide position
   *
   * @returns {Promise<void>}
   */
  async function publishScrollTo() {
    if (!handleCmsElements()) {
      mercurePublish.guide('scrolledTo', {
        elementId: elementId,
        index: null,
        attendeeId: appointment.value?.attendeeId,
        slotContext: slotContext.value
      });
    }

    // always show loader for a short amount of time to signal the guide something is happening on the client side
    scrollToIsLoading.value = true;
    setTimeout(() => {
      scrollToIsLoading.value = false;
    }, 1000);
    return;
  }

  function handleCmsElements(): boolean {
    const element = tryGetElement();
    if (!element) return false;
    const index = getTargetIndex(element);
    if (index === null) return false;
    mercurePublish.guide('scrolledTo', {
      elementId: elementId,
      index,
      attendeeId: appointment.value?.attendeeId,
      slotContext: slotContext.value
    });

    return true;
  }

  // tabs: product-description-reviews and cross-selling
  // image-gallery
  // product-slider

  function getTargetIndex(element: Element): number {
    let target = null;

    if (slotContext.value.type === 'image-gallery') {
      target = element.querySelector('.gallery.gallery-big');
      if (!target) target = element.querySelector('.sf-gallery__item--selected');
    } else if (slotContext.value.type === 'product-description-reviews' || slotContext.value.type ===  'cross-selling') {
      target = element.querySelector('.sf-tabs');
      const listTabs = target.getElementsByClassName('sf-tabs__title');
      return Array.from(listTabs).findIndex((x: any) => x.className.includes('is-active'));
    } else {
      target = element.closest('.glide__slide');
    }
    if (!target) return null;

    try {
      return target.querySelector('.glide').glide.index;
    } catch (e) {
      const isProductGlider = target.classList.contains('glide__slide') && target.firstChild.id;

      let children: Array<Element> = target.parentElement.children;
      if (!isProductGlider) return Array.from(children).indexOf(target);


      children = Array.from(children).filter((item:Element) => !item.classList.contains('glide__slide--clone'));
      const productIndex = Array.from(children).indexOf(target)
      if (productIndex > 0) return productIndex;

      let idArray = [];
      children.forEach((child: Element) => {
        const innerElement = <Element>child.firstChild;
        idArray.push(innerElement.id);
      });

      return idArray.indexOf(target.firstChild.id);
    }
  }

  function getTargetsForElementType(element: Element): Array<Element> {
    let target = null;

    if (slotContext.value.type === 'image-gallery') {
      target = element.querySelector('.sf-gallery__item');
    } else if (slotContext.value.type === 'product-description-reviews' || slotContext.value.type ===  'cross-selling') {
      target = element.getElementsByClassName('sf-tabs__title');
      return Array.from(target).length ? Array.from(target) : null;
    } else {
      target = element.closest('.glide__slide');
    }

    if (!target) return null;
    return Array.from(target.parentElement.children);
  }

  function tryGetElement(tempElementId = elementId): Element {
    try {
      return document.querySelector(`#element-${tempElementId}`)
    } catch (e) {
      return null;
    }
  }

  /**
   * client scrolls to guide
   */
  function goToGuide() {
    if (!lastGuideHoveredElement.value) return;
    // set the element as the last hovered one
    mercurePublish.client('client.hovered', lastGuideHoveredElement.value);
    scrollToElement(lastGuideHoveredElement.value);
  }

  /**
   * guide scrolls to client
   */
  function goToClient() {
    if (!lastClientHoveredElement.value) return;
    // set the element as the last hovered one
    mercurePublish.client('guide.hovered', lastClientHoveredElement.value);
    scrollToElement(lastClientHoveredElement.value);
  }

  function scrollToElement(tempElementId = elementId, index: number | null = null, highlight: boolean = true) {
    VueScrollTo.scrollTo(`#element-${tempElementId}`, SCROLL_DURATION, scrollOptions.value);
    if (index !== null) {
      let element = tryGetElement(tempElementId);

      if (element.classList.contains('product-carousel__product')) {
        element = element.closest('.sf-carousel__wrapper');
      }

      try {
        if (slotContext.value.type === 'cross-selling') {
          throw '';
        }
        const glider = element.querySelector('.glide') as any;
        glider.glide.go(`=${index}`);
      } catch (e) {
        const targets = getTargetsForElementType(element);
        if (targets !== null && targets.length > 0) {
          const target: HTMLElement = targets[index] as HTMLElement;
          if (target) target.click();
        }
      }
    }

    publishHover();
    if (highlight) temporaryHighlightElement(tempElementId);
  }

  // temporary highlight element and remove highlighting after a certain duration
  function temporaryHighlightElement(elementId) {
    mercurePublish.local('highlighted', elementId);
    setTimeout(() => {
      mercurePublish.local('highlighted', '');
    }, HIGHLIGHT_DURATION);
  }

  return {
    goToGuide,
    goToClient,
    publishScrollTo,
    scrollToIsLoading,
    setSlotContext,
    scrollToElement
  };
};
