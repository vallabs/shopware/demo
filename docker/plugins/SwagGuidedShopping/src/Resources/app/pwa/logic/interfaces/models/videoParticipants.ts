import { DailyParticipant } from "@daily-co/daily-js";

export interface VideoParticipant {
  data: DailyParticipant,
  streams: {
    video: MediaStream,
    audio: MediaStream,
  }
}

export interface VideoParticipants {
  self: VideoParticipant,
  members: { [key: string]: VideoParticipant; }
  memberCount: number,
  guide: VideoParticipant
}

export interface CustomMediaDeviceInfo {
  readonly deviceId: string;
  readonly groupId: string;
  readonly kind: MediaDeviceKind;
  readonly label: string;
}

export interface MediaDevices {
  videoInput: CustomMediaDeviceInfo[],
  audioInput: CustomMediaDeviceInfo[],
  audioOutput: CustomMediaDeviceInfo[],
}
