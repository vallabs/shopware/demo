import { ref } from "vue-demi";
import { Product } from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useState} from '../useState';
jest.mock('../useState');
const mockedUseState = useState as jest.Mocked<typeof useState>;

import {useInteractions} from '../useInteractions';
jest.mock('../useInteractions');
const mockedUseInteractions = useInteractions as jest.Mocked<typeof useInteractions>;

import { useLastSeen } from '../useLastSeen';
jest.mock('../useLastSeen');
const mockedUseLastSeen = useLastSeen as jest.Mocked<typeof useLastSeen>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { useQuickView } from "../useQuickView";

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

describe("Composables - useQuickView", () => {
  const inputMock = ref<Product>();
  const handleErrorMock = jest.fn();
  const publishInteractionMock = jest.fn();
  const loadLastSeenMock = jest.fn();
  const getQuickViewMock = jest.fn();
  const switchQuickViewModalStateMock = jest.fn();
  const rootContextMock = prepareRootContextMock();
  const _isLoadingMock = ref<boolean>(false);
  const _quickViewCmsPageMock = ref<any>(null);  
  const stateForAllMock = ref(null);

  beforeEach(() => {
    jest.resetAllMocks();
    _isLoadingMock.value = false;
    _quickViewCmsPageMock.value = null;
    stateForAllMock.value = {
      quickviewPageId: 'dummyPageId',
    };
    inputMock.value = {
      id: 'dummyProductId'
    } as any;

    

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("loading"))
            return _isLoadingMock;
          if (contextName.includes("page"))
            return _quickViewCmsPageMock;
        }
      } as any
    });

    mockedComposables.useUIState.mockImplementation(() => {
      return {
        switchState: switchQuickViewModalStateMock,
      } as any;
    });

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    (mockedUseState as any).mockImplementation(() => ({
      stateForAll: stateForAllMock,
    } as any));

    (mockedUseInteractions as any).mockImplementation(() => ({
      publishInteraction: publishInteractionMock
    } as any));

    (mockedUseLastSeen as any).mockImplementation(() => ({
      loadLastSeen: loadLastSeenMock
    }) as any);

    getQuickViewMock.mockResolvedValue(null);

    mockedGSApiClient.QuickViewService = {
      getQuickView: getQuickViewMock,
    } as any;

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("openQuickView", () => {
      it("should work correctly when isNewProduct is false", async () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }

        inputMock.value = {
          id: 'dummyProductId'
        } as any;

        const { openQuickView } = useQuickView();
        await openQuickView(inputMock.value);
        expect(getQuickViewMock).not.toHaveBeenCalled();
        expect(switchQuickViewModalStateMock).toHaveBeenCalledTimes(1);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'quickview.opened',
          payload: {
            productId: undefined
          }
        });
        expect(loadLastSeenMock).toHaveBeenCalledTimes(1);
      });

      it("should work correctly when isNewProduct is true & product is valid", async () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }

        inputMock.value = {
          id: 'newProductId'
        } as any;

        const { openQuickView } = useQuickView();
        await openQuickView(inputMock.value);
        expect(getQuickViewMock).toHaveBeenCalledWith({
          productId:'newProductId',
          cmsPageLayoutId: 'dummyPageId'
        }, rootContextMock.apiInstance);
        expect(switchQuickViewModalStateMock).toHaveBeenCalledTimes(1);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'quickview.opened',
          payload: {
            productId: 'newProductId'
          }
        });
        expect(loadLastSeenMock).toHaveBeenCalledTimes(1);
      });

      it("should work correctly when isNewProduct is true & product is invalid", async () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }

        inputMock.value = null as any;

        const { openQuickView } = useQuickView();
        await openQuickView(inputMock.value);
        expect(getQuickViewMock).not.toHaveBeenCalled();
        expect(switchQuickViewModalStateMock).toHaveBeenCalledTimes(1);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'quickview.opened',
          payload: {
            productId: undefined
          }
        });
        expect(loadLastSeenMock).toHaveBeenCalledTimes(1);
      });
    });

    describe("setQuickViewProduct", () => {
      it("should return when isNewProduct is false", async () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }
        inputMock.value = {
          id: 'dummyProductId'
        } as any;
        
        const { setQuickViewProduct } = useQuickView();
        await setQuickViewProduct(inputMock.value);
        expect(_isLoadingMock.value).toEqual(false);
        expect(getQuickViewMock).not.toHaveBeenCalled();
      });

      it("should return correctly when isNewProduct is true and state is true", async () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }
        inputMock.value = {
          id: 'newProductId'
        } as any;

        getQuickViewMock.mockResolvedValue({
          "resourceType": null,
          "resourceIdentifier": null,
          "canonicalPathInfo": null,
          "cmsPage": {
              "name": "Digital Sales Rooms Quickview",
              "type": "product_detail",
              "id": "182d3f7f988044adbba449b70c8bc472",
              "apiAlias": "cms_page"
          },
          "breadcrumb": null,
          "product": {
              "name": "Awesome Steel Saguarro Foam",
              "id": "012d584306614690ab5ba44020050645",
              "apiAlias": "product"
          },
          "configurator": [],
          "apiAlias": "pwa_page_result"
        });

        const { setQuickViewProduct, quickViewCmsPage } = useQuickView();
        await setQuickViewProduct(inputMock.value);

        expect(getQuickViewMock).toHaveBeenCalledWith({
          productId:'newProductId',
          cmsPageLayoutId: 'dummyPageId'
        }, rootContextMock.apiInstance);
        expect(quickViewCmsPage.value).toEqual({
          "resourceType": null,
          "resourceIdentifier": null,
          "canonicalPathInfo": null,
          "cmsPage": {
              "name": "Digital Sales Rooms Quickview",
              "type": "product_detail",
              "id": "182d3f7f988044adbba449b70c8bc472",
              "apiAlias": "cms_page"
          },
          "breadcrumb": null,
          "product": {
              "name": "Awesome Steel Saguarro Foam",
              "id": "012d584306614690ab5ba44020050645",
              "apiAlias": "product"
          },
          "configurator": [],
          "apiAlias": "pwa_page_result"
        });
      });

      it("should handle error correctly when API getQuickViewCmsPage failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400,
        }
        getQuickViewMock.mockRejectedValueOnce(res);
        const { setQuickViewProduct } = useQuickView();
        await setQuickViewProduct(inputMock.value);
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useQuickView",
            method: "getQuickViewCmsPage"
          }
        );
        expect(_isLoadingMock.value).toEqual(false);
      });
    });

    describe("isQuickViewProductChanging", () => {
      it("should return true when product id of _quickViewCmsPage is different with product id", () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }
        inputMock.value = {
          id: 'newProductId'
        } as any;
        const { isQuickViewProductChanging } = useQuickView();
        const res = isQuickViewProductChanging(inputMock.value);
        expect(res).toEqual(true);
      });

      it("should return false when product id of _quickViewCmsPage is equal with product id", () => {
        _quickViewCmsPageMock.value = {
          product: {
            id: 'dummyProductId'
          }
        }
        inputMock.value = {
          id: 'dummyProductId'
        } as any;
        const { isQuickViewProductChanging } = useQuickView();
        const res = isQuickViewProductChanging(inputMock.value);
        expect(res).toEqual(false);
      });
    });

    describe("setQuickViewLoadingState", () => {
      it("should return when state is null", async () => {
        _isLoadingMock.value = false;
        const { setQuickViewLoadingState } = useQuickView();
        setQuickViewLoadingState();
        expect(_isLoadingMock.value).toBe(false);
      });

      it("should return correctly state of params", () => {
        _isLoadingMock.value = false;
        const { setQuickViewLoadingState } = useQuickView();
        setQuickViewLoadingState(true);
        expect(_isLoadingMock.value).toBe(true);
      });
    });
  });

  describe("computed", () => {
    describe("quickViewCmsPage", () => {
      it("should return cms page value", () => {
        const { quickViewCmsPage } = useQuickView();
        expect(quickViewCmsPage.value).toBe(_quickViewCmsPageMock.value);
      });
    });

    describe("isQuickViewLoading", () => {
      it("should return loading value", () => {
        const { isQuickViewLoading } = useQuickView();
        expect(isQuickViewLoading.value).toBe(_isLoadingMock.value);
      });
    });
  });
});
