import { ref } from "@vue/composition-api";
import { prepareRootContextMock } from "./contextRunner";
import { useGsRoute } from "../useGsRoute";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

describe("Composables - useGsRoute", () => {
  const rootContextMock = prepareRootContextMock();
  const mockedRoot = ref(rootContextMock);

  beforeEach(() => {
    jest.resetAllMocks();
    mockedRoot.value = rootContextMock;
    mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
  });

  describe("Computed", () => {
    describe("appointmentId", () => {
      it("should return appointmentId from route params", () => {
        mockedRoot.value = {
          ...rootContextMock,
          route: {
          params: {
            appointmentId: 1
          },
          query: {
            slide: 1,
            slideGroup: 2,
            il: true
          }
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { appointmentId } = useGsRoute();
        expect(appointmentId.value).toEqual(1);
      });
    });

    describe("openInstantListing", () => {
      it("should return true if query param il is set", () => {
        mockedRoot.value = {
          ...rootContextMock,
          route: {
          params: {
            appointmentId: 1
          },
          query: {
            slide: 1,
            slideGroup: 2,
            il: 'edit'
          }
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { il } = useGsRoute();
        expect(il.value).toEqual('edit');
      });

      it("should return false if query param il isn't set", () => {
        mockedRoot.value = {
          ...rootContextMock,
          route: {
          params: {
            appointmentId: 1
          },
          query: {
            slide: 1,
            slideGroup: 2,
            il: null
          }
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { il } = useGsRoute();
        expect(il.value).toEqual(null);
      });
    });

    describe("slide", () => {
      it("should return number slide from query param", () => {
        mockedRoot.value = {
          ...rootContextMock,
          route: {
          params: {
            appointmentId: 1
          },
          query: {
            slide: 1,
            slideGroup: 2,
            il: true
          }
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { slide } = useGsRoute();
        expect(slide.value).toEqual(1);
      });

      it("should return 0 from query param", () => {
        mockedRoot.value = {
        ...rootContextMock,
        route: {
          params: {
          appointmentId: 1
          },
          query: {
          slide: null,
          slideGroup: 2,
          il: true
          }
        }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { slide } = useGsRoute();
        expect(slide.value).toEqual(0);
      });
    });

    describe("slideGroup", () => {
      it("should return number slideGroup from query param", () => {
        mockedRoot.value = {
          ...rootContextMock,
          route: {
            params: {
              appointmentId: 1
            },
            query: {
              slide: 1,
              slideGroup: 2,
              il: true
            }
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { slideGroup } = useGsRoute();
        expect(slideGroup.value).toEqual(2);
      });

      it("should return 0 slideGroup from query param", () => {
        mockedRoot.value = {
          ...rootContextMock,
          route: {
            params: {
              appointmentId: 1
            },
            query: {
              slide: 1,
              slideGroup: null,
              il: true
            }
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        const { slideGroup } = useGsRoute();
        expect(slideGroup.value).toEqual(0);
      });
    });
  });
});
