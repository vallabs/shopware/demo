import {ref} from "vue-demi";
import {getApplicationContext } from "@shopware-pwa/composables";

import { AppointmentService } from '../api-client';
import { useErrorHandler } from "./useErrorHandler";
import { AppointmentModel } from "../interfaces";

export type InvitationRequest = {
  token: string,
  answer: 'accepted' | 'maybe' | 'declined'
};

export interface AppointmentInvitationResponse {
  answer: 'accepted' | 'maybe' | 'declined',
  appointment: {
    accessibleFrom: {
      date: string,
      timezone: string,
      timezone_type: number
    },
    accessibleTo: {
      date: string,
      timezone: string,
      timezone_type: number
    },
    id: string,
    status?: string
  }
}

/**
 * Composable for invitation management of the attendee.
 */
export function useInvitation() {
  const COMPOSABLE_NAME = "useInvitation";
  const contextName = COMPOSABLE_NAME;
  const {apiInstance} = getApplicationContext({contextName});
  const { handleError } = useErrorHandler();


  const loading = ref({});

  async function respondAppointmentInvitation(appointmentId: string, data: InvitationRequest): Promise<AppointmentInvitationResponse | undefined> {
    loading.value['respondAppointmentInvitation'] = true;

    try {
      const res = AppointmentService.respondAppointmentInvitation(appointmentId, data, apiInstance);
      return res;
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'replyInvitation',
      });
    } finally {
      loading.value['respondAppointmentInvitation'] = false;
    }
  }

  async function downloadInvitationFile(appointmentId: string, token: string): Promise<void> {
      loading.value['downloadInvitationFile'] = true;

    try {
      const res = await AppointmentService.downloadInvitationFile(appointmentId, token, apiInstance);
      const href = URL.createObjectURL(res);
    
      const link = document.createElement('a');
      link.href = href;
      link.setAttribute('download', `${appointmentId}.ics`);
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
      URL.revokeObjectURL(href);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'replyInvitation',
      });
    } finally {
      loading.value['downloadInvitationFile'] = false;
    }
  }

  return {
    respondAppointmentInvitation,
    downloadInvitationFile,
    loading
  };
}
