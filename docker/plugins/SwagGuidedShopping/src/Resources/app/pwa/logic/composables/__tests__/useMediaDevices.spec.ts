import { ref, createApp } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { useMediaDevices } from "../useMediaDevices";

import { prepareRootContextMock } from "./contextRunner";

function mockLoadComposableInApp(composable) {
  let result;
  const app = createApp({
  setup() {
    result = composable();
    // suppress missing template warning
    return result;
  },
  });
  const wrapper = app.mount(document.createElement("div"));
  return [result, app, wrapper];
}

import { MediaDevices } from "../../interfaces";

export type DeviceList = {
  videoInput: MediaDeviceInfo[],
  audioInput: MediaDeviceInfo[],
  audioOutput: MediaDeviceInfo[],
};

export type DeviceError = 'NO_DEVICES_FOUND' | 'CAMERA_IN_USE' | 'MICROPHONE_IN_USE' | 'CAMERA_AND_MICROPHONE_IN_USE' | 'SPEAKER_NOT_ALLOWED';
export type MediaDeviceChanged = { storageKey, deviceId };

const FAKE_SPEAKER_ID = '__fakeSpeakerDeviceId';

const SAVED_SETTINGS_STORAGE_KEY = 'sw-gs-call-saved-settings';
const DEVICES_STORAGE_KEY = 'sw-gs-call-devices';
const AUDIO_ONLY_STORAGE_KEY = 'sw-gs-call-audio-only';
const MICROPHONE_STATE_STORAGE_KEY = 'sw-gs-call-microphone-state';
const CAMERA_STATE_STORAGE_KEY = 'sw-gs-call-camera-state';

const ENUMERATE_DEVICES = [
  {
    deviceId: "111",
    kind: "audioinput",
    label: "",
    groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b"
  },
  {
    deviceId: "222",
    kind: "videoinput",
    label: "",
    groupId: "942762a18cf5115ddc027808db0d733d75ff7898a22b8fd3976f03eaa35525fe"
  },
  {
    deviceId: "333",
    kind: "audiooutput",
    label: "",
    groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b"
  },
  {}
]

describe("Composables - useMediaDevices", () => {
  let result;
  let app;
  let wrapper;

  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const addEventListenerMock = jest.fn();

  const microphoneDeviceMock = ref<string>("");
  const speakerDeviceMock = ref<string>("");
  const cameraDeviceMock = ref<string>("");


  const _hasMediaDevicesStoredMock = ref<boolean>(false);
  const _hasSavedSettingsMock = ref<boolean>(false);
  const _mediaDeviceChangedMock = ref<MediaDeviceChanged>(null);
  const _deviceErrorMock = ref<DeviceError>(null);
  const isAudioOnlyMock = ref<boolean>(false);
  const _mediaStreamMock = ref<MediaStream>(null);

  const _mediaDevicesMock = ref<MediaDevices>({
    videoInput: [],
    audioInput: [],
    audioOutput: [],
  });

  const enumerateDevicesMock = ref(null);

  const enumerateDevicesFncMock = async () => {
    return new Promise<any>(resolve => {
      resolve(enumerateDevicesMock.value)
    })
  }

  const getUserMediaFncMock = jest.fn();

  const enumerateDevicesErrorFncMock = async () => {
    return new Promise<any>((resolve, reject) => {
      reject(false)
    })
  }

  beforeEach(() => {
    jest.resetAllMocks();

    enumerateDevicesMock.value = null;
    _mediaStreamMock.value = null;
    microphoneDeviceMock.value = "";
    speakerDeviceMock.value = "";
    cameraDeviceMock.value = "";

    _hasMediaDevicesStoredMock.value = false;
    _hasSavedSettingsMock.value = false;
    _mediaDeviceChangedMock.value = null;
    _deviceErrorMock.value = null;
    isAudioOnlyMock.value = false;

    _mediaDevicesMock.value = {
      videoInput: [],
      audioInput: [],
      audioOutput: [],
    };

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("microphoneDevice"))
            return microphoneDeviceMock;
          if (contextName.includes("speakerDevice"))
            return speakerDeviceMock;
          if (contextName.includes("cameraDevice"))
            return cameraDeviceMock;
          if (contextName.includes("hasMediaDevicesStored"))
            return _hasMediaDevicesStoredMock;
          if (contextName.includes("hasSavedSettings"))
            return _hasSavedSettingsMock;
          if (contextName.includes("mediaDeviceChanged"))
            return _mediaDeviceChangedMock;
          if (contextName.includes("deviceError"))
            return _deviceErrorMock;
          if (contextName.includes("isAudioOnly"))
            return isAudioOnlyMock;
          if (contextName.includes("devices"))
            return _mediaDevicesMock;
          if (contextName.includes("mediaStream"))
            return _mediaStreamMock;
        },
      } as any;
    });

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});

    (global.navigator as any).mediaDevices = {
      enumerateDevices: enumerateDevicesFncMock,
      getUserMedia: getUserMediaFncMock,
      addEventListener: addEventListenerMock,
      removeEventListener: jest.fn(),
    }

    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
    }
  });

  describe("methods", () => {
    describe("mounted hook", () => {
      it("should work correctly when mounted", async () => {
        sessionStorage.removeItem('sw-gs-call-microphone-device');
        sessionStorage.removeItem('sw-gs-call-speaker-device');
        sessionStorage.removeItem('sw-gs-call-camera-device');
        sessionStorage.setItem(SAVED_SETTINGS_STORAGE_KEY, "testStorageKey");
        sessionStorage.setItem(AUDIO_ONLY_STORAGE_KEY, "true");

        [result, app] = mockLoadComposableInApp(() => useMediaDevices());
        result.init();
        expect(result.hasSavedSettings.value).toEqual(true);
        expect(result.isAudioOnly.value).toEqual(true);
        expect(result.hasMediaDevicesStored.value).toEqual(false);

        // test eventlistener
        expect(addEventListenerMock).toHaveBeenCalledWith('devicechange', expect.any(Function));

        // assume devicechange event trigger
        sessionStorage.setItem(DEVICES_STORAGE_KEY, JSON.stringify(ENUMERATE_DEVICES));
        enumerateDevicesMock.value = null;
        const handler = addEventListenerMock.mock.calls[0][1];
        handler();

        expect(result.mediaDevices.value).toEqual({
          videoInput: [],
          audioInput: [],
          audioOutput: [],
        });
        expect(result.cameraCount.value).toEqual(0);
        expect(result.speakerCount.value).toEqual(0);
        expect(result.microphoneCount.value).toEqual(0);
      });

    });

    describe("getDeviceKeys", () => {
      it("should return device keys", () => {
        const { getDeviceKeys } = useMediaDevices();
        expect(getDeviceKeys()).toEqual({
          microphone: 'microphone-device',
          speaker: 'speaker-device',
          camera: 'camera-device'
        });
      });
    });

    describe("getMediaDevices", () => {
      it("should work correctly when sessionStorage save DEVICES_STORAGE_KEY with wrong JSON format value", async () => {
        sessionStorage.setItem(DEVICES_STORAGE_KEY, "{a}");
        enumerateDevicesMock.value = null;

        const { getMediaDevices, mediaDevices, cameraCount, microphoneCount, speakerCount } = useMediaDevices();
        await getMediaDevices();
        expect(mediaDevices.value).toEqual({
          videoInput: [],
          audioInput: [],
          audioOutput: [],
        });
        expect(cameraCount.value).toEqual(0);
        expect(speakerCount.value).toEqual(0);
        expect(microphoneCount.value).toEqual(0);
      });

      it("should work correctly when there is not any device from storage & no enumerateDevices", async () => {
        sessionStorage.setItem(DEVICES_STORAGE_KEY, "null");
        enumerateDevicesMock.value = null;

        const { getMediaDevices, mediaDevices, cameraCount, microphoneCount, speakerCount } = useMediaDevices();
        await getMediaDevices();
        expect(mediaDevices.value).toEqual({
          videoInput: [],
          audioInput: [],
          audioOutput: [],
        });
        expect(cameraCount.value).toEqual(0);
        expect(speakerCount.value).toEqual(0);
        expect(microphoneCount.value).toEqual(0);
      });

      it("should work correctly when there are devices from storage & renewList true & no enumerateDevices", async () => {
        sessionStorage.setItem(DEVICES_STORAGE_KEY, JSON.stringify(ENUMERATE_DEVICES));
        enumerateDevicesMock.value = null;

        const { getMediaDevices, mediaDevices, cameraCount, microphoneCount, speakerCount } = useMediaDevices();
        await getMediaDevices(true);
        expect(mediaDevices.value).toEqual({
          videoInput: [],
          audioInput: [],
          audioOutput: [],
        });
        expect(cameraCount.value).toEqual(0);
        expect(speakerCount.value).toEqual(0);
        expect(microphoneCount.value).toEqual(0);
      });

      it("should work correctly when allowEmptyLabels false & there is not any device from storage & there are enumerateDevices with noLabel", async () => {
        sessionStorage.setItem(DEVICES_STORAGE_KEY, "null");
        enumerateDevicesMock.value = ENUMERATE_DEVICES;

        const { getMediaDevices, mediaDevices, cameraCount, microphoneCount, speakerCount } = useMediaDevices();
        await getMediaDevices();
        expect(mediaDevices.value).toEqual({
          videoInput: [],
          audioInput: [],
          audioOutput: [
            {
             deviceId: "__fakeSpeakerDeviceId",
             groupId: "__fakeSpeakerDeviceId",
             kind: "audiooutput",
             label: "System Speaker",
            }
          ],
        });
        expect(cameraCount.value).toEqual(0);
        expect(speakerCount.value).toEqual(1);
        expect(microphoneCount.value).toEqual(0);
      });

      it("should work correctly when allowEmptyLabels true & there is not any device from storage & there are enumerateDevices with noLabel", async () => {
        sessionStorage.setItem(DEVICES_STORAGE_KEY, "null");
        enumerateDevicesMock.value = ENUMERATE_DEVICES;

        const { getMediaDevices, mediaDevices, cameraCount, microphoneCount, speakerCount } = useMediaDevices();
        await getMediaDevices(false, true);
        expect(mediaDevices.value).toEqual({
          videoInput: [
            {
              deviceId: "222",
              groupId: "942762a18cf5115ddc027808db0d733d75ff7898a22b8fd3976f03eaa35525fe",
              kind: "videoinput",
              label: "",
            }
          ],
          audioInput: [
            {
              deviceId: "111",
              groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b",
              kind: "audioinput",
              label: "",
            }
          ],
          audioOutput: [
            {
             deviceId: "333",
             groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b",
             kind: "audiooutput",
             label: "",
            }
          ],
        });
        expect(cameraCount.value).toEqual(1);
        expect(speakerCount.value).toEqual(1);
        expect(microphoneCount.value).toEqual(1);
      });

      it("should handle error when got bug", async () => {
        sessionStorage.setItem(DEVICES_STORAGE_KEY, "null");
        enumerateDevicesMock.value = ENUMERATE_DEVICES;

        (global.navigator as any).mediaDevices = {
          enumerateDevices: enumerateDevicesErrorFncMock
        }

        const { getMediaDevices, deviceError } = useMediaDevices();
        await getMediaDevices();
        expect(deviceError.value).toEqual('NO_DEVICES_FOUND');
      });

    });

    describe("storeHasDevices", () => {
      it("should return false when session store not found device", async () => {
        sessionStorage.removeItem('sw-gs-call-microphone-device');
        sessionStorage.removeItem('sw-gs-call-speaker-device');
        sessionStorage.removeItem('sw-gs-call-camera-device');
        const { storeHasDevices, hasMediaDevicesStored } = useMediaDevices();
        await storeHasDevices();
        expect(hasMediaDevicesStored.value).toEqual(false);
      });

      it("should return false when there is only device not existed in session store", async () => {
        sessionStorage.setItem('sw-gs-call-microphone-device', "test microphone");
        sessionStorage.setItem('sw-gs-call-speaker-device', "test speaker");
        sessionStorage.setItem('sw-gs-call-camera-device', "null");
        const { storeHasDevices } = useMediaDevices();
        await storeHasDevices();
        expect(_hasMediaDevicesStoredMock.value).toEqual(false);
      });

      it("should return true when session store found device", async () => {
        sessionStorage.setItem('sw-gs-call-microphone-device', "test microphone");
        sessionStorage.setItem('sw-gs-call-speaker-device', "test speaker");
        sessionStorage.setItem('sw-gs-call-camera-device', "test camera");
        const { storeHasDevices } = useMediaDevices();
        await storeHasDevices();
        expect(_hasMediaDevicesStoredMock.value).toEqual(true);
      });
    });

    describe("hasDevices", () => {
      it("should return false when there is not any device", async () => {
        _mediaDevicesMock.value = {
          videoInput: [],
          audioInput: [],
          audioOutput: [],
        };
        const { hasDevices } = useMediaDevices();
        await hasDevices();
        expect(hasDevices()).toEqual(false);
      });

      it("should return  value when have devices", async () => {
        _mediaDevicesMock.value = {
          videoInput: [
            {
              deviceId: "",
              kind: "videoinput",
              label: "",
              groupId: "1979ce0c13ed366e87263f8a0356ae61e769948e4cebb5df647e56ec18b5d82e"
            },
          ],
          audioInput: [
            {
              deviceId: "",
              kind: "audioinput",
              label: "",
              groupId: "1979ce0c13ed366e87263f8a0356ae61e769948e4cebb5df647e56ec18b5d82e"
            }
          ],
          audioOutput: [],
        };
        const { hasDevices } = useMediaDevices();
        await hasDevices();
        expect(hasDevices()).toEqual(true);
      });
    });

    describe("saveDevicesToStorage", () => {
      it("should return null when sessionStorage setItem", async () => {
        microphoneDeviceMock.value = 'test-micro';
        speakerDeviceMock.value = 'test-speaker';
        cameraDeviceMock.value = null;
        const { saveDevicesToStorage, hasSavedSettings, mediaDeviceChanged } = useMediaDevices();
        saveDevicesToStorage();
        expect(sessionStorage.getItem('sw-gs-call-microphone-device')).toEqual('test-micro');
        expect(sessionStorage.getItem('sw-gs-call-speaker-device')).toEqual('test-speaker');
        expect(sessionStorage.getItem('sw-gs-call-camera-device')).toEqual('');
        expect(sessionStorage.getItem(SAVED_SETTINGS_STORAGE_KEY)).toEqual('true');
        expect(hasSavedSettings.value).toEqual(true);
        expect(mediaDeviceChanged.value).toEqual({
          storageKey: 'camera-device',
          deviceId: null
        });
      });
    });

    describe("setDevicesFromStorage", () => {
      it("should works correctly when isAudioOnly true", async () => {
        const devices = {
          videoInput: [
            {
              deviceId: "videoInputDeviceID",
              groupId: "942762a18cf5115ddc027808db0d733d75ff7898a22b8fd3976f03eaa35525fe",
              kind: "videoinput",
              label: "",
            }
          ],
          audioInput: [
            {
              deviceId: "audioInputDeviceID",
              groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b",
              kind: "audioinput",
              label: "",
            }
          ],
          audioOutput: [
            {
             deviceId: "audioOutputDeviceID",
             groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b",
             kind: "audiooutput",
             label: "",
            }
          ],
        };
        isAudioOnlyMock.value = true
        sessionStorage.removeItem('sw-gs-call-microphone-device');
        sessionStorage.removeItem('sw-gs-call-camera-device');
        sessionStorage.removeItem('sw-gs-call-speaker-device');
        const { setDevicesFromStorage, microphoneDevice, speakerDevice, cameraDevice } = useMediaDevices();

        setDevicesFromStorage(devices);
        expect(microphoneDevice.value).toEqual('audioInputDeviceID');
        expect(speakerDevice.value).toEqual('audioOutputDeviceID');
        expect(cameraDevice.value).toEqual(null);
      });

      it("should works correctly when isAudioOnly false", async () => {
        const devices = {
          videoInput: [
            {
              deviceId: "videoInputDeviceID",
              groupId: "942762a18cf5115ddc027808db0d733d75ff7898a22b8fd3976f03eaa35525fe",
              kind: "videoinput",
              label: "",
            }
          ],
          audioInput: [
            {
              deviceId: "audioInputDeviceID",
              groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b",
              kind: "audioinput",
              label: "",
            }
          ],
          audioOutput: [
            {
             deviceId: "audioOutputDeviceID",
             groupId: "f5a37f01d0dd702067d3624a70a6ab545ad75890fad2ee12cde15edf361a2e4b",
             kind: "audiooutput",
             label: "",
            }
          ],
        };
        isAudioOnlyMock.value = false;
        sessionStorage.removeItem('sw-gs-call-microphone-device');
        sessionStorage.removeItem('sw-gs-call-camera-device');
        sessionStorage.removeItem('sw-gs-call-speaker-device');
        const { setDevicesFromStorage, microphoneDevice, speakerDevice, cameraDevice } = useMediaDevices();

        setDevicesFromStorage(devices);
        expect(microphoneDevice.value).toEqual('audioInputDeviceID');
        expect(speakerDevice.value).toEqual('audioOutputDeviceID');
        expect(cameraDevice.value).toEqual('videoInputDeviceID');
      });
    });

    describe("unsetDevicesFromStorage", () => {
      it("should works correctly", async () => {
        microphoneDeviceMock.value = 'test-micro';
        speakerDeviceMock.value = null;
        cameraDeviceMock.value = 'test-camera';
        const { saveDevicesToStorage, hasSavedSettings, mediaDeviceChanged, unsetDevicesFromStorage } = useMediaDevices();
        saveDevicesToStorage();
        expect(sessionStorage.getItem('sw-gs-call-microphone-device')).toEqual('test-micro');
        expect(sessionStorage.getItem('sw-gs-call-speaker-device')).toEqual('');
        expect(sessionStorage.getItem('sw-gs-call-camera-device')).toEqual('test-camera');
        expect(sessionStorage.getItem(SAVED_SETTINGS_STORAGE_KEY)).toEqual('true');
        expect(hasSavedSettings.value).toEqual(true);
        expect(mediaDeviceChanged.value).toEqual({
          storageKey: 'camera-device',
          deviceId: 'test-camera'
        });
        unsetDevicesFromStorage();
        expect(sessionStorage.getItem('sw-gs-call-microphone-device')).toEqual(null);
        expect(sessionStorage.getItem('sw-gs-call-speaker-device')).toEqual(null);
        expect(sessionStorage.getItem('sw-gs-call-camera-device')).toEqual(null);
        expect(mediaDeviceChanged.value).toEqual({
          storageKey: 'camera-device',
          deviceId: null
        });
      });
    });

    describe("clearDeviceState", () => {
      it("should works correctly", async () => {
        microphoneDeviceMock.value = 'test-micro';
        speakerDeviceMock.value = null;
        cameraDeviceMock.value = 'test-camera';
        const { saveDevicesToStorage, hasSavedSettings, mediaDeviceChanged, clearDeviceState } = useMediaDevices();
        saveDevicesToStorage();
        expect(sessionStorage.getItem('sw-gs-call-microphone-device')).toEqual('test-micro');
        expect(sessionStorage.getItem('sw-gs-call-speaker-device')).toEqual('');
        expect(sessionStorage.getItem('sw-gs-call-camera-device')).toEqual('test-camera');
        expect(sessionStorage.getItem(SAVED_SETTINGS_STORAGE_KEY)).toEqual('true');
        expect(hasSavedSettings.value).toEqual(true);
        expect(mediaDeviceChanged.value).toEqual({
          storageKey: 'camera-device',
          deviceId: 'test-camera'
        });
        clearDeviceState();
        expect(sessionStorage.getItem('sw-gs-call-microphone-device')).toEqual(null);
        expect(sessionStorage.getItem('sw-gs-call-speaker-device')).toEqual(null);
        expect(sessionStorage.getItem('sw-gs-call-camera-device')).toEqual(null);
        expect(mediaDeviceChanged.value).toEqual({
          storageKey: 'camera-device',
          deviceId: null
        });
        expect(sessionStorage.getItem(AUDIO_ONLY_STORAGE_KEY)).toEqual(null);
        expect(sessionStorage.getItem(DEVICES_STORAGE_KEY)).toEqual(null);
        expect(sessionStorage.getItem(MICROPHONE_STATE_STORAGE_KEY)).toEqual(null);
        expect(sessionStorage.getItem(CAMERA_STATE_STORAGE_KEY)).toEqual(null);
        expect(sessionStorage.getItem(SAVED_SETTINGS_STORAGE_KEY)).toEqual(null);
      });
    });

    describe("setAudioOnly", () => {
      it("should set true value for AUDIO_ONLY_STORAGE_KEY", async () => {
        const { setAudioOnly, cameraDevice, isAudioOnly } = useMediaDevices();
        setAudioOnly();
        expect(sessionStorage.getItem(AUDIO_ONLY_STORAGE_KEY)).toEqual('true');
        expect(cameraDevice.value).toEqual(null);
        expect(isAudioOnly.value).toEqual(true);
      });
    });

    describe("unsetDeviceError", () => {
      it("should return null value", async () => {
        const { unsetDeviceError, deviceError } = useMediaDevices();
        await unsetDeviceError();
        expect(deviceError.value).toEqual(null);
      });
    });

    describe("getMediaStream", () => {
      it("should stop track when _mediaStream is true", async () => {
        const trackStop = jest.fn();
        getUserMediaFncMock.mockResolvedValue({
          active: true,
          id: "2TGQ9XI07uSSjrjiOhgLumpmc10Oa6R00nzb",
          getTracks: () => {
            return [
              {
                id: '1',
                stop: trackStop
              },
              {
                id: '2',
                stop: trackStop
              }
            ]
          }
        });
        const { getMediaStream } = useMediaDevices();
        await getMediaStream({audio: '', video: ''});
        expect(getUserMediaFncMock).toHaveBeenCalledWith({
          video: true,
          audio: true
        })
        expect(_mediaStreamMock.value.active).toEqual(true);
        expect(_mediaStreamMock.value.id).toEqual("2TGQ9XI07uSSjrjiOhgLumpmc10Oa6R00nzb");
        await getMediaStream({audio: '', video: ''});
        expect(trackStop).toHaveBeenCalled();
      });
    });

    describe("getConstraints", () => {
      it("should return true when audio and video is empty", async () => {
        const { getConstraints } = useMediaDevices();
        const getConstraintsValue = getConstraints({
          audio: "",
          video: ""
        });
        expect(getConstraintsValue).toEqual({
          audio: true,
          video: true
        });
      });

      it("should return true when audio and video have value", async () => {
        const { getConstraints } = useMediaDevices();
        const getConstraintsValue = getConstraints({
          audio: "testAudio",
          video: ""
        });
        expect(getConstraintsValue).toEqual({
          audio: {
            deviceId: {
              exact: "testAudio"
            }
          },
          video: true
        });
      });
    });
  });

});
