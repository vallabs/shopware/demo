import { ref, createApp } from "vue-demi";
import { SearchFilterType } from "@shopware-pwa/commons";
import { Cart } from "@shopware-pwa/commons";
import { ClientApiError } from "../../interfaces";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import {useState} from '../useState';
jest.mock('../useState');
const mockedUseState = useState as jest.Mocked<typeof useState>;

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

import {useAdminApiInstance} from '../useAdminApiInstance';
jest.mock('../useAdminApiInstance');
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<typeof useAdminApiInstance>;

import {useCustomerInsights} from '../widgets/useCustomerInsights';
jest.mock('../widgets/useCustomerInsights');
const mockedUseCustomerInsights = useCustomerInsights as jest.Mocked<typeof useCustomerInsights>;

import { useCartInsights } from '../widgets/useCartInsights';
jest.mock('../widgets/useCartInsights');
const mockedUseCartInsights = useCartInsights as jest.Mocked<typeof useCartInsights>;

import * as shopwareClient from "@shopware-pwa/shopware-6-client";
jest.mock("@shopware-pwa/shopware-6-client");
let mockedApiClient = shopwareClient as jest.Mocked<typeof shopwareClient>;

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { useGsCart } from "../useGsCart";

import * as GSApiClient from '../../api-client';
jest.mock('../../api-client');
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

function mockLoadComposableInApp(composable) {
	let result;
	const app = createApp( {
		setup() {
			result = composable();
			// suppress missing template warning
			return result;
		}
	} );
	const wrapper = app.mount( document.createElement( 'div' ) );
	return [ result, app, wrapper ];
}

describe("Composables - useGsCart", () => {
  let result;
  let app;
  let wrapper;
  const product = ref<any>({
    id: 'dummyProductId',
  });
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const getContextTokenForCurrentClientMock = jest.fn(() => Promise.resolve(null as any));
  const loadAttendeeDataMock = jest.fn(() => Promise.resolve(null as any));
  const getCartGuideMock = jest.fn(() => Promise.resolve(null as any));
  const addLineItemsGuideMock = jest.fn(() => Promise.resolve(null as any));
  const changeLineItemQuantitiesGuideMock = jest.fn(() => Promise.resolve(null as any));
  const removeLineItemsGuideMock = jest.fn(() => Promise.resolve(null as any));
  const getCartClientMock = jest.fn(() => Promise.resolve(null as any));
  const addLineItemsClientMock = jest.fn(() => Promise.resolve(null as any));
  const changeLineItemQuantitiesClientMock = jest.fn(() => Promise.resolve(null as any));
  const removeLineItemsClientMock = jest.fn(() => Promise.resolve(null as any));
  const loadCartInsightsMock = jest.fn(() => Promise.resolve(null as any));
  const mercureGuidePublishMock = jest.fn();
  const getProductsMock = jest.fn();

  const  _storeCartMock = ref<Cart>();
  const disabledCartCountMock = ref<number>(0);
  const currentClientForGuideMock = ref<string>("");
  const currentValidTokenMock = ref<string>("");
  const currentClientTokenPermissionsMock = ref<boolean>(false);
  const attendeeDataMock = ref(null);
  const lastClientChangedCartMock = ref(null);
  const lastGuideChangedCartMock = ref(null);
  const isGuideMock = ref(true);
  const isClientMock = ref(null);
  const appointmentMock = ref(null);
  const adminApiInstanceMock = ref(null);
  const getIncludesConfigMock = jest.fn();
  const getDefaultsMock = jest.fn();
  const getAssociationsConfigMock = jest.fn();

  // mockedCompositionAPI.onUnmounted = jest.fn();
  beforeEach(() => {
    jest.resetAllMocks();
    _storeCartMock.value = null;
    disabledCartCountMock.value = 0;
    product.value = {
      id: 'dummyProductId'
    } as any;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("cart"))
            return _storeCartMock;
          if (contextName.includes("disabledCartCount"))
            return disabledCartCountMock;
        },
      } as any;
    });
    mockedComposables.useDefaults.mockReturnValue({
      getIncludesConfig: getIncludesConfigMock,
      getAssociationsConfig: getAssociationsConfigMock,
      getDefaults: getDefaultsMock,
    });

    (mockedUseAppointment as any).mockImplementation(() => ({
      isGuide: isGuideMock,
      isClient: isClientMock,
      appointment: appointmentMock,
    } as any));

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    (mockedUseState as any).mockImplementation(() => ({
      currentClientForGuide: currentClientForGuideMock,
      currentValidToken: currentValidTokenMock,
      currentClientTokenPermissions: currentClientTokenPermissionsMock,
      getContextTokenForCurrentClient: getContextTokenForCurrentClientMock,
    } as any));

    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        guide: mercureGuidePublishMock,
      }
    } as any));

    (mockedUseMercureObserver as any).mockImplementation(() => ({
      lastClientChangedCart: lastClientChangedCartMock,
      lastGuideChangedCart: lastGuideChangedCartMock,
    } as any));

    (mockedUseAdminApiInstance as any).mockImplementation(() => ({
      adminApiInstance: adminApiInstanceMock,
    } as any));

    (mockedUseCustomerInsights as any).mockImplementation(() => ({
      attendeeData: attendeeDataMock,
      loadAttendeeData: loadAttendeeDataMock
    } as any));

    (mockedUseCartInsights as any).mockImplementation(() => ({
      loadCartInsights: loadCartInsightsMock
    } as any));

    mockedGSApiClient.CartService = {
      Guide: {
        getCart: getCartGuideMock,
        addLineItems: addLineItemsGuideMock,
        changeLineItemQuantities: changeLineItemQuantitiesGuideMock,
        removeLineItems: removeLineItemsGuideMock,
      },

      Client: {
        getCart: getCartClientMock,  
        addLineItems: addLineItemsClientMock,
        changeLineItemQuantities: changeLineItemQuantitiesClientMock,
        removeLineItems: removeLineItemsClientMock,
      }
    } as any;
    // mockedApiClient.getProducts = getProductsMock;
    mockedApiClient.getProducts.mockImplementation(getProductsMock);

    consoleErrorSpy.mockImplementationOnce(() => {});

    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
    }
  });

  describe("methods", () => {
    describe("requestTokenPermissions", () => {
      it("should return when attendeeId and currentClientForGuide.value is false", async () => {
        currentClientForGuideMock.value = '';

        const { requestTokenPermissions } = useGsCart();
        await requestTokenPermissions();
        expect(mercureGuidePublishMock).not.toHaveBeenCalled();
      });

      it("should call mercurePublishAllMock when attendeeId is true", async () => {
        currentClientForGuideMock.value = null;

        const { requestTokenPermissions } = useGsCart();
        await requestTokenPermissions("dummyAttendeeId");
        expect(mercureGuidePublishMock).toHaveBeenCalledWith('requestedTokenPermissions', 'dummyAttendeeId');
      });

      it("should call mercurePublishAllMock when currentClientForGuide.value is true", async () => {
        currentClientForGuideMock.value = 'dummyClientId';

        const { requestTokenPermissions } = useGsCart();
        await requestTokenPermissions();
        expect(mercureGuidePublishMock).toHaveBeenCalledWith('requestedTokenPermissions', 'dummyClientId');
      });
    });

    describe("refreshCart", () => {
      describe("as a Guide,", () => {
        beforeEach(() => {
          isGuideMock.value = true;
          isClientMock.value = false;
        });

        it("should stop correctly when currentClientForGuide.value invalid", async () => {
          currentClientForGuideMock.value = null;

          const { refreshCart } = useGsCart();
          await refreshCart();
          expect(loadCartInsightsMock).toHaveBeenCalled();
          expect(loadAttendeeDataMock).toHaveBeenCalled();
          expect(_storeCartMock.value).toEqual(null);
          expect(getContextTokenForCurrentClientMock).not.toHaveBeenCalled();
        });

        it("should stop correctly when currentClientForGuide.value valid but getContextTokenForCurrentClient return invalid value", async () => {
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue(null);
          attendeeDataMock.value = {
            attendees: {
              'dummy': {
                productCount: 5
              }
            }
          }
          const { disabledCartCount, refreshCart } = useGsCart();
          await refreshCart();

          expect(loadCartInsightsMock).toHaveBeenCalled();
          expect(loadAttendeeDataMock).toHaveBeenCalled();
          expect(_storeCartMock.value).toEqual(null);
          expect(getContextTokenForCurrentClientMock).toHaveBeenCalled();
          expect(disabledCartCount.value).toEqual(5);
        });

        it("should work correctly when currentClientForGuide.value valid & getContextTokenForCurrentClient return valid value", async () => {
          currentClientForGuideMock.value = 'dummy';
          appointmentMock.value = {
            salesChannelId: 'dummySaleChannelId'
          }
          getContextTokenForCurrentClientMock.mockResolvedValue('token');
          attendeeDataMock.value = {
            attendees: {
              'dummy': {
                productCount: 5
              }
            }
          }
          getCartGuideMock.mockResolvedValue({
            cartSum: 109.25,
            productCount: 2,
            currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
            currencySymbol: "£",
            topProducts: {
              byQuantity: [
              ],
              byRevenue: [
              ]
            },
            extensions: []
          });
          const { disabledCartCount, refreshCart } = useGsCart();
          await refreshCart();
          expect(loadCartInsightsMock).toHaveBeenCalled();
          expect(loadAttendeeDataMock).toHaveBeenCalled();
          expect(getContextTokenForCurrentClientMock).toHaveBeenCalled();
          expect(disabledCartCount.value).toEqual(0);
          expect(getCartGuideMock).toHaveBeenCalledWith(
            'token',
            'dummySaleChannelId',
            adminApiInstanceMock,
            expect.anything()
          );
          expect(_storeCartMock.value).toEqual({
            cartSum: 109.25,
            productCount: 2,
            currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
            currencySymbol: "£",
            topProducts: {
              byQuantity: [
              ],
              byRevenue: [
              ]
            },
            extensions: []
          });
        });

        it("should handle error correctly when API getCart failed", async () => {
          currentClientForGuideMock.value = 'dummy';
          appointmentMock.value = {
            salesChannelId: 'dummySaleChannelId'
          }
          getContextTokenForCurrentClientMock.mockResolvedValue('token');
          attendeeDataMock.value = {
            attendees: {
              'dummy': {
                productCount: 5
              }
            }
          };
          const err: ClientApiError = {
            statusCode: 400,
            message: 'dummy error message',
            messages: ['1', '2'] as any
          };

          getCartGuideMock.mockRejectedValue(err);

          const { refreshCart, error } = useGsCart();;

          await refreshCart();
          expect(_storeCartMock.value).toEqual(null);
          expect(error.value).toEqual(err.messages);
        });
      });

      describe("as a Client,", () => {
        beforeEach(() => {
          isGuideMock.value = false;
          isClientMock.value = true;
        });

        it("should call getCart client of cart service when isClient.value is true", async () => {
          getCartClientMock.mockResolvedValue({
            cartSum: 109.25,
            productCount: 2,
            currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
            currencySymbol: "£",
            topProducts: {
              byQuantity: [
              ],
              byRevenue: [
              ]
            },
            extensions: []
          });

          const { refreshCart } = useGsCart();;
          await refreshCart();
          expect(_storeCartMock.value).toEqual({
            cartSum: 109.25,
            productCount: 2,
            currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
            currencySymbol: "£",
            topProducts: {
              byQuantity: [
              ],
              byRevenue: [
              ]
            },
            extensions: []
          });
        });

        it("should handle error correctly when API getCart failed", async () => {
          const err: ClientApiError = {
            statusCode: 400,
            message: 'dummy error message',
            messages: ['1'] as any
          };

          getCartClientMock.mockRejectedValue(err);

          const { refreshCart, error } = useGsCart();;

          await refreshCart();
          expect(_storeCartMock.value).toEqual(null);
          expect(error.value).toEqual(err.messages);
        });
      });
    });

    describe("addProducts", () => {
      it("should return null when item is 0 value", async () => {
        const { addProducts } = useGsCart();
        const res = await addProducts([]);
        expect(res).toEqual(null);
        expect(_storeCartMock.value).toEqual(null);
      });

      describe("As a guide", () => {
        beforeEach(() => {
          isGuideMock.value = true;
          isClientMock.value = false;
        });

        it("should return undefined when currentClientForGuide invalid", async () => {
          addLineItemsGuideMock.mockResolvedValue({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          currentClientForGuideMock.value = null;
          const { addProducts } = useGsCart();
          const res = await addProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(res).toEqual(undefined);
          expect(_storeCartMock.value).toEqual(undefined);
          expect(addLineItemsGuideMock).not.toHaveBeenCalled();
        });

        it("should return undefined when currentClientForGuide valid & getContextTokenForCurrentClient return invalid value", async () => {
          addLineItemsGuideMock.mockResolvedValue({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue(null);

          const { addProducts } = useGsCart();
          const res = await addProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(res).toEqual(undefined);
          expect(_storeCartMock.value).toEqual(null);
          expect(addLineItemsGuideMock).not.toHaveBeenCalled();
        });

        it("should return result of guide cart service when currentClientForGuide valid & getContextTokenForCurrentClient return valid value", async () => {
          addLineItemsGuideMock.mockResolvedValue({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });

          appointmentMock.value = {
            salesChannelId: 'dummySaleChannelId'
          }
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue('token');

          const { addProducts } = useGsCart();
          const res = await addProducts([
            {
              id: "dummyId",
              quantity: undefined,
            }
          ]);
          expect(res).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(_storeCartMock.value).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(addLineItemsGuideMock).toHaveBeenCalledWith(
            'token', 'dummySaleChannelId', [{
              id: 'dummyId',
              quantity: 1,
            }], adminApiInstanceMock
          );
        });
      });

      describe("As a client", () => {
        beforeEach(() => {
          isGuideMock.value = false;
          isClientMock.value = true;
        });

        it("should return result of client service cart when item >= 1", async () => {
          addLineItemsClientMock.mockResolvedValue({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });

          const { addProducts } = useGsCart();
          const res = await addProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(_storeCartMock.value).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(res).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
        });
      });
    });

    describe("changeProducts", () => {
      it("should return null when item is 0 value", async () => {
        const { changeProducts } = useGsCart();
        const res = await changeProducts([]);
        expect(_storeCartMock.value).toEqual(null);
        expect(res).toEqual(null);
      });

      describe("as a Guide,", () => {
        beforeEach(() => {
          isGuideMock.value = true;
          isClientMock.value = false;
        });

        it("should return undefined when currentClientForGuide invalid", async () => {
          changeLineItemQuantitiesGuideMock.mockResolvedValue({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          currentClientForGuideMock.value = null;
          const { changeProducts } = useGsCart();
          const res = await changeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(res).toEqual(undefined);
          expect(_storeCartMock.value).toEqual(undefined);
          expect(changeLineItemQuantitiesGuideMock).not.toHaveBeenCalled();
        });

        it("should return undefined when currentClientForGuide valid & getContextTokenForCurrentClient return invalid value", async () => {
          changeLineItemQuantitiesGuideMock.mockResolvedValue({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue(null);

          const { changeProducts } = useGsCart();
          const res = await changeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(res).toEqual(undefined);
          expect(_storeCartMock.value).toEqual(null);
          expect(changeLineItemQuantitiesGuideMock).not.toHaveBeenCalled();
        });

        it("should return result of guide cart service when currentClientForGuide valid & getContextTokenForCurrentClient return valid value", async () => {
          changeLineItemQuantitiesGuideMock.mockResolvedValue({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });

          appointmentMock.value = {
            salesChannelId: 'dummySaleChannelId'
          }
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue('token');

          const { changeProducts } = useGsCart();
          await changeProducts([
            {
              id: "dummyId",
              quantity: 1,
            }
          ]);
          const res = await changeProducts([
            {
              id: "dummyId",
              quantity: 1,
            }
          ]);
          expect(res).toEqual({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(_storeCartMock.value).toEqual({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(changeLineItemQuantitiesGuideMock).toHaveBeenCalledWith(
            'token', 'dummySaleChannelId', [{
              id: 'dummyId',
              quantity: 1,
            }], adminApiInstanceMock,
            expect.anything()
          );
        });
      });

      describe("as a client,", () => {
        beforeEach(() => {
          isGuideMock.value = false;
          isClientMock.value = true;
        });

        it("should return result of client service cart when item >= 1", async () => {
          changeLineItemQuantitiesClientMock.mockResolvedValue({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });

          const { changeProducts } = useGsCart();
          await changeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          const res = await changeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(_storeCartMock.value).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(res).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
        });
      });
    });

    describe("removeProducts", () => {
      it("should return null when item is 0 value", async () => {
        const { removeProducts } = useGsCart();
        const res = await removeProducts([]);
        expect(_storeCartMock.value).toEqual(null);
        expect(res).toEqual(null);
      });

      describe("as a Guide,", () => {
        beforeEach(() => {
          isGuideMock.value = true;
          isClientMock.value = false;
        });

        it("should return undefined when currentClientForGuide invalid", async () => {
          removeLineItemsGuideMock.mockResolvedValue({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          currentClientForGuideMock.value = null;
          const { removeProducts } = useGsCart();
          const res = await removeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(res).toEqual(undefined);
          expect(_storeCartMock.value).toEqual(undefined);
          expect(removeLineItemsGuideMock).not.toHaveBeenCalled();
        });

        it("should return undefined when currentClientForGuide valid & getContextTokenForCurrentClient return invalid value", async () => {
          removeLineItemsGuideMock.mockResolvedValue({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue(null);

          const { removeProducts } = useGsCart();
          const res = await removeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(res).toEqual(undefined);
          expect(_storeCartMock.value).toEqual(null);
          expect(removeLineItemsGuideMock).not.toHaveBeenCalled();
        });

        it("should return result of guide cart service when currentClientForGuide valid & getContextTokenForCurrentClient return valid value", async () => {
          removeLineItemsGuideMock.mockResolvedValue({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });

          appointmentMock.value = {
            salesChannelId: 'dummySaleChannelId'
          }
          currentClientForGuideMock.value = 'dummy';
          getContextTokenForCurrentClientMock.mockResolvedValue('token');

          const { removeProducts } = useGsCart();
          const res = await removeProducts([
            {
              id: "dummyId",
              quantity: 1,
            }
          ]);
          expect(res).toEqual({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(_storeCartMock.value).toEqual({
            quatity: 2,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(removeLineItemsGuideMock).toHaveBeenCalledWith(
            'token', 'dummySaleChannelId', [{
              id: 'dummyId',
              quantity: 1,
            }], adminApiInstanceMock,
          );
        });
      });

      describe("as a client,", () => {
        beforeEach(() => {
          isGuideMock.value = false;
          isClientMock.value = true;
        });

        it("should return result of client service cart when item >= 1", async () => {
          removeLineItemsClientMock.mockResolvedValue({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });

          const { removeProducts } = useGsCart();
          const res = await removeProducts([
            {
              id: "dummyId",
              quantity: 2,
            }
          ]);
          expect(_storeCartMock.value).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
          expect(res).toEqual({
            quatity: 1,
            type: "product",
            referencedId: "dummyreferencedId",
            id: "dummyId"
          });
        });
      });
    });

    describe("getProductItemsSeoUrlsData", () => {
      it("should return empty array when cartItems.value.length is false", async () => {
        _storeCartMock.value = {
          lineItems: []
        } as any;
        const { getProductItemsSeoUrlsData } = useGsCart();
        const res = await getProductItemsSeoUrlsData();
        expect(res).toEqual([]);
        expect(getProductsMock).not.toHaveBeenCalled();
      });

      it("should return correctly when calling getProducts API successfully", async () => {
        getDefaultsMock.mockReturnValue({
          getProductItemsSeoUrlsData: {
            includes: {
              product: ["id", "seoUrls"],
            },
            associations: {
              seoUrls: {}
            }
          }
        })
        _storeCartMock.value = {
          lineItems: [{
            id: '1',
            referencedId: '1' ,
          }]
        } as any;
        getProductsMock.mockResolvedValue({
          elements: [
            {
              translated: {
                name: "translated Sunset One Pocket Slim Fit Shirt"
              },
              name: "Sunset One Pocket Slim Fit Shirt",
              id: "0096740cecc3491288793afb4c9bd364",
            },
            {
              translated: {
                name: "translated Levis X-star Wars Chewbacca Tee Teenager"
              },
              name: "Levis X-star Wars Chewbacca Tee Teenager",
              id: "00404d6f11964290b1f83a11ca5b0ee1",
            },
          ],
        } as any);
        const { getProductItemsSeoUrlsData } = useGsCart();
        const res = await getProductItemsSeoUrlsData();

        expect(getProductsMock).toHaveBeenCalledWith({
          ids: ['1'],
          includes: {
            product: ["id", "seoUrls"],
          },
          associations: {
            seoUrls: {}
          }
        }, rootContextMock.apiInstance);
        expect(res).toEqual([
          {
            translated: {
              name: "translated Sunset One Pocket Slim Fit Shirt"
            },
            name: "Sunset One Pocket Slim Fit Shirt",
            id: "0096740cecc3491288793afb4c9bd364",
          },
          {
            translated: {
              name: "translated Levis X-star Wars Chewbacca Tee Teenager"
            },
            name: "Levis X-star Wars Chewbacca Tee Teenager",
            id: "00404d6f11964290b1f83a11ca5b0ee1",
          },
        ])
      });

      it("should return correctly when calling getProducts API failed", async () => {
        getDefaultsMock.mockReturnValue({
          getProductItemsSeoUrlsData: {
            includes: {
              product: ["id", "seoUrls"],
            },
            associations: {
              seoUrls: {}
            }
          }
        })
        _storeCartMock.value = {
          lineItems: [{
            id: '1',
            referencedId: '1' ,
          }]
        } as any;
        const err: ClientApiError = {
          statusCode: 400,
          message: 'dummy error message',
          messages: ['1', '2'] as any
        };
        getProductsMock.mockRejectedValue(err);
        const { getProductItemsSeoUrlsData } = useGsCart();
        const res = await getProductItemsSeoUrlsData();

        expect(res).toEqual([]);
        expect(consoleErrorSpy).toHaveBeenCalledWith('[useGsCart][getProductItemsSeoUrlsData]', ['1', '2'])
      });
    });

    describe("initCartForClient", () => {
      beforeEach(() => {
        isGuideMock.value = false;
        isClientMock.value = true;
      });

      it("should work correctly", async () => {
        lastGuideChangedCartMock.value = '';
        appointmentMock.value = {
          attendeeId: '1'
        }
        getCartClientMock.mockResolvedValue({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });

        const [result, app] = mockLoadComposableInApp(() => useGsCart());

        await result.initCartForClient();
        await result.initCartForClient();
        lastGuideChangedCartMock.value = '1';
        await jest.setTimeout(100);
        expect(lastGuideChangedCartMock.value).toEqual('');
        expect(_storeCartMock.value).toEqual({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });
      });
    });

    describe("initCartForGuide", () => {
      beforeEach(() => {
        isGuideMock.value = true;
        isClientMock.value = false;
      });

      it("should work correctly when currentValidToken changed", async () => {
        currentValidTokenMock.value = null;
        currentClientForGuideMock.value = 'dummy';
        appointmentMock.value = {
          salesChannelId: 'dummySaleChannelId'
        }
        getContextTokenForCurrentClientMock.mockResolvedValue('token');
        attendeeDataMock.value = {
          attendees: {
            'dummy': {
              productCount: 5
            }
          }
        }
        getCartGuideMock.mockResolvedValue({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });

        const [result, app] = mockLoadComposableInApp(() => useGsCart());
        await result.initCartForGuide();
        await result.initCartForGuide();
        currentValidTokenMock.value = '111';
        await jest.setTimeout(100);
        expect(loadCartInsightsMock).toHaveBeenCalled();
        expect(loadAttendeeDataMock).toHaveBeenCalled();
        expect(getContextTokenForCurrentClientMock).toHaveBeenCalled();
        expect(result.disabledCartCount.value).toEqual(0);
        expect(getCartGuideMock).toHaveBeenCalledWith(
          'token',
          'dummySaleChannelId',
          adminApiInstanceMock,
          expect.anything()
        );
        expect(_storeCartMock.value).toEqual({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });
      });

      it("should work correctly when lastClientChangedCart changed", async () => {
        lastClientChangedCartMock.value = '';
        currentClientForGuideMock.value = 'dummy';
        appointmentMock.value = {
          salesChannelId: 'dummySaleChannelId'
        }
        getContextTokenForCurrentClientMock.mockResolvedValue('token');
        attendeeDataMock.value = {
          attendees: {
            'dummy': {
              productCount: 5
            }
          }
        }
        getCartGuideMock.mockResolvedValue({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });

        const [result, app] = mockLoadComposableInApp(() => useGsCart());
        await result.initCartForGuide();
        await result.initCartForGuide();
        lastClientChangedCartMock.value = '111';
        await jest.setTimeout(100);
        expect(loadCartInsightsMock).toHaveBeenCalled();
        expect(loadAttendeeDataMock).toHaveBeenCalled();
        expect(getContextTokenForCurrentClientMock).toHaveBeenCalled();
        expect(result.disabledCartCount.value).toEqual(0);
        expect(lastClientChangedCartMock.value).toEqual('');
        expect(getCartGuideMock).toHaveBeenCalledWith(
          'token',
          'dummySaleChannelId',
          adminApiInstanceMock,
          expect.anything()
        );
        expect(_storeCartMock.value).toEqual({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });
      });

      it("should work correctly when lastGuideChangedCart changed & currentClientForGuide same with lastGuideChangedCart", async () => {
        lastGuideChangedCartMock.value = '';
        currentClientForGuideMock.value = 'dummy';
        appointmentMock.value = {
          salesChannelId: 'dummySaleChannelId'
        }
        getContextTokenForCurrentClientMock.mockResolvedValue('token');
        attendeeDataMock.value = {
          attendees: {
            'dummy': {
              productCount: 5
            }
          }
        }
        getCartGuideMock.mockResolvedValue({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });

        const [result, app] = mockLoadComposableInApp(() => useGsCart());
        await result.initCartForGuide();
        await result.initCartForGuide();
        lastGuideChangedCartMock.value = 'dummy';
        await jest.setTimeout(100);
        expect(loadCartInsightsMock).toHaveBeenCalled();
        expect(loadAttendeeDataMock).toHaveBeenCalled();
        expect(getContextTokenForCurrentClientMock).toHaveBeenCalled();
        expect(result.disabledCartCount.value).toEqual(0);
        expect(lastGuideChangedCartMock.value).toEqual('');
        expect(getCartGuideMock).toHaveBeenCalledWith(
          'token',
          'dummySaleChannelId',
          adminApiInstanceMock,
          expect.anything()
        );
        expect(_storeCartMock.value).toEqual({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });
      });

      it("should not run when lastGuideChangedCart changed & currentClientForGuide different with lastGuideChangedCart", async () => {
        lastGuideChangedCartMock.value = '';
        currentClientForGuideMock.value = null;

        const [result, app] = mockLoadComposableInApp(() => useGsCart());
        await result.initCartForGuide();
        await result.initCartForGuide();
        lastGuideChangedCartMock.value = '111';
        await jest.setTimeout(100);
        expect(loadCartInsightsMock).toHaveBeenCalledTimes(1);
        expect(loadAttendeeDataMock).toHaveBeenCalledTimes(1);
      });

    });


  });

  describe("computed", () => {
    describe("appliedPromotionCodes", () => {
      it("should return cartItem type === promotion", () => {
        _storeCartMock.value = {
          lineItems: [{
            id: '1',
            referencedId: '1',
            type: 'promotion'
          }, {
            id: '2',
            referencedId: '2',
            type: 'product'
          }]
        } as any;
        const { appliedPromotionCodes } = useGsCart();
        expect(appliedPromotionCodes.value).toEqual([{
          id: '1',
          referencedId: '1',
          type: 'promotion'
        }]);
      });
    });

    describe("cart", () => {
      it("should return _storeCart.value", () => {
        _storeCartMock.value = {
          lineItems: [{
            id: '1',
            referencedId: '1',
            quantity: 2,
            type: 'promotion'
          }, {
            id: '2',
            referencedId: '2',
            quantity: 2,
            type: 'product'
          },
          {
            id: '3',
            referencedId: '3',
            quantity: 2,
            type: 'product'
          }]
        } as any;
        const { cart } = useGsCart();
        expect(cart.value).toEqual({
          lineItems: [{
            id: '1',
            referencedId: '1',
            quantity: 2,
            type: 'promotion'
          }, {
            id: '2',
            referencedId: '2',
            quantity: 2,
            type: 'product'
          },
          {
            id: '3',
            referencedId: '3',
            quantity: 2,
            type: 'product'
          }]
        });
      });
    });

    describe("cartItems", () => {
      it("should return value when cart.value is true", () => {
        _storeCartMock.value = {
          lineItems: [{
            id: '1',
            referencedId: '1',
            quantity: 2,
            type: 'promotion'
          }, {
            id: '2',
            referencedId: '2',
            quantity: 2,
            type: 'product'
          },
          {
            id: '3',
            referencedId: '3',
            quantity: 2,
            type: 'product'
          }]
        } as any;
        const { cartItems } = useGsCart();
        expect(cartItems.value).toEqual([{
          id: '1',
          referencedId: '1',
          quantity: 2,
          type: 'promotion'
        }, {
          id: '2',
          referencedId: '2',
          quantity: 2,
          type: 'product'
        },
        {
          id: '3',
          referencedId: '3',
          quantity: 2,
          type: 'product'
        }]);
      });
    });

    describe("count", () => {
      it("should return sum lineItem.quantity and accumulator when lineItem.type is product", () => {
        _storeCartMock.value = {
          lineItems: [{
            id: '1',
            referencedId: '1',
            quantity: 2,
            type: 'promotion'
          }, {
            id: '2',
            referencedId: '2',
            quantity: 2,
            type: 'product'
          },
          {
            id: '3',
            referencedId: '3',
            quantity: 2,
            type: 'product'
          }]
        } as any;
        const { count } = useGsCart();
        expect(count.value).toEqual(4);
      });

      it("should return accumulator number when lineItem.type isn't product", () => {
        const { cartItems } = useGsCart();
        expect(cartItems.value).toEqual([]);
      });
    });

    describe("totalPrice", () => {
      it("should return 0 when one of cart.value, cart.value.price, cart.value.price.totalPrice is false", () => {
        _storeCartMock.value = null;
        const { totalPrice } = useGsCart();
        expect(totalPrice.value).toEqual(0);
      });

      it("should return lastest number of list cart.value, cart.value.price, cart.value.price.totalPrice", () => {
        _storeCartMock.value = {
          price: {
            netPrice: 1,
            totalPrice: 200,
            calculatedTaxes: [
              {
                tax: 3.19,
                taxRate: 19,
                price: 20,
              },
            ],
            taxRules: [
              { taxRate: 19, percentage: 100 },
            ],
            positionPrice: 1,
            taxStatus: 'dummyTaxStatus'
          },
        } as any;
        const { totalPrice } = useGsCart();
        expect(totalPrice.value).toEqual(200);
      });

      it("should return net price when taxStatus is net", () => {
        _storeCartMock.value = {
          price: {
            netPrice: 1,
            totalPrice: 200,
            calculatedTaxes: [
              {
                tax: 3.19,
                taxRate: 19,
                price: 20,
              },
            ],
            taxRules: [
              { taxRate: 19, percentage: 100 },
            ],
            positionPrice: 1,
            taxStatus: 'net'
          },
        } as any;
        const { totalPrice } = useGsCart();
        expect(totalPrice.value).toEqual(1);
      });

      it("should return net price when taxStatus is tax-free", () => {
        _storeCartMock.value = {
          price: {
            netPrice: 1,
            totalPrice: 200,
            calculatedTaxes: [
              {
                tax: 3.19,
                taxRate: 19,
                price: 20,
              },
            ],
            taxRules: [
              { taxRate: 19, percentage: 100 },
            ],
            positionPrice: 1,
            taxStatus: 'tax-free'
          },
        } as any;
        const { totalPrice } = useGsCart();
        expect(totalPrice.value).toEqual(1);
      });
    });

    describe("shippingTotal", () => {
      it("should return 0 when shippingTotal don't have value", () => {
        _storeCartMock.value = null;
        const { shippingTotal } = useGsCart();
        expect(shippingTotal.value).toEqual(0);
      });

      it("should return shippingTotal value", () => {
        _storeCartMock.value = {
          deliveries: [
            {
              shippingCosts: {
                unitPrice: 1,
                quantity: 1,
                listPrice: 1,
                apiAlias: "dummyapiAlias",
                totalPrice: 100,
              },
            }
          ],
        } as any
        const { shippingTotal } = useGsCart();
        expect(shippingTotal.value).toEqual(100);
      });
    });

    describe("subtotal", () => {
      it("should return 0 when subtotal don't have value", () => {
        _storeCartMock.value = null;
        const { subtotal } = useGsCart();
        expect(subtotal.value).toEqual(0);
      });

      it("should return subtotal value", () => {
        _storeCartMock.value = {
          price: {
            netPrice: 1,
            totalPrice: 2,
            calculatedTaxes: [
              {
                tax: 3.19,
                taxRate: 19,
                price: 20,
              },
            ],
            taxRules: [
              { taxRate: 19, percentage: 100 },
            ],
            positionPrice: 100,
            taxStatus: 'dummyTaxStatus'
          },
        } as any

        const { subtotal } = useGsCart();
        expect(subtotal.value).toEqual(100);
      });
    });

    describe("cartErrors", () => {
      it("should return empty array when errors don't have value", () => {
        _storeCartMock.value = null;
        const { cartErrors } = useGsCart();
        expect(cartErrors.value).toEqual([]);
      });

      it("should return subtotal value", () => {
        _storeCartMock.value = {
          errors: {
            test1: {
              id: '11',
              name: '11',
            },
            test2: {
              id: '22',
              name: '22',
            }
          },
        } as any

        const { cartErrors } = useGsCart();
        expect(cartErrors.value).toEqual([
          {
            id: '11',
            name: '11',
          },
          {
            id: '22',
            name: '22',
          },
        ]);
      });
    });

  });
});
