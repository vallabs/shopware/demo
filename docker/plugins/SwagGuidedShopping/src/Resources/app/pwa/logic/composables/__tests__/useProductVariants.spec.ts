import { ref } from "vue-demi";
import { Product, SearchFilterType } from "@shopware-pwa/commons";

// Mock API client
import * as shopwareClient from "@shopware-pwa/shopware-6-client";
jest.mock("@shopware-pwa/shopware-6-client");
const mockedApiClient = shopwareClient as jest.Mocked<typeof shopwareClient>;

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
import { useProductVariants } from "../widgets/useProductVariants";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

describe("Composables - useProductVariants", () => {
  const product = ref<any>({
    id: 'dummyProductId',
  })
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();

  const _storeProductVariants = ref<Product[]>(null);
  const _storeProductMainVariantsProducts = ref<Product[]>(null);

  beforeEach(() => {
    jest.resetAllMocks();
    _storeProductVariants.value = null;
    _storeProductMainVariantsProducts.value = null;
    product.value = {
      id: 'dummyProductId'
    } as any;

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("productVariants"))
            return _storeProductVariants;
          if (contextName.includes("mainVariantsProducts"))
            return _storeProductMainVariantsProducts;
        },
      } as any;
    });

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("loadProductVariants", () => {
      it("should receive correct productVariantResponse", async () => {
        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: [
            {
              calculatedPrices: [],
              calculatedPrice: {
                unitPrice: 20,
                quantity: 1,
                totalPrice: 20,
                calculatedTaxes: [
                  {
                    tax: 3.19,
                    taxRate: 19,
                    price: 20,
                  },
                ],
                taxRules: [
                  { taxRate: 19, percentage: 100 },
                ],
                referencePrice: null,
                listPrice: null,
              },
              stock: 14,
              availableStock: 4,
              optionIds: null,
              name: "93 Shorts",
              options: null,
              id: "dummyMainVariantId",
            },
          ],
        } as any);
        const { loadProductVariants } = useProductVariants(product.value);
        await loadProductVariants();
        expect(mockedApiClient.getProducts).toBeCalledWith(
          {
            includes: {
              product: [
                "id",
                "name",
                "options",
                "availableStock",
                "calculatedPrice",
                "calculatedPrices",
              ],
            },
            associations: {
              options: {},
            },
            filter: [
              {
                type: SearchFilterType.EQUALS,
                field: 'parentId',
                value: 'dummyProductId'
              },
            ],
          },
          expect.any(Function)
        );
        expect(_storeProductVariants.value).toEqual([{
          calculatedPrices: [],
          calculatedPrice: {
            unitPrice: 20,
            quantity: 1,
            totalPrice: 20,
            calculatedTaxes: [
              {
                tax: 3.19,
                taxRate: 19,
                price: 20,
              },
            ],
            taxRules: [
              { taxRate: 19, percentage: 100 },
            ],
            referencePrice: null,
            listPrice: null,
          },
          stock: 14,
          availableStock: 4,
          optionIds: null,
          name: "93 Shorts",
          options: null,
          id: "dummyMainVariantId",
        }]);
      });

      it("should send correct error when call API failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedApiClient.getProducts.mockRejectedValueOnce(res as any);
        const { loadProductVariants } = useProductVariants(product.value);
        await loadProductVariants();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: 'useProductVariants',
            method: "loadProductVariants",
          }
        );
      });
    });

    describe("loadMainVariantsProducts", () => {
      it("should return when mainVariantId not existed", async () => {
        product.value = {
          id: 'dummyProductId',
        }
        const { loadMainVariantsProducts } = useProductVariants(product.value);
        const res = await loadMainVariantsProducts();
        expect(res).toBeUndefined();
      });

      it("should return when product not existed", async () => {
        product.value = null
        const { loadMainVariantsProducts } = useProductVariants(product.value);
        const res = await loadMainVariantsProducts();
        expect(res).toBeUndefined();
      });

      it("should receive correct productVariantResponse", async () => {
        product.value = {
          id: 'dummyProductId',
          mainVariantId: 'dummyMainVariantId'
        }
        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: [
            {
              calculatedPrices: [],
              calculatedPrice: {
                unitPrice: 20,
                quantity: 1,
                totalPrice: 20,
                calculatedTaxes: [
                  {
                    tax: 3.19,
                    taxRate: 19,
                    price: 20,
                  },
                ],
                taxRules: [
                  { taxRate: 19, percentage: 100 },
                ],
                referencePrice: null,
                listPrice: null,
              },
              stock: 14,
              availableStock: 4,
              optionIds: null,
              name: "93 Shorts",
              options: null,
              id: "dummyMainVariantId",
            },
          ],
        } as any);
        const { loadMainVariantsProducts } = useProductVariants(product.value);
        await loadMainVariantsProducts();
        expect(mockedApiClient.getProducts).toBeCalledWith(
          { ids: ['dummyMainVariantId'] },
          expect.any(Function)
        );
        expect(_storeProductMainVariantsProducts.value).toEqual([{
          calculatedPrices: [],
          calculatedPrice: {
            unitPrice: 20,
            quantity: 1,
            totalPrice: 20,
            calculatedTaxes: [
              {
                tax: 3.19,
                taxRate: 19,
                price: 20,
              },
            ],
            taxRules: [
              { taxRate: 19, percentage: 100 },
            ],
            referencePrice: null,
            listPrice: null,
          },
          stock: 14,
          availableStock: 4,
          optionIds: null,
          name: "93 Shorts",
          options: null,
          id: "dummyMainVariantId",
        }]);
      });

      it("should send correct error when call API failed", async () => {
        product.value = {
          id: 'dummyProductId',
          mainVariantId: 'dummyMainVariantId'
        };
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };
        mockedApiClient.getProducts.mockRejectedValueOnce(res as any);
        const { loadMainVariantsProducts } = useProductVariants(product.value);
        await loadMainVariantsProducts();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: 'useProductVariants',
            method: 'loadMainVariantsProducts',
          }
        );
      });
    });


  });

  describe("computed", () => {
    describe("productVariants", () => {
      it("should return _storeProductVariants value", async () => {
        const dummyElements = [
          {
            calculatedPrices: [],
            calculatedPrice: {
              unitPrice: 20,
              quantity: 1,
              totalPrice: 20,
              calculatedTaxes: [
                {
                  tax: 3.19,
                  taxRate: 19,
                  price: 20,
                },
              ],
              taxRules: [
                { taxRate: 19, percentage: 100 },
              ],
              referencePrice: null,
              listPrice: null,
            },
            stock: 14,
            availableStock: 4,
            optionIds: null,
            name: "93 Shorts",
            options: null,
            id: "dummyMainVariantId",
          },
        ];
        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: dummyElements,
        } as any);
        const { loadProductVariants, productVariants } = useProductVariants(product.value);
        await loadProductVariants();
        expect(productVariants.value).toEqual(dummyElements);
      });
    });

    describe("productVariantsCount", () => {
      it("should return undefined when _storeProductVariants not existed", async () => {
        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: null,
        } as any);
        const { loadProductVariants, productVariantsCount } = useProductVariants(product.value);
        await loadProductVariants();
        expect(productVariantsCount.value).toBeUndefined();
      });
      it("should return _storeProductVariants length", async () => {
        const dummyElements = [
          {
            calculatedPrices: [],
            calculatedPrice: {
              unitPrice: 20,
              quantity: 1,
              totalPrice: 20,
              calculatedTaxes: [
                {
                  tax: 3.19,
                  taxRate: 19,
                  price: 20,
                },
              ],
              taxRules: [
                { taxRate: 19, percentage: 100 },
              ],
              referencePrice: null,
              listPrice: null,
            },
            stock: 14,
            availableStock: 4,
            optionIds: null,
            name: "93 Shorts",
            options: null,
            id: "dummyMainVariantId",
          },
        ];
        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: dummyElements,
        } as any);
        const { loadProductVariants, productVariantsCount } = useProductVariants(product.value);
        await loadProductVariants();
        expect(productVariantsCount.value).toEqual(dummyElements.length);
      });
    });

    describe("mainVariantsProducts", () => {
      it("should return _storeProductMainVariantsProducts value", async () => {
        product.value = {
          id: 'dummyProductId',
          mainVariantId: 'dummyMainVariantId'
        };
        const dummyElements = [
          {
            calculatedPrices: [],
            calculatedPrice: {
              unitPrice: 20,
              quantity: 1,
              totalPrice: 20,
              calculatedTaxes: [
                {
                  tax: 3.19,
                  taxRate: 19,
                  price: 20,
                },
              ],
              taxRules: [
                { taxRate: 19, percentage: 100 },
              ],
              referencePrice: null,
              listPrice: null,
            },
            stock: 14,
            availableStock: 4,
            optionIds: null,
            name: "93 Shorts",
            options: null,
            id: "dummyMainVariantId",
          },
        ];
        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: dummyElements,
        } as any);
        const { loadMainVariantsProducts, mainVariantsProducts } = useProductVariants(product.value);
        await loadMainVariantsProducts();
        expect(mainVariantsProducts.value).toEqual(dummyElements);
      });
    });
  });
});
