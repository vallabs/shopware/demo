import {useNotifications} from '@shopware-pwa/composables';
import {ClientApiError} from '../interfaces';

let IS_DEV:boolean = null;
const DEFAULT_PUSH_TIMEOUT:number = 4000;

export const useErrorHandler = () => {
  const {pushError} = useNotifications();
  
  try {
    IS_DEV = process.env.NODE_ENV === 'development';
  } catch (e) {
    IS_DEV = false;
  }

  function logError(content: string, options: {
    context: string,
    method?: string,
    push?:boolean,
    timeout?: number,
  }) {
    if (options.context && options.method) {
      console.error(`[${options.context}][${options.method}] ${content}`);
    } else if (options.context) {
      console.error(`[${options.context}] ${content}`);
    } else {
      console.error(content);
    }

    if (options.push || IS_DEV) {
      if (!options.timeout) {
        options.timeout = DEFAULT_PUSH_TIMEOUT;
      }
      pushError(content, {timeout: options.timeout});
    }
  }

  function handleError(error: ClientApiError, options: {
    context: string,
    method?: string,
    push?: boolean,
    timeout?: number,
    asGuide?: boolean,
  }) {
    if (error.message) {
      const code = error.statusCode ? `(${error.statusCode})` : '';
      logError(`${code} ${error.message}`, options);
    } else if (error.messages) {
      error.messages.forEach(err => {
        const title = (err.title !== '') ? `${err.title}` : '';
        const status = (err.status !== '') ? ` (${err.status})` : '';
        const code = (err.code !== '') ? `(${err.code}) ` : '';
        const detail = (err.detail !== '') ? `${err.detail}` : '';
        logError(`${title}${status}: ${code}${detail}`, options);
      });
    }
  }

  return {
    handleError,
  };
};
