export interface OrderLineItem {
  apiAlias: string,
  id: string,
  productId: string,
  type: string;
}
