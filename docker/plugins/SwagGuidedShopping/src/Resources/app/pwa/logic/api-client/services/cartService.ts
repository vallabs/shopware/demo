import { Cart } from "@shopware-pwa/commons/interfaces/models/checkout/cart/Cart";
import { ShopwareAdminApiInstance, CartInsights } from '../../interfaces';
import {
  getGuideCartEndpoint,
  getGuideCartLineItemEndpoint,
  getCartInsightsEndpoint,
} from '../endpoints';
import { LineItem } from "@shopware-pwa/commons/interfaces/models/checkout/cart/line-item/LineItem";
import { ShopwareApiInstance, getCheckoutCartEndpoint, getCheckoutCartLineItemEndpoint } from "@shopware-pwa/shopware-6-client";

type MinimumLineItem = {
  id: string,
  quantity?: number,
}

const createClientHeader = (token, data = null) => {
  return {
    'headers': {
      'sw-context-token': token
    },
    data,
  }
};

export const Client = {

  /**
   * fetch cart
   * @param adminApiInstance
   */
  async getCart(adminApiInstance: ShopwareAdminApiInstance, controller?: AbortController): Promise<Cart> {
    const control = controller ? { signal: controller.signal } : {};

    const result = await adminApiInstance.invoke.get(getCheckoutCartEndpoint(), control);
    return result.data;
  },

  /**
   * add line items
   *
   * @param itemsToAdd
   * @param apiInstance
   */
  async addLineItems(itemsToAdd: MinimumLineItem[], apiInstance: ShopwareApiInstance): Promise<Cart> {
    const items = itemsToAdd.map(product => {
      const item: Partial<LineItem> = {
        quantity: product.quantity,
        type: "product",
        referencedId: product.id,
        id: product.id,
      };
      return item;
    });

    const result = await apiInstance.invoke.post(getCheckoutCartLineItemEndpoint(), { items });
    return result.data;
  },

  /**
   * update quantities
   *
   * @param itemsToUpdate
   * @param apiInstance
   */
  async changeLineItemQuantities(itemsToUpdate: MinimumLineItem[], apiInstance: ShopwareApiInstance, controller?: AbortController): Promise<Cart> {
    const items = itemsToUpdate.map(product => {
      const item: Partial<LineItem> = {
        quantity: parseInt(product.quantity.toString(), 10),
        id: product.id,
      };
      return item;
    });

    const control = controller ? { signal: controller.signal } : {};

    const result = await apiInstance.invoke.patch(getCheckoutCartLineItemEndpoint(), { items }, control);
    return result.data;
  },

  /**
   * remove line items
   *
   * @param itemsToRemove
   * @param apiInstance
   */
  async removeLineItems(itemsToRemove: MinimumLineItem[], apiInstance: ShopwareApiInstance): Promise<Cart> {
    const ids = itemsToRemove.map(item => item.id);

    const result = await apiInstance.invoke.delete(`${getCheckoutCartLineItemEndpoint()}`, { data: { ids } });
    return result.data;
  }

};


export const Guide = {

  /**
   * fetch cart of client
   *
   * @param clientToken
   * @param salesChannelId
   * @param adminApiInstance
   */
  async getCart(
    clientToken: string,
    salesChannelId: string,
    adminApiInstance: ShopwareAdminApiInstance,
    controller?: AbortController
  ): Promise<Cart> {
    const control = controller ? { signal: controller.signal } : {};
    
    const result = await adminApiInstance.invoke.get(getGuideCartEndpoint(salesChannelId), {
      ...createClientHeader(clientToken),
      ...control
    });
    return result.data;
  },

  /**
   * add line items for client
   *
   * @param clientToken
   * @param salesChannelId
   * @param itemsToAdd
   * @param adminApiInstance
   */
  async addLineItems(
    clientToken: string,
    salesChannelId: string,
    itemsToAdd: MinimumLineItem[],
    adminApiInstance: ShopwareAdminApiInstance
  ): Promise<Cart> {
    const items = itemsToAdd.map(product => {
      const item: Partial<LineItem> = {
        quantity: product.quantity,
        type: "product",
        referencedId: product.id,
        id: product.id,
      };
      return item;
    });

    const result = await adminApiInstance.invoke.post(getGuideCartLineItemEndpoint(salesChannelId), { items }, createClientHeader(clientToken));
    return result.data;
  },

  /**
   * change line item quantities for client
   *
   * @param clientToken
   * @param salesChannelId
   * @param itemsToUpdate
   * @param adminApiInstance
   */
  async changeLineItemQuantities(
    clientToken: string,
    salesChannelId: string,
    itemsToUpdate: MinimumLineItem[],
    adminApiInstance: ShopwareAdminApiInstance,
    controller?: AbortController
  ): Promise<Cart> {
    const items = itemsToUpdate.map(product => {
      const item: Partial<LineItem> = {
        quantity: parseInt(product.quantity.toString(), 10),
        id: product.id,
      };
      return item;
    });

    const control = controller ? { signal: controller.signal } : {};

    const result = await adminApiInstance.invoke.patch(getGuideCartLineItemEndpoint(salesChannelId), { items }, {
      ...createClientHeader(clientToken),
      ...control
    });
    return result.data;
  },

  /**
   * remove line items for client
   *
   * @param clientToken
   * @param salesChannelId
   * @param itemsToRemove
   * @param adminApiInstance
   */
  async removeLineItems(
    clientToken: string,
    salesChannelId: string,
    itemsToRemove: MinimumLineItem[],
    adminApiInstance: ShopwareAdminApiInstance
  ): Promise<Cart> {
    const ids = itemsToRemove.map(item => item.id);
    const result = await adminApiInstance.invoke.delete(`${getGuideCartLineItemEndpoint(salesChannelId)}`, createClientHeader(clientToken, { ids }));
    return result.data;
  }

};


export async function getCartInsights(
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<CartInsights> {
  const resp = await adminApiInstance.invoke.get(
    getCartInsightsEndpoint(appointmentId)
  );

  return resp.data;
}
