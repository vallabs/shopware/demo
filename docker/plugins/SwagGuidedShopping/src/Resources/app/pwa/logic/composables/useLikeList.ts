import {getApplicationContext, useSharedState} from '@shopware-pwa/composables';
import {computed, ref, Ref, watch} from "@vue/composition-api"
import {LikeListService} from '../api-client';
import {Product} from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import {useInteractions} from './useInteractions';
import {useErrorHandler} from './useErrorHandler';
import {useAppointment} from "./useAppointment";
import {useAdminApiInstance} from "./useAdminApiInstance";
import {useState} from "./useState";
import {useMercure} from "./useMercure";
import {useMercureObserver} from "./useMercureObserver";
import {useDynamicPage} from "./useDynamicPage";
import {useCustomerInsights} from "./widgets/useCustomerInsights";
import {useWatchers} from './useWatchers';

const COMPOSABLE_NAME = 'useLikeList';

export const useLikeList = (product?: Product) => {
  const contextName = COMPOSABLE_NAME;
  const {apiInstance} = getApplicationContext({contextName});
  const productId = ref<string>(product?.id);
  const initialized: Ref<boolean> = ref(false);
  const {sharedRef} = useSharedState();
  const {publishInteraction} = useInteractions();
  const {handleError} = useErrorHandler();
  const {isGuide, isClient, appointment} = useAppointment();
  const {adminApiInstance} = useAdminApiInstance();
  const {currentClientForGuide} = useState();
  const {addWatcher} = useWatchers();
  const {mercurePublish} = useMercure();
  const {lastClientChangedLikes, likedProduct, lastGuideChangedLikes} = useMercureObserver();
  const {refreshCustomerData: refreshCustomerWidgetData} = useCustomerInsights();
  const {dynamicPage} = useDynamicPage();
  const _storeLikeList = sharedRef<Product[]>(`${contextName}-likeList`, []);
  const _storeGuideLikeList = sharedRef<Product[]>(`${contextName}-guideLikeList`, []);
  const _storeDislikeList = sharedRef<Product[]>(`${contextName}-dislikeList`, []);
  const _storeAllProductLikesCount = ref<Product[]>([]);
  const _storeAllProductDislikesCount = ref<Product[]>([]);
  const loading = ref<boolean>(false);
  const toggleLikeLoading = ref<boolean>(false);
  

  function changeProduct(productValue: Product) {
    if (productValue) {
      productId.value = productValue.id;
    }
  }

  async function like() {
    if (loading.value) return;

    try {
      loading.value = true;

      if (isGuide.value && currentClientForGuide.value) {
        await LikeListService.addLikedProductForAttendee(productId.value, currentClientForGuide.value, adminApiInstance);
        await Promise.all([
          mercurePublish.all('guide.changedLikes', currentClientForGuide.value),
          mercurePublish.all('product.wasLiked', productId.value),
          publishInteraction({
            name: 'attendee.product.collection.liked',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      } else if (isClient.value) {
        await LikeListService.addLikedProduct(productId.value, apiInstance);
        await Promise.all([
          mercurePublish.all('client.changedLikes', appointment.value.attendeeId),
          mercurePublish.all('product.wasLiked', productId.value),
          loadLikeAndDislikeList(),
          publishInteraction({
            name: 'attendee.product.collection.liked',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      }
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'like',
      });
    } finally {
      loading.value = false;
    }
  }

  async function dislike() {
    if (loading.value) return;

    try {
      loading.value = true;

      if (isGuide.value && currentClientForGuide.value) {
        await LikeListService.addDislikedProductForAttendee(productId.value, currentClientForGuide.value, adminApiInstance);
        await Promise.all([
          mercurePublish.all('guide.changedLikes', currentClientForGuide.value),
          mercurePublish.all('product.wasLiked', productId.value),
          publishInteraction({
            name: 'attendee.product.collection.disliked',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      } else if (isClient.value) {
        await LikeListService.addDislikedProduct(productId.value, apiInstance);
        await Promise.all([
          mercurePublish.all('client.changedLikes', appointment.value.attendeeId),
          mercurePublish.all('product.wasLiked', productId.value),
          loadLikeAndDislikeList(),
          publishInteraction({
            name: 'attendee.product.collection.disliked',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      }
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'dislike',
      });
    } finally {
      loading.value = false;
    }
  }

  async function guideLike() {
    try {
      await LikeListService.addLikedProduct(productId.value, apiInstance);
      await Promise.all([
        publishInteraction({
          name: 'attendee.product.collection.liked',
          payload: {
            productId: productId.value
          }
        }),

        loadGuideProductList()
      ])
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'like',
      });
    }
  }

  async function removeFromLikeList() {
    if (loading.value) return;

    try {
      loading.value = true;

      if (isGuide.value && currentClientForGuide.value) {
        await LikeListService.removeLikedProductForAttendee(adminApiInstance, productId.value, currentClientForGuide.value);
        await Promise.all([
          mercurePublish.all('guide.changedLikes', currentClientForGuide.value),
          mercurePublish.all('product.wasLiked', productId.value),
          publishInteraction({
            name: 'attendee.product.collection.removed',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      } else if (isClient.value) {
        await LikeListService.removeLikedProduct(productId.value, apiInstance);
        await Promise.all([
          mercurePublish.all('client.changedLikes', appointment.value.attendeeId),
          mercurePublish.all('product.wasLiked', productId.value),
          loadLikeAndDislikeList(),
          publishInteraction({
            name: 'attendee.product.collection.removed',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      }
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'removeFromLikeList',
      });
    } finally {
      loading.value = false;
    }
  }

  async function removeFromGuideList() {
    try {
      await LikeListService.removeLikedProduct(productId.value, apiInstance);
      await Promise.all([
        publishInteraction({
          name: 'attendee.product.collection.removed',
          payload: {
            productId: productId.value,
          }
        }),

        loadGuideProductList()
      ])
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'removeFromLikeList',
      });
    }
  }

  async function removeFromDislikeList() {
    if (loading.value) return;

    try {
      loading.value = true;

      if (isGuide.value && currentClientForGuide.value) {
        await LikeListService.removeDislikedProductForAttendee(productId.value, currentClientForGuide.value, adminApiInstance);
        await Promise.all([
          mercurePublish.all('guide.changedLikes', currentClientForGuide.value),
          mercurePublish.all('product.wasLiked', productId.value),
          publishInteraction({
            name: 'attendee.product.collection.removed',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      } else if (isClient.value) {
        await LikeListService.removeDislikedProduct(productId.value, apiInstance);
        await Promise.all([
          mercurePublish.all('client.changedLikes', appointment.value.attendeeId),
          mercurePublish.all('product.wasLiked', productId.value),
          loadLikeAndDislikeList(),
          publishInteraction({
            name: 'attendee.product.collection.removed',
            payload: {
              productId: productId.value,
            }
          }),
        ])
      }
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'removeFromDislikeList',
      });
    } finally {
      loading.value = false;
    }
  }

  async function loadLikeList() {
    try {
      let data;
      if (isGuide.value && currentClientForGuide.value) {
        data = await LikeListService.getLikedProductsForAttendee(
          apiInstance,
          {},
          currentClientForGuide.value,
          adminApiInstance
        );
      } else if (isClient.value) {
        data = await LikeListService.getLikedProducts(
          {},
          apiInstance
        );
      }
      _storeLikeList.value = data?.elements || [];
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadLikeList',
      });
    }
  }

  async function loadDislikeList() {
    try {
      let data;
      if (isGuide.value && currentClientForGuide.value) {
        data = await LikeListService.getDislikedProductsForAttendee(
          {},
          currentClientForGuide.value,
          apiInstance,
          adminApiInstance
        );
      } else if (isClient.value) {
        data = await LikeListService.getDislikedProducts(
          {},
          apiInstance
        );
      }
      _storeDislikeList.value = data?.elements || [];
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadDislikeList',
      });
    }
  }

  async function loadGuideProductList() {
    try {
      let data = await LikeListService.getLikedProducts(
        {},
        apiInstance
      );

      _storeGuideLikeList.value = data?.elements || [];
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadGuideProductList',
      });
    }
  }

  // load both lists
  function loadLikeAndDislikeList() {
    return Promise.all([
      loadLikeList(),
      loadDislikeList(),
    ]);
  }

  async function initLikeListForProductWidget() {
    // reload like list for guide if current client liked/disliked something
    likedProduct.value = '';

    addWatcher(watch(likedProduct, () => {
      if (!likedProduct.value) return;
      const id = likedProduct.value;
      likedProduct.value = '';

      if (!productId.value) return;
      if (id !== productId.value) return;
      loadAllProductLikesAndDislikes();
    }));
  }

  // init function which is called only once in guide page
  async function initLikeListForGuide() {
    if (initialized.value) return;
    initialized.value = true;

    // watch for changes and load the like and dislike lists or clear it
    addWatcher(watch(currentClientForGuide, () => {
      if (currentClientForGuide.value) {
        loadLikeAndDislikeList();
      } else {
        _storeLikeList.value = [];
        _storeDislikeList.value = [];
      }
    }));

    // reload like list for guide if current client liked/disliked something
    addWatcher(watch(lastClientChangedLikes, () => {
      if (!lastClientChangedLikes.value) return;

      // reset flag to get notified if same client does something again
      const id = lastClientChangedLikes.value;
      lastClientChangedLikes.value = '';

      refreshCustomerWidgetData(id);

      if (currentClientForGuide.value === id) {
        loadLikeAndDislikeList();
      }
    }));

    // reload like list in customer widget if guide likes for user
    addWatcher(watch(lastGuideChangedLikes, () => {
      if (!lastGuideChangedLikes.value) return;

      // reset flag to get notified if same client does something again
      const id = lastGuideChangedLikes.value;
      lastGuideChangedLikes.value = '';

      refreshCustomerWidgetData(id);

      if (currentClientForGuide.value === id) {
        loadLikeAndDislikeList();
      }

    }));

    // load lists for first client (if he already exists)
    if (currentClientForGuide.value) {
      loadLikeAndDislikeList();
    }

    // load like list for guide
    loadGuideProductList();

  }

  // init function which is called only once in client page
  function initLikeListForClient() {
    if (initialized.value) return;
    initialized.value = true;

    // reload like list of client if guide liked/disliked something for him
    addWatcher(watch(lastGuideChangedLikes, () => {
      if (!lastGuideChangedLikes.value) return;

      if (appointment.value.attendeeId === lastGuideChangedLikes.value) {
        // reset flag to get notified if same guide does something again
        lastGuideChangedLikes.value = '';

        loadLikeAndDislikeList();
      }
    }));

    // load the list initially for the client
    loadLikeAndDislikeList();
  }

  async function loadAllProductLikesAndDislikes() {
    if (!productId.value) return;
    if ((product.extensions as any)?.attendeeProductCollections) {
      _storeAllProductLikesCount.value = Array((product.extensions as any)?.attendeeProductCollections.likes).fill(1);
      _storeAllProductDislikesCount.value = Array((product.extensions as any)?.attendeeProductCollections.dislikes).fill(1);
      return;
    }

    try {
      const data = await LikeListService.getLikesAndDislikesForProduct(productId.value, adminApiInstance);
      _storeAllProductLikesCount.value = data.filter((item) => item.alias === 'liked');
      _storeAllProductDislikesCount.value = data.filter((item) => item.alias === 'disliked');
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadAllProductLikesAndDislikes',
      });
    }
  }

  async function toggleGuideLike() {
    toggleLikeLoading.value = true;
    if (_storeGuideLikeList.value.some(product => product.id === productId.value)) {
      await removeFromGuideList();
    } else {
      await guideLike();
    }
    toggleLikeLoading.value = false;
  }

  return {
    like,
    dislike,
    guideLike,
    loadLikeList,
    loadDislikeList,
    loadLikeAndDislikeList,
    initLikeListForProductWidget,
    loadAllProductLikesAndDislikes,
    removeFromLikeList,
    removeFromGuideList,
    removeFromDislikeList,
    productLiked: computed(() => _storeLikeList.value.some(product => product.id === productId.value)),
    productDisliked: computed(() => _storeDislikeList.value.some(product => product.id === productId.value)),
    productOnGuideList: computed(() => _storeGuideLikeList.value.some(product => product.id === productId.value)),
    likeList: computed(() => _storeLikeList.value),
    guideLikeList: computed(() => _storeGuideLikeList.value),
    dislikeList: computed(() => _storeDislikeList.value),
    likedCount: computed(() => _storeLikeList.value.length),
    dislikedCount: computed(() => _storeDislikeList.value.length),
    allProductLikesCount: computed(() => _storeAllProductLikesCount.value.length),
    allProductDislikesCount: computed(() => _storeAllProductDislikesCount.value.length),
    initLikeListForGuide,
    initLikeListForClient,
    showLikeListPage: computed(() => dynamicPage.value?.type === 'likeList' && dynamicPage.value?.opened),
    loadGuideProductList,
    changeProduct,
    productId,
    toggleGuideLike,
    toggleLikeLoading
  };
};
