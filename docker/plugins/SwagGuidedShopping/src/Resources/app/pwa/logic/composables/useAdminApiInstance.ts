import {getApplicationContext, useSharedState} from '@shopware-pwa/composables';
import {computed, ref, Ref} from '@vue/composition-api';
import {AxiosRequestConfig, AxiosResponse} from '~/node_modules/axios';
import {AuthRequestData, AuthResponseData, ShopwareAdminApiInstance} from "../interfaces";
import {createInstance} from '@shopware-pwa/shopware-6-client';
import {useErrorHandler} from "./useErrorHandler";

const COMPOSABLE_NAME = 'useAdminApiInstance';

const COOKIE_NAME: string = 'adminApiAuth';
const COOKIE_PATH: string = '/';
const COOKIE_LIFESPAN: number = 60 * 60 * 24; // min for cookie save
const PRE_EXPIRE_TOKEN_REFRESH_TIME = 10 * 1000; // 10s in ms to refresh token 10 seconds before it will be revoked

let refreshPromise: Promise<any>;
let resolveRefreshPromise: Function = null;
let rejectRefreshPromise: Function = null;

export const useAdminApiInstance = () => {
  const {handleError} = useErrorHandler();
  const contextName = COMPOSABLE_NAME;
  const {apiInstance, cookies} = getApplicationContext({contextName});
  const {sharedRef} = useSharedState();
  const expiresAt = ref<number>(0);
  const isAuthenticated = sharedRef<boolean>(`${contextName}-isAuthenticated`, false);
  const refreshToken = sharedRef<string>(`${contextName}-refreshToken`, '');
  const accessToken = sharedRef<string>(`${contextName}-accessToken`, '');
  const errors: Ref<any> = ref([]);
  let customApiInstance = null;

  createCustomApiInstance();

  function createCustomApiInstance() {
    if (customApiInstance !== null) return;

    customApiInstance = createInstance(apiInstance.config);
    customApiInstance.onConfigChange(({ config }) => {
      try {
        cookies.set("sw-language-id", config.languageId, {
          maxAge: 60 * 60 * 24 * 365,
          sameSite: "Lax",
          path: "/",
        });
      } catch (e) {
        // Sometimes cookie is set on server after request is send, it can fail silently
      }
    });

    const interceptors = {};
    Object.entries(customApiInstance._axiosInstance.interceptors).forEach(entry => {
      const key = entry[0];
      if (key !== 'request') {
        interceptors[key] = entry[1]
      }
    });
    customApiInstance._axiosInstance.interceptors = interceptors;
  }

  async function keepAliveToken() {
    await tryGetAuthenticationCookie();
    if (!isAuthenticated.value) return;
    if (refreshPromise) return refreshPromise;
    if (!refreshToken.value) return;

    refreshPromise = new Promise((resolve, reject): void => {
      resolveRefreshPromise = resolve;
      rejectRefreshPromise = () => {
        reject('token refresh failed');
      };
    }).catch(unsetAuthentication);

    let requestData: AuthRequestData = {
      client_id: 'administration',
      scopes: 'write',
      grant_type: 'refresh_token',
      refresh_token: refreshToken.value
    };

    return await customApiInstance.invoke.post('/api/oauth/token', requestData).then(async (response) => {
      await saveAuthentication(response.data);
    }).catch(async () => {
      await unsetAuthentication();
    });
  }

  /**
   * this message is only called by login screen
   * @param username
   * @param password
   */
  async function authenticate(username: string, password: string): Promise<void> {

    if (!customApiInstance) {
      throw new Error('critical error');
    }

    let requestData: AuthRequestData = {
      client_id: 'administration',
      scopes: 'write',
      grant_type: 'password',
      username: username,
      password: password
    };

    await customApiInstance.invoke.post('/api/oauth/token', requestData).then(async (response) => {
      await saveAuthentication(response.data);
    }).catch((e) => {
      return catchAuthErrors(e);
    }).catch(catchLoginErrors);
  }

  function unsetAuthentication(resetErrors: boolean = true) {
    cookies.remove(COOKIE_NAME);
    tryGetAuthenticationCookie(); // here the local variables (e.g. isAuthenticate) will be cleared

    if (resetErrors) {
      errors.value = [];
    }

    if (refreshPromise) {
      if (rejectRefreshPromise) rejectRefreshPromise();
      refreshPromise = null;
    }
  }

  async function saveAuthentication(data: AuthResponseData) {
    const expiresInMs = data.expires_in * 1000;

    cookies.set(COOKIE_NAME, JSON.stringify({
      expiresAt: Date.now() + expiresInMs,
      isAuthenticated: true,
      accessToken: data.access_token,
      refreshToken: data.refresh_token,
    }), {path: COOKIE_PATH, maxAge: COOKIE_LIFESPAN,});

    tryGetAuthenticationCookie();

    if (refreshPromise) {
      resolveRefreshPromise();
      refreshPromise = null;
    }

    isAuthenticated.value = true;

    setTimeout(() => {
        const catchRefreshPromiseRejection = () => {
        };
        keepAliveToken().catch(catchRefreshPromiseRejection);
      },
      data.expires_in * 1000 - PRE_EXPIRE_TOKEN_REFRESH_TIME
    );
  }

  function tryGetAuthenticationCookie() {
    const cookieData = cookies.get(COOKIE_NAME);
    if (!cookieData) {
      expiresAt.value = 0;
      isAuthenticated.value = null;
      accessToken.value = null;
      refreshToken.value = null;
      return;
    }

    expiresAt.value = cookieData.expiresAt;
    isAuthenticated.value = cookieData.isAuthenticated;
    accessToken.value = cookieData.accessToken;
    refreshToken.value = cookieData.refreshToken;
  }

  /**
   * add Authentication to the header
   * @param config
   */
  function setBearerToken(config?: AxiosRequestConfig) {
    config = config || {};
    config.headers = config.headers || {};
    config.headers['Authorization'] = `Bearer ${accessToken.value}`;
    config.headers['sw-context-token-alias'] = cookies.get('sw-context-token');
    return config;
  }

  async function catchAuthErrors(e) {
    if (e.statusCode === 401 || e.statusCode === 403) {
      await unsetAuthentication();
    }
    throw e;
  }

  function catchLoginErrors(e) {
    errors.value = e.messages;
    handleError(e, {
      context: contextName,
      method: 'authenticate',
    });
  }

  async function assureAuthentication(): Promise<void> {
    if (refreshPromise) return refreshPromise; // do nothing wait for last refresh

    if (!customApiInstance) {
      throw new Error('critical error');
    }

    if (isAuthenticated.value) return;

    throw new Error('not authenticated');
  }

  const adminApiInstance: ShopwareAdminApiInstance = {
    ...customApiInstance,
    isAuthenticated: computed(() => isAuthenticated.value),
    refreshToken: computed(() => refreshToken.value),
    accessToken: computed(() => accessToken.value),
    invoke: {
      post: <T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> => {
        const request = () => customApiInstance.invoke.post(url, data, setBearerToken(config));
        return assureAuthentication().then(request).catch(catchAuthErrors);
      },
      get: <T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> => {
        const request = () => customApiInstance.invoke.get(url, setBearerToken(config));
        return assureAuthentication().then(request).catch(catchAuthErrors);
      },
      put: <T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> => {
        const request = () => customApiInstance.invoke.put(url, data, setBearerToken(config))
        return assureAuthentication().then(request).catch(catchAuthErrors);
      },
      patch: <T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> => {
        const request = () => customApiInstance.invoke.patch(url, data, setBearerToken(config));
        return assureAuthentication().then(request).catch(catchAuthErrors);
      },
      delete: <T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> => {
        const request = () => customApiInstance.invoke.delete(url, setBearerToken(config));
        return assureAuthentication().then(request).catch(catchAuthErrors);
      },
    },
  };

  return {
    adminApiInstance,
    isGuideAuthenticated: computed(() => isAuthenticated.value),
    authenticate,
    unsetAuthentication,
    keepAliveToken,
    errors
  };
};
