import { ShopwareApiInstance } from '@shopware-pwa/shopware-6-client';
import { getQuickViewEndpoint } from '../endpoints';
import { CmsPage } from '@shopware-pwa/commons/interfaces/models/content/cms/CmsPage';

export function getQuickView(params: {
  productId: string,
  cmsPageLayoutId: string
},apiInstance: ShopwareApiInstance): Promise<CmsPage> {
  return apiInstance.invoke.get(getQuickViewEndpoint(params.productId, params.cmsPageLayoutId)).then((response) => {
    return response.data;
  });
}
