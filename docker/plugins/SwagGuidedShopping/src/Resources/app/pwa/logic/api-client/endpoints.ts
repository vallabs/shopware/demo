export const getJoinAsGuideEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/join-as-guide`;

export const getJoinAsClientEndpoint = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/join-as-client`;

export const getPresentationPageEndpoint = () =>
  `/store-api/guided-shopping/appointment/presentation`;

export const getPresentationPageDataEndpoint = (pageId: string, slideId: string) =>
  `/store-api/guided-shopping/appointment/presentation/${pageId}/slide/${slideId}`;

export const getPresentationPageProductsEndpoint = (pageId: string, slideId: string) =>
  `/store-api/guided-shopping/appointment/presentation/${pageId}/slide/${slideId}/products`;

export const getAppointmentStartEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/start`;

export const getAppointmentEndEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/end`;

export const respondInvitationEndpoint = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/attendee/respond-invitation`;

export const downloadCalendarFile = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/download-ics`;

export const getPresentationGuideStateEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/presentation/state`;

export const getPresentationClientStateEndpoint = () =>
  `/store-api/guided-shopping/appointment/presentation/state`;

export const getLikesDislikesForAttendeeEndpoint = () =>
  `/api/search/guided-shopping-attendee-product-collection`;

export const addLikesDislikesForAttendeeEndpoint = () =>
  `/api/guided-shopping-attendee-product-collection`;

export const removeLikesDislikesForAttendeeEndpoint = (id: string) =>
  `/api/guided-shopping-attendee-product-collection/${id}`;

export const getInteractionEndpoint = () =>
  `/store-api/guided-shopping/interaction`;

export const getLikedProductsEndpoint = () =>
  `/store-api/guided-shopping/appointment/collection/liked`;

export const getDislikedProductsEndpoint = () =>
  `/store-api/guided-shopping/appointment/collection/disliked`;

export const addLikedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/liked/${productId}`;

export const addDislikedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/disliked/${productId}`;

export const removeLikedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/liked/${productId}`;

export const removeDislikedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/disliked/${productId}`;

export const getLastSeenProductsEndpoint = () =>
  `/store-api/guided-shopping/appointment/collection/last-seen`;

export const getProductsEndpoint = () =>
  `/store-api/product`;

export const getCmsPageByIdEndpoint = (pageId: string) =>
  `/store-api/cms/${pageId}`;

export const updateAttendeeEndpoint = () =>
  `/store-api/guided-shopping/appointment/attendee`

export const getQuickViewEndpoint = (productId: string, cmsPageLayoutId: string) =>
  `/store-api/guided-shopping/quickview/${productId}/${cmsPageLayoutId}`;

export const getGuideCartEndpoint = (salesChannelId: string) =>
  `/api/_proxy/store-api/${salesChannelId}/checkout/cart`;

export const getGuideCartLineItemEndpoint = (salesChannelId: string) =>
  `/api/_proxy/store-api/${salesChannelId}/checkout/cart/line-item`;

export const getAppointmentByPathEndpoint = () =>
  `/api/search/guided-shopping-appointment`;

export const getCartInsightsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/widgets/cart-insights`;

export const getOrderLineItemEndpoint = () =>
  `/api/search/order-line-item`;

export const getOrderCustomerEndpoint = () =>
  `/api/search/order-customer`;

export const getAttendeeEndpoint = () =>
  `/api/search/guided-shopping-appointment-attendee`

export const getAttendeeInsightsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/widgets/attendee-insights`;

export const getProductStreamsEndpoint = () =>
  `/api/search/product-stream`;

export const getSyncProductsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/instant-listing`;

export const getAttendeeContextTokenEndpoint = (attendeeId: string) =>
  `/api/_action/guided-shopping/appointment/attendee/${attendeeId}/sw-context-token`;

export const getAllProductsWithDataEndpoint = () =>
  `/store-api/guided-shopping/product-listing`;

export const getAllProductIdsEndpoint = () =>
  `/store-api/guided-shopping/all-product-ids`;

export const getVideoAndAudioSettings = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/video-audio-settings`;
