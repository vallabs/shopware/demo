import { debounce } from '@shopware-pwa/helpers'
import { getApplicationContext, useSharedState } from '@shopware-pwa/composables';
import { computed, onMounted, watch } from "@vue/composition-api"
import { useAppointment } from "./useAppointment";
import { useErrorHandler } from "./useErrorHandler";
import { useState } from './useState';
import DailyIframe, {
    DailyCall,
    DailyEventObjectActiveSpeakerChange,
    DailyEventObjectParticipantLeft,
    DailyParticipant
} from "@daily-co/daily-js";

import { VideoAudioSettingsType, VideoParticipant, VideoParticipants } from "../interfaces";
import { AppointmentService } from '../api-client';
import { useInteractions } from "./useInteractions";
import { useVideoSetup } from "./useVideoSetup";
import { DeviceError, useMediaDevices } from "./useMediaDevices";
import { useMercureObserver } from './useMercureObserver';
import { useMercure } from "./useMercure";
import { useWatchers } from './useWatchers';

const COMPOSABLE_NAME = 'useVideoCall';

const MICROPHONE_STATE_STORAGE_KEY = 'sw-gs-call-microphone-state';
const CAMERA_STATE_STORAGE_KEY = 'sw-gs-call-camera-state';
const SPEAKER_DEVICE_SET_DELAY = 300;

declare global {
    interface Window {
        dailyCallInstanceCreated: boolean,
        dailyCallInstance: DailyCall,
    }
}

export const useVideoCall = function () {
    const contextName = COMPOSABLE_NAME;
    const { sharedRef } = useSharedState();
    const { isGuide, isClient, isSelfService, attendeeName } = useAppointment();
    const { handleError } = useErrorHandler();
    const { stateForAll, stateForGuides, stateForClients, currentGuide } = useState();
    const { publishInteraction } = useInteractions();
    const { activeSpeaker } = useMercureObserver();
    const { mercurePublish } = useMercure();
    const { hasSavedSettings, mediaDeviceChanged, getMediaDevices, handleDeviceError, microphoneDevice, cameraDevice, speakerDevice, setDevicesFromStorage, isAudioOnly, microphoneCount} = useMediaDevices();
    const { stopDeviceChanges } = useVideoSetup();
    const { apiInstance } = getApplicationContext({ contextName });
    const isBroadCastEnabled = computed(() => stateForAll.value?.broadcastMode);
    const appointmentIsRunning = computed(() => stateForAll.value?.running);
    const _hasJoinedCall = sharedRef<boolean>(`${contextName}-hasJoinedCall`, false);
    const _reinitializeWatchersStarted = sharedRef<boolean>(`${contextName}-reinitializeWatchersStarted`, false);
    const _connectedWatchersStarted = sharedRef<boolean>(`${contextName}-connectedWatchersStarted`, false);
    const _deviceSettingsWatchersStarted = sharedRef<boolean>(`${contextName}-deviceSettingsWatchersStarted`, false);
    const _activeSpeakerId = sharedRef<string>(`${contextName}-activeSpeaker`);
    const _microphoneState = sharedRef<boolean>(`${contextName}-microphoneState`, false);
    const _cameraState = sharedRef<boolean>(`${contextName}-cameraState`, false);
    const _speakerDeviceMuted = sharedRef<boolean>(`${contextName}-speakerDeviceMuted`, true);
    const { addWatcher } = useWatchers();
    const speakerAudios = sharedRef<any>(`${contextName}-speakerAudios`, null);

    watch([speakerAudios], () => {
        unlockSpeakerAudio();
    });

    function unlockSpeakerAudio() {
        if (!speakerAudios.value) return;
        speakerAudios.value.forEach(async (audio) => {
            try {
                await audio.play();
            } catch (e) {
                handleDeviceError('SPEAKER_NOT_ALLOWED');
            }
        });
    }

    const room = {
        running: appointmentIsRunning,
        url: computed(() => stateForAll.value?.videoRoomUrl),
        token: computed(() => {
            if (isGuide.value) return stateForGuides.value?.videoGuideToken;
            if (isClient.value) return stateForClients.value?.videoClientToken;
        }),
    };

    let members = {};
    const participants = sharedRef<VideoParticipants>(`${contextName}-participants`, {
        self: null,
        members: members,
        memberCount: 0,
        guide: null
    });

    onMounted(() => {
        _microphoneState.value = JSON.parse(sessionStorage.getItem(MICROPHONE_STATE_STORAGE_KEY));
        _cameraState.value = JSON.parse(sessionStorage.getItem(CAMERA_STATE_STORAGE_KEY));
        if (isAudioOnly.value) _cameraState.value = false;
        // first request to get plain list without labels
        getMediaDevices(true, true);
    });

    const setup = () => {
        initCall();
        startReinitializeWatchers();
    };

    /**
     * creates all necessary call objects and watchers
     */
    const initCall = async () => {
        const instance: DailyCall = window.dailyCallInstance;

        if (!hasSavedSettings.value) {
            _hasJoinedCall.value = false;
            return;
        } // will be retried by watcher on retry
        if (!appointmentIsRunning.value) return; // will be retried by watcher on retry

        if (instance) return;
        if (_hasJoinedCall.value) return;

        stopDeviceChanges();

        const devices = await getMediaDevices(true, true); // this list will have empty labels too if no setup window appears after refresh

        // set shared refs from storage for the device ids
        setDevicesFromStorage(devices);

        await createCallInstance();
        await joinCall();

        await getMediaDevices(true); // if daily is the first one to ask for permissions we have to update the list again
        startDeviceSettingsWatchers();
    };

    /**
     * register watchers
     */
    function startReinitializeWatchers() {
        if (_reinitializeWatchersStarted.value) return;
        _reinitializeWatchersStarted.value = true;

        addWatcher(watch(hasSavedSettings, (savedSettings) => {
            if (!savedSettings) return;
            initCall();
        }));
        addWatcher(watch([appointmentIsRunning, room.token], ([running, token]) => {
            if (!running || !token) return;
            initCall();
        }));
    }

    /**
     * register device settings watchers
     */
    function startDeviceSettingsWatchers() {
        if (_deviceSettingsWatchersStarted.value) return;
        _deviceSettingsWatchersStarted.value = true;

        addWatcher(watch(mediaDeviceChanged, (config) => {
            if (!config) return;
            const storageKey = config.storageKey;
            const deviceId = config.deviceId;

            if (!deviceId) return;
            updateCallDevice(storageKey, deviceId);
        }));
    }

    /**
     * watchers for when the call is instantiated
     */
    function startConnectedWatchers() {
        if (_connectedWatchersStarted.value) return;
        _connectedWatchersStarted.value = true;

        // watch for broadcast mode activation on client side
        if (isClient.value) {
            addWatcher(watch(isBroadCastEnabled, (state) => {
                if (state === false) return;

                toggleMicrophone(false);
                toggleCamera(false);
            }));
        }

        // update guide attendee name for daily
        addWatcher(watch(currentGuide, () => {
            const instance: DailyCall = window.dailyCallInstance;
            if (!currentGuide.value || !instance) return;
            instance.setUserName(currentGuide.value.attendeeName);
        }));

        // update client attendee name for daily
        addWatcher(watch(attendeeName, () => {
            const instance: DailyCall = window.dailyCallInstance;
            if (!attendeeName.value && !instance) return;
            instance.setUserName(attendeeName.value);
        }));

        addWatcher(watch(activeSpeaker, setActiveSpeaker));
    }

    /**
     * returns options with which the instance is created
     */
    const getInstanceOptions = () => {
        const options: any = {};

        // force camera state to off when audioOnly is active
        if (isAudioOnly.value) _cameraState.value = false;

        if (!_microphoneState.value) {
            options.audioSource = false;
        } else if (microphoneDevice.value) {
            options.audioSource = microphoneDevice.value;
        }

        if (!_cameraState.value) {
            options.videoSource = false;
        } else if (microphoneDevice.value) {
            options.videoSource = cameraDevice.value;
        }

        return options;
    };

    /**
     * initializes the video call
     */
    async function createCallInstance() {
        if (!window) return;
        if (window.dailyCallInstanceCreated) return;
        if (window.dailyCallInstance) return;
        if (!hasSavedSettings.value) return false;
        if (isSelfService.value) return;

        window.dailyCallInstanceCreated = true;

        try {
            const options = getInstanceOptions();
            if (callEnabled.value) {
                if (!videoChatEnabled.value) {
                    window.dailyCallInstance = await DailyIframe.createCallObject({
                        startVideoOff: true,
                        videoSource: false,
                    });
                } else {
                    window.dailyCallInstance = await DailyIframe.createCallObject();
                }
            }
        } catch (e) {
            window.dailyCallInstance = await DailyIframe.createCallObject({
                audioSource: null,
                videoSource: null
            });
        }

        startConnectedWatchers();
    }

    /**
     * register daily events
     */
    function registerCallEvents() {
        const instance = window.dailyCallInstance;

        instance.on('joined-meeting', updateParticipant);
        instance.on('participant-joined', (event) => {
            publishActiveSpeaker(activeSpeaker.value);
            updateParticipant(event);
        });
        instance.on('participant-left', removeParticipant);
        instance.on('participant-updated', updateParticipant);
        instance.on('track-stopped', updateParticipant);
        instance.on('track-started', updateParticipant);

        instance.on('active-speaker-change', onActiveSpeakerChanged);
        instance.on('camera-error', onDeviceError);

    }


    /**
     * @param dailyError
     */
    const onDeviceError = (dailyError) => {
        if (isAudioOnly.value) return;
        let errorName: DeviceError = 'NO_DEVICES_FOUND';
        const error = dailyError.error;
        if (!error) return handleDeviceError(errorName);

        const errorType = error.type;
        if (errorType === 'cam-in-use') errorName = 'CAMERA_IN_USE';
        if (errorType === 'mic-in-use') errorName = 'MICROPHONE_IN_USE';
        if (errorType === 'cam-mic-in-use') errorName = 'CAMERA_AND_MICROPHONE_IN_USE';

        handleDeviceError(errorName);
    };


    /**
     * checks if call is join able
     */
    function canNotJoin() {
        const isDefined = (value) => value != undefined || value != null;
        const hasInstance = isDefined(window.dailyCallInstance);
        const roomRunning = isDefined(room.running.value);
        const roomHasUrl = isDefined(room.url.value);
        const roomHasToken = isDefined(room.token.value);

        return !hasInstance || !roomRunning || !roomHasUrl || !roomHasToken;
    }

    /**
     * join the call
     */
    async function joinCall() {
        if (isSelfService.value) return;
        if (_hasJoinedCall.value) return;

        try {
            if (canNotJoin()) return;

            const instance: DailyCall = window.dailyCallInstance;
            registerCallEvents();

            let options = {
                url: room.url.value,
                token: room.token.value,
                activeSpeakerMode: true,
            };

            if (isAudioOnly.value) {
                // on audio only mode disable devices with "false" "for ever" (see daily docs)
                options['audioSource'] = microphoneCount.value === 0 ? false : microphoneDevice.value;
                options['videoSource'] = false;
            } else {
                // otherwise do not set the the device options to let it open for adding it later or using default device by daily
                if (microphoneDevice.value) options['audioSource'] = microphoneDevice.value;
                if (cameraDevice.value && stateForAll?.value?.videoAudioSettings === VideoAudioSettingsType.BOTH) options['videoSource'] = cameraDevice.value;
            }

            await instance.join(options);
            setOutputDevice(speakerDevice.value);

            // do this after join to mute video and cam, because on join we only can inject deviceId OR false, if we inject false the user can never be unmuted
            if (isBroadCastEnabled.value && !isGuide.value) {
                toggleMicrophone(false);
                toggleCamera(false);
            }else {
                toggleMicrophone(_microphoneState.value);
                toggleCamera(_cameraState.value);
            }

            // update daily username for guide or client
            if (isGuide && currentGuide.value) instance.setUserName(currentGuide.value.attendeeName);
            if (isClient && attendeeName.value) instance.setUserName(attendeeName.value);

            // save video call id in attendee
            await AppointmentService.updateAttendee({
                videoUserId: participants.value.self.data.session_id
            }, apiInstance);

            window.addEventListener("beforeunload", instance.leave);

            _hasJoinedCall.value = true;
        } catch (e) {
            handleError(e, {
                context: contextName,
                method: 'joinCall',
            });
        }
    }

    /**
     * we debounce this because if multiple participants join, this would be executed the same amount of times
     * daily issue https://github.com/daily-co/daily-js/issues/181
     * @param deviceId
     */
    function setOutputDevice(deviceId) {
        const debouncedSetOutputDevice = debounce(() => {
            const instance: DailyCall = window.dailyCallInstance;
            instance.setOutputDevice({ outputDeviceId: deviceId });
            _speakerDeviceMuted.value = false;
        }, SPEAKER_DEVICE_SET_DELAY);

        debouncedSetOutputDevice();
    }

    /**
     * updates the currently used device for the call instance
     *
     * @param storageKey
     * @param deviceId
     */
    function updateCallDevice(storageKey, deviceId) {
        const instance: DailyCall = window.dailyCallInstance;
        if (!instance) return;

        switch (storageKey) {
            case 'microphone-device':
                instance.setInputDevicesAsync({ 'audioDeviceId': deviceId });
                break;
            case 'camera-device':
                instance.setInputDevicesAsync({ 'videoDeviceId': deviceId });
                break;
            case 'speaker-device':
                setOutputDevice(deviceId);
                break;
            default:
                break
        }
    }

    /**
     * toggles audio state
     *
     * @param forcedState
     */
    function toggleMicrophone(forcedState: boolean = null) {
        const instance: DailyCall = window.dailyCallInstance;

        _microphoneState.value = (forcedState !== null) ? forcedState : !_microphoneState.value;
        sessionStorage.setItem(MICROPHONE_STATE_STORAGE_KEY, JSON.stringify(_microphoneState.value));

        if (!participants.value || !participants.value.self || !instance) return;

        const self = participants.value.self;
        instance.setLocalAudio(_microphoneState.value);
        self.data.audio = _microphoneState.value;
    }

    /**
     * toggles video state
     *
     * @param forcedState
     */
    function toggleCamera(forcedState: boolean = null) {
        const instance: DailyCall = window.dailyCallInstance;

        _cameraState.value = (forcedState !== null) ? forcedState : !_cameraState.value;
        sessionStorage.setItem(CAMERA_STATE_STORAGE_KEY, JSON.stringify(_cameraState.value));

        if (!participants.value || !participants.value.self || !instance) return;

        const self = participants.value.self;
        instance.setLocalVideo(_cameraState.value);
        self.data.video = _cameraState.value;
    }

    /**
     * toggles the broadcast mode via interaction
     */
    function toggleBroadcastMode() {
        publishInteraction({
            name: 'broadcastMode.toggled',
            payload: {
                active: !isBroadCastEnabled.value
            }
        });
    }

    /**
     * checks whether the participant is local or external
     *
     * @param event
     */
    function updateParticipant(event) {
        if (checkIfLocalParticipant(event)) return createLocalParticipant(event);

        // we have to set the speaker every time a participant joins because of a annoying daily issue https://github.com/daily-co/daily-js/issues/181
        setOutputDevice(speakerDevice.value);

        createClientParticipant(event);
    }

    /**
     * executed when a participant leaves the call
     *
     * @param event
     */
    function removeParticipant(event: DailyEventObjectParticipantLeft) {
        delete members[event.participant.session_id];
        participants.value.members = Object.assign({}, members);
        participants.value.memberCount = Object.keys(members).length;
        checkIfGuideLeft(event.participant.session_id);

        const instance: DailyCall = window.dailyCallInstance;
        if (instance) instance.updateParticipant(event.participant.session_id, { eject: true });
    }

    /**
     * executed when the active speaker changes
     *
     * @param event
     */
    function onActiveSpeakerChanged(event: DailyEventObjectActiveSpeakerChange) {
        const activeSpeakerId = event.activeSpeaker.peerId;
        publishActiveSpeaker(activeSpeakerId)
    }

    /**
     * saves the local participant as shared ref
     *
     * @param event
     */
    function createLocalParticipant(event) {
        const participant = getParticipantData(event);
        if (!participant) return;

        participants.value.self = participant;
    }

    /**
     * check if not local participant and then save the participant as member
     *
     * @param event
     */
    function createClientParticipant(event) {
        const participant = getParticipantData(event);

        if (!participant) return;

        // check if local id is equal to member id, if so exit
        if (participants.value.self) {
            if (participants.value.self.data.session_id === participant.data.session_id) return;
        }

        members[participant.data.session_id] = participant;
        participants.value.members = Object.assign({}, members);
        participants.value.memberCount = Object.keys(members).length;

        createGuideParticipant(participant);
    }

    /**
     * checks if guide and saves the participant as guide
     *
     * @param participant
     */
    function createGuideParticipant(participant) {
        if (!participant.data.owner && participants.value.guide !== null) return;

        participants.value.guide = participant;
    }

    /**
     * checks if the guide has left the meeting and deletes him
     * @param participantId
     */
    function checkIfGuideLeft(participantId) {
        if (participants.value.guide.data.session_id !== participantId) return;

        participants.value.guide = null;
    }

    /**
     * creates a participant object
     * @param event
     */
    function getParticipantData(event): VideoParticipant {
        const participant: DailyParticipant = getParticipantFromEvent(event);

        if (!participant) return;

        return {
            data: participant,
            streams: {
                video: (participant.video && participant.videoTrack) ? new MediaStream([participant.videoTrack]) : null,
                audio: (participant.audio && participant.audioTrack) ? new MediaStream([participant.audioTrack]) : null,
            }
        };
    }

    /**
     * returns if the participant is local
     *
     * @param event
     */
    function checkIfLocalParticipant(event): boolean {
        const participant = getParticipantFromEvent(event);
        if (participant) return participant.local;
        return false;
    }

    /**
     * helper method to get the participant from daily events
     *
     * @param event
     */
    function getParticipantFromEvent(event): DailyParticipant {
        if (event.participant) return event.participant;
        if (event.participants && event.participants.local) return event.participants.local;
        return null;
    }

    /**
     * helper method to publish the active speaker on join and speaker change
     * @param activeSpeakerId
     */
    function publishActiveSpeaker(activeSpeakerId) {
        if (!isGuide.value) return;
        mercurePublish.all('activeSpeakerUpdated', activeSpeakerId);
        setActiveSpeaker(activeSpeakerId);
    }

    const ownParticipant = computed(() => participants.value.self);
    const ownVideoStream = computed(() => ownParticipant.value?.streams?.video);
    const memberParticipantsById = computed(() => participants.value.members);
    const memberParticipants = computed(() => Object.values(memberParticipantsById.value));
    const memberCount = computed(() => memberParticipants.value.length);
    const hasMembers = computed(() => memberCount.value > 0);
    const callEnabled = computed(() => [VideoAudioSettingsType.BOTH, VideoAudioSettingsType.AUDIO_ONLY].includes(stateForAll?.value?.videoAudioSettings) && !isSelfService.value);
    const videoChatEnabled = computed(() => stateForAll?.value?.videoAudioSettings === VideoAudioSettingsType.BOTH && !isSelfService.value);


    /**
     * helper method to publish the active speaker on join and speaker change
     * @param activeSpeakerId
     */
    function setActiveSpeaker(activeSpeakerId) {
        const self = participants.value.self;
        if (!activeSpeakerId || activeSpeakerId === self.data.session_id) return;
        _activeSpeakerId.value = activeSpeakerId;
    }

    const _evaluatedActiveSpeaker: { value: any } = computed(() => {
        if (memberParticipants.value.length == 1) {
            return memberParticipants.value[0];
        }
        return memberParticipantsById.value[_activeSpeakerId.value];
    });

    const activeSpeakerStream = computed(() => _evaluatedActiveSpeaker.value?.streams?.video);

    return {
        setup,
        callIsSetup: computed(() => _hasJoinedCall.value),
        ownParticipant,
        ownVideoStream,
        memberParticipantsById,
        memberParticipants,
        memberCount,
        hasMembers,
        activeSpeaker: computed(() => _evaluatedActiveSpeaker.value),
        activeSpeakerStream,
        toggleMicrophone,
        microphoneState: computed(() => _microphoneState.value),
        toggleCamera,
        cameraState: computed(() => _cameraState.value),
        speakerDeviceMuted: computed(() => _speakerDeviceMuted.value),
        toggleBroadcastMode,
        isBroadCastEnabled,
        speakerAudios,
        unlockSpeakerAudio,
        updateCallDevice,
        callEnabled,
        videoChatEnabled
    };

};
