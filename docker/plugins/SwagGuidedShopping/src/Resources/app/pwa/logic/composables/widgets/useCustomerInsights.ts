import { useSharedState } from "@shopware-pwa/composables";
import { CustomerService, OrderService } from "../../api-client";
import { computed, ref } from "@vue/composition-api";
import { useAdminApiInstance } from "../useAdminApiInstance";
import { useAppointment } from "../useAppointment";
import {
  ClientApiError,
  CustomerInsightsAttendeeData,
  CustomerInsightsCustomerData,
  CustomerInsightsYearOrder,
} from "../../interfaces";
import { useErrorHandler } from "../useErrorHandler";
import dayjs from "dayjs";
import {
  ShopwareSearchParams,
  SearchFilterType,
  EqualsFilter,
} from "@shopware-pwa/commons";

const COMPOSABLE_NAME = "useCustomerInsights";

export const useCustomerInsights = () => {
  const contextName = COMPOSABLE_NAME;
  const { appointment } = useAppointment();
  const { adminApiInstance } = useAdminApiInstance();
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const _storeAttendeeData = sharedRef<CustomerInsightsAttendeeData>(
    `${contextName}-attendeeData`,
    null
  );
  const _storeCurrentCustomerData = sharedRef<CustomerInsightsCustomerData>(
    `${contextName}-customerData`,
    null
  );
  const isLoading = ref<boolean>(true);
  const customerDataCriteria: ShopwareSearchParams = {
    associations: {
      productCollections: {},
      customer: {
        associations: {
          defaultBillingAddress: {
            associations: {
              country: {},
            },
          },
        },
      },
    },
    includes: {
      customer: ["email", "defaultBillingAddress"],
    },
    filter: [
      {
        type: SearchFilterType.EQUALS,
        field: "id",
        value: null,
      },
    ],
  };
  const _lastYearOrders = sharedRef<CustomerInsightsYearOrder[]>(
    `${contextName}-lastYearOrders`,
    []
  );
  const _currentYearOrders = sharedRef<CustomerInsightsYearOrder[]>(
    `${contextName}-currentYearOrders`,
    []
  );

  async function loadCustomerData(attendeeId, isOpen) {
    if (!attendeeId || !isOpen) return;

    (customerDataCriteria.filter[0] as EqualsFilter).value = attendeeId;

    try {
      _storeCurrentCustomerData.value = await CustomerService.getCustomerData(
        customerDataCriteria,
        adminApiInstance
      );

      const customerId = _storeCurrentCustomerData.value.customerId;
      await loadCustomerOrdersData(customerId);
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadCustomerData",
      });
    }
    isLoading.value = false;
  }

  async function loadAttendeeData() {
    if (!appointment?.value?.id) return;
    try {
      _storeAttendeeData.value = await CustomerService.getAttendeeData(
        appointment.value.id,
        adminApiInstance
      );
      isLoading.value = false;
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadAttendeeData",
      });
    }
  }

  async function loadCustomerOrdersData(customerId) {
    _lastYearOrders.value = [];
    _currentYearOrders.value = [];
    const DATE_FORMAT: string = "YYYY-MM-DD";
    const pastYearStartDate = dayjs()
      .subtract(1, "year")
      .startOf("year")
      .format(DATE_FORMAT);
    const currentYearEndDate = dayjs().endOf("year").format(DATE_FORMAT);

    const orderCustomerCriteria: ShopwareSearchParams = {
      includes: {
        order: ["amountNet", "amountTotal", "lineItems", "orderDate"],
        order_customer: ["order"],
      },
      associations: {
        order: {
          associations: {
            lineItems: {},
          },
        },
      },
      filter: [
        {
          type: SearchFilterType.MULTI,
          operator: "and",
          queries: [
            {
              type: SearchFilterType.EQUALS,
              field: "order.stateMachineState.technicalName",
              value: "completed",
            },
            {
              type: SearchFilterType.EQUALS,
              field: "customerId",
              value: customerId,
            },
            {
              type: SearchFilterType.RANGE,
              field: "order.orderDate",
              parameters: {
                gte: pastYearStartDate,
                lte: currentYearEndDate,
              },
            },
          ],
        },
      ],
      // total_count_mode: 0,
    };

    const orderCustomerResponse = await OrderService.getOrderCustomer(
      orderCustomerCriteria,
      adminApiInstance
    );

    // sort after last and current year
    const lastYear = new Date().getFullYear() - 1;
    const currentYear = new Date().getFullYear();

    orderCustomerResponse.forEach((item) => {
      const orderDate = item.order.orderDate;
      const orderDateYear = new Date(orderDate).getFullYear();

      if (orderDateYear === lastYear) {
        _lastYearOrders.value.push(item.order);
      } else if (orderDateYear === currentYear) {
        _currentYearOrders.value.push(item.order);
      }
    });

    isLoading.value = false;
  }

  async function refreshCustomerData(attendeeId) {
    if (
      !_storeCurrentCustomerData.value ||
      _storeCurrentCustomerData.value.id !== attendeeId
    )
      return;
    await loadCustomerData(_storeCurrentCustomerData.value.id, true);
    isLoading.value = false;
  }

  return {
    loadCustomerData,
    loadAttendeeData,
    refreshCustomerData,
    customerData: computed(() => _storeCurrentCustomerData.value),
    attendeeData: computed(() => _storeAttendeeData.value),
    customerLikesCount: computed(
      () =>
        _storeCurrentCustomerData.value?.productCollections.filter(
          (item) => item.alias === "liked"
        ).length
    ),
    customerDislikesCount: computed(
      () =>
        _storeCurrentCustomerData.value?.productCollections.filter(
          (item) => item.alias === "disliked"
        ).length
    ),
    isLoading: computed(() => isLoading.value),
    lastOrdersItemCount: computed(() => {
      const lastFlattenedLineItems = _lastYearOrders.value.flatMap(
        (order) => order.lineItems
      );
      return lastFlattenedLineItems.reduce((count, lineItem) => {
        const item = lineItem as any;
        return count + item.quantity;
      }, 0);
    }),
    currentOrdersItemCount: computed(() => {
      const currentFlattenedLineItems = _currentYearOrders.value.flatMap(
        (order) => order.lineItems
      );
      return currentFlattenedLineItems.reduce((count, lineItem) => {
        const item = lineItem as any;
        return count + item.quantity;
      }, 0);
    }),
    lastOrdersTotalPrice: computed(() => {
      return _lastYearOrders.value.reduce(
        (accumulator: number, item) => item.amountNet + accumulator,
        0
      );
    }),
    currentOrdersTotalPrice: computed(() => {
      return _currentYearOrders.value.reduce(
        (accumulator: number, item) => item.amountNet + accumulator,
        0
      );
    }),
  };
};
