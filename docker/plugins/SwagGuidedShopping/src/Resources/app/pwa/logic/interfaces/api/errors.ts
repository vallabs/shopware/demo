export interface ShopwareError {
  code: string,
  detail: string,
  meta: {
    parameters: object
  },
  status: string,
  title: string,
}


export interface ClientApiError {
  messages: ShopwareError[];
  message: string,
  statusCode: number;
}
