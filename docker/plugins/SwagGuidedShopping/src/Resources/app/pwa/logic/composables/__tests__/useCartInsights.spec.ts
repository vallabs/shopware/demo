import { ref } from "vue-demi";
import { ShopwareSearchParams, SearchFilterType } from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import { useAdminApiInstance } from "../useAdminApiInstance";
jest.mock("../useAdminApiInstance");
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<
  typeof useAdminApiInstance
>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

import * as shopwareClient from "@shopware-pwa/shopware-6-client";
jest.mock("@shopware-pwa/shopware-6-client");
const mockedApiClient = shopwareClient as jest.Mocked<typeof shopwareClient>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import { useCartInsights } from "../widgets/useCartInsights";


describe("Composables - useCartInsights", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const getCartInsightsMock = jest.fn();

  const adminApiInstanceMock = ref({});
  const _storeCartInsightsMock = ref(null);
  const appointmentIdMock = ref(null);

  beforeEach(() => {
    jest.resetAllMocks();
    _storeCartInsightsMock.value = null;


    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    (mockedUseAppointment as any).mockImplementation(
      () =>
        ({
          appointmentId: appointmentIdMock,
        } as any)
    );

    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("cartInsights"))
            return _storeCartInsightsMock;
        },
      } as any;
    });

    (mockedUseAdminApiInstance as any).mockImplementation(
      () =>
        ({
          adminApiInstance: adminApiInstanceMock,
        } as any)
    );

    mockedGSApiClient.CartService = {
      getCartInsights: getCartInsightsMock,
    } as any;

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("loadCartInsights", () => {
      it("should work correctly when productCount = 0", async () => {
        getCartInsightsMock.mockResolvedValue({
          cartSum: 0,
          productCount: 0,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [],
            byRevenue: []
          },
          extensions: []
        });

      
        const { loadCartInsights, isLoading, cartSum, productCount, topProductsByQuantity, topProductsByRevenue } = useCartInsights();
        await loadCartInsights();
        expect(isLoading.value).toEqual(false);
        expect(cartSum.value).toEqual(0);
        expect(productCount.value).toEqual(0);
        expect(topProductsByQuantity.value).toEqual([]);
        expect(topProductsByRevenue.value).toEqual([]);
        expect(_storeCartInsightsMock.value).toEqual({
          cartSum: 0,
          productCount: 0,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [],
            byRevenue: []
          },
          extensions: []
        });
      })

      it("should work correctly when there is no byQuantity & byRevenue in topProducts", async () => {
        getCartInsightsMock.mockResolvedValue({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });

      
        const { loadCartInsights, isLoading, cartSum, productCount, topProductsByQuantity, topProductsByRevenue } = useCartInsights();
        await loadCartInsights();
        expect(isLoading.value).toEqual(false);
        expect(cartSum.value).toEqual(109.25);
        expect(productCount.value).toEqual(2);
        expect(topProductsByQuantity.value).toEqual([]);
        expect(topProductsByRevenue.value).toEqual([]);
        expect(_storeCartInsightsMock.value).toEqual({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
            ],
            byRevenue: [
            ]
          },
          extensions: []
        });
      })

      it("should work correctly when productCount valid & topProducts valid", async () => {
        getCartInsightsMock.mockResolvedValue({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
              {
                productId: "0096740cecc3491288793afb4c9bd364",
                value: 1,
                name: "Sunset One Pocket Slim Fit Shirt",
                translated: {
                  name: "Sunset One Pocket Slim Fit Shirt"
                }
              },
              {
                productId: "00404d6f11964290b1f83a11ca5b0ee1",
                value: 1,
                name: "Levis X-star Wars Chewbacca Tee Teenager",
                translated: {
                  name: "Levis X-star Wars Chewbacca Tee Teenager"
                }
              }
            ],
            byRevenue: [
              {
                productId: "00404d6f11964290b1f83a11ca5b0ee1",
                value: 67.23,
                name: "Levis X-star Wars Chewbacca Tee Teenager",
                translated: {
                  name: "Levis X-star Wars Chewbacca Tee Teenager"
                }
              },
              {
                productId: "0096740cecc3491288793afb4c9bd364",
                value: 42.02,
                name: "Sunset One Pocket Slim Fit Shirt",
                translated: {
                  name: "Sunset One Pocket Slim Fit Shirt"
                }
              }
            ]
          },
          extensions: []
        });

        mockedApiClient.getProducts.mockResolvedValueOnce({
          elements: [
            {
              translated: {
                name: "translated Sunset One Pocket Slim Fit Shirt"
              },
              name: "Sunset One Pocket Slim Fit Shirt",
              id: "0096740cecc3491288793afb4c9bd364",
            },
            {
              translated: {
                name: "translated Levis X-star Wars Chewbacca Tee Teenager"
              },
              name: "Levis X-star Wars Chewbacca Tee Teenager",
              id: "00404d6f11964290b1f83a11ca5b0ee1",
            },
          ],
        } as any);
      
        const { loadCartInsights, isLoading, cartSum, productCount, topProductsByQuantity, topProductsByRevenue } = useCartInsights();
        await loadCartInsights();


        expect(mockedApiClient.getProducts).toHaveBeenCalledWith({
          includes: {
            product: ["id", "name", "translated"],
          },
          filter: [
            {
              type: SearchFilterType.EQUALS_ANY,
              field: "id",
              value: [
                "0096740cecc3491288793afb4c9bd364",
                "00404d6f11964290b1f83a11ca5b0ee1",
                "00404d6f11964290b1f83a11ca5b0ee1",
                "0096740cecc3491288793afb4c9bd364",
              ],
            },
          ],
        }, rootContextMock.apiInstance);

        expect(isLoading.value).toEqual(false);
        expect(cartSum.value).toEqual(109.25);
        expect(productCount.value).toEqual(2);
        expect(topProductsByQuantity.value).toEqual([
          {
            productId: "0096740cecc3491288793afb4c9bd364",
            value: 1,
            name: "Sunset One Pocket Slim Fit Shirt",
            translated: {
              name: "translated Sunset One Pocket Slim Fit Shirt"
            }
          },
          {
            productId: "00404d6f11964290b1f83a11ca5b0ee1",
            value: 1,
            name: "Levis X-star Wars Chewbacca Tee Teenager",
            translated: {
              name: "translated Levis X-star Wars Chewbacca Tee Teenager"
            }
          }
        ]);
        expect(topProductsByRevenue.value).toEqual([
          {
            productId: "00404d6f11964290b1f83a11ca5b0ee1",
            value: 67.23,
            name: "Levis X-star Wars Chewbacca Tee Teenager",
            translated: {
              name: "translated Levis X-star Wars Chewbacca Tee Teenager"
            }
          },
          {
            productId: "0096740cecc3491288793afb4c9bd364",
            value: 42.02,
            name: "Sunset One Pocket Slim Fit Shirt",
            translated: {
              name: "translated Sunset One Pocket Slim Fit Shirt"
            }
          }
        ]);

        expect(_storeCartInsightsMock.value).toEqual({
          cartSum: 109.25,
          productCount: 2,
          currencyId: "b7d2554b0ce847cd82f3ac9bd1c0dfca",
          currencySymbol: "£",
          topProducts: {
            byQuantity: [
              {
                productId: "0096740cecc3491288793afb4c9bd364",
                value: 1,
                name: "Sunset One Pocket Slim Fit Shirt",
                translated: {
                  name: "translated Sunset One Pocket Slim Fit Shirt"
                }
              },
              {
                productId: "00404d6f11964290b1f83a11ca5b0ee1",
                value: 1,
                name: "Levis X-star Wars Chewbacca Tee Teenager",
                translated: {
                  name: "translated Levis X-star Wars Chewbacca Tee Teenager"
                }
              }
            ],
            byRevenue: [
              {
                productId: "00404d6f11964290b1f83a11ca5b0ee1",
                value: 67.23,
                name: "Levis X-star Wars Chewbacca Tee Teenager",
                translated: {
                  name: "translated Levis X-star Wars Chewbacca Tee Teenager"
                }
              },
              {
                productId: "0096740cecc3491288793afb4c9bd364",
                value: 42.02,
                name: "Sunset One Pocket Slim Fit Shirt",
                translated: {
                  name: "translated Sunset One Pocket Slim Fit Shirt"
                }
              }
            ]
          },
          extensions: []
        }); 
      })

      it("should handle error correctly when API getCartInsights failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }

        getCartInsightsMock.mockRejectedValueOnce(res);

        const { loadCartInsights } = useCartInsights();
        await loadCartInsights();
        
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useCartInsights",
            method: "loadCartInsights"
          }
        );
      });
    })
  });
});
