import {
  getAllProductIds,
  getAllProductsWithData,
  getProductStreams,
  saveInstantListingUpdate,
  saveNewInstantListing
} from "../instantListingService";
import {SelectedFilters} from "../../../interfaces";

describe("Services - instantListingService", () => {
  let postMock = jest.fn();
  let patchMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        post: postMock,
        patch: patchMock,
      },
    };
  });

  describe("getAllProductsWithData", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{
              id: '111'
            }]
          },
        });
      });

      const criteria: SelectedFilters = {... {
        manufacturer: 'dummy-manufacturer-id',
        properties: ['dummy-property-id', 'dummy-property-id2']
      }};

      const res = await getAllProductsWithData(criteria, apiInstanceMock);

      expect(postMock).toHaveBeenCalledWith(`/store-api/guided-shopping/product-listing`, {
        manufacturer: 'dummy-manufacturer-id',
        properties: 'dummy-property-id|dummy-property-id2'
      }, {});
      expect(res).toEqual({
        data: [{
          id: '111'
        }]
      })
    })
  });

  describe("getAllProductIds", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{
              id: '111'
            }]
          },
        });
      });

      const criteria: SelectedFilters = {... {
        manufacturer: ['dummy-manufacturer-id', 'dummy-manufacturer-id2'],
        properties: ['dummy-property-id', 'dummy-property-id2']
      }};

      const res = await getAllProductIds(criteria, apiInstanceMock);

      expect(postMock).toHaveBeenCalledWith(`/store-api/guided-shopping/all-product-ids`, {
        manufacturer: 'dummy-manufacturer-id|dummy-manufacturer-id2',
        properties: 'dummy-property-id|dummy-property-id2'
      }, {});
      expect(res).toEqual({
        data: [{
          id: '111'
        }]
      })
    })
  });

  describe("getProductStreams", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{
              id: '111'
            }]
          },
        });
      });

      const res = await getProductStreams({}, apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/search/product-stream`, {});
      expect(res).toEqual([{
        id: '111'
      }])
    })
  });

  describe("saveNewInstantListing", () => {
    it('should work correctly', async () => {
      const appointmentId = 'dummy-appointment-id-to-sync';
      await saveNewInstantListing( {
        productIds: ['dummy-product-id', 'dummy-product-id2'],
        currentPageGroupId: 'dummy-current-page-group-id',
        appointmentIdToSync: appointmentId,
        pageName: 'dummy-page-name',
      }, apiInstanceMock);

      expect(postMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/${appointmentId}/instant-listing`, {
        productIds: ['dummy-product-id', 'dummy-product-id2'],
        currentPageGroupId: 'dummy-current-page-group-id',
        pageName: 'dummy-page-name'
      });
    })
  });

  describe("saveInstantListingUpdate", () => {
    it('should work correctly', async () => {
      const appointmentId = 'dummy-appointment-id-to-sync';
      const params = {
        addProductIds: ['dummy-add-product-id', 'dummy-add-product-id2'],
        removeProductIds: ['dummy-remove-product-id'],
        currentPageGroupId: 'dummy-current-page-group-id',
        appointmentIdToSync: appointmentId,
        pageName: 'dummy-page-name',
      };
      await saveInstantListingUpdate( params, apiInstanceMock);
      expect(patchMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/${appointmentId}/instant-listing`, params, {});
    })
  });
});
