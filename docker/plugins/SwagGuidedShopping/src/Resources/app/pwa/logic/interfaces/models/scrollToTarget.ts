export interface ScrollToTarget {
  elementId: string,
  attendeeId: string
  index?: number,
}
