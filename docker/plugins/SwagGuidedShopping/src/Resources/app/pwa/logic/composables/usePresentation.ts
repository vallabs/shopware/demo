import { computed, Ref, ref, watch } from "@vue/composition-api";
import {
  getApplicationContext,
  useSharedState,
  useIntercept,
} from "@shopware-pwa/composables";
import { PresentationService, QuickViewService } from "../api-client";
import { useMercure } from "./useMercure";
import { useMercureObserver } from "./useMercureObserver";
import { PresentationPageModel } from "../interfaces";
import { useErrorHandler } from "./useErrorHandler";
import { useInteractions } from "./useInteractions";
import { CmsPage } from "@shopware-pwa/commons/interfaces/models/content/cms/CmsPage";
import { useState } from "./useState";
import { useDynamicPage } from "./useDynamicPage";
import { useAppointment } from "./useAppointment";
import { useLastSeen } from "./useLastSeen";
import { useWatchers } from "./useWatchers";
import { isEqual } from 'lodash';

const COMPOSABLE_NAME = "usePresentation";

export const usePresentation = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { mercurePublish } = useMercure();
  const { handleError } = useErrorHandler();
  const { navigationPageIndex, newInstantListing, instantListingUpdated } =
    useMercureObserver();
  const { sharedRef } = useSharedState();
  const { addWatcher } = useWatchers();
  const { broadcast } = useIntercept();
  const { publishInteraction } = useInteractions();
  const { loadLastSeen } = useLastSeen();
  const { stateForAll, ended, started, resetCurrentClientForGuide } =
    useState();
  const _mediaStream = sharedRef<MediaStream>(`useMediaDevices-mediaStream`, null);
  const {
    dynamicPage,
    dynamicPageProductId,
    closeDynamicPage,
    resetSlidePosition,
  } = useDynamicPage();
  const { isSelfService, isClient, isGuide } = useAppointment();

  const _storePresentation = sharedRef<PresentationPageModel>(
    `${contextName}-page`,
    null
  );
  const _storeProductPage = sharedRef<CmsPage>(
    `${contextName}-productPage`,
    null
  );
  const _slideLoading = sharedRef<boolean>(
    `${contextName}-slideLoading`,
    false
  );

  const currentSlideIndex = sharedRef<number>(
    `${contextName}-currentSlideIndex`,
    1
  );

  const newSlideIndexAfterReload = sharedRef<number>(
    `${contextName}-newSlideIndexAfterReload`,
    0
  );

  const loadPresentationError: Ref<any> = ref(null);
  const watcherStarted: Ref<boolean> = ref(false);
  const timeout: Ref<any> = ref();

  const showProductPage = computed(
    () => dynamicPage.value?.type === "product" && dynamicPage.value?.opened
  );

  const totalCount = computed(
    () => _storePresentation.value?.navigation.length
  );
  const navigation = computed(() => _storePresentation.value?.navigation);
  const currentPage = computed(() => {
    if (!_storePresentation.value) return null;
    return _storePresentation.value.cmsPageResults[currentSlideIndex.value - 1]
      ?.cmsPage;
  });
  const currentPageResponse = computed(() => {
    if (showProductPage.value) return _storeProductPage.value;
    if (!_storePresentation.value) return null;

    return _storePresentation.value.cmsPageResults[currentSlideIndex.value - 1];
  });

  const currentMainPageResponse = computed(() => {
    if (!_storePresentation.value) return null;
    return _storePresentation.value.cmsPageResults[currentSlideIndex.value - 1];
  });

  const treeNavigation = computed(() => {
    let tree = {};
    _storePresentation.value?.navigation.forEach((item) => {
      if (typeof tree[item.groupId] == "undefined") {
        tree[item.groupId] = [];
      }
      if (typeof tree[item.groupId]["items"] == "undefined") {
        tree[item.groupId]["items"] = [];
      }
      tree[item.groupId]["items"].push(item);
      tree[item.groupId]["name"] = item.groupName;
    });

    return tree;
  });

  const indexOfFirstSlideInNextGroup = computed(() => {
    let currentGroupId = null;
    let slideIndexOfFirstSlideInNextGroup = 1;

    _storePresentation.value?.navigation.every((item) => {
      if (item.index === currentSlideIndex.value) currentGroupId = item.groupId;

      if (currentGroupId !== null && currentGroupId !== item.groupId) {
        slideIndexOfFirstSlideInNextGroup = item.index;
        return false;
      }

      slideIndexOfFirstSlideInNextGroup = item.index + 1;
      return true;
    });

    return slideIndexOfFirstSlideInNextGroup;
  });

  const indexOfFirstSlideInGroup = (params: { groupIndex: number }) => {
    const treeItems = Object.values(treeNavigation.value);
    if (!treeItems || !treeItems.length) return 0;

    const group: any = treeItems[params.groupIndex - 1];
    if (!group) return 0;

    return group.items[0].index;
  };

  const loadPresentation = async (
    forceSlide: number = 0,
    forceSlideGroup: number = 0
  ) => {
    try {
      // load dynamic product page if available first
      if (showProductPage.value && dynamicPage?.value?.type === "product") {
        await loadProductPage(dynamicPage.value.productId);
      }

      _storePresentation.value = await PresentationService.getPage(apiInstance);

      if (forceSlideGroup)
        forceSlide = indexOfFirstSlideInGroup({ groupIndex: forceSlideGroup });

      // initially set current page index
      if (forceSlide && isGuide.value) {
        currentSlideIndex.value = forceSlide;
        await goToSlide(currentSlideIndex.value);
      } else if (isSelfService.value) {
        const pageIndexFromSession = sessionStorage.getItem("sw-gs-navigated");
        if (forceSlide) {
          currentSlideIndex.value = forceSlide;
        } else if (pageIndexFromSession) {
          currentSlideIndex.value = parseInt(pageIndexFromSession);
        }
        await goToSlide(currentSlideIndex.value);
      } else if (stateForAll.value) {
        if (totalCount.value && !stateForAll.value.currentSlideAlias) {
          currentSlideIndex.value = 1;
        } else {
          currentSlideIndex.value = stateForAll.value.currentSlideAlias;
        }
        await goToSlide(currentSlideIndex.value);
      }
    } catch (e) {
      loadPresentationError.value = e.messages[0];
      handleError(e, {
        context: contextName,
        method: "loadPresentation",
      });
    }

    // only add watchers once on loadPresentation
    startPresentationWatchers();
  };

  const reloadPresentation = async () => {
    try {
      broadcast("usePresentation-reloadPresentation");
      let pageData = await PresentationService.getPage(apiInstance);
      pageData.cmsPageResults[currentSlideIndex.value - 1] =
      _storePresentation.value.cmsPageResults[currentSlideIndex.value - 1];
      _storePresentation.value = pageData;
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: "reloadPresentation",
      });
    }
  };

  const loadProductPage = async (id, publish = true) => {
    if (!id) return;

    if (publish) {
      await publishInteraction({
        name: "product.viewed",
        payload: {
          productId: id,
        },
      });
    }

    try {
      const params = {
        productId: id,
        cmsPageLayoutId: null,
      };

      // use or load configurated cmsPage if available
      if (stateForAll?.value?.productDetailDefaultPageId) {
        params.cmsPageLayoutId = stateForAll.value.productDetailDefaultPageId;
      }

      _storeProductPage.value = await QuickViewService.getQuickView(
        params,
        apiInstance
      );
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: "loadProductPage",
      });
    }

    if (!publish) return;
    loadLastSeen();
  };

  function setUniqueIdentifier(index) {
    if (
      _storePresentation.value?.cmsPageResults[index - 1]?.cmsPage.type ===
      "presentation_product_list"
    )
      return;
    if (
      !_storePresentation.value?.cmsPageResults[index - 1]?.cmsPage?.sections[0]
        ?._uniqueIdentifierBackup
    ) {
      _storePresentation.value.cmsPageResults[
        index - 1
      ].cmsPage.sections[0]._uniqueIdentifierBackup =
        _storePresentation.value.cmsPageResults[
          index - 1
        ].cmsPage.sections[0]._uniqueIdentifier;
    }
    _storePresentation.value.cmsPageResults[
      index - 1
    ].cmsPage.sections[0]._uniqueIdentifier =
      _storePresentation.value.cmsPageResults[index - 1].cmsPage.sections[0]
        ._uniqueIdentifierBackup +
      "|" +
      Date.now();
  }

  async function getSlideData(index, forceReload: boolean = false, cleanup: boolean = true) {
    if (!navigation.value) return;

    const navItem = navigation.value[index - 1];
    if (!navItem) return;

    try {
      const aliasIndex = index - 1;

      const requestedSlide = _storePresentation.value.cmsPageResults[aliasIndex];

      if (cleanup) {
        for (const [loopIndex, cmsPageResult] of Object.entries(_storePresentation.value.cmsPageResults)) {
          if (
            (parseInt(loopIndex) == aliasIndex - 1) || 
            (parseInt(loopIndex) == aliasIndex) || 
            (parseInt(loopIndex) == aliasIndex + 1)
          ) continue;
          _storePresentation.value.cmsPageResults[loopIndex] = null;
        }
      }

      if (!requestedSlide || !requestedSlide.loaded || forceReload) {
        _storePresentation.value.cmsPageResults[index - 1] =
          await PresentationService.getSlideData(
            navItem.groupId,
            navItem.sectionId,
            apiInstance
          );
        _storePresentation.value.cmsPageResults[index - 1].loaded = true;
        _storePresentation.value = Object.assign({}, _storePresentation.value);
      }
      setUniqueIdentifier(index);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: "getSlideData",
      });
    }
  }

  async function getSlideProducts(index: number, query: object, controller?: AbortController) {
    if (!navigation.value) return;
    const navItem = navigation.value[index - 1];
    if (!navItem) return;

    try {
      return await PresentationService.getSlideProducts(
        navItem.groupId,
        navItem.sectionId,
        query,
        apiInstance,
        controller,
      );
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: "getSlideProducts",
      });
    }
  }

  function goToSlideAfterReload(index) {
    _slideLoading.value = true;
    newSlideIndexAfterReload.value = index;
  }

  async function goToSlide(index, forceReload: boolean = false, cb: () => void = () => {}) {
    const navItem = _storePresentation.value?.navigation[index - 1];
    currentSlideIndex.value = index;
    _slideLoading.value = true;
    if (timeout.value) {
      clearTimeout(timeout.value);
      timeout.value = null;
    }
    timeout.value = setTimeout(async () => {
      if (dynamicPage?.value?.opened) {
        closeDynamicPage(dynamicPage.value);
      }

      await getSlideData(index, forceReload);

      resetSlidePosition(true);
      if (isSelfService.value) {
        mercurePublish.local("navigated", index);
        sessionStorage.setItem("sw-gs-navigated", index);
      } else {
        mercurePublish.guide("navigated", index);
      }

      if (navItem)
        publishInteraction({
          name: "page.viewed",
          payload: {
            pageId: navItem.groupId,
            sectionId: navItem.sectionId,
            slideAlias: parseInt(index),
          },
        });
      if(typeof cb === 'function') cb();
      _slideLoading.value = false;
    }, 500);
  }

  async function goToPreviousSlide() {
    if (currentSlideIndex.value <= 1) {
      return;
    }

    const newIndex = currentSlideIndex.value - 1;
    await goToSlide(newIndex, false, () => {
      // already pre load next slide data in the background
      getSlideData(newIndex - 1, false, false);
    });

  }

  async function goToNextSlide() {
    if (currentSlideIndex.value >= totalCount.value) {
      return;
    }

    const newIndex = currentSlideIndex.value + 1;
    await goToSlide(newIndex, false, () => {
      // already pre load next slide data in the background
      getSlideData(newIndex + 1, false, false);
    });
  }

  function checkIfClientIsWaiting() {
    return (
      isClient.value &&
      !isSelfService.value &&
      !stateForAll.value.started &&
      !stateForAll.value.running
    );
  }

  function reloadPage() {
    if (!navigation.value) return;
    if (!currentSlideIndex.value) return;
    broadcast("usePresentation-reloadPage");
    getSlideData(currentSlideIndex.value, true);
  }

  function startPresentationWatchers() {
    if (watcherStarted.value) return;
    watcherStarted.value = true;
    addWatcher(watch(dynamicPageProductId, async (id) => {
      if (!id) {
        resetSlidePosition();

        // reset product page
        _storeProductPage.value = null;
        return;
      }

      if (dynamicPage.value.type !== "product") return;
      loadProductPage(id);
    }));

    addWatcher(watch(navigationPageIndex, async (index: any) => {
      if (currentSlideIndex.value === index || checkIfClientIsWaiting()) {
        return;
      }
      
      _slideLoading.value = true;
      await getSlideData(index);
      currentSlideIndex.value = index;
      _slideLoading.value = false;
      resetSlidePosition(true);
    }));

    // actions when presentation has started
    addWatcher(watch(started, async () => {
      _slideLoading.value = true;
      await reloadPresentation();
      await getSlideData(stateForAll.value.currentSlideAlias);
      currentSlideIndex.value = stateForAll.value.currentSlideAlias;
      _slideLoading.value = false;
    }));

    // actions when presentation has ended
    addWatcher(watch(ended, async () => {
      if (ended.value) {
        // update the stored presentation structure
        await reloadPresentation();
        await resetCurrentClientForGuide();
        // go to last slide
        await goToSlide(totalCount.value);
        if (_mediaStream.value) {
          for (const track of _mediaStream.value.getTracks()) {
            await track.stop();
          }
        }
      }
    }));

    addWatcher(watch(newInstantListing, async () => {
      if (!newInstantListing.value) return;

      newInstantListing.value = false;

      await reloadPresentation();
      if (newSlideIndexAfterReload.value) {
        goToSlide(newSlideIndexAfterReload.value);
        newSlideIndexAfterReload.value = 0;
      }
    }));

    addWatcher(watch(instantListingUpdated, async (newValue, oldValue) => {
      if (!instantListingUpdated.value.updated || isEqual(newValue, oldValue)) return;
      await reloadPresentation();

      const finishUpdated = () => {
        instantListingUpdated.value = {
          ...instantListingUpdated.value,
          updated: false
        };
      }

      if (
        currentSlideIndex.value - 1 ===
        instantListingUpdated.value.currentIlIndex
      ) {
          _storePresentation.value.cmsPageResults[
            currentSlideIndex.value - 1
          ].loaded = false;

          await goToSlide(currentSlideIndex.value, undefined, finishUpdated);
      } else {
        finishUpdated();
      }
    }));
  }

  return {
    presentation: computed(() => _storePresentation.value),
    showProductPage,
    totalCount,
    currentSlideIndex,
    currentPage,
    currentPresentation: _storePresentation,
    currentProductPage: _storeProductPage,
    currentPageResponse,
    currentMainPageResponse,
    treeNavigation,
    loadPresentation,
    loadProductPage,
    getSlideData,
    getSlideProducts,
    goToSlide,
    goToSlideAfterReload,
    goToPreviousSlide,
    goToNextSlide,
    loadPresentationError,
    navigation,
    reloadPage,
    isProductListing: computed(() => !!currentMainPageResponse.value?.extensions?.cmsPageRelation?.isInstantListing),
    listingProductIds: computed(
      () =>
        currentMainPageResponse.value?.extensions?.cmsPageRelation
          ?.pickedProductIds
    ),
    listingProductStreamId: computed(
      () =>
        currentMainPageResponse.value?.extensions?.cmsPageRelation
          ?.productStreamId
    ),
    slideLoading: computed(() => _slideLoading.value),
    reloadPresentation,
    indexOfFirstSlideInNextGroup,
  };
};
