import { getApplicationContext, useSharedState } from '@shopware-pwa/composables';
import { computed } from "@vue/composition-api"
import { LastSeenService } from '../api-client';
import { Product } from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import { useErrorHandler } from './useErrorHandler';

const COMPOSABLE_NAME = 'useLastSeen';

export const useLastSeen = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const _storeLastSeen = sharedRef<Product[]>(`${contextName}-lastSeen`, []);
  const isLastSeenLoading = sharedRef<boolean>(`${contextName}-lastSeenLoading`, true);

  async function loadLastSeen() {
    isLastSeenLoading.value = true;
    try {
      const data = await LastSeenService.getLastSeenProducts(
        {},
        apiInstance
      );

      _storeLastSeen.value = data?.elements || [];
      isLastSeenLoading.value = false;
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadLastSeen',
      });
      isLastSeenLoading.value = false;
    }
  }

  return {
    loadLastSeen,
    isLastSeenLoading: computed(() => isLastSeenLoading.value),
    lastSeen: computed(() => _storeLastSeen.value)
  };
};
