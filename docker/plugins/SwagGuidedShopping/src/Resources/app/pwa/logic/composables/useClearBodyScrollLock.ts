import { clearAllBodyScrollLocks } from "body-scroll-lock";
import { onMounted, onBeforeUnmount } from '@vue/composition-api';

export const useClearBodyScrollLock = () => {

  onMounted(() => {
    clearBodyScrollLock();
  });
  onBeforeUnmount(() => {
    clearBodyScrollLock();
  });

  const clearBodyScrollLock = () => {
    setTimeout(() => {
      clearAllBodyScrollLocks();
    }, 1);
  };

  return {
    clearBodyScrollLock,
  }

};
