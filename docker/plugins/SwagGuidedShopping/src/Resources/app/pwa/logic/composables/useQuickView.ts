import { ref, computed } from '@vue/composition-api';
import { useSharedState, getApplicationContext, useUIState } from '@shopware-pwa/composables';
import { QuickViewService } from '../api-client';
import { useErrorHandler } from "./useErrorHandler";
import { Product } from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import { useState } from "./useState";
import { useInteractions } from "./useInteractions";
import { useLastSeen } from "./useLastSeen";

const COMPOSABLE_NAME = 'useQuickView';

export const useQuickView = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { handleError } = useErrorHandler();
  const { sharedRef } = useSharedState();
  const { stateForAll } = useState();
  const { publishInteraction } = useInteractions();
  const { loadLastSeen } = useLastSeen();
  const _quickViewCmsPage = sharedRef<any>(`${contextName}-page`, null);
  const _isLoading = sharedRef<any>(`${contextName}-loading`, false);
  const _product = ref(null);
  const { switchState: switchQuickViewModalState } = useUIState({ stateName: `GS_QUICKVIEW_MODAL_STATE` });

  async function getQuickViewCmsPage() {
    if (_product.value === null) return false;
    await QuickViewService.getQuickView({
      productId: _product.value.id,
      cmsPageLayoutId: stateForAll.value.quickviewPageId
    }, apiInstance).then(data => {
      _quickViewCmsPage.value = data;
    }).finally(() => {
      _isLoading.value = false
    }).catch(e => {
      handleError(e, {
        context: COMPOSABLE_NAME,
        method: 'getQuickViewCmsPage'
      });
    });
  }

  async function openQuickView(product) {
    setQuickViewProduct(product);
    switchQuickViewModalState(true);

    await publishInteraction({
      name: 'quickview.opened',
      payload: {
        productId: _product.value?.id,
      }
    });

    loadLastSeen();
  }

  async function setQuickViewProduct(product: Product) {
    const isNewProduct = isQuickViewProductChanging(product);
    if (!isNewProduct) return;
    setQuickViewLoadingState(isNewProduct);
    _product.value = product;
    await getQuickViewCmsPage();
  }

  function isQuickViewProductChanging(product: Product) {
    const currentId = _quickViewCmsPage?.value?.product?.id;
    const newId = product?.id;
    return currentId !== newId;
  }

  function setQuickViewLoadingState(state: boolean = null) {
    if (state == null) return;
    _isLoading.value = state;
  }

  return {
    quickViewCmsPage: computed(() => _quickViewCmsPage.value),
    isQuickViewLoading: computed(() => _isLoading.value),
    openQuickView,
    isQuickViewProductChanging,
    setQuickViewProduct,
    setQuickViewLoadingState,
  };
};
