import { ref } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { useInteractions } from "../useInteractions";
import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import * as GSApiClient from '../../api-client';
jest.mock('../../api-client');
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

describe("Composables - useInteractions", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();

    (mockedUseAppointment as any).mockImplementation(() => ({
      isSelfService: ref(true),
      isClient: ref(true),
    } as any));

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedGSApiClient.InteractionService = {
      send: () => new Promise(resolve => resolve({
        apiAlias: "dummyAlias"
      }))
    };

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("publishInteraction", () => {
      it("should send correct interaction", async () => {
        const { publishInteraction } = useInteractions();
        const result = await publishInteraction({
          name: 'page.viewed',
          payload: {
            pageId: '1',
            sectionId: '1',
            slideAlias: 1
          }
        });
        expect(result).toEqual({
          apiAlias: "dummyAlias"
        });
      });

      it("should send correct error when call API failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.InteractionService = {
          send: () => new Promise((resolve, reject) => reject(res))
        };
        const { publishInteraction } = useInteractions();
        await publishInteraction({
          name: 'page.viewed',
          payload: {
            pageId: '1',
            sectionId: '1',
            slideAlias: 1
          }
        });
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInteractions",
            method: "publishInteraction"
          }
        );
      });
    });

    describe("publishClientInteraction", () => {
      it("should not send anything when when current link is self service", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isSelfService: ref(true),
          isClient: ref(true),
        } as any));
        const { publishClientInteraction } = useInteractions();
        const result = await publishClientInteraction({
          name: 'page.viewed',
          payload: {
            pageId: '1',
            sectionId: '1',
            slideAlias: 1
          }
        });
        expect(result).toBeUndefined();
      });

      it("should not send anything when current link is not self service & not client", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isSelfService: ref(false),
          isClient: ref(false),
        } as any));
        
        const { publishClientInteraction } = useInteractions();
        const result = await publishClientInteraction({
          name: 'page.viewed',
          payload: {
            pageId: '1',
            sectionId: '1',
            slideAlias: 1
          }
        });
        expect(result).toBeUndefined();
      });
  
      it("should send correct interaction when current link is not self service & is client", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isSelfService: ref(false),
          isClient: ref(true),
        } as any));
        
        const { publishClientInteraction } = useInteractions();
        const result = await publishClientInteraction({
          name: 'page.viewed',
          payload: {
            pageId: '1',
            sectionId: '1',
            slideAlias: 1
          }
        });
        expect(result).toEqual({
          apiAlias: "dummyAlias"
        });
      });
    });
  });
});
