import {ref, onUnmounted} from "@vue/composition-api"

export const useWatchers = () => {
  const _unWatchers = ref<any[]>([]);

  const addWatcher = (watch) => {
    _unWatchers.value.push(watch)
  }

  onUnmounted(() => {
    _unWatchers.value.forEach(unwatch => {
      unwatch();
    })
  });

  return {
    addWatcher
  };
};
