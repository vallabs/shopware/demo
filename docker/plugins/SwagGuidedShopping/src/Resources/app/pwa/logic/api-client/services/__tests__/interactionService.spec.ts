import {
  send,
} from "../interactionService";

describe("Services - interactionService", () => {
  let postMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        post: postMock,
      },
    };
  });

  describe("send", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {id: '111'}
        });
      });
      const res = await send({}, apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/store-api/guided-shopping/interaction`, {});
      expect(res).toEqual({
        id: '111'
      })
    })
  });

});
