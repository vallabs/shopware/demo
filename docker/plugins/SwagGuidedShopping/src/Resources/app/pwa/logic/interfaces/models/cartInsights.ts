export interface CartInsights {
    cartSum: number,
    currencyId: string,
    currencySymbol: string,
    extensions: [],
    productCount: number,
    topProducts: CartInsightsTopProducts
}

export interface CartInsightsTopProducts {
    byQuantity: CartInsightsTopProduct[],
    byRevenue: CartInsightsTopProduct[]
}

export interface CartInsightsTopProduct {
    name?: string,
    productId: string,
    value: number,
    translated: {
        name: string
    }
}
