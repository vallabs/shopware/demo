import { ref } from "vue-demi";;

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { EventSourcePolyfill } from 'event-source-polyfill';
jest.mock('event-source-polyfill');
const mockedEventSourcePolyfill = EventSourcePolyfill as jest.Mocked<
  typeof EventSourcePolyfill
>;

import { MercureEventCallbacks, useMercure } from "../useMercure";

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;
const PRESENTATION_LAYER_NAMESPACE: string = 'presentationLayer';

describe("Composables - useMercure", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const broadcastMock = jest.fn();
  const XMLHttpRequestOpenMock = jest.fn();
  const XMLHttpRequestSendMock = jest.fn();
  const XMLHttpRequestSetRequestHeaderMock = jest.fn();

  const _isConnectedMock = ref<boolean>();
  const _showConnectErrorMock = ref<boolean>(false);
  const isTryingToConnectMock = ref<boolean>();
  const _lastMercureMsgIdMock = ref<string>();
  const eventCallbacksMock = ref<MercureEventCallbacks>({});
  const joinedAppointmentMock = ref(null);
  const appointmentMock = ref(null);
  const isGuideMock = ref(null);
  const isClientMock = ref(null);
  const isSelfServiceMock = ref(null);
  const highlightedElementMock = ref(null);
  const navigationPageIndexMock = ref(null);

  beforeEach(() => {
    jest.resetAllMocks();
    _isConnectedMock.value = false;
    _showConnectErrorMock.value = false;
    isTryingToConnectMock.value = false;
    _lastMercureMsgIdMock.value = null;
    eventCallbacksMock.value = {};
    joinedAppointmentMock.value = null;
    appointmentMock.value = null;
    isGuideMock.value = null;
    isClientMock.value = null;
    isSelfServiceMock.value = null;
    highlightedElementMock.value = null;
    navigationPageIndexMock.value = null;

    const xhrMock: Partial<XMLHttpRequest> = {
      open: XMLHttpRequestOpenMock,
      send: XMLHttpRequestSendMock,
      setRequestHeader: XMLHttpRequestSetRequestHeaderMock,
      readyState: 4,
      status: 200,
      response: 'Hello World!'
    };
    
    jest.spyOn(window, 'XMLHttpRequest').mockImplementation(() => xhrMock as XMLHttpRequest);

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("isConnected"))
            return _isConnectedMock;
          if (contextName.includes("showConnectError"))
            return _showConnectErrorMock;
          if (contextName.includes("isTryingToConnect"))
            return isTryingToConnectMock;
          if (contextName.includes("lastMercureMsgId"))
            return _lastMercureMsgIdMock;
          if (contextName.includes("eventCallbacks"))
            return eventCallbacksMock;
          if (contextName.includes("highlightedElement"))
            return highlightedElementMock;
          if (contextName.includes("navigationPageIndex"))
            return navigationPageIndexMock;
        }
      } as any
    });

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    (mockedUseAppointment as any).mockImplementation(() => ({
      joinedAppointment: joinedAppointmentMock,
      appointment: appointmentMock,
      isGuide: isGuideMock,
      isClient: isClientMock,
      isSelfService: isSelfServiceMock,
    } as any));

    mockedComposables.useIntercept.mockImplementation(() => {
      return {
        broadcast: broadcastMock,
      } as any;
    });

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("connect", () => {
      it("should return when isSelfService.value is true", () => {
        isSelfServiceMock.value = true;
        const { connect } = useMercure();
        connect();
        expect(mockedEventSourcePolyfill).not.toHaveBeenCalled();
      });

      it("should return when _isConnected.value is true", () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = true;
        const { connect } = useMercure();
        connect();
        expect(mockedEventSourcePolyfill).not.toHaveBeenCalled();
      });

      it("should return when isTryingToConnect.value is true", () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = true;
        const { connect } = useMercure();
        connect();
        expect(mockedEventSourcePolyfill).not.toHaveBeenCalled();
      });

      it("should return when !joinedAppointment.value is false", () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = false;
        const { connect } = useMercure();
        connect();
        expect(mockedEventSourcePolyfill).not.toHaveBeenCalled();
      });

      it("should return when !appointment.value is false", () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = null;
        const { connect } = useMercure();
        connect();
        expect(mockedEventSourcePolyfill).not.toHaveBeenCalled();
      });

      it("should work correctly when finish connect & received message with data is null", async () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a", "gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a", "gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a", "gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        const { onEventSourceMessage, mercureSubscribe, connect } = useMercure();
        connect();

        mercureSubscribe('navigated', (value) => {
          navigationPageIndexMock.value = parseInt(value);
          highlightedElementMock.value = '';
        }, 'presentationLayer');

        onEventSourceMessage({
          type: "message",
          target: {
              _listeners: {},
              url: "http://localhost:8081/.well-known/mercure?topic=gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a",
              readyState: 1,
              withCredentials: true,
              headers: {
                Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXJjdXJlIjp7InN1YnNjcmliZSI6WyJncy1jbGllbnQtYWN0aW9ucy0zNzllNGQ5NTFjZTI0ODI0YWI2YmQ4NTllYTRiNWQyYSIsImdzLWd1aWRlLWFjdGlvbnMtMzc5ZTRkOTUxY2UyNDgyNGFiNmJkODU5ZWE0YjVkMmEiLCJncy1wcmVzZW50YXRpb24tc3RhdGUtZm9yLWd1aWRlLTM3OWU0ZDk1MWNlMjQ4MjRhYjZiZDg1OWVhNGI1ZDJhIiwiZ3MtcHJlc2VudGF0aW9uLXN0YXRlLWZvci1hbGwtMzc5ZTRkOTUxY2UyNDgyNGFiNmJkODU5ZWE0YjVkMmEiXSwicHVibGlzaCI6W119fQ.ME6slOoCeIU4d6vRPPP1pmF72rQ1-fSy3vyaMHSbrHA"
              }
          },
          data: null,
          lastEventId: "urn:uuid:0b76ad57-fcea-43a6-9599-66bd53458a92"
        });
        
        expect(_lastMercureMsgIdMock.value).toEqual(null);
      });

      it("should work correctly when finish connect & received message with parsed data is null", async () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a", "gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a", "gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a", "gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        const { onEventSourceMessage, mercureSubscribe, connect } = useMercure();
        connect();

        mercureSubscribe('navigated', (value) => {
          navigationPageIndexMock.value = parseInt(value);
          highlightedElementMock.value = '';
        }, 'presentationLayer');

        onEventSourceMessage({
          type: "message",
          target: {
              _listeners: {},
              url: "http://localhost:8081/.well-known/mercure?topic=gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a",
              readyState: 1,
              withCredentials: true,
              headers: {
                Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXJjdXJlIjp7InN1YnNjcmliZSI6WyJncy1jbGllbnQtYWN0aW9ucy0zNzllNGQ5NTFjZTI0ODI0YWI2YmQ4NTllYTRiNWQyYSIsImdzLWd1aWRlLWFjdGlvbnMtMzc5ZTRkOTUxY2UyNDgyNGFiNmJkODU5ZWE0YjVkMmEiLCJncy1wcmVzZW50YXRpb24tc3RhdGUtZm9yLWd1aWRlLTM3OWU0ZDk1MWNlMjQ4MjRhYjZiZDg1OWVhNGI1ZDJhIiwiZ3MtcHJlc2VudGF0aW9uLXN0YXRlLWZvci1hbGwtMzc5ZTRkOTUxY2UyNDgyNGFiNmJkODU5ZWE0YjVkMmEiXSwicHVibGlzaCI6W119fQ.ME6slOoCeIU4d6vRPPP1pmF72rQ1-fSy3vyaMHSbrHA"
              }
          },
          data: 'null',
          lastEventId: "urn:uuid:0b76ad57-fcea-43a6-9599-66bd53458a92"
        });
        
        expect(_lastMercureMsgIdMock.value).toEqual('urn:uuid:0b76ad57-fcea-43a6-9599-66bd53458a92');
        expect(navigationPageIndexMock.value).toEqual(null);
        expect(highlightedElementMock.value).toEqual(null);
      });

      it("should work correctly when finish connect & received message with parsed data is valid", async () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a", "gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a", "gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a", "gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        const { onEventSourceMessage, mercureSubscribe, connect } = useMercure();
        connect();

        mercureSubscribe('navigated', (value) => {
          navigationPageIndexMock.value = parseInt(value);
          highlightedElementMock.value = '';
        }, 'presentationLayer');

        onEventSourceMessage({
          type: "message",
          target: {
              _listeners: {},
              url: "http://localhost:8081/.well-known/mercure?topic=gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a",
              readyState: 1,
              withCredentials: true,
              headers: {
                Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXJjdXJlIjp7InN1YnNjcmliZSI6WyJncy1jbGllbnQtYWN0aW9ucy0zNzllNGQ5NTFjZTI0ODI0YWI2YmQ4NTllYTRiNWQyYSIsImdzLWd1aWRlLWFjdGlvbnMtMzc5ZTRkOTUxY2UyNDgyNGFiNmJkODU5ZWE0YjVkMmEiLCJncy1wcmVzZW50YXRpb24tc3RhdGUtZm9yLWd1aWRlLTM3OWU0ZDk1MWNlMjQ4MjRhYjZiZDg1OWVhNGI1ZDJhIiwiZ3MtcHJlc2VudGF0aW9uLXN0YXRlLWZvci1hbGwtMzc5ZTRkOTUxY2UyNDgyNGFiNmJkODU5ZWE0YjVkMmEiXSwicHVibGlzaCI6W119fQ.ME6slOoCeIU4d6vRPPP1pmF72rQ1-fSy3vyaMHSbrHA"
              }
          },
          data: "{\"navigated\":2}",
          lastEventId: "urn:uuid:0b76ad57-fcea-43a6-9599-66bd53458a92"
        });
        
        expect(mockedEventSourcePolyfill).toHaveBeenCalledWith("http://localhost:8081/.well-known/mercure?topic=gs-client-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-guide-actions-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-guide-379e4d951ce24824ab6bd859ea4b5d2a&topic=gs-presentation-state-for-all-379e4d951ce24824ab6bd859ea4b5d2a", {
          withCredentials: true,
          headers: {
            Authorization: `Bearer ${appointmentMock.value.JWTMercureSubscriberToken}`,
          },
        });
        expect(_lastMercureMsgIdMock.value).toEqual('urn:uuid:0b76ad57-fcea-43a6-9599-66bd53458a92');
        expect(navigationPageIndexMock.value).toEqual(2);
        expect(highlightedElementMock.value).toEqual('');
      });

      it("should work correctly when finish reconnect", async () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        _lastMercureMsgIdMock.value = 'urn:uuid:0b76ad57-fcea-43a6-9599-66bd53458a92';
        const { connect } = useMercure();
        connect(true);

        expect(mockedEventSourcePolyfill).toHaveBeenCalledWith("http://localhost:8081/.well-known/mercure?topic=*&Last-Event-ID=urn%3Auuid%3A0b76ad57-fcea-43a6-9599-66bd53458a92", {
          withCredentials: true,
          headers: {
            Authorization: `Bearer ${appointmentMock.value.JWTMercureSubscriberToken}`,
          },
        });
      });

      it("should work correctly when finish connect & open connection", async () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        const { onEventSourceOpen, connected, connect, showConnectError } = useMercure();
        connect();

        onEventSourceOpen();
        expect(connected.value).toEqual(true);
        expect(showConnectError.value).toEqual(false);
        expect(isTryingToConnectMock.value).toEqual(false);
        expect(broadcastMock).not.toHaveBeenCalled();
      });

      it("should work correctly when finish connect & reopen connection", async () => {
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        const { onEventSourceReconnectedOpen, connected, connect, showConnectError } = useMercure();
        connect();

        onEventSourceReconnectedOpen();
        expect(connected.value).toEqual(true);
        expect(showConnectError.value).toEqual(false);
        expect(isTryingToConnectMock.value).toEqual(false);
        expect(broadcastMock).toHaveBeenCalledWith('mercure-reconnect', expect.anything());
      });

      it("should work correctly when finish connect & received error", async () => {
        jest.useFakeTimers();
        isSelfServiceMock.value = false;
        _isConnectedMock.value = false;
        isTryingToConnectMock.value = false;
        joinedAppointmentMock.value = true;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
        };
        isGuideMock.value = true;
        const { onEventSourceError, connected, connect, showConnectError } = useMercure();
        connect();
        jest.spyOn(global, 'setTimeout');
        onEventSourceError();
        jest.runAllTimers();
        expect(connected.value).toEqual(false);
        expect(showConnectError.value).toEqual(false);
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
        expect(showConnectError.value).toEqual(false);

        onEventSourceError();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(2);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 2000);
        expect(showConnectError.value).toEqual(false);

        onEventSourceError();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(3);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 4000);
        expect(showConnectError.value).toEqual(false);

        onEventSourceError();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(4);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 8000);
        expect(showConnectError.value).toEqual(true);

        onEventSourceError();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(5);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 16000);
        expect(showConnectError.value).toEqual(true);

        onEventSourceError();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(6);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 32000);
        expect(showConnectError.value).toEqual(true);

        onEventSourceError();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(7);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 64000);
        expect(showConnectError.value).toEqual(true);
      });


    });

    describe("mercurePublish.guide", () => {
      it("should call eventCallbacks.value when subscriber is a guide", () => {
        isGuideMock.value = true;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const navigatedCallback = jest.fn();
        mercureSubscribe('navigated', navigatedCallback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.guide('navigated', 2);
        expect(navigatedCallback).toHaveBeenCalled();
      });

      it("should not call eventCallbacks.value when subscriber is not a guide", () => {
        isGuideMock.value = false;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const navigatedCallback = jest.fn();
        mercureSubscribe('navigated', navigatedCallback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.guide('navigated', 2);
        expect(navigatedCallback).not.toHaveBeenCalled();
      });
    });

    describe("mercurePublish.client", () => {
      it("should call eventCallbacks.value when subscriber is a client", () => {
        isClientMock.value = true;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.client('client.hovered', 'id');
        
        expect(callback).toHaveBeenCalled();
      });

      it("should not call eventCallbacks.value when subscriber is not a client", () => {
        isClientMock.value = false;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.client('client.hovered', 'id');
        
        expect(callback).not.toHaveBeenCalled();
      });
    });

    describe("mercurePublish.all", () => {
      it("should not call eventCallbacks.value when isSelfService true", () => {
        isSelfServiceMock.value = true;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('client.hovered', 'id');
        
        expect(callback).not.toHaveBeenCalled();
      });

      it("should not call eventCallbacks.value when _isConnected false", () => {
        isSelfServiceMock.value = false;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('client.hovered', 'id');
        
        expect(callback).not.toHaveBeenCalled();
      });
      
      it("should call eventCallbacks.value when isSelfService false & _isConnected true & apiInstance true", () => {
        isSelfServiceMock.value = false;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const navigatedCallback = jest.fn();
        mercureSubscribe('navigated', navigatedCallback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('navigated', 2);
        expect(navigatedCallback).toHaveBeenCalled();
      });

      it("should publishEvent when appointment have correct value", () => {
        isSelfServiceMock.value = false;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
          JWTMercurePublisherToken: "testPubToken",
          mercurePublisherTopic: "*",
        };
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('client.hovered', 'id');
        
        expect(XMLHttpRequestOpenMock).toHaveBeenCalledWith('POST', 'http://localhost:8081/.well-known/mercure', true);
        expect(XMLHttpRequestSetRequestHeaderMock).toHaveBeenCalledWith('Content-type', 'application/x-www-form-urlencoded');
        expect(XMLHttpRequestSetRequestHeaderMock).toHaveBeenCalledWith('Authorization', 'Bearer testPubToken');
        expect(XMLHttpRequestSendMock).toHaveBeenCalledWith(
          new URLSearchParams({
            data: JSON.stringify({
              'client.hovered' : 'id'
            }),
            topic: "*",
            private: 'on'
          })
        );
      });

      it("should not publishEvent when appointment doesn't have JWTMercurePublisherToken", () => {
        isSelfServiceMock.value = false;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
          JWTMercurePublisherToken: null,
          mercurePublisherTopic: "*",
        };
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('client.hovered', 'id');
        
        expect(XMLHttpRequestOpenMock).not.toHaveBeenCalled();
        expect(XMLHttpRequestSetRequestHeaderMock).not.toHaveBeenCalled();
        expect(XMLHttpRequestSendMock).not.toHaveBeenCalled();
      });

      it("should not publishEvent when appointment doesn't have mercurePublisherTopic", () => {
        isSelfServiceMock.value = false;
        appointmentMock.value = {
          mercureHubPublicUrl: "http://localhost:8081/.well-known/mercure",
          mercureSubscriberTopics: ["*"],
          JWTMercureSubscriberToken: "testToken",
          JWTMercurePublisherToken: 'testPubToken',
          mercurePublisherTopic: null
        };
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const callback = jest.fn();
        mercureSubscribe('client.hovered', callback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('client.hovered', 'id');
        
        expect(XMLHttpRequestOpenMock).not.toHaveBeenCalled();
        expect(XMLHttpRequestSetRequestHeaderMock).not.toHaveBeenCalled();
        expect(XMLHttpRequestSendMock).not.toHaveBeenCalled();
      });

    });

    describe("mercurePublish.local", () => {
      it("should call eventCallbacks.value when connected", () => {
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const navigatedCallback = jest.fn();
        mercureSubscribe('navigated', navigatedCallback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.local('navigated', 2);
        expect(navigatedCallback).toHaveBeenCalled();
      });

      it("shouldn't call eventCallbacks.value when there is not any subscribeName similar", () => {
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const navigatedCallback = jest.fn();
        mercureSubscribe('navigated', navigatedCallback, PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.local('client.hovered', 2);
        expect(navigatedCallback).not.toHaveBeenCalled();
      });
    });


    describe("mercureUnsubscribe", () => {
      it("should not call eventCallbacks.value after unsubscribed", () => {
        isSelfServiceMock.value = false;
        const { connect, onEventSourceOpen, mercureSubscribe, mercurePublish, mercureUnsubscribe } = useMercure();
        // fake connection
        connect();
        onEventSourceOpen();

        const navigatedCallback = jest.fn();
        mercureSubscribe('navigated', navigatedCallback, PRESENTATION_LAYER_NAMESPACE);
        mercureUnsubscribe('navigated', PRESENTATION_LAYER_NAMESPACE);

        mercurePublish.all('navigated', 2);
        expect(navigatedCallback).not.toHaveBeenCalled();
      });
    });
  });

  describe("computed", () => {
    it("should return isConnected", () => {
      const { connected } = useMercure();
      expect(connected.value).toEqual(false);
    });

    it("should return showConnectError", () => {
      const { showConnectError } = useMercure();
      expect(showConnectError.value).toEqual(false);
    });
  });

});
