import {computed, ComputedRef, Ref, unref} from '@vue/composition-api'
import {useSharedState, getApplicationContext} from '@shopware-pwa/composables';

const COMPOSABLE_NAME = "useTabsUIState";

/**
 * Simple state management for UI purposes.
 *
 * @remarks
 * You need to pass a `stateName` on composable invocation (ex. `useTabsUIState({stateName: 'sidebarOffcanvas'})`), then
 * state is shared between all instances with this key.
 *
 * @example
 * ```ts
 * // Component1
 * const {activeTab, switchState} = useTabsUIState({stateName: 'SIDEBAR_OFFCANVAS_STATE'})
 * switchState(2)
 * ```
 */
export const useTabsUIState = (params?: { stateName?: Ref<string> | string }): {
  activeTab: ComputedRef<number>; switchState: (to: number) => void
} => {
  const contextName = COMPOSABLE_NAME;

  const stateName = unref(params?.stateName);
  getApplicationContext({contextName});

  const {sharedRef} = useSharedState();
  const _activeTab = sharedRef<number>(`sw-${contextName}-${stateName}`);

  function switchState(to: number) {
    if (stateName) {
      _activeTab.value = to !== undefined ? to : _activeTab.value;
    }
  }

  return {
    activeTab: computed(() => _activeTab.value),
    switchState,
  };
};
