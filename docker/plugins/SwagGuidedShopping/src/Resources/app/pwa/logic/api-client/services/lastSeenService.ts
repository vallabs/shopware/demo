import {ShopwareApiInstance} from '@shopware-pwa/shopware-6-client';
import {getLastSeenProductsEndpoint} from '../endpoints';
import {EntityResult} from "@shopware-pwa/commons/interfaces/response/EntityResult";
import {Product} from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import {ShopwareSearchParams} from "@shopware-pwa/commons/interfaces/search/SearchCriteria";
import {getProducts} from "@shopware-pwa/shopware-6-client";

export async function getLastSeenProducts(
  searchCriteria: ShopwareSearchParams,
  apiInstance: ShopwareApiInstance
): Promise<EntityResult<"product", Product[]>> {
  const resp = await apiInstance.invoke.get(getLastSeenProductsEndpoint());
  
  if (resp.data.collection.lastSeen.length === 0) return;

  return await getProducts({ids: resp.data.collection.lastSeen}, apiInstance)
}
