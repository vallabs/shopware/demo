import {
  useDefaults,
  useProductAssociations,
  useSharedState,
} from "@shopware-pwa/composables";
import { computed, ComputedRef, watch } from "@vue/composition-api";
import {
  CrossSelling,
  Product,
  ShopwareSearchParams,
  SearchFilterType,
  MultiFilter,
  EqualsFilter,
} from "@shopware-pwa/commons";
import { OrderService } from "../../api-client";
import { useErrorHandler } from "../useErrorHandler";
import { useAdminApiInstance } from "../useAdminApiInstance";
import dayjs from "dayjs";
import { ClientApiError } from "../../interfaces";

const COMPOSABLE_NAME = "useCrossSelling";

export const useCrossSelling = (product?: Product) => {
  const contextName = COMPOSABLE_NAME;
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const { adminApiInstance } = useAdminApiInstance();
  const { getIncludesConfig } = useDefaults({
    defaultsKey: "useProductListing",
  });
  const { loadAssociations: loadCrossSells, productAssociations } =
      useProductAssociations({ product, associationContext: "cross-selling" });

  const _crossSellTopMatches = sharedRef<Product[]>(
    `${contextName}-topMatches`,
    []
  );
  const crossSellCollection = sharedRef<CrossSelling[]>(
    `${contextName}-crossSellCollection`,
    []
  );
  const flattenedCrossSellProducts = computed(() =>
    crossSellCollection.value.reduce((accumulator, item) => {
      // fix: CrossSelling type does not contain "products" for some reason
      const crossSellItem = item as any;
      return accumulator.concat(crossSellItem.products);
    }, [])
  );
  const DATE_FORMAT: string = "YYYY-MM-DD";
  const currentDate = dayjs().format(DATE_FORMAT);
  const pastDate = dayjs().subtract(1, "year").format(DATE_FORMAT);

  async function loadCrossSellCollection() {
    if (!product) return;

    await loadCrossSells({
      method: "post",
      params: {
        associations: {
          seoUrls: {},
        },
        includes: getIncludesConfig(),
      },
    });

    crossSellCollection.value = productAssociations.value;
  }

  async function loadCrossSellTopMatches() {
    try {
      const productIds = flattenedCrossSellProducts.value.reduce(
        (accumulator, item) => {
          return accumulator.concat(item.id);
        },
        []
      );

      if (productIds.length === 0) return;

      const topMatchesCriteria: ShopwareSearchParams = {
        includes: {
          order_line_item: ["type", "id", "productId"],
        },
        filter: [
          {
            type: SearchFilterType.MULTI,
            operator: "or",
            queries: [
              {
                type: SearchFilterType.EQUALS_ANY,
                field: "product.parentId",
                value: null,
              },
              {
                type: SearchFilterType.EQUALS_ANY,
                field: "product.id",
                value: null,
              },
            ],
          },
          {
            type: SearchFilterType.RANGE,
            field: "order.orderDate",
            parameters: {
              gte: pastDate,
              lte: currentDate,
            },
          },
          {
            type: SearchFilterType.EQUALS,
            field: "order.stateMachineState.technicalName",
            value: "completed",
          },
        ],
        aggregations: [
          {
            name: "sum",
            type: "sum",
            field: "quantity",
          },
        ],
        //   total_count_mode: 0,
        limit: 10,
      };

      (topMatchesCriteria.filter[0] as MultiFilter).queries.forEach(
        (query) => ((query as EqualsFilter).value = productIds)
      );

      const lineItemResponse = await OrderService.getOrderLineItems(
        topMatchesCriteria,
        adminApiInstance
      );
      const products = [];
      const productsIds = [];

      lineItemResponse.forEach((product) => {
        const foundProduct = flattenedCrossSellProducts.value.find(
          (crossSellProduct) => crossSellProduct.id === product.productId
        );

        if (productsIds.indexOf(foundProduct.id) >= 0) return;
        productsIds.unshift(foundProduct.id);
        products.unshift(foundProduct);
      });

      _crossSellTopMatches.value = products;
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadCrossSellTopMatches",
      });
    }
  }

  return {
    loadCrossSellCollection,
    loadCrossSellTopMatches,
    crossSellCollection: computed(() => crossSellCollection.value),
    crossSellTopMatches: computed(() => _crossSellTopMatches.value),
    crossSellCount: computed(() => flattenedCrossSellProducts.value?.length),
  };
};
