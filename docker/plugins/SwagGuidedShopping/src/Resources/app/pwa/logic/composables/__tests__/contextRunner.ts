import { reactive } from "vue-demi";

export function prepareRootContextMock(extendContextMock?: any) {
  const rootContextMock: any = Object.assign(
    {
      apiInstance: jest.fn(), // $shopwareApiInstance
      shopwareDefaults: jest.fn(),
      i18n: {
        t: (text: string) => text,
      },
      router: {
        currentRoute: {
          query: null
        },
        push: jest.fn()
      },
      $routing: jest.fn(),
      sharedStore: reactive({}),
      interceptors: {},
    },
    extendContextMock || {}
  );
  return rootContextMock;
}
