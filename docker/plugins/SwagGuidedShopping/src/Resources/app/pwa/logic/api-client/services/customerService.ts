import {ShopwareAdminApiInstance} from '../../interfaces';
import {getAttendeeEndpoint, getAttendeeInsightsEndpoint} from '../endpoints';
import {CustomerInsightsCustomerData, CustomerInsightsAttendeeData} from '../../interfaces';
import {ShopwareSearchParams} from "@shopware-pwa/commons/interfaces/search/SearchCriteria";


export async function getCustomerData(
  criteria: ShopwareSearchParams,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<CustomerInsightsCustomerData> {
  const resp = await adminApiInstance.invoke.post(
    getAttendeeEndpoint(),
    criteria
  );

  return resp.data.data[0];
}

export async function getAttendeeData(
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<CustomerInsightsAttendeeData> {
  const resp = await adminApiInstance.invoke.get(getAttendeeInsightsEndpoint(appointmentId));

  return resp.data;
}
