import {
  addDislikedProduct,
  addDislikedProductForAttendee,
  addLikedProduct,
  addLikedProductForAttendee,
  getDislikedProducts,
  getDislikedProductsForAttendee,
  getLikedProducts,
  getLikedProductsForAttendee,
  getLikesAndDislikesForProduct,
  removeDislikedProduct,
  removeDislikedProductForAttendee,
  removeLikedProduct,
  removeLikedProductForAttendee,
} from "../likeListService";

describe("Services - likeListService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
      },
    };
  });

  describe("getLikedProducts", () => {
    it("when call successfully & get liked length greater than 1", async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            collection: {
              liked: ["1", "2", "3"],
            },
          },
        });
      });

      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await getLikedProducts({}, apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(
        "/store-api/guided-shopping/appointment/collection/liked"
      );
      expect(postMock).toHaveBeenCalledWith("/store-api/product", {
        filter: [{ field: "id", type: "equalsAny", value: ["1", "2", "3"] }],
      });
    });

    it("when call successfully & get liked length equal 0", async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            collection: {
              liked: [],
            },
          },
        });
      });

      await getLikedProducts({}, apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(
        "/store-api/guided-shopping/appointment/collection/liked"
      );
      expect(postMock).not.toHaveBeenCalled();
    });
  });

  describe("getDislikedProducts", () => {
    it("when call successfully & get liked length greater than 1", async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            collection: {
              disliked: ["1", "2", "3"],
            },
          },
        });
      });

      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await getDislikedProducts({}, apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(
        "/store-api/guided-shopping/appointment/collection/disliked"
      );
      expect(postMock).toHaveBeenCalledWith("/store-api/product", {
        filter: [{ field: "id", type: "equalsAny", value: ["1", "2", "3"] }],
      });
    });

    it("when call successfully & get liked length equal 0", async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            collection: {
              disliked: [],
            },
          },
        });
      });

      await getDislikedProducts({}, apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(
        "/store-api/guided-shopping/appointment/collection/disliked"
      );
      expect(postMock).not.toHaveBeenCalled();
    });
  });

  describe("addLikedProduct", () => {
    it("should works correctly", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await addLikedProduct("productId", apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(
        `/store-api/guided-shopping/appointment/collection/liked/productId`
      );
    });
  });

  describe("addDislikedProduct", () => {
    it("should works correctly", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await addDislikedProduct("productId", apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(
        `/store-api/guided-shopping/appointment/collection/disliked/productId`
      );
    });
  });

  describe("removeLikedProduct", () => {
    it("should works correctly", async () => {
      deleteMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await removeLikedProduct("productId", apiInstanceMock);
      expect(deleteMock).toHaveBeenCalledWith(
        `/store-api/guided-shopping/appointment/collection/liked/productId`
      );
    });
  });

  describe("removeDislikedProduct", () => {
    it("should works correctly", async () => {
      deleteMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await removeDislikedProduct("productId", apiInstanceMock);
      expect(deleteMock).toHaveBeenCalledWith(
        `/store-api/guided-shopping/appointment/collection/disliked/productId`
      );
    });
  });

  describe("addLikedProductForAttendee", () => {
    it("should works correctly when received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ id: "11111" }],
          },
        });
      });

      await addLikedProductForAttendee(
        "productId",
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "disliked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection/11111`
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection`,
        {
          alias: "liked",
          productId: "productId",
          attendeeId: "attendeeId",
        }
      );
    });

    it("should works correctly when NOT received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: null,
        });
      });

      await addLikedProductForAttendee(
        "productId",
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "disliked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).not.toHaveBeenCalled();
      expect(postMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection`,
        {
          alias: "liked",
          productId: "productId",
          attendeeId: "attendeeId",
        }
      );
    });
  });

  describe("addDislikedProductForAttendee", () => {
    it("should works correctly when received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ id: "11111" }],
          },
        });
      });

      await addDislikedProductForAttendee(
        "productId",
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "liked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection/11111`
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection`,
        {
          alias: "disliked",
          productId: "productId",
          attendeeId: "attendeeId",
        }
      );
    });

    it("should works correctly when NOT received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: null,
        });
      });

      await addDislikedProductForAttendee(
        "productId",
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "liked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).not.toHaveBeenCalled();
      expect(postMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection`,
        {
          alias: "disliked",
          productId: "productId",
          attendeeId: "attendeeId",
        }
      );
    });
  });

  describe("removeLikedProductForAttendee", () => {
    it("should works correctly when received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ id: "11111" }],
          },
        });
      });

      deleteMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await removeLikedProductForAttendee(
        apiInstanceMock,
        "productId",
        "attendeeId"
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "liked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection/11111`
      );
    });

    it("should works correctly when NOT received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: null,
        });
      });

      await removeLikedProductForAttendee(
        apiInstanceMock,
        "productId",
        "attendeeId"
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "liked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).not.toHaveBeenCalled();
    });
  });

  describe("removeDislikedProductForAttendee", () => {
    it("should works correctly when received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ id: "11111" }],
          },
        });
      });

      deleteMock.mockImplementation(() => {
        return Promise.resolve({
          data: {},
        });
      });

      await removeDislikedProductForAttendee(
        "productId",
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "disliked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).toHaveBeenCalledWith(
        `/api/guided-shopping-attendee-product-collection/11111`
      );
    });

    it("should works correctly when NOT received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: null,
        });
      });

      await removeDislikedProductForAttendee(
        "productId",
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "productId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "disliked",
                },
              ],
            },
          ],
        }
      );
      expect(deleteMock).not.toHaveBeenCalled();
    });
  });

  describe("getLikedProductsForAttendee", () => {
    it("should works correctly when received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ productId: "11" }],
          },
        });
      });

      await getLikedProductsForAttendee(
        apiInstanceMock,
        {},
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "liked",
                },
              ],
            },
          ],
        }
      );
      expect(postMock).toHaveBeenCalledWith("/store-api/product", {
        filter: [{ field: "id", type: "equalsAny", value: ["11"] }],
      });
    });

    it("should works correctly when NOT received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [],
          },
        });
      });

      await getLikedProductsForAttendee(
        apiInstanceMock,
        {},
        "attendeeId",
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "liked",
                },
              ],
            },
          ],
        }
      );
      expect(postMock).not.toHaveBeenCalledWith(
        "/store-api/product",
        expect.anything()
      );
    });
  });

  describe("getDislikedProductsForAttendee", () => {
    it("should works correctly when received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ productId: "11" }],
          },
        });
      });

      await getDislikedProductsForAttendee(
        {},
        "attendeeId",
        apiInstanceMock,
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "disliked",
                },
              ],
            },
          ],
        }
      );
      expect(postMock).toHaveBeenCalledWith("/store-api/product", {
        filter: [{ field: "id", type: "equalsAny", value: ["11"] }],
      });
    });

    it("should works correctly when NOT received id from getLikesDislikesForAttendeeEndpoint", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [],
          },
        });
      });

      await getDislikedProductsForAttendee(
        {},
        "attendeeId",
        apiInstanceMock,
        apiInstanceMock
      );
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          filter: [
            {
              type: "multi",
              operator: "and",
              queries: [
                {
                  type: "equals",
                  field: "attendeeId",
                  value: "attendeeId",
                },
                {
                  type: "equals",
                  field: "alias",
                  value: "disliked",
                },
              ],
            },
          ],
        }
      );
      expect(postMock).not.toHaveBeenCalledWith(
        "/store-api/product",
        expect.anything()
      );
    });
  });

  describe("getLikesAndDislikesForProduct", () => {
    it("should works correctly", async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{ productId: "11" }],
          },
        });
      });

      await getLikesAndDislikesForProduct("productId", apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(
        `/api/search/guided-shopping-attendee-product-collection`,
        {
          includes: {
            guided_shopping_attendee_product_collection: ["alias"],
          },
          filter: [
            {
              type: "multi",
              operator: "or",
              queries: [
                {
                  type: "equals",
                  field: "product.parentId",
                  value: "productId",
                },
                {
                  type: "equals",
                  field: "product.id",
                  value: "productId",
                },
              ],
            },
            {
              type: "equalsAny",
              field: "alias",
              value: ["liked", "disliked"],
            },
            {
              type: "equals",
              field: "attendee.type",
              value: "CLIENT",
            },
          ],
          aggregations: [
            {
              name: "sum",
              type: "terms",
              field: "alias",
            },
          ],
          total_count_mode: 0,
        }
      );
    });
  });
});
