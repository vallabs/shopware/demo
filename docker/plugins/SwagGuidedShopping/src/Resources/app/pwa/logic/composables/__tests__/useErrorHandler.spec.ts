import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
import { ClientApiError } from "../../interfaces";

describe("Composables - useErrorHandler", () => {
  const rootContextMock = prepareRootContextMock();
  const pushErrorMock = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();

    mockedComposables.useNotifications.mockImplementation(() => {
      return {
        pushError: pushErrorMock,
      } as any;
    });

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("handleError", () => {
      it("should executed correctly when both message & context & method valid", () => {
        const { handleError } = useErrorHandler();
        const err: ClientApiError = {
          statusCode: 400,
          message: 'dummy error message',
          messages: []
        };
        const options = {
          context: 'useDummyContext',
          method: 'GET'
        };
        handleError(err, options);
        expect(consoleErrorSpy).toHaveBeenCalledWith(`[useDummyContext][GET] (400) dummy error message`);
      });

      it("should executed correctly when both message & context valid & method invalid", () => {
        const { handleError } = useErrorHandler();
        const err: ClientApiError = {
          statusCode: 400,
          message: 'dummy error message',
          messages: []
        };
        const options = {
          context: 'useDummyContext',
        };
        handleError(err, options);
        expect(consoleErrorSpy).toHaveBeenCalledWith(`[useDummyContext] (400) dummy error message`);
      });

      it("should executed correctly when message valiad and both context & method invalid", () => {
        process.env = null 
        const { handleError } = useErrorHandler();
        const err: ClientApiError = {
          statusCode: 400,
          message: 'dummy error message',
          messages: []
        };
        const options = {
          context: '',
        };
        handleError(err, options);
        expect(consoleErrorSpy).toHaveBeenCalledWith(`(400) dummy error message`);
      });

      it("should executed correctly when IS_DEV true", () => {
        process.env = {}; 
        process.env.NODE_ENV = 'development';
        const { handleError } = useErrorHandler();
        const err: ClientApiError = {
          statusCode: 400,
          message: 'dummy error message',
          messages: []
        };
        const options = {
          context: '',
        };
        handleError(err, options);
        expect(pushErrorMock).toHaveBeenCalledWith(`(400) dummy error message`, {"timeout": 4000});
      });

      it("should executed correctly when message invalid but messages valid", () => {
        process.env = {}; 
        process.env.NODE_ENV = 'development';
        const { handleError } = useErrorHandler();
        const err: ClientApiError = {
          statusCode: 400,
          message: '',
          messages: [
            {
              title: 'dummyTitle1',
              status: 'dummyStatus1',
              code: '400',
              detail: 'dummyDetail1',
              meta: {
                parameters: {}
              },
            },
            {
              title: 'dummyTitle2',
              status: 'dummyStatus2',
              code: '404',
              detail: 'dummyDetail2',
              meta: {
                parameters: {}
              },
            }
          ]
        };
        const options = {
          context: '',
        };
        handleError(err, options);
        expect(consoleErrorSpy).toHaveBeenCalledTimes(2);
        expect(consoleErrorSpy).toHaveBeenCalledWith(`dummyTitle1 (dummyStatus1): (400) dummyDetail1`);
        expect(consoleErrorSpy).toHaveBeenCalledWith(`dummyTitle2 (dummyStatus2): (404) dummyDetail2`);
      });
    });
  });

});
