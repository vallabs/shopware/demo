import {
  getCustomerData,
  getAttendeeData,
} from "../customerService";

describe("Services - customerService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
      },
    };
  });

  describe("getCustomerData", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [{
              id: '111'
            }]
          },
        });
      });
      const res = await getCustomerData({}, apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith('/api/search/guided-shopping-appointment-attendee', {});
      expect(res).toEqual({
        id: '111'
      })
    })
  });

  describe("getAttendeeData", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            id: '111'
          },
        });
      });
      const res = await getAttendeeData('appointmentDummyId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/appointmentDummyId/widgets/attendee-insights`);
      expect(res).toEqual({
        id: '111'
      })
    })
  });
});
