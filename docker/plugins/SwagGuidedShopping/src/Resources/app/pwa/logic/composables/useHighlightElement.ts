import {computed, ref} from '@vue/composition-api';
import {useMercure} from './useMercure';
import {useMercureObserver} from './useMercureObserver';

export const useHighlightElement = (elementId: string) => {
  const {mercurePublish} = useMercure();
  const {highlightedElement} = useMercureObserver();
  const elementIsHighlighted = computed(() => highlightedElement.value === elementId);
  const elementHighlightLoading = ref<boolean>(false);

  async function publishHighlightElement() {
    const value = (elementIsHighlighted.value) ? 'empty' : elementId;
    mercurePublish.guide('highlighted', value);

    // always fake a loading time of 1sec so the client sees interaction
    setTimeout(() => {
      elementHighlightLoading.value = false;
    }, 1000);
  }

  return {
    elementIsHighlighted,
    elementHighlightLoading,
    publishHighlightElement,
  };
};
