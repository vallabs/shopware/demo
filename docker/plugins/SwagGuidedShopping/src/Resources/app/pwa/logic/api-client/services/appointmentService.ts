import {ShopwareApiInstance} from '@shopware-pwa/shopware-6-client';
import {AppointmentModel, ShopwareAdminApiInstance, AttendeeData} from '../../interfaces';
import {
  getJoinAsGuideEndpoint,
  getJoinAsClientEndpoint,
  getAppointmentStartEndpoint,
  getAppointmentEndEndpoint,
  updateAttendeeEndpoint,
  getAppointmentByPathEndpoint,
  getAttendeeContextTokenEndpoint,
  respondInvitationEndpoint,
  downloadCalendarFile,
  getVideoAndAudioSettings
} from '../endpoints';
import { AppointmentInvitationResponse, InvitationRequest } from '../../composables/useInvitation';

interface AppointmentVideoAndAudioSetting {
  apiAlias: 'appointment_video_audio_settings';
  videoAudioSettings: string;
}

export function startAppointment(appointmentId: string, adminApiInstance: ShopwareAdminApiInstance): Promise<void> {
  return adminApiInstance.invoke.post(getAppointmentStartEndpoint(appointmentId));
}

export function endAppointment(appointmentId: string, adminApiInstance: ShopwareAdminApiInstance): Promise<void> {
  return adminApiInstance.invoke.post(getAppointmentEndEndpoint(appointmentId));
}

export async function joinAsGuide(appointmentId: string, adminApiInstance: ShopwareAdminApiInstance): Promise<AppointmentModel> {
  const appointmentResp = await adminApiInstance.invoke.post(
    getAppointmentByPathEndpoint(),
    {
      filter: [
        {
          type: 'equals',
          field: 'presentationPath',
          value: appointmentId
        }
      ]
    }
  );

  let id = appointmentResp?.data?.data[0]?.id || appointmentId;
  if (!id) return null;

  const joinResp = await adminApiInstance.invoke.post(getJoinAsGuideEndpoint(id));
  // add appointment id
  joinResp.data.id = id;

  return joinResp.data as AppointmentModel;
}

export function joinAsClient(appointmentId: string, attendeeName: string, apiInstance: ShopwareApiInstance): Promise<AppointmentModel> {
  return apiInstance.invoke.post(
    getJoinAsClientEndpoint(appointmentId),
    {
      attendeeName: attendeeName
    }).then((response) => {
    return response.data as AppointmentModel;
  });
}

export function updateAttendee(data: AttendeeData, apiInstance: ShopwareApiInstance): Promise<void> {
  return apiInstance.invoke.patch(
    updateAttendeeEndpoint(),
    data
  );
}

export async function getAttendeeContextToken(attendeeId: string, adminApiInstance: ShopwareAdminApiInstance): Promise<string> {
  const tokenResponse = await adminApiInstance.invoke.get(
    getAttendeeContextTokenEndpoint(attendeeId),
  );
  return tokenResponse ? tokenResponse.data['attendee-sw-context-token'] : '';
}

export async function respondAppointmentInvitation(appointmentId: string, data: InvitationRequest, apiInstance: ShopwareApiInstance): Promise<AppointmentInvitationResponse> {
  const res = await apiInstance.invoke.patch(respondInvitationEndpoint(appointmentId), data);
  return res.data;
}

export async function downloadInvitationFile(appointmentId: string, token: string, apiInstance: ShopwareApiInstance): Promise<any> {
  const res = await apiInstance.invoke.post(downloadCalendarFile(appointmentId), { token }, {
    responseType: 'blob'
  });
  return res.data;
}

export async function getAppointmentVideoAndAudioSetting(
  appointmentId: string,
  apiInstance: ShopwareApiInstance,
): Promise<AppointmentVideoAndAudioSetting> {
  return apiInstance.invoke.get(getVideoAndAudioSettings(appointmentId)).then((response) => {
    return response.data as AppointmentVideoAndAudioSetting;
  });
};
