import {ShopwareApiInstance} from '@shopware-pwa/shopware-6-client';
import {
  getLikedProductsEndpoint,
  getDislikedProductsEndpoint,
  addLikedProductEndpoint,
  addDislikedProductEndpoint,
  removeLikedProductEndpoint,
  removeDislikedProductEndpoint,
  getLikesDislikesForAttendeeEndpoint,
  addLikesDislikesForAttendeeEndpoint,
  removeLikesDislikesForAttendeeEndpoint,
  getProductsEndpoint
} from '../endpoints';
import {EntityResult} from "@shopware-pwa/commons/interfaces/response/EntityResult";
import {Product} from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import {ShopwareSearchParams} from "@shopware-pwa/commons/interfaces/search/SearchCriteria";
import {ShopwareAdminApiInstance} from '../../interfaces';

export async function getLikedProducts(
  searchCriteria: ShopwareSearchParams,
  apiInstance: ShopwareApiInstance
): Promise<EntityResult<"product", Product[]>> {
  const getLikeResponse = await apiInstance.invoke.get(getLikedProductsEndpoint());

  if (getLikeResponse.data.collection.liked.length === 0) {
    return;
  }

  return await getProductsFromIdList(searchCriteria, getLikeResponse.data.collection.liked, apiInstance);
}

export async function getDislikedProducts(
  searchCriteria: ShopwareSearchParams,
  apiInstance: ShopwareApiInstance
): Promise<EntityResult<"product", Product[]>> {
  const getDislikeResponse = await apiInstance.invoke.get(getDislikedProductsEndpoint());

  if (getDislikeResponse.data.collection.disliked.length === 0) {
    return;
  }

  return await getProductsFromIdList(searchCriteria, getDislikeResponse.data.collection.disliked, apiInstance);
}

export async function addLikedProduct(
  productId: string,
  apiInstance: ShopwareApiInstance
): Promise<any> {
  const addLikeResponse = await apiInstance.invoke.post(addLikedProductEndpoint(productId));
  return addLikeResponse.data;
}

export async function addLikedProductForAttendee(
  productId: string,
  attendeeId: string,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<any> {
  // get dislike entry for product
  const getDislikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'productId',
              value: productId
            },
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'disliked'
            }
          ]
        }
      ]
    }
  );

  // remove dislike if it exists
  let id = getDislikeResponse?.data?.data[0]?.id;
  if (id) {
    await adminApiInstance.invoke.delete(removeLikesDislikesForAttendeeEndpoint(id));
  }

  // add new like entry
  const addLikeResponse = await adminApiInstance.invoke.post(
    addLikesDislikesForAttendeeEndpoint(),
    {
      alias: 'liked',
      productId: productId,
      attendeeId: attendeeId
    }
  );

  return addLikeResponse.data;
}

export async function addDislikedProduct(
  productId: string,
  apiInstance: ShopwareApiInstance
): Promise<any> {
  const addDislikeResponse = await apiInstance.invoke.post(addDislikedProductEndpoint(productId));
  return addDislikeResponse.data;
}

export async function addDislikedProductForAttendee(
  productId: string,
  attendeeId: string,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<any> {
  // get like entry for product
  const getLikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'productId',
              value: productId
            },
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'liked'
            }
          ]
        }
      ]
    }
  );

  // remove like if it exists
  let id = getLikeResponse?.data?.data[0]?.id;
  if (id) {
    await adminApiInstance.invoke.delete(removeLikesDislikesForAttendeeEndpoint(id));
  }

  // add new dislike entry
  const addDislikeResponse = await adminApiInstance.invoke.post(
    addLikesDislikesForAttendeeEndpoint(),
    {
      alias: 'disliked',
      productId: productId,
      attendeeId: attendeeId
    }
  );

  return addDislikeResponse.data;
}

export async function removeLikedProduct(
  productId: string,
  apiInstance: ShopwareApiInstance
): Promise<any> {
  const removeLikeResponse = await apiInstance.invoke.delete(removeLikedProductEndpoint(productId));
  return removeLikeResponse.data;
}

export async function removeLikedProductForAttendee(
  adminApiInstance: ShopwareAdminApiInstance,
  productId: string,
  attendeeId: string
): Promise<any> {
  // get like entry of product
  const getLikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'productId',
              value: productId
            },
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'liked'
            }
          ]
        }
      ]
    }
  );

  let id = getLikeResponse?.data?.data[0]?.id;
  if (!id) return null;

  // remove like
  const removeLikeResponse = await adminApiInstance.invoke.delete(removeLikesDislikesForAttendeeEndpoint(id));
  return removeLikeResponse.data;
}

export async function removeDislikedProduct(
  productId: string,
  apiInstance: ShopwareApiInstance
): Promise<any> {
  const removeDislikeResponse = await apiInstance.invoke.delete(removeDislikedProductEndpoint(productId));
  return removeDislikeResponse.data;
}

export async function removeDislikedProductForAttendee(
  productId: string,
  attendeeId: string,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<any> {
  // get dislike entry of product
  const getDislikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'productId',
              value: productId
            },
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'disliked'
            }
          ]
        }
      ]
    }
  );

  let id = getDislikeResponse?.data?.data[0]?.id;
  if (!id) return null;

  // remove dislike
  const removeDislikeResponse = await adminApiInstance.invoke.delete(removeLikesDislikesForAttendeeEndpoint(id));
  return removeDislikeResponse.data;
}

export async function getLikedProductsForAttendee(
  apiInstance: ShopwareApiInstance,
  searchCriteria: ShopwareSearchParams,
  attendeeId: string,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<any> {
  // get product ids of likes
  const getLikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'liked'
            }
          ]
        }
      ]
    }
  );

  let productIds = [];
  getLikeResponse.data.data.forEach((product) => {
    productIds.push(product.productId);
  });

  if (productIds.length === 0) return null;

  // get product data
  const products = await getProductsFromIdList(searchCriteria, productIds, apiInstance);
  return products;
}

export async function getDislikedProductsForAttendee(
  searchCriteria: ShopwareSearchParams,
  attendeeId: string,
  apiInstance: ShopwareApiInstance,
  adminApiInstance: ShopwareAdminApiInstance
): Promise<any> {
  // get product ids of dislikes
  const getDislikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'disliked'
            }
          ]
        }
      ]
    }
  );

  let productIds = [];
  getDislikeResponse.data.data.forEach((product) => {
    productIds.push(product.productId);
  });

  if (productIds.length === 0) return null;

  // get product data
  const products = await getProductsFromIdList(searchCriteria, productIds, apiInstance);
  return products;
}

export async function getProductsFromIdList(
  searchCriteria: ShopwareSearchParams,
  idList: string[],
  apiInstance: ShopwareApiInstance
) {
  const resp = await apiInstance.invoke.post(
    getProductsEndpoint(),
    {
      ...searchCriteria,
      filter: [
        {
          type: 'equalsAny',
          field: 'id',
          value: idList
        }
      ]
    }
  );

  return resp.data;
}

export async function getLikesAndDislikesForProduct(
    productId: string,
    adminApiInstance: ShopwareAdminApiInstance
): Promise<any> {
  // get total likes and dislike of the product
  const getLikeResponse = await adminApiInstance.invoke.post(
      getLikesDislikesForAttendeeEndpoint(),
      {
        includes: {
          guided_shopping_attendee_product_collection: [
            'alias'
          ]
        },
        filter: [
          {
            type: 'multi',
            operator: 'or',
            queries: [
              {
                type: 'equals',
                field: 'product.parentId',
                value: productId
              },
              {
                type: 'equals',
                field: 'product.id',
                value: productId
              }
            ]
          },
          {
            type: 'equalsAny',
            field: 'alias',
            value: [
              'liked',
              'disliked'
            ]
          },
          {
            type: 'equals',
            field: 'attendee.type',
            value: 'CLIENT'
          }
        ],
        aggregations: [
          {
            name: 'sum',
            type: 'terms',
            field: 'alias'
          }
        ],
        total_count_mode: 0
      }
  );

  return getLikeResponse.data.data;
}
