import {computed, ComputedRef, ref, Ref, watch} from "vue-demi";
import {getProducts} from "@shopware-pwa/shopware-6-client";

import {ClientApiError} from "@shopware-pwa/commons/interfaces/errors/ApiError";
import {Cart} from "@shopware-pwa/commons/interfaces/models/checkout/cart/Cart";
import {EntityError} from "@shopware-pwa/commons/interfaces/models/common/EntityError";
import {Product} from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import {LineItem} from "@shopware-pwa/commons/interfaces/models/checkout/cart/line-item/LineItem";
import {getApplicationContext, useDefaults, useSharedState,} from "@shopware-pwa/composables";

import {useAdminApiInstance} from "./useAdminApiInstance";
import {useState} from "./useState";
import {CartService} from '../api-client';
import {useAppointment} from "./useAppointment";
import {useMercureObserver} from "./useMercureObserver";
import {useMercure} from "./useMercure";
import {useCustomerInsights} from "./widgets/useCustomerInsights";
import {useCartInsights} from "./widgets/useCartInsights";
import { useWatchers } from "./useWatchers";


type MinimumLineItem = {
  id: string,
  quantity?: number,
}

/**
 * Composable for cart management of the client by the guide.
 */
export function useGsCart() {
  const COMPOSABLE_NAME = "useGsCart";
  const contextName = COMPOSABLE_NAME;
  const {apiInstance} = getApplicationContext({contextName});
  const {getDefaults} = useDefaults({
    defaultsKey: COMPOSABLE_NAME,
  });
  const quantity: Ref<number> = ref(1);
  const loading: Ref<boolean> = ref(false);
  const error: Ref<any> = ref(null);
  const initialized: Ref<boolean> = ref(false);
  const {sharedRef} = useSharedState();
  const _storeCart = sharedRef<Cart>(`${contextName}-cart`);
  const disabledCartCount = sharedRef<number>(`${contextName}-disabledCartCount`, 0);
  const {adminApiInstance} = useAdminApiInstance();
  const {currentClientForGuide, currentValidToken, currentClientTokenPermissions, getContextTokenForCurrentClient} = useState();
  const {isGuide, isClient, appointment} = useAppointment();
  const { addWatcher } = useWatchers();
  const {lastClientChangedCart, lastGuideChangedCart } = useMercureObserver();
  const {mercurePublish} = useMercure();
  const {attendeeData, loadAttendeeData} = useCustomerInsights();
  const {loadCartInsights} = useCartInsights();
  const changeLineItemController = ref<AbortController>();
  const refreshCartController = ref<AbortController>();

  function requestTokenPermissions(attendeeId?: string) {
    if (!attendeeId && !currentClientForGuide.value) return;
    mercurePublish.guide('requestedTokenPermissions', attendeeId || currentClientForGuide.value);
  }

  async function refreshCart(): Promise<void> {
    loading.value = true;

    try {

      if (isGuide.value) {
        loadCartInsights();
        const attendeeDataLoaded = loadAttendeeData();// do this to update customer widget always (do not register 2 watchers to one event because the flag is used double)

        if (!currentClientForGuide.value) {
          _storeCart.value = null;
          return;
        }

        const token = await getContextTokenForCurrentClient();

        if (!token) {
          _storeCart.value = null;

          await attendeeDataLoaded;
          disabledCartCount.value = attendeeData.value?.attendees[currentClientForGuide.value]?.productCount;

          return;
        }

        disabledCartCount.value = 0;
        if (refreshCartController.value) {
          refreshCartController.value.abort();
        }
        refreshCartController.value = new AbortController();
        _storeCart.value = await CartService.Guide.getCart(
          token,
          appointment.value.salesChannelId,
          adminApiInstance,
          refreshCartController.value
        );
      }


      if (isClient.value) {
        if (refreshCartController.value) {
          refreshCartController.value.abort();
        }
        refreshCartController.value = new AbortController();
        _storeCart.value = await CartService.Client.getCart(apiInstance, refreshCartController.value);
      }

    } catch (e) {
      // _storeCart.value = null;
      const err: ClientApiError = e;
      error.value = err.messages;
    } finally {
      loading.value = false;
    }
  }

  async function addProducts(items: MinimumLineItem[]) {
    if (items.length === 0) return null;
    let result;

    const itemsToAdd: MinimumLineItem[] = [];
    items.forEach(item => {
      if (!item.quantity || item.quantity == 0) {
        item.quantity = quantity.value;
      }

      itemsToAdd.push({
        id: item.id,
        quantity: item.quantity,
      })
    });

    if (isGuide.value && currentClientForGuide.value) {
      const token = await getContextTokenForCurrentClient();
      if (!token) return;

      result = await CartService.Guide.addLineItems(token, appointment.value.salesChannelId, itemsToAdd, adminApiInstance);
    } else if (isClient.value) {
      result = await CartService.Client.addLineItems(itemsToAdd, apiInstance);
    }

    _storeCart.value = result;
    return result;
  }

  async function changeProducts(items: MinimumLineItem[]) {
    if (items.length === 0) return null;
    let result;

    if (isGuide.value && currentClientForGuide.value) {
      const token = await getContextTokenForCurrentClient();
      if (!token) return;
      if (changeLineItemController.value) {
        changeLineItemController.value.abort();
      }
      changeLineItemController.value = new AbortController();

      result = await CartService.Guide.changeLineItemQuantities(token, appointment.value.salesChannelId, items, adminApiInstance, changeLineItemController.value);
    } else if (isClient.value) {
      if (changeLineItemController.value) {
        changeLineItemController.value.abort();
      }
      changeLineItemController.value = new AbortController();
      result = await CartService.Client.changeLineItemQuantities(items, apiInstance, changeLineItemController.value);
    }

    _storeCart.value = result;
    return result;
  }

  async function removeProducts(items: MinimumLineItem[]) {
    if (items.length === 0) return null;
    let result;

    if (isGuide.value && currentClientForGuide.value) {
      const token = await getContextTokenForCurrentClient();
      if (!token) return;

      result = await CartService.Guide.removeLineItems(token, appointment.value.salesChannelId, items, adminApiInstance);
    } else if (isClient.value) {
      result = await CartService.Client.removeLineItems(items, apiInstance);
    }

    _storeCart.value = result;
    return result;
  }

  async function getProductItemsSeoUrlsData(): Promise<Partial<Product>[]> {
    if (!cartItems.value.length) {
      return [];
    }
    try {
      const result = await getProducts(
        {
          ids: cartItems.value.map(
            ({referencedId}) => referencedId
          ) as string[],
          includes: (getDefaults() as any).getProductItemsSeoUrlsData.includes,
          associations: (getDefaults() as any).getProductItemsSeoUrlsData
            .associations,
        },
        apiInstance
      );
      return result?.elements || [];
    } catch (error) {
      console.error("[useGsCart][getProductItemsSeoUrlsData]", error.messages);
    }

    return [];
  }

  const appliedPromotionCodes = computed(() => {
    return cartItems.value.filter(
      (cartItem: LineItem) => cartItem.type === "promotion"
    );
  });

  const cart: ComputedRef<Cart | null> = computed(() => _storeCart.value);

  const cartItems = computed(() => {
    return cart.value ? cart.value.lineItems || [] : [];
  });

  const count = computed(() => {
    return cartItems.value.reduce(
      (accumulator: number, lineItem: LineItem) =>
        lineItem.type === "product"
          ? lineItem.quantity + accumulator
          : accumulator,
      0
    );
  });

  const totalPrice = computed(() => {
    const cartPrice = cart.value?.price?.totalPrice || 0;
    const netPrice = cart.value?.price?.netPrice || 0;
    if (['net', 'tax-free'].includes(cart.value?.price?.taxStatus)) {
      return netPrice;
    }
    return cartPrice;
  });

  const shippingTotal = computed(() => {
    const shippingTotal =
      cart.value?.deliveries?.[0]?.shippingCosts?.totalPrice;
    return shippingTotal || 0;
  });

  const subtotal = computed(() => {
    const cartPrice = cart.value?.price?.positionPrice;
    return cartPrice || 0;
  });

  const cartErrors: ComputedRef<EntityError[]> = computed(
    () => (cart.value?.errors && Object.values(cart.value.errors)) || []
  );

  // init function which is called only once in guide page
  async function initCartForGuide() {
    if (initialized.value) return;
    initialized.value = true;

    initCartForGuideWatcher();

    // load cart for first client (if he already exists)
    if (currentClientForGuide.value) {
      await refreshCart();
    }

  }

  function initCartForGuideWatcher() {
    // watch for changes and refresh the cart or clear it
    addWatcher(watch(currentValidToken, () => {
      refreshCart();
    }));

    // reload cart for guide if current client changed something in his cart
    addWatcher(watch(lastClientChangedCart, () => {
      if (!lastClientChangedCart.value) return;
      lastClientChangedCart.value = '';
      refreshCart();
    }));

    // reload cart for guide if other guide changed something in his cart
    addWatcher(watch(lastGuideChangedCart, () => {
      if (!lastGuideChangedCart.value) return;

      // reset flag to get notified if same client does something again
      const id = lastGuideChangedCart.value;
      lastGuideChangedCart.value = '';

      if (currentClientForGuide.value !== id) {
        loadCartInsights();
        loadAttendeeData();
        return;
      }

      refreshCart();
    }));
  }

  // init function which is called only once in client page
  async function initCartForClient() {
    if (initialized.value) return;
    initialized.value = true;

    // register watcher for reload cart of client if guide changed something for him
    addWatcher(watch(lastGuideChangedCart, () => {
      if (appointment.value.attendeeId === lastGuideChangedCart.value) {
        // reset flag to get notified if same guide does something again
        lastGuideChangedCart.value = '';
        refreshCart();
      }
    }));
    
    // load the cart initially for the client
    await refreshCart();
  }

  return {
    addProducts,
    changeProducts,
    removeProducts,
    appliedPromotionCodes,
    cart,
    cartItems,
    count,
    disabledCartCount,
    error: computed(() => error.value),
    totalPrice,
    shippingTotal,
    subtotal,
    cartErrors,
    getProductItemsSeoUrlsData,
    initCartForGuide,
    initCartForClient,
    requestTokenPermissions: requestTokenPermissions,
    currentClientCartPermissions: currentClientTokenPermissions,
    refreshCart,
  };
}
