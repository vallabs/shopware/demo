import { ref, createApp } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
import { useHoverElement } from "../useHoverElement";
jest.mock('../useErrorHandler');

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

function mockLoadComposableInApp(composable) {
	let result;
	const app = createApp( {
		setup() {
			result = composable();
			// suppress missing template warning
			return result;
		}
	} );
	const wrapper = app.mount( document.createElement( 'div' ) );
	return [ result, app, wrapper ];
}

describe("Composables - useHoverElement", () => {
  let result;
  let app;
  let wrapper;
  const lastElementIdMock = ref<string>();
  const isGuideMock = ref<boolean>();
  const isClientMock = ref<boolean>();
  const lastGuideHoveredElementMock = ref();
  const lastClientHoveredElementMock = ref();

  const handleErrorMock = jest.fn();
  const mercurePublishGuideMock = jest.fn();
  const mercurePublishClientMock = jest.fn();
  const rootContextMock = prepareRootContextMock();
  
  beforeEach(() => {
    jest.resetAllMocks();
    lastElementIdMock.value = null;
    isGuideMock.value = null;
    isClientMock.value = null;
    lastGuideHoveredElementMock.value = null;
    lastClientHoveredElementMock.value = null;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("lastElementId"))
            return lastElementIdMock;
        },
      } as any;
    });
    (mockedUseAppointment as any).mockImplementation(() => ({
      isGuide: isGuideMock,
      isClient: isClientMock,
    } as any));
   
    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));
   
    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        guide: mercurePublishGuideMock,
        client: mercurePublishClientMock,
      }
    } as any));
    (mockedUseMercureObserver as any).mockImplementation(() => ({
      lastGuideHoveredElement: lastGuideHoveredElementMock, 
      lastClientHoveredElement: lastClientHoveredElementMock,
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});
    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
    }
  });

  describe("methods", () => {
    describe("publishHover", () => {
      it("should publish guide.hovered when isGuide true", async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        const { publishHover } = useHoverElement('dummyId');
        await publishHover();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('guide.hovered', 'dummyId');
      });

      it("should publish client.hovered when isClient true", async () => {
        isGuideMock.value = false;
        isClientMock.value = true;
        const { publishHover } = useHoverElement('dummyId');
        await publishHover();
        expect(mercurePublishClientMock).toHaveBeenCalledWith('client.hovered', 'dummyId');
      });

      it("should not do anything when elementId invalid", async () => {
        isGuideMock.value = false;
        isClientMock.value = true;
        const { publishHover } = useHoverElement('');
        await publishHover();
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
        expect(mercurePublishClientMock).not.toHaveBeenCalled();
      });

      it("should not do anything when elementId equal lastGuideHoveredElement", async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        lastGuideHoveredElementMock.value = 'dummyId';
        const { publishHover } = useHoverElement('dummyId');
        await publishHover();
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
      });

      it("should not do anything when elementId equal lastClientHoveredElementMock", async () => {
        isGuideMock.value = false;
        isClientMock.value = true;
        lastClientHoveredElementMock.value = 'dummyId';
        const { publishHover } = useHoverElement('dummyId');
        await publishHover();
        expect(mercurePublishClientMock).not.toHaveBeenCalled();
      });
    });

    describe("publishHoverStart", () => {
      it("should publishHover when hover new element more than 2 secs", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        lastElementIdMock.value = null;
        isGuideMock.value = true;
        isClientMock.value = false;
        const { publishHoverStart } = useHoverElement('dummyId');
        publishHoverStart(2000);
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 2000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('guide.hovered', 'dummyId');
      });

      it("should not publishHover when hover same element", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        lastElementIdMock.value = 'dummyId';
        isGuideMock.value = true;
        isClientMock.value = false;
        const { publishHoverStart } = useHoverElement('dummyId');
        publishHoverStart(2000);
        jest.runAllTimers();
        expect(setTimeout).not.toHaveBeenCalled();
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
      });
    });

    describe("publishHoverEnd", () => {
      it("should not publishHover when hover new element not enough 2 secs", async () => {
        jest.useFakeTimers();
        lastElementIdMock.value = null;
        isGuideMock.value = true;
        isClientMock.value = false;
        const { publishHoverStart, publishHoverEnd } = useHoverElement('dummyId');
        publishHoverStart(2000);
        publishHoverEnd();
        jest.runAllTimers();
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
      });

      it("should publishHover when hover in & hover out more than 2 secs", async () => {
        jest.useFakeTimers();
        lastElementIdMock.value = null;
        isGuideMock.value = true;
        isClientMock.value = false;
        const { publishHoverStart, publishHoverEnd } = useHoverElement('dummyId');
        publishHoverStart(2000);
        jest.runAllTimers();
        publishHoverEnd();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('guide.hovered', 'dummyId');
      });
    });
  });

  describe("hooks", () => {
    it("should not publish guide.hovered when unmount before hover into element more than 2 secs", async () => {
      jest.useFakeTimers();
      lastElementIdMock.value = null;
      isGuideMock.value = true;
      isClientMock.value = false;
      [result, app] = mockLoadComposableInApp(() => useHoverElement('dummyId'));
      result.publishHoverStart(2000);
      app.unmount();
      jest.runAllTimers();
      expect(mercurePublishGuideMock).not.toHaveBeenCalled();
    });
  });

});
