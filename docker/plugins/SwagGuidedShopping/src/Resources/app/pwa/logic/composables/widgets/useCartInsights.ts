import {
  getApplicationContext,
  useSharedState,
} from "@shopware-pwa/composables";
import { computed, ref } from "@vue/composition-api";
import { CartService } from "../../api-client";
import { useAdminApiInstance } from "../useAdminApiInstance";
import { useAppointment } from "../useAppointment";
import { useErrorHandler } from "../useErrorHandler";
import { CartInsights, ClientApiError } from "../../interfaces";
import { getProducts } from "@shopware-pwa/shopware-6-client";
import { ShopwareSearchParams, SearchFilterType } from "@shopware-pwa/commons";

const COMPOSABLE_NAME = "useCartInsights";

export const useCartInsights = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { adminApiInstance } = useAdminApiInstance();
  const { appointmentId } = useAppointment();
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const isLoading = ref<boolean>(true);
  const _storeCartInsights = sharedRef<CartInsights>(
    `${contextName}-cartInsights`,
    null
  );

  async function loadCartInsights() {
    try {
      isLoading.value = true;
      let data = await CartService.getCartInsights(
        appointmentId.value,
        adminApiInstance
      );

      if (data.productCount === 0) {
        _storeCartInsights.value = data;
        isLoading.value = false;
        return;
      }

      let productIds = [];
      data.topProducts.byQuantity.forEach((product) => {
        productIds.push(product.productId);
      });
      data.topProducts.byRevenue.forEach((product) => {
        productIds.push(product.productId);
      });

      if (productIds.length === 0) {
        _storeCartInsights.value = data;
        isLoading.value = false;
        return;
      }

      const criteria: ShopwareSearchParams = {
        includes: {
          product: ["id", "name", "translated"],
        },
        filter: [
          {
            type: SearchFilterType.EQUALS_ANY,
            field: "id",
            value: productIds,
          },
        ],
      };

      // get product data
      const products = await getProducts(criteria, apiInstance);

      // get product name
      data.topProducts.byQuantity.forEach((product) => {
        const foundProduct = products.elements.find(
          (productData) => productData.id === product.productId
        );
        product.name = foundProduct.name;
        product.translated = foundProduct.translated;
      });
      data.topProducts.byRevenue.forEach((product) => {
        const foundProduct = products.elements.find(
          (productData) => productData.id === product.productId
        );
        product.name = foundProduct.name;
        product.translated = foundProduct.translated;
      });

      _storeCartInsights.value = data;
      isLoading.value = false;
    } catch (e) {
      _storeCartInsights.value = null;
      isLoading.value = false;
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadCartInsights",
      });
    }
  }

  return {
    loadCartInsights,
    isLoading: computed(() => isLoading.value),
    cartSum: computed(() => _storeCartInsights.value?.cartSum),
    productCount: computed(() =>
      _storeCartInsights.value?.productCount
        ? _storeCartInsights.value?.productCount
        : 0
    ),
    topProductsByQuantity: computed(
      () => _storeCartInsights.value?.topProducts?.byQuantity
    ),
    topProductsByRevenue: computed(
      () => _storeCartInsights.value?.topProducts?.byRevenue
    ),
  };
};
