export interface PresentationPageModel {
  cmsPageResults: object,
  navigation: Array<NavigationItem>
}

export type TreeNavigationItem =  {
  name: string,
  items: Array<NavigationItem>
}

export type TreeNavigation = {
  [key: string]: TreeNavigationItem
}

export interface NavigationItem {
  groupId: string,
  groupName: string,
  cmsPageId: string,
  sectionId: string,
  index: number,
  sectionName: string,
  notes: string
}
