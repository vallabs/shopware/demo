import {InstantListingService} from "../api-client";
import {useAdminApiInstance} from "./useAdminApiInstance";
import {Product} from "@shopware-pwa/commons/interfaces/models/content/product/Product";
import {getApplicationContext, useSharedState} from '@shopware-pwa/composables';
import {useErrorHandler} from "./useErrorHandler";
import {computed} from "@vue/composition-api";
import {useAppointment} from "./useAppointment";
import {usePresentation} from "./usePresentation";
import {useMercureObserver} from "./useMercureObserver";
import {useMercure} from './useMercure';
import {debounce} from "lodash";
import {
  AllProductIdsApiResult,
  InstantListingApiResult,
  InstantListingType,
  PaginationData,
  ProductStreamFilterItem,
  SelectedFilters
} from "../interfaces";
import {getListingFilters, toggleSearchFilter} from "@shopware-pwa/helpers";
import {useProductInsights} from "./widgets/useProductInsights";
import {useLikeList} from "./useLikeList";

const COMPOSABLE_NAME = 'useInstantListing';
const INSTANT_LISTING_PRODUCTS_IDS_KEY = 'useInstantListing-instantListingProductIds';
const ADD_PRODUCT_IDS_KEY = 'useInstantListing-addProductIds';
const REMOVE_PRODUCT_IDS_KEY = 'useInstantListing-removeProductIds';
const INSTANT_LISTING_MODE_KEY = 'useInstantListing-instantListingMode';
const IL_PRODUCT_LIMIT_PER_PAGE = 36;
export const INSTANT_LISTING_TYPES: InstantListingType[]  = ['instant-listing', 'guided-product-listing'];
const DEBOUNCE_AUTO_SAVING = 750;

export const useInstantListing = () => {
  const contextName = COMPOSABLE_NAME;
  const {router, apiInstance} = getApplicationContext({contextName});
  const {adminApiInstance} = useAdminApiInstance();
  const {sharedRef} = useSharedState();
  const {handleError} = useErrorHandler();
  const {appointmentId, isSelfService} = useAppointment();
  const {mercurePublish} = useMercure();
  const { instantListingUpdated, navigationPageIndex } = useMercureObserver();
  const {currentSlideIndex, navigation, goToSlideAfterReload, indexOfFirstSlideInNextGroup, isProductListing, listingProductIds, listingProductStreamId} = usePresentation();
  const {filteredProperties} = useProductInsights();
  const {
    changeProduct,
    guideLike,
    removeFromGuideList,
    productOnGuideList,
    guideLikeList
  } = useLikeList();

  const instantListingAccordion = sharedRef<any>(`${contextName}-instantListingAccordion`, null);
  const isInstantListingModalOpen = sharedRef<boolean>(`${contextName}-isInstantListingModalOpen`, false);
  const instantListingModalType = sharedRef<InstantListingType>(`${contextName}-instantListingModalType`, 'instant-listing');
  const instantListingLoading = sharedRef<boolean>(`${contextName}-instantListingLoading`, false);
  const guideProductsLoading = sharedRef<string[]>(`${contextName}-guideProductsLoading`, []);
  const instantListingSaving = sharedRef<boolean>(`${contextName}-instantListingSaving`, false);
  const _storeInstantListingProducts = sharedRef<{
    id: string,
    data?: Product,
    loaded?: boolean
  }[]>(`${contextName}-storeInstantListingProducts`, []);
  const _storeProductStreams = sharedRef<ProductStreamFilterItem[]>(`${contextName}-productStreams`, []);
  const _storeSelectedProductStreams = sharedRef<ProductStreamFilterItem[]>(`${contextName}-selectedProductStreams`, []);
  const currentInstantListingIndex = sharedRef<number | null>(`${contextName}-currentInstantListingPageId`, null);
  const addInstantListingProductIds = sharedRef<string[]>(`${contextName}-addInstantListingProductIds`, []);
  const removeInstantListingProductIds = sharedRef<string[]>(`${contextName}-removeInstantListingProductIds`, []);
  const _storeApiProductsResponse = sharedRef<InstantListingApiResult>(`${contextName}-apiResponse`, {});
  const _storeApiAllProductsResponse = sharedRef<AllProductIdsApiResult>(`${contextName}-apiAllProductsResponse`, {});
  const autoSyncProducts = sharedRef<boolean>(`${contextName}-autoSyncProducts`, false);
  const pageName = sharedRef<string>(`${contextName}-pageName`, '');
  const scrollLastPosition = sharedRef<string>(`${contextName}-scrollLastPosition`);
  const scrollLoading = sharedRef<boolean>(`${contextName}-scrollLoading`, false);
  const selectedFilters = sharedRef<SelectedFilters>(`${contextName}-selectedFilters`, {});
  const _storePaginationData = sharedRef<PaginationData>(`${contextName}-storePaginationData`, {
    getLimit: selectedFilters.value?.limit || IL_PRODUCT_LIMIT_PER_PAGE,
    getTotal: selectedFilters.value?.total || 0,
    getCurrentPage: selectedFilters.value?.page || 1,
    getTotalPagesCount: Math.ceil(selectedFilters.value.total / selectedFilters.value.limit) || 0
  });
  const _ilSearchTerm = sharedRef<string | null>(`${contextName}-_ilSearchTerm`, null);
  const loadAllController = sharedRef<AbortController | null>(`${contextName}-loadAllController`);
  const loadAllProductDataController = sharedRef<AbortController | null>(`${contextName}-loadAllProductDataController`);
  const saveInstantListingUpdateController = sharedRef<AbortController | null>(`${contextName}-saveInstantListingUpdateController`);
  
  const isCreationMode = computed(() => currentInstantListingIndex.value === 0);
  const isEditMode = computed(() => currentInstantListingIndex.value > 0);

  // Sorting
  const getSortingOrders = computed(() => {
    const oldSortings = Object.values(_storeApiProductsResponse.value?.sortings || {}); // before Shopware 6.4
    return _storeApiProductsResponse.value?.availableSortings || oldSortings;
  });
  const getCurrentSortingOrder = computed(() => _storeApiProductsResponse.value?.sorting);

  function openInstantListing(forceMode: string = null, type: InstantListingType = INSTANT_LISTING_TYPES[0]) {
    let mode = isProductListing.value ? 'edit' : 'create';
    if (forceMode) mode = forceMode;
    if (type === INSTANT_LISTING_TYPES[1] || mode === 'edit') {
      editInstantListing(type);
    } else {
      createNewInstantListing(type);
    }
  }

  const createNewInstantListing = (type: InstantListingType = INSTANT_LISTING_TYPES[0]) => {
    if (type === INSTANT_LISTING_TYPES[0]) {
      router.push({ query: { il: 'create' } });
    } else {
      router.push({ query: {} });
    }
    isInstantListingModalOpen.value = true;
    instantListingModalType.value = type;
    currentInstantListingIndex.value = 0;
    _storeInstantListingProducts.value = [];
    addInstantListingProductIds.value = [];
    removeInstantListingProductIds.value = [];
    pageName.value = '';
    loadProductsFromStorage();
    resyncInstantListingProducts(true);
  };

  const editInstantListing = async (type: InstantListingType = INSTANT_LISTING_TYPES[0]) => {
    if (type === INSTANT_LISTING_TYPES[0]) {
      router.push({ query: { il: 'edit' } });
    } else {
      router.push({ query: {} });
    }
    isInstantListingModalOpen.value = true;
    instantListingModalType.value = type;
    currentInstantListingIndex.value = currentSlideIndex.value;
    addInstantListingProductIds.value = [];
    removeInstantListingProductIds.value = [];
    pageName.value = navigation.value[currentInstantListingIndex.value -1].groupName;
    if (type === INSTANT_LISTING_TYPES[1]) {
      _storeInstantListingProducts.value = guideLikeList.value;
    } else {
      _storeInstantListingProducts.value = listingProductIds.value?.map(id => ({ id })) ?? [];
      // save current listing into storage
      if (_storeInstantListingProducts.value?.length && !sessionStorage.getItem(INSTANT_LISTING_PRODUCTS_IDS_KEY)) {
        updateSessionStorageData();
      }
      // load product of instant listing when it existed in sessionStorage
      loadProductsFromStorage();
      resyncInstantListingProducts(true);
    }
  }

  const scrollLoadInstantListing = async () => {
    scrollLoading.value = true;
    let lastIndex = 0;
    let partNeedToShow;
    if (scrollLastPosition.value) {
      lastIndex = _storeInstantListingProducts.value.findIndex(x => x.id === scrollLastPosition.value) + 1;
    }
    partNeedToShow = _storeInstantListingProducts.value.slice(lastIndex, lastIndex + IL_PRODUCT_LIMIT_PER_PAGE);
    const idsDoesNotHaveData = partNeedToShow.filter(x => !x.data).map(x => x.id);
    let tempProducts;
    if (idsDoesNotHaveData.length) {
      tempProducts = await loadPageProducts(idsDoesNotHaveData);
    }
    partNeedToShow.forEach(x => {
      if (!x.data && tempProducts) {
        x.data = tempProducts.find(product => product.id === x.id);
      }
      x.loaded = true;
    });
    _storeInstantListingProducts.value.splice(lastIndex, IL_PRODUCT_LIMIT_PER_PAGE, ...partNeedToShow);
    scrollLastPosition.value = partNeedToShow[partNeedToShow.length - 1]?.id;

    setTimeout(() => {
      scrollLoading.value = false;
    }, 500);
  }

  async function loadPageProducts(ids: string[]) {
    let filters: SelectedFilters = {};
    filters.filter = [
      {
        type: 'equalsAny',
        field: 'id',
        value: ids
      }
    ]
    // else if (listingProductStreamId.value) {
    //   const streamItem: ProductStreamFilterItem = await getProductStream(listingProductStreamId.value);
    //   if (!streamItem) return;
    //   filters.filter = streamItem.apiFilter;
    // } else {
    //   return;
    // }

    filters.limit = 36;
    filters.p = 1;

    try {
      let response = await InstantListingService.getAllProductsWithData(
        filters,
        apiInstance
      );

      return response.elements;
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadAllProductData',
      });
    }
  }

  const closeInstantListing = () => {
    router.push({ query: {}});
    addInstantListingProductIds.value = [];
    removeInstantListingProductIds.value = [];
    isInstantListingModalOpen.value = false;
    instantListingModalType.value = null;
    _storeInstantListingProducts.value = [];
    _storeApiAllProductsResponse.value = {};
    _storeSelectedProductStreams.value = [];
    selectedFilters.value = {};
    currentInstantListingIndex.value = null;
    loadAllController.value = null;
    loadAllProductDataController.value = null;
    saveInstantListingUpdateController.value = null;
    scrollLastPosition.value = null;
    clearSessionStorageData();
  }

  const changeCurrentSortingOrder = async (order) => {
    toggleLoader(true);
    selectedFilters.value.order = order;

    let response = await InstantListingService.getAllProductsWithData(
      selectedFilters.value,
      apiInstance
    );
    setResponseData(response);
  };

  const changeCurrentPage = async (pageNumber: number | string) => {
    toggleLoader(true);
    selectedFilters.value.p = pageNumber;

    let response = await InstantListingService.getAllProductsWithData(
      selectedFilters.value,
      apiInstance
    );

    setResponseData(response);
  };

  async function loadAllProductData(clearSearchFilter = false) {
    toggleLoader(true);
    clearSearch(clearSearchFilter);

    const control = handleAbortForGetAllProductsWithData();

    if (clearSearchFilter) {
      selectedFilters.value.query = null;
      selectedFilters.value.search = null;
      selectedFilters.value.term = null;
    }

    selectedFilters.value.limit = IL_PRODUCT_LIMIT_PER_PAGE;
    selectedFilters.value.interaction = true;
    selectedFilters.value.loadVariants = true;
    selectedFilters.value.loadAllIds = true;

    try {
      const response = await InstantListingService.getAllProductsWithData(
        selectedFilters.value,
        apiInstance,
        control
      );

      setResponseData(response);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadAllProductData',
      });
    }
  }

  async function getProductStream(streamId: string) {
    try {
      const streams = await InstantListingService.getProductStreams(
        {
          filter: [
            {
              type: 'equals',
              field: 'id',
              value: streamId
            }
          ],
          includes: {
            'product_stream': [
              'id',
              'name',
              'translated',
              'apiFilter'
            ]
          }
        },
        adminApiInstance
      );
      return streams[0];
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadProductStreams',
      });
    }
  }


  async function loadProductStreams() {
    try {
      _storeProductStreams.value = await InstantListingService.getProductStreams(
        {
          includes: {
            'product_stream': [
              'id',
              'name',
              'translated',
              'apiFilter'
            ]
          }
        },
        adminApiInstance
      );
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'loadProductStreams',
      });
    }
  }

  async function getSearchedProducts(searchTerm) {
    toggleLoader(true);
    _ilSearchTerm.value = searchTerm;

    selectedFilters.value.query = searchTerm;
    selectedFilters.value.search = searchTerm;
    selectedFilters.value.term = searchTerm;
    selectedFilters.value.loadVariants = true;
    selectedFilters.value.loadAllIds = true;

    try {
      const response = await InstantListingService.getAllProductsWithData(
        selectedFilters.value,
        apiInstance
      );

      setResponseData(response);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'getSearchedProducts',
      });
    }
  }

  async function toggleFilterValue(filter) {
    selectedFilters.value = toggleSearchFilter(
      selectedFilters.value,
      filter
    );
    _storeApiProductsResponse.value.currentFilters = {
      ..._storeApiProductsResponse.value.currentFilters,
      properties: selectedFilters.value.properties,
      manufacturer: selectedFilters.value.manufacturer as string[],
    };
    await applyFilters();
  }

  async function applyFilters() {
    const control = handleAbortForGetAllProductsWithData();
    toggleLoader(true);

    selectedFilters.value.loadVariants = true;
    selectedFilters.value.loadAllIds = true;

    try {
      const response = await InstantListingService.getAllProductsWithData(
        selectedFilters.value,
        apiInstance,
        control
      );

      setResponseData(response);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'applyFilters',
      });
    }
  }

  function removeSelectedFilters() {
    let cacheStream;
    if (selectedFilters.value.streams) cacheStream = selectedFilters.value.streams;
    selectedFilters.value = {};
    if (cacheStream) {
      selectedFilters.value.streams = cacheStream;
    }
    selectedFilters.value.limit = IL_PRODUCT_LIMIT_PER_PAGE;
    // this code was commented because it's useless, selectedFilters.value.search will never have value in this case
    // if (selectedFilters.value.search) {
    //   getSearchedProducts(_storeApiProductsResponse.value.currentFilters.search);
    // } else {
      loadAllProductData();
    // }
  }

  function toggleProductForInstantListing(product, forcedValue: boolean = null) {
    let shouldSelect = forcedValue ?? !isProductSelected(product.id);
    if (shouldSelect) {
      if (isProductSelected(product.id)) return;
      _storeInstantListingProducts.value.unshift({
        id: product.id,
        data: product,
        loaded: true
      });
      const index = removeInstantListingProductIds.value.findIndex(id => id === product.id);
      if (index >= 0) {
        removeInstantListingProductIds.value.splice(index, 1);
      } else {
        addInstantListingProductIds.value.unshift(product.id);
      }
    } else {
      // remove if already selected
      _storeInstantListingProducts.value = _storeInstantListingProducts.value.filter(filterProduct => filterProduct.id !== product.id);
      const index = addInstantListingProductIds.value.findIndex(id => id === product.id);
      if (index >= 0) {
        addInstantListingProductIds.value.splice(index, 1);
      } else {
        removeInstantListingProductIds.value.push(product.id);
      }
    }

    if (autoSyncProducts.value) {
      updateSessionStorageData();
      debounceSaveInstantListingUpdate({close: false})
    }
  }

  async function removeProductForGuidedProductListing(product) {
    changeProduct(product);
    guideProductsLoading.value.push(product.id);
    await removeFromGuideList();
    guideProductsLoading.value = guideProductsLoading.value.filter(x => x !== product.id);
    _storeInstantListingProducts.value = _storeInstantListingProducts.value.filter(filterProduct => filterProduct.id !== product.id);
  }

  async function addProductForGuidedProductListing(product) {
    changeProduct(product);
    guideProductsLoading.value.push(product.id);
    await guideLike();
    guideProductsLoading.value = guideProductsLoading.value.filter(x => x !== product.id);
    _storeInstantListingProducts.value.push(product);
  }

  async function toggleProductForGuidedProductListing(product) {
    const isLoading = guideProductsLoading.value.findIndex(x => x === product.id) >= 0;
    if (isLoading) {
      return;
    }
    changeProduct(product);
    if (productOnGuideList.value) {
      await removeProductForGuidedProductListing(product);
    } else {
      await addProductForGuidedProductListing(product);
    }
  }

  function toggleProduct(product, forcedValue: boolean = null) {
    if (instantListingModalType.value === INSTANT_LISTING_TYPES[0]) {
      toggleProductForInstantListing(product, forcedValue);
      return;
    }
    if (instantListingModalType.value === INSTANT_LISTING_TYPES[1]) {
      toggleProductForGuidedProductListing(product);
      return;
    }
  }

  function updateSessionStorageData() {
    sessionStorage.setItem(ADD_PRODUCT_IDS_KEY, addInstantListingProductIds.value.join(','));
    sessionStorage.setItem(REMOVE_PRODUCT_IDS_KEY, removeInstantListingProductIds.value.join(','));
    sessionStorage.setItem(INSTANT_LISTING_MODE_KEY, isEditMode.value ? 'edit' : 'create');
    sessionStorage.setItem(INSTANT_LISTING_PRODUCTS_IDS_KEY, _storeInstantListingProducts.value.map(x => x.id).join(','));
  }

  function loadProductsFromStorage() {
    const lastMode = sessionStorage.getItem(INSTANT_LISTING_MODE_KEY);
    const currentMode =  isEditMode.value ? 'edit' : 'create';
    if (lastMode === currentMode) {
      addInstantListingProductIds.value = sessionStorage.getItem(ADD_PRODUCT_IDS_KEY)?.split(',').filter(x => !!x);
      removeInstantListingProductIds.value = sessionStorage.getItem(REMOVE_PRODUCT_IDS_KEY)?.split(',').filter(x => !!x);
      _storeInstantListingProducts.value = sessionStorage.getItem(INSTANT_LISTING_PRODUCTS_IDS_KEY)?.split(',').filter(x => !!x).map(id => ({ id }));
    }
  }

  function clearSessionStorageData() {
    sessionStorage.removeItem(ADD_PRODUCT_IDS_KEY);
    sessionStorage.removeItem(REMOVE_PRODUCT_IDS_KEY);
    sessionStorage.removeItem(INSTANT_LISTING_MODE_KEY);
    sessionStorage.removeItem(INSTANT_LISTING_PRODUCTS_IDS_KEY);
  }

  function isProductSelected(productId: string) {
    if (instantListingModalType.value === INSTANT_LISTING_TYPES[0]) {
      if (removeInstantListingProductIds.value.some(id => id === productId)) return false;
      const listingProductIdsTemp = listingProductIds.value?.length ? listingProductIds.value : [];
      const addInstantListingProductIdsTemp = addInstantListingProductIds.value?.length ? addInstantListingProductIds.value : [];
      const activeProducts = isCreationMode.value ? [...addInstantListingProductIdsTemp] : [...listingProductIdsTemp, ...addInstantListingProductIdsTemp]
      return activeProducts.some(id => id === productId);
    } else {
      return _storeInstantListingProducts.value.some(product => product.id === productId);
    }
  }

  const saveInstantListingUpdate = async (params: {
    close: boolean
  }) => {
    if (!saveInstantListingUpdateController.value) {
      saveInstantListingUpdateController.value = new AbortController();
    } else {
      saveInstantListingUpdateController.value.abort();
      saveInstantListingUpdateController.value = new AbortController();
    }
    const control = saveInstantListingUpdateController.value ? { signal: saveInstantListingUpdateController.value.signal } : {};
    try {
      instantListingSaving.value = true;
      let currentPageGroupId = navigation.value[currentInstantListingIndex.value -1].groupId;
      let appointmentIdToSync = appointmentId.value;
      await InstantListingService.saveInstantListingUpdate(
        {
          addProductIds: addInstantListingProductIds.value,
          removeProductIds: removeInstantListingProductIds.value,
          currentPageGroupId,
          appointmentIdToSync,
          pageName: pageName.value
        },
        adminApiInstance,
        control
      );

      const mercurePayload = {
        updated: true,
        currentIlIndex: currentInstantListingIndex.value - 1
      }
      if (isSelfService.value) {
        mercurePublish.local('instantListingUpdated', mercurePayload);
      } else {
        mercurePublish.guide('instantListingUpdated', mercurePayload);
      }
      if (params.close) {
        closeInstantListing();
      }
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'saveInstantListingUpdate',
      });
    } finally {
      instantListingSaving.value = false;
    }
  }

  const debounceSaveInstantListingUpdate = debounce(async (params: {
    close: boolean
  }) => {
    await saveInstantListingUpdate(params);
  }, DEBOUNCE_AUTO_SAVING);

  async function saveNewInstantListing(navigate: boolean = false) {
    try {
      let index = currentSlideIndex.value - 1;
      let currentPageGroupId = navigation.value[index]?.groupId;
      let productIds = getProductIdsToSync();
      let appointmentIdToSync = appointmentId.value;

      await InstantListingService.saveNewInstantListing(
        {
          productIds,
          currentPageGroupId,
          appointmentIdToSync,
          pageName: pageName.value
        },
        adminApiInstance
      );

      if (navigate || indexOfFirstSlideInNextGroup.value === 1) {
        goToSlideAfterReload(indexOfFirstSlideInNextGroup.value);
      }

      if (isSelfService.value) {
        mercurePublish.local('newInstantListingCreated', true);
      } else {
        mercurePublish.guide('newInstantListingCreated', true);
      }

      closeInstantListing();
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'saveNewInstantListing',
      });
    }
  }

  function handleAbortForGetAllProductsWithData () {
    if (!loadAllProductDataController.value) {
      loadAllProductDataController.value = new AbortController();
    } else {
      loadAllProductDataController.value.abort();
      loadAllProductDataController.value = new AbortController();
      if (loadAllController.value) {
        loadAllController.value.abort();
        loadAllController.value = new AbortController();
      }
    }

    const control = loadAllProductDataController.value ? { signal: loadAllProductDataController.value.signal } : {};
    return control;
  }

  function getProductIdsToSync(): string[] {
    return _storeInstantListingProducts.value.map(x => x.id);
  }

  async function toggleProductStreamSelection(stream) {
    const control = handleAbortForGetAllProductsWithData();

    toggleLoader(true);
    updateSelectedProductStreams(stream);
    addStreamsToSelectedFilters();

    selectedFilters.value.loadVariants = true;
    selectedFilters.value.loadAllIds = true;

    try {
      const response = await InstantListingService.getAllProductsWithData(
        selectedFilters.value,
        apiInstance,
        control
      );

      setResponseData(response);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'toggleProductStreamSelection',
      });
    }
  }

  function updateSelectedProductStreams(stream) {
    if (!isInProductStreamArray(stream.id)) {
      _storeSelectedProductStreams.value.push(stream);
    } else {
      _storeSelectedProductStreams.value = _storeSelectedProductStreams.value.filter(productStream => productStream.id !== stream.id);
    }
  }

  function addStreamsToSelectedFilters() {
    const allStreamIds = _storeSelectedProductStreams.value.flatMap(
      (streamItem) => streamItem.id
    );

    if (allStreamIds.length > 0) {
        const filter = {
            type: "equalsAny",
            field: "streamIds",
            value: allStreamIds
        };

        Object.assign(selectedFilters.value, {filter: [filter]});
    }
  }

  function isInProductStreamArray(streamId) {
    return _storeSelectedProductStreams.value.some(
      (stream) => stream.id === streamId
    );
  }

  function toggleAutoProductSync() {
    if (!autoSyncProducts.value) {
      checkForInstantListing();
    }

    autoSyncProducts.value = !autoSyncProducts.value;
  }

  function checkForInstantListing() {
    if (isCreationMode.value) {
      saveNewInstantListing();
    } else {
      saveInstantListingUpdate({close: false});
    }
  }

  function setResponseData(response) {
    _storeApiProductsResponse.value = response;
    selectedFilters.value.p = null;
    _storePaginationData.value.getCurrentPage = response.page;
    _storePaginationData.value.getLimit = response.limit;
    _storePaginationData.value.getTotal = response.total;
    _storePaginationData.value.getTotalPagesCount = Math.ceil(response.total / response.limit);

    _storeApiAllProductsResponse.value = {
      ids: response.extensions?.allIds?.data,
      total: response.total
    }

    toggleLoader(false);
  }

  function toggleLoader(isLoading) {
    instantListingLoading.value = isLoading;
  }

  function clearSearch(clearSearchTerm) {
    if (!clearSearchTerm) return;
    _ilSearchTerm.value = null;
  }

  async function searchProductsByInsightsFilters() {
    selectedFilters.value.properties = filteredProperties.value.map(filteredProperty => filteredProperty.id)
    await applyFilters();
  }



  const ilProductCurrentFilters = computed(() => {
    const currentFiltersResult: any = {};
    const currentFilters = {
      ..._storeApiProductsResponse.value?.currentFilters,
      ...router.currentRoute.query,
    };
    Object.keys(currentFilters).forEach((objectKey) => {
      if (!currentFilters[objectKey]) return;
      if (objectKey === "navigationId") return;
      if (objectKey === "price") {
        if (currentFilters[objectKey].min)
          currentFiltersResult["min-price"] = currentFilters[objectKey].min;
        if (currentFilters[objectKey].max)
          currentFiltersResult["max-price"] = currentFilters[objectKey].max;
        return;
      }
      if (objectKey === "p") return;
      currentFiltersResult[objectKey] = currentFilters[objectKey];
    });
    return currentFiltersResult;
  });

  const allProductsCanBeAdded = computed(() => {
    const all = _storeApiAllProductsResponse.value?.ids || [];
    if (all.length) {
      const removeInstantListingProductIdsTemp = removeInstantListingProductIds.value ?? [];
      const listingProductIdsTemp = listingProductIds.value?.length ? listingProductIds.value.filter(x => !removeInstantListingProductIdsTemp.includes(x)) : [];
      const addInstantListingProductIdsTemp = addInstantListingProductIds.value?.length ? addInstantListingProductIds.value : [];
      const selectedProducts = isCreationMode.value ? [...addInstantListingProductIdsTemp] : [...listingProductIdsTemp, ...addInstantListingProductIdsTemp];

      return all.filter(id => !selectedProducts.includes(id)) || [];
    }
    return [];
  });

  const allProductsCanBeRemoved = computed(() => {
    const all = _storeApiAllProductsResponse.value?.ids || [];
    if (all.length) {
      const removeInstantListingProductIdsTemp = removeInstantListingProductIds.value ?? [];
      const listingProductIdsTemp = listingProductIds.value?.length ? listingProductIds.value.filter(x => !removeInstantListingProductIdsTemp.includes(x)) : [];
      const addInstantListingProductIdsTemp = addInstantListingProductIds.value?.length ? addInstantListingProductIds.value : [];
      const selectedProducts = isCreationMode.value ? [...addInstantListingProductIdsTemp] : [...listingProductIdsTemp, ...addInstantListingProductIdsTemp]

      return all.filter(id => selectedProducts.includes(id)) || [];
    }
    return [];
  });

  function* chunkArray (array, size = 1) {
    const clone = array.slice(0);
    while (clone.length>0) {
      yield clone.splice(0,size);
    }
  }

  const resyncInstantListingProducts = (refresh?: boolean) => {
    _storeInstantListingProducts.value = _storeInstantListingProducts.value.map(x => {
      x.loaded = false;
      return x;
    });
    scrollLastPosition.value = null;
    if (refresh) {
      scrollLoadInstantListing();
    }
  }

  const addAllProducts = () => {
    if (!allProductsCanBeAdded.value?.length) return;
    const temp = [...allProductsCanBeAdded.value];
    const allProductsCanBeAddedIds = allProductsCanBeAdded.value;
    const idsBelongToDeleteList = removeInstantListingProductIds.value.filter(x => allProductsCanBeAddedIds.includes(x));
    removeInstantListingProductIds.value = removeInstantListingProductIds.value.filter(x => !idsBelongToDeleteList.includes(x));
    const productCanAdded = allProductsCanBeAddedIds
                              .filter(x => !idsBelongToDeleteList.includes(x) && !addInstantListingProductIds.value.includes(x));
    addInstantListingProductIds.value = [...productCanAdded, ...addInstantListingProductIds.value];
    if (autoSyncProducts.value) {
      debounceSaveInstantListingUpdate({ close: false })
    }
    _storeInstantListingProducts.value.unshift(...temp.map(id => ({ id })));
    resyncInstantListingProducts(true);
  }

  const removeAddedProducts = () => {
    if (!allProductsCanBeRemoved.value?.length) return;
    const allProductsCanBeRemovedIds = allProductsCanBeRemoved.value;
    _storeInstantListingProducts.value = _storeInstantListingProducts.value.filter(filterProduct => !allProductsCanBeRemovedIds.includes(filterProduct.id));
    const idsShouldBeDeleted = listingProductIds.value ? listingProductIds.value.filter(x => allProductsCanBeRemovedIds.includes(x)) : [];
    addInstantListingProductIds.value = addInstantListingProductIds.value.filter(x => !allProductsCanBeRemovedIds.includes(x));
    removeInstantListingProductIds.value = [...removeInstantListingProductIds.value, ...idsShouldBeDeleted];
    if (autoSyncProducts.value) {
      debounceSaveInstantListingUpdate({ close: false })
    }
    resyncInstantListingProducts(true);
  }

  const removeAllSelectedProducts = () => {
    addInstantListingProductIds.value = [];
    removeInstantListingProductIds.value = listingProductIds.value ? [...listingProductIds.value]: [];
    _storeInstantListingProducts.value = [];
  }

  return {
    createNewInstantListing,
    instantListingUpdated,
    addInstantListingProductIds,
    removeInstantListingProductIds,
    openInstantListing,
    editInstantListing,
    closeInstantListing,
    isCreationMode,
    isEditMode,
    toggleProductForGuidedProductListing,
    removeProductForGuidedProductListing,
    addProductForGuidedProductListing,
    loadAllProductData,
    updateSessionStorageData,
    loadProductStreams,
    productStreams: computed(() => _storeProductStreams.value),
    ilProducts: computed(() => _storeApiProductsResponse.value?.elements),
    allProductsCanBeAdded,
    allProductsCanBeRemoved,
    ilProductFilter: computed(() => getListingFilters(_storeApiProductsResponse.value?.aggregations)),
    ilProductCurrentFilters,
    currentProducts: computed(() => _storeInstantListingProducts.value.filter(x => x.data && x.loaded).map(x => x.data)),
    instantListingProducts: computed(() => _storeInstantListingProducts.value),
    isInstantListingModalOpen,
    instantListingModalType,
    guideProductsLoading,
    getSearchedProducts,
    toggleProduct,
    isProductSelected,
    saveNewInstantListing,
    toggleAutoProductSync,
    toggleProductStreamSelection,
    loadProductsFromStorage,
    saveInstantListingUpdate,
    isInProductStreamArray,
    toggleFilterValue,
    applyFilters,
    selectedFilters,
    removeSelectedFilters,
    getSortingOrders,
    getCurrentSortingOrder,
    changeCurrentSortingOrder,
    changeCurrentPage,
    getTotal: computed(() => _storePaginationData.value.getTotal),
    getLimit: computed(() => _storePaginationData.value.getLimit),
    getCurrentPage: computed(() => _storePaginationData.value.getCurrentPage),
    getTotalPagesCount: computed(() => _storePaginationData.value.getTotalPagesCount),
    ilSearchTerm: computed(() => _ilSearchTerm.value),
    instantListingLoading,
    autoSyncProducts,
    pageName,
    currentInstantListingIndex,
    indexOfFirstSlideInNextGroup,
    searchProductsByInsightsFilters,
    instantListingAccordion,
    scrollLoading,
    scrollLoadInstantListing,
    instantListingSaving,
    addAllProducts,
    removeAddedProducts,
    removeAllSelectedProducts,
    clearSessionStorageData
  }

};
