import {
  startAppointment,
  endAppointment,
  joinAsGuide,
  joinAsClient,
  updateAttendee,
  getAttendeeContextToken
} from "../appointmentService";

describe("Services - appointmentService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let patchMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
        patch: patchMock
      },
    };
  });

  describe("startAppointment", () => {
    it('should work correctly with correct response', async () => {
      await startAppointment('appointmentId', apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/appointmentId/start`);
    })
  });

  describe("endAppointment", () => {
    it('should work correctly with correct response', async () => {
      await endAppointment('appointmentId', apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/appointmentId/end`);
    })
  });

  describe("joinAsGuide", () => {
    it('should work correctly with correct response when appointmentId valid', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [
              {
                id: 'appointmentId'
              }
            ]
          }
        });
      });
      const res = await joinAsGuide('appointmentId', apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/search/guided-shopping-appointment`, {
        filter: [
          {
            type: 'equals',
            field: 'presentationPath',
            value: 'appointmentId'
          }
        ]
      });

      expect(postMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/appointmentId/join-as-guide`);
    });

    it('should work correctly with correct response when appointmentId invalid', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            data: [
              {
                id: ''
              }
            ]
          }
        });
      });
      const res = await joinAsGuide('', apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/api/search/guided-shopping-appointment`, {
        filter: [
          {
            type: 'equals',
            field: 'presentationPath',
            value: ''
          }
        ]
      });

      expect(postMock).not.toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/appointmentId/join-as-guide`);
    })
  });

  describe("joinAsClient", () => {
    it('should work correctly with correct response', async () => {
      postMock.mockImplementation(() => {
        return Promise.resolve({
          data: {}
        });
      });
      await joinAsClient('appointmentId', 'attendeeName', apiInstanceMock);
      expect(postMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/appointmentId/join-as-client`, {
        attendeeName: 'attendeeName'
      });
    })
  });

  describe("updateAttendee", () => {
    it('should work correctly with correct response', async () => {
      await updateAttendee({ 
        attendeeName: '1',
        videoUserId: '123',
        guideCartPermissionsGranted: true
      },  apiInstanceMock);
      expect(patchMock).toHaveBeenCalledWith(`/store-api/guided-shopping/appointment/attendee`, { 
        attendeeName: '1',
        videoUserId: '123',
        guideCartPermissionsGranted: true
      });
    })
  });

  describe("getAttendeeContextToken", () => {
    it('should work correctly with correct response when attendee-sw-context-token of response return valid', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            'attendee-sw-context-token': '11111'
          }
        });
      });
      const res = await getAttendeeContextToken('attendeeId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/attendee/attendeeId/sw-context-token`);
      expect(res).toEqual('11111')
    });

    it('should work correctly with correct response when attendee-sw-context-token of response return null', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve();
      });
      const res = await getAttendeeContextToken('attendeeId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/attendee/attendeeId/sw-context-token`);
      expect(res).toEqual('')
    });
  });

});
