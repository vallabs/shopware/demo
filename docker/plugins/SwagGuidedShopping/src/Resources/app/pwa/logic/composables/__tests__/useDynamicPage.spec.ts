import { ref } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useState} from '../useState';
jest.mock('../useState');
const mockedUseState = useState as jest.Mocked<typeof useState>;

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import {useInteractions} from '../useInteractions';
jest.mock('../useInteractions');
const mockedUseInteractions = useInteractions as jest.Mocked<typeof useInteractions>;

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { useDynamicPage } from "../useDynamicPage";

export type DynamicPage = {
  type: string,
  opened: boolean,
  productId?: string
  page?: number
}

export type DynamicProductPage = {
  type: 'product',
  productId: string
};

export type DynamicProductListingPage = {
  type: 'productList',
  page: number
};

export type DynamicLikeListPage = {
  type: 'likeList'
};

export type DynamicPageResponse = DynamicPage & (DynamicProductPage | DynamicLikeListPage | DynamicProductListingPage);

describe("Composables - useDynamicPage", () => {
  const handleErrorMock = jest.fn();
  const mercurePublishGuideMock = jest.fn();
  const mercurePublishLocalMock = jest.fn();
  const publishInteractionMock = jest.fn();

  const rootContextMock = prepareRootContextMock();
  const _storeLastDynamicPageMock = ref<DynamicPageResponse>(null);
  const _slidePositionMock = ref<number>(0);
  const stateForAllMock = ref(null);
  const currentDynamicPageMock = ref(null);
  const isSelfServiceMock = ref(false)

  beforeEach(() => {
    jest.resetAllMocks();

    _storeLastDynamicPageMock.value = null;
    _slidePositionMock.value = 0;

    stateForAllMock.value = {
      currentDynamicPage: {
        type: 'product',
        productId: 'dummyProductId',
        opened: true,
      },
    };
    currentDynamicPageMock.value = {
      type: 'product',
      productId: 'dummyProductId',
      opened: true,
    }
    isSelfServiceMock.value = false;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("last-dynamic-page"))
            return _storeLastDynamicPageMock;
          if (contextName.includes("slidePosition"))
            return _slidePositionMock;
        }
      } as any
    });

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    (mockedUseState as any).mockImplementation(() => ({
      stateForAll: stateForAllMock,
    } as any));

    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        guide: mercurePublishGuideMock,
        local: mercurePublishLocalMock,
      }
    } as any));

    (mockedUseMercureObserver as any).mockImplementation(() => ({
      currentDynamicPage: currentDynamicPageMock,
    } as any));

    (mockedUseInteractions as any).mockImplementation(() => ({
      publishInteraction: publishInteractionMock
    } as any));

    (mockedUseAppointment as any).mockImplementation(() => ({
      isSelfService: isSelfServiceMock,
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});

    document.body.innerHTML = `<div class="sw-gs-layout" > </div>`;
  });

  describe("methods", () => {
    describe("openDynamicPage", () => {
      it("should return when page.opened is invalid", async () => {
        const { openDynamicPage } = useDynamicPage();
        const page = {
          type: 'product',
          productId: 'dummyProductId',
          opened: false,
        } as any;

        await openDynamicPage(page);

        expect(publishInteractionMock).not.toHaveBeenCalled();
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();

      });

      it("should return when interactionName is invalid", async () => {
        const { openDynamicPage } = useDynamicPage();
        const page = {
          type: 'testPage',
          productId: 'testPageId',
          opened: true,
        } as any;

        await openDynamicPage(page);

        expect(publishInteractionMock).not.toHaveBeenCalled();
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();

      });

      it("should open dynamic page with type is product", async () => {
        const { openDynamicPage } = useDynamicPage();
        const page = {
          type: 'product',
          productId: 'dummyProductId',
          opened: true,
        } as any;

        await openDynamicPage(page);

        expect(_storeLastDynamicPageMock.value).toEqual(currentDynamicPageMock.value);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'dynamicProductPage.opened',
          payload: {
            type: 'product',
          productId: 'dummyProductId',
          opened: true,
          }
        });
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('dynamicPage.opened', {
          type: 'product',
          productId: 'dummyProductId',
          opened: true,
        });
      });

      it("should open dynamic page with type is productList", async () => {
        const { openDynamicPage } = useDynamicPage();
        const page = {
          type: 'productList',
          page: 1,
          opened: true,
        } as any;

        await openDynamicPage(page);

        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'dynamicProductListingPage.opened',
          payload: {
            type: 'productList',
            page: 1,
            opened: true,
          }
        });
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('dynamicPage.opened', {
          type: 'productList',
          page: 1,
          opened: true,
        });
      });

      it("should open dynamic page with type is likeList", async () => {
        const { openDynamicPage } = useDynamicPage();
        const page = {
          type: 'likeList',
          opened: true,
        } as any;

        await openDynamicPage(page);

        expect(_storeLastDynamicPageMock.value).toEqual(currentDynamicPageMock.value);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'dynamicPage.opened',
          payload: {
            type: 'likeList',
            opened: true,
          }
        });
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('dynamicPage.opened', {
          type: 'likeList',
          opened: true,
        });
      });

      it("should open dynamic page when isSelfService is true", async () => {
        isSelfServiceMock.value = true;
        _storeLastDynamicPageMock.value = {
          type: 'product',
          productId: 'dummyProductId',
          opened: true,
        }

        const { openDynamicPage } = useDynamicPage();
        const page = {
          type: 'likeList',
          opened: true,
        } as any;

        await openDynamicPage(page);

        expect(mercurePublishLocalMock).toHaveBeenCalledWith('dynamicPage.opened', {
          type: 'likeList',
          opened: true,
        });
      });
    });

    describe("closeDynamicPage", () => {
      it("should call mercure publish local when isSelfService.value is true", async () => {
        isSelfServiceMock.value = true;
        _storeLastDynamicPageMock.value = {
          type: 'likeList'
        } as any;
        const page = {
          type: 'product',
          productId: 'dummyProductId',
          opened: false,
        } as any;

        const { closeDynamicPage, openDynamicPage } = useDynamicPage();
        await closeDynamicPage(page);

        expect(mercurePublishLocalMock).toHaveBeenCalledWith('dynamicPage.closed', {
          type: 'product',
          productId: 'dummyProductId',
          opened: false,
        });
      });

      it("should call publis interaction and mercure publish guide when isSelfService.value is false", async () => {
        const { closeDynamicPage } = useDynamicPage();
        const page = {
          type: 'product',
          productId: 'dummyProductId',
          opened: false,
        } as any;

        await closeDynamicPage(page);

        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'dynamicPage.closed',
        });
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('dynamicPage.closed', {
          type: 'product',
          productId: 'dummyProductId',
          opened: false,
        });
      });
    });

  });

  describe("computed", () => {
    describe("slidePosition", () => {
      it("should return slidePosition value", () => {
        const { slidePosition } = useDynamicPage();
        expect(slidePosition.value).toEqual(_slidePositionMock.value);
      });
    });

    describe("dynamicPage", () => {
      it("should return currentDynamicPage value", () => {
        const { dynamicPage } = useDynamicPage();
        expect(dynamicPage.value).toEqual(currentDynamicPageMock.value);
      });
      it("should return stateForAll value", () => {
        const { dynamicPage } = useDynamicPage();
        expect(dynamicPage.value).toEqual(stateForAllMock.value.currentDynamicPage);
      });
    });

    describe("dynamicPageProductId", () => {
      it("should return productId value when dynamicPage?.value?.opened is true", () => {
        currentDynamicPageMock.value = {
          type: 'product',
          productId: 'dummyProductId',
          opened: true,
        }
        const { dynamicPageProductId } = useDynamicPage();
        expect(dynamicPageProductId.value).toEqual('dummyProductId');
      });

      it("should return null value when dynamicPage?.value?.opened is false", () => {
        currentDynamicPageMock.value = {
          type: 'product',
          productId: 'dummyProductId',
          opened: false,
        }
        const { dynamicPageProductId } = useDynamicPage();
        expect(dynamicPageProductId.value).toEqual(null);
      });
    });
  });

});
