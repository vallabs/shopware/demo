import { unref } from "@vue/composition-api";
import { useCurrency } from "@shopware-pwa/composables";
import { formatPrice as formatPriceHelper } from "@/helpers/formatPrice";

export function usePriceFilter() {
  const { currencySymbol } = useCurrency();

  const formatPrice = (price, options = {}) => {
    return formatPriceHelper(unref(price), { 
      symbol: currencySymbol.value, 
      pattern: `! #`,
      negativePattern: `-! #`,
      ...options,
    })
  }

  return {
    formatPrice
  }
}
