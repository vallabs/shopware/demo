import { ref, createApp } from "vue-demi";
import flushPromises from 'flush-promises';

// Mock API client
import * as shopwareClient from "@shopware-pwa/shopware-6-client";
jest.mock("@shopware-pwa/shopware-6-client");
const mockedApiClient = shopwareClient as jest.Mocked<typeof shopwareClient>;

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

import {useState} from '../useState';
jest.mock('../useState');
const mockedUseState = useState as jest.Mocked<typeof useState>;

import {useInteractions} from '../useInteractions';
jest.mock('../useInteractions');
const mockedUseInteractions = useInteractions as jest.Mocked<typeof useInteractions>;

import {useLastSeen} from '../useLastSeen';
jest.mock('../useLastSeen');
const mockedUseLastSeen = useLastSeen as jest.Mocked<typeof useLastSeen>;

import {useDynamicPage} from '../useDynamicPage';
jest.mock('../useDynamicPage');
const mockedUseDynamicPage = useDynamicPage as jest.Mocked<typeof useDynamicPage>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import {useErrorHandler} from "../useErrorHandler";
import { usePresentation } from "../usePresentation";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import * as GSApiClient from '../../api-client';
jest.mock('../../api-client');
const mockedGSApiClient: any = GSApiClient as jest.Mocked<typeof GSApiClient>;


function mockLoadComposableInApp(composable) {
	let result;
	const app = createApp( {
		setup() {
			result = composable();
			// suppress missing template warning
			return result;
		}
	} );
	const wrapper = app.mount( document.createElement( 'div' ) );
	return [ result, app, wrapper ];
}

describe("Composables - usePresentation", () => {
  let result;
  let app;
  let wrapper;
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const mercurePublishLocalMock = jest.fn();
  const mercurePublishGuideMock = jest.fn();
  const publishInteractionMock = jest.fn();
  const loadLastSeenMock = jest.fn();
  const closeDynamicPageMock = jest.fn();
  const resetSlidePositionMock = jest.fn();
  const broadcastMock = jest.fn();
  const getPageMock = jest.fn();
  const getSlideDataMock = jest.fn();
  const getSlideProductsMock = jest.fn();
  const getQuickViewMock = jest.fn();
  const resetCurrentClientForGuideMock = jest.fn();

  const navigationPageIndexMock = ref(null);
  const newInstantListingMock = ref(null);
  const instantListingUpdatedMock = ref(null);
  const stateForAllMock = ref(null);
  const endedMock = ref(null);
  const startedMock = ref(null);
  const dynamicPageMock = ref(null);
  const _storePresentationMock = ref(null);
  const _mediaStreamMock = ref(null);
  const dynamicPageProductIdMock = ref(null);
  const _storeProductPageMock = ref(null);
  const _slideLoadingMock = ref(null);
  const currentSlideIndexMock = ref(null);
  const newSlideIndexAfterReloadMock = ref(null);
  const isGuideMock = ref(null);
  const isClientMock = ref(null);
  const isSelfServiceMock = ref(null);

  beforeEach(() => {
    jest.resetAllMocks();

    sessionStorage.clear();
    navigationPageIndexMock.value = null;
    newInstantListingMock.value = null;
    instantListingUpdatedMock.value = null;
    stateForAllMock.value = null;
    endedMock.value = null;
    startedMock.value = null;
    dynamicPageMock.value = null;
    _storePresentationMock.value = null;
    dynamicPageProductIdMock.value = null;
    _storeProductPageMock.value = null;
    _slideLoadingMock.value = null;
    currentSlideIndexMock.value = null;
    _mediaStreamMock.value = null;
    newSlideIndexAfterReloadMock.value = null;
    isGuideMock.value = null;
    isClientMock.value = null;
    isSelfServiceMock.value = null;

    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        local: mercurePublishLocalMock,
        guide: mercurePublishGuideMock,
      }
    } as any));

    (mockedUseMercureObserver as any).mockImplementation(() => ({
      navigationPageIndex: navigationPageIndexMock,
      newInstantListing: newInstantListingMock,
      instantListingUpdated: instantListingUpdatedMock,
    } as any));

    (mockedUseState as any).mockImplementation(() => ({
      stateForAll: stateForAllMock,
      ended: endedMock,
      started: startedMock,
      resetCurrentClientForGuide: resetCurrentClientForGuideMock
    } as any));

    (mockedUseInteractions as any).mockImplementation(() => ({
      publishInteraction: publishInteractionMock
    } as any));

    (mockedUseLastSeen as any).mockImplementation(() => ({
      loadLastSeen: loadLastSeenMock
    } as any));

    (mockedUseDynamicPage as any).mockImplementation(() => ({
      dynamicPage: dynamicPageMock,
      dynamicPageProductId: dynamicPageProductIdMock,
      closeDynamicPage: closeDynamicPageMock,
      resetSlidePosition: resetSlidePositionMock,
    } as any));

    (mockedUseAppointment as any).mockImplementation(() => ({
      isGuide: isGuideMock,
      isClient: isClientMock,
      isSelfService: isSelfServiceMock,
    } as any));

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("productPage"))
            return _storeProductPageMock;
          if (contextName.includes("-page"))
            return _storePresentationMock;
          if (contextName.includes("slideLoading"))
            return _slideLoadingMock;
          if (contextName.includes("currentSlideIndex"))
            return currentSlideIndexMock;
          if (contextName.includes("newSlideIndexAfterReload"))
            return newSlideIndexAfterReloadMock;
          if (contextName.includes("-mediaStream"))
            return _mediaStreamMock;
        },
      } as any;
    });

    mockedGSApiClient.PresentationService = {
      getPage: getPageMock,
      getSlideData: getSlideDataMock,
      getSlideProducts: getSlideProductsMock,
    };

    mockedGSApiClient.QuickViewService = {
      getQuickView: getQuickViewMock,
    };

    mockedComposables.useIntercept.mockImplementation(() => {
      return {
        broadcast: broadcastMock,
      } as any;
    });

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    consoleErrorSpy.mockImplementationOnce(() => {});
    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
      app = undefined;
    }
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  describe("methods", () => {
    describe("loadPresentation", () => {
      it("should execute correctly when forceSlide && isGuide valid", async () => {
        jest.useFakeTimers();
        const { currentSlideIndex, loadPresentation } = usePresentation();
        const forceSlideDummy = 1;
        isGuideMock.value = true;
        jest.spyOn(global, 'setTimeout');
        await loadPresentation(forceSlideDummy);
        jest.runAllTimers();
        expect(getPageMock).toHaveBeenCalledTimes(1);
        expect(currentSlideIndex.value).toEqual(forceSlideDummy);
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 1);
      });

      it("should init startPresentationWatchers once", async () => {
        jest.useFakeTimers();
        dynamicPageProductIdMock.value = undefined;
        dynamicPageMock.value = {
          type: 'product',
          opened: true,
          productId: 'dummyProductId'
        }
        isGuideMock.value = true;
        [result, app, wrapper] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        await result.loadPresentation();
        await flushPromises();
        // trigger watcher
        dynamicPageProductIdMock.value = 'dummyId';
        await jest.advanceTimersByTime(1000);
        expect(getPageMock).toHaveBeenCalledTimes(2);
        expect(getQuickViewMock).toHaveBeenCalledTimes(2);
      });

      it("should execute correctly when forceSlide && isSelfService valid", async () => {
        jest.useFakeTimers();
        const { currentSlideIndex, loadPresentation } = usePresentation();
        const forceSlideDummy = 1;
        isSelfServiceMock.value = true;
        await loadPresentation(forceSlideDummy);
        jest.runAllTimers();
        expect(getPageMock).toHaveBeenCalledTimes(1);
        expect(currentSlideIndex.value).toEqual(forceSlideDummy);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
      });

      it("should execute correctly when forceSlide invalid && isSelfService valid", async () => {
        sessionStorage.setItem("sw-gs-navigated", '1');
        const { currentSlideIndex, loadPresentation } = usePresentation();
        isSelfServiceMock.value = true;
        await loadPresentation();
        expect(getPageMock).toHaveBeenCalledTimes(1);
        expect(currentSlideIndex.value).toEqual(1);
      });

      it("should execute correctly when forceSlide invalid && isGuide valid && stateForAll valid", async () => {
        jest.useFakeTimers();
        const { currentSlideIndex, loadPresentation } = usePresentation();
        isGuideMock.value = true;
        stateForAllMock.value = {
          currentSlideAlias: 2
        }
        jest.spyOn(global, 'setTimeout');
        await loadPresentation();
        jest.runAllTimers();
        expect(getPageMock).toHaveBeenCalledTimes(1);
        expect(currentSlideIndex.value).toEqual(2);
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 2);
      });

      it("should trigger loadProductPage when showProductPage valid & the type of dynamicPage is `product`", async () => {
        dynamicPageMock.value = {
          type: 'product',
          opened: true,
          productId: 'dummyProductId'
        }
        const { loadPresentation } = usePresentation();
        isGuideMock.value = true;
        await loadPresentation();
        expect(getQuickViewMock).toHaveBeenCalledWith({
          productId: 'dummyProductId',
          cmsPageLayoutId: null,
        }, rootContextMock.apiInstance);
        expect(getPageMock).toHaveBeenCalledTimes(1);
      });

      describe("should assign correct value for forceSlide with indexOfFirstSlideInGroup result when forceSlideGroup valid forceSlide invalid && isGuide valid", () => {
        it("currentSlideIndex should equal with index of navigation item when group item valid", async () => {
          jest.useFakeTimers();
          getPageMock.mockResolvedValue({
            cmsPageResults: {},
            navigation: [
              {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              id: 'string',
              index: 2,
              name: 'string',
              notes: 'string'
              }
            ]
          });
          const { currentSlideIndex, loadPresentation } = usePresentation();
          isGuideMock.value = true;
          jest.spyOn(global, 'setTimeout');
          await loadPresentation(0, 1);
          jest.runAllTimers();
          expect(getPageMock).toHaveBeenCalledTimes(1);
          expect(currentSlideIndex.value).toEqual(2);
          expect(setTimeout).toHaveBeenCalledTimes(1);
          expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
          await jest.advanceTimersByTime(1000);
          expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 2);
        });

        it("currentSlideIndex should equal with currentSlideAlias of stateForAll when treeItems invalid", async () => {
          jest.useFakeTimers();
          stateForAllMock.value = {
            currentSlideAlias: 3
          }
          getPageMock.mockResolvedValue({
            cmsPageResults: {},
            navigation: []
          });
          const { currentSlideIndex, loadPresentation } = usePresentation();
          isGuideMock.value = true;
          jest.spyOn(global, 'setTimeout');
          await loadPresentation(0, 1);
          jest.runAllTimers();
          expect(getPageMock).toHaveBeenCalledTimes(1);
          expect(currentSlideIndex.value).toEqual(3);
        });

        it("currentSlideIndex should equal with currentSlideAlias of stateForAll when the specific item of treeItems invalid", async () => {
          jest.useFakeTimers();
          stateForAllMock.value = {
            currentSlideAlias: 3
          }
          getPageMock.mockResolvedValue({
            cmsPageResults: {},
            navigation: [{
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              id: 'string',
              index: 2,
              name: 'string',
              notes: 'string'
            }]
          });
          const { currentSlideIndex, loadPresentation } = usePresentation();
          isGuideMock.value = true;
          jest.spyOn(global, 'setTimeout');
          await loadPresentation(0, 2);
          jest.runAllTimers();

          expect(currentSlideIndex.value).toEqual(3);
        });

        it("should handle correct error when API failed", async () => {
          jest.useFakeTimers();
          const res = {
            messages: ['something went wrong'],
            statusCode: 400
          }
          getPageMock.mockRejectedValueOnce(res as any);
          const { loadPresentation } = usePresentation();
          await loadPresentation();
          expect(handleErrorMock).toHaveBeenCalledWith(
            res,
            {
              context: 'usePresentation',
              method: "loadPresentation",
            }
          );
        });
      });
    });

    describe("reloadPresentation", () => {
      it("should execute correctly", async () => {
        const { currentPresentation, reloadPresentation } = usePresentation();
        _storePresentationMock.value = {
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            id: 'string',
            index: 2,
            name: 'string',
            notes: 'string'
            }
          ]
        }
        getPageMock.mockResolvedValue({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            id: 'string',
            index: 2,
            name: 'string',
            notes: 'string'
            }
          ]
        });
        await reloadPresentation();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(currentPresentation.value).toEqual({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            id: 'string',
            index: 2,
            name: 'string',
            notes: 'string'
            }
          ]
        });
      });

      it("should handle correct error when API failed", async () => {
        jest.useFakeTimers();
        const res = {
          messages: ['something went wrong'],
          statusCode: 400
        }
        getPageMock.mockRejectedValueOnce(res as any);
        const { reloadPresentation } = usePresentation();
        await reloadPresentation();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: 'usePresentation',
            method: "reloadPresentation",
          }
        );
      });
    });

    describe("loadProductPage", () => {
      it("should stop when id params invalid", async () => {
        const { loadProductPage } = usePresentation();
        const res = await loadProductPage(null);
        expect(res).toBeUndefined();
      });

      it("should excuted correctly when id params valid & publish true", async () => {
        const { loadProductPage } = usePresentation();
        stateForAllMock.value = {
          productDetailDefaultPageId: 'dummyPageId'
        }
        await loadProductPage('1');
        expect(getQuickViewMock).toHaveBeenCalledWith({
          productId: '1',
          cmsPageLayoutId: 'dummyPageId'
        }, rootContextMock.apiInstance);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: "product.viewed",
          payload: {
            productId: '1',
          },
        });
        expect(loadLastSeenMock).toHaveBeenCalledTimes(1);
      });

      it("should excuted correctly when id params valid & publish false", async () => {
        const { loadProductPage } = usePresentation();
        stateForAllMock.value = {
          productDetailDefaultPageId: 'dummyPageId'
        }
        await loadProductPage('1', false);
        expect(getQuickViewMock).toHaveBeenCalledWith({
          productId: '1',
          cmsPageLayoutId: 'dummyPageId'
        }, rootContextMock.apiInstance);
        expect(publishInteractionMock).not.toHaveBeenCalled();
        expect(loadLastSeenMock).not.toHaveBeenCalled();
      });

      it("should handle correct error when API failed", async () => {
        const res = {
          messages: ['something went wrong'],
          statusCode: 400
        }
        getQuickViewMock.mockRejectedValueOnce(res as any);
        const { loadProductPage } = usePresentation();
        await loadProductPage('1');
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: 'usePresentation',
            method: "loadProductPage",
          }
        );
      });
    });

    describe("getSlideData", () => {
      it("should stop when navigation invalid", async () => {
        _storePresentationMock.value = {
          navigation: null
        };
        const { getSlideData } = usePresentation();
        const res = await getSlideData(1);
        expect(res).toBeUndefined();
      });

      it("should stop when navItem invalid", async () => {
        _storePresentationMock.value = {
          navigation: []
        };
        const { getSlideData } = usePresentation();
        const res = await getSlideData(1);
        expect(res).toBeUndefined();
      });

      it("should executed correctly when cmsPage type presentation_product_list", async () => {
        _storePresentationMock.value = {
          cmsPageResults: [{}, {}, {}, {}],
          navigation: [{
            groupId: 'groupId',
            id: 'id',
          }]
        };
        getSlideDataMock.mockResolvedValueOnce({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        const { currentPresentation, getSlideData } = usePresentation();
        await getSlideData(1);
        expect(currentPresentation.value.cmsPageResults[0].loaded).toEqual(true);
        expect(currentPresentation.value.cmsPageResults[0]).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          loaded: true,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
      });

      it("should executed setUniqueIdentifier correctly", async () => {
        jest
          .spyOn(Date, 'now')
          .mockImplementationOnce(() =>
            new Date(1668393358066).valueOf()
          );
        _storePresentationMock.value = {
          cmsPageResults: [],
          navigation: [{
            groupId: 'groupId',
            id: 'id',
          }]
        };
        getSlideDataMock.mockResolvedValueOnce({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            sections: [
              {
                _uniqueIdentifier: 'dummyUniqueIdentifier',
                _uniqueIdentifierBackup: null
              }
            ]
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        const { currentPresentation, getSlideData } = usePresentation();
        await getSlideData(1);
        expect(currentPresentation.value.cmsPageResults[0].loaded).toEqual(true);
        expect(currentPresentation.value.cmsPageResults[0]).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            sections: [
              {
                _uniqueIdentifier: 'dummyUniqueIdentifier|1668393358066',
                _uniqueIdentifierBackup: 'dummyUniqueIdentifier'
              }
            ]
          },
          breadcrumb: null,
          loaded: true,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
      });
    });
    describe("getSlideProducts", () => {
      it("should stop when navigation invalid", async () => {
        _storePresentationMock.value = {
          navigation: null
        };
        const { getSlideProducts } = usePresentation();
        const res = await getSlideProducts(1, {});
        expect(res).toBeUndefined();
      });

      it("should stop when navItem invalid", async () => {
        _storePresentationMock.value = {
          navigation: []
        };
        const { getSlideProducts } = usePresentation();
        const res = await getSlideProducts(1, {});
        expect(res).toBeUndefined();
      });

      it("should executed correctly", async () => {
        _storePresentationMock.value = {
          cmsPageResults: [],
          navigation: [{
            groupId: 'groupId',
            sectionId: 'id',
          }]
        };
        const { getSlideProducts } = usePresentation();
        await getSlideProducts(1, {});
        expect(getSlideProductsMock).toHaveBeenCalledWith(
          'groupId',
          'id',
          {},
          rootContextMock.apiInstance,
          undefined
        );
      });

      it("should handle correct error when API failed", async () => {
        _storePresentationMock.value = {
          cmsPageResults: [],
          navigation: [{
            groupId: 'groupId',
            sectionId: 'id',
          }]
        };
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getSlideProductsMock.mockRejectedValueOnce(res as any);
        const { getSlideProducts } = usePresentation();
        await getSlideProducts(1, {});
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: 'usePresentation',
            method: "getSlideProducts",
          }
        );
      });
    });

    describe("goToSlideAfterReload", () => {
      it("should executed correctly", async () => {
        const { slideLoading, goToSlideAfterReload } = usePresentation();
        await goToSlideAfterReload(1);
        expect(slideLoading.value).toEqual(true);
        expect(newSlideIndexAfterReloadMock.value).toEqual(1);
      });
    });

    describe("goToPreviousSlide", () => {
      it("should stop when currentSlideIndex <= 1", async () => {
        jest.useFakeTimers();
        currentSlideIndexMock.value = 1;
        const { goToPreviousSlide } = usePresentation();
        isGuideMock.value = true;
        jest.spyOn(global, 'setTimeout');
        await goToPreviousSlide();
        jest.runAllTimers();
        expect(setTimeout).not.toHaveBeenCalled();
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
      });

      it("should executed correctly when currentSlideIndex > 1", async () => {
        jest.useFakeTimers();
        currentSlideIndexMock.value = 2;
        const { goToPreviousSlide } = usePresentation();
        isGuideMock.value = true;
        jest.spyOn(global, 'setTimeout');
        await goToPreviousSlide();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 1);
      });
    });

    describe("goToNextSlide", () => {
      it("should stop when currentSlideIndex >= totalCount", async () => {
        _storePresentationMock.value = {
          navigation: [{}, {}, {}]
        }
        jest.useFakeTimers();
        currentSlideIndexMock.value = 3;
        const { goToNextSlide } = usePresentation();
        isGuideMock.value = true;
        jest.spyOn(global, 'setTimeout');
        await goToNextSlide();
        jest.runAllTimers();
        expect(setTimeout).not.toHaveBeenCalled();
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).not.toHaveBeenCalled();
      });

      it("should executed correctly when currentSlideIndex < totalCount", async () => {
        jest.useFakeTimers();
        _storePresentationMock.value = {
          navigation: [{}, {}, {}]
        }
        currentSlideIndexMock.value = 1;
        const { goToNextSlide } = usePresentation();
        isGuideMock.value = true;
        jest.spyOn(global, 'setTimeout');
        await goToNextSlide();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 2);
      });
    });

    describe("reloadPage", () => {
      it("should stop when navigation invalid", async () => {
        _storePresentationMock.value = {
          navigation: null
        }
        const { reloadPage } = usePresentation();
        await reloadPage();
        expect(broadcastMock).not.toHaveBeenCalled();
      });

      it("should stop when navigation valid & currentSlideIndex invalid", async () => {
        _storePresentationMock.value = {
          navigation: [{}]
        }
        currentSlideIndexMock.value = null;
        const { reloadPage } = usePresentation();
        await reloadPage();
        expect(broadcastMock).not.toHaveBeenCalled();
      });

      it("should run correctly when navigation valid & currentSlideIndex valid", async () => {
        _storePresentationMock.value = {
          cmsPageResults: [],
          navigation: [{
            groupId: 'groupId',
            sectionId: 'id',
          }]
        }

        currentSlideIndexMock.value = 1;
        const { reloadPage } = usePresentation();
        await reloadPage();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPage');
        expect(getSlideDataMock).toHaveBeenCalledWith(
          'groupId',
          'id',
          rootContextMock.apiInstance
        );
      });
    });

    describe("goToSlide", () => {
      it ("should executed correctly if trigger once per time with self service is true", async () => {
        jest.useFakeTimers();
        isSelfServiceMock.value = true;
        jest.spyOn(global, 'setTimeout');
        const { currentSlideIndex, goToSlide } = usePresentation();
        await goToSlide(1);
        jest.runAllTimers();
        expect(currentSlideIndex.value).toEqual(1);
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('navigated', 1);
        expect(sessionStorage.getItem("sw-gs-navigated")).toEqual('1');
      });

      it ("should executed correctly if trigger once per time with self service is false", async () => {
        jest.useFakeTimers();
        isSelfServiceMock.value = false;
        jest.spyOn(global, 'setTimeout');
        const { currentSlideIndex, goToSlide } = usePresentation();
        await goToSlide(1);
        jest.runAllTimers();
        expect(currentSlideIndex.value).toEqual(1);
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 1);
      });

      it("should executed correctly if trigger multiple the same time with self service is true", async () => {
        jest.useFakeTimers();
        isSelfServiceMock.value = true;
        jest.spyOn(global, 'setTimeout');
        const { currentSlideIndex, goToSlide } = usePresentation();
        goToSlide(1);
        goToSlide(2);
        jest.runAllTimers();
        expect(currentSlideIndex.value).toEqual(2);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('navigated', 2);
        expect(sessionStorage.getItem("sw-gs-navigated")).toEqual('2');
      })

      it("should executed correctly if trigger multiple the same time with self service is false", async () => {
        jest.useFakeTimers();
        isSelfServiceMock.value = false;
        jest.spyOn(global, 'setTimeout');
        const { currentSlideIndex, goToSlide } = usePresentation();
        goToSlide(1);
        goToSlide(2);
        jest.runAllTimers();
        expect(currentSlideIndex.value).toEqual(2);
        await jest.advanceTimersByTime(1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 2);
      })

      it("should call closeDynamicPage when dynamicPage is opened", async () => {
        jest.useFakeTimers();
        dynamicPageMock.value = {
          type: 'product',
          opened: true,
          productId: 'dummyProductId'
        }
        isSelfServiceMock.value = true;
        const { currentSlideIndex, goToSlide } = usePresentation();
        goToSlide(1);
        jest.runAllTimers();
        expect(currentSlideIndex.value).toEqual(1);
        await jest.advanceTimersByTime(1000);
        expect(closeDynamicPageMock).toHaveBeenCalledWith({
          type: 'product',
          opened: true,
          productId: 'dummyProductId'
        });
      })
    });

  });

  describe("computed", () => {
    describe("currentPage", () => {
      it("should return null when _storePresentation null", () => {
        _storePresentationMock.value = null;
        const { currentPage } = usePresentation();
        expect(currentPage.value).toBeNull();
      });

      it("should return cmsPageResult item when _storePresentation valid", () => {
        const dummyCmsPage = {
          a: 'b'
        }
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: dummyCmsPage,
              breadcrumb: null,
              loaded: true,
              extensions: {},
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        }
        currentSlideIndexMock.value = 1;
        const { currentPage } = usePresentation();
        expect(currentPage.value).toEqual(dummyCmsPage);
      });
    });

    describe("currentPageResponse", () => {
      it("should return _storeProductPage value when showProductPage true", () => {
        dynamicPageMock.value = {
          type: 'product',
          opened: true,
          productId: 'dummyProductId'
        };
        _storeProductPageMock.value = {
          category: 'Category',
          type: 'CmsPageType',
          name: 'string',
        };
        const { currentPageResponse } = usePresentation();
        expect(currentPageResponse.value).toEqual({
          category: 'Category',
          type: 'CmsPageType',
          name: 'string',
        });
      });

      it("should return null when showProductPage false && _storePresentation invalid", () => {
        _storePresentationMock.value = null;
        dynamicPageMock.value = {
          type: 'product',
          opened: false,
          productId: 'dummyProductId'
        };
        const { currentPageResponse } = usePresentation();
        expect(currentPageResponse.value).toBeNull();
      });

      it("should return cmsPageResult item when showProductPage false && _storePresentation valid", () => {
        dynamicPageMock.value = {
          type: 'product',
          opened: false,
          productId: 'dummyProductId'
        };
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: {},
              breadcrumb: null,
              loaded: true,
              extensions: {},
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        };
        const { currentPageResponse } = usePresentation();
        expect(currentPageResponse.value).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {},
          breadcrumb: null,
          loaded: true,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
      });

    });

    describe("currentMainPageResponse", () => {
      it("should return null value when _storePresentation invalid", () => {
        _storePresentationMock.value = null;
        const { currentMainPageResponse } = usePresentation();
        expect(currentMainPageResponse.value).toBeNull();
      });

      it("should return correct value when _storePresentation valid", () => {
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: {},
              breadcrumb: null,
              loaded: true,
              extensions: {},
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        };
        const { currentMainPageResponse } = usePresentation();
        expect(currentMainPageResponse.value).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {},
          breadcrumb: null,
          loaded: true,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
      });
    });

    describe("indexOfFirstSlideInNextGroup", () => {
      it("should return correct value", () => {
        currentSlideIndexMock.value = 2;
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: {},
              breadcrumb: null,
              loaded: true,
              extensions: {},
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 1,
              sectionName: 'string',
              notes: 'string'
            },
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            },
            {
              groupId: 'groupIdDummy2',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 3,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        };
        const { indexOfFirstSlideInNextGroup } = usePresentation();
        expect(indexOfFirstSlideInNextGroup.value).toEqual(3);
      });
    });

    describe("isProductListing", () => {
      it("should return correct value", () => {
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: {},
              breadcrumb: null,
              loaded: true,
              extensions: {
                cmsPageRelation: {
                  isInstantListing: true
                }
              },
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        };
        const { isProductListing, currentMainPageResponse } = usePresentation();
        expect(currentMainPageResponse.value).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {},
          breadcrumb: null,
          loaded: true,
          extensions: {
            cmsPageRelation: {
              isInstantListing: true
            }
          },
          apiAlias: "pwa_page_result",
        });
        expect(isProductListing.value).toEqual(true);
      });
    });

    describe("listingProductIds", () => {
      it("should return correct value", () => {
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: {},
              breadcrumb: null,
              loaded: true,
              extensions: {
                cmsPageRelation: {
                  isInstantListing: true,
                  pickedProductIds: ['1', '2', '3']
                }
              },
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        };
        const { listingProductIds } = usePresentation();
        expect(listingProductIds.value).toEqual(['1', '2', '3']);
      });
    });

    describe("listingProductStreamId", () => {
      it("should return correct value", () => {
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: [
            {
              category: {},
              resourceType: null,
              resourceIdentifier: null,
              canonicalPathInfo: null,
              cmsPage: {},
              breadcrumb: null,
              loaded: true,
              extensions: {
                cmsPageRelation: {
                  isInstantListing: true,
                  productStreamId: 'dummyStreamId'
                }
              },
              apiAlias: "pwa_page_result",
            }
          ],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        };
        const { listingProductStreamId } = usePresentation();
        expect(listingProductStreamId.value).toEqual('dummyStreamId');
      });
    });
  });

  describe("watchers", () => {
    describe("dynamicPageProductId", () => {
      it("should not load loadProductPage when value invalid", async () => {
        isGuideMock.value = true;
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();

        // trigger watcher
        dynamicPageProductIdMock.value = undefined;
        expect(getQuickViewMock).not.toHaveBeenCalled();
      });

      it("should not load loadProductPage when value valid & dynamicPage type is not `product`", async () => {
        dynamicPageMock.value = {
          type: 'dummyType',
          opened: true,
          productId: 'dummyProductId'
        }
        isGuideMock.value = true;
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();

        // trigger watcher
        dynamicPageProductIdMock.value = 'dummyId';
        expect(getQuickViewMock).not.toHaveBeenCalled();
      });

      it("should load loadProductPage when value valid & dynamicPage type is `product`", async () => {
        dynamicPageMock.value = {
          type: 'product',
          opened: true,
          productId: 'dummyProductId'
        }
        isGuideMock.value = true;
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();

        // trigger watcher
        dynamicPageProductIdMock.value = 'dummyId';
        expect(getQuickViewMock).toHaveBeenCalledTimes(1);
      });
    });

    describe("navigationPageIndex", () => {
      it("should not load getSlideData when navigationPageIndex is equal with currentSlideIndex", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        isGuideMock.value = true;
        isClientMock.value = false;
        currentSlideIndexMock.value = 1;
        getPageMock.mockResolvedValue({
          cmsPageResults: [],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            id: 'string',
            index: 2,
            name: 'string',
            notes: 'string'
            }
          ]
        });
        getSlideDataMock.mockResolvedValueOnce({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        jest.runAllTimers();
        jest.runAllTicks();

        // trigger watcher
        navigationPageIndexMock.value = 1;
        await flushPromises();
        expect(getSlideDataMock).not.toHaveBeenCalled();
      });

      it("should not load getSlideData when ClientIsWaiting true", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        isGuideMock.value = false;
        isClientMock.value = true;
        isSelfServiceMock.value = false;
        stateForAllMock.value = {
          started: false,
          running: false
        }
        currentSlideIndexMock.value = null;
        getPageMock.mockResolvedValue({
          cmsPageResults: [],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        getSlideDataMock.mockResolvedValueOnce({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        jest.runAllTimers();
        jest.runAllTicks();

        // trigger watcher
        navigationPageIndexMock.value = 1;
        await flushPromises();
        expect(getSlideDataMock).toHaveBeenCalledTimes(1);
      });

      it("should load getSlideData when navigationPageIndex is not equal with currentSlideIndex & ClientIsWaiting false", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        _storePresentationMock.value = null;
        isGuideMock.value = true;
        isClientMock.value = false;
        currentSlideIndexMock.value = null;
        getPageMock.mockResolvedValue({
          cmsPageResults: [],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        getSlideDataMock.mockResolvedValue({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        jest.runAllTimers();
        jest.runAllTicks();
        expect(currentSlideIndexMock.value).toEqual(null);

        // trigger watcher
        navigationPageIndexMock.value = 1;
        await flushPromises();
        expect(currentSlideIndexMock.value).toEqual(1);
        expect(result.presentation.value.cmsPageResults[0]).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          loaded: true,
          cmsPage: { type: 'presentation_product_list' },
          breadcrumb: null,
          extensions: {},
          apiAlias: 'pwa_page_result'
        });
      });
    });

    describe("started", () => {
      it("should load getSlideData correctly", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        startedMock.value = false;
        currentSlideIndexMock.value = null;
        stateForAllMock.value = {
          currentSlideAlias: 1
        };
        getPageMock.mockResolvedValue({
          cmsPageResults: [],
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        getSlideDataMock.mockResolvedValue({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        jest.runAllTimers();
        jest.runAllTicks();

        // trigger watcher
        startedMock.value = true;
        await flushPromises();
        expect(currentSlideIndexMock.value).toEqual(1);
        expect(result.presentation.value.cmsPageResults[0]).toEqual({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          loaded: true,
          cmsPage: { type: 'presentation_product_list' },
          breadcrumb: null,
          extensions: {},
          apiAlias: 'pwa_page_result'
        });
      });
    });

    describe("ended", () => {
      it("should reloadPresentation correctly when ended with self service is true", async () => {
        const getUserMediaFncMock = jest.fn();
        const trackStop = jest.fn();
        getUserMediaFncMock.mockResolvedValue({
          active: true,
          id: "2TGQ9XI07uSSjrjiOhgLumpmc10Oa6R00nzb",
          getTracks: () => {
            return [
              {
                id: '1',
                stop: trackStop
              },
              {
                id: '2',
                stop: trackStop
              }
            ]
          }
        });
        (global.navigator as any).mediaDevices = {
          getUserMedia: getUserMediaFncMock,
        }
        _mediaStreamMock.value = await navigator.mediaDevices.getUserMedia({
          video: false
        });
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        endedMock.value = false;
        _storePresentationMock.value = {
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        }
        getPageMock.mockResolvedValue({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        isSelfServiceMock.value = true;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        endedMock.value = true;
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        jest.runAllTimers();
        await flushPromises();
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('navigated', 1);
        expect(sessionStorage.getItem("sw-gs-navigated")).toEqual('1');
        expect(trackStop).toHaveBeenCalled();
      });

      it("should reloadPresentation correctly when ended with self service is false", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        endedMock.value = false;
        _storePresentationMock.value = {
            cmsPageResults: {},
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        }
        getPageMock.mockResolvedValue({
            cmsPageResults: {},
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        });
        isSelfServiceMock.value = false;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        endedMock.value = true;
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
            cmsPageResults: {},
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        });
        jest.runAllTimers();
        await flushPromises();
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 1);
      });
    });

    describe("newInstantListing", () => {
      it("should stop when newInstantListing false", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        newInstantListingMock.value = null;
        newSlideIndexAfterReloadMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        }
        getPageMock.mockResolvedValue({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        isSelfServiceMock.value = true;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        newInstantListingMock.value = false;
        await flushPromises();
        expect(broadcastMock).not.toHaveBeenCalledWith('usePresentation-reloadPresentation');
      });

      it("should reloadPresentation correctly when newInstantListing changes with self service is true", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        newInstantListingMock.value = null;
        newSlideIndexAfterReloadMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        }
        getPageMock.mockResolvedValue({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        isSelfServiceMock.value = true;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        newInstantListingMock.value = true;
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
          cmsPageResults: {},
          navigation: [
            {
            groupId: 'groupIdDummy',
            groupName: 'groupNameDummy',
            cmsPageId: 'string',
            sectionId: 'string',
            index: 2,
            sectionName: 'string',
            notes: 'string'
            }
          ]
        });
        jest.runAllTimers();
        await flushPromises();
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('navigated', 1);
        expect(sessionStorage.getItem("sw-gs-navigated")).toEqual('1');
      });

      it("should reloadPresentation correctly when newInstantListing changes with self service is false", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        newInstantListingMock.value = null;
        newSlideIndexAfterReloadMock.value = 1;
        _storePresentationMock.value = {
            cmsPageResults: {},
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        }
        getPageMock.mockResolvedValue({
            cmsPageResults: {},
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        });
        isSelfServiceMock.value = false;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        newInstantListingMock.value = true;
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
            cmsPageResults: {},
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        });
        jest.runAllTimers();
        await flushPromises();
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 1);
      });
    });

    describe("instantListingUpdated", () => {
      it("should reloadPresentation correctly when the updated field of instantListingUpdated is true", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        instantListingUpdatedMock.value = null;
        _storePresentationMock.value = {
          cmsPageResults: {},
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        }
        getPageMock.mockResolvedValue({
          cmsPageResults: {},
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        });
        isSelfServiceMock.value = true;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        instantListingUpdatedMock.value = {
          updated: true
        };
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
          cmsPageResults: {},
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        });
        expect(instantListingUpdatedMock.value.updated).toEqual(false);
      });

      it("should execute goToSlide when (currentSlideIndex - 1) equal with currentIlIndex and self service is true", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        instantListingUpdatedMock.value = null;
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
          cmsPageResults: [{
            cmsPage: {}
          }],
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        }
        getPageMock.mockResolvedValue({
          cmsPageResults: [{
            cmsPage: {}
          }],
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        });
        getSlideDataMock.mockResolvedValueOnce({
          category: {},
          resourceType: null,
          resourceIdentifier: null,
          canonicalPathInfo: null,
          cmsPage: {
            type: 'presentation_product_list'
          },
          breadcrumb: null,
          extensions: {},
          apiAlias: "pwa_page_result",
        });
        isSelfServiceMock.value = true;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        instantListingUpdatedMock.value = {
          updated: true,
          currentIlIndex: 0
        };
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
          cmsPageResults: [{
            cmsPage: {},
            "loaded": false,
          }],
          navigation: [
            {
              groupId: 'groupIdDummy',
              groupName: 'groupNameDummy',
              cmsPageId: 'string',
              sectionId: 'string',
              index: 2,
              sectionName: 'string',
              notes: 'string'
            }
          ]
        });
        jest.runAllTimers();
        await flushPromises();
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('navigated', 1);
        expect(sessionStorage.getItem("sw-gs-navigated")).toEqual('1');
        expect(instantListingUpdatedMock.value.updated).toEqual(false);
      });

      it("should execute goToSlide when (currentSlideIndex - 1) equal with currentIlIndex and self service is false", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');

        instantListingUpdatedMock.value = null;
        currentSlideIndexMock.value = 1;
        _storePresentationMock.value = {
            cmsPageResults: [{
                cmsPage: {}
            }],
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        }
        getPageMock.mockResolvedValue({
            cmsPageResults: [{
                cmsPage: {}
            }],
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        });
        getSlideDataMock.mockResolvedValueOnce({
            category: {},
            resourceType: null,
            resourceIdentifier: null,
            canonicalPathInfo: null,
            cmsPage: {
                type: 'presentation_product_list'
            },
            breadcrumb: null,
            extensions: {},
            apiAlias: "pwa_page_result",
        });
        isSelfServiceMock.value = false;

        [result, app] = mockLoadComposableInApp(() => usePresentation());
        await result.loadPresentation();
        instantListingUpdatedMock.value = {
            updated: true,
            currentIlIndex: 0
        };
        await flushPromises();
        expect(broadcastMock).toHaveBeenCalledWith('usePresentation-reloadPresentation');
        expect(result.currentPresentation.value).toEqual({
            cmsPageResults: [{
                cmsPage: {},
                "loaded": false,
            }],
            navigation: [
                {
                    groupId: 'groupIdDummy',
                    groupName: 'groupNameDummy',
                    cmsPageId: 'string',
                    sectionId: 'string',
                    index: 2,
                    sectionName: 'string',
                    notes: 'string'
                }
            ]
        });
        jest.runAllTimers();
        await flushPromises();
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('navigated', 1);
        expect(instantListingUpdatedMock.value.updated).toEqual(false);
      });
    });
  });
});
