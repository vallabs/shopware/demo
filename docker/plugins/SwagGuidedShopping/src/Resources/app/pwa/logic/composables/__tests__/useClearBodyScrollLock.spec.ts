import { createApp } from "vue-demi";
import { useClearBodyScrollLock } from "../useClearBodyScrollLock";

import { clearAllBodyScrollLocks } from "body-scroll-lock";
jest.mock("body-scroll-lock");
const mockedClearAllBodyScrollLocks = clearAllBodyScrollLocks as jest.Mocked<
  typeof clearAllBodyScrollLocks
>;

function mockLoadComposableInApp(composable) {
  let result;
  const app = createApp({
  setup() {
    result = composable();
    // suppress missing template warning
    return result;
  },
  });
  const wrapper = app.mount(document.createElement("div"));
  return [result, app, wrapper];
}

describe("Composables - useClearBodyScrollLock", () => {
  let result;
  let app;

  it('hooks method have been called', async () => {
    jest.useFakeTimers();
    jest.spyOn(global, 'setTimeout');
    [result, app] = mockLoadComposableInApp(() => useClearBodyScrollLock());
    await jest.setTimeout(1);
    jest.runAllTimers();
    expect(mockedClearAllBodyScrollLocks).toHaveBeenCalledTimes(1);
    app.unmount()
    jest.runAllTimers();
    expect(mockedClearAllBodyScrollLocks).toHaveBeenCalledTimes(2);
  });
});
