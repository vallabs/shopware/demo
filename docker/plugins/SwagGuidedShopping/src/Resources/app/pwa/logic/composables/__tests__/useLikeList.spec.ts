import { ref, createApp } from "vue-demi";
import { Product } from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import {useInteractions} from '../useInteractions';
jest.mock('../useInteractions');
const mockedUseInteractions = useInteractions as jest.Mocked<typeof useInteractions>;

import {useCustomerInsights} from '../widgets/useCustomerInsights';
jest.mock('../widgets/useCustomerInsights');
const mockedUseCustomerInsights = useCustomerInsights as jest.Mocked<typeof useCustomerInsights>;

import {useState} from '../useState';
jest.mock('../useState');
const mockedUseState = useState as jest.Mocked<typeof useState>;

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

import {useDynamicPage} from '../useDynamicPage';
jest.mock('../useDynamicPage');
const mockedUseDynamicPage = useDynamicPage as jest.Mocked<typeof useDynamicPage>;

import {useAdminApiInstance} from '../useAdminApiInstance';
jest.mock('../useAdminApiInstance');
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<typeof useAdminApiInstance>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { useLikeList } from "../useLikeList";

import * as GSApiClient from '../../api-client';
jest.mock('../../api-client');
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

function mockLoadComposableInApp(composable) {
	let result;
	const app = createApp( {
		setup() {
			result = composable();
			// suppress missing template warning
			return result;
		}
	} );
	const wrapper = app.mount( document.createElement( 'div' ) );
	return [ result, app, wrapper ];
}

describe("Composables - useLikeList", () => {
  let result;
  let app;
  let wrapper;
  const product = ref<any>({
    id: 'dummyProductId',
  })
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const addLikedProductForAttendeeMock = jest.fn(() => Promise.resolve(null as any));
  const addDislikedProductForAttendeeMock = jest.fn(() => Promise.resolve(null as any));
  const removeLikedProductForAttendeeMock = jest.fn(() => Promise.resolve(null as any));
  const removeDislikedProductForAttendeeMock = jest.fn(() => Promise.resolve(null as any));
  const getDislikedProductsForAttendeeMock = jest.fn(() => Promise.resolve(null as any));
  const getLikedProductsForAttendeeMock = jest.fn(() => Promise.resolve(null as any));
  const getDislikedProductsMock = jest.fn(() => Promise.resolve(null as any));
  const getLikedProductsMock = jest.fn(() => Promise.resolve(null as any));
  const refreshCustomerDataMock = jest.fn(() => Promise.resolve(null as any));
  const addLikedProductMock = jest.fn(() => Promise.resolve(null as any));
  const addDislikedProductMock = jest.fn(() => Promise.resolve(null as any));
  const removeLikedProductMock = jest.fn(() => Promise.resolve(null as any));
  const removeDislikedProductMock = jest.fn(() => Promise.resolve(null as any));
  const getLikesAndDislikesForProductMock = jest.fn(() => Promise.resolve(null as any));
  const mercurePublishAllMock = jest.fn();
  const publishInteractionMock = jest.fn();

  const _storeLikeList = ref<Product[]>([]);
  const _storeGuideLikeList = ref<Product[]>([]);
  const _storeDislikeList = ref<Product[]>([]);

  const likedProductMock = ref(null);
  const currentClientForGuideMock = ref(null);
  const lastClientChangedLikesMock = ref(null);
  const lastGuideChangedLikesMock = ref(null);
  const dynamicPageMock = ref({
    type: '',
    opened: false
  });

  // mockedCompositionAPI.onUnmounted = jest.fn();
  beforeEach(() => {
    jest.resetAllMocks();
    _storeLikeList.value = [];
    _storeGuideLikeList.value = [];
    _storeDislikeList.value = [];
    product.value = {
      id: 'dummyProductId'
    } as any;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("dislikeList"))
            return _storeDislikeList;
          if (contextName.includes("likeList"))
            return _storeLikeList;
          if (contextName.includes("guideLikeList"))
            return _storeGuideLikeList;
        },
      } as any;
    });
    (mockedUseAppointment as any).mockImplementation(() => ({
      isGuide: ref(true),
      isClient: ref(false),
      appointment: ref(null),
    } as any));
    (mockedUseInteractions as any).mockImplementation(() => ({
      publishInteraction: publishInteractionMock
    } as any));
    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));
    (mockedUseCustomerInsights as any).mockImplementation(() => ({
      refreshCustomerData: refreshCustomerDataMock
    } as any));
    (mockedUseState as any).mockImplementation(() => ({
      currentClientForGuide: currentClientForGuideMock,
    } as any));
    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        all: mercurePublishAllMock,
      }
    } as any));
    (mockedUseMercureObserver as any).mockImplementation(() => ({
      lastClientChangedLikes: lastClientChangedLikesMock,
      likedProduct: likedProductMock,
      lastGuideChangedLikes: lastGuideChangedLikesMock,
    } as any));
    (mockedUseDynamicPage as any).mockImplementation(() => ({
      dynamicPage: dynamicPageMock
    } as any));
    (mockedUseAdminApiInstance as any).mockImplementation(() => ({
      adminApiInstance: ref({}),
    } as any));

    mockedGSApiClient.LikeListService = {
      addLikedProductForAttendee: addLikedProductForAttendeeMock,
      addDislikedProductForAttendee: addDislikedProductForAttendeeMock,
      removeLikedProductForAttendee: removeLikedProductForAttendeeMock,
      removeDislikedProductForAttendee: removeDislikedProductForAttendeeMock,
      addLikedProduct: addLikedProductMock,
      addDislikedProduct: addDislikedProductMock,
      removeLikedProduct: removeLikedProductMock,
      removeDislikedProduct: removeDislikedProductMock,
      getLikedProducts: getLikedProductsMock,
      getDislikedProducts: getDislikedProductsMock,
      getLikesAndDislikesForProduct: getLikesAndDislikesForProductMock,
      getLikedProductsForAttendee: getLikedProductsForAttendeeMock,
      getDislikedProductsForAttendee: getDislikedProductsForAttendeeMock,
      getProductsFromIdList: () => new Promise(resolve => resolve(null as any)),
    };

    consoleErrorSpy.mockImplementationOnce(() => {});

    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
    }
  });

  describe("methods", () => {
    describe("changeProduct", () => {
      it("should change product id if value valid", () => {
        const newProduct: any = {
          id: 'newDummyProductId',
        };
        const { productId, changeProduct } = useLikeList(product.value);
        expect(productId.value).toEqual(product.value.id);
        changeProduct(newProduct);
        expect(productId.value).toEqual(newProduct.id);
      });

      it("should not change product id if value valid", async () => {
        const { productId, changeProduct } = useLikeList(product.value);
        changeProduct(null);
        expect(productId.value).toEqual(product.value.id);
      });
    });

    describe("like", () => {
      it("should only run once if user perform multiple like at the same time", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { like } = useLikeList(product.value);
        await Promise.all([like(), like()]);
        expect(addLikedProductForAttendeeMock).toHaveBeenCalledTimes(1);
      });

      it("should publish mercure event guide.changedLikes & product.wasLiked and publish interaction attendee.product.collection.liked when user is guide & currentClientForGuide valid", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { like } = useLikeList(product.value);
        await like();
        expect(addLikedProductForAttendeeMock).toHaveBeenCalledTimes(1);
        expect(getLikedProductsForAttendeeMock).not.toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).not.toHaveBeenCalled();
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'guide.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.liked',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should publish mercure event client.changedLikes & product.wasLiked and publish interaction attendee.product.collection.liked when user is isClient", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const { like } = useLikeList(product.value);
        await like();
        expect(addLikedProductMock).toHaveBeenCalledTimes(1);
        expect(getLikedProductsMock).toHaveBeenCalled();
        expect(getDislikedProductsMock).toHaveBeenCalled();
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'client.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.liked',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should handle correct error when API failed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.addLikedProduct = () => new Promise((resolve, reject) => reject(res));

        const { like } = useLikeList(product.value);
        await like();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "like"
          }
        );
      });
    });

    describe("dislike", () => {
      it("should only run once if user perform multiple dislike at the same time", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { dislike } = useLikeList(product.value);
        await Promise.all([dislike(), dislike()]);
        expect(addDislikedProductForAttendeeMock).toHaveBeenCalledTimes(1);
      });

      it("should publish mercure event guide.changedLikes & product.wasLiked and publish interaction attendee.product.collection.disliked when user is guide & currentClientForGuide valid", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { dislike } = useLikeList(product.value);
        await dislike();
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'guide.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.disliked',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should publish mercure event client.changedLikes & product.wasLiked and publish interaction attendee.product.collection.disliked when user is isClient", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const { dislike } = useLikeList(product.value);
        await dislike();
        expect(addDislikedProductMock).toHaveBeenCalledTimes(1);
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'client.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.disliked',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should handle correct error when API failed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.addDislikedProduct = () => new Promise((resolve, reject) => reject(res));

        const { dislike } = useLikeList(product.value);
        await dislike();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "dislike"
          }
        );
      });
    });

    describe("removeFromLikeList", () => {
      it("should only run once if user perform multiple removeFromLikeList at the same time", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { removeFromLikeList } = useLikeList(product.value);
        await Promise.all([removeFromLikeList(), removeFromLikeList()]);
        expect(removeLikedProductForAttendeeMock).toHaveBeenCalledTimes(1);
      });

      it("should publish mercure event guide.changedLikes & product.wasLiked and publish interaction attendee.product.collection.removed when user is guide & currentClientForGuide valid", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { removeFromLikeList } = useLikeList(product.value);
        await removeFromLikeList();
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'guide.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.removed',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should publish mercure event client.changedLikes & product.wasLiked and publish interaction attendee.product.collection.removed when user is isClient", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const { removeFromLikeList } = useLikeList(product.value);
        await removeFromLikeList();
        expect(removeLikedProductMock).toHaveBeenCalledTimes(1);
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'client.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.removed',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should handle correct error when API failed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.removeLikedProduct = () => new Promise((resolve, reject) => reject(res));

        const { removeFromLikeList } = useLikeList(product.value);
        await removeFromLikeList();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "removeFromLikeList"
          }
        );
      });
    });

    describe("removeFromDisLikeList", () => {
      it("should only run once if user perform multiple removeFromDisLikeList at the same time", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { removeFromDislikeList } = useLikeList(product.value);
        await Promise.all([removeFromDislikeList(), removeFromDislikeList()]);
        expect(removeDislikedProductForAttendeeMock).toHaveBeenCalledTimes(1);
      });

      it("should publish mercure event guide.changedLikes & product.wasLiked and publish interaction attendee.product.collection.removed when user is guide & currentClientForGuide valid", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { removeFromDislikeList } = useLikeList(product.value);
        await removeFromDislikeList();
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'guide.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.removed',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should publish mercure event client.changedLikes & product.wasLiked and publish interaction attendee.product.collection.removed when user is isClient", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const { removeFromDislikeList } = useLikeList(product.value);
        await removeFromDislikeList();
        expect(removeDislikedProductMock).toHaveBeenCalledTimes(1);
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(1, 'client.changedLikes', '123');
        expect(mercurePublishAllMock).toHaveBeenNthCalledWith(2, 'product.wasLiked', product.value.id);
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.removed',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should handle correct error when API failed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.removeDislikedProduct = () => new Promise((resolve, reject) => reject(res));

        const { removeFromDislikeList } = useLikeList(product.value);
        await removeFromDislikeList();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "removeFromDislikeList"
          }
        );
      });
    });

    describe("loadLikeAndDislikeList", () => {
      it("should run loadLikeList & loadDislikeList when user is isGuide & currentClientForGuide is valid", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { loadLikeAndDislikeList } = useLikeList(product.value);
        await loadLikeAndDislikeList();
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalledTimes(1);
        expect(getDislikedProductsForAttendeeMock).toHaveBeenCalledTimes(1);
        expect(getLikedProductsMock).not.toHaveBeenCalled();
        expect(getDislikedProductsMock).not.toHaveBeenCalled();
      });

      it("should run loadLikeList & loadDislikeList when user is isClient", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        const { loadLikeAndDislikeList } = useLikeList(product.value);
        await loadLikeAndDislikeList();
        expect(getLikedProductsForAttendeeMock).not.toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).not.toHaveBeenCalled();
        expect(getLikedProductsMock).toHaveBeenCalledTimes(1);
        expect(getDislikedProductsMock).toHaveBeenCalledTimes(1);
      });
    });

    describe("guideLike", () => {
      it("should call API addLikedProduct & publish correct interaction", async () => {
        const { guideLike } = useLikeList(product.value);
        await guideLike();
        expect(addLikedProductMock).toHaveBeenCalled();
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.liked',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should handle correct error when API failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.addLikedProduct = () => new Promise((resolve, reject) => reject(res));

        const { guideLike } = useLikeList(product.value);
        await guideLike();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "like"
          }
        );
      });
    });

    describe("removeFromGuideList", () => {
      it("should call API removeLikedProduct & publish correct interaction", async () => {
        const { removeFromGuideList } = useLikeList(product.value);
        await removeFromGuideList();
        expect(removeLikedProductMock).toHaveBeenCalled();
        expect(publishInteractionMock).toHaveBeenCalledWith({
          name: 'attendee.product.collection.removed',
          payload: {
            productId: product.value.id,
          }
        });
      });

      it("should handle correct error when API failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.removeLikedProduct = () => new Promise((resolve, reject) => reject(res));

        const { removeFromGuideList } = useLikeList(product.value);
        await removeFromGuideList();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "removeFromLikeList"
          }
        );
      });
    });

    describe("loadDislikeList", () => {
      it("should call API getDislikedProductsForAttendee when user is guide && current client existed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { loadDislikeList } = useLikeList(product.value);
        await loadDislikeList();
        expect(getDislikedProductsForAttendeeMock).toHaveBeenCalled();
      });

      it("should have correct _storeDislikeList value when elements from API is array", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        mockedGSApiClient.LikeListService.getDislikedProducts = jest.fn(() => new Promise((resolve, reject) => resolve({
          elements: [ {id: 1} ]
        } as any)));
        const { loadDislikeList, dislikeList } = useLikeList(product.value);
        await loadDislikeList();
        expect(dislikeList.value.length).toEqual(1);
      });

      it("should save _storeDislikeList with empty array when elements from API not existed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        mockedGSApiClient.LikeListService.getDislikedProducts = () => new Promise((resolve) => resolve(null as any));
        const { loadDislikeList, dislikeList } = useLikeList(product.value);
        await loadDislikeList();
        expect(dislikeList.value).toEqual([]);
      });

      it("should call API getDislikedProducts when user is client", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        const { loadDislikeList } = useLikeList(product.value);
        await loadDislikeList();
        expect(getDislikedProductsMock).toHaveBeenCalled();
      });

      it("should handle correct error when API failed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.getDislikedProducts = () => new Promise((resolve, reject) => reject(res));

        const { loadDislikeList } = useLikeList(product.value);
        await loadDislikeList();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "loadDislikeList"
          }
        );
      });
    });

    describe("loadLikeList", () => {
      it("should call API getLikedProductsForAttendeeMock when user is guide && current client existed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        const { loadLikeList } = useLikeList(product.value);
        await loadLikeList();
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalled();
      });

      it("should call API getLikedProducts when user is client", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        const { loadLikeList } = useLikeList(product.value);
        await loadLikeList();
        expect(getLikedProductsMock).toHaveBeenCalled();
      });

      it("should have correct _storeLikeList value when elements from API is array", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        mockedGSApiClient.LikeListService.getLikedProducts = () => new Promise((resolve, reject) => resolve({
          elements: [ {id: 1} ]
        } as any));
        const { loadLikeList, likeList } = useLikeList(product.value);
        await loadLikeList();
        expect(likeList.value.length).toEqual(1);
      });

      it("should return empty array when elements from API not existed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        mockedGSApiClient.LikeListService.getLikedProducts = () => new Promise((resolve, reject) => resolve({
          elements: null
        } as any));
        const { loadLikeList, likeList } = useLikeList(product.value);
        await loadLikeList();
        expect(likeList.value).toEqual([]);
      });

      it("should handle correct error when API failed", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(true),
          appointment: ref(null),
        } as any));
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.getLikedProducts = () => new Promise((resolve, reject) => reject(res));

        const { loadLikeList } = useLikeList(product.value);
        await loadLikeList();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "loadLikeList"
          }
        );
      });
    });

    describe("loadGuideProductList", () => {
      it("should return correct array value for guideLikeList when API called successfully", async () => {
        mockedGSApiClient.LikeListService.getLikedProducts = () => new Promise((resolve, reject) => resolve({
          elements: [{id:1}]
        } as any));
        const { loadGuideProductList, guideLikeList } = useLikeList(product.value);
        await loadGuideProductList();
        expect(guideLikeList.value.length).toEqual(1);
      });

      it("should handle correct error when API failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.getLikedProducts = () => new Promise((resolve, reject) => reject(res));

        const { loadGuideProductList } = useLikeList(product.value);
        await loadGuideProductList();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "loadGuideProductList"
          }
        );
      });
    });

    describe("loadAllProductLikesAndDislikes", () => {
      it("should return when no product", async () => {
        const { loadAllProductLikesAndDislikes } = useLikeList();
        const res = await loadAllProductLikesAndDislikes();
        expect(res).toBeUndefined();
      });

      it("should return likes & dislikes count if product contains those information", async () => {
        product.value = {
          id: 'dummyProductId',
          extensions: {
            attendeeProductCollections: {
              likes: 10,
              dislikes: 20
            }
          }
        }
        const { loadAllProductLikesAndDislikes, allProductLikesCount, allProductDislikesCount } = useLikeList(product.value);
        await loadAllProductLikesAndDislikes();
        expect(allProductLikesCount.value).toEqual(10);
        expect(allProductDislikesCount.value).toEqual(20);
      });

      it("should fetch API getLikesAndDislikesForProduct to get likes & dislikes count if product does not contain those information", async () => {
        const { loadAllProductLikesAndDislikes, allProductLikesCount, allProductDislikesCount } = useLikeList(product.value);
        mockedGSApiClient.LikeListService.getLikesAndDislikesForProduct = () => new Promise((resolve, reject) => resolve([
          {
            alias: 'liked'
          },
          {
            alias: 'liked'
          },
          {
            alias: 'liked'
          },
          {
            alias: 'liked'
          },
          {
            alias: 'disliked'
          },
          {
            alias: 'disliked'
          }
        ]));
        await loadAllProductLikesAndDislikes();
        expect(allProductLikesCount.value).toEqual(4);
        expect(allProductDislikesCount.value).toEqual(2);
      });

      it("should handle correct error when API failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        mockedGSApiClient.LikeListService.getLikesAndDislikesForProduct = () => new Promise((resolve, reject) => reject(res));

        const { loadAllProductLikesAndDislikes } = useLikeList(product.value);
        await loadAllProductLikesAndDislikes();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLikeList",
            method: "loadAllProductLikesAndDislikes"
          }
        );
      });
    });

    describe("initLikeListForProductWidget", () => {
      it("should make likedProductMock equal empty string & load like, dislike count", async () => {
        product.value = {
          id: 'dummyProductId',
          extensions: {
            interaction: {
              likes: 10,
              dislikes: 20
            }
          }
        };
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForProductWidget();
        expect(likedProductMock.value).toEqual('');
      });
    });

    describe("initLikeListForGuide", () => {
      it("should add only 1 time", async () => {
        [result, app, wrapper] = mockLoadComposableInApp(() => useLikeList(product.value));
        await wrapper.initLikeListForGuide();
        const res = await wrapper.initLikeListForGuide();
        expect(res).toBeUndefined();
      });

      it("should run loadLikeAndDislikeList when currentClientForGuide valid", async () => {
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        [result, app, wrapper] = mockLoadComposableInApp(() => useLikeList(product.value));
        const res = await wrapper.initLikeListForGuide();
        expect(res).toBeUndefined();
      });
    });

    describe("initLikeListForClient", () => {
      it("should add only 1 time", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        [result, app, wrapper] = mockLoadComposableInApp(() => useLikeList(product.value));
        await wrapper.initLikeListForClient();
        await wrapper.initLikeListForClient();
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalledTimes(1);
      });
    });

  });

  describe("watchers", () => {
    describe("likedProduct", () => {
      it("should make likedProductMock equal empty string & load like, dislike count", async () => {
        product.value = {
          id: 'dummyProductId',
          extensions: {
            attendeeProductCollections: {
              likes: 10,
              dislikes: 20
            }
          }
        };
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForProductWidget();
        // trigger watcher
        likedProductMock.value = 'dummyProductId';
        await new Promise((r) => setTimeout(r, 500));
        expect(likedProductMock.value).toEqual('');
        expect(result.allProductLikesCount.value).toEqual(10);
        expect(result.allProductDislikesCount.value).toEqual(20);
      });

      it("should skip watch 1 time when productId undefined", async () => {

        [result, app] = mockLoadComposableInApp(() => useLikeList());
        await result.initLikeListForProductWidget();
        // trigger watcher
        likedProductMock.value = 'newdummyProductId';
        expect(result.allProductLikesCount.value).toEqual(0);
        expect(result.allProductDislikesCount.value).toEqual(0);
      });

      it("should skip watch 1 time when productId have value but not same with product", async () => {
        product.value = {
          id: 'dummyProductId',
          extensions: {
            interaction: {
              likes: 10,
              dislikes: 20
            }
          }
        };
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForProductWidget();
        // trigger watcher
        likedProductMock.value = 'newDummyProductId';
        expect(result.allProductLikesCount.value).toEqual(0);
        expect(result.allProductDislikesCount.value).toEqual(0);
      });
    });

    describe("currentClientForGuide", () => {
      it("should run loadLikeAndDislikeList when currentClientForGuide change", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        currentClientForGuideMock.value = '111';
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForGuide();
        currentClientForGuideMock.value = '222';
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).toHaveBeenCalled();
      });
      it("should reset _storeLikeList & _storeDislikeList when currentClientForGuide invalid", async () => {
        currentClientForGuideMock.value = '111';
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForGuide();
        currentClientForGuideMock.value = '';
        expect(result.likedCount.value).toEqual(0);
        expect(result.dislikedCount.value).toEqual(0);
      });
    });

    describe("lastClientChangedLikes", () => {
      it("should return when lastClientChangedLikes invalid", async () => {
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        const res = await result.initLikeListForGuide();
        lastClientChangedLikesMock.value = '';
        expect(res).toBeUndefined();
        expect(refreshCustomerDataMock).not.toHaveBeenCalled();
      });

      it("should call loadLikeAndDislikeList when lastClientChangedLikes equal with currentClientForGuide", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForGuide();
        currentClientForGuideMock.value = '123';
        lastClientChangedLikesMock.value = '123';
        await new Promise((r) => setTimeout(r, 500));
        expect(refreshCustomerDataMock).toHaveBeenCalled();
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).toHaveBeenCalled();
      });
    });

    describe("lastGuideChangedLikes - from initLikeListForGuide", () => {
      it("should return when lastGuideChangedLikes invalid", async () => {
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        const res = await result.initLikeListForGuide();
        lastGuideChangedLikesMock.value = '';
        expect(res).toBeUndefined();
        expect(refreshCustomerDataMock).not.toHaveBeenCalled();
      });

      it("should call loadLikeAndDislikeList when lastGuideChangedLikes equal with currentClientForGuide", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForGuide();
        currentClientForGuideMock.value = '123';
        lastGuideChangedLikesMock.value = '123';
        await new Promise((r) => setTimeout(r, 500));
        expect(refreshCustomerDataMock).toHaveBeenCalled();
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).toHaveBeenCalled();
      });
    });

    describe("lastGuideChangedLikes - from initLikeListForClient", () => {
      it("should return when lastGuideChangedLikes invalid", async () => {
        lastGuideChangedLikesMock.value = '1';
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(false),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForClient();
        lastGuideChangedLikesMock.value = '';
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref(null),
        } as any));
        (mockedUseState as any).mockImplementation(() => ({
          currentClientForGuide: ref('123'),
        } as any));
        await new Promise((r) => setTimeout(r, 500));
        expect(getLikedProductsForAttendeeMock).not.toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).not.toHaveBeenCalled();
      });

      it("should call loadLikeAndDislikeList when appointment.value.attendeeId equal with lastGuideChangedLikes", async () => {
        (mockedUseAppointment as any).mockImplementation(() => ({
          isGuide: ref(true),
          isClient: ref(false),
          appointment: ref({
            attendeeId: '123',
          }),
        } as any));
        [result, app] = mockLoadComposableInApp(() => useLikeList(product.value));
        await result.initLikeListForClient();
        lastGuideChangedLikesMock.value = '123';
        await new Promise((r) => setTimeout(r, 500));
        expect(getLikedProductsForAttendeeMock).toHaveBeenCalled();
        expect(getDislikedProductsForAttendeeMock).toHaveBeenCalled();
      });
    });
  });

  describe("computed", () => {
    describe("productLiked", () => {
      it("should be true when productId existed in storeLikeList", () => {
        _storeLikeList.value = [{id: 'dummyProductId'}] as any;
        product.value = {
          id: 'dummyProductId',
        };
        const {productLiked} = useLikeList(product.value);
        expect(productLiked.value).toEqual(true);
      });

      it("should be false when productId not existed in storeLikeList", () => {
        _storeLikeList.value = [{id: 'anotherDummyProductId'}] as any;
        product.value = {
          id: 'dummyProductId',
        };
        const {productLiked} = useLikeList(product.value);
        expect(productLiked.value).toEqual(false);
      });
    });

    describe("productDisliked", () => {
      it("should be true when productId existed in _storeDislikeList", () => {
        _storeDislikeList.value = [{id: 'dummyProductId'}] as any;
        product.value = {
          id: 'dummyProductId',
        };
        const {productDisliked} = useLikeList(product.value);
        expect(productDisliked.value).toEqual(true);
      });

      it("should be false when productId not existed in _storeDislikeList", () => {
        _storeDislikeList.value = [{id: 'anotherDummyProductId'}] as any;
        product.value = {
          id: 'dummyProductId',
        };
        const {productDisliked} = useLikeList(product.value);
        expect(productDisliked.value).toEqual(false);
      });
    });

    describe("productOnGuideList", () => {
      it("should be true when productId existed in _storeGuideLikeList", () => {
        _storeGuideLikeList.value = [{id: 'dummyProductId'}] as any;
        product.value = {
          id: 'dummyProductId',
        };
        const {productOnGuideList} = useLikeList(product.value);
        expect(productOnGuideList.value).toEqual(true);
      });

      it("should be false when productId not existed in _storeGuideLikeList", () => {
        _storeGuideLikeList.value = [{id: 'anotherDummyProductId'}] as any;
        product.value = {
          id: 'dummyProductId',
        };
        const {productOnGuideList} = useLikeList(product.value);
        expect(productOnGuideList.value).toEqual(false);
      });
    });

    describe("showLikeListPage", () => {
      it("should be true when the dynamicPage have type 'likeList' && opened", () => {
        dynamicPageMock.value = {
          type: 'likeList',
          opened: true
        }
        const {showLikeListPage} = useLikeList(product.value);
        expect(showLikeListPage.value).toEqual(true);
      });

      it("should be false when the dynamicPage not existed", () => {
        dynamicPageMock.value = null;
        const {showLikeListPage} = useLikeList(product.value);
        expect(showLikeListPage.value).toEqual(false);
      });

      it("should be false when the dynamicPage don't have type 'likeList'", () => {
        dynamicPageMock.value = {
          type: 'anotherType',
          opened: true
        }
        const {showLikeListPage} = useLikeList(product.value);
        expect(showLikeListPage.value).toEqual(false);
      });

      it("should be false when the dynamicPage didn't open", () => {
        dynamicPageMock.value = {
          type: 'likeList',
          opened: false
        }
        const {showLikeListPage} = useLikeList(product.value);
        expect(showLikeListPage.value).toEqual(false);
      });
    });
  });

});
