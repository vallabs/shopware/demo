import type {DynamicPageResponse} from "../../composables/useDynamicPage";

export interface PresentationStateModel {
  stateForAll: PresentationStateForAll
  stateForGuides: PresentationStateForGuides
  stateForClients: PresentationStateForClients
  stateForMe: PresentationStateForMe
}

export interface PresentationStateForAll {
  accessibleFrom: string,
  accessibleTo: string,
  allowUserActionsForGuide: boolean,
  appointmentMode: string,
  attendeeRestrictionType: string,
  broadcastMode: boolean,
  currentGuideProductId: string,
  currentPageId: string,
  currentDynamicPage: DynamicPageResponse,
  currentSectionId: string,
  currentSlideAlias: number,
  started: boolean,
  startedAt: string,
  running: boolean,
  ended: boolean,
  endedAt: string,
  lastActiveGuideSection: string,
  productDetailDefaultPageId: string,
  productListingDefaultPageId: string,
  quickviewPageId: string,
  videoAudioSettings: VideoAudioSettingsType,
  videoRoomUrl: string,
  extensions: any,
}

export enum VideoAudioSettingsType {
  BOTH = 'both',
  AUDIO_ONLY = 'audio-only',
  NONE = 'none'
}

export interface PresentationStateForGuides {
  clients: object,
  inactiveClients: object,
  guides: object,
  videoGuideToken: string
  extensions: any
}

export interface PresentationStateForClients {
  videoClientToken: string
  extensions: any
}

export interface PresentationStateForMe {
  attendeeName: string
  attendeeSubmittedAt: {
    date: string
    timezone: string
    timezone_type: number
  }
  guideCartPermissionsGranted: boolean
}
