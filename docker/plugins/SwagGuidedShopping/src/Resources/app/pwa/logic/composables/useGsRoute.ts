import { computed } from '@vue/composition-api';
import { getApplicationContext } from '@shopware-pwa/composables';

const COMPOSABLE_NAME = 'useGsRoute';

export const useGsRoute = () => {
  const contextName = COMPOSABLE_NAME;
  const { route, router } = getApplicationContext({ contextName });

  const params = route.params;
  const query = route.query;
  // router.push({ query: {} });

  return {
    appointmentId: computed(() => params?.appointmentId),
    il: computed(() => query?.il),
    slide: computed(() => parseInt(query?.slide ?? 0)),
    slideGroup: computed(() => parseInt(query?.slideGroup ?? 0))
  }

}
