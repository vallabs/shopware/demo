import {getApplicationContext} from '@shopware-pwa/composables';
import {InteractionService} from '../api-client';
import {useErrorHandler} from "./useErrorHandler";
import type {DynamicPageResponse, DynamicProductPage} from "./useDynamicPage";
import {useAppointment} from './useAppointment';
import {DynamicProductListingPage} from "./useDynamicPage";

const COMPOSABLE_NAME = 'useInteractions';

export const useInteractions = () => {
  const contextName = COMPOSABLE_NAME;
  const {apiInstance} = getApplicationContext({contextName});
  const {handleError} = useErrorHandler();
  const {isSelfService, isClient} = useAppointment();

  type PageInteraction = {
    payload: {
      pageId: string,
      sectionId: string,
      slideAlias: number
    }
  }

  type DynamicPageInteraction = {
    payload: DynamicPageResponse
  }

  type DynamicProductPageInteraction = {
    payload: DynamicProductPage
  }

  type ProductInteraction = {
    payload: {
      productId: string,
    }
  }

  type PageViewedInteraction = PageInteraction & {
    name: 'page.viewed'
  }

  type ProductViewedInteraction = ProductInteraction & {
    name: 'product.viewed'
  }

  type LikeListProductLikedInteraction = ProductInteraction & {
    name: 'attendee.product.collection.liked'
  }

  type LikeListProductDislikedInteraction = ProductInteraction & {
    name: 'attendee.product.collection.disliked'
  }

  type LikeListProductRemovedInteraction = ProductInteraction & {
    name: 'attendee.product.collection.removed'
  }

  type DynamicPageOpenedInteraction = DynamicPageInteraction & {
    name: 'dynamicPage.opened'
  }

  type DynamicProductPageOpenedInteraction = DynamicProductPageInteraction & {
    name: 'dynamicProductPage.opened'
  }

  type DynamicProductListingPageLoadedMoreInteraction = DynamicProductListingPage & {
    name: 'dynamicProductListingPage.loadedMore'
  }

  type DynamicPageClosedInteraction = {
    name: 'dynamicPage.closed'
  }

  type QuickViewOpened = ProductInteraction & {
    name: 'quickview.opened'
  }

  type QuickViewClosed = ProductInteraction & {
    name: 'quickview.closed'
  }

  type KeepAliveInteraction = {
    name: 'keep.alive'
  }

  type BroadcastModeToggledInteraction = {
    name: 'broadcastMode.toggled',
    payload: {
      active: boolean,
    }
  }

  type GuideHoveredInteraction = {
      name: 'guide.hovered',
      payload: {
          hoveredElementId: string,
      }
  }

  type Interactions =
    PageViewedInteraction |
    ProductViewedInteraction |
    LikeListProductLikedInteraction |
    LikeListProductDislikedInteraction |
    LikeListProductRemovedInteraction |
    DynamicPageOpenedInteraction |
    DynamicProductPageOpenedInteraction |
    DynamicProductListingPageLoadedMoreInteraction |
    DynamicPageClosedInteraction |
    QuickViewOpened |
    QuickViewClosed |
    KeepAliveInteraction |
    BroadcastModeToggledInteraction |
    GuideHoveredInteraction;

  /**
   * usage:
   *    import {useInteractions} from './useInteractions';
   *    ...
   *    const {publishInteraction} = useInteractions();
   *    publishInteraction({
   *      name: 'my.interaction',
   *      payload: {
   *        some: 'data',
   *      }
   *    });
   *
   * @param interaction
   */
  async function publishInteraction(interaction: Interactions) {
    try {
      return await InteractionService.send(interaction, apiInstance);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'publishInteraction',
      });
    }
  }

  /**
   * only publishes interaction if user is a client
   *
   * usage:
   *    import {useInteractions} from './useInteractions';
   *    ...
   *    const {publishClientInteraction} = useInteractions();
   *    publishClientInteraction({
   *      name: 'my.interaction',
   *      payload: {
   *        some: 'data',
   *      }
   *    });
   *
   * @param interaction
   */
  async function publishClientInteraction(interaction: Interactions) {
    if (isSelfService.value) return;
    if (!isClient.value) return;
    return publishInteraction(interaction);
  }

  return {
    publishInteraction,
    publishClientInteraction
  }
};
