import { ref } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { useScrollToElement } from "../useScrollToElement";

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

import {useHoverElement} from '../useHoverElement';
jest.mock('../useHoverElement');
const mockedUseHoverElement = useHoverElement as jest.Mocked<typeof useHoverElement>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import * as VueScrollTo from "vue-scrollto";
jest.mock("vue-scrollto");
const mockedVueScrollTo = VueScrollTo as jest.Mocked<typeof VueScrollTo>;


const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

describe("Composables - useScrollToElement", () => {
  const handleErrorMock = jest.fn();
  const rootContextMock = prepareRootContextMock();
  const lastGuideHoveredElementMock = ref();
  const lastClientHoveredElementMock = ref();
  const appointmentMock = ref();
  const mercurePublishGuideMock = jest.fn();
  const mercurePublishLocalMock = jest.fn();
  const mercurePublishClientMock = jest.fn();
  const publishHoverMock = jest.fn();
  const scrollToMock = jest.fn();


  beforeEach(() => {
    jest.resetAllMocks();
    lastGuideHoveredElementMock.value = null;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    (mockedUseMercureObserver as any).mockImplementation(() => ({
      lastGuideHoveredElement: lastGuideHoveredElementMock,
      lastClientHoveredElement: lastClientHoveredElementMock
    } as any));

    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        guide: mercurePublishGuideMock,
        local: mercurePublishLocalMock,
        client: mercurePublishClientMock,
      }
    } as any));

    (mockedUseHoverElement as any).mockImplementation(() => ({
      publishHover: publishHoverMock
    } as any));

    (mockedUseAppointment as any).mockImplementation(() => ({
      appointment: appointmentMock
    } as any));

    (mockedVueScrollTo as any).scrollTo.mockImplementation(scrollToMock);

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});
    
    document.body.innerHTML = `<div></div>`;
  });

  describe("methods", () => {
    describe("goToGuide", () => {
      it('should not run when lastGuideHoveredElement invalid', async () => {
        lastGuideHoveredElementMock.value = null;
        const { goToGuide } = useScrollToElement('elementId');
        await goToGuide();
        expect(mercurePublishClientMock).not.toHaveBeenCalled();
      })

      it('work correctly when have lastGuideHoveredElement', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        lastGuideHoveredElementMock.value = 'dummy-element';
        const { goToGuide } = useScrollToElement('elementId');
        await goToGuide();
        expect(mercurePublishClientMock).toHaveBeenCalledWith('client.hovered', 'dummy-element');
        expect(scrollToMock).toHaveBeenCalledWith('#element-dummy-element', 500, {
          container: '.sw-gs-layout',
          offset: -200,
        });
        expect(publishHoverMock).toHaveBeenCalled();
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('highlighted', 'dummy-element');
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 5000);
        jest.runAllTimers();
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('highlighted', '');
      })
    });

    describe("goToClient", () => {
      it('should not run when lastClientHoveredElement invalid', async () => {
        lastClientHoveredElementMock.value = null;
        const { goToClient } = useScrollToElement('elementId');
        await goToClient();
        expect(mercurePublishClientMock).not.toHaveBeenCalled();
      })

      it('work correctly when have lastClientHoveredElementMock', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        lastClientHoveredElementMock.value = 'dummy-element';
        const { goToClient } = useScrollToElement('elementId');
        await goToClient();
        expect(mercurePublishClientMock).toHaveBeenCalledWith('guide.hovered', 'dummy-element');
        expect(scrollToMock).toHaveBeenCalledWith('#element-dummy-element', 500, {
          container: '.sw-gs-layout',
          offset: -200,
        });
        expect(publishHoverMock).toHaveBeenCalled();
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('highlighted', 'dummy-element');
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 5000);
        jest.runAllTimers();
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('highlighted', '');
      })
    });
  
    describe("publishScrollTo", () => {
      it('should work correctly when document not found', async () => {
        const temp = document.querySelector;
        document.querySelector = 'aaaa' as any;
        const { publishScrollTo } = useScrollToElement('elementId');
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          index: null,
          attendeeId: undefined,
          slotContext: null
        });
        document.querySelector = temp;
      });

      it('should publish guide scrolledTo event without handleCmsElements', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, scrollToIsLoading } = useScrollToElement('elementId');
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          index: null,
          attendeeId: 'attendeeId',
          slotContext: null
        });
        expect(scrollToIsLoading.value).toEqual(true);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
        jest.runAllTimers();
        expect(scrollToIsLoading.value).toEqual(false);
      });

      it('should publish guide scrolledTo event with index null', async () => {
        document.body.innerHTML = `<div id="element-elementId"><div class="gallery gallery-big"><div class="glide"></div></div></div>`;
        (document.querySelector('.glide') as any).glide = {
         index: null
        };

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, scrollToIsLoading, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
         type: 'image-gallery'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          attendeeId: 'attendeeId',
          index: null,
          slotContext: {
            type: 'image-gallery'
          }
        });
      });

      it('should publish guide scrolledTo event when target null', async () => {
        document.body.innerHTML = `<div id="element-elementId"></div>`;

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
         type: 'image-gallery'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
         elementId: 'elementId',
         index: null,
         attendeeId: 'attendeeId',
         slotContext: {
           type: 'image-gallery'
         }
       });
     });

      it('should publish guide scrolledTo event with index valid & type = image-gallery & gallery-big', async () => {
         document.body.innerHTML = `<div id="element-elementId"><div class="gallery gallery-big"><div class="glide"></div></div></div>`;
         (document.querySelector('.glide') as any).glide = {
          index: 1
         };

         appointmentMock.value = {
           attendeeId: 'attendeeId'
         }
         const { publishScrollTo, setSlotContext } = useScrollToElement('elementId');
         setSlotContext({
          type: 'image-gallery'
         });
         await publishScrollTo();
         expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          index: 1,
          attendeeId: 'attendeeId',
          slotContext: {
            type: 'image-gallery'
          }
        });
      });

      it('should publish guide scrolledTo event with index valid & type = image-gallery & sf-gallery__item--selected', async () => {
        document.body.innerHTML = `<div id="element-elementId"><div class="sf-gallery__item--selected"><div class="glide"></div></div></div>`;
        (document.querySelector('.glide') as any).glide = {
         index: 1
        };

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, scrollToIsLoading, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
         type: 'image-gallery'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          index: 1,
          attendeeId: 'attendeeId',
          slotContext: {
            type: 'image-gallery'
          }
        });
      });

      it('should publish guide scrolledTo event with index valid & type = product-description-reviews', async () => {
        document.body.innerHTML = `
        <div id="element-elementId">
          <div class="sf-tabs">
            <div class="sf-tabs__title"></div>
            <div class="sf-tabs__title"></div>
            <div class="sf-tabs__title is-active"></div>
          </div>
        </div>`;

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
         type: 'product-description-reviews'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          index: 2,
          attendeeId: 'attendeeId',
          slotContext: {
            type: 'product-description-reviews'
          }
        });
      });

      it('should publish guide scrolledTo event with index valid & others type with glide closet', async () => {
        document.body.innerHTML = `
        <div class="glide__slide">
          <div id="element-elementId">
          </div>
        </div>`;

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
          type: 'not-sure'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
          elementId: 'elementId',
          index: 0,
          attendeeId: 'attendeeId',
          slotContext: {
            type: 'not-sure'
          }
        });
      });

      it('should publish guide scrolledTo event with index valid & type = image-gallery', async () => {
        document.body.innerHTML = `
          <div id="element-elementId">
            <div class="sf-gallery__item">
              <div id="1"></div>
            </div>
            <div class="sf-gallery__item sf-gallery__item--selected glide__slide"><div id="2"></div></div>
          </div>`;

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
         type: 'image-gallery'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
         elementId: 'elementId',
         index: 1,
         attendeeId: 'attendeeId',
         slotContext: {
           type: 'image-gallery'
         }
       });
      });

      it('should publish guide scrolledTo event with index valid & type = image-gallery', async () => {
        document.body.innerHTML = `
          <div id="element-elementId">
            <div class="sf-gallery__item sf-gallery__item--selected glide__slide"><div id="1"></div></div>
          </div>`;

        appointmentMock.value = {
          attendeeId: 'attendeeId'
        }
        const { publishScrollTo, setSlotContext } = useScrollToElement('elementId');
        setSlotContext({
         type: 'image-gallery'
        });
        await publishScrollTo();
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('scrolledTo', {
         elementId: 'elementId',
         index: 0,
         attendeeId: 'attendeeId',
         slotContext: {
           type: 'image-gallery'
         }
       });
      });
    });

    describe("scrollToElement", () => {
      it('should work correctly when element not contains product-carousel__product & slotContext is not cross-selling', async () => {
        document.body.innerHTML = `<div id="element-elementId"><div class="gallery gallery-big"><div class="glide"></div></div></div>`;
        const goMock = jest.fn();
        (document.querySelector('.glide') as any).glide = {
          go: goMock
        };
        const { setSlotContext, scrollToElement } = useScrollToElement('elementId');
        setSlotContext({
          type: 'image-gallery'
        });
        await scrollToElement('elementId', 1);
        expect(goMock).toHaveBeenCalledWith(`=1`);
      });

      it('should work correctly when element contains product-carousel__product & slotContext is not cross-selling', async () => {
        document.body.innerHTML = `
          <div class="sf-carousel__wrapper">
            <div id="element-elementId" class="product-carousel__product">
              <div class="glide"></div>
            </div>
          </div>`;
        const goMock = jest.fn();
        (document.querySelector('.glide') as any).glide = {
          go: goMock
        };
        const { setSlotContext, scrollToElement } = useScrollToElement('elementId');
        setSlotContext({
          type: 'image-gallery'
        });
        await scrollToElement('elementId', 1);
        expect(goMock).toHaveBeenCalledWith(`=1`);
      });

      it('should work correctly when slotContext is cross-selling', async () => {
        document.body.innerHTML = `
          <div id="element-elementId">
            <div class="sf-tabs">
              <div class="sf-tabs__title"></div>
              <div id="test" class="sf-tabs__title"></div>
              <div class="sf-tabs__title is-active"></div>
            </div>
          </div>`;
        const fakeClick = jest.fn();
        document.querySelector('#test')?.addEventListener("click", fakeClick);

        const { setSlotContext, scrollToElement } = useScrollToElement('elementId');
        setSlotContext({
          type: 'cross-selling'
        });
        await scrollToElement('elementId', 1);
        expect(fakeClick).toHaveBeenCalled();
      });

      it('should work correctly when slotContext is image-gallery', async () => {
        document.body.innerHTML = `
          <div id="element-elementId">
            <div class="sf-gallery__item"></div>
            <div id="test" class="sf-gallery__item"></div>
            <div class="sf-gallery__item"></div>
          </div>
        `;
        const fakeClick = jest.fn();
        document.querySelector('#test')?.addEventListener("click", fakeClick);

        const { setSlotContext, scrollToElement } = useScrollToElement('elementId');
        setSlotContext({
          type: 'image-gallery'
        });
        await scrollToElement('elementId', 1);
        expect(fakeClick).toHaveBeenCalled();
      });

      it('should work correctly when slotContext is other type', async () => {
        document.body.innerHTML = `
          <div>
            <div class="glide__slide">
              <div id="element-elementId"></div>
            </div>
            <div class="glide__slide" id="test"></div>
          </div>
        `;
        const fakeClick = jest.fn();
        document.querySelector('#test')?.addEventListener("click", fakeClick);

        const { setSlotContext, scrollToElement } = useScrollToElement('elementId');
        setSlotContext({
          type: 'not-sure'
        });
        await scrollToElement('elementId', 1);
        expect(fakeClick).toHaveBeenCalled();
      });

      it('should work correctly when target null', async () => {
        document.body.innerHTML = `
          <div id="element-elementId">
            <div class="sf-gallery__item"></div>
            <div id="test" class="sf-gallery__item"></div>
            <div class="sf-gallery__item"></div>
          </div>
        `;
        const fakeClick = jest.fn();
        document.querySelector('#test')?.addEventListener("click", fakeClick);

        const { setSlotContext, scrollToElement } = useScrollToElement('elementId');
        setSlotContext({
          type: 'not-sure'
        });
        await scrollToElement('elementId', 1);
        expect(fakeClick).not.toHaveBeenCalled();
      });
    });
  });

});
