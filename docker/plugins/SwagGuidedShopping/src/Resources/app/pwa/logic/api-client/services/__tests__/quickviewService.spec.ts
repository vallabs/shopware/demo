import {
  getQuickView,
} from "../quickviewService";

describe("Services - quickviewService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
      },
    };
  });

  describe("getQuickView", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {id: '111'}
        });
      });
      const res = await getQuickView({
        productId: 'dummyProductId',
        cmsPageLayoutId: 'dummyCmsPageLayoutId'
      }, apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/store-api/guided-shopping/quickview/dummyProductId/dummyCmsPageLayoutId`);
      expect(res).toEqual({
        id: '111'
      })
    })
  });

});
