import { ref, createApp } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { usePresentationLayer } from '../usePresentationLayer';

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

function mockLoadComposableInApp(composable) {
  let result;
  const app = createApp({
  setup() {
    result = composable();
    // suppress missing template warning
    return result;
  },
  });
  const wrapper = app.mount(document.createElement("div"));
  return [result, app, wrapper];
}

export type PresentationLayerPosition = 'fixed' | 'static';
export type PresentationLayerSize = 'maximized' | 'minimized';

describe("Composables - usePresentationLayer", () => {
  let result;
  let app;
  let wrapper

  const handleErrorMock = jest.fn();
  const rootContextMock = prepareRootContextMock();
  const positionStateMock = ref<PresentationLayerPosition>("fixed");
  const sizeStateMock = ref<PresentationLayerSize>("maximized");
  const isClientMock = ref<boolean>();
  const isSidebarOpenMock = ref<boolean>(false); 

  beforeEach(() => {
    jest.resetAllMocks();
    positionStateMock.value = "fixed";
    sizeStateMock.value = "maximized";
    isClientMock.value = null;
    isSidebarOpenMock.value = false;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("positionState"))
            return positionStateMock;
          if (contextName.includes("sizeState"))
            return sizeStateMock;
        },
      } as any;
    });
    mockedComposables.useUIState.mockImplementation(() => {
      return {
        isOpen: ref(false),
      } as any;
    });

    (mockedUseAppointment as any).mockImplementation(() => ({
      isClient: isClientMock,
    } as any));

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});

    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
    }
  });

  describe("methods", () => {
    describe("toggleLayerSize", () => {
      it("should return minimized when sizeState value is maximized", () => {
        sizeStateMock.value = "maximized";
        const { toggleLayerSize } = usePresentationLayer();
        toggleLayerSize();
        expect(sizeStateMock.value).toEqual("minimized");
      });

      it("should return maximized when sizeState value is minimized", () => {
        sizeStateMock.value = "minimized";
        const { toggleLayerSize } = usePresentationLayer();
        toggleLayerSize();
        expect(sizeStateMock.value).toEqual("maximized");
      });
    });

    describe("toggleLayerPosition", () => {
      it("should return fixed when positionState value is static", () => {
        positionStateMock.value = "static";
        const { toggleLayerPosition } = usePresentationLayer();
        toggleLayerPosition(positionStateMock.value);
        expect(positionStateMock.value).toEqual("fixed");
      });

      it("should return static when positionState value is fixed", () => {
        positionStateMock.value = "fixed";
        const { toggleLayerPosition } = usePresentationLayer();
        toggleLayerPosition(positionStateMock.value);
        expect(positionStateMock.value).toEqual("static");
      });
    });

    describe("mounted hook", () => {
      it("should return static when innerWidth >= 1200", () => {
        global.innerWidth = 1200;
        [result, app ] = mockLoadComposableInApp(() => usePresentationLayer());
        expect(result.positionState.value).toEqual("static");
      });

      it("should return fixed when innerWidth < 1200", () => {
        global.innerWidth = 1000;
        [result, app ] = mockLoadComposableInApp(() => usePresentationLayer());
        expect(result.positionState.value).toEqual("fixed");
      });
    });
  });

  describe("computed", () => {
    describe("sidebarClasses", () => {
      it("should sidebarClasses return value correctly", () => {
        positionStateMock.value = "fixed";
        sizeStateMock.value = "maximized";
        const { sidebarClasses } = usePresentationLayer();
        expect(sidebarClasses.value).toEqual("maximized fixed");
      });
    });

    describe("navPositionClass", () => {
      it("should return layer-maximized when isSidebarOpen valid", () => {
        isSidebarOpenMock.value = true;
        const { navPositionClass } = usePresentationLayer();
        expect(navPositionClass.value).toEqual("layer-maximized");
      });

      it("should return when isSidebarOpen invalid & isClient valid", () => {
        isSidebarOpenMock.value = false;
        isClientMock.value = true;
        const { navPositionClass } = usePresentationLayer();
        expect(navPositionClass.value).toEqual(undefined);
      });

      it("should return layer-minimized isSidebarOpen invalid & isClient invalid & sizeState.value = minimized", () => {
        sizeStateMock.value = "minimized";
        const { navPositionClass } = usePresentationLayer();
        expect(navPositionClass.value).toEqual("layer-minimized");
      });
      
      it("should return layer-maximized sSidebarOpen invalid & isClient invalid & sizeState.value != minimized", () => {
        const { navPositionClass } = usePresentationLayer();
        expect(navPositionClass.value).toEqual("layer-maximized");
      });
    });
  });
});
