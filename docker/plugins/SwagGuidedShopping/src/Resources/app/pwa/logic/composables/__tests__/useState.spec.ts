import { ref, createApp } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');

import { useState } from "../useState";

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

import {useInteractions} from '../useInteractions';
jest.mock('../useInteractions');
const mockedUseInteractions = useInteractions as jest.Mocked<typeof useInteractions>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import * as GSApiClient from '../../api-client';
import {useDynamicPage} from "../useDynamicPage";
jest.mock('../../api-client');
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

function mockLoadComposableInApp(composable) {
  let result;
  const app = createApp( {
    setup() {
      result = composable();
      // suppress missing template warning
      return result;
    }
  } );
  const wrapper = app.mount( document.createElement( 'div' ) );
  return [ result, app, wrapper ];
}

describe("Composables - useState", () => {
  let result;
  let app;
  let wrapper;
  const _storeCurrentClientForGuideMock = ref();
  const _storeCurrentClientForGuideTokenMock = ref();
  const _firstClientDefaultSetMock = ref();
  const _currentValidTokenMock = ref();
  const stateForAllChangeMock = ref();
  const stateForGuidesChangeMock = ref();
  const stateForClientsChangeMock = ref();
  const requestedTokenPermissionsMock = ref();
  const _storeStateMock = ref({
    stateForAll: null,
    stateForGuides: null,
    stateForClients: null,
    stateForMe: null,
  });
  const appointmentIdMock = ref();
  const isGuideMock  = ref();
  const isClientMock = ref();
  const isSelfServiceMock  = ref();
  const appointmentMock = ref();
  const handleErrorMock = jest.fn();
  const rootContextMock = prepareRootContextMock();
  const lastGuideHoveredElementMock = ref();
  const mercurePublishGuideMock = jest.fn();
  const mercurePublishLocalMock = jest.fn();
  const mercurePublishClientMock = jest.fn();
  const mercurePublishAllMock = jest.fn();
  const publishInteractionMock = jest.fn();
  const broadcastMock = jest.fn();
  const getAttendeeContextTokenMock = jest.fn(() => Promise.resolve(null as any));
  const getStateForGuidesMock = jest.fn(() => Promise.resolve(null as any));
  const getStateForClientsMock = jest.fn(() => Promise.resolve(null as any));

  beforeEach(() => {
    jest.resetAllMocks();
    _storeStateMock.value = {
      stateForAll: null,
      stateForGuides: null,
      stateForClients: null,
      stateForMe: null,
    };
    lastGuideHoveredElementMock.value = null;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    (mockedUseMercureObserver as any).mockImplementation(() => ({
      stateForAll: stateForAllChangeMock,
      stateForGuides: stateForGuidesChangeMock,
      stateForClients: stateForClientsChangeMock,
      lastGuideHoveredElement: lastGuideHoveredElementMock,
      requestedTokenPermissions: requestedTokenPermissionsMock,
    } as any));

    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("current-client-for-guide"))
            return _storeCurrentClientForGuideMock;
          if (contextName.includes("storeCurrentClientForGuideToken"))
            return _storeCurrentClientForGuideTokenMock;
          if (contextName.includes("firstInitializationDone"))
            return _firstClientDefaultSetMock;
          if (contextName.includes("currentClientTokenPermissions"))
            return _currentValidTokenMock;
          if (contextName.includes("state"))
            return _storeStateMock;
        },
      } as any;
    });

    (mockedUseMercure as any).mockImplementation(() => ({
      mercurePublish: {
        guide: mercurePublishGuideMock,
        local: mercurePublishLocalMock,
        client: mercurePublishClientMock,
        all: mercurePublishAllMock
      }
    } as any));

    (mockedUseInteractions as any).mockImplementation(() => ({
      publishInteraction: publishInteractionMock
    } as any));

    mockedComposables.useIntercept.mockImplementation(() => {
      return {
        broadcast: broadcastMock,
      } as any;
    });

    (mockedUseAppointment as any).mockImplementation(() => ({
      appointmentId: appointmentIdMock,
      isGuide: isGuideMock,
      isClient: isClientMock,
      isSelfService: isSelfServiceMock,
      appointment: appointmentMock
    } as any));

    mockedGSApiClient.AppointmentService = {
      getAttendeeContextToken: getAttendeeContextTokenMock
    } as any;

    mockedGSApiClient.PresentationService = {
      getStateForGuides: getStateForGuidesMock,
      getStateForClients: getStateForClientsMock
    } as any;

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("getContextTokenForCurrentClient", () => {
      it('should return current value if current token is valid without forcing refresh', async () => {
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';
        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient();
        expect(expectToken).toEqual('dummy-token');
      });

      it('should return null if current token is null and do not force refresh with empty attendeeId', async () => {
        _storeCurrentClientForGuideTokenMock.value = null;
        _storeCurrentClientForGuideMock.value = 'dummy-empty-attendee-id';

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient();

        expect(_storeCurrentClientForGuideTokenMock.value).toEqual(null);
        expect(_currentValidTokenMock.value).toEqual('tokendummy-empty-attendee-idnull');
        expect(broadcastMock).not.toHaveBeenCalled();
        expect(expectToken).toEqual(null);
      });

      it('should return null if current token is null and do not force refresh with valid attendeeId', async () => {
        _storeCurrentClientForGuideTokenMock.value = null;

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient(false, 'dummy-valid-attendee-id');

        expect(_storeCurrentClientForGuideTokenMock.value).toEqual(null);
        expect(_currentValidTokenMock.value).toEqual('tokendummy-valid-attendee-idnull');
        expect(broadcastMock).not.toHaveBeenCalled();
        expect(expectToken).toEqual(null);
      });

      it('should return null if current token is valid and force refresh with empty attendeeId', async () => {
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';
        _storeCurrentClientForGuideMock.value = 'dummy-empty-attendee-id';

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient(true);

        expect(_storeCurrentClientForGuideTokenMock.value).toEqual(null);
        expect(_currentValidTokenMock.value).toEqual('tokendummy-empty-attendee-idnull');
        expect(broadcastMock).toHaveBeenCalledWith('client-for-guide-token-changed', null);
        expect(expectToken).toEqual(null);
      });

      it('should return null if current token is valid and force refresh with valid attendeeId', async () => {
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient(true, 'dummy-valid-attendee-id');

        expect(_storeCurrentClientForGuideTokenMock.value).toEqual(null);
        expect(_currentValidTokenMock.value).toEqual('tokendummy-valid-attendee-idnull');
        expect(broadcastMock).toHaveBeenCalledWith('client-for-guide-token-changed', null);
        expect(expectToken).toEqual(null);
      });

      it('should return new token if current token is null and do not force refresh with empty attendeeId and client granted cart permissions', async () => {
        _storeCurrentClientForGuideTokenMock.value = null;
        _storeCurrentClientForGuideMock.value = 'dummy-empty-attendee-id';
        const clients = {};
        clients['dummy-empty-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
            stateForAll: null,
            stateForGuides: {
                clients: clients
            },
            stateForClients: null,
            stateForMe: null
        }
        getAttendeeContextTokenMock.mockResolvedValueOnce('dummy-new-token');

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient();

        expect(getAttendeeContextTokenMock).toHaveBeenCalledTimes(1);
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual('dummy-new-token');
        expect(_currentValidTokenMock.value).toEqual('tokendummy-empty-attendee-iddummy-new-token');
        expect(broadcastMock).toHaveBeenCalledWith('client-for-guide-token-changed', 'dummy-new-token');
        expect(expectToken).toEqual('dummy-new-token');
      });

      it('should return new token if current token is null and do not force refresh with valid attendeeId and client granted cart permissions', async () => {
        _storeCurrentClientForGuideTokenMock.value = null;
        const clients = {};
        clients['dummy-valid-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
            stateForAll: null,
            stateForGuides: {
                clients: clients
            },
            stateForClients: null,
            stateForMe: null
        }
        getAttendeeContextTokenMock.mockResolvedValueOnce('dummy-new-token');

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient(false, 'dummy-valid-attendee-id');

        expect(getAttendeeContextTokenMock).toHaveBeenCalledTimes(1);
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual('dummy-new-token');
        expect(_currentValidTokenMock.value).toEqual('tokendummy-valid-attendee-iddummy-new-token');
        expect(broadcastMock).toHaveBeenCalledWith('client-for-guide-token-changed', 'dummy-new-token');
        expect(expectToken).toEqual('dummy-new-token');
      });

      it('should return new token same to current token if current token is valid and force refresh with empty attendeeId and client granted cart permissions', async () => {
        _storeCurrentClientForGuideTokenMock.value = 'dummy-no-change-token';
        _storeCurrentClientForGuideMock.value = 'dummy-empty-attendee-id';
        const clients = {};
        clients['dummy-empty-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
            stateForAll: null,
            stateForGuides: {
                clients: clients
            },
            stateForClients: null,
            stateForMe: null
        }
        getAttendeeContextTokenMock.mockResolvedValueOnce('dummy-no-change-token');

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient(true);

        expect(getAttendeeContextTokenMock).toHaveBeenCalledTimes(1);
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual('dummy-no-change-token');
        expect(_currentValidTokenMock.value).toEqual('tokendummy-empty-attendee-iddummy-no-change-token');
        expect(broadcastMock).not.toHaveBeenCalled();
        expect(expectToken).toEqual('dummy-no-change-token');
      });

      it('should return new token same to current token if current token is valid and force refresh with valid attendeeId and client granted cart permissions', async () => {
        _storeCurrentClientForGuideTokenMock.value = 'dummy-no-change-token';
        const clients = {};
        clients['dummy-valid-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
            stateForAll: null,
            stateForGuides: {
                clients: clients
            },
            stateForClients: null,
            stateForMe: null
        }
        getAttendeeContextTokenMock.mockResolvedValueOnce('dummy-no-change-token');

        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient(true, 'dummy-valid-attendee-id');

        expect(getAttendeeContextTokenMock).toHaveBeenCalledTimes(1);
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual('dummy-no-change-token');
        expect(_currentValidTokenMock.value).toEqual('tokendummy-valid-attendee-iddummy-no-change-token');
        expect(broadcastMock).not.toHaveBeenCalled();
        expect(expectToken).toEqual('dummy-no-change-token');
      });

      it("should handle error correctly when API getAttendeeContextToken failed", async () => {
        _storeCurrentClientForGuideTokenMock.value = null;
        _storeCurrentClientForGuideMock.value = 'dummy-empty-attendee-id';
        const clients = {};
        clients['dummy-empty-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
            stateForAll: null,
            stateForGuides: {
                clients: clients
            },
            stateForClients: null,
            stateForMe: null
        }

        const res = {
            message: 'something went wrong',
            statusCode: 400
        };
        getAttendeeContextTokenMock.mockRejectedValueOnce(res);
        const { getContextTokenForCurrentClient } = useState();
        const expectToken = await getContextTokenForCurrentClient();

        expect(getAttendeeContextTokenMock).toHaveBeenCalledTimes(1);
        expect(handleErrorMock).toHaveBeenCalledWith(
            res,
            {
                context: "useState",
                method: "getContextToken",
                push: true
            }
        );
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual(null);
        expect(_currentValidTokenMock.value).toEqual('tokendummy-empty-attendee-idnull');
        expect(broadcastMock).not.toHaveBeenCalled();
        expect(expectToken).toEqual(null);
      });
    });

    describe("setCurrentClientForGuide", () => {
      it('should run correctly if there\'s requestedClientTokenPermissions is true and call API successfully', async () => {
        _storeCurrentClientForGuideTokenMock.value = null;
        const clients = {};
        clients['dummy-valid-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
          stateForAll: null,
          stateForGuides: {
              clients: clients
          },
          stateForClients: null,
          stateForMe: null
        }
        getAttendeeContextTokenMock.mockResolvedValueOnce('dummy-new-token');

        const { setCurrentClientForGuide } = useState();
        await setCurrentClientForGuide('dummy-valid-attendee-id');

        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-valid-attendee-id');
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual('dummy-new-token');
        expect(_currentValidTokenMock.value).toEqual('tokendummy-valid-attendee-iddummy-new-token');
        expect(broadcastMock).toHaveBeenCalledWith('client-for-guide-token-changed', 'dummy-new-token');
      });
    });

    describe("resetCurrentClientForGuide", () => {
      it('should run correctly', async () => {
        _storeCurrentClientForGuideTokenMock.value = 'dummy-current-token';
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        const clients = {};
        clients['dummy-current-attendee-id'] = {guideCartPermissionsGranted: true};
        _storeStateMock.value = {
            stateForAll: null,
            stateForGuides: {
                clients: clients
            },
            stateForClients: null,
            stateForMe: null
        }

        const { resetCurrentClientForGuide } = useState();
        await resetCurrentClientForGuide();

        expect(_storeCurrentClientForGuideMock.value).toEqual(null);
        expect(_storeCurrentClientForGuideTokenMock.value).toEqual(null);
        expect(_currentValidTokenMock.value).toEqual('tokennullnull');
        expect(broadcastMock).toHaveBeenCalledWith('client-for-guide-token-changed', null);
      });
    });

    describe("loadState", () => {
      it('should run correctly if GUIDE do not skip stateForAll and current client is null with expect calling API get 0 clients', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = false;
        _storeCurrentClientForGuideMock.value = null;
        _firstClientDefaultSetMock.value = false;
        const res = {
          stateForAll: {'test-all': true},
          stateForGuides: {
            clients: []
          },
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForGuidesMock.mockResolvedValueOnce(res);
        const { loadState } = useState();
        await loadState();
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': true})
        expect(_storeStateMock.value.stateForGuides).toEqual({clients: []})
        expect(_storeStateMock.value.stateForClients).toEqual(null);
        expect(_storeStateMock.value.stateForMe).toEqual(null);
        expect(_storeCurrentClientForGuideMock.value).toEqual(null);
        expect(_firstClientDefaultSetMock.value).toEqual(false);
      });

      it('should run correctly if GUIDE skip stateForAll and current client is null with expect calling API get 1 clients', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = false;
        _storeCurrentClientForGuideMock.value = null;
        _firstClientDefaultSetMock.value = false;
        const clients = {};
        clients['dummy-attendee-id'] = {guideCartPermissionsGranted: false};
        const res = {
          stateForAll: {'test-all': true},
          stateForGuides: {
            clients: clients
          },
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForGuidesMock.mockResolvedValueOnce(res);
        const { loadState } = useState();
        await loadState(true);
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual(null)
        expect(_storeStateMock.value.stateForGuides).toEqual({clients: clients})
        expect(_storeStateMock.value.stateForClients).toEqual(null);
        expect(_storeStateMock.value.stateForMe).toEqual(null);
        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-attendee-id');
        expect(_firstClientDefaultSetMock.value).toEqual(true);
      });

      it('should run correctly if GUIDE skip stateForAll and current client is already exists and not in client list with expect calling API get 1 clients', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = false;
        _storeCurrentClientForGuideMock.value = 'dummy-existed-attendee-id';
        _firstClientDefaultSetMock.value = false;
        const clients = {};
        clients['dummy-attendee-id'] = {guideCartPermissionsGranted: false};
        const res = {
          stateForAll: {'test-all': true},
          stateForGuides: {
            clients: clients
          },
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForGuidesMock.mockResolvedValueOnce(res);
        const { loadState } = useState();
        await loadState(true);
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual(null)
        expect(_storeStateMock.value.stateForGuides).toEqual({clients: clients})
        expect(_storeStateMock.value.stateForClients).toEqual(null);
        expect(_storeStateMock.value.stateForMe).toEqual(null);
        expect(_storeCurrentClientForGuideMock.value).toEqual(null);
        expect(_firstClientDefaultSetMock.value).toEqual(false);
      });

      it('should run correctly if GUIDE access SELF SERVICE, do not skip stateForAll and current client is null with expect calling API get 1 clients and hoverElementId', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = true;
        _storeCurrentClientForGuideMock.value = null;
        _firstClientDefaultSetMock.value = false;
        const clients = {};
        clients['dummy-attendee-id'] = {guideCartPermissionsGranted: false};
        const guidesRes = {
          stateForAll: {'test-all': true},
          stateForGuides: {
            clients: clients
          },
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForGuidesMock.mockResolvedValueOnce(guidesRes);
        const clientsRes = {
          stateForAll: {'test-all': false},
          stateForGuides: {'test-guides': true},
          stateForClients: {hoveredElementId: 'dummy-element-id'},
          stateForMe: {'test-me': true}
        };
        getStateForClientsMock.mockResolvedValueOnce(clientsRes);
        const { loadState } = useState();
        await loadState();
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': false})
        expect(_storeStateMock.value.stateForGuides).toEqual({clients: clients});
        expect(_storeStateMock.value.stateForClients).toEqual({hoveredElementId: 'dummy-element-id'});
        expect(_storeStateMock.value.stateForMe).toEqual({'test-me': true});
        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-attendee-id');
        expect(_firstClientDefaultSetMock.value).toEqual(true);
        expect(lastGuideHoveredElementMock.value).toEqual('dummy-element-id');
      });

      it('should run correctly if GUIDE access SELF SERVICE, skip stateForAll and current client is null with expect calling API get 1 clients and no hoverElementId', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = true;
        _storeCurrentClientForGuideMock.value = null;
        _firstClientDefaultSetMock.value = false;
        const clients = {};
        clients['dummy-attendee-id'] = {guideCartPermissionsGranted: false};
        const guidesRes = {
          stateForAll: {'test-all': true},
          stateForGuides: {
            clients: clients
          },
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForGuidesMock.mockResolvedValueOnce(guidesRes);
        const clientsRes = {
          stateForAll: {'test-all': false},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForClientsMock.mockResolvedValueOnce(clientsRes);
        const { loadState } = useState();
        await loadState(true);
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual(null)
        expect(_storeStateMock.value.stateForGuides).toEqual({clients: clients});
        expect(_storeStateMock.value.stateForClients).toEqual({'test-clients': true});
        expect(_storeStateMock.value.stateForMe).toEqual({'test-me': true});
        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-attendee-id');
        expect(_firstClientDefaultSetMock.value).toEqual(true);
        expect(lastGuideHoveredElementMock.value).toEqual(null);
      });

      it('should run correctly if CLIENT do not skip stateForAll with existed lastGuideHoveredElement expect calling API get hoverElementId', async () => {
        isGuideMock.value = false;
        isClientMock.value = true;
        isSelfServiceMock.value = false;
        lastGuideHoveredElementMock.value = 'dummy-hovered-element-id';
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _firstClientDefaultSetMock.value = false;
        const clientsRes = {
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true, hoveredElementId: 'dummy-element-id'},
          stateForMe: {'test-me': true}
        };
        getStateForClientsMock.mockResolvedValueOnce(clientsRes);
        const { loadState } = useState();
        await loadState();
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': true})
        expect(_storeStateMock.value.stateForGuides).toEqual(null);
        expect(_storeStateMock.value.stateForClients).toEqual({'test-clients': true, hoveredElementId: 'dummy-element-id'});
        expect(_storeStateMock.value.stateForMe).toEqual({'test-me': true});
        expect(lastGuideHoveredElementMock.value).toEqual('dummy-hovered-element-id');
        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-current-attendee-id');
        expect(_firstClientDefaultSetMock.value).toEqual(false);
      });

      it('should run correctly if CLIENT skip stateForAll with non-existed lastGuideHoveredElement expect calling API get hoverElementId', async () => {
        isGuideMock.value = false;
        isClientMock.value = true;
        isSelfServiceMock.value = false;
        lastGuideHoveredElementMock.value = null;
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _firstClientDefaultSetMock.value = false;
        const clientsRes = {
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true, hoveredElementId: 'dummy-element-id'},
          stateForMe: {'test-me': true}
        };
        getStateForClientsMock.mockResolvedValueOnce(clientsRes);
        const { loadState } = useState();
        await loadState(true);
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual(null)
        expect(_storeStateMock.value.stateForGuides).toEqual(null);
        expect(_storeStateMock.value.stateForClients).toEqual({'test-clients': true, hoveredElementId: 'dummy-element-id'});
        expect(_storeStateMock.value.stateForMe).toEqual({'test-me': true});
        expect(lastGuideHoveredElementMock.value).toEqual('dummy-element-id');
        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-current-attendee-id');
        expect(_firstClientDefaultSetMock.value).toEqual(false);
      });

      it('should handle error correctly if GUIDE access SELF SERVICE do not skip stateForAll, failed at calling API getStateForGuides ', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = true;
        lastGuideHoveredElementMock.value = null;
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _firstClientDefaultSetMock.value = false;
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };
        getStateForGuidesMock.mockRejectedValueOnce(res);
        const { loadState } = useState();
        await loadState();
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(getStateForClientsMock).not.toHaveBeenCalled();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useState",
            method: "loadState"
          }
        );
        expect(_storeStateMock.value.stateForAll).toEqual(null)
        expect(_storeStateMock.value.stateForGuides).toEqual(null);
        expect(_storeStateMock.value.stateForClients).toEqual(null);
        expect(_storeStateMock.value.stateForMe).toEqual(null);
        expect(lastGuideHoveredElementMock.value).toEqual(null);
        expect(_storeCurrentClientForGuideMock.value).toEqual('dummy-current-attendee-id');
        expect(_firstClientDefaultSetMock.value).toEqual(false);
      });

      it('should handle error correctly if GUIDE access SELF SERVICE do not skip stateForAll, failed at calling API getStateForClients ', async () => {
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = true;
        lastGuideHoveredElementMock.value = null;
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _firstClientDefaultSetMock.value = false;
        const clients = {};
        clients['dummy-attendee-id'] = {guideCartPermissionsGranted: false};
        const guidesRes = {
          stateForAll: {'test-all': true},
          stateForGuides: {
            clients: clients
          },
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        };
        getStateForGuidesMock.mockResolvedValueOnce(guidesRes);
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };
        getStateForClientsMock.mockRejectedValueOnce(res);
        const { loadState } = useState();
        await loadState();
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useState",
            method: "loadState"
          }
        );
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': true})
        expect(_storeStateMock.value.stateForGuides).toEqual({clients: clients});
        expect(_storeStateMock.value.stateForClients).toEqual(null);
        expect(_storeStateMock.value.stateForMe).toEqual(null);
        expect(lastGuideHoveredElementMock.value).toEqual(null);
        expect(_storeCurrentClientForGuideMock.value).toEqual(null);
        expect(_firstClientDefaultSetMock.value).toEqual(false);
      });
    });

    describe("initState", () => {
      it('should run correctly with GUIDE access NOT SELF SERVICE', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        jest.spyOn(global, 'setInterval');

        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = false;
        getStateForGuidesMock.mockResolvedValueOnce({
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        });

        const { initState } = useState();
        await initState();

        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(getStateForClientsMock).not.toHaveBeenCalled();
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': true});

        jest.advanceTimersByTime(100000);
       

        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenCalledWith(expect.any(Function));
        expect(setInterval).toHaveBeenCalledTimes(1);
        expect(setInterval).toHaveBeenCalledWith(expect.any(Function), 35000)
        expect(mercurePublishAllMock).toHaveBeenCalledTimes(2);
        expect(mercurePublishAllMock).toHaveBeenCalledWith('heartbeat', true);
        expect(getStateForGuidesMock).toHaveBeenCalledTimes(3);
        expect(getStateForClientsMock).not.toHaveBeenCalled();

        await initState();
        jest.advanceTimersByTime(100000);

        jest.clearAllTimers();
      });

      it('should run correctly with GUIDE access SELF SERVICE', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        jest.spyOn(global, 'setInterval');

        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = true;
        getStateForGuidesMock.mockResolvedValueOnce({
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        });
        getStateForClientsMock.mockResolvedValueOnce({
          stateForAll: {'test-all': false},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        });

        const { initState } = useState();
        await initState();

        expect(getStateForGuidesMock).toHaveBeenCalledTimes(1);
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': false});

        jest.advanceTimersByTime(100000);

        expect(setTimeout).not.toHaveBeenCalled();
        expect(setInterval).not.toHaveBeenCalled();

        jest.clearAllTimers();
      });

      it('should run correctly with CLIENT access NOT SELF SERVICE', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        jest.spyOn(global, 'setInterval');
        jest.spyOn(global, 'clearInterval');

        isGuideMock.value = false;
        isClientMock.value = true;
        isSelfServiceMock.value = false;
        getStateForClientsMock.mockResolvedValueOnce({
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        });
        const [result, app] = mockLoadComposableInApp(() => useState());
        await result.initState();

        expect(getStateForGuidesMock).not.toHaveBeenCalled();
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': true});

        jest.advanceTimersByTime(100000);

        expect(setTimeout).not.toHaveBeenCalled();
        expect(setInterval).toHaveBeenCalledTimes(1);
        expect(setInterval).toHaveBeenCalledWith(expect.any(Function), 35000)
        expect(publishInteractionMock).toHaveBeenCalledTimes(2);
        expect(publishInteractionMock).toHaveBeenCalledWith({"name": "keep.alive"});

        await result.initState();
        jest.advanceTimersByTime(100000);

        jest.clearAllTimers();

        app.unmount();
        expect(clearInterval).toHaveBeenCalled();
      });

      it('should run correctly with CLIENT access SELF SERVICE', async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        jest.spyOn(global, 'setInterval');

        isGuideMock.value = false;
        isClientMock.value = true;
        isSelfServiceMock.value = true;
        getStateForClientsMock.mockResolvedValueOnce({
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        });

        const { initState } = useState();
        await initState();

        expect(getStateForGuidesMock).not.toHaveBeenCalled();
        expect(getStateForClientsMock).toHaveBeenCalledTimes(1);
        expect(_storeStateMock.value.stateForAll).toEqual({'test-all': true});

        jest.advanceTimersByTime(100000);

        expect(setTimeout).not.toHaveBeenCalled();
        expect(setInterval).not.toHaveBeenCalled();

        jest.clearAllTimers();
      });
    });
  });

  describe("watchers", () => {
    describe("stateForAllChanged", () => {
      it('should work correctly', async () => {
        stateForAllChangeMock.value = {appointmentMode: 'guided'};
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = false;
        appointmentMock.value = {
          presentationGuideMode:  'guided'
        };
        [result, app] = mockLoadComposableInApp(() => useState());
        await result.initState();
    
        // trigger watcher
        stateForAllChangeMock.value = {appointmentMode: 'self'};
        await jest.advanceTimersByTime(500);
        expect(result.stateForAll.value).toEqual({ appointmentMode: 'self' });
        expect(appointmentMock.value.presentationGuideMode).toEqual('self');
      });
    });

    describe("stateForGuidesChanged", () => {
      it('should work correctly', async () => {
        stateForGuidesChangeMock.value = {
          clients: {
            example: true
          },
        };
        isGuideMock.value = true;
        isClientMock.value = false;
        isSelfServiceMock.value = false;
        appointmentMock.value = {
          presentationGuideMode:  'guided'
        };
        [result, app] = mockLoadComposableInApp(() => useState());
        await result.initState();
    
        // trigger watcher
        stateForGuidesChangeMock.value = {
          clients: {
            example: false
          },
        };
        await jest.advanceTimersByTime(500);
        expect(result.stateForGuides.value).toEqual({ 
          clients: {
            example: false
          } 
        });
      });
    });

    describe("stateForClientsChanged", () => {
      it('should work correctly', async () => {
        stateForClientsChangeMock.value = {
          videoClientToken: null
        };
        isGuideMock.value = false;
        isClientMock.value = true;
        isSelfServiceMock.value = false;
        appointmentMock.value = {
          presentationGuideMode:  'guided'
        };
        [result, app] = mockLoadComposableInApp(() => useState());
        await result.initState();
    
        // trigger watcher
        stateForClientsChangeMock.value = {
          videoClientToken: '123213'
        };
        await jest.advanceTimersByTime(500);
        expect(result.stateForClients.value).toEqual({ 
          videoClientToken: '123213'
        });
      });
    });
  })

  describe("computed", () => {
    describe("stateForAll", () => {
      it("should return stateForAll value", () => {
        const { stateForAll } = useState();
        expect(stateForAll.value).toEqual(_storeStateMock.value.stateForAll);
      });
    });

    describe("stateForGuides", () => {
      it("should return stateForGuides value", () => {
        const { stateForGuides } = useState();
        expect(stateForGuides.value).toEqual(_storeStateMock.value.stateForGuides);
      });
    });

    describe("stateForClients", () => {
      it("should return stateForClients value", () => {
        const { stateForClients } = useState();
        expect(stateForClients.value).toEqual(_storeStateMock.value.stateForClients);
      });
    });

    describe("stateForMe", () => {
      it("should return stateForMe value", () => {
        const { stateForMe } = useState();
        expect(stateForMe.value).toEqual(_storeStateMock.value.stateForMe);
      });
    });

    describe("clients", () => {
      it("should return correct client ids if stateForAll has list of clients", () => {
        const dummyClients = {};
        dummyClients['dummy-attendee-id'] = {test: true};
        _storeStateMock.value = {
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true, clients: dummyClients},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        }
        const { clients } = useState();
        expect(clients.value).toEqual(['dummy-attendee-id']);
      });

      it("should return empty client ids if stateForAll doesn't has list of clients", () => {
        _storeStateMock.value = {
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        }
        const { clients } = useState();
        expect(clients.value).toEqual([]);
      });
    });

    describe("guides", () => {
      it("should return correct guide ids if stateForAll has list of guide", () => {
        const dummyGuides = {};
        dummyGuides['dummy-guide-id'] = {test: true};
        _storeStateMock.value = {
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true, guides: dummyGuides},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        }
        const { guides } = useState();
        expect(guides.value).toEqual(['dummy-guide-id']);
      });

      it("should return empty guide ids if stateForAll doesn't has list of guides", () => {
        _storeStateMock.value = {
          stateForAll: {'test-all': true},
          stateForGuides: {'test-guides': true},
          stateForClients: {'test-clients': true},
          stateForMe: {'test-me': true}
        }
        const { guides } = useState();
        expect(guides.value).toEqual([]);
      });
    });

    describe("currentClientForGuide", () => {
      it("should return currentClientForGuide value", () => {
        const { currentClientForGuide } = useState();
        expect(currentClientForGuide.value).toEqual(_storeCurrentClientForGuideMock.value);
      });
    });

    describe("currentValidToken", () => {
      it("should return currentValidToken value", () => {
        const { currentValidToken } = useState();
        expect(currentValidToken.value).toEqual(_currentValidTokenMock.value);
      });
    });

    describe("currentClientTokenPermissions", () => {
      it("should return true if the configuration allow guide action as an user and client in stateForGuides granted the cart permissions", () => {
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';
        const clients = {};
        clients['dummy-current-attendee-id'] = {guideCartPermissionsGranted: true}
        _storeStateMock.value = {
          stateForAll: {allowUserActionsForGuide: true},
          stateForGuides: {clients},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentClientTokenPermissions } = useState();
        expect(currentClientTokenPermissions.value).toEqual(true);
      });

      it("should return false if the configuration doesn't allow guide action as an user although client in stateForGuides granted the cart permissions", () => {
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';
        const clients = {};
        clients['dummy-current-attendee-id'] = {guideCartPermissionsGranted: true}
        _storeStateMock.value = {
          stateForAll: {allowUserActionsForGuide: false},
          stateForGuides: {clients},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentClientTokenPermissions } = useState();
        expect(currentClientTokenPermissions.value).not.toEqual(true);
      });

      it("should return false if the configuration allow guide action as an user but client in stateForGuides doesn't grant the cart permissions", () => {
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';
        const clients = {};
        clients['dummy-current-attendee-id'] = {guideCartPermissionsGranted: false}
        _storeStateMock.value = {
          stateForAll: {allowUserActionsForGuide: true},
          stateForGuides: {clients},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentClientTokenPermissions } = useState();
        expect(currentClientTokenPermissions.value).not.toEqual(true);
      });

      it("should return false if the configuration allow guide action as an user and client in stateForGuides granted the cart permissions, but current attendee id is not in stateForGuides", () => {
        _storeCurrentClientForGuideMock.value = '';
        _storeCurrentClientForGuideTokenMock.value = 'dummy-token';
        const clients = {};
        clients['dummy-current-attendee-id'] = {guideCartPermissionsGranted: false}
        _storeStateMock.value = {
          stateForAll: {allowUserActionsForGuide: true},
          stateForGuides: {clients},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentClientTokenPermissions } = useState();
        expect(currentClientTokenPermissions.value).not.toEqual(true);
      });

      it("should return false if the configuration allow guide action as an user and client in stateForGuides granted the cart permissions, but current token is empty", () => {
        _storeCurrentClientForGuideMock.value = 'dummy-current-attendee-id';
        _storeCurrentClientForGuideTokenMock.value = null;
        const clients = {};
        clients['dummy-current-attendee-id'] = {guideCartPermissionsGranted: false}
        _storeStateMock.value = {
          stateForAll: {allowUserActionsForGuide: true},
          stateForGuides: {clients},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentClientTokenPermissions } = useState();
        expect(currentClientTokenPermissions.value).not.toEqual(true);
      });
    });

    describe("currentGuide", () => {
      it("should return correct currentGuide value if the stateForGuides contains appointment.value.attendeeId", () => {
        appointmentMock.value = {attendeeId: 'dummy-guide-id'};
        const guides = {};
        guides['dummy-guide-id'] = {test: true};
        _storeStateMock.value = {
          stateForAll: {},
          stateForGuides: {guides},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentGuide } = useState();
        expect(currentGuide.value).toEqual({test: true});
      });

      it("should return undefined if the stateForGuides doesn't contain appointment.value.attendeeId", () => {
        appointmentMock.value = {attendeeId: 'dummy-guide-id'};
        _storeStateMock.value = {
          stateForAll: {},
          stateForGuides: {guides: {}},
          stateForClients: {},
          stateForMe: {}
        }

        const { currentGuide } = useState();
        expect(currentGuide.value).toEqual(undefined);
      });
    });

    describe("ended", () => {
      it("should return correct value if stateForAll has ended value", () => {
        _storeStateMock.value = {
          stateForAll: {ended: true},
          stateForGuides: {},
          stateForClients: {},
          stateForMe: {}
        }
        const { ended } = useState();
        expect(ended.value).toEqual(true);
      });
      it("should return undefined if stateForAll doesn't has ended value", () => {
        _storeStateMock.value = {
          stateForAll: {},
          stateForGuides: {},
          stateForClients: {},
          stateForMe: {}
        }
        const { ended } = useState();
        expect(ended.value).toEqual(undefined);
      });
    });

    describe("started", () => {
      it("should return correct value if stateForAll has started value", () => {
        _storeStateMock.value = {
          stateForAll: {started: true},
          stateForGuides: {},
          stateForClients: {},
          stateForMe: {}
        }
        const { started } = useState();
        expect(started.value).toEqual(true);
      });
      it("should return undefined if stateForAll doesn't has started value", () => {
        _storeStateMock.value = {
          stateForAll: {},
          stateForGuides: {},
          stateForClients: {},
          stateForMe: {}
        }
        const { started } = useState();
        expect(started.value).toEqual(undefined);
      });
    });

    describe("showTokenPermissionsRequest", () => {
      it('should return true if the requestedTokenPermissions is same with appointment.attendeeId', function () {
        appointmentMock.value = {attendeeId: 'dummy-attendee-id'};
        requestedTokenPermissionsMock.value = 'dummy-attendee-id';

        const { showTokenPermissionsRequest } = useState();
        expect(showTokenPermissionsRequest.value).toEqual(true);
      });

      it('should return false if the requestedTokenPermissions is not same with appointment.attendeeId', function () {
        appointmentMock.value = {attendeeId: 'dummy-attendee-id'};
        requestedTokenPermissionsMock.value = 'dummy-diff-attendee-id';

        const { showTokenPermissionsRequest } = useState();
        expect(showTokenPermissionsRequest.value).toEqual(false);
      });

      it('should return false if the appointment.attendeeId is empty', function () {
        appointmentMock.value = {};
        requestedTokenPermissionsMock.value = 'dummy-diff-attendee-id';

        const { showTokenPermissionsRequest } = useState();
        expect(showTokenPermissionsRequest.value).toEqual(false);
      });
    })
  });
});
