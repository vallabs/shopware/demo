import { Product, Sort, Aggregations } from "@shopware-pwa/commons";

export interface InstantListingApiResult {
  elements?: Product[];
  sorting?: string;
  sortings?: Sort[];
  page?: number;
  limit?: number;
  total?: number;
  availableSortings?: Sort[];
  aggregations?: Aggregations;
  currentFilters?: {
    manufacturer?: string[];
    properties?: string[];
    price?: { min: null | number; max: null | number };
    rating?: number;
    search?: string | null;
    "shipping-free"?: boolean | null;
  };
}

export interface AllProductIdsApiResult {
  ids?: string[];
  total?: number;
}

export interface ProductStreamFilterItem {
  apiAlias: string;
  id: string;
  name: string;
  translated: {
    name: string;
  };
  apiFilter: [] | null;
}

export interface SelectedFilters {
  search?: string | undefined;
  streams?: string[] | undefined;
  term?: string | undefined;
  properties?: string[] | undefined;
  manufacturer?: string | undefined | string[];
  includes?: any;
  query?: string;
  order?: string | undefined;
  filter?: ProductStreamFilter[];
  limit?: number | string | any | undefined;
  unlimited?: boolean;
  total?: number | string | any | undefined;
  page?: number | string | undefined;
  p?: number | string | undefined;
  interaction?: boolean;
  loadVariants?: boolean;
  loadAllIds?: boolean;
}

export interface ProductStreamFilter {
  type: string;
  operator?: string;
  field?: string;
  value?: any;
  queries?: ProductStreamFilter[];
}

export interface PaginationData {
  getTotal: number | string | undefined;
  getLimit: number | string | undefined;
  getCurrentPage: number | string | undefined;
  getTotalPagesCount: number | string | undefined;
}

export interface InstantListingUpdateData {
  updated: boolean;
  currentIlIndex: number;
}

export type InstantListingType = 'instant-listing'| 'guided-product-listing';
