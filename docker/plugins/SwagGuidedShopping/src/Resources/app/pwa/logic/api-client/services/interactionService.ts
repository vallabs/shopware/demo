import {ShopwareApiInstance} from '@shopware-pwa/shopware-6-client';
import {getInteractionEndpoint} from '../endpoints';

export function send(data: object, apiInstance: ShopwareApiInstance): Promise<any> {
  return apiInstance.invoke.post(getInteractionEndpoint(), data).then((response) => {
    return response.data;
  });
}
