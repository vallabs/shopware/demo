import { computed } from '@vue/composition-api';
import { useSharedState } from '@shopware-pwa/composables';
import { useMercureObserver } from './useMercureObserver';
import { useState } from "./useState";
import { useInteractions } from "./useInteractions";
import { useMercure } from "./useMercure";
import { useAppointment } from "./useAppointment";

export type DynamicPage = {
  type: string,
  opened: boolean,
  productId?: string
  page?: number
}

export type DynamicProductPage = {
  type: 'product',
  productId: string
};

export type DynamicProductListingPage = {
  type: 'productList',
  page: number
};

export type DynamicLikeListPage = {
  type: 'likeList'
};

export type DynamicPageResponse = DynamicPage & (DynamicProductPage | DynamicLikeListPage | DynamicProductListingPage);

export const useDynamicPage = () => {
  const COMPOSABLE_NAME = 'useDynamicPage';
  const contextName = COMPOSABLE_NAME;
  const { sharedRef } = useSharedState();
  const { currentDynamicPage } = useMercureObserver();
  const { stateForAll } = useState();
  const { mercurePublish } = useMercure();
  const { publishInteraction } = useInteractions();
  const { isSelfService } = useAppointment();
  const _storeLastDynamicPage = sharedRef<DynamicPageResponse>(`${contextName}-last-dynamic-page`, null);
  const _slidePosition = sharedRef<number>(`${contextName}-slidePosition`, 0);
  const dynamicPage = computed(() => currentDynamicPage?.value || stateForAll?.value?.currentDynamicPage);
  const dynamicPageProductId = computed(() => dynamicPage?.value?.opened ? dynamicPage?.value?.productId : null );

  // open dynamic page and inform others about it
  async function openDynamicPage(page: DynamicPageResponse) {

    if (!page.opened) return;

    const handleScrollingAndNavigation = page.type !== 'productList';

    // store original listing position
    let oldPageFound = false
    if (_storeLastDynamicPage.value) {
      oldPageFound = true;
    }

    // store last dynamic page to open it again if a dynamic page was opened on a dynamic page
    if (handleScrollingAndNavigation) {
      _storeLastDynamicPage.value = dynamicPage.value;
    }

    let interactionName = null;
    if (page.type === 'product') {
      interactionName = 'dynamicProductPage.opened';
    } else if (page.type === 'productList') {
      interactionName = 'dynamicProductListingPage.opened';
    } else if (page.type === 'likeList') {
      interactionName = 'dynamicPage.opened';
    }

    if (!interactionName) return;

    if (isSelfService.value) {
      mercurePublish.local('dynamicPage.opened', page);
    } else {
      publishInteraction({
        name: interactionName,
        payload: page
      });
      mercurePublish.guide('dynamicPage.opened', page);
    }

    if (!oldPageFound) {
      _slidePosition.value = document.getElementsByClassName('sw-gs-layout').item(0).scrollTop;
    }

    if (handleScrollingAndNavigation) {
      resetSlidePosition(true);
    }
  }

  // close dynamic page and inform others about it
  async function closeDynamicPage(givenPage: DynamicPageResponse) {
    const page = givenPage;
    page.opened = false;

    if (isSelfService.value) {
      mercurePublish.local('dynamicPage.closed', page)
    } else {
      publishInteraction({
        name: 'dynamicPage.closed'
      });
      mercurePublish.guide('dynamicPage.closed', page)
    }

    //TODO: clean product infos

    if (_storeLastDynamicPage.value) {
      await openDynamicPage(_storeLastDynamicPage.value);

      // reset last dynamic page
      _storeLastDynamicPage.value = null;
    }

    resetSlidePosition()
  }

  function resetSlidePosition(toTop = false) {
    let position = _slidePosition.value;
    if (toTop) position = 0;

    try {
      document.getElementsByClassName('sw-gs-layout').item(0).scrollTo(0, position);
    } catch (e) {
    }
  }

  return {
    slidePosition: computed(() => _slidePosition.value),
    resetSlidePosition,
    dynamicPage,
    dynamicPageProductId,
    openDynamicPage,
    closeDynamicPage
  };
};
