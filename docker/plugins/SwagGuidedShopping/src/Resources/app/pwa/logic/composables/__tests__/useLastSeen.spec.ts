import { ref } from "vue-demi";
import { Product } from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import { useErrorHandler } from "../useErrorHandler";
jest.mock("../useErrorHandler");

import { useLastSeen } from '../useLastSeen';

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<
  typeof useErrorHandler
>;

describe("Composables - useLastSeen", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const loadLastSeenMock = jest.fn();
  const getLastSeenProductsMock = jest.fn();
  const _storeLastSeenMock = ref<Product[]>([]);
  const isLastSeenLoadingMock = ref<boolean>(true);

  beforeEach(() => {
    jest.resetAllMocks();
    _storeLastSeenMock.value = [];
    isLastSeenLoadingMock.value = true;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("lastSeenLoading"))
            return isLastSeenLoadingMock;
          if (contextName.includes("lastSeen"))
            return _storeLastSeenMock;
        }
      } as any
    });

    (mockedUseErrorHandler as any).mockImplementation(
      () =>
        ({
        handleError: handleErrorMock,
      } as any)
    );

    mockedGSApiClient.LastSeenService = {
      getLastSeenProducts: getLastSeenProductsMock,
      loadLastSeen: loadLastSeenMock,
    } as any;

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("loadLastSeen", () => {
      it("should return correct array value for loadLastSeen when API called successfully", async () => {
        mockedGSApiClient.LastSeenService.getLastSeenProducts = () => new Promise((resolve) => resolve({
          elements: [{id:1}]
        } as any));
        const { loadLastSeen } = useLastSeen();
        await loadLastSeen();
        expect(_storeLastSeenMock.value).toEqual([{id:1}]);
      });

      it("should return empty array", async () => {
        mockedGSApiClient.LastSeenService.getLastSeenProducts = () => new Promise((resolve) => resolve(null));
        const { loadLastSeen } = useLastSeen();
        await loadLastSeen();
        expect(_storeLastSeenMock.value).toEqual([]);
      });

      it("should handle error correctly when API loadLastSeen failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400,
        }
        getLastSeenProductsMock.mockRejectedValueOnce(res);
        const { loadLastSeen, isLastSeenLoading } = useLastSeen();
        await loadLastSeen();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useLastSeen",
            method: "loadLastSeen"
          }
        );
        expect(isLastSeenLoading.value).toEqual(false);
      });
    });
  });

  describe("computed", () => {
    describe("lastSeen", () => {
      it("should return _storeLastSeen value", async () => {
        const { loadLastSeen, lastSeen } = useLastSeen();
        await loadLastSeen();
        expect(lastSeen.value).toEqual([]);
      });
    });
  });
});
