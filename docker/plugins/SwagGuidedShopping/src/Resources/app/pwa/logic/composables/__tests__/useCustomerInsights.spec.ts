import { ref } from "vue-demi";

import { SearchFilterType } from "@shopware-pwa/commons";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useAppointment} from '../useAppointment';
jest.mock('../useAppointment');
const mockedUseAppointment = useAppointment as jest.Mocked<typeof useAppointment>;

import {useAdminApiInstance} from '../useAdminApiInstance';
jest.mock('../useAdminApiInstance');
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<typeof useAdminApiInstance>;

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { useCustomerInsights } from "../widgets/useCustomerInsights";
  
import { prepareRootContextMock } from "./contextRunner";

import {
  CustomerInsightsAttendeeData,
  CustomerInsightsCustomerData,
  CustomerInsightsYearOrder,
} from "../../interfaces";

describe("Composables - useCustomerInsights", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const getCustomerDataMock = jest.fn();
  const getAttendeeDataMock = jest.fn();
  const getOrderCustomerMock = jest.fn();

  const _storeAttendeeDataMock = ref<CustomerInsightsAttendeeData>(null);
  const _storeCurrentCustomerDataMock = ref<CustomerInsightsCustomerData>(null);
  const _lastYearOrdersMock = ref<CustomerInsightsYearOrder[]>([]);
  const _currentYearOrdersMock = ref<CustomerInsightsYearOrder[]>([]);
  const adminApiInstanceMock = ref(null);
  const appointmentMock = ref(null);

  beforeEach(() => {
    jest.resetAllMocks();
    _storeAttendeeDataMock.value = null;
    _storeCurrentCustomerDataMock.value = null;
    appointmentMock.value = null;
    _lastYearOrdersMock.value = [];
    _currentYearOrdersMock.value = [];

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("attendeeData"))
            return _storeAttendeeDataMock;
          else if (contextName.includes("customerData"))
            return _storeCurrentCustomerDataMock;
          else if (contextName.includes("lastYearOrders"))
            return _lastYearOrdersMock;
          else if (contextName.includes("currentYearOrders"))
            return _currentYearOrdersMock;
        },
      } as any;
    });

    (mockedUseAppointment as any).mockImplementation(() => ({
      appointment: appointmentMock,
    } as any));

    (mockedUseAdminApiInstance as any).mockImplementation(() => ({
      adminApiInstance: adminApiInstanceMock,
    } as any));

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedGSApiClient.CustomerService = {
      getCustomerData: getCustomerDataMock,
      getAttendeeData: getAttendeeDataMock
    } as any;

    mockedGSApiClient.OrderService = {
      getOrderCustomer: getOrderCustomerMock
    } as any;

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("loadCustomerData", () => {
      it("should return when attendeeId or isOpen params is invalid", async () => {
        const attendeeId = "";
        const isOpen = true;
        const { loadCustomerData } = useCustomerInsights();
        await loadCustomerData(attendeeId, isOpen);
        expect(getCustomerDataMock).not.toHaveBeenCalled();
      });

      it("should return when attendeeId or isOpen params is invalid", async () => {
        const attendeeId = "dummyAttendeeId";
        const isOpen = false;
        const { loadCustomerData } = useCustomerInsights();
        await loadCustomerData(attendeeId, isOpen);
        expect(getCustomerDataMock).not.toHaveBeenCalled();
      });

      it("should work correct when params are valid", async () => {
        jest
          .useFakeTimers()
          .setSystemTime(new Date('2023-01-01'));

        getCustomerDataMock.mockResolvedValueOnce({
          apiAlias: "dummnyapiAlias",
          appointmentId: "dummyAppointmentId",
          attendeeName: "dummyAttendeeName",
          createdAt: "dummyCreatedAt",
          customer: {},
          customerId: "dummyCustomerId",
          extensions: [],
          guideCartPermissionsGranted: false,
          id: "dummyId",
          joinedAt: "dummyJoinedAt",
          productCollections: [],
          type: "dummyType",
          updatedAt: "dummyUpdatedAt",
          videoUserId: "dummyVideoUserId",
        });

        getOrderCustomerMock.mockResolvedValueOnce([
          {
            apiAlias: "orderCustomer",
            amountNet: 200,
            amountTotal: 200,
            lineItems: [],
            order: {
              orderDate: "2023-01-01",
            }
          },
          {
            apiAlias: "orderCustomer",
            amountNet: 100,
            amountTotal: 100,
            lineItems: [],
            order: {
              orderDate: "2022-01-01",
            }
          },
        ]);

        const attendeeId = "dummyAttendeeId";
        const isOpen = true;
        const { loadCustomerData, customerData } = useCustomerInsights();
        await loadCustomerData(attendeeId, isOpen);
        expect(getCustomerDataMock).toHaveBeenCalledWith({
          associations: {
            productCollections: {},
            customer: {
              associations: {
                defaultBillingAddress: {
                  associations: {
                    country: {},
                  },
                },
              },
            },
          },
          includes: {
            customer: ["email", "defaultBillingAddress"],
          },
          filter: [
            {
              field: "id",
              type: "equals",
              value: "dummyAttendeeId",
            }
          ],
        }, adminApiInstanceMock);

        expect(getOrderCustomerMock).toHaveBeenCalledWith({
          includes: {
            order: ["amountNet", "amountTotal", "lineItems", "orderDate"],
            order_customer: ["order"],
          },
          associations: {
            order: {
              associations: {
                lineItems: {},
              },
            },
          },
          filter: [
            {
              type: SearchFilterType.MULTI,
              operator: "and",
              queries: [
                {
                  type: SearchFilterType.EQUALS,
                  field: "order.stateMachineState.technicalName",
                  value: "completed",
                },
                {
                  type: SearchFilterType.EQUALS,
                  field: "customerId",
                  value: "dummyCustomerId",
                },
                {
                  type: SearchFilterType.RANGE,
                  field: "order.orderDate",
                  parameters: {
                    gte: "2022-01-01",
                    lte: "2023-12-31",
                  },
                },
              ],
            },
          ],
        }, adminApiInstanceMock);
        expect(customerData.value).toEqual({
          apiAlias: "dummnyapiAlias",
          appointmentId: "dummyAppointmentId",
          attendeeName: "dummyAttendeeName",
          createdAt: "dummyCreatedAt",
          customer: {},
          customerId: "dummyCustomerId",
          extensions: [],
          guideCartPermissionsGranted: false,
          id: "dummyId",
          joinedAt: "dummyJoinedAt",
          productCollections: [],
          type: "dummyType",
          updatedAt: "dummyUpdatedAt",
          videoUserId: "dummyVideoUserId",
        });
        expect(_currentYearOrdersMock.value).toEqual([{
          orderDate: "2023-01-01",
        }]);
        expect(_lastYearOrdersMock.value).toEqual([{
          orderDate: "2022-01-01",
        }]);
      });

      it("should handle error correctly when API getCustomerData failed", async () => {
        const attendeeId = "dummyAttendeeId";
        const isOpen = true;

        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        getCustomerDataMock.mockRejectedValueOnce(res);

        const { loadCustomerData } = useCustomerInsights();
        await loadCustomerData(attendeeId, isOpen);
        
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useCustomerInsights",
            method: "loadCustomerData"
          }
        );
      });
    });

    describe("loadAttendeeData", () => {
      it("should return when appointment.value.id is false", async () => {
        appointmentMock.value = {
          id: '',
        }
        const { loadAttendeeData } = useCustomerInsights();
        await loadAttendeeData();
        expect(getAttendeeDataMock).not.toHaveBeenCalled();
      });

      it("should work correctly when appointment.value.id is valid", async () => {
        appointmentMock.value = {
          id: '123',
        }

        getAttendeeDataMock.mockResolvedValueOnce({
          attendees: {
            cartSum: "100",
            extensions: [],
            id: "1",
            productCount: 2
          },
          currencyId: "USD",
          currencySymbol: "$",
          extensions: [],
        });
        const { loadAttendeeData, attendeeData, isLoading } = useCustomerInsights();
        await loadAttendeeData();
        expect(attendeeData.value).toEqual({
          attendees: {
            cartSum: "100",
            extensions: [],
            id: "1",
            productCount: 2
          },
          currencyId: "USD",
          currencySymbol: "$",
          extensions: [],
        });
        expect(isLoading.value).toEqual(false);
      });

      it("should handle error correctly when API getAttendeeData failed", async () => {
        appointmentMock.value = {
          id: '123',
        };

        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        getAttendeeDataMock.mockRejectedValueOnce(res);
        const { loadAttendeeData } = useCustomerInsights();
        await loadAttendeeData();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useCustomerInsights",
            method: "loadAttendeeData"
          }
        );
      });
    });

    describe("refreshCustomerData", () => {
      it("should return when _storeCurrentCustomerData.value invalid", async () => {
        _storeCurrentCustomerDataMock.value = null;
        const attendeeId = "";
        const { refreshCustomerData } = useCustomerInsights();
        await refreshCustomerData(attendeeId);

        expect(getCustomerDataMock).not.toHaveBeenCalled();
      });

      it("should return when _storeCurrentCustomerData.value invalid", async () => {
        _storeCurrentCustomerDataMock.value = {
          id: "dummyCustomerId",
        } as any;
        const attendeeId = "dummyAttendeeId";
        const { refreshCustomerData } = useCustomerInsights();
        await refreshCustomerData(attendeeId);

        expect(getCustomerDataMock).not.toHaveBeenCalled();
      });

      it("should work correct when params is true", async () => {
        jest
          .useFakeTimers()
          .setSystemTime(new Date('2023-01-01'));
        getCustomerDataMock.mockResolvedValueOnce({
          apiAlias: "dummnyapiAlias",
          appointmentId: "dummyAppointmentId",
          attendeeName: "dummyAttendeeName",
          createdAt: "dummyCreatedAt",
          customer: {},
          customerId: "dummyCustomerId",
          extensions: [],
          guideCartPermissionsGranted: false,
          id: "dummyId",
          joinedAt: "dummyJoinedAt",
          productCollections: [],
          type: "dummyType",
          updatedAt: "dummyUpdatedAt",
          videoUserId: "dummyVideoUserId",
        });

        getOrderCustomerMock.mockResolvedValueOnce([{
          apiAlias: "orderCustomer",
          amountNet: 100,
          amountTotal: 100,
          lineItems: [],
          order: {
            orderDate: "2023-01-01",
          }
        }]);

        _storeCurrentCustomerDataMock.value = {
          id: "dummyAttendeeId",
        } as any;

        const { refreshCustomerData } = useCustomerInsights();
        await refreshCustomerData("dummyAttendeeId");

        expect(getCustomerDataMock).toHaveBeenCalledWith({
          associations: {
            productCollections: {},
            customer: {
              associations: {
                defaultBillingAddress: {
                  associations: {
                    country: {},
                  },
                },
              },
            },
          },
          includes: {
            customer: ["email", "defaultBillingAddress"],
          },
          filter: [
            {
              field: "id",
              type: "equals",
              value: "dummyAttendeeId",
            }
          ],
        }, adminApiInstanceMock);

        expect(getOrderCustomerMock).toHaveBeenCalledWith({
          includes: {
            order: ["amountNet", "amountTotal", "lineItems", "orderDate"],
            order_customer: ["order"],
          },
          associations: {
            order: {
              associations: {
                lineItems: {},
              },
            },
          },
          filter: [
            {
              type: SearchFilterType.MULTI,
              operator: "and",
              queries: [
                {
                  type: SearchFilterType.EQUALS,
                  field: "order.stateMachineState.technicalName",
                  value: "completed",
                },
                {
                  type: SearchFilterType.EQUALS,
                  field: "customerId",
                  value: "dummyCustomerId",
                },
                {
                  type: SearchFilterType.RANGE,
                  field: "order.orderDate",
                  parameters: {
                    gte: "2022-01-01",
                    lte: "2023-12-31",
                  },
                },
              ],
            },
          ],
        }, adminApiInstanceMock);

        expect(_currentYearOrdersMock.value).toEqual([{
          orderDate: "2023-01-01",
        }]);
        expect(_lastYearOrdersMock.value).toEqual([]);
      });

      it("should handle error correctly when API getCustomerData failed", async () => {
        const attendeeId = 'dummyId';
        const isOpen = true;

        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        getCustomerDataMock.mockRejectedValueOnce(res);

        const { loadCustomerData } = useCustomerInsights();
        await loadCustomerData(attendeeId, isOpen);
        
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useCustomerInsights",
            method: "loadCustomerData"
          }
        );
      });
    });
  });

  describe("computed", () => {
    describe("customerLikesCount", () => {
      it("should return correctly data", () => {
        _storeCurrentCustomerDataMock.value = {
          productCollections: [
            {
              alias: "liked",
            },
            {
              alias: "liked",
            },
            {
              alias: "disliked",
            }
          ]
        } as any;
        const { customerLikesCount } = useCustomerInsights();
        expect(customerLikesCount.value).toEqual(2);
      });
    });

    describe("customerDislikesCount", () => {
      it("should return correctly data", () => {
        _storeCurrentCustomerDataMock.value = {
          productCollections: [
            {
              alias: "disliked",
            },
            {
              alias: "disliked",
            },
            {
              alias: "liked",
            }
          ]
        } as any;
        const { customerDislikesCount } = useCustomerInsights();
        expect(customerDislikesCount.value).toEqual(2);
      });
    });

    describe("lastOrdersItemCount", () => {
      it("should return correctly data", () => {
        _lastYearOrdersMock.value = [
          {
            lineItems: [
              {
                quantity: 1,
              },
              {
                quantity: 2,
              }
            ],
          },
          {
            lineItems: [
              {
                quantity: 1,
              },
              {
                quantity: 2,
              }
            ],
          }
        ] as any
        const { lastOrdersItemCount } = useCustomerInsights();
        expect(lastOrdersItemCount.value).toEqual(6);
      });
    });

    describe("currentOrdersItemCount", () => {
      it("should return correctly data", () => {
        _currentYearOrdersMock.value = [
          {
            lineItems: [
              {
                quantity: 1,
              },
              {
                quantity: 1,
              }
            ],
          },
          {
            lineItems: [
              {
                quantity: 1,
              }
            ],
          }
        ] as any
        const { currentOrdersItemCount } = useCustomerInsights();
        expect(currentOrdersItemCount.value).toEqual(3);
      });
    });

    describe("lastOrdersTotalPrice", () => {
      it("should return correctly data", () => {
        _lastYearOrdersMock.value = [
          {
            amountNet: 100
          },
          {
            amountNet: 200
          }
        ] as any
        const { lastOrdersTotalPrice } = useCustomerInsights();
        expect(lastOrdersTotalPrice.value).toEqual(300);
      });
    });

    describe("currentOrdersTotalPrice", () => {
      it("should return correctly data", () => {
        _currentYearOrdersMock.value = [
          {
            amountNet: 100
          },
          {
            amountNet: 150
          }
        ] as any
        const { currentOrdersTotalPrice } = useCustomerInsights();
        expect(currentOrdersTotalPrice.value).toEqual(250);
      });
    });

  });
});
