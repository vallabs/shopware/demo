import {getOrderLineItemEndpoint, getOrderCustomerEndpoint} from '../endpoints';
import {ShopwareAdminApiInstance, OrderLineItem, OrderCustomer} from "../../interfaces";
import {ShopwareSearchParams} from "@shopware-pwa/commons/interfaces/search/SearchCriteria";

export async function getOrderLineItems(
    searchCriteria: ShopwareSearchParams,
    adminApiInstance: ShopwareAdminApiInstance
): Promise<OrderLineItem[]> {
    const resp = await adminApiInstance.invoke.post(
        getOrderLineItemEndpoint(),
        searchCriteria
    );
    return resp.data.data;
}

export async function getOrderCustomer(
    searchCriteria: ShopwareSearchParams,
    adminApiInstance: ShopwareAdminApiInstance
): Promise<OrderCustomer[]> {
    const resp = await adminApiInstance.invoke.post(
        getOrderCustomerEndpoint(),
        searchCriteria
    );

    return resp.data.data;
}
