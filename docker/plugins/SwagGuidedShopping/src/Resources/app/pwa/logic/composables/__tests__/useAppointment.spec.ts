import { ref } from "vue-demi";
import flushPromises from 'flush-promises';

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import { prepareRootContextMock } from "./contextRunner";

import {useAdminApiInstance} from '../useAdminApiInstance';
jest.mock('../useAdminApiInstance');
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<typeof useAdminApiInstance>;

import {useErrorHandler} from "../useErrorHandler";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

const consoleErrorSpy = jest.spyOn(console, "error");

import * as GSApiClient from '../../api-client';
jest.mock('../../api-client');
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

import { useAppointment } from "../useAppointment";
import { AppointmentModel, AttendeeData } from "../../interfaces";

const CONTEXT_TOKEN_COOKIE_NAME: string = 'sw-context-token';
const ATTENDEE_NAME_COOKIE_NAME: string = 'sw-gs-attendee-name';
const ATTENDEE_NAME_COOKIE_LIFESPAN: number = 60 * 60 * 24;
const ATTENDEE_NAME_COOKIE_PATH: string = '/';

describe("Composables - useAppointment", () => {
  const handleErrorMock = jest.fn();
  const startAppointmentMock = jest.fn();
  const endAppointmentMock = jest.fn();
  const joinAsGuideMock = jest.fn();
  const joinAsClientMock = jest.fn();
  const updateAttendeeMock = jest.fn();
  const broadcastMock = jest.fn();

  const rootContextMock = prepareRootContextMock();
  const isGuideMock = ref<boolean>(false);
  const isClientMock = ref<boolean>(false);
  const joinedAppointmentMock = ref<boolean>();
  const appointmentMock = ref<AppointmentModel>();
  const appointmentIdMock = ref<string>();
  const _attendeeNameMock = ref<string>();
  const joinErrorMock = ref<any>(null);
  const joinErrorAsGuideMock = ref<boolean>();
  const _appointmentStartedMock = ref<boolean>(false);
  const adminApiInstanceMock = ref(null);
  const userMock = ref(null);
  const mockedRoot = ref(rootContextMock);

  beforeEach(() => {
    jest.resetAllMocks();

    isGuideMock.value = false;
    isClientMock.value = false;
    joinedAppointmentMock.value = false;
    appointmentMock.value = null;
    appointmentIdMock.value = '';
    _attendeeNameMock.value = '';
    joinErrorMock.value = null;
    joinErrorAsGuideMock.value = false;
    _appointmentStartedMock.value = false;
    adminApiInstanceMock.value = null;
    userMock.value = null;
    mockedRoot.value = rootContextMock;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("appointmentStarted"))
            return _appointmentStartedMock;
          if (contextName.includes("isGuide"))
            return isGuideMock;
          if (contextName.includes("isClient"))
            return isClientMock;
          if (contextName.includes("attendeeName"))
            return _attendeeNameMock;
          if (contextName.includes("appointmentId"))
            return appointmentIdMock;
          if (contextName.includes("joinedAppointment"))
            return joinedAppointmentMock;
          if (contextName.includes("appointment"))
            return appointmentMock;
          if (contextName.includes("joinErrorAsGuide"))
            return joinErrorAsGuideMock;
          if (contextName.includes("joinError"))
            return joinErrorMock;
        },
      } as any;
    });

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    (mockedUseAdminApiInstance as any).mockImplementation(() => ({
      adminApiInstance: adminApiInstanceMock,
    } as any));

    mockedGSApiClient.AppointmentService = {
      startAppointment: startAppointmentMock,
      endAppointment: endAppointmentMock,
      joinAsGuide: joinAsGuideMock,
      joinAsClient: joinAsClientMock,
      updateAttendee: updateAttendeeMock,
    } as any;

    mockedComposables.useIntercept.mockImplementation(() => {
      return {
        broadcast: broadcastMock,
      } as any;
    });

    mockedComposables.useUser.mockImplementation(() => {
      return {
        user: userMock,
      } as any;
    });

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("start", () => {
      it("should return when isGuide.value invalid", async () => {
        isGuideMock.value = false;
        const { start } = useAppointment();
        await start();
        expect(startAppointmentMock).not.toBeCalled();
      });

      it("should work correctly when isGuide.value valid", async () => {
        _appointmentStartedMock.value = true;
        startAppointmentMock.mockResolvedValueOnce({ data: { id: '1' } });
        isGuideMock.value = true;
        const { start, appointmentStarted } = useAppointment();
        await start();
        expect(startAppointmentMock).toHaveBeenCalled();
        expect(appointmentStarted.value).toEqual(true);
      });

      it("should handle error correctly when API startAppointment failed", async () => {
        isGuideMock.value = true;
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        startAppointmentMock.mockRejectedValueOnce(res);

        const { start } = useAppointment();
        await start();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useAppointment",
            method: "start"
          }
        );
      });
    });

    describe("end", () => {
      it("should return when isGuide.value invalid", async () => {
        isGuideMock.value = false;
        const { end } = useAppointment();
        await end();
        expect(endAppointmentMock).not.toBeCalled();
      });

      it("should work correctly when isGuide.value valid", async () => {
        endAppointmentMock.mockResolvedValueOnce({ data: { id: '1' } });
        isGuideMock.value = true;
        const { end } = useAppointment();
        await end();
        expect(endAppointmentMock).toHaveBeenCalled();
      });

      it("should handle error correctly when API endAppointment failed", async () => {
        isGuideMock.value = true;
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        endAppointmentMock.mockRejectedValueOnce(res);

        const { end } = useAppointment();
        await end();

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useAppointment",
            method: "end"
          }
        );
      });
    });

    describe("getAttendeeName", () => {
      it("should return null when user is invalid", () => {
        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            get: () => null
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        userMock.value = null;
        const { getAttendeeName } = useAppointment();
        expect(getAttendeeName()).toEqual(null);
      });

      it("should return first name and last name value when user is valid", () => {
        userMock.value = {
          firstName: 'Lee',
          lastName: 'Nguyen',
        }
        const { getAttendeeName } = useAppointment();
        expect(getAttendeeName()).toEqual('Lee Nguyen');
      });

      it("should return attendeeName value when get correctly attendee cookie name", () => {
        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            get: () => ATTENDEE_NAME_COOKIE_NAME
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        userMock.value = null;
        const { getAttendeeName } = useAppointment();
        expect(getAttendeeName()).toEqual(ATTENDEE_NAME_COOKIE_NAME);
      });
    });

    describe("resetAppointmentValues", () => {
      it("should work correctly when param is false", () => {
        appointmentIdMock.value = '123';
        const { resetAppointmentValues, joinError, joinErrorAsGuide, appointmentId, joinedAppointment} = useAppointment();
        resetAppointmentValues();
        expect(joinError.value).toEqual(null);
        expect(joinErrorAsGuide.value).toEqual(false);
        expect(appointmentId.value).toEqual('');
        expect(joinedAppointment.value).toEqual(false);
      });

      it("should work correctly when param is true", () => {
        appointmentIdMock.value = '123';
        const { resetAppointmentValues, joinError, joinErrorAsGuide, appointmentId } = useAppointment();
        resetAppointmentValues(true);
        expect(joinError.value).toEqual(null);
        expect(joinErrorAsGuide.value).toEqual(false);
        expect(appointmentId.value).toEqual('123');
      });
    });

    describe("joinAsGuide", () => {
      it("should return when appointmentId.value is invalid", async () => {
        appointmentIdMock.value = '';
        const { joinAsGuide } = useAppointment();
        await joinAsGuide();
        expect(joinAsGuideMock).not.toBeCalled();
      });

      it("should work correctly when appointmentId.value is valid and guide param is true", async () => {
        appointmentIdMock.value = '1';
        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            set: () => CONTEXT_TOKEN_COOKIE_NAME
          },
          apiInstance: {
            config: {
              contextToKen: CONTEXT_TOKEN_COOKIE_NAME
            },
            update: jest.fn()
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        joinAsGuideMock.mockResolvedValueOnce({
          id: '1',
        });
        const {
          joinAsGuide,
          appointment,
          joinError,
          appointmentId,
          isGuide,
          joinedAppointment
        } = useAppointment();
        await joinAsGuide();
        expect(joinAsGuideMock).toHaveBeenCalledWith('1', adminApiInstanceMock);
        expect(appointment.value).toEqual({ id: '1' });
        expect(appointmentId.value).toEqual('1');
        expect(isGuide.value).toEqual(true);
        expect(joinError.value).toEqual(null);
        expect(joinErrorAsGuideMock.value).toEqual(false);
        expect(joinedAppointment.value).toEqual(true);
      });

      it("should handle error correctly when API joinAsGuide failed", async () => {
        appointmentIdMock.value = '1';
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        joinAsGuideMock.mockRejectedValueOnce(res);

        const { joinAsGuide, joinError, joinErrorAsGuide } = useAppointment();
        await joinAsGuide();

        expect(joinError.value).toEqual(res);
        expect(joinErrorAsGuide.value).toEqual(true);
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useAppointment",
            method: "join",
            push: false,
            asGuide: true,
          }
        );
        expect(broadcastMock).toHaveBeenCalledWith('useAppointment-join-error', res);
      });
    });

    describe("joinAsClient", () => {
      it("should return when appointmentId.value is invalid", async () => {
        appointmentIdMock.value = '';
        const { joinAsClient } = useAppointment();
        await joinAsClient();
        expect(joinAsClientMock).not.toBeCalled();
      });

      it("should work correctly when appointmentId.value is valid and guide param is false", async () => {
        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            get: () => ATTENDEE_NAME_COOKIE_NAME
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        appointmentIdMock.value = '1';
        userMock.value = {
          firstName: 'Lee',
          lastName: 'Nguyen',
        }
        joinAsClientMock.mockResolvedValueOnce({
          id: '1',
        });
        const { joinAsClient, appointment, isClient } = useAppointment();
        await joinAsClient();
        expect(joinAsClientMock).toHaveBeenCalledWith('1', 'Lee Nguyen', rootContextMock.apiInstance);
        expect(_attendeeNameMock.value).toEqual("Lee Nguyen");
        expect(appointment.value).toEqual({ id: '1' });
        expect(isClient.value).toEqual(true)
      });

      it("should work correctly when appointmentId.value is valid and guide param is false and user is invalid", async () => {
        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            get: () => ATTENDEE_NAME_COOKIE_NAME
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        appointmentIdMock.value = '1';
        userMock.value = null
        joinAsClientMock.mockResolvedValueOnce({
          id: '1',
        });
        const { joinAsClient, appointment, isClient } = useAppointment();
        await joinAsClient();
        expect(_attendeeNameMock.value).toEqual(ATTENDEE_NAME_COOKIE_NAME);
        expect(appointment.value).toEqual({ id: '1' });
        expect(isClient.value).toEqual(true)
      });

      it("should handle error correctly when API joinAsClient failed", async () => {
        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            get: () => ATTENDEE_NAME_COOKIE_NAME
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        appointmentIdMock.value = '1';
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        joinAsClientMock.mockRejectedValueOnce(res);

        const { joinAsClient, joinError, joinErrorAsGuide } = useAppointment();
        await joinAsClient();

        expect(joinError.value).toEqual(res);
        expect(joinErrorAsGuide.value).toEqual(false);
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useAppointment",
            method: "join",
            push: false,
            asGuide: false,
          }
        );
        expect(broadcastMock).toHaveBeenCalledWith('useAppointment-join-error', res);
      });

      it("should handle error correctly when API joinAsClient failed and statusCode is 307", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        await jest.setTimeout(1000);

        mockedRoot.value = {
          ...rootContextMock,
          cookies: {
            get: () => ATTENDEE_NAME_COOKIE_NAME
          }
        };
        mockedComposables.getApplicationContext.mockReturnValue(mockedRoot.value);
        appointmentIdMock.value = '1';
        const res = {
          message: 'something went wrong',
          statusCode: 307
        };

        joinAsClientMock.mockRejectedValueOnce(res);

        const { joinAsClient, joinError, joinErrorAsGuide } = useAppointment();
        await joinAsClient();

        expect(joinError.value).toEqual(res);
        expect(joinErrorAsGuide.value).toEqual(false);

        jest.runAllTimers();
        await flushPromises();

        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenCalledWith(expect.any(Function), 10000);
      });
    });

    describe("updateAttendeeData", () => {
      it("should return when attendeeName param is invalid", async () => {
        _attendeeNameMock.value = '';
        updateAttendeeMock.mockResolvedValueOnce({
          videoUserId: "testVideoUserId",
          guideCartPermissionsGranted: true,
        })
        const { updateAttendeeData, attendeeName } = useAppointment();
        await updateAttendeeData({
          videoUserId: "testVideoUserId",
          guideCartPermissionsGranted: true,
        });
        expect(updateAttendeeMock).toHaveBeenCalledWith({
          videoUserId: "testVideoUserId",
          guideCartPermissionsGranted: true,
        }, rootContextMock.apiInstance);
        expect(attendeeName.value).toEqual('');
      });

      it("should return when attendeeName param is valid", async () => {
        _attendeeNameMock.value = '11';
        updateAttendeeMock.mockResolvedValueOnce({
          attendeeName: "testAttendeeName",
          videoUserId: "testVideoUserId",
          guideCartPermissionsGranted: true,
        })
        const { updateAttendeeData, attendeeName } = useAppointment();
        await updateAttendeeData({
          attendeeName: "testAttendeeName",
          videoUserId: "testVideoUserId",
          guideCartPermissionsGranted: true,
        });
        expect(updateAttendeeMock).toBeCalled();
        expect(attendeeName.value).toEqual("testAttendeeName");
      });

      it("should handle error correctly when API getCustomerData failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        };

        updateAttendeeMock.mockRejectedValueOnce(res);

        const { updateAttendeeData } = useAppointment();
        await updateAttendeeData({
          guideCartPermissionsGranted: true,
        });

        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useAppointment",
            method: "updateAttendeeData"
          }
        );
      });
    });
  });

  describe("computed", () => {
    describe("appointmentStarted", () => {
      it("should return attendeeName value", () => {
        const { appointmentStarted } = useAppointment();
        expect(appointmentStarted.value).toEqual(_appointmentStartedMock.value);
      });
    });

    describe("isPreview", () => {
      it("should return attendeeName value", () => {
        appointmentMock.value = {
          isPreview: true
        } as any;
        const { isPreview } = useAppointment();
        expect(isPreview.value).toEqual(true);
      });
    });

    describe("isSelfService", () => {
      it("should return isSelfService value", () => {
        appointmentMock.value = {
          presentationGuideMode: 'self'
        } as any;
        const { isSelfService } = useAppointment();
        expect(isSelfService.value).toEqual(true);
      });
    });
  });
});
