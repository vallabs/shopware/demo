import { computed, onBeforeUnmount, ref, watch } from "@vue/composition-api";
import {
  getApplicationContext,
  useSharedState,
  useIntercept,
} from "@shopware-pwa/composables";
import { AppointmentService, PresentationService } from "../api-client";
import { useMercureObserver } from "./useMercureObserver";
import {
  PresentationStateForAll,
  PresentationStateForClients,
  PresentationStateForGuides,
  PresentationStateModel,
} from "../interfaces";
import { useErrorHandler } from "./useErrorHandler";
import { useAdminApiInstance } from "./useAdminApiInstance";
import { useAppointment } from "./useAppointment";
import { useInteractions } from "./useInteractions";
import { useMercure } from "./useMercure";
import { useWatchers } from "./useWatchers";

const COMPOSABLE_NAME = "useState";
const STATE_REFRESH_INTERVAL = 35 * 1000; // sec * ms = 60 sec;
const CLIENT_KEEP_ALIVE_PUSH_TIMEOUT = 10 * 1000; // sec * ms = 10 sec;

export const useState = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { handleError } = useErrorHandler();
  const {
    stateForAll: stateForAllChanged,
    stateForGuides: stateForGuidesChanged,
    stateForClients: stateForClientsChanged,
    lastGuideHoveredElement,
    requestedTokenPermissions,
  } = useMercureObserver();
  const { sharedRef } = useSharedState();
  const { mercurePublish } = useMercure();
  const { adminApiInstance } = useAdminApiInstance();
  const { publishInteraction } = useInteractions();
  const { broadcast } = useIntercept();
  const { appointmentId, isGuide, isClient, isSelfService, appointment } =
    useAppointment();
  const _storeState = sharedRef<PresentationStateModel>(
    `${contextName}-state`,
    {
      stateForAll: null,
      stateForGuides: null,
      stateForClients: null,
      stateForMe: null,
    }
  );
  const { addWatcher } = useWatchers();
  const _storeCurrentClientForGuide = sharedRef<string>(
    `${contextName}-current-client-for-guide`,
    null
  );
  const _storeCurrentClientForGuideToken = sharedRef<string>(
    `${contextName}-storeCurrentClientForGuideToken`,
    null
  );
  const _firstClientDefaultSet = sharedRef<boolean>(
    `${contextName}-firstInitializationDone`,
    false
  );
  const _currentValidToken = sharedRef<string>(
    `${contextName}-currentClientTokenPermissions`,
    ""
  );

  const clients = computed(() =>
    Object.keys(_storeState.value?.stateForGuides?.clients || {})
  );
  const guides = computed(() =>
    Object.keys(_storeState.value?.stateForGuides?.guides || {})
  );

  // indicates if current client gave guide his token permissions
  const _watchCurrentClientTokenPermissions = computed(() =>
    _storeCurrentClientForGuide.value &&
    _storeState.value?.stateForGuides?.clients[
      _storeCurrentClientForGuide.value
    ]?.guideCartPermissionsGranted
      ? _storeCurrentClientForGuide.value
      : null
  );
  const currentClientTokenPermissions = computed(
    () =>
      _storeCurrentClientForGuide.value &&
      _storeState.value?.stateForGuides?.clients[
        _storeCurrentClientForGuide.value
      ]?.guideCartPermissionsGranted &&
      _storeCurrentClientForGuideToken.value &&
      _storeState.value?.stateForAll?.allowUserActionsForGuide
  );

  async function getContextTokenForCurrentClient(
    forceRefresh = false,
    attendeeId?: string
  ) {
    // if token already exist and should not be changed
    if (_storeCurrentClientForGuideToken.value && !forceRefresh)
      return _storeCurrentClientForGuideToken.value;

    if (typeof attendeeId === "undefined") {
      attendeeId = _storeCurrentClientForGuide.value;
    }

    const requestedClientTokenPermissions =
      attendeeId &&
      _storeState.value?.stateForGuides?.clients[attendeeId]
        ?.guideCartPermissionsGranted;

    if (!requestedClientTokenPermissions) {
      await setCurrentClientForGuideToken(null, attendeeId);
      return null;
    }

    try {
      const newToken = await AppointmentService.getAttendeeContextToken(
        attendeeId,
        adminApiInstance
      );
      await setCurrentClientForGuideToken(newToken, attendeeId);
      return newToken;
    } catch (e) {
      // reset current client for guide if error occured
      await setCurrentClientForGuideToken(null, attendeeId);

      handleError(e, {
        context: contextName,
        method: "getContextToken",
        push: true,
      });
      return null;
    }
  }

  async function setCurrentClientForGuide(attendeeId) {
    _storeCurrentClientForGuide.value = attendeeId;
    await getContextTokenForCurrentClient(true, attendeeId);
  }

  function setCurrentClientForGuideToken(newToken, attendeeId) {
    const oldToken = _storeCurrentClientForGuideToken.value;
    _storeCurrentClientForGuideToken.value = newToken;

    updateTriggerVariable(attendeeId, newToken);

    if (oldToken !== newToken) {
      broadcast("client-for-guide-token-changed", newToken);
    }
  }

  const resetCurrentClientForGuide = async () => {
    await setCurrentClientForGuide(null);
  };

  function updateTriggerVariable(attendeeId, token) {
    _currentValidToken.value = "token" + attendeeId + token;
  }

  const setDefaultClientForGuide = async () => {
    // if NO client is set already
    if (!_storeCurrentClientForGuide.value && !_firstClientDefaultSet.value) {
      // set first client as current client for guide if it is empty and only one client is present
      if (clients.value.length === 1) {
        await setCurrentClientForGuide(clients.value[0]);
      }
      if (clients.value.length >= 1) {
        _firstClientDefaultSet.value = true;
      }
    }

    // if a client was SET already
    if (_storeCurrentClientForGuide.value) {
      // if some old clients exists but the current client is no more in there
      if (clients.value.indexOf(_storeCurrentClientForGuide.value) < 0) {
        await resetCurrentClientForGuide();
      }
    }
  };

  const _registerStateWatchers = () => {
    addWatcher(watch(stateForAllChanged, (newStateForAll: unknown) => {
      _storeState.value.stateForAll = newStateForAll as PresentationStateForAll;

      // set appointment presentation mode if state was updated from guided to self service
      if (
        appointment.value.presentationGuideMode === "guided" &&
        _storeState.value?.stateForAll?.appointmentMode === "self"
      ) {
        appointment.value.presentationGuideMode = "self";
      }
    }));

    addWatcher(watch(
      stateForGuidesChanged,
      (newStateForGuides: unknown) => {
        _storeState.value.stateForGuides = newStateForGuides as PresentationStateForGuides;
        setTimeout(setDefaultClientForGuide, 100); // because shared ref updates are not ready at this moment (also the shared ref of the mercure observer..)
      }
    ));

    addWatcher(watch(
      stateForClientsChanged,
      (newStateForClients: unknown) => {
        _storeState.value.stateForClients = newStateForClients as PresentationStateForClients;
      }
    ));

    //reload cart if client gave cart Permissions
    addWatcher(watch(_watchCurrentClientTokenPermissions, () => {
      getContextTokenForCurrentClient(
        true,
        _watchCurrentClientTokenPermissions.value
      );
    }));
  };

  const loadState = async (skipStateForAll: boolean = false) => {
    try {
      if (isGuide.value) {
        const response = await PresentationService.getStateForGuides(
          appointmentId.value,
          adminApiInstance
        );
        if (!skipStateForAll)
          _storeState.value.stateForAll = response.stateForAll;

        _storeState.value.stateForGuides = response.stateForGuides;
        await setDefaultClientForGuide();
      }

      if (isSelfService.value || isClient.value) {
        // also load state for client of guide joins self service appointment
        const response = await PresentationService.getStateForClients(
          apiInstance
        );
        if (!skipStateForAll)
          _storeState.value.stateForAll = response.stateForAll;

        _storeState.value.stateForClients = response.stateForClients;
        _storeState.value.stateForMe = response.stateForMe;

        if (!lastGuideHoveredElement.value && response.stateForClients?.hoveredElementId) {
          lastGuideHoveredElement.value = response.stateForClients.hoveredElementId;
        }
        return;
      }
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: "loadState",
      });
    }
  };

  const interval = ref(null);

  const initState = async () => {
    await loadState();

    if (isSelfService.value) {
      return;
    }

    _registerStateWatchers();

    if (isGuide.value) {
      setTimeout(() => {
        if (interval.value) return;
        interval.value = setInterval(() => {
          loadState(true);
          mercurePublish.all("heartbeat", true);
        }, STATE_REFRESH_INTERVAL);
        CLIENT_KEEP_ALIVE_PUSH_TIMEOUT;
      });
    }

    if (isClient.value) {
      if (interval.value) return;
      interval.value = setInterval(() => {
        publishInteraction({
          name: "keep.alive",
        });
      }, STATE_REFRESH_INTERVAL);
    }
  };

  onBeforeUnmount(() => {
    if (interval.value) clearInterval(interval.value);
  });

  return {
    initState,
    loadState,
    stateForAll: computed(() => _storeState.value?.stateForAll),
    stateForGuides: computed(() => _storeState.value?.stateForGuides),
    stateForClients: computed(() => _storeState.value?.stateForClients),
    stateForMe: computed(() => _storeState.value?.stateForMe),
    clients,
    guides,
    currentClientForGuide: computed(() => _storeCurrentClientForGuide.value),
    currentValidToken: computed(() => _currentValidToken.value),
    currentClientTokenPermissions,
    currentGuide: computed(
      () =>
        _storeState.value?.stateForGuides?.guides[appointment.value.attendeeId]
    ),
    ended: computed(() => _storeState.value?.stateForAll?.ended),
    started: computed(() => _storeState.value?.stateForAll?.started),
    showTokenPermissionsRequest: computed(
      () => requestedTokenPermissions.value === appointment.value?.attendeeId
    ),
    setCurrentClientForGuide,
    resetCurrentClientForGuide,
    getContextTokenForCurrentClient,
  };
};
