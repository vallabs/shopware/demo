import {useSharedState} from '@shopware-pwa/composables';
import {computed, ComputedRef, onMounted} from '@vue/composition-api';
import {useAppointment} from "./useAppointment";
import {useUIState} from "@shopware-pwa/composables"
export type PresentationLayerPosition = 'fixed' | 'static';
export type PresentationLayerSize = 'maximized' | 'minimized';

const COMPOSABLE_NAME = 'usePresentationLayer';
interface IUsePresentationLayer {
    sidebarClasses: ComputedRef<string>;
    positionState: any;
    sizeState: any;
    toggleLayerSize: () => void;
    toggleLayerPosition: (position: PresentationLayerPosition) => void;
    navPositionClass: ComputedRef<string>;
}

export const usePresentationLayer = (): IUsePresentationLayer => {

  const contextName = COMPOSABLE_NAME;
  const { sharedRef } = useSharedState();
  const positionState = sharedRef<PresentationLayerPosition>(`${contextName}-positionState`, 'fixed');
  const sizeState = sharedRef<PresentationLayerSize>(`${contextName}-sizeState`, 'maximized');
  const { isOpen: isSidebarOpen } = useUIState({ stateName: "GS_OFFCANVAS_SIDEBAR_STATE" })
  const { isClient } = useAppointment();

  const sidebarClasses = computed(
    () => sizeState.value + ' ' + positionState.value
  );

  const navPositionClass = computed(() => {
    if (isSidebarOpen.value) return 'layer-maximized';
    if (isClient.value) return;
    if (sizeState.value === 'minimized') return 'layer-minimized';
    return 'layer-maximized'
  });

  function toggleLayerSize() {
    if (sizeState.value === 'maximized') {
      sizeState.value = 'minimized';
    } else if (sizeState.value === 'minimized') {
      sizeState.value = 'maximized';
    }
  }

  function toggleLayerPosition() {
    positionState.value = (positionState.value == 'static') ? 'fixed' : 'static';
  }

  onMounted(() => {
    positionState.value = window.innerWidth >= 1200 ? 'static' : 'fixed';
  });


  return {
    sidebarClasses,
    positionState,
    sizeState,
    toggleLayerSize,
    toggleLayerPosition,
    navPositionClass
  }
};
