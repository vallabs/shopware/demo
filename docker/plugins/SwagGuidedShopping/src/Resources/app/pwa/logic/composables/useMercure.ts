import {
  useSharedState,
  getApplicationContext,
  useIntercept
} from '@shopware-pwa/composables';
import { useAppointment } from './useAppointment';
import { computed } from 'vue-demi';
import { AppointmentModel } from "../interfaces";
import { AxiosResponse } from 'axios';
import { EventSourcePolyfill } from 'event-source-polyfill';

const COMPOSABLE_NAME = 'useMercure';

type MercureEvent =
  'heartbeat'
  | 'scrolledTo'
  | 'highlighted'
  | 'guide.hovered'
  | 'client.hovered'
  | 'client.changedLikes'
  | 'guide.changedLikes'
  | 'product.wasLiked'
  | 'client.changedCart'
  | 'guide.changedCart'
  | 'dynamicPage.opened'
  | 'dynamicPage.closed'
  | 'dynamicProductListingPage.loadedMore'
  | 'navigated'
  | 'requestedTokenPermissions'
  | 'grantedTokenPermissions'
  | 'state-for-all'
  | 'state-for-guides'
  | 'state-for-clients'
  | 'newInstantListingCreated'
  | 'instantListingUpdated'
  | 'activeSpeakerUpdated';

export interface MercureEventCallbacks {
  [key: string]: {
    [key: string]: { (value: string): void; } []
  }
}

const DEFAULT_NAMESPACE: string = 'mercure';

export const useMercure = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { joinedAppointment, appointment, isGuide, isClient, isSelfService } = useAppointment();
  const { sharedRef } = useSharedState();
  const { broadcast } = useIntercept();
  const _isConnected = sharedRef<boolean>(`${contextName}-isConnected`);
  const _showConnectError = sharedRef<boolean>(`${contextName}-showConnectError`, false);
  const isTryingToConnect = sharedRef<boolean>(`${contextName}-isTryingToConnect`);
  const _lastMercureMsgId = sharedRef<string>(`${contextName}-lastMercureMsgId`);
  const eventCallbacks = sharedRef<MercureEventCallbacks>(`${contextName}-eventCallbacks`, {});

  let eventSource: EventSource = null;

  const reconnectFrequencyMultiplier: number = 2;
  const reconnectMaxSeconds: number = 64;
  let reconnectFrequencySeconds: number = 1;
  const showErrorMsgAfterSeconds: number = 8;

  /**
   * connects to the mercure server
   */
  function connect(reconnect = false) {
    if (isSelfService.value) return;
    if (_isConnected.value) return;
    if (isTryingToConnect.value) return;
    if (!joinedAppointment.value || !appointment.value) return;
    isTryingToConnect.value = true;
    const url = new URL(appointment.value.mercureHubPublicUrl);
    appointment.value.mercureSubscriberTopics.forEach(topic => {
      url.searchParams.append('topic', encodeURIComponent(topic));

      // append id of last event on reconnect so the client gets all events from that point on
      if (reconnect && _lastMercureMsgId.value) {
        url.searchParams.append('Last-Event-ID', _lastMercureMsgId.value);
      }
    });
    const options = !process.env.ALLOW_ANONYMOUS_MERCURE ? {
      withCredentials: true,
    } : {};
    eventSource = new EventSourcePolyfill(url.toString(), {
      ...options,
      headers: {
        'Authorization': `Bearer ${appointment.value.JWTMercureSubscriberToken}`
      }
    });
    eventSource.onmessage = onEventSourceMessage;
    eventSource.onopen = !reconnect ? onEventSourceOpen : onEventSourceReconnectedOpen;
    eventSource.onerror = onEventSourceError;
  }

  /**
   * on message receive from the mercure server
   * @param e
   */
  function onEventSourceMessage(e) {
    if (!e.data) return;
    const data = JSON.parse(e.data);

    _lastMercureMsgId.value = e.lastEventId;

    if (!data) return;

    Object.entries(data).forEach(entry => {
      onValueChangeFromMercure(entry[0].toString() as MercureEvent, entry[1]);
    });
  }

  /**
   * on connection open to the mercure server
   */
  function onEventSourceOpen() {
    resetReconnectDelay();
    _isConnected.value = true;
    _showConnectError.value = false;
    isTryingToConnect.value = false;
    console.log('[Mercure] connected');
  }

  /**
   * on reconnected connection open to the mercure server
   */
  function onEventSourceReconnectedOpen() {
    resetReconnectDelay();
    _isConnected.value = true;
    _showConnectError.value = false;
    isTryingToConnect.value = false;
    console.log('[Mercure] reconnected');
    broadcast('mercure-reconnect', { eventSource: eventSource })
  }

  /**
   * on connection error to the mercure server
   */
  function onEventSourceError() {
    _isConnected.value = false;
    isTryingToConnect.value = false;
    eventSource.close();
    eventSource = null;

    setTimeout(() => {
      if (reconnectFrequencySeconds >= showErrorMsgAfterSeconds) {
        _showConnectError.value = true;
      }

      connect(true);
      increaseReconnectDelay();
    }, reconnectFrequencySeconds * 1000);
  }

  /**
   * increments the reconnect delay
   */
  function increaseReconnectDelay() {
    reconnectFrequencySeconds *= reconnectFrequencyMultiplier;
    if (reconnectFrequencySeconds >= reconnectMaxSeconds) {
      reconnectFrequencySeconds = reconnectMaxSeconds;
    }
  }

  /**
   * resets the reconnect delay
   */
  function resetReconnectDelay() {
    reconnectFrequencySeconds = 1;
  }

  /**
   * fires all registered callbacks for the event
   *
   * @param event
   * @param value
   */
  function onValueChangeFromMercure(event: MercureEvent, value: any) {
    if (!eventCallbacks.value[event]) return;
    Object.values(eventCallbacks.value[event]).forEach(callbacks => {
      callbacks.forEach(callback => {
        if (typeof callback === 'function') callback(value);
      });
    });
  }

  /**
   * registers a callback for a mercure event on the current element
   * @param event
   * @param callback
   * @param namespace
   */
  function mercureSubscribe(event: MercureEvent, callback: (value: any) => void, namespace: string = DEFAULT_NAMESPACE) {
    eventCallbacks.value = eventCallbacks.value || {};
    eventCallbacks.value[event] = eventCallbacks.value[event] || {};
    eventCallbacks.value[event][namespace] = eventCallbacks.value[event][namespace] || [];
    eventCallbacks.value[event][namespace].push(callback);
  }

  /**
   * unregisters all callbacks for a mercure event on the current element
   *
   * @param event
   * @param namespace
   */
  function mercureUnsubscribe(event: MercureEvent, namespace: string = DEFAULT_NAMESPACE) {
    eventCallbacks.value[event] = eventCallbacks.value[event] || {};
    delete eventCallbacks.value[event][namespace];
  }

  /**
   * publish mercure event only allowed as a client
   *
   * @param event
   * @param value
   */
  function mercureClientPublish(event: MercureEvent, value: any): Promise<AxiosResponse> {
    if (!isClient.value) return;
    return mercurePublish(event, value);
  }

  /**
   * publish mercure event only allowed as a guide
   *
   * @param event
   * @param value
   */
  function mercureGuidePublish(event: MercureEvent, value: any): Promise<AxiosResponse> {
    if (!isGuide.value) return;
    return mercurePublish(event, value);
  }

  /**
   * publish mercure event only locally
   *
   * @param event
   * @param value
   */
  function mercureLocalPublish(event: MercureEvent, value: any) {
    onValueChangeFromMercure(event, value);
  }

  /**
   * publish mercure event
   *
   * @param event
   * @param value
   */
  function mercurePublish(event: MercureEvent, value: any): Promise<AxiosResponse> {
    if (!apiInstance || isSelfService.value || !_isConnected.value) return null;

    mercureLocalPublish(event, value);
    return publishEvent(event, value, appointment.value);
  }

  /**
   * publishes mercure event
   *
   * @param event
   * @param value
   * @param appointment
   */
  function publishEvent(event: MercureEvent, value: any, appointment: AppointmentModel): Promise<AxiosResponse> {
    if (!appointment) return null;
    if (!appointment.JWTMercurePublisherToken) return null;
    if (!appointment.mercurePublisherTopic) return null;

    const data = {
      [event]: value,
    };

    // cross domain mercure configuration
    const http = new XMLHttpRequest();
    const url = appointment.mercureHubPublicUrl;
    const body = new URLSearchParams({
      data: JSON.stringify(data),
      topic: appointment.mercurePublisherTopic,
      private: 'on'
    });
    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.setRequestHeader('Authorization', 'Bearer ' + appointment.JWTMercurePublisherToken);
    http.send(body);
  }

  return {
    mercurePublish: {
      guide: mercureGuidePublish,
      client: mercureClientPublish,
      local: mercureLocalPublish,
      all: mercurePublish,
    },
    mercureSubscribe,
    mercureUnsubscribe,
    connect,
    onEventSourceMessage,
    onEventSourceOpen, 
    onEventSourceReconnectedOpen,
    onEventSourceError,
    connected: computed(() => _isConnected.value),
    showConnectError: computed(() => _showConnectError.value)
  }
};
