import { ref, createApp } from "vue-demi";
import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import { prepareRootContextMock } from "./contextRunner";
import { useMercureObserver } from '../useMercureObserver';
import { useMercure } from "../useMercure";
import {
  InstantListingUpdateData,
  PresentationStateForAll,
  PresentationStateForClients,
  PresentationStateForGuides,
  ScrollToTarget,
  VideoAudioSettingsType
} from "../../interfaces";
import {DynamicPageResponse, DynamicProductListingPage} from "../useDynamicPage";
jest.mock("../useMercure");
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

function mockLoadComposableInApp(composable) {
  let result;
  const app = createApp( {
    setup() {
      result = composable();
      return result;
    }
  } );
  const wrapper = app.mount( document.createElement( 'div' ) );
  return [ result, app, wrapper ];
}

describe("Composables - useMercureObserver", () => {
  let result;
  let app;
  let wrapper;
  let eventCallbacks = ref({});
  const mercureSubscribeMock = jest.fn();;
  const mercureUnsubscribeMock = jest.fn();

  const rootContextMock = prepareRootContextMock();
  const highlightedElementMock = ref<string>("");
  const lastGuideHoveredElementMock = ref<string>("");
  const lastClientHoveredElementMock = ref<string>("");
  const lastClientChangedLikesMock = ref<string>("");
  const likedProductMock = ref<string>("");
  const lastGuideChangedLikesMock = ref<string>("");
  const lastClientChangedCartMock = ref<string>("");
  const lastGuideChangedCartMock = ref<string>("");
  const scrollToTargetMock = ref<ScrollToTarget>(null);
  const navigationPageIndexMock = ref<number>(1);
  const requestedTokenPermissionsMock = ref<string>("");
  const grantedTokenPermissionsMock = ref<string>("");
  const currentDynamicPageMock = ref<DynamicPageResponse>(null);
  const stateForAllMock = ref<PresentationStateForAll>(null);
  const stateForGuidesMock = ref<PresentationStateForGuides>(null);
  const stateForClientsMock = ref<PresentationStateForClients>(null);
  const newInstantListingMock = ref<boolean>(true);
  const instantListingUpdatedMock = ref<InstantListingUpdateData>(null);
  const activeSpeakerMock = ref<string>("");
  const currentLoadedInfoMock = ref<DynamicProductListingPage>(null);

  beforeEach(() => {
    jest.resetAllMocks();

    mercureSubscribeMock.mockImplementation((event, cb, namespace) => {
      eventCallbacks.value = eventCallbacks.value || {};
      eventCallbacks.value[event] = eventCallbacks.value[event] || {};
      eventCallbacks.value[event][namespace] = eventCallbacks.value[event][namespace] || [];
      eventCallbacks.value[event][namespace].push(cb);
    });

    highlightedElementMock.value = "";
    lastGuideHoveredElementMock.value = "";
    lastClientHoveredElementMock.value = "";
    lastClientChangedLikesMock.value = "";
    likedProductMock.value = "";
    lastGuideChangedLikesMock.value = "";
    lastClientChangedCartMock.value = "";
    lastGuideChangedCartMock.value = "";
    scrollToTargetMock.value = {
      elementId: 'dummy-element-id',
      attendeeId: 'dummy-attendee-id',
      index: 1
    };
    navigationPageIndexMock.value = 1;
    requestedTokenPermissionsMock.value = "";
    grantedTokenPermissionsMock.value = "";
    currentDynamicPageMock.value = {
      type: "product",
      productId: "dummy-product-id",
      opened: false
    };
    stateForAllMock.value = {
      accessibleFrom: "",
      accessibleTo: "",
      allowUserActionsForGuide: false,
      appointmentMode: "guide",
      attendeeRestrictionType: "open",
      broadcastMode: false,
      currentGuideProductId: "",
      currentPageId: "",
      currentDynamicPage: currentDynamicPageMock.value,
      currentSectionId: "",
      currentSlideAlias: 1,
      started: false,
      startedAt: "",
      running: false,
      ended: false,
      endedAt: "",
      lastActiveGuideSection: "",
      productDetailDefaultPageId: "",
      productListingDefaultPageId: "",
      quickviewPageId: "",
      videoAudioSettings: VideoAudioSettingsType.BOTH,
      videoRoomUrl: "",
      extensions: {},
    };
    stateForGuidesMock.value = {
      clients: {},
      inactiveClients: {},
      guides: {},
      videoGuideToken: "",
      extensions: {}
    };
    stateForClientsMock.value = {
      videoClientToken: "",
      extensions: {}
    };
    newInstantListingMock.value = false;
    instantListingUpdatedMock.value = {
      updated: false,
      currentIlIndex: 0
    };
    activeSpeakerMock.value = "";
    currentLoadedInfoMock.value = {
      type: 'productList',
      page: 1
    };

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("highlightedElement"))
            return highlightedElementMock;
          if (contextName.includes("-lastGuideHoveredElement"))
            return lastGuideHoveredElementMock;
          if (contextName.includes("-lastClientHoveredElement"))
            return lastClientHoveredElementMock;
          if (contextName.includes("lastClientChangedLikes"))
            return lastClientChangedLikesMock;
          if (contextName.includes("likedProduct"))
            return likedProductMock;
          if (contextName.includes("lastGuideChangedLikes"))
            return lastGuideChangedLikesMock;
          if (contextName.includes("lastClientChangedCart"))
            return lastClientChangedCartMock;
          if (contextName.includes("lastGuideChangedCart"))
            return lastGuideChangedCartMock;
          if (contextName.includes("scrollToTarget"))
            return scrollToTargetMock;
          if (contextName.includes("navigationPageIndex"))
            return navigationPageIndexMock;
          if (contextName.includes("requestedTokenPermissions"))
            return requestedTokenPermissionsMock;
          if (contextName.includes("grantedTokenPermissions"))
            return grantedTokenPermissionsMock;
          if (contextName.includes("current-dynamicPage"))
            return currentDynamicPageMock;
          if (contextName.includes("stateForAll"))
            return stateForAllMock;
          if (contextName.includes("stateForGuides"))
            return stateForGuidesMock;
          if (contextName.includes("stateForClients"))
            return stateForClientsMock;
          if (contextName.includes("newInstantListing")) return newInstantListingMock;
          if (contextName.includes("instantListingUpdated")) return instantListingUpdatedMock;
          if (contextName.includes("activeSpeaker")) return activeSpeakerMock;
          if (contextName.includes("currentLoadedInfo")) return currentLoadedInfoMock;
        },
      } as any;
    });

    (mockedUseMercure as any).mockImplementation(
      () =>
        ({
          mercureSubscribe: mercureSubscribeMock,
          mercureUnsubscribe: mercureUnsubscribeMock
        } as any)
    );
  });

  describe("process", () => {
    it("should not unsubscribe when unmounting if did not subscribe yet", async () => {
      [result, app, wrapper] = mockLoadComposableInApp(() => useMercureObserver());
      await app.unmount();
      expect(mercureUnsubscribeMock).not.toHaveBeenCalled();
    });

    it("should unsubscribe when unmounting if subscribed mercure events", async () => {
      [result, app, wrapper] = mockLoadComposableInApp(() => useMercureObserver());
      await wrapper.subscribeToMercureEvents();
      expect(mercureSubscribeMock).toHaveBeenCalledTimes(21);
      await app.unmount();
      expect(mercureUnsubscribeMock).toHaveBeenCalledTimes(21);
    });

    it("should not subscribe again if already subscribed mercure events and not unmount yet", async () => {
      [result, app, wrapper] = mockLoadComposableInApp(() => useMercureObserver());
      await wrapper.subscribeToMercureEvents();
      expect(mercureSubscribeMock).toHaveBeenCalledTimes(21);
      await wrapper.subscribeToMercureEvents();
      expect(mercureUnsubscribeMock).not.toHaveBeenCalled();
    });
  });

  describe("it should works correctly, when mercure event fire:", () => {
    beforeEach(async () => {
      [result, app, wrapper] = mockLoadComposableInApp(() => useMercureObserver());
      await wrapper.subscribeToMercureEvents();
    });

    afterEach(async () => {
      await app.unmount();
    });

    it("highlighted", async () => {
      eventCallbacks.value["highlighted"]?.['presentationLayer']?.[0]?.('1');
      expect(result.highlightedElement.value).toEqual('1');
    });

    it("guide.hovered", async () => {
      eventCallbacks.value["guide.hovered"]?.['presentationLayer']?.[0]?.('1');
      expect(result.lastGuideHoveredElement.value).toEqual('1');
    });

    it("client.hovered", async () => {
      eventCallbacks.value["client.hovered"]?.['presentationLayer']?.[0]?.('1');
      expect(result.lastClientHoveredElement.value).toEqual('1');
    });

    it("client.changedLikes", async () => {
      eventCallbacks.value["client.changedLikes"]?.['presentationLayer']?.[0]?.('1');
      expect(result.lastClientChangedLikes.value).toEqual('1');
    });

    it("product.wasLiked", async () => {
      eventCallbacks.value["product.wasLiked"]?.['presentationLayer']?.[0]?.('1');
      expect(result.likedProduct.value).toEqual('1');
    });

    it("guide.changedLikes", async () => {
      eventCallbacks.value["guide.changedLikes"]?.['presentationLayer']?.[0]?.('1');
      expect(result.lastGuideChangedLikes.value).toEqual('1');
    });

    it("client.changedCart", async () => {
      eventCallbacks.value["client.changedCart"]?.['presentationLayer']?.[0]?.('1');
      expect(result.lastClientChangedCart.value).toEqual('1');
    });

    it("guide.changedCart", async () => {
      eventCallbacks.value["guide.changedCart"]?.['presentationLayer']?.[0]?.('1');
      expect(result.lastGuideChangedCart.value).toEqual('1');
    });

    it("scrolledTo", async () => {
      eventCallbacks.value["scrolledTo"]?.['presentationLayer']?.[0]?.('1');
      expect(result.scrollToTarget.value).toEqual('1');
    });

    it("navigated", async () => {
      eventCallbacks.value["navigated"]?.['presentationLayer']?.[0]?.('1');
      expect(result.navigationPageIndex.value).toEqual(1);
      expect(result.highlightedElement.value).toEqual('');
    });

    it("requestedTokenPermissions", async () => {
      eventCallbacks.value["requestedTokenPermissions"]?.['presentationLayer']?.[0]?.('1');
      expect(result.requestedTokenPermissions.value).toEqual('1');
    });

    it("grantedTokenPermissions", async () => {
      eventCallbacks.value["grantedTokenPermissions"]?.['presentationLayer']?.[0]?.('1');
      expect(result.grantedTokenPermissions.value).toEqual('1');
    });

    it("dynamicPage.opened", async () => {
      eventCallbacks.value["dynamicPage.opened"]?.['presentationLayer']?.[0]?.('1');
      expect(result.currentDynamicPage.value).toEqual('1');
      expect(result.highlightedElement.value).toEqual('');
    });

    it("dynamicProductListingPage.loadedMore", async () => {
      eventCallbacks.value["dynamicProductListingPage.loadedMore"]?.['presentationLayer']?.[0]?.({
        page: 2,
        type: 'productList'
      });
      expect(result.currentLoadedInfo.value).toEqual({"page": 2, "type": "productList"});
    });

    it("dynamicPage.closed", async () => {
      eventCallbacks.value["dynamicPage.closed"]?.['presentationLayer']?.[0]?.('1');
      expect(result.currentDynamicPage.value).toEqual('1');
      expect(result.highlightedElement.value).toEqual('');
    });

    it("state-for-all", async () => {
      eventCallbacks.value["state-for-all"]?.['presentationLayer']?.[0]?.({
        a: 1,
        b: 2
      });
      expect(result.stateForAll.value).toEqual({
        a: 1,
        b: 2
      });
    });

    it("state-for-clients", async () => {
      eventCallbacks.value["state-for-clients"]?.['presentationLayer']?.[0]?.({
        a: 1,
        b: 2
      });
      expect(result.stateForClients.value).toEqual({
        a: 1,
        b: 2
      });
    });

    it("state-for-guides", async () => {
      eventCallbacks.value["state-for-guides"]?.['presentationLayer']?.[0]?.({
        a: 1,
        b: 2
      });
      expect(result.stateForGuides.value).toEqual({
        a: 1,
        b: 2
      });
    });

    it("newInstantListingCreated", async () => {
      eventCallbacks.value["newInstantListingCreated"]?.['presentationLayer']?.[0]?.(true);
      expect(result.newInstantListing.value).toEqual(true);
    });

    it("instantListingUpdated", async () => {
      eventCallbacks.value["instantListingUpdated"]?.['presentationLayer']?.[0]?.({
        updated: true,
        currentIlIndex: 1
      });
      expect(result.instantListingUpdated.value).toEqual({
        updated: true,
        currentIlIndex: 1
      });
    });

    it("activeSpeakerUpdated", async () => {
      eventCallbacks.value["activeSpeakerUpdated"]?.['presentationLayer']?.[0]?.('1');
      expect(result.activeSpeaker.value).toEqual('1');
    });
  });
});

