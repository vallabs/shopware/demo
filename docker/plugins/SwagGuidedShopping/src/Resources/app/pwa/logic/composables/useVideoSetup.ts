import { useSharedState } from '@shopware-pwa/composables';
import { ref, watch, computed } from "@vue/composition-api"

import { useErrorHandler } from "./useErrorHandler";
import { useMediaDevices, DeviceList } from "./useMediaDevices";
import { useState } from './useState';
import { useWatchers } from './useWatchers';

const COMPOSABLE_NAME = 'useVideoSetup';

export const useVideoSetup = function () {
  const contextName = COMPOSABLE_NAME;
  const { sharedRef } = useSharedState();
  const { stateForAll } = useState();
  const { handleError } = useErrorHandler();
  const {
    getMediaDevices,
    setDevicesFromStorage,
    mediaDeviceChanged,
    microphoneDevice,
    cameraDevice,
    deviceError,
    handleDeviceError,
    getMediaStream,
    isAudioOnly,
  } = useMediaDevices();
  const { addWatcher } = useWatchers();
  const _videoEl = ref<HTMLVideoElement>(null);
  const _isSetupReady = sharedRef<boolean>(`${contextName}-isSetupReady`, false);
  const _watchersStarted = sharedRef<boolean>(`${contextName}-watchersStarted`, false);
  const _stopDeviceChange = sharedRef<boolean>(`${contextName}-stopDeviceChange`, false);

  /**
   * init custom stream to request devices
   *
   * @param videoEl
   */
  const initSetup = async (videoEl) => {

    try {
      _videoEl.value = videoEl;

      startWatchers();

      let devices = await getMediaDevices(true, true); // get almost empty default list

      // triggers browser permission request for default devices
      await onDeviceChange({
        audio: devices?.audioInput[0]?.deviceId,
        video: devices?.videoInput[0]?.deviceId,
      }, true);

      // request devices again to get their full name
      devices = await getMediaDevices(true);

      // if it would has been an permission error, the exception would be thrown a way earlier
      // if your cam is already used by f.e. zoom firefox on windows will deliver the devices without labels anyway but does NOT throw a "permission denied" exeption
      // so we make sure that there are labels, and if not we throw a custom exception
      ensureDeviceLabels(devices);

      // load from storage but use defaults, watchers are already started, its like choosing one with the dropdown
      setDevicesFromStorage(devices);

      _isSetupReady.value = true;

    } catch (err) {
      if (err === "CAMERA_IN_USE" || err === "MICROPHONE_IN_USE") {
        handleDeviceError(err);
      }else {
        handleDeviceError('NO_DEVICES_FOUND');
      }
    }
  };

  const ensureDeviceLabels = (devices: DeviceList) => {
    if (isAudioOnly.value) return;
    devices.videoInput.forEach(info => {
      if (!info?.label) throw "CAMERA_IN_USE";
    });
    devices.audioInput.forEach(info => {
      if (!info?.label) throw "MICROPHONE_IN_USE";
    });
  }

  const stopDeviceChanges = () => {
    _stopDeviceChange.value = true;
  };

  /**
   * attach the stream to the video element
   * @param stream
   */
  const gotMediaStream = (stream) => {
    try {
      if (!_videoEl.value) return;

      _videoEl.value.srcObject = stream;

    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'gotMediaStream',
      });
    }
  };


  /**
   * callback when devices change or get set up
   */
  const onDeviceChange = async (state, ensureException: boolean = false) => {
    if (deviceError.value) return;
    if (isAudioOnly.value) return;

    try {
      const stream = await getMediaStream(state);
      await gotMediaStream(stream);
    } catch (err) {
      if (err.code === 0) handleDeviceError("NO_DEVICES_FOUND");
      if (ensureException) throw "NO_DEVICES_FOUND";
    }

  };


  /**
   * registers a callback that listens to permission change
   * experimental feature, does not work in firefox
   */
  const registerPermissionChangeEvent = async () => {
    try {
      const status = await navigator.permissions.query({ name: "camera" });
      status.addEventListener("change", () => {
        onDeviceChange({
          video: cameraDevice.value
        });
      }, { once: true });
    } catch (e) {
      // experimental feature, ignore if not available
    }
  };

  const startWatchers = async () => {
    if (_watchersStarted.value) return;
    _watchersStarted.value = true;

    registerPermissionChangeEvent();

    addWatcher(watch(mediaDeviceChanged, (state) => {
      if (_stopDeviceChange.value) return;
      if (!state) return;

      const deviceId = state.deviceId;
      if (!deviceId) return;

      const data = {
        audio: microphoneDevice.value,
        video: cameraDevice.value,
      };

      if (state.storageKey === 'camera-device') data.video = deviceId;
      if (state.storageKey === 'microphone-device') data.audio = deviceId;

      onDeviceChange(data);
    }));

  };

  return {
    initSetup,
    startWatchers,
    stopDeviceChanges,
    isSetupReady: computed(() => _isSetupReady.value),
  };

};
