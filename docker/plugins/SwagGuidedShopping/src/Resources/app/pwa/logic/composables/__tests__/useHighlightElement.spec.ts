import { ref } from "vue-demi";

import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import {useMercure} from '../useMercure';
jest.mock('../useMercure');
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import {useMercureObserver} from '../useMercureObserver';
jest.mock('../useMercureObserver');
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<typeof useMercureObserver>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import { useErrorHandler } from "../useErrorHandler";
import { useHighlightElement } from "../useHighlightElement";
jest.mock('../useErrorHandler');
const mockedUseErrorHandler = useErrorHandler as jest.Mocked<typeof useErrorHandler>;

describe("Composables - useHighlightElement", () => {
  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const mercurePublishGuideMock = jest.fn();
  const highlightedElementMock = ref<any>(null);

  beforeEach(() => {
    jest.resetAllMocks();

    (mockedUseErrorHandler as any).mockImplementation(() => ({
      handleError: handleErrorMock
    } as any));

    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);

    (mockedUseMercure as any).mockImplementation(
      () =>
        ({
          mercurePublish: {
            guide: mercurePublishGuideMock,
          },
        } as any)
    );
    (mockedUseMercureObserver as any).mockImplementation(
      () =>
        ({
          highlightedElement: highlightedElementMock,
        } as any)
    );

    consoleErrorSpy.mockImplementationOnce(() => {});
  });

  describe("methods", () => {
    describe("publishHighlightElement", () => {
      it("should executed correctly when call publishHighlightElement", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        const { publishHighlightElement } = useHighlightElement('elementId');
        await publishHighlightElement();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('highlighted', 'elementId');
      });

      it("should executed correctly when call publishHighlightElement with elementIsHighlighted false", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        highlightedElementMock.value = 'elementId';
        const { publishHighlightElement } = useHighlightElement('elementId');
        await publishHighlightElement();
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1000);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('highlighted', 'empty');
      });
    });
  });

});
