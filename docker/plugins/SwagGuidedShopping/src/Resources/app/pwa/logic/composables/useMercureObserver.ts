import { useSharedState } from '@shopware-pwa/composables';
import { useMercure } from "./useMercure";
import { onUnmounted, ref } from 'vue-demi';
import {
  InstantListingUpdateData,
  PresentationStateForAll,
  PresentationStateForClients,
  PresentationStateForGuides,
  ScrollToTarget
} from "../interfaces";
import type { DynamicPageResponse, DynamicProductListingPage } from "./useDynamicPage";

const COMPOSABLE_NAME = 'useMercureObserver';
const PRESENTATION_LAYER_NAMESPACE: string = 'presentationLayer';

export const useMercureObserver = () => {
    const contextName = COMPOSABLE_NAME;
    const { mercureSubscribe, mercureUnsubscribe } = useMercure();
    const isSubscribed = ref<boolean>(false);
    const { sharedRef } = useSharedState();

    const highlightedElement = sharedRef<string>(`${contextName}-highlightedElement`);
    const lastGuideHoveredElement = sharedRef<string>(`${contextName}-lastGuideHoveredElement`);
    const lastClientHoveredElement = sharedRef<string>(`${contextName}-lastClientHoveredElement`);
    const lastClientChangedLikes = sharedRef<string>(`${contextName}-lastClientChangedLikes`);
    const likedProduct = sharedRef<string>(`${contextName}-likedProduct`);
    const lastGuideChangedLikes = sharedRef<string>(`${contextName}-lastGuideChangedLikes`);
    const lastClientChangedCart = sharedRef<string>(`${contextName}-lastClientChangedCart`);
    const lastGuideChangedCart = sharedRef<string>(`${contextName}-lastGuideChangedCart`);
    const scrollToTarget = sharedRef<ScrollToTarget>(`${contextName}-scrollToTarget`);
    const navigationPageIndex = sharedRef<number>(`${contextName}-navigationPageIndex`);
    const requestedTokenPermissions = sharedRef<string>(`${contextName}-requestedTokenPermissions`);
    const grantedTokenPermissions = sharedRef<string>(`${contextName}-grantedTokenPermissions`);
    const currentDynamicPage = sharedRef<DynamicPageResponse>(`${contextName}-current-dynamicPage`);
    const stateForAll = sharedRef<PresentationStateForAll>(`${contextName}-stateForAll`);
    const stateForGuides = sharedRef<PresentationStateForGuides>(`${contextName}-stateForGuides`);
    const stateForClients = sharedRef<PresentationStateForClients>(`${contextName}-stateForClients`);
    const newInstantListing = sharedRef<boolean>(`${contextName}-newInstantListing`, false);
    const instantListingUpdated = sharedRef<InstantListingUpdateData>(`${contextName}-instantListingUpdated`);
    const activeSpeaker = sharedRef(`${contextName}-activeSpeaker`);
    const currentLoadedInfo = sharedRef<DynamicProductListingPage>(`${contextName}-currentLoadedInfo`);

    /**
     * registers all event listeners
     * and removes all previously registered listeners
     */
    function subscribeToMercureEvents() {
      if (isSubscribed.value) return;
      isSubscribed.value = true;

      mercureSubscribe('highlighted', (value) => {
        highlightedElement.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('guide.hovered', (value: string) => {
        lastGuideHoveredElement.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('client.hovered', (value: string) => {
        lastClientHoveredElement.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('client.changedLikes', (value: string) => {
        lastClientChangedLikes.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('product.wasLiked', (value: string) => {
        likedProduct.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('guide.changedLikes', (value: string) => {
        lastGuideChangedLikes.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('client.changedCart', (value: string) => {
        lastClientChangedCart.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('guide.changedCart', (value: string) => {
        lastGuideChangedCart.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('scrolledTo', (value) => {
        scrollToTarget.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('navigated', (value) => {
        navigationPageIndex.value = parseInt(value);
        highlightedElement.value = '';
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('requestedTokenPermissions', (value) => {
        requestedTokenPermissions.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('grantedTokenPermissions', (value) => {
        grantedTokenPermissions.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('dynamicPage.opened', (value: DynamicPageResponse) => {
        currentDynamicPage.value = value;
        highlightedElement.value = '';
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('dynamicProductListingPage.loadedMore', (value: DynamicProductListingPage) => {
        if (value.page && value.type) {
          if (value.page > (currentLoadedInfo.value?.page || 0)) {
            currentLoadedInfo.value = value;
          }
        }
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('dynamicPage.closed', (value: DynamicPageResponse) => {
        currentDynamicPage.value = value;
        highlightedElement.value = '';
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('state-for-all', (value) => {
        stateForAll.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('state-for-guides', (value) => {
        stateForGuides.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('state-for-clients', (value) => {
        stateForClients.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('newInstantListingCreated', (value: boolean) => {
        newInstantListing.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('instantListingUpdated', (value: InstantListingUpdateData) => {
        instantListingUpdated.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

      mercureSubscribe('activeSpeakerUpdated', (value: string) => {
        activeSpeaker.value = value;
      }, PRESENTATION_LAYER_NAMESPACE);

    }

    /**
     * removes all event listeners
     */
    function unsubscribeMercureEvents() {
      if (!isSubscribed.value) return;
      isSubscribed.value = false;

      mercureUnsubscribe('highlighted', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('guide.hovered', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('client.hovered', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('client.changedLikes', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('product.wasLiked', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('guide.changedLikes', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('client.changedCart', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('guide.changedCart', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('scrolledTo', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('navigated', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('requestedTokenPermissions', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('grantedTokenPermissions', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('dynamicPage.opened', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('dynamicProductListingPage.loadedMore', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('dynamicPage.closed', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('state-for-all', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('state-for-guides', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('state-for-clients', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('newInstantListingCreated', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('instantListingUpdated', PRESENTATION_LAYER_NAMESPACE);
      mercureUnsubscribe('activeSpeakerUpdated', PRESENTATION_LAYER_NAMESPACE);
    }

    /**
     * remove all subscribed events
     */
    onUnmounted(() => {
      unsubscribeMercureEvents();
    });

    return {
      subscribeToMercureEvents,
      highlightedElement,
      lastGuideHoveredElement,
      lastClientHoveredElement,
      lastClientChangedLikes,
      likedProduct,
      lastGuideChangedLikes,
      lastClientChangedCart,
      lastGuideChangedCart,
      scrollToTarget,
      navigationPageIndex,
      requestedTokenPermissions,
      grantedTokenPermissions,
      currentDynamicPage,
      stateForAll,
      stateForGuides,
      stateForClients,
      newInstantListing,
      instantListingUpdated,
      currentLoadedInfo,
      activeSpeaker
    }

  }
;
