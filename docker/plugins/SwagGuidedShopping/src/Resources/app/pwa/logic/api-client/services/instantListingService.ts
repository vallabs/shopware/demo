import {ShopwareAdminApiInstance} from "../../interfaces";
import {getProductStreamsEndpoint, getSyncProductsEndpoint, getAllProductsWithDataEndpoint, getAllProductIdsEndpoint} from "../endpoints";
import {ProductStreamFilterItem, SelectedFilters} from "../../interfaces/models/instantListing";
import {ShopwareApiInstance} from '@shopware-pwa/shopware-6-client';

export async function getAllProductsWithData(
  criteria: SelectedFilters,
  apiInstance: ShopwareApiInstance,
  control: any = {}
): Promise<any> {
  const newCriteria: any = processCriteria(criteria);

  const productResponse = await apiInstance.invoke.post(getAllProductsWithDataEndpoint(), newCriteria, control);

  return productResponse.data;
}

export async function getAllProductIds(
  criteria: SelectedFilters,
  apiInstance: ShopwareApiInstance,
  control: any = {}
): Promise<any> {
  const newCriteria: any = processCriteria(criteria);

  const productResponse = await apiInstance.invoke.post(getAllProductIdsEndpoint(), newCriteria, control);

  return productResponse.data;
}

export async function getProductStreams(
  criteria,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<ProductStreamFilterItem[]> {
  const productStreamResponse = await adminApiInstance.invoke.post(getProductStreamsEndpoint(), criteria);

  return productStreamResponse.data.data;
}

export async function saveNewInstantListing(
  params: {
    productIds: string[],
    currentPageGroupId: string,
    appointmentIdToSync: string,
    pageName: string,
  },
  apiInstance: ShopwareAdminApiInstance
): Promise<any> {
  let productIds = params.productIds;
  let pageName = params.pageName;
  let currentPageGroupId = params.currentPageGroupId ?? "";

  await apiInstance.invoke.post(getSyncProductsEndpoint(params.appointmentIdToSync),
    {
      productIds,
      currentPageGroupId,
      pageName
    });
}

export async function saveInstantListingUpdate(
  params: {
    addProductIds: string[],
    removeProductIds: string[],
    currentPageGroupId: string,
    appointmentIdToSync: string,
    pageName: string
  },
  apiInstance: ShopwareAdminApiInstance,
  control: any = {}
): Promise<void> {
  return apiInstance.invoke.patch(getSyncProductsEndpoint(params.appointmentIdToSync), params, control);
}

function processCriteria(criteria: SelectedFilters): any
{
  const newCriteria: any = {...criteria};

  if (newCriteria.manufacturer && Array.isArray(newCriteria.manufacturer)) {
    newCriteria.manufacturer = newCriteria.manufacturer.join('|');
  }

  if (newCriteria.properties && Array.isArray(newCriteria.properties)) {
    newCriteria.properties = newCriteria.properties.join('|');
  }

  return newCriteria;
}
