import { ref, onBeforeUnmount } from '@vue/composition-api';
import { useSharedState } from "@shopware-pwa/composables";
import {useAppointment} from './useAppointment';
import {useMercure} from './useMercure';
import {useMercureObserver} from './useMercureObserver';
import {useInteractions} from "./useInteractions";

const COMPOSABLE_NAME = 'useHoverElement';

export const useHoverElement = (elementId: string) => {
  const contextName = COMPOSABLE_NAME;
  const {mercurePublish} = useMercure();
  const {isGuide, isClient} = useAppointment();
  const { publishInteraction } = useInteractions();
  const {lastGuideHoveredElement, lastClientHoveredElement} = useMercureObserver();
  const { sharedRef } = useSharedState();
  const lastElementId = sharedRef(`${contextName}-lastElementId`, null);
  const debounceHover = ref();

  const publishHoverStart = (duration = 2000) => {
    if (lastElementId.value !== elementId) {
      debounceHover.value = setTimeout(() => {
        publishHover();
      }, duration);
    }
  }

  const publishHoverEnd = () => {
    clearDebounceHover();
  }

  onBeforeUnmount(() => {
    clearDebounceHover();
  });

  function clearDebounceHover() {
    if (debounceHover.value) {
      clearTimeout(debounceHover.value);
      debounceHover.value = null;
    }
  }

  // publishes currently hovered element by guide or client
  function publishHover() {
    if (!elementId || (elementId && lastElementId.value === elementId)) return;
    publishGuideHover();
    publishClientHover();
  }

  function publishGuideHover() {
    if (!isGuide.value) return;
    if (lastGuideHoveredElement.value === elementId) return;

    mercurePublish.guide('guide.hovered', elementId);
    publishInteraction({
      name: 'guide.hovered',
        payload: {
          hoveredElementId: elementId,
        }
    });
    lastElementId.value = elementId;
  }

  function publishClientHover() {
    if (!isClient.value) return;
    if (lastClientHoveredElement.value === elementId) return;

    mercurePublish.client('client.hovered', elementId);
    lastElementId.value = elementId;
  }

  return {
    publishHover,
    publishHoverStart,
    publishHoverEnd
  };
};
