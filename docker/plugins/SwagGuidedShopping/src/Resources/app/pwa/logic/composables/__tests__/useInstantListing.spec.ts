import { ref, createApp } from "vue-demi";
import { Product } from "@shopware-pwa/commons";
import {
  ProductStreamFilterItem,
  SelectedFilters,
  InstantListingApiResult,
  PaginationData,
  InstantListingType,
} from "../../interfaces";
import * as Composables from "@shopware-pwa/composables";
jest.mock("@shopware-pwa/composables");
const mockedComposables = Composables as jest.Mocked<typeof Composables>;

import { useAppointment } from "../useAppointment";
jest.mock("../useAppointment");
const mockedUseAppointment = useAppointment as jest.Mocked<
  typeof useAppointment
>;

import { useProductInsights } from "../widgets/useProductInsights";
jest.mock("../widgets/useProductInsights");
const mockedUseProductInsights = useProductInsights as jest.Mocked<
  typeof useProductInsights
>;

import { useMercure } from "../useMercure";
jest.mock("../useMercure");
const mockedUseMercure = useMercure as jest.Mocked<typeof useMercure>;

import { useMercureObserver } from "../useMercureObserver";
jest.mock("../useMercureObserver");
const mockedUseMercureObserver = useMercureObserver as jest.Mocked<
  typeof useMercureObserver
>;

import { useLikeList } from "../useLikeList";
jest.mock("../useLikeList");
const mockedUseLikeList = useLikeList as jest.Mocked<typeof useLikeList>;

import { usePresentation } from "../usePresentation";
jest.mock("../usePresentation");
const mockedUsePresentation = usePresentation as jest.Mocked<
  typeof usePresentation
>;

import { useAdminApiInstance } from "../useAdminApiInstance";
jest.mock("../useAdminApiInstance");
const mockedUseAdminApiInstance = useAdminApiInstance as jest.Mocked<
  typeof useAdminApiInstance
>;

const consoleErrorSpy = jest.spyOn(console, "error");

import { prepareRootContextMock } from "./contextRunner";

import { useErrorHandler } from "../useErrorHandler";
jest.mock("../useErrorHandler");

import { useInstantListing } from "../useInstantListing";

import * as GSApiClient from "../../api-client";
jest.mock("../../api-client");
const mockedGSApiClient = GSApiClient as jest.Mocked<typeof GSApiClient>;

const mockedUseErrorHandler = useErrorHandler as jest.Mocked<
  typeof useErrorHandler
>;

function mockLoadComposableInApp(composable) {
  let result;
  const app = createApp({
    setup() {
      result = composable();
      // suppress missing template warning
      return result;
    },
  });
  const wrapper = app.mount(document.createElement("div"));
  return [result, app, wrapper];
}

const IL_PRODUCT_LIMIT_PER_PAGE = 36;

expect.extend({
  toHaveBeenPrivateCalled(actual, params) {
    if (
      typeof actual !== 'string'
    ) {
      throw new Error('These must be of type string!');
    }
    let pass;
    if (actual === 'scrollLoadInstantListing') {
      pass = params.scrollLoading.value === true;
    }
    if (pass) {
      return {
        message: () =>
          `expected ${actual} to be called`,
        pass: true,
      };
    } else {
      return {
        message: () =>
          `expected ${actual} not to be called`,
        pass: false,
      };
    }
  },
});

declare global {
  namespace jest {
    interface Matchers<R> {
      toHaveBeenPrivateCalled(params: any): R;
    }
  }
}

describe("Composables - useInstantListing", () => {
  let result;
  let app;
  let wrapper;

  const rootContextMock = prepareRootContextMock();
  const handleErrorMock = jest.fn();
  const mercurePublishLocalMock = jest.fn();
  const mercurePublishGuideMock = jest.fn();
  const guideLikeMock = jest.fn();
  const changeProductMock = jest.fn();
  const getAllProductsWithDataMock = jest.fn();
  const saveNewInstantListingMock = jest.fn();
  const removeFromGuideListMock = jest.fn();
  const saveInstantListingUpdateMock = jest.fn();
  const goToSlideAfterReloadMock = jest.fn();
  const getProductStreamsMock = jest.fn();
  const instantListingAccordionMock = ref<any>(null);
  const loadAllProductDataControllerMock = ref<any>(null);
  const loadAllControllerMock = ref<any>(null);
  const instantListingUpdatedMock = ref<any>(null);
  const productOnGuideListMock = ref<any>(null);
  const isInstantListingModalOpenMock = ref<boolean>(false);
  const instantListingModalTypeMock = ref<InstantListingType>("instant-listing");
  const instantListingLoadingMock = ref<boolean>(false);
  const guideProductsLoadingMock = ref<string[]>([]);
  const navigationPageIndexMock = ref(null);
  const instantListingSavingMock = ref<boolean>(false);
  const _storeInstantListingProductsMock = ref<Product[]>([]);
  const _scrollLastPositionMock = ref<string>();
  const _storeProductStreamsMock = ref<ProductStreamFilterItem[]>([]);
  const _storeSelectedProductStreamsMock = ref<ProductStreamFilterItem[]>([]);
  const currentInstantListingIndexMock = ref<number | null>(null);
  const addInstantListingProductIdsMock = ref<string[]>([]);
  const removeInstantListingProductIdsMock = ref<string[]>([]);
  const _storeApiProductsResponseMock = ref<InstantListingApiResult>({});
  const _storeApiAllProductsResponseMock = ref<InstantListingApiResult>({});
  const autoSyncProductsMock = ref<boolean>(false);
  const pageNameMock = ref<string>("");
  const scrollLoadingMock = ref<boolean>(false);
  const scrollPagesCountMock = ref<number>(0);
  const selectedFiltersMock = ref<SelectedFilters>({});
  const indexOfFirstSlideInNextGroupMock = ref(null);
  const adminApiInstanceMock = ref({});
  const _storePaginationDataMock = ref<PaginationData>({
    getLimit: selectedFiltersMock.value?.limit || IL_PRODUCT_LIMIT_PER_PAGE,
    getTotal: selectedFiltersMock.value?.total || 0,
    getCurrentPage: selectedFiltersMock.value?.page || 1,
    getTotalPagesCount:
      Math.ceil(selectedFiltersMock.value.total / selectedFiltersMock.value.limit) || 0,
  });
  const _ilSearchTermMock = ref<string | null>(null);

  beforeEach(() => {
    jest.resetAllMocks();
    instantListingAccordionMock.value = null;
    isInstantListingModalOpenMock.value = false;
    instantListingModalTypeMock.value = "instant-listing";
    instantListingLoadingMock.value = false;
    guideProductsLoadingMock.value = [];
    instantListingSavingMock.value = false;
    _storeInstantListingProductsMock.value = [];
    _storeProductStreamsMock.value = [];
    _storeSelectedProductStreamsMock.value = [];
    currentInstantListingIndexMock.value = null;
    addInstantListingProductIdsMock.value = [];
    removeInstantListingProductIdsMock.value = [];
    navigationPageIndexMock.value = null;
  _storeApiProductsResponseMock.value = {};
    autoSyncProductsMock.value = false;
    pageNameMock.value = "";
    scrollLoadingMock.value = false;
    scrollPagesCountMock.value = 0;
    selectedFiltersMock.value = {};
    _storePaginationDataMock.value = {
      getLimit: selectedFiltersMock.value?.limit || IL_PRODUCT_LIMIT_PER_PAGE,
      getTotal: selectedFiltersMock.value?.total || 0,
      getCurrentPage: selectedFiltersMock.value?.page || 1,
      getTotalPagesCount:
        Math.ceil(selectedFiltersMock.value.total / selectedFiltersMock.value.limit) ||
        0,
    };
    _ilSearchTermMock.value = null;

    // mocking Composables
    mockedComposables.getApplicationContext.mockReturnValue(rootContextMock);
    mockedComposables.useSharedState.mockImplementation(() => {
      return {
        sharedRef: (contextName: string) => {
          if (contextName.includes("apiAllProductsResponse"))
            return _storeApiAllProductsResponseMock;
          if (contextName.includes("-loadAllProductDataController"))
            return loadAllProductDataControllerMock;
          if (contextName.includes("-loadAllController"))
            return loadAllControllerMock;
          if (contextName.includes("instantListingAccordion"))
            return instantListingAccordionMock;
          if (contextName.includes("isInstantListingModalOpen"))
            return isInstantListingModalOpenMock;
          if (contextName.includes("instantListingModalType"))
            return instantListingModalTypeMock;
          if (contextName.includes("instantListingLoading"))
            return instantListingLoadingMock;
          if (contextName.includes("guideProductsLoading"))
            return guideProductsLoadingMock;
          if (contextName.includes("instantListingSaving"))
            return instantListingSavingMock;
          if (contextName.includes("storeInstantListingProducts"))
            return _storeInstantListingProductsMock;
          if (contextName.includes("productStreams"))
            return _storeProductStreamsMock;
          if (contextName.includes("selectedProductStreams"))
            return _storeSelectedProductStreamsMock;
          if (contextName.includes("currentInstantListingPageId"))
            return currentInstantListingIndexMock;
          if (contextName.includes("addInstantListingProductIds"))
            return addInstantListingProductIdsMock;
          if (contextName.includes("removeInstantListingProductIds"))
            return removeInstantListingProductIdsMock;
          if (contextName.includes("apiResponse"))
            return _storeApiProductsResponseMock;
          if (contextName.includes("scrollLastPosition"))
            return _scrollLastPositionMock;
          if (contextName.includes("autoSyncProducts")) return autoSyncProductsMock;
          if (contextName.includes("pageName")) return pageNameMock;
          if (contextName.includes("scrollPagesCount")) return scrollPagesCountMock;
          if (contextName.includes("scrollLoading")) return scrollLoadingMock;
          if (contextName.includes("selectedFilters")) return selectedFiltersMock;
          if (contextName.includes("storePaginationData"))
            return _storePaginationDataMock;
          if (contextName.includes("_ilSearchTerm")) return _ilSearchTermMock;
        },
      } as any;
    });

    (mockedUseAppointment as any).mockImplementation(
      () =>
        ({
          appointmentId: ref(null),
          isSelfService: ref(false),
        } as any)
    );
    (mockedUseErrorHandler as any).mockImplementation(
      () =>
        ({
          handleError: handleErrorMock,
        } as any)
    );
    (mockedUseProductInsights as any).mockImplementation(
      () =>
        ({
          filteredProperties: ref(null),
        } as any)
    );
    (mockedUseMercure as any).mockImplementation(
      () =>
        ({
          mercurePublish: {
            local: mercurePublishLocalMock,
            guide: mercurePublishGuideMock,
          },
        } as any)
    );
    (mockedUseMercureObserver as any).mockImplementation(
      () =>
        ({
          instantListingUpdated: instantListingUpdatedMock,
          navigationPageIndex: navigationPageIndexMock,
        } as any)
    );
    (mockedUseLikeList as any).mockImplementation(
      () =>
        ({
          changeProduct: changeProductMock,
          guideLike: guideLikeMock,
          removeFromGuideList: removeFromGuideListMock,
          productOnGuideList: productOnGuideListMock,
          guideLikeList: ref(null),
        } as any)
    );
    (mockedUsePresentation as any).mockImplementation(
      () =>
        ({
          currentSlideIndex: ref(null),
          navigation: ref(null),
          goToSlideAfterReload: goToSlideAfterReloadMock,
          indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock,
          isProductListing: ref(null),
          listingProductIds: ref(null),
          listingProductStreamId: ref(null),
        } as any)
    );
    (mockedUseAdminApiInstance as any).mockImplementation(
      () =>
        ({
          adminApiInstance: adminApiInstanceMock,
        } as any)
    );

    mockedGSApiClient.InstantListingService = {
      getAllProductsWithData: getAllProductsWithDataMock,
      getProductStreams: getProductStreamsMock,
      saveNewInstantListing: saveNewInstantListingMock,
      saveInstantListingUpdate: saveInstantListingUpdateMock,
    } as any;

    consoleErrorSpy.mockImplementationOnce(() => {});

    if (app) {
      app.unmount();
      result = undefined;
      wrapper = undefined;
    }
  });

  describe("methods", () => {
    describe("createNewInstantListing", () => {
      it("return type instant-listing & run correctly without any parameter", () => {
        const {
          createNewInstantListing,
          ...params
        } = useInstantListing();
        createNewInstantListing();
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.currentInstantListingIndex.value).toEqual(0);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.pageName.value).toEqual('');
        expect(params.instantListingModalType.value).toEqual("instant-listing");
        expect(addInstantListingProductIdsMock.value).toEqual([]);
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
      });

      it("return type guided-product-listing & run correctly when type params is guided-product-listing", () => {
        const {
          createNewInstantListing,
          ...params
        } = useInstantListing();
        createNewInstantListing('guided-product-listing');
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.currentInstantListingIndex.value).toEqual(0);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.pageName.value).toEqual('');
        expect(params.instantListingModalType.value).toEqual("guided-product-listing");
      });
    });

    describe("editInstantListing", () => {
      it('return type instant-listing & run correctly without any parameter', async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(null),
              listingProductStreamId: ref(null),
              currentSlideIndex: ref(1),
              navigation: ref([
                {
                  groupName: 'dummyGroupName'
                }
              ])
            } as any)
        );
        const { editInstantListing, isInstantListingModalOpen, instantListingModalType, currentInstantListingIndex, pageName, ...params } = useInstantListing();
        await editInstantListing();
        expect(isInstantListingModalOpen.value).toEqual(true);
        expect(currentInstantListingIndex.value).toEqual(1);
        expect(instantListingModalType.value).toEqual('instant-listing');
        expect(pageName.value).toEqual('dummyGroupName');
        expect('scrollLoadInstantListing').toHaveBeenPrivateCalled(params);
      });

      it('return type guided-product-listing & run correctly when parameter is guided-product-listing', async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(null),
              listingProductStreamId: ref(null),
              currentSlideIndex: ref(2),
              navigation: ref([
                {
                  groupName: 'dummyGroupName1'
                },
                {
                  groupName: 'dummyGroupName2'
                }
              ])
            } as any)
        );
        const { editInstantListing, ...params } = useInstantListing();
        await editInstantListing('guided-product-listing');
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.instantListingModalType.value).toEqual('guided-product-listing');
        expect(params.currentInstantListingIndex.value).toEqual(2);
        expect(params.pageName.value).toEqual('dummyGroupName2');
        expect('scrollLoadInstantListing').not.toHaveBeenPrivateCalled(params);
      })
    });

    describe("scrollLoadInstantListing", () => {
      it("should run correctly when executed first time with scrollLastPosition = null & listingProductIds is not empty", async () => {
        jest.useFakeTimers();
        jest.spyOn(global, 'setTimeout');
        getAllProductsWithDataMock.mockResolvedValue({
          total: 3,
          elements: [{id: '1'}, {id: '2'}, {id: '3'}]
        });
        _storeInstantListingProductsMock.value = [{id: '1'}, {id: '2'}, {id: '3'} as any];
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref(['1', '2', '3']),
            } as any)
        );
        const {
          scrollLoadInstantListing,
          ...params
        } = useInstantListing();
        await scrollLoadInstantListing();
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith({
          filter: [
            {
              type: 'equalsAny',
              field: 'id',
              value: ["1", "2", "3"]
            }
          ],
          limit: 36,
          p: 1,
        }, rootContextMock.apiInstance);
        expect('scrollLoadInstantListing').toHaveBeenPrivateCalled(params);
        expect(params.currentProducts.value?.length).toEqual(3);
        jest.runAllTimers();
        expect(setTimeout).toHaveBeenCalledTimes(1);
        expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 500);
        expect(params.scrollLoading.value).toEqual(false);
      });

      it("should handle error correctly when API getAllProductsWithData failed - the function executed first time with scrollLastPosition = null & listingProductIds is not empty", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getAllProductsWithDataMock.mockRejectedValueOnce(res);
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref(['id1']),
            } as any)
        );
        _storeInstantListingProductsMock.value = [{id: 'id1'} as any];
        const {
          scrollLoadInstantListing,
        } = useInstantListing();
        await scrollLoadInstantListing();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "loadAllProductData"
          }
        );
      });

      it("should run correctly when executed first time with scrollLastPosition = null & listingProductIds is empty & listingProductStreamId is valid", async () => {
        getProductStreamsMock.mockResolvedValue([{
          apiFilter: [{
            type: 'equalsAny',
            field: 'id',
            value: ['id2', 'id3']
          }]
        }]);
        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{id: 'id2'}, {id: 'id3'}]
        });
        _storeInstantListingProductsMock.value = [{id: 'id2'}, {id: 'id3'} as any];
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref([]),
              listingProductStreamId: ref('id1')
            } as any)
        );
        const {
          scrollLoadInstantListing,
          ...params
        } = useInstantListing();
        await scrollLoadInstantListing();
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith({
          filter: [
            {
              type: 'equalsAny',
              field: 'id',
              value: ['id2', 'id3']
            }
          ],
          limit: 36,
          p: 1,
        }, rootContextMock.apiInstance);
        expect('scrollLoadInstantListing').toHaveBeenPrivateCalled(params);
        expect(params.currentProducts.value?.length).toEqual(2);
      });

      it("should stop when scrollPage = 0 & listingProductIds is empty & listingProductStreamId is valid & streamItem not valid", async () => {
        getProductStreamsMock.mockResolvedValue([]);
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref([]),
              listingProductStreamId: ref('id1')
            } as any)
        );
        const {
          scrollLoadInstantListing,
          ...params
        } = useInstantListing();
        await scrollLoadInstantListing();
        expect(getAllProductsWithDataMock).not.toHaveBeenCalled();
      });

      it("should stop when scrollPage = 0 & listingProductIds is empty & listingProductStreamId is not valid", async () => {
        getProductStreamsMock.mockResolvedValue([]);
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref([]),
              listingProductStreamId: ref(null)
            } as any)
        );
        const {
          scrollLoadInstantListing,
          ...params
        } = useInstantListing();
        await scrollLoadInstantListing();
        expect(getAllProductsWithDataMock).not.toHaveBeenCalled();
      });

      it("run correctly with currentProducts is merged from _storeInstantListingProducts & tempProduct when executed from second time with scrollPage > 0", async () => {
        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{id:1}, {id:2}, {id:3}, {id:4}, {id:5}]
        });
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref([1,2,3,4,5]),
            } as any)
        );
        _storeInstantListingProductsMock.value = [{id:1}, {id:2}, {id:3}, {id:4}, {id:5} as any]
        
        const {
          scrollLoadInstantListing,
          ...params
        } = useInstantListing();
        await scrollLoadInstantListing();
        expect(params.currentProducts.value?.length).toEqual(5);
      });

    });

    describe("openInstantListing", () => {
      it("should run createNewInstantListing when executed without any parameter", () => {
        const {
          openInstantListing,
          ...params
        } = useInstantListing();
        openInstantListing();
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.currentInstantListingIndex.value).toEqual(0);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.pageName.value).toEqual('');
        expect(params.instantListingModalType.value).toEqual("instant-listing");
      });

      it("should run createNewInstantListing when executed with type instant-listing", () => {
        const {
          openInstantListing,
          ...params
        } = useInstantListing();
        openInstantListing(undefined, 'instant-listing');
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.currentInstantListingIndex.value).toEqual(0);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.pageName.value).toEqual('');
        expect(params.instantListingModalType.value).toEqual("instant-listing");
      });

      it("should run editInstantListing when executed with type instant-listing & forceMode edit", () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(null),
              currentSlideIndex: ref(2),
              listingProductStreamId: ref(null),
              isProductListing: ref(null),
              navigation: ref([
                {
                  groupName: 'dummyGroupName1'
                },
                {
                  groupName: 'dummyGroupName2'
                }
              ])
            } as any)
        );
        const {
          openInstantListing,
          ...params
        } = useInstantListing();
        openInstantListing('edit', 'instant-listing');
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.instantListingModalType.value).toEqual('instant-listing');
        expect(params.currentInstantListingIndex.value).toEqual(2);
        expect(params.pageName.value).toEqual('dummyGroupName2');
      });

      it("should run editInstantListing when executed with type instant-listing and isProductListing true from usePresentation", () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(null),
              currentSlideIndex: ref(2),
              listingProductStreamId: ref(null),
              isProductListing: ref(true),
              navigation: ref([
                {
                  groupName: 'dummyGroupName1'
                },
                {
                  groupName: 'dummyGroupName2'
                }
              ])
            } as any)
        );
        const {
          openInstantListing,
          ...params
        } = useInstantListing();
        openInstantListing(undefined, 'instant-listing');
        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.instantListingModalType.value).toEqual('instant-listing');
        expect(params.currentInstantListingIndex.value).toEqual(2);
        expect(params.pageName.value).toEqual('dummyGroupName2');
      });

      it("should run editInstantListing when executed with type guided-product-listing", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(null),
              currentSlideIndex: ref(2),
              listingProductStreamId: ref(null),
              isProductListing: ref(null),
              navigation: ref([
                {
                  groupName: 'dummyGroupName1'
                },
                {
                  groupName: 'dummyGroupName2'
                }
              ])
            } as any)
        );
        const {
          openInstantListing,
          ...params
        } = useInstantListing();
        await openInstantListing(undefined, 'guided-product-listing');

        expect(params.isInstantListingModalOpen.value).toEqual(true);
        expect(params.instantListingModalType.value).toEqual('guided-product-listing');
        expect(params.currentInstantListingIndex.value).toEqual(2);
        expect(params.pageName.value).toEqual('dummyGroupName2');
      });
    });

    describe("closeInstantListing", () => {
      it("should run correctly when executed", () => {
        const {
          closeInstantListing,
          ...params
        } = useInstantListing();
        closeInstantListing();
        expect(params.isInstantListingModalOpen.value).toEqual(false);
        expect(params.instantListingModalType.value).toEqual(null);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.currentInstantListingIndex.value).toEqual(null);
        expect(params.selectedFilters.value).toEqual({});
        expect(addInstantListingProductIdsMock.value).toEqual([]);
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
        expect(_storeSelectedProductStreamsMock.value).toEqual([]);
      });
    });

    describe("changeCurrentSortingOrder", () => {
      it("should run correctly when executed", async () => {
        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{}, {}],
          sorting: "price-asc",
          limit: 10,
          page: 1
        });
        const {
          changeCurrentSortingOrder,
          ...params
        } = useInstantListing();
        await changeCurrentSortingOrder('price-asc');

        expect(params.selectedFilters.value.order).toEqual('price-asc');
        expect(params.ilProducts.value.length).toEqual(2);
        expect(params.selectedFilters.value.p).toEqual(null);
        expect(params.getTotal.value).toEqual(2);
        expect(params.getLimit.value).toEqual(10);
        expect(params.getCurrentPage.value).toEqual(1);
        expect(params.getTotalPagesCount.value).toEqual(1);
      });
    });

    describe("changeCurrentPage", () => {
      it("should run correctly when executed", async () => {
        getAllProductsWithDataMock.mockResolvedValue({
          total: 16,
          elements: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
          limit: 10,
          page: 2
        });
        const {
          changeCurrentPage,
          ...params
        } = useInstantListing();
        await changeCurrentPage(2);

        expect(params.ilProducts.value.length).toEqual(16);
        expect(params.selectedFilters.value.p).toEqual(null);
        expect(params.getTotal.value).toEqual(16);
        expect(params.getLimit.value).toEqual(10);
        expect(params.getCurrentPage.value).toEqual(2);
        expect(params.getTotalPagesCount.value).toEqual(2);
      });
    });

    describe("loadAllProductData", () => {
      it("run correctly when executed and clear search filter when param is true", async () => {
        const {
          loadAllProductData,
          ...params
        } = useInstantListing();

        getAllProductsWithDataMock.mockResolvedValue({
          total: 16,
          elements: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
          limit: 10,
          page: 2
        });
        await loadAllProductData(true);
        expect(params.ilProducts.value.length).toEqual(16);
        expect(params.selectedFilters.value.p).toEqual(null);
        expect(params.selectedFilters.value.query).toEqual(null);
        expect(params.selectedFilters.value.search).toEqual(null);
        expect(params.selectedFilters.value.term).toEqual(null);
        expect(params.getTotal.value).toEqual(16);
        expect(params.getLimit.value).toEqual(10);
        expect(params.getCurrentPage.value).toEqual(2);
        expect(params.getTotalPagesCount.value).toEqual(2);
      });

      it("run correctly when executed and not clear search filter when param is false", async () => {
        const {
          loadAllProductData,
          ...params
        } = useInstantListing();

        getAllProductsWithDataMock.mockResolvedValue({
          total: 16,
          elements: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
          limit: 10,
          page: 2
        });
        await loadAllProductData(false);
        expect(params.selectedFilters.value.query).not.toEqual(null);
        expect(params.selectedFilters.value.search).not.toEqual(null);
        expect(params.selectedFilters.value.term).not.toEqual(null);
        expect(params.ilProducts.value.length).toEqual(16);
        expect(params.selectedFilters.value.p).toEqual(null);
        expect(params.getTotal.value).toEqual(16);
        expect(params.getLimit.value).toEqual(10);
        expect(params.getCurrentPage.value).toEqual(2);
        expect(params.getTotalPagesCount.value).toEqual(2);
      });

      it("run correctly when executed and not clear search filter when param is empty", async () => {
        const {
          loadAllProductData,
          ...params
        } = useInstantListing();

        getAllProductsWithDataMock.mockResolvedValue({
          total: 16,
          elements: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
          limit: 10,
          page: 2
        });
        await loadAllProductData();
        expect(params.selectedFilters.value.query).not.toEqual(null);
        expect(params.selectedFilters.value.search).not.toEqual(null);
        expect(params.selectedFilters.value.term).not.toEqual(null);
        expect(params.ilProducts.value.length).toEqual(16);
        expect(params.selectedFilters.value.p).toEqual(null);
        expect(params.getTotal.value).toEqual(16);
        expect(params.getLimit.value).toEqual(10);
        expect(params.getCurrentPage.value).toEqual(2);
        expect(params.getTotalPagesCount.value).toEqual(2);
      });

      it("should handle error correctly when API getAllProductsWithData failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getAllProductsWithDataMock.mockRejectedValueOnce(res);
        const {
          loadAllProductData,
        } = useInstantListing();
        await loadAllProductData();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "loadAllProductData"
          }
        );
      });
    });

    describe("loadProductStreams", () => {
      it("run correctly when executed", async () => {
        const {
          loadProductStreams,
          ...params
        } = useInstantListing();

        getProductStreamsMock.mockResolvedValue({
          id: '',
          name: 'dummyName',
        });
        await loadProductStreams();
        expect(params.productStreams.value).toEqual({
          id: '',
          name: 'dummyName',
        });
      });

      it("should handle error correctly when API getProductStreams failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getProductStreamsMock.mockRejectedValueOnce(res);
        const {
          loadProductStreams,
        } = useInstantListing();
        await loadProductStreams();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "loadProductStreams"
          }
        );
      });
    });

    describe("getSearchedProducts", () => {
      it("run correctly when executed", async () => {
        const {
          getSearchedProducts,
          ...params
        } = useInstantListing();

        getAllProductsWithDataMock.mockResolvedValue({
          total: 16,
          elements: [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}],
          limit: 10,
          page: 2
        });
        await getSearchedProducts('dummySearch');
        expect(params.selectedFilters.value.query).toEqual('dummySearch');
        expect(params.ilSearchTerm.value).toEqual('dummySearch');
        expect(params.selectedFilters.value.search).toEqual('dummySearch');
        expect(params.selectedFilters.value.term).toEqual('dummySearch');
        expect(params.ilProducts.value.length).toEqual(16);
        expect(params.selectedFilters.value.p).toEqual(null);
        expect(params.getTotal.value).toEqual(16);
        expect(params.getLimit.value).toEqual(10);
        expect(params.getCurrentPage.value).toEqual(2);
        expect(params.getTotalPagesCount.value).toEqual(2);
      });

      it("should handle error correctly when API getProductStreams failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getAllProductsWithDataMock.mockRejectedValueOnce(res);
        const {
          getSearchedProducts,
        } = useInstantListing();
        await getSearchedProducts('dummySearch');
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "getSearchedProducts"
          }
        );
      });
    });

    describe("isProductSelected", () => {
      it("should return false when product existed in removeInstantListingProductIds", async () => {
        const {
          isProductSelected,
          ...params
        } = useInstantListing();
        removeInstantListingProductIdsMock.value = ['dummyProductId'];
        const res = isProductSelected('dummyProductId');
        expect(res).toEqual(false);
      });

      it("should return false when product not existed in both listingProductIds & addInstantListingProductIds", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref(['id1', 'id2']),
            } as any)
        );

        const {
          isProductSelected,
          ...params
        } = useInstantListing();

        addInstantListingProductIdsMock.value = ['id3'];
        const res = isProductSelected('dummyProductId');
        expect(res).toEqual(false);
      });

      it("should return false when product existed in listingProductIds, but not existed addInstantListingProductIds & modal listing is creation mode", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref(['id1', 'id2', 'dummyProductId']),
            } as any)
        );
        const {
          isProductSelected,
          ...params
        } = useInstantListing();
        currentInstantListingIndexMock.value = 0;
        addInstantListingProductIdsMock.value = ['id3'];
        const res = isProductSelected('dummyProductId');
        expect(res).toEqual(false);
      });

      it("should return true when product existed either in listingProductIds or in addInstantListingProductIds & modal listing is edit mode", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref(['id1', 'id2', 'dummyProductId']),
            } as any)
        );
        const {
          isProductSelected,
          ...params
        } = useInstantListing();
        currentInstantListingIndexMock.value = 1;
        addInstantListingProductIdsMock.value = ['id3'];
        const res = isProductSelected('dummyProductId');
        expect(res).toEqual(true);
      });

      it("should return true when product existed in addInstantListingProductIds & modal listing is creation mode", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductStreamId: ref(null),
              listingProductIds: ref([]),
            } as any)
        );
        currentInstantListingIndexMock.value = 0;
        const {
          isProductSelected,
        } = useInstantListing();
        addInstantListingProductIdsMock.value = ['id1', 'id2', 'dummyProductId'];
        const res = isProductSelected('dummyProductId');
        expect(res).toEqual(true);
      });

      it("should return true when productId is included in list & modal type guided-product-listing", async () => {
        instantListingModalTypeMock.value = 'guided-product-listing';
        _storeInstantListingProductsMock.value = [{id: 'id1'}, {id: 'id2'} as any]
        const {
          isProductSelected,
        } = useInstantListing();
        const res = isProductSelected('id1');
        expect(res).toEqual(true);
      });

      it("should return false when productId is not included in list & modal type guided-product-listing", async () => {
        instantListingModalTypeMock.value = 'guided-product-listing';
        _storeInstantListingProductsMock.value = [{id: 'id1'}, {id: 'id2'} as any]
        const {
          isProductSelected,
        } = useInstantListing();
        const res = isProductSelected('dddd');
        expect(res).toEqual(false);
      });
    });

    describe("addAllProducts", () => {
      it ('should not do anything when allProductsCanBeAdded empty', async () => {
        _storeApiAllProductsResponseMock.value = {
          elements: []
        };
        const {
          addAllProducts,
        } = useInstantListing();
        addAllProducts();
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
        expect(addInstantListingProductIdsMock.value).toEqual([]);
      });

      it ('should work correctly when allProductsCanBeAdded have value & listingProductIds empty', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: ['1', '2']
        } as any;
        const {
          addAllProducts,
        } = useInstantListing();
        addAllProducts();
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
        expect(addInstantListingProductIdsMock.value).toEqual(['1', '2']);
      });

      it ('should work correctly when allProductsCanBeAdded have value & listingProductIds have value', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: ['1', '2', '3']
        } as any;
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(['1']),
            } as any)
        );
        currentInstantListingIndexMock.value = 1;
        const {
          allProductsCanBeAdded,
          addAllProducts,
        } = useInstantListing();
        expect(allProductsCanBeAdded.value).toEqual(['2', '3']);
        addAllProducts();
        expect(addInstantListingProductIdsMock.value).toEqual(['2', '3']);
      });

      it ('should work correctly when allProductsCanBeAdded have value & listingProductIds have value & removeInstantListingProductIds have value', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: ['1', '2', '3']
        } as any;
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(['1', '2']),
            } as any)
        );
        currentInstantListingIndexMock.value = 1;
        removeInstantListingProductIdsMock.value = ['2'];
        const {
          allProductsCanBeAdded,
          addAllProducts,
        } = useInstantListing();
        expect(allProductsCanBeAdded.value).toEqual(['2', '3']);
        addAllProducts();
        expect(addInstantListingProductIdsMock.value).toEqual(['3']);
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
      })
    });

    describe("removeAddedProducts", () => {
      it ('should not do anything when allProductsCanBeRemoved empty', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: []
        } as any;
        const {
          removeAddedProducts,
        } = useInstantListing();
        removeAddedProducts();
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
        expect(addInstantListingProductIdsMock.value).toEqual([]);
      });

      it ('should work correctly when allProductsCanBeRemoved have value & listingProductIds empty', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: ['1', '2', '3']
        } as any;
        const {
          removeAddedProducts,
        } = useInstantListing();
        removeAddedProducts();
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
      });

      it ('should work correctly when allProductsCanBeRemoved have value & listingProductIds have value', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: ['1', '2', '3']
        } as any;
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(['1', '2', '3']),
            } as any)
        );
        const {
          removeAddedProducts,
          allProductsCanBeRemoved,
        } = useInstantListing();
        expect(allProductsCanBeRemoved.value).toEqual(['1', '2', '3']);
        removeAddedProducts();
        expect(removeInstantListingProductIdsMock.value).toEqual(['1', '2', '3']);
      });

      it ('should work correctly when allProductsCanBeRemoved have value & listingProductIds have value & addInstantListingProductIds have value', async () => {
        _storeApiAllProductsResponseMock.value = {
          ids: ['1', '2', '3']
        } as any;
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              listingProductIds: ref(['1']),
            } as any)
        );
        addInstantListingProductIdsMock.value = ['2'];
        const {
          removeAddedProducts,
          allProductsCanBeRemoved,
        } = useInstantListing();
        expect(allProductsCanBeRemoved.value).toEqual(['1', '2']);
        removeAddedProducts();
        expect(removeInstantListingProductIdsMock.value).toEqual(['1']);
        expect(addInstantListingProductIdsMock.value).toEqual([]);
      });
    });

    describe("removeSelectedFilters", () => {
      it("should clear selectedFilters after executed", async () => {
        const {
          removeSelectedFilters,
          ...params
        } = useInstantListing();
        removeSelectedFilters();
        expect(params.selectedFilters.value).toEqual({
          interaction: true,
          limit: 36,
        })
      });
    });

    describe("saveNewInstantListing", () => {
      it("should run with goToSlideAfterReload, publish mercure local when executed with navigate true & isSelfService true", async () => {
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              isSelfService: ref(true),
              appointmentId: ref('dummyAppointmentId'),
            } as any)
        );
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              listingProductIds: ref(null),
              currentSlideIndex: ref(1),
              listingProductStreamId: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        pageNameMock.value = "dummyPageName";
        _storeInstantListingProductsMock.value = [{id: 'id1'}, {id: 'id2'} as any]
        const {
          saveNewInstantListing,
        } = useInstantListing();
        await saveNewInstantListing(true);
        expect(saveNewInstantListingMock).toHaveBeenCalledWith({
          productIds: ['id1', 'id2'],
          currentPageGroupId: 'dummyGroupId',
          appointmentIdToSync: 'dummyAppointmentId',
          pageName: 'dummyPageName',
        }, adminApiInstanceMock)
        expect(goToSlideAfterReloadMock).toHaveBeenCalledWith(indexOfFirstSlideInNextGroupMock.value);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('newInstantListingCreated', true);
      });

      it("should run with goToSlideAfterReload, publish mercure guide when executed with navigate false but indexOfFirstSlideInNextGroup = 1 & isSelfService false", async () => {
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              appointmentId: ref('dummyAppointmentId'),
              isSelfService: ref(false),
            } as any)
        );
        indexOfFirstSlideInNextGroupMock.value = 1;
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              listingProductIds: ref(null),
              listingProductStreamId: ref(null),
              currentSlideIndex: ref(1),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        pageNameMock.value = "dummyPageName";
        _storeInstantListingProductsMock.value = [{id: 'id1'}, {id: 'id2'} as any]
        const {
          saveNewInstantListing,
        } = useInstantListing();
        await saveNewInstantListing();
        expect(saveNewInstantListingMock).toHaveBeenCalledWith({
          productIds: ['id1', 'id2'],
          currentPageGroupId: 'dummyGroupId',
          appointmentIdToSync: 'dummyAppointmentId',
          pageName: 'dummyPageName',
        }, adminApiInstanceMock)
        expect(goToSlideAfterReloadMock).toHaveBeenCalledWith(indexOfFirstSlideInNextGroupMock.value);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('newInstantListingCreated', true);
      });

      it("should always run with closeInstantListing after executed", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              currentSlideIndex: ref(1),
              listingProductStreamId: ref(null),
              listingProductIds: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        const {
          saveNewInstantListing,
          ...params
        } = useInstantListing();
        await saveNewInstantListing();
        expect(params.isInstantListingModalOpen.value).toEqual(false);
        expect(params.instantListingModalType.value).toEqual(null);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.currentInstantListingIndex.value).toEqual(null);
        expect(params.selectedFilters.value).toEqual({});
        expect(addInstantListingProductIdsMock.value).toEqual([]);
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
        expect(_storeSelectedProductStreamsMock.value).toEqual([]);
      });

      it("should handle error correctly when API saveNewInstantListing failed", async () => {
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              currentSlideIndex: ref(1),
              listingProductStreamId: ref(null),
              listingProductIds: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        saveNewInstantListingMock.mockRejectedValueOnce(res);
        const {
          saveNewInstantListing,
        } = useInstantListing();
        await saveNewInstantListing();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "saveNewInstantListing"
          }
        );
      });

    });

    describe("saveInstantListingUpdate", () => {
      it("should run with saveInstantListingUpdate API, publish mercure local when executed with isSelfService true", async () => {
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              isSelfService: ref(true),
              appointmentId: ref('dummyAppointmentId'),
            } as any)
        );
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              currentSlideIndex: ref(1),
              listingProductIds: ref(null),
              listingProductStreamId: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        currentInstantListingIndexMock.value = 1;
        pageNameMock.value = "dummyPageName";
        addInstantListingProductIdsMock.value = ['id3'];
        removeInstantListingProductIdsMock.value = ['id1', 'id2'];

        const {
          saveInstantListingUpdate,
        } = useInstantListing();
        await saveInstantListingUpdate({close: false});
        expect(saveInstantListingUpdateMock).toHaveBeenCalledWith({
          addProductIds: ['id3'],
          removeProductIds: ['id1', 'id2'],
          currentPageGroupId: 'dummyGroupId',
          appointmentIdToSync: 'dummyAppointmentId',
          pageName: 'dummyPageName'
        }, adminApiInstanceMock);
        expect(mercurePublishLocalMock).toHaveBeenCalledWith('instantListingUpdated', {
          updated: true,
          currentIlIndex: 0
        });
      });

      it("should run with saveInstantListingUpdate API & close listing & publish mercure guide when executed with isSelfService false & close true", async () => {
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              isSelfService: ref(false),
              appointmentId: ref('dummyAppointmentId'),
            } as any)
        );
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              currentSlideIndex: ref(1),
              listingProductStreamId: ref(null),
              listingProductIds: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        currentInstantListingIndexMock.value = 1;
        pageNameMock.value = "dummyPageName";
        addInstantListingProductIdsMock.value = ['id3'];
        removeInstantListingProductIdsMock.value = ['id1', 'id2'];

        const {
          saveInstantListingUpdate,
          ...params
        } = useInstantListing();
        await saveInstantListingUpdate({close: true});
        expect(saveInstantListingUpdateMock).toHaveBeenCalledWith({
          addProductIds: ['id3'],
          removeProductIds: ['id1', 'id2'],
          currentPageGroupId: 'dummyGroupId',
          appointmentIdToSync: 'dummyAppointmentId',
          pageName: 'dummyPageName'
        }, adminApiInstanceMock);
        expect(mercurePublishGuideMock).toHaveBeenCalledWith('instantListingUpdated', {
          updated: true,
          currentIlIndex: 0
        });
        expect(params.isInstantListingModalOpen.value).toEqual(false);
        expect(params.instantListingModalType.value).toEqual(null);
        expect(params.currentProducts.value).toEqual([]);
        expect(params.currentInstantListingIndex.value).toEqual(null);
        expect(params.selectedFilters.value).toEqual({});
        expect(addInstantListingProductIdsMock.value).toEqual([]);
        expect(removeInstantListingProductIdsMock.value).toEqual([]);
        expect(_storeSelectedProductStreamsMock.value).toEqual([]);
      });

      it("should handle error correctly when API saveNewInstantListing failed", async () => {
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              isSelfService: ref(false),
              appointmentId: ref('dummyAppointmentId'),
            } as any)
        );
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              currentSlideIndex: ref(1),
              listingProductStreamId: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              listingProductIds: ref(null),
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        currentInstantListingIndexMock.value = 1;
        pageNameMock.value = "dummyPageName";
        addInstantListingProductIdsMock.value = ['id3'];
        removeInstantListingProductIdsMock.value = ['id1', 'id2'];
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        saveInstantListingUpdateMock.mockRejectedValueOnce(res);
        const {
          saveInstantListingUpdate,
        } = useInstantListing();
        await saveInstantListingUpdate({ close: false });
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "saveInstantListingUpdate"
          }
        );
      });
    });

    describe("toggleProductStreamSelection", () => {
      it("should run with stream & getAllProductsWithData API, when streamId not existed in _storeSelectedProductStreams", async () => {
        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{}, {}]
        });

        const {
          toggleProductStreamSelection,
        } = useInstantListing();
        await toggleProductStreamSelection({ id: 'dummyStreamId' });
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith(
          {
            p: null,
            streams: ['dummyStreamId'],
          },
          rootContextMock.apiInstance,
          expect.anything()
        );
      });

      it("should run with stream & getAllProductsWithData API, when streamId not existed in _storeSelectedProductStreams", async () => {
        _storeSelectedProductStreamsMock.value = [{id: 'dummyStreamId'} as any]

        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{}, {}]
        });

        const {
          toggleProductStreamSelection,
        } = useInstantListing();
        await toggleProductStreamSelection({ id: 'dummyStreamId' });
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith(
          {
            p: null,
            streams: [],
          }, rootContextMock.apiInstance,
          expect.anything()
        );
      });

      it("should handle error correctly when API getAllProductsWithData failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getAllProductsWithDataMock.mockRejectedValueOnce(res);
        const {
          toggleProductStreamSelection,
        } = useInstantListing();
        await toggleProductStreamSelection({ id: 'dummyStreamId' });
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "toggleProductStreamSelection"
          }
        );
      });
    });

    describe("toggleAutoProductSync", () => {
      it("should change autoSyncProducts to true & saveNewInstantListing when autoSyncProducts false & isCreationMode true", async () => {
        autoSyncProductsMock.value = false;
        currentInstantListingIndexMock.value = 0;
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              isSelfService: ref(true),
              appointmentId: ref('dummyAppointmentId'),
            } as any)
        );
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              currentSlideIndex: ref(1),
              listingProductStreamId: ref(null),
              listingProductIds: ref(null),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        pageNameMock.value = "dummyPageName";
        _storeInstantListingProductsMock.value = [{id: 'id1'}, {id: 'id2'} as any]
        const {
          toggleAutoProductSync,
        } = useInstantListing();
        toggleAutoProductSync();
        expect(autoSyncProductsMock.value).toEqual(true);
        expect(saveNewInstantListingMock).toHaveBeenCalledWith({
          productIds: ['id1', 'id2'],
          currentPageGroupId: 'dummyGroupId',
          appointmentIdToSync: 'dummyAppointmentId',
          pageName: 'dummyPageName',
        }, adminApiInstanceMock)
      });

      it("should change autoSyncProducts to true & saveInstantListingUpdate when autoSyncProducts false & isCreationMode false", async () => {
        autoSyncProductsMock.value = false;
        (mockedUseAppointment as any).mockImplementation(
          () =>
            ({
              isSelfService: ref(true),
              appointmentId: ref('dummyAppointmentId'),
            } as any)
        );
        (mockedUsePresentation as any).mockImplementation(
          () =>
            ({
              navigation: ref([
                {
                  groupId: 'dummyGroupId'
                }
              ]),
              listingProductIds: ref(null),
              currentSlideIndex: ref(1),
              goToSlideAfterReload: goToSlideAfterReloadMock,
              listingProductStreamId: ref(null),
              indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
            } as any)
        );
        currentInstantListingIndexMock.value = 1;
        pageNameMock.value = "dummyPageName";
        addInstantListingProductIdsMock.value = ['id3'];
        removeInstantListingProductIdsMock.value = ['id1', 'id2'];

        const {
          toggleAutoProductSync,
        } = useInstantListing();
        toggleAutoProductSync();
        expect(autoSyncProductsMock.value).toEqual(true);
        expect(saveInstantListingUpdateMock).toHaveBeenCalledWith({
          addProductIds: ['id3'],
          removeProductIds: ['id1', 'id2'],
          currentPageGroupId: 'dummyGroupId',
          appointmentIdToSync: 'dummyAppointmentId',
          pageName: 'dummyPageName'
        }, adminApiInstanceMock);
      });

      it("should change autoSyncProducts to false when autoSyncProducts true", async () => {
        autoSyncProductsMock.value = true;
        const {
          toggleAutoProductSync,
        } = useInstantListing();
        toggleAutoProductSync();
        expect(autoSyncProductsMock.value).toEqual(false);
      });
    });

    describe("searchProductsByInsightsFilters", () => {
      it("should run correctly with getAllProductsWithData API", async () => {
        (mockedUseProductInsights as any).mockImplementation(
          () =>
            ({
              filteredProperties: ref([
                {id: 'property1'},
                {id: 'property2'},
              ]),
            } as any)
        );
        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{}, {}]
        });
        const {
          searchProductsByInsightsFilters,
        } = useInstantListing();
        await searchProductsByInsightsFilters();
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith({
          p: null,
          properties: ['property1', 'property2'],
        }, rootContextMock.apiInstance, expect.anything());
      });
    });

    describe("applyFilters", () => {
      it("should run correctly with getAllProductsWithData API", async () => {
        selectedFiltersMock.value = {
          limit: 10,
        }
        getAllProductsWithDataMock.mockResolvedValue({
          total: 2,
          elements: [{}, {}]
        });
          const {
          applyFilters,
        } = useInstantListing();
        await applyFilters();
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith({
          limit: 10,
          p: null,
        }, rootContextMock.apiInstance, expect.anything());
      });

      it("should handle error correctly when API getAllProductsWithData failed", async () => {
        const res = {
          message: 'something went wrong',
          statusCode: 400
        }
        getAllProductsWithDataMock.mockRejectedValueOnce(res);
        const {
          applyFilters,
        } = useInstantListing();
        await applyFilters();
        expect(handleErrorMock).toHaveBeenCalledWith(
          res,
          {
            context: "useInstantListing",
            method: "applyFilters"
          }
        );
      });
    });

    describe("toggleFilterValue", () => {
      it("run correctly when executed", async () => {
        selectedFiltersMock.value = {}
        const {
          toggleFilterValue,
        } = useInstantListing();

        getProductStreamsMock.mockResolvedValue({
          id: '',
          name: 'dummyName',
        });
        await toggleFilterValue({
          code: "properties",
          label: "Size",
          value: "358a5839d6c3446bbca1430c5cf64f01",
        });
        expect(selectedFiltersMock.value).toEqual({
          p: null,
          properties: ["358a5839d6c3446bbca1430c5cf64f01"],
        });
        expect(getAllProductsWithDataMock).toHaveBeenCalledWith({
          p: null,
          properties: ["358a5839d6c3446bbca1430c5cf64f01"],
        }, rootContextMock.apiInstance, expect.anything());
      });
    });

    describe("removeProductForGuidedProductListing", () => {
      it("should run correctly when executed", async () => {
        const product = {
          id: 'dummyProductId'
        };
        _storeInstantListingProductsMock.value = [
          {id: 'id1'} as any,
          {id: 'id2'} as any,
          {id: 'dummyProductId'} as any
        ];
        const {
          removeProductForGuidedProductListing,
        } = useInstantListing();
        await removeProductForGuidedProductListing(product);
        expect(changeProductMock).toHaveBeenCalled();
        expect(removeFromGuideListMock).toHaveBeenCalled();
        expect(_storeInstantListingProductsMock.value).toEqual([
          {id: 'id1'},
          {id: 'id2'},
        ]);
      });
    });

    describe("addProductForGuidedProductListing", () => {
      it("should run correctly when executed", async () => {
        const product = {
          id: 'dummyProductId'
        };
        _storeInstantListingProductsMock.value = [
          {id: 'id1'} as any,
          {id: 'id2'} as any,
        ];
        const {
          addProductForGuidedProductListing,
        } = useInstantListing();
        await addProductForGuidedProductListing(product);
        expect(changeProductMock).toHaveBeenCalledWith(product);
        expect(guideLikeMock).toHaveBeenCalled();
        expect(_storeInstantListingProductsMock.value).toEqual([
          {id: 'id1'},
          {id: 'id2'},
          {id: 'dummyProductId'},
        ]);
      });
    });

    describe("toggleProduct", () => {
      describe("with instantListingModalType instant-listing", () => {
        beforeEach(() => {
          instantListingModalTypeMock.value = 'instant-listing';
        });

        it("should select product when product is not selected & forcedValue null", async () => {
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductStreamId: ref(null),
                listingProductIds: ref(['id1', 'id2']),
              } as any)
          );

          addInstantListingProductIdsMock.value = ['id3'];
          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
            // loadProductsFromStorage,
            ...params
          } = useInstantListing();
          await toggleProduct(product);
          expect(_storeInstantListingProductsMock.value).toEqual([
            {
              data: {
                id: "dummyProductId",
              },
              id: "dummyProductId",
              loaded: true,
            }
          ]);
        });

        it("should do nothing when product is selected & forcedValue true", async () => {
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductStreamId: ref(null),
                listingProductIds: ref(['id1', 'id2']),
              } as any)
          );

          addInstantListingProductIdsMock.value = ['dummyProductId'];
          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
            // loadProductsFromStorage,
            ...params
          } = useInstantListing();
          await toggleProduct(product, true);
          expect(_storeInstantListingProductsMock.value).toEqual([]);
        });

        it("should add product into addInstantListingProductIds when product is not selected & forcedValue null & editMode true & product not existed in removeInstantListingProductIds", async () => {
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductStreamId: ref(null),
                listingProductIds: ref(['id1', 'id2']),
              } as any)
          );

          addInstantListingProductIdsMock.value = ['id3'];
          removeInstantListingProductIdsMock.value = [];
          currentInstantListingIndexMock.value = 1;
          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
            // loadProductsFromStorage,
            ...params
          } = useInstantListing();
          await toggleProduct(product);
          expect(addInstantListingProductIdsMock.value).toEqual([
            'id3', 'dummyProductId'
          ]);
        });

        it("should remove product from removeInstantListingProductIds when product is not selected & forcedValue null & editMode true & product existed in removeInstantListingProductIds", async () => {
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductStreamId: ref(null),
                listingProductIds: ref(['id1', 'id2']),
              } as any)
          );

          currentInstantListingIndexMock.value = 1;
          addInstantListingProductIdsMock.value = ['id3'];
          removeInstantListingProductIdsMock.value = ['dummyProductId'];
          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
            // loadProductsFromStorage,
          } = useInstantListing();
          await toggleProduct(product);
          expect(addInstantListingProductIdsMock.value).toEqual(['id3']);
          expect(removeInstantListingProductIdsMock.value).toEqual([]);
        });

        it("should remove product when product already selected & forcedValue null", async () => {
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductStreamId: ref(null),
                listingProductIds: ref(['dummyProductId', 'id2']),
              } as any)
          );
          _storeInstantListingProductsMock.value = [{id: 'dummyProductId'}, {id: 'id2'} as any]

          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
          } = useInstantListing();
          await toggleProduct(product);
          expect(_storeInstantListingProductsMock.value).toEqual([
            {
              id: 'id2'
            }
          ]);
        });

        it("should add product into removeInstantListingProductIds when product already selected & forcedValue null & editMode true & product not existed in addInstantListingProductIds", async () => {
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductStreamId: ref(null),
                listingProductIds: ref(['dummyProductId', 'id2']),
              } as any)
          );
          currentInstantListingIndexMock.value = 1;
          _storeInstantListingProductsMock.value = [{id: 'dummyProductId'}, {id: 'id2'} as any]
          addInstantListingProductIdsMock.value = [];
          removeInstantListingProductIdsMock.value = ['id1']
          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
          } = useInstantListing();
          await toggleProduct(product);
          expect(_storeInstantListingProductsMock.value).toEqual([{ id: 'id2' }]);
          expect(removeInstantListingProductIdsMock.value).toEqual(['id1', 'dummyProductId']);
        });

        it("should remove product from addInstantListingProductIds when product already selected & forcedValue null & editMode true & product existed in addInstantListingProductIds", async () => {
          (mockedUseAppointment as any).mockImplementation(
            () =>
              ({
                isSelfService: ref(true),
                appointmentId: ref('dummyAppointmentId'),
              } as any)
          );
          (mockedUsePresentation as any).mockImplementation(
            () =>
              ({
                listingProductIds: ref(['dummyProductId', 'id2']),
                listingProductStreamId: ref(null),
                navigation: ref([
                  {
                    groupId: 'dummyGroupId'
                  }
                ]),
                currentSlideIndex: ref(1),
                goToSlideAfterReload: goToSlideAfterReloadMock,
                indexOfFirstSlideInNextGroup: indexOfFirstSlideInNextGroupMock
              } as any)
          );
          currentInstantListingIndexMock.value = 1;
          pageNameMock.value = "dummyPageName";
          autoSyncProductsMock.value = true;
          currentInstantListingIndexMock.value = 1;
          _storeInstantListingProductsMock.value = [{id: 'dummyProductId'}, {id: 'id2'} as any]
          addInstantListingProductIdsMock.value = ['dummyProductId'];
          removeInstantListingProductIdsMock.value = ['id1']
          const product = {
            id: 'dummyProductId'
          };
          const {
            toggleProduct,
          } = useInstantListing();
          await toggleProduct(product);
          expect(_storeInstantListingProductsMock.value).toEqual([{ id: 'id2' }]);
          expect(addInstantListingProductIdsMock.value).toEqual([]);
          expect(removeInstantListingProductIdsMock.value).toEqual(['id1']);
          expect(saveInstantListingUpdateMock).toHaveBeenCalledWith({
            addProductIds: [],
            removeProductIds: ['id1'],
            currentPageGroupId: 'dummyGroupId',
            appointmentIdToSync: 'dummyAppointmentId',
            pageName: 'dummyPageName'
          }, adminApiInstanceMock);
        });
      });
      describe("with instantListingModalType guided-product-listing", () => {
        beforeEach(() => {
          instantListingModalTypeMock.value = 'guided-product-listing';
        });

        it("should call once when trigger many toggleProduct at the same time", async () => {
          _storeInstantListingProductsMock.value = [
            {id: 'id1'} as any,
            {id: 'id2'} as any,
            {id: 'dummyProductId'} as any
          ];
          const product = {
            id: 'dummyProductId'
          };
          productOnGuideListMock.value = true;
          const {
            toggleProduct,
          } = useInstantListing();
          await Promise.all([
            toggleProduct(product),
            toggleProduct(product)
          ]);
          expect(changeProductMock).toHaveBeenCalled();
          expect(removeFromGuideListMock).toHaveBeenCalledTimes(1);
        });

        it("should remove product when product is in GuideList & forcedValue null", async () => {
          _storeInstantListingProductsMock.value = [
            {id: 'id1'} as any,
            {id: 'id2'} as any,
            {id: 'dummyProductId'} as any
          ];
          const product = {
            id: 'dummyProductId'
          };
          productOnGuideListMock.value = true;
          const {
            toggleProduct,
            ...params
          } = useInstantListing();
          await toggleProduct(product);

          expect(changeProductMock).toHaveBeenCalled();
          expect(removeFromGuideListMock).toHaveBeenCalled();
          expect(_storeInstantListingProductsMock.value).toEqual([
            {id: 'id1'},
            {id: 'id2'},
          ]);
        });

        it("should add product when product was not existed in GuideList & forcedValue null", async () => {
          _storeInstantListingProductsMock.value = [
            {id: 'id1'} as any,
            {id: 'id2'} as any,
          ];
          const product = {
            id: 'dummyProductId'
          };
          productOnGuideListMock.value = false;
          const {
            toggleProduct,
            ...params
          } = useInstantListing();
          await toggleProduct(product);

          expect(changeProductMock).toHaveBeenCalledWith(product);
          expect(guideLikeMock).toHaveBeenCalled();
          expect(_storeInstantListingProductsMock.value).toEqual([
            {id: 'id1'},
            {id: 'id2'},
            {id: 'dummyProductId'},
          ]);
        });
      });
    });

    describe("loadProductsFromStorage", () => {
      it("should return when LOCAL_STORAGE_PRODUCT_KEY not have value in sessionStorage", async () => {
        sessionStorage.removeItem('useInstantListing-instantListingProductIds');
        _storeInstantListingProductsMock.value = [];
        const {
          loadProductsFromStorage
        } = useInstantListing();
        const res = loadProductsFromStorage();
        expect(res).toBeUndefined();
        expect(_storeInstantListingProductsMock.value).toEqual([]);
      });

      it("should update _storeInstantListingProducts when LOCAL_STORAGE_PRODUCT_KEY contains products in sessionStorage", async () => {
        _storeInstantListingProductsMock.value = [];
        currentInstantListingIndexMock.value = 0;
        sessionStorage.setItem('useInstantListing-instantListingProductIds','1,2');
        sessionStorage.setItem('useInstantListing-instantListingMode', 'create');
        const {
          loadProductsFromStorage
        } = useInstantListing();
        loadProductsFromStorage();
        expect(_storeInstantListingProductsMock.value).toEqual([{ "id": "1" }, { "id": "2" }]);
      });
    });

  });

  describe("computed", () => {
    describe("ilProductCurrentFilters", () => {
      it("should not return the field navigationId, p, and other fields which don't have value", () => {
        _storeApiProductsResponseMock.value = {
          currentFilters: {
            test: null,
            navigationId: '1',
            p: 1
          }
        } as any;
        const {
          ilProductCurrentFilters
        } = useInstantListing();
        expect(ilProductCurrentFilters.value).toEqual({});
      });

      it("should convert the field price to min-price, max-price", () => {
        _storeApiProductsResponseMock.value = {
          currentFilters: {
            price: {
              min: 10,
              max: 20
            },
          }
        } as any;
        const {
          ilProductCurrentFilters
        } = useInstantListing();
        expect(ilProductCurrentFilters.value).toEqual({
          'min-price': 10,
          'max-price': 20,
        });
      });

      it("should return all other fields which is not in special field list & have value", () => {
        _storeApiProductsResponseMock.value = {
          currentFilters: {
            field1: 1,
            field2: 1,
          }
        } as any;
        const {
          ilProductCurrentFilters
        } = useInstantListing();
        expect(ilProductCurrentFilters.value).toEqual({
          field1: 1,
          field2: 1,
        });
      });
    });

    describe("ilProductFilter", () => {
      it("should return correct data", () => {
        _storeApiProductsResponseMock.value = {
          aggregations: {
            price: {},
            rating: {},
          }
        } as any;

        const {
          ilProductFilter
        } = useInstantListing();
        expect(ilProductFilter.value).toEqual([
          {
            code: "price",
            label: "price",
          },
          {
            code: "rating",
            label: "rating",
          },
        ]);
      });

    });

    describe("getCurrentSortingOrder", () => {
      it("should return correct data when sorting have value", () => {
        _storeApiProductsResponseMock.value = {
          sorting: "price-asc"
        } as any;

        const {
          getCurrentSortingOrder
        } = useInstantListing();
        expect(getCurrentSortingOrder.value).toEqual('price-asc');
      });

      it("should return undefined when sorting does not have value", () => {
        _storeApiProductsResponseMock.value = null as any;

        const {
          getCurrentSortingOrder
        } = useInstantListing();
        expect(getCurrentSortingOrder.value).toBeUndefined();
      });

    });

    describe("getSortingOrders", () => {
      it("should return sortings when availableSortings empty", () => {
        _storeApiProductsResponseMock.value = {
          sortings: [
            {
              key: 'sort-1',
              priority: 1,
              label: 'sort 1',
            }
          ]
        } as any;

        const {
          getSortingOrders
        } = useInstantListing();
        expect(getSortingOrders.value).toEqual([
          {
            key: 'sort-1',
            priority: 1,
            label: 'sort 1',
          }
        ]);
      });

      it("should return availableSortings when availableSortings not empty", () => {
        _storeApiProductsResponseMock.value = {
          availableSortings: [
            {
              key: 'sort-1',
              priority: 1,
              label: 'sort 1',
            }
          ],
          sortings: [
            {
              key: 'sort-2',
              priority: 2,
              label: 'sort 2',
            }
          ]
        } as any;

        const {
          getSortingOrders
        } = useInstantListing();
        expect(getSortingOrders.value).toEqual([
          {
            key: 'sort-1',
            priority: 1,
            label: 'sort 1',
          }
        ]);
      });

      it("should return empty array when  when _storeApiProductsResponse empty", () => {
        _storeApiProductsResponseMock.value = null as any;

        const {
          getSortingOrders
        } = useInstantListing();
        expect(getSortingOrders.value).toEqual([]);
      });

    });

    describe("isEditMode", () => {
      it("should return true when currentInstantListingIndex > 0", () => {
        currentInstantListingIndexMock.value = 1;

        const {
          isEditMode
        } = useInstantListing();
        expect(isEditMode.value).toEqual(true);
      });

      it("should return false when currentInstantListingIndex = 0", () => {
        currentInstantListingIndexMock.value = 0;

        const {
          isEditMode
        } = useInstantListing();
        expect(isEditMode.value).toEqual(false);
      });

      it("should return false when currentInstantListingIndex < 0", () => {
        currentInstantListingIndexMock.value = -1;

        const {
          isEditMode
        } = useInstantListing();
        expect(isEditMode.value).toEqual(false);
      });

    });
  });

});
