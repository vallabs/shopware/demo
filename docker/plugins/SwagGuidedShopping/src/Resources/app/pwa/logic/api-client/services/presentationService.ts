import { ShopwareApiInstance } from '@shopware-pwa/shopware-6-client';
import {
  getPresentationPageEndpoint,
  getPresentationPageDataEndpoint,
  getPresentationGuideStateEndpoint,
  getPresentationClientStateEndpoint,
  getCmsPageByIdEndpoint, getPresentationPageProductsEndpoint
} from '../endpoints';
import { PresentationPageModel, ShopwareAdminApiInstance } from '../../interfaces';
import { CmsPage } from '@shopware-pwa/commons/interfaces/models/content/cms/CmsPage';
import { ProductListingResult } from '@shopware-pwa/commons/interfaces/response/ProductListingResult';

export function getPage(apiInstance: ShopwareApiInstance): Promise<PresentationPageModel> {
  return apiInstance.invoke.get(getPresentationPageEndpoint()).then((response) => {
    return response.data as PresentationPageModel;
  });
}

export function getSlideData(pageId: string, slideId: string, apiInstance: ShopwareApiInstance): Promise<any> {
  return apiInstance.invoke.get(getPresentationPageDataEndpoint(pageId, slideId)).then((response) => {
    return response.data;
  });
}

export function getSlideProducts(pageId: string, slideId: string, query: object, apiInstance: ShopwareApiInstance, controller?: AbortController): Promise<ProductListingResult> {
  const control = controller ? { signal: controller.signal } : {};
  return apiInstance.invoke.post(getPresentationPageProductsEndpoint(pageId, slideId), query, control).then((response) => {
    return response.data;
  });
}

export function getStateForGuides(appointmentId: string, adminApiInstance: ShopwareAdminApiInstance): Promise<any> {
  return adminApiInstance.invoke.get(getPresentationGuideStateEndpoint(appointmentId)).then((response) => {
    return response.data;
  });
}

export function getStateForClients(apiInstance: ShopwareApiInstance): Promise<any> {
  return apiInstance.invoke.get(getPresentationClientStateEndpoint()).then((response) => {
    return response.data;
  });
}

export function getCmsPageById(pageId: string, apiInstance: ShopwareApiInstance): Promise<CmsPage> {
  return apiInstance.invoke.get(getCmsPageByIdEndpoint(pageId)).then((response) => {
    return response.data as CmsPage;
  });
}
