import {
  getCartInsights,
  Client,
  Guide
} from "../cartService";
import { Cart } from "@shopware-pwa/commons/interfaces/models/checkout/cart/Cart";

describe("Services - cartService", () => {
  let getMock = jest.fn();
  let deleteMock = jest.fn();
  let postMock = jest.fn();
  let patchMock = jest.fn();
  let apiInstanceMock: any;

  beforeEach(() => {
    jest.resetAllMocks();
    apiInstanceMock = {
      invoke: {
        get: getMock,
        post: postMock,
        delete: deleteMock,
        patch: patchMock
      },
    };
  });

  describe("getCartInsights", () => {
    it('should work correctly with correct response', async () => {
      getMock.mockImplementation(() => {
        return Promise.resolve({
          data: {
            cartSum: 1,
            productCount: 1,
          }
        });
      });
      const res = await getCartInsights('appointmentId', apiInstanceMock);
      expect(getMock).toHaveBeenCalledWith(`/api/_action/guided-shopping/appointment/appointmentId/widgets/cart-insights`);
      expect(res).toEqual({
        cartSum: 1,
        productCount: 1,
      });
    })
  });

  describe("Client", () => {
    describe("getCart", () => {
      it('should work correctly with correct response', async () => {
        getMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const res = await Client.getCart(apiInstanceMock);
        expect(getMock).toHaveBeenCalledWith(`/store-api/checkout/cart`, expect.anything());
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

    describe("addLineItems", () => {
      it('should work correctly with correct response', async () => {
        postMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const items = [
          {
            id: '1',
            quantity: 2
          },
        ];
        const res = await Client.addLineItems(items, apiInstanceMock);
        expect(postMock).toHaveBeenCalledWith(`/store-api/checkout/cart/line-item`, {
          items: [
            {
              id: "1",
              quantity: 2,
              referencedId: "1",
              type: "product",
            }
          ]
        });
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

    describe("changeLineItemQuantities", () => {
      it('should work correctly with correct response', async () => {
        patchMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const items = [
          {
            id: '1',
            quantity: 2
          },
        ];
        const res = await Client.changeLineItemQuantities(items, apiInstanceMock);
        expect(patchMock).toHaveBeenCalledWith(`/store-api/checkout/cart/line-item`, {
          items: [
            {
              id: "1",
              quantity: 2,
            }
          ]
        }, expect.anything());
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

    describe("removeLineItems", () => {
      it('should work correctly with correct response', async () => {
        deleteMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const items = [
          {
            id: '1',
            quantity: 2
          },
        ];
        const res = await Client.removeLineItems(items, apiInstanceMock);
        expect(deleteMock).toHaveBeenCalledWith(`/store-api/checkout/cart/line-item`, {
          data: {
            ids: ['1']
          }
        });
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

  });

  describe("Guide", () => {
    describe("getCart", () => {
      it('should work correctly with correct response', async () => {
        getMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const res = await Guide.getCart('token', 'salesChannelId', apiInstanceMock);
        expect(getMock).toHaveBeenCalledWith(`/api/_proxy/store-api/salesChannelId/checkout/cart`, {
          'headers': {
            'sw-context-token': 'token'
          },
          data: null
        });
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

    describe("addLineItems", () => {
      it('should work correctly with correct response', async () => {
        postMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const items = [
          {
            id: '1',
            quantity: 2
          },
        ];
        const res = await Guide.addLineItems('token', 'salesChannelId', items, apiInstanceMock);
        expect(postMock).toHaveBeenCalledWith(`/api/_proxy/store-api/salesChannelId/checkout/cart/line-item`, {
          items: [
            {
              id: "1",
              quantity: 2,
              referencedId: "1",
              type: "product",
            }
          ]
        }, {
          'headers': {
            'sw-context-token': 'token'
          },
          data: null
        });
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

    describe("changeLineItemQuantities", () => {
      it('should work correctly with correct response', async () => {
        patchMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const items = [
          {
            id: '1',
            quantity: 2
          },
        ];
        const res = await Guide.changeLineItemQuantities('token', 'salesChannelId', items, apiInstanceMock);
        expect(patchMock).toHaveBeenCalledWith(`/api/_proxy/store-api/salesChannelId/checkout/cart/line-item`, {
          items: [
            {
              id: "1",
              quantity: 2,
            }
          ]
        }, {
          'headers': {
            'sw-context-token': 'token'
          },
          data: null
        });
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

    describe("removeLineItems", () => {
      it('should work correctly with correct response', async () => {
        deleteMock.mockImplementation(() => {
          return Promise.resolve({
            data: {
              name: 'string',
              token: 'string',
            } as Cart
          });
        });
        const items = [
          {
            id: '1',
            quantity: 2
          },
        ];
        const res = await Guide.removeLineItems('token', 'salesChannelId', items, apiInstanceMock);
        expect(deleteMock).toHaveBeenCalledWith(`/api/_proxy/store-api/salesChannelId/checkout/cart/line-item`, {
          'headers': {
            'sw-context-token': 'token'
          },
          data: {
            ids: ['1']
          }
        });
        expect(res).toEqual({
          name: 'string',
          token: 'string',
        });
      });
    });

  });
});
