import { ref } from "vue-demi";

import { usePresentation } from "../usePresentation";
jest.mock("../usePresentation");
const mockedUsePresentation = usePresentation as jest.Mocked<
  typeof usePresentation
>;

import { useNotes } from "../widgets/useNotes";


describe("Composables - useNotes", () => {
  const currentSlideIndexMock = ref(null);
  const treeNavigationMock = ref(null);

  beforeEach(() => {
    jest.resetAllMocks();

    currentSlideIndexMock.value = null;
    treeNavigationMock.value = null;

    // mocking Composables
    (mockedUsePresentation as any).mockImplementation(
      () =>
        ({
          currentSlideIndex: currentSlideIndexMock,
          treeNavigation: treeNavigationMock
        } as any)
    );
  });

  describe("computed", () => {
    describe("slideNotes & notesCount", () => {
        beforeEach(() => {
          treeNavigationMock.value = {
            "slide1": {
              items: [
                {
                  "groupName": "Kollektion K3 - Tamaris",
                  "groupId": "5aaa1cd91d4541a49f2b25022b77e5bd",
                  "cmsPageId": "25e33130ef9f49bb9314b99c796904c0",
                  "name": null,
                  "id": "821b5bc915cf4f35bfdef5cdb835c4f8",
                  "index": 1,
                  "notes": [
                    {
                      "type": "notes",
                      "slot": "content",
                      "config": {
                        "content": {
                          "value": "<h2>Lorem Ipsum dolor sit amet</h2>",
                          "source": "static"
                        },
                        "verticalAlign": {
                          "value": null,
                          "source": "static"
                        }
                      },
                      "id": "note1",
                    }
                  ]
                }
              ],
              name: 'slide 1'
            },
            "slide2": {
              items: [
                {
                    "groupName": "Trends - Tamaris",
                    "groupId": "9bc0f49d674c4faca6a16a145c70d574",
                    "cmsPageId": "630812056a4544f18e945688e4c3aec8",
                    "name": null,
                    "id": "092293ab33494b0aa8fbf55f439c9065",
                    "index": 2,
                    "notes": [
                      {
                        "type": "notes",
                        "slot": "content",
                        "config": {
                          "content": {
                            "value": "<h2>Lorem Ipsum dolor sit amet</h2>",
                            "source": "static"
                          },
                          "verticalAlign": {
                            "value": null,
                            "source": "static"
                          }
                        },
                        "id": "note2",
                      },
                      {
                        "type": "notes",
                        "slot": "content",
                        "config": {
                          "content": {
                            "value": "<h2>Lorem Ipsum dolor sit amet</h2>",
                            "source": "static"
                          },
                          "verticalAlign": {
                            "value": null,
                            "source": "static"
                          }
                        },
                        "id": "note3",
                      }
                    ]
                },
              ],
              name: 'slide 2'
            },
          };
        });
        it('should return 1 note when currentSlideIndex is 1 & index 1 only have 1 note', () => {
          currentSlideIndexMock.value = 1;
          const {
            slideNotes,
            notesCount
          } = useNotes();
          expect(notesCount.value).toEqual(1);
          expect(slideNotes.value).toEqual([
            {
              "type": "notes",
              "slot": "content",
              "config": {
                "content": {
                  "value": "<h2>Lorem Ipsum dolor sit amet</h2>",
                  "source": "static"
                },
                "verticalAlign": {
                  "value": null,
                  "source": "static"
                }
              },
              "id": "note1",
            }
          ]);
        });

        it('should return 2 notes when currentSlideIndex is 2 & index 2 only have 2 notes', () => {
          currentSlideIndexMock.value = 2;
          
          const {
            slideNotes,
            notesCount
          } = useNotes();
          expect(notesCount.value).toEqual(2);
          expect(slideNotes.value).toEqual([
            {
              "type": "notes",
              "slot": "content",
              "config": {
                "content": {
                  "value": "<h2>Lorem Ipsum dolor sit amet</h2>",
                  "source": "static"
                },
                "verticalAlign": {
                  "value": null,
                  "source": "static"
                }
              },
              "id": "note2",
            },
            {
              "type": "notes",
              "slot": "content",
              "config": {
                "content": {
                  "value": "<h2>Lorem Ipsum dolor sit amet</h2>",
                  "source": "static"
                },
                "verticalAlign": {
                  "value": null,
                  "source": "static"
                }
              },
              "id": "note3",
            }
          ]);
        })

        it('should return undefined when currentSlideIndex not existed', () => {
          currentSlideIndexMock.value = 3;
          
          const {
            slideNotes,
            notesCount
          } = useNotes();
          expect(notesCount.value).toEqual(undefined);
          expect(slideNotes.value).toEqual(undefined);
        })
    });
  });
});
