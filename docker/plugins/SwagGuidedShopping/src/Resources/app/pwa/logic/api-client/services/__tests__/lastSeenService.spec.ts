import {getLastSeenProducts} from "../lastSeenService";
import * as shopwareClient from "@shopware-pwa/shopware-6-client";
jest.mock("@shopware-pwa/shopware-6-client");
const mockedApiClient = shopwareClient as jest.Mocked<typeof shopwareClient>;

describe("Services - lastSeenService", () => {
    let getMock = jest.fn();
    let apiInstanceMock: any;

    beforeEach(() => {
        jest.resetAllMocks();
        apiInstanceMock = {
            invoke: {
                get: getMock,
            },
        };
    });

    describe("getLastSeenProducts", () => {
        it('should work correctly with empty lastSeen collection', async () => {
            getMock.mockImplementation(() => {
                return Promise.resolve({
                    data: {
                        collection: {
                            lastSeen: []
                        }
                    },
                });
            });

            const res = await getLastSeenProducts({}, apiInstanceMock);
            expect(getMock).toHaveBeenCalledWith('/store-api/guided-shopping/appointment/collection/last-seen');
            expect(mockedApiClient.getProducts).not.toHaveBeenCalled();
            expect(res).toBeUndefined();
        });

        it('should work correctly with empty lastSeen collection', async () => {
            getMock.mockImplementation(() => {
                return Promise.resolve({
                    data: {
                        collection: {
                            lastSeen: ['dummy-product-id-1', 'dummy-product-id-2']
                        }
                    },
                });
            });

            const res = await getLastSeenProducts({}, apiInstanceMock);
            expect(getMock).toHaveBeenCalledWith('/store-api/guided-shopping/appointment/collection/last-seen');
            expect(mockedApiClient.getProducts).toHaveBeenCalledWith({ids: ['dummy-product-id-1', 'dummy-product-id-2']}, apiInstanceMock);
            expect(res).toBeUndefined();
        });
    });
});
