import {
  getApplicationContext,
  useSharedState,
} from "@shopware-pwa/composables";
import { computed } from "@vue/composition-api";
import { useErrorHandler } from "../useErrorHandler";
import {
  Product,
  ShopwareSearchParams,
  SearchFilterType,
} from "@shopware-pwa/commons";
import { getProducts } from "@shopware-pwa/shopware-6-client";
import { ClientApiError } from "../../interfaces";

const COMPOSABLE_NAME = "useProductVariants";

export const useProductVariants = (product?: Product) => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance } = getApplicationContext({ contextName });
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const _storeProductVariants = sharedRef<Product[]>(
    `${contextName}-productVariants`,
    null
  );
  const _storeProductMainVariantsProducts = sharedRef<Product[]>(
    `${contextName}-mainVariantsProducts`,
    null
  );

  async function loadProductVariants() {
    try {
      const variantsCriteria: ShopwareSearchParams = {
        includes: {
          product: [
            "id",
            "name",
            "options",
            "availableStock",
            "calculatedPrice",
            "calculatedPrices",
          ],
        },
        associations: {
          options: {},
        },
        filter: [
          {
            type: SearchFilterType.EQUALS,
            field: "parentId",
            value: product.parentId || product.id,
          },
        ],
      };

      const productVariantResponse = await getProducts(
        variantsCriteria,
        apiInstance
      );
      _storeProductVariants.value = productVariantResponse.elements;
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadProductVariants",
      });
    }
  }

  async function loadMainVariantsProducts() {
    try {
      const productElement = product as any; // fix: Product type does not contain "mainVariantId" for some reason
      let mainVariantsIds: any[] = [];
      const mainVariantId = productElement?.mainVariantId;
      if (mainVariantId) {
        mainVariantsIds.push(mainVariantId);
      }
      if (mainVariantsIds.length === 0) return;

      const mainVariantsResponse = await getProducts(
        { ids: mainVariantsIds },
        apiInstance
      );
      _storeProductMainVariantsProducts.value = mainVariantsResponse.elements;
    } catch (e) {
      handleError(e as ClientApiError, {
        context: contextName,
        method: "loadMainVariantsProducts",
      });
    }
  }

  return {
    loadProductVariants,
    loadMainVariantsProducts,
    productVariants: computed(() => _storeProductVariants.value),
    productVariantsCount: computed(() => _storeProductVariants.value?.length),
    mainVariantsProducts: computed(
      () => _storeProductMainVariantsProducts.value
    ),
  };
};
