import {BillingAddress} from "@shopware-pwa/commons/interfaces/models/checkout/customer/BillingAddress";

export interface CustomerInsightsAttendeeData {
  attendees: CustomerInsightsAttendee[],
  currencyId: string,
  currencySymbol: string,
  extensions: [],
}

export interface CustomerInsightsAttendee {
  cartSum: string,
  extensions: [],
  id: string,
  productCount: number
}

export interface CustomerInsightsCustomerData {
  apiAlias: string,
  appointmentId: string,
  attendeeName: string,
  createdAt: string,
  customer: CustomerInsightsCustomer,
  customerId: string,
  extensions: [],
  guideCartPermissionsGranted: boolean,
  id: string
  joinedAt: string,
  productCollections: CustomerInsightsProductCollection[],
  type: string,
  updatedAt: string,
  videoUserId: string
}

export interface CustomerInsightsCustomer {
  apiAlias: string,
  email: string,
  defaultBillingAddress: BillingAddress
}

export interface CustomerInsightsProductCollection {
  alias: string
}

export interface OrderCustomer {
  apiAlias: string,
  order: CustomerInsightsYearOrder,
}

export interface CustomerInsightsYearOrder {
  apiAlias: string,
  amountNet: number,
  amountTotal: number,
  lineItems: [],
  orderDate: string,
}
