import { useSharedState, getApplicationContext, useIntercept } from '@shopware-pwa/composables';
import { AppointmentService } from '../api-client';
import { AppointmentModel, AttendeeData, VideoAudioSettingsType } from "../interfaces";
import { useAdminApiInstance } from "./useAdminApiInstance";
import { useErrorHandler } from "./useErrorHandler";
import { computed } from '@vue/composition-api';
import { useUser } from "@shopware-pwa/composables";

const COMPOSABLE_NAME = 'useAppointment';

const CONTEXT_TOKEN_COOKIE_NAME: string = 'sw-context-token';
const ATTENDEE_NAME_COOKIE_NAME: string = 'sw-gs-attendee-name';
const ATTENDEE_NAME_COOKIE_LIFESPAN: number = 60 * 60 * 24;
const ATTENDEE_NAME_COOKIE_PATH: string = '/';

export const useAppointment = () => {
  const contextName = COMPOSABLE_NAME;
  const { apiInstance, cookies } = getApplicationContext({ contextName });
  const { adminApiInstance } = useAdminApiInstance();
  const { sharedRef } = useSharedState();
  const { handleError } = useErrorHandler();
  const { user } = useUser();
  const { broadcast } = useIntercept();
  const isGuide = sharedRef<boolean>(`${contextName}-isGuide`, false);
  const isClient = sharedRef<boolean>(`${contextName}-isClient`, false);
  const joinedAppointment = sharedRef<boolean>(`${contextName}-joinedAppointment`);
  const appointment = sharedRef<AppointmentModel>(`${contextName}-appointment`);
  const appointmentId = sharedRef<string>(`${contextName}-appointmentId`);
  const isSelfService = computed(() => appointment?.value?.presentationGuideMode === 'self');
  const isPreview = computed(() => appointment?.value?.isPreview);
  const _attendeeName = sharedRef<string>(`${contextName}-attendeeName`);
  const joinError = sharedRef<any>(`${contextName}-joinError`, null);
  const joinErrorAsGuide = sharedRef<boolean>(`${contextName}-joinErrorAsGuide`);
  const selfServiceReconnectTime: number = 10000;
  const _appointmentStarted = sharedRef<boolean>(`${contextName}-appointmentStarted`, false);
  const videoAndAudioSetting = sharedRef<boolean>(`${contextName}-videoAndAudioSetting`, false);

  async function start() {
    if (!isGuide.value) return;
    try {
      await AppointmentService.startAppointment(appointmentId.value,  adminApiInstance);
      _appointmentStarted.value = true
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'start',
      });
    }
  }

  async function end() {
    if (!isGuide.value) return;
    try {
      await AppointmentService.endAppointment(appointmentId.value, adminApiInstance);
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'end',
      });
    }
  }

  function getAttendeeName() {
    if (user.value && user.value.firstName && user.value.lastName) {
      return `${user.value.firstName} ${user.value.lastName}`;
    }

    const storedName = cookies.get(ATTENDEE_NAME_COOKIE_NAME);
    if (storedName) {
      return storedName.toString();
    }

    return null;
  }

  function resetAppointmentValues(onlyErrors = false) {
    joinError.value = null;
    joinErrorAsGuide.value = false;

    if (!onlyErrors) {
      appointmentId.value = '';
      joinedAppointment.value = false;
    }
  }

  async function join(guide: boolean = false) {
    if (!appointmentId.value) return;

    try {
      if (guide) {
        appointment.value = await AppointmentService.joinAsGuide(appointmentId.value, adminApiInstance);
        // set id new because it could have been a path and the id is needed for further actions
        appointmentId.value = appointment.value.id;
        cookies.set(CONTEXT_TOKEN_COOKIE_NAME, appointment.value.newContextToken);

        isGuide.value = true;
        apiInstance.config.contextToken = appointment.value.newContextToken;
        apiInstance.update();
      } else {
        const storedName = cookies.get(ATTENDEE_NAME_COOKIE_NAME);

        if (user.value && user.value.firstName && user.value.lastName) {
          _attendeeName.value = user.value.firstName + ' ' + user.value.lastName;
        } else if (storedName) {
          _attendeeName.value = storedName;
        }

        _attendeeName.value = getAttendeeName();

        appointment.value = await AppointmentService.joinAsClient(appointmentId.value, _attendeeName.value, apiInstance);
        isClient.value = true;
      }

      joinedAppointment.value = true;
      resetAppointmentValues(true);
    } catch (e) {

      joinError.value = e;
      joinErrorAsGuide.value = guide;

      // try to reconnect client if "not accessible" error occured in self service
      if (!guide && e.statusCode && e.statusCode === 307) {
        setTimeout(async () => {
          await join(false);
        }, selfServiceReconnectTime);

        return;
      }

      handleError(e, {
        context: contextName,
        method: 'join',
        push: isGuide.value,
        asGuide: guide
      });

      broadcast('useAppointment-join-error', e);
    }
  }

  async function joinAsGuide() {
    return await join(true);
  }

  async function joinAsClient() {
    return await join(false);
  }

  async function updateAttendeeData(data: AttendeeData) {
    try {
      await AppointmentService.updateAttendee(data, apiInstance);
      if (!data.attendeeName) return;

      _attendeeName.value = data.attendeeName;
      cookies.set(ATTENDEE_NAME_COOKIE_NAME, data.attendeeName, {
        path: ATTENDEE_NAME_COOKIE_PATH,
        maxAge: ATTENDEE_NAME_COOKIE_LIFESPAN,
      });
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'updateAttendeeData',
      });
    }
  }

  async function getVideoOrAudioAvailableStatus(appointmentIdValue: string) {
    if (!appointmentIdValue) return;
    try {
      const res = await AppointmentService.getAppointmentVideoAndAudioSetting(
        appointmentIdValue,
        apiInstance,
      );

      videoAndAudioSetting.value = res.videoAudioSettings as any !== VideoAudioSettingsType.NONE;
    } catch (e) {
      handleError(e, {
        context: contextName,
        method: 'getVideoOrAudioAvailableStatus',
      });
    }
  }

  return {
    start,
    end,
    joinAsGuide,
    joinAsClient,
    joinedAppointment,
    isGuide,
    isClient,
    isSelfService,
    appointment,
    getAttendeeName,
    appointmentId,
    updateAttendeeData,
    attendeeName: computed(() => _attendeeName.value),
    appointmentStarted: computed(() => _appointmentStarted.value),
    joinError,
    joinErrorAsGuide,
    resetAppointmentValues,
    isPreview,
    getVideoOrAudioAvailableStatus,
    videoAndAudioSetting
  }
};
