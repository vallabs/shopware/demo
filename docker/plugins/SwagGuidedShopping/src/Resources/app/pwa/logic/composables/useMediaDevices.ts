import { computed } from "@vue/composition-api"
import { useSharedState } from '@shopware-pwa/composables';
import { MediaDevices } from "../interfaces";

const FAKE_SPEAKER_ID = '__fakeSpeakerDeviceId';

type DeviceStorageKey = 'microphone-device' | 'speaker-device' | 'camera-device';

export type DeviceList = {
  videoInput: MediaDeviceInfo[],
  audioInput: MediaDeviceInfo[],
  audioOutput: MediaDeviceInfo[],
};

const DEVICE_KEYS: { [key: string]: DeviceStorageKey; } = {
  microphone: 'microphone-device',
  speaker: 'speaker-device',
  camera: 'camera-device'
};

const COMPOSABLE_NAME = 'useMediaDevices';

export type DeviceError = 'NO_DEVICES_FOUND' | 'CAMERA_IN_USE' | 'MICROPHONE_IN_USE' | 'CAMERA_AND_MICROPHONE_IN_USE' | 'SPEAKER_NOT_ALLOWED';
export type MediaDeviceChanged = { storageKey, deviceId };

const SAVED_SETTINGS_STORAGE_KEY = 'sw-gs-call-saved-settings';
const DEVICES_STORAGE_KEY = 'sw-gs-call-devices';
const AUDIO_ONLY_STORAGE_KEY = 'sw-gs-call-audio-only';
const MICROPHONE_STATE_STORAGE_KEY = 'sw-gs-call-microphone-state';
const CAMERA_STATE_STORAGE_KEY = 'sw-gs-call-camera-state';

export const useMediaDevices = function () {
  const contextName = COMPOSABLE_NAME;
  const { sharedRef } = useSharedState();

  const microphoneDevice = sharedRef<string>(`${contextName}-microphoneDevice`);
  const speakerDevice = sharedRef<string>(`${contextName}-speakerDevice`);
  const cameraDevice = sharedRef<string>(`${contextName}-cameraDevice`);

  const _hasMediaDevicesStored = sharedRef<boolean>(`${contextName}-hasMediaDevicesStored`, false);
  const _hasSavedSettings = sharedRef<boolean>(`${contextName}-hasSavedSettings`, false);
  const _mediaDeviceChanged = sharedRef<MediaDeviceChanged>(`${contextName}-mediaDeviceChanged`, null);
  const _deviceError = sharedRef<DeviceError>(`${contextName}-deviceError`, null);
  const isAudioOnly = sharedRef<boolean>(`${contextName}-isAudioOnly`, false);
  const _mediaStream = sharedRef<MediaStream>(`${contextName}-mediaStream`, null);

  const _mediaDevices = sharedRef<MediaDevices>(`${contextName}-devices`, {
    videoInput: [],
    audioInput: [],
    audioOutput: [],
  });

  const init = () => {
    storeHasDevices();
    if (navigator && navigator.mediaDevices) {
      navigator.mediaDevices.addEventListener('devicechange', onDeviceChange);
    }
    _hasSavedSettings.value = Boolean(sessionStorage.getItem(SAVED_SETTINGS_STORAGE_KEY));
    isAudioOnly.value = Boolean(sessionStorage.getItem(AUDIO_ONLY_STORAGE_KEY));
  };

  const cleanup = () => {
    if (navigator && navigator.mediaDevices) {
      navigator.mediaDevices.removeEventListener('devicechange', onDeviceChange);
    }
  };

  const onDeviceChange = () => getMediaDevices(true);

  /**
   * evaluates available devices and saves them
   */
  const getMediaDevices = async (renewList:boolean = false, allowEmptyLabels:boolean = false): Promise<DeviceList> => {

    try {
      let devices = getMediaDevicesFromStorage();

      if (!devices || renewList) {
        // if no devices were found in storage or we want to force
        const enumeratedDevices = await navigator.mediaDevices.enumerateDevices();
        devices = gotDevices(enumeratedDevices, allowEmptyLabels);
      }

      _mediaDevices.value = devices;
      return devices;
    } catch (err) {
      handleDeviceError('NO_DEVICES_FOUND');
    }
  };

  /**
   * returns a new mediaStream constrained to the currently used devices
   * @param devices
   */
  const getMediaStream = async (devices: { video: string, audio: string, }): Promise<MediaStream> => {
    if (_mediaStream.value) {
      for (const track of _mediaStream.value.getTracks()) {
        await track.stop();
      }
    }

    const constraints = getConstraints(devices);
    _mediaStream.value = await navigator.mediaDevices.getUserMedia(constraints);
    return _mediaStream.value;
  };

  /**
   * returns the constraints to create a media stream with the first selectable mediaDevices
   *
   * @param devices
   */
  const getConstraints = (devices: { audio: string, video: string, }): MediaStreamConstraints => {
    const constraints: MediaStreamConstraints = {};
    constraints.audio = devices.audio ? { deviceId: { exact: devices.audio } } : true;
    if (!isAudioOnly.value) {
      constraints.video = devices.video ? { deviceId: { exact: devices.video } } : true;
    }
    return constraints;
  };

  /**
   * call back when devices have been found
   */
  const gotDevices = (deviceInfo, allowEmptyLabels: boolean = false): DeviceList => {
    const devices = getDevices(deviceInfo, allowEmptyLabels);
    addFakeSpeaker(devices);
    setMediaDevicesToStorage(devices);
    return devices;
  };

  /**
   * returns object sorted by devices kind
   */
  const getDevices = (deviceInfo, allowEmptyLabels: boolean = false): DeviceList => {
    const devices: DeviceList = {
      videoInput: [],
      audioInput: [],
      audioOutput: [],
    };

    deviceInfo.forEach(info => {
      if (!allowEmptyLabels && !info.label) return;

      switch (info.kind) {
        case "audioinput":
          devices.audioInput.push(info);
          break;
        case "audiooutput":
          devices.audioOutput.push(info);
          break;
        case "videoinput":
          devices.videoInput.push(info);
          break;
        default:
          break
      }
    });

    return devices;
  };


  /**
   * returns a device form the storage
   * falls back to first entry for device type
   *
   * @param devices
   * @param type
   * @param storageKey
   */
  const readDeviceFromStorage = (devices, type: 'audioInput' | 'audioOutput' | 'videoInput', storageKey) => {
    let device = getDeviceFromStorage(storageKey);
    if (!device && devices[type].length > 0) device = devices[type][0].deviceId;
    return device;
  };

  /**
   * sets devices to storage or first value for select box
   *
   * @param devices
   */
  const setDevicesFromStorage = (devices) => {
    microphoneDevice.value = readDeviceFromStorage(devices, 'audioInput', 'microphone-device');
    speakerDevice.value = readDeviceFromStorage(devices, 'audioOutput', 'speaker-device');

    // disable camera evaluation if audioOnly is active
    if (isAudioOnly.value) {
      cameraDevice.value = null;
    } else {
      cameraDevice.value = readDeviceFromStorage(devices, 'videoInput', 'camera-device');
    }
  };

  /**
   * Firefox always uses the default Speaker
   * so we need to fake this because the speaker response is empty
   *
   * @param devices
   */
  const addFakeSpeaker = (devices) => {
    if (devices.audioOutput.length !== 0) return;

    devices.audioOutput.push({
      deviceId: FAKE_SPEAKER_ID,
      groupId: FAKE_SPEAKER_ID,
      kind: 'audiooutput',
      label: 'System Speaker',
    });
  };

  /**
   * sets the found media devices to the session storage
   */
  const setMediaDevicesToStorage = (devices) => {
    sessionStorage.setItem(DEVICES_STORAGE_KEY, JSON.stringify(devices));
  };

  /**
   * retrieves the media devices from the session storage
   */
  const getMediaDevicesFromStorage = (): DeviceList => {
    const devices = sessionStorage.getItem(DEVICES_STORAGE_KEY);
    try {
      return JSON.parse(devices)
    } catch (e) {
      return null;
    }
  };

  /**
   * saves used device to storage
   *
   * @param storageKey
   * @param deviceId
   */
  const setDeviceToStorage = (storageKey: DeviceStorageKey, deviceId) => {
    _mediaDeviceChanged.value = { storageKey, deviceId };
    if (deviceId === null || deviceId === 'null') deviceId = '';
    sessionStorage.setItem(`sw-gs-call-${storageKey}`, deviceId);
  };


  /**
   * sets the initially selected devices in the storage
   */
  const saveDevicesToStorage = () => {
    setDeviceToStorage('microphone-device', microphoneDevice.value);
    setDeviceToStorage('speaker-device', speakerDevice.value);
    setDeviceToStorage('camera-device', cameraDevice.value);

    setSavedSettings();
  };

  const setSavedSettings = () => {
    sessionStorage.setItem(SAVED_SETTINGS_STORAGE_KEY, 'true');
    _hasSavedSettings.value = true;
  };

  const setAudioOnly = () => {
    //reset devices
    cameraDevice.value = null;
    // microphoneDevice.value = null;

    sessionStorage.setItem(AUDIO_ONLY_STORAGE_KEY, 'true');
    isAudioOnly.value = true;
  };

  /**
   * saves used device to storage
   *
   * @param storageKey
   */
  const unsetDeviceFromStorage = (storageKey: DeviceStorageKey) => {
    _mediaDeviceChanged.value = { storageKey, 'deviceId': null };
    sessionStorage.removeItem(`sw-gs-call-${storageKey}`);
  };

  /**
   * sets the found media devices to the session storage
   */
  const unsetDevicesFromStorage = () => {
    unsetDeviceFromStorage('microphone-device');
    unsetDeviceFromStorage('speaker-device');
    unsetDeviceFromStorage('camera-device');
  };

  /**
   * resets the whole device state
   */
  const clearDeviceState = () => {
    unsetDevicesFromStorage();
    sessionStorage.removeItem(AUDIO_ONLY_STORAGE_KEY);
    sessionStorage.removeItem(DEVICES_STORAGE_KEY);
    sessionStorage.removeItem(MICROPHONE_STATE_STORAGE_KEY);
    sessionStorage.removeItem(CAMERA_STATE_STORAGE_KEY);
    sessionStorage.removeItem(SAVED_SETTINGS_STORAGE_KEY);
  };


  /**
   * resets the whole device state
   */
  const clearDeviceStateOnError = () => {
    sessionStorage.removeItem(SAVED_SETTINGS_STORAGE_KEY);
  };

  /**
   * returns the length of devices found
   */
  const hasDevices = () => {
    let deviceCount = 0;
    Object.values(_mediaDevices.value).forEach(deviceArray => deviceCount += deviceArray.length);
    return deviceCount > 0;
  };

  /**
   * checks if the devices are set in the session storage
   */
  const storeHasDevices = () => {
    let devicesFound = true;
    Object.values(DEVICE_KEYS).forEach(storageKey => {
      const device = getDeviceFromStorage(storageKey);
      if (!device) devicesFound = false;
    });

    _hasMediaDevicesStored.value = devicesFound;
  };

  /**
   * returns session stored device
   * @param storageKey
   */
  const getDeviceFromStorage = (storageKey: DeviceStorageKey): string => {
    let value = sessionStorage.getItem(`sw-gs-call-${storageKey}`);
    if (value == 'null') value = null;
    return value;
  };

  /**
   * returns the device keys for outside usage
   */
  const getDeviceKeys = () => {
    return DEVICE_KEYS
  };

  /**
   * opens error modal if device error occurs
   * @param {DeviceError} errType
   */
  const handleDeviceError = (errType: DeviceError) => {
    if (!_deviceError.value) clearDeviceStateOnError();
    _deviceError.value = errType;
  };

  /**
   * opens error modal if device error occurs
   */
  const unsetDeviceError = () => {
    _deviceError.value = null;
  };

  const deviceCount = (type) => {
    let deviceCount = 0;
    _mediaDevices.value[type].forEach(deviceArray => {
      if (deviceArray.deviceId) deviceCount++;
    });
    return deviceCount;
  }

  return {
    getDeviceKeys,
    getMediaDevices,
    storeHasDevices,
    hasDevices,
    saveDevicesToStorage,
    setDevicesFromStorage,
    unsetDevicesFromStorage,
    clearDeviceState,
    getDeviceFromStorage,
    deviceError: computed(() => _deviceError.value),
    mediaDeviceChanged: computed(() => _mediaDeviceChanged.value),
    mediaDevices: computed(() => _mediaDevices.value),
    hasMediaDevicesStored: computed(() => _hasMediaDevicesStored.value),
    hasSavedSettings: computed(() => _hasSavedSettings.value),
    cameraCount: computed(() => deviceCount('videoInput')),
    microphoneCount: computed(() => deviceCount('audioInput')),
    speakerCount: computed(() => deviceCount('audioOutput')),
    isAudioOnly,
    cameraDevice,
    microphoneDevice,
    speakerDevice,
    setAudioOnly,
    setDeviceToStorage,
    cleanup,
    init,
    getConstraints,
    handleDeviceError,
    unsetDeviceError,
    getMediaStream,
  };

};
