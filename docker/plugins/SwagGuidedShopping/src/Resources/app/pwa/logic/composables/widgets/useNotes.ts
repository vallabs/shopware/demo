import {computed} from '@vue/composition-api';
import {NavigationItem, TreeNavigationItem} from "../../interfaces";
import {usePresentation} from "../usePresentation";

export const useNotes = () => {
    const { currentSlideIndex, treeNavigation } = usePresentation();
    const slideNotes = computed(() => {
        const navigation: Array<TreeNavigationItem> = Object.values(treeNavigation.value);

        const flattenedItems = navigation.reduce(( accumulator: Array<NavigationItem>, navGroup ) => {
            return accumulator.concat(navGroup.items)
        }, []);

        const current = flattenedItems.find((item) => item.index === currentSlideIndex.value);

        return current?.notes
    });

    return {
        slideNotes,
        notesCount: computed(() => slideNotes.value?.length),
    }
};
