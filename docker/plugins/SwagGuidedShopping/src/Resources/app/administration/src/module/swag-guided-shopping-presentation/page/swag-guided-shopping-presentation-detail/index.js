import template from './swag-guided-shopping-presentation-detail.html.twig';
import './swag-guided-shopping-presentation-detail.scss';
import swagGuidedShoppingPresentationDetailState from './state';

const { Component, Mixin, Context, Defaults } = Shopware;
const { Criteria } = Shopware.Data;
const { mapState, mapGetters } = Shopware.Component.getComponentHelper();

Component.register('swag-guided-shopping-presentation-detail', {
    template,

    inject: ['repositoryFactory', 'acl', 'systemConfigApiService'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    shortcuts: {
        'SYSTEMKEY+S': {
            active() {
                return this.acl.can('guided_shopping_presentation.editor');
            },
            method: 'onSave'
        },
        ESCAPE: 'onCancel'
    },

    props: {
        presentationId: {
            type: String,
            required: false,
            default: null
        }
    },

    data() {
        return {
            term: '',
            isSaveSuccessful: false,
            previewSlotTypes: ['image'],
        };
    },

    computed: {
        ...mapState('swagGuidedShoppingPresentationDetail', [
            'presentation'
        ]),

        ...mapGetters('swagGuidedShoppingPresentationDetail', [
            'presentationRepository',
            'isLoading'
        ]),

        ...mapState('cmsPageState', [
            'currentPage'
        ]),

        creationMode() {
            return !this.presentationId;
        },

        identifier() {
            return this.presentationTitle;
        },

        presentationTitle() {
            // return name
            return this.placeholder(this.presentation, 'name', this.$tc('swag-guided-shopping-presentation.detail.textHeadline'));
        },

        presentationRepository() {
            return this.repositoryFactory.create('guided_shopping_presentation');
        },

        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        presentationCmsPageRepository() {
            return this.repositoryFactory.create('guided_shopping_presentation_cms_page');
        },

        assignmentRepository() {
            return this.repositoryFactory.create(
                this.presentation.cmsPages.entity,
                this.presentation.cmsPages.source
            );
        },

        cmsPageRepository() {
            return this.repositoryFactory.create('cms_page');
        },

        customFieldSetRepository() {
            return this.repositoryFactory.create('custom_field_set');
        },

        presentationCriteria() {
            const criteria = new Criteria();
            criteria.addAssociation('cmsPages.cmsPage.sections.blocks.slots');
            criteria.getAssociation('cmsPages').addSorting(Criteria.sort('position', 'ASC'));
            criteria.getAssociation('cmsPages').addFilter(Criteria.equals('guidedShoppingPresentationVersionId', Defaults.versionId));
            criteria.getAssociation('cmsPages.cmsPage.sections.blocks.slots');

            return criteria;
        },

        customFieldSetCriteria() {
            const criteria = new Criteria(1, 100);

            criteria.addFilter(Criteria.equals('relations.entityName', 'guided_shopping_presentation'));
            criteria
                .getAssociation('customFields')
                .addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

            return criteria;
        },

        tooltipSave() {
            const systemKey = this.$device.getSystemKey();

            return {
                message: `${systemKey} + S`,
                appearance: 'light'
            };
        },

        tooltipCancel() {
            return {
                message: 'ESC',
                appearance: 'light'
            };
        },
    },

    beforeCreate() {
        Shopware.State.registerModule('swagGuidedShoppingPresentationDetail', swagGuidedShoppingPresentationDetailState);
    },

    created() {
        this.createdComponent();
    },

    beforeDestroy() {
        Shopware.State.unregisterModule('swagGuidedShoppingPresentationDetail');
    },

    destroyed() {
        this.destroyedComponent();
    },

    watch: {
        presentationId() {
            this.destroyedComponent();
            this.createdComponent();
        }
    },

    methods: {
        async createdComponent() {
            Shopware.State.dispatch('cmsPageState/resetCmsPageState');

            // when create
            if (this.creationMode) {
                // set language to system language
                if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                    Shopware.State.commit('context/resetLanguageToDefault');
                }
            }

            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setCurrentLanguageId', Shopware.Context.api.languageId);

            // initialize default state
            await this.initState();

            this.$root.$on('presentation-reload', () => {
                this.reloadAll();
            });

            this.$root.$on('presentation-save', () => {
                this.onSave();
            });
        },

        async createNewCmsPageReference(cmsPageId) {
            if (!this.presentation?.cmsPages) {
                await this.createState();
            }

            const newCmsPage = this.assignmentRepository.create();
            newCmsPage.presenationId = this.presentation.id;
            newCmsPage.guidedShoppingPresentationVersionId = this.presentation.versionId;
            newCmsPage.cmsPageId = cmsPageId;
            newCmsPage.position = this.presentation.cmsPages.length + 1;

            const criteria = new Criteria();
            criteria.addAssociation('sections');

            this.cmsPageRepository.get(cmsPageId, { ...Context.api }, criteria).then((cmsPage) => {
                newCmsPage.cmsPage = cmsPage;
                newCmsPage.cmsPageVersionId = cmsPage.versionId;
                this.presentation.cmsPages.add(newCmsPage);
                this.onSave();
            });
        },

        onSearch(value) {
            if (value.length === 0) {
                value = undefined;
            }
            this.term = value;
        },

        destroyedComponent() {
            this.$root.$off('presentation-reload');
            this.$root.$off('presentation-save');
        },

        initState() {
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setApiContext', Shopware.Context.api);

            // when presentation exists
            if (this.presentationId) {
                return this.loadState();
            }

            // When no presentation id exists init state and new presentation with the repositoryFactory
            return this.createState();
        },

        loadState() {
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLocalMode', false);
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setPresentationId', this.presentationId);
            Shopware.State.commit('shopwareApps/setSelectedIds', [this.presentationId]);

            return this.loadAll();
        },

        loadAll() {
            return Promise.all([
                this.loadPresentation()
            ]);
        },

        createState() {
            // set local mode
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLocalMode', true);
            Shopware.State.commit('shopwareApps/setSelectedIds', []);

            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', true]);

            // create empty presentation
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setPresentation', this.presentationRepository.create(Shopware.Context.api));
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setPresentationId', this.presentation.id);

            // fill empty data
            this.presentation.active = true;

            return Promise.all([]).then(() => {
                Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);
            });
        },

        async createDefaultAppointments() {
            if (!this.presentation._isNew) return;
            if (!this.acl.can('guided_shopping_appointment.creator')) return;

            const response = await this.systemConfigApiService.getValues('SwagGuidedShopping');
            const defaultDomain = response['SwagGuidedShopping.config.defaultAppointmentDomain'];
            if (!defaultDomain) return false; // return false value only for the case do not have configuration

            let appointment = this.appointmentRepository.create(Shopware.Context.api);
            appointment.active = true;
            appointment.mode = 'self';
            appointment.name = this.presentation.name;
            appointment.attendeeRestrictionType = 'open';
            appointment.presentationPath = appointment.id;
            appointment.videoAudioSettings = 'none';
            appointment.default = true;
            appointment.salesChannelDomainId = defaultDomain;

            this.presentation.appointments = [appointment];
            return true;
        },

        loadPresentation() {
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', true]);

            this.presentationRepository.get(
                this.presentationId || this.presentation.id,
                Shopware.Context.api,
                this.presentationCriteria
            ).then(async (res) => {
                if (this.$route.query?.newCmsPageId) {
                    const id = this.$route.query.newCmsPageId;
                    this.$router.push(this.$route.path);
                    this.createNewCmsPageReference(id);
                }

                if (res) {
                    Shopware.State.commit('swagGuidedShoppingPresentationDetail/setPresentation', res);
                    await this.updatePreviewPresentation();
                    this.loadCustomFieldSets();
                }

                Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);
            });
        },


        abortOnLanguageChange() {
            return Shopware.State.getters['swagGuidedShoppingPresentationDetail/hasChanges'];
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setCurrentLanguageId', languageId);
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.reloadAll();
        },

        reloadAll(cms = true) {
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', true]);
            this.loadAll().then(() => {
                Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);
            });
        },

        saveFinish() {
            this.isSaveSuccessful = false;

            if (this.$route.name === "swag.guided.shopping.presentation.create.base") {
                this.$router.push({
                    name: 'swag.guided.shopping.presentation.detail',
                    params: { id: this.presentation.id }
                });
            }

        },

        async onSave() {
            this.isSaveSuccessful = false;

            if (this.$route.name === "swag.guided.shopping.presentation.detail.cms") {
                this.$root.$emit('presentation-slot-config-save', this.savePresentation);
                return;
            }

            await this.savePresentation();
        },

        onCancel() {
            this.$router.push({ name: 'swag.guided.shopping.presentation.index' });
        },

        async savePresentation(reload = false) {
            Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', true]);

            if (!this.presentationRepository.hasChanges(this.presentation)) {
                if (reload) this.reloadAll();
                Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);

                this.isSaveSuccessful = true;
                return;
            }

            const isCreateSuccess = await this.createDefaultAppointments();
            if (isCreateSuccess === false) {
                this.createNotificationError({
                    message: this.$tc('swag-guided-shopping-presentation.detail.defaultDomainIsNotSetError'),
                });
                Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);
                return;
            }

            // save presentation
            this.presentationRepository.save(this.presentation, Shopware.Context.api).then(() => {
                this.loadAll().then(() => {
                    if (reload) this.reloadAll();
                    Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);
                    this.isSaveSuccessful = true;
                });
            }).catch((response) => {
                Shopware.State.commit('swagGuidedShoppingPresentationDetail/setLoading', ['presentation', false]);
            });
        },

        async updatePreviewPresentation() {
            if (!this.presentation) return;

            const presentationId = this.presentation.id;

            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('parentId', presentationId));

            const previewPresentations = await this.presentationRepository.search(criteria);
            if (previewPresentations.first()) {
                const previewPresentation = previewPresentations.first();

                const criteria = new Criteria();
                criteria.addFilter(Criteria.equals('presentationId', previewPresentation.id));

                const previewCmsPageIds = await this.presentationCmsPageRepository.searchIds(criteria);
                if (previewCmsPageIds.total) {
                    await this.presentationCmsPageRepository.syncDeleted(previewCmsPageIds.data);
                }

                this.presentation.cmsPages.forEach((cmsPage) => {
                    // remove id for clone, not update the original one
                    delete cmsPage['id'];
                    delete cmsPage['createdAt'];
                    delete cmsPage['updatedAt'];

                    /**
                     * clone presentationCmsPage with previewPresentationId
                     */
                    const newCmsPage = this.assignmentRepository.create();
                    Object.keys(cmsPage).forEach((key) => {
                        if (key === 'presentationId') {
                            newCmsPage[key] = previewPresentation.id;
                        } else {
                            newCmsPage[key] = cmsPage[key];
                        }
                    });

                    previewPresentation.cmsPages.add(newCmsPage)
                });

                await this.presentationRepository.save(previewPresentation);
            }
        },

        loadCustomFieldSets() {
            return this.customFieldSetRepository.search(this.customFieldSetCriteria)
                .then((customFieldSets) => {
                    return this.$store.commit('swagGuidedShoppingPresentationDetail/setCustomFieldSets', customFieldSets);
                });
        },
    }
});


