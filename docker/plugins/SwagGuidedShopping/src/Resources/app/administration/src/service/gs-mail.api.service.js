const ApiService = Shopware.Classes.ApiService;
/**
 * Gateway for the API end point "mail"
 * @class
 * @extends ApiService
 * @package system-settings
 */


class GSMailApiService extends ApiService {
    constructor(httpClient, loginService, apiEndpoint = 'mail-template') {
        super(httpClient, loginService, apiEndpoint);
        this.name = 'gsMailService';
    }

    handleInvitation(appointmentId, invitationAction, cancelMessage = '', recipientEmail = null) {
        const apiRoute = `/_action/${this.getApiBasePath()}/send`;

        const requestData = {
            appointmentId,
            invitationAction,
            ...(invitationAction === 'cancel' && { cancelMessage, recipientEmail })
        };

        return this.httpClient.post(
            apiRoute,
            requestData,
            {
                headers: this.getBasicHeaders(),
            },
        ).then((response) => {
            return ApiService.handleResponse(response);
        });
    }

    sendInvitation(
        appointmentId,
    ) {
        return this.handleInvitation(appointmentId, 'invite');
    }

    sendInvitationForAll(
        appointmentId,
    ) {
        return this.handleInvitation(appointmentId, 'inviteAll');
    }

    cancelInvitation(
        appointmentId,
        cancelMessage,
        recipientEmail
    ) {
        return this.handleInvitation(appointmentId, 'cancel', cancelMessage, recipientEmail);
    }
}

export default GSMailApiService;
