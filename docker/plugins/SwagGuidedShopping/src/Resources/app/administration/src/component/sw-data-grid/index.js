import template from './sw-data-grid.html.twig';

const { Component } = Shopware;

Component.override('sw-data-grid', {
    template,

    props: {
        searchResultIds: {
            type: Array,
            required: false,
            default: () => []
        },

        enableCompactMode: {
            type: Boolean,
            default: true,
            required: false,
        },
    },

    methods: {
        getRowClasses(item, itemIndex) {
            return [
                {
                    'is--inline-edit': this.isInlineEdit(item),
                    'is--selected': this.isSelected(item.id) || this.searchResultIds.includes(item.id),
                },
                `sw-data-grid__row--${itemIndex}`,
            ];
        },
    }
});
