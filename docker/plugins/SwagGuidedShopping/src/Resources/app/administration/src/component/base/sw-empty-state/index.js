import template from './sw-empty-state.html.twig';
import './sw-empty-state.scss';

const { Component } = Shopware;

Component.override('sw-empty-state', {
    template,
});
