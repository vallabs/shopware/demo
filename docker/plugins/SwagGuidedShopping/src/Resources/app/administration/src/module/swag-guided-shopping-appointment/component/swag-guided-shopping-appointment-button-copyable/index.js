import template from './swag-guided-shopping-appointment-button-copyable.html.twig';
import './swag-guided-shopping-appointment-button-copyable.scss';

const { Component, Mixin } = Shopware;

Component.extend('swag-guided-shopping-appointment-button-copyable', 'sw-field-copyable', {
    template,
    props: {
        compact: {
            type: Boolean,
            default: false
        }
    },
    computed: {
        tooltipText() {
            if (this.wasCopied) {
                return this.$tc('global.sw-field-copyable.tooltip.wasCopied');
            }

            return this.$tc('swag-guided-shopping-appointment-button-copyable.tooltip.canCopy');
        },
    },
});
