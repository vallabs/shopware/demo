import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'notes',
    label: 'sw-cms.blocks.presentation.notes.label',
    category: 'presentation',
    component: 'sw-cms-block-notes',
    previewComponent: 'sw-cms-preview-notes',
    defaultConfig: {
        marginBottom: '',
        marginTop: '',
        marginLeft: '',
        marginRight: '',
        sizingMode: 'boxed',
    },
    slots: {
        content: 'notes',
    },
});
