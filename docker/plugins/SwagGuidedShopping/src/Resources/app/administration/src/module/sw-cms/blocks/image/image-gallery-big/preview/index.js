import template from './sw-cms-preview-image-gallery-big.html.twig';
import './sw-cms-preview-image-gallery-big.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-image-gallery-big', {
    template
});
