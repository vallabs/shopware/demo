/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-el-preview-like', () => import('./preview'));
/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-el-config-like', () => import('./config'));
/**
 * @private
 * @package buyers-experience
 */
Shopware.Component.register('sw-cms-el-like', () => import('./component'));

const Criteria = Shopware.Data.Criteria;
const criteria = new Criteria(1, 25);
criteria.addAssociation('deliveryTime');

Shopware.Service('cmsService').registerCmsElement({
    name: 'like',
    label: 'sw-cms.elements.like.label',
    component: 'sw-cms-el-like',
    configComponent: 'sw-cms-el-config-like',
    previewComponent: 'sw-cms-el-preview-like',
    disabledConfigInfoTextKey: 'sw-cms.elements.buyBox.infoText.tooltipSettingDisabled',
    defaultConfig: {
        product: {
            source: 'static',
            value: null,
            required: true,
            entity: {
                name: 'product',
                criteria: criteria,
            },
        },
    },
    defaultData: {
        product: {
            name: 'Lorem Ipsum dolor',
            productNumber: 'XXXXXX',
        },
    },
    collect: Shopware.Service('cmsService').getCollectFunction(),
});
