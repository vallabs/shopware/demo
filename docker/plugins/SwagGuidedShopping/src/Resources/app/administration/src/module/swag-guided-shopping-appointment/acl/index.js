Shopware.Service('privileges').addPrivilegeMappingEntry({
    category: 'permissions',
    parent: 'guided_shopping',
    key: 'guided_shopping_appointment',
    roles: {
        viewer: {
            privileges: [
                'guided_shopping_appointment:read',
                'guided_shopping_presentation:read',
                'guided_shopping_appointment_video_chat:read',
                'guided_shopping_appointment_attendee:read',
                'customer:read',
                'user:read',
                'user_config:read',
                'sales_channel_domain:read',
                'sales_channel:read',
                'system_config:read',
                'rule:read'
            ],
            dependencies: []
        },
        editor: {
            privileges: [
                'guided_shopping_appointment:update',
                'guided_shopping_appointment_attendee:create',
                'guided_shopping_appointment_video_chat:update',
                'guided_shopping_appointment_attendee:delete'
            ],
            dependencies: [
                'guided_shopping_appointment.viewer'
            ]
        },
        creator: {
            privileges: [
                'guided_shopping_appointment:create',
                'guided_shopping_appointment_video_chat:create'
            ],
            dependencies: [
                'guided_shopping_appointment.editor'
            ]
        },
        deleter: {
            privileges: [
                'guided_shopping_appointment:delete',
                'guided_shopping_appointment_attendee:delete',
                'guided_shopping_appointment_video_chat:delete',
                'guided_shopping_appointment_video_chat:delete',
            ],
            dependencies: [
                'guided_shopping_appointment.viewer'
            ]
        }
    }
});