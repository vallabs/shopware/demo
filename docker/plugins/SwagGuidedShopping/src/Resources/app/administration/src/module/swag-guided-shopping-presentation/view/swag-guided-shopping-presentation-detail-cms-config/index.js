import template from './swag-guided-shopping-presentation-detail-cms-config.html.twig';
import './swag-guided-shopping-presentation-detail-cms-config.scss';

const { Component } = Shopware;
const { Criteria, ChangesetGenerator } = Shopware.Data;
const { mapState, mapGetters } = Component.getComponentHelper();
const { cloneDeep, merge } = Shopware.Utils.object;
const type = Shopware.Utils.types;

Component.register('swag-guided-shopping-presentation-detail-cms-config', {
    template,

    inject: [
        'acl',
        'repositoryFactory',
        'cmsService',
    ],

    mixins: [
        Shopware.Mixin.getByName('notification'),
    ],

    data() {
        return {
            cmsPageRelationId: '',
            isSlotConfigLoading: false,
            overwrittenSlotIds: [], // contains slot that has already been overwritten content
            resetContentSlotIds: [], // contains slot that has already been reset content
            isDirty: false, // the flag check whether the slot content is changed
            slotConfigIconSize: 30,
        };
    },

    beforeCreate() {
        Shopware.State.dispatch('cmsPageState/resetCmsPageState');
    },

    mounted() {
        this.initPageRelation();
    },

    created() {
        this.createdComponent();
    },

    computed: {
        ...mapState('swagGuidedShoppingPresentationDetail', [
            'presentation',
            'loading'
        ]),

        ...mapState('cmsPageState', [
            'currentPage'
        ]),

        ...mapGetters('swagGuidedShoppingPresentationDetail', [
            'isLoading',
        ]),

        cmsPageRepository() {
            return this.repositoryFactory.create('cms_page');
        },

        cmsSlotRepository() {
            return this.repositoryFactory.create('cms_slot');
        },

        cmsPages() {
            const pages = [];

            if (!this.presentation.cmsPages) return pages;

            this.presentation.cmsPages.forEach((pageRelation) => {
                const title = pageRelation.title || pageRelation?.translated.title || pageRelation.cmsPage?.translated.name || pageRelation.cmsPage.name;

                pages.push(
                    {
                        value: pageRelation.id,
                        label: title,
                        hasSlotConfig: this.hasSlotConfig(pageRelation)
                    },
                )
            });

            return pages;
        },

        cmsPage() {
            return Shopware.State.get('cmsPageState').currentPage;
        },

        pageRelation() {
            if (!this.presentation.cmsPages) return;

            if (typeof this.cmsPageRelationId == "undefined") return;

            return this.presentation.cmsPages.get(this.cmsPageRelationId);
        },

        cmsPageId() {
            const relation = this.pageRelation;

            if (!relation) return;

            return relation.cmsPage.id;
        }
    },

    methods: {
        createdComponent() {
            this.$root.$on('presentation-slot-config-save', (callback) => {
                this.onSave(callback);
            });
        },

        initPageRelation() {
            if (this.cmsPageRelationId) return;

            if (this.$route.params.relationId) {
                this.cmsPageRelationId = this.$route.params.relationId;
                return;
            }

            if (!this.cmsPages) return;
            if (!this.cmsPages[0]) return;
            this.cmsPageRelationId = this.cmsPages[0].value;
        },

        getAssignedCmsPage(onReset = false) {
            if (this.cmsPageId === null || this.pageRelation === null) {
                return Promise.resolve(null);
            }

            const cmsPageId = this.cmsPageId;
            const criteria = new Criteria(1, 1);
            criteria.setIds([cmsPageId]);
            criteria.addAssociation('previewMedia');
            criteria.addAssociation('sections');
            criteria.getAssociation('sections').addSorting(Criteria.sort('position'));

            criteria.addAssociation('sections.blocks');
            criteria.getAssociation('sections.blocks')
                .addSorting(Criteria.sort('position', 'ASC'))
                .addAssociation('slots');
            return this.cmsPageRepository.search(criteria).then((response) => {
                const cmsPage = response.get(cmsPageId);

                if (cmsPageId !== this.cmsPageId) {
                    return null;
                }

                if (!this.hasSlotConfig(this.pageRelation) && this.pageRelation.translated.slotConfig != null) {
                    this.pageRelation.slotConfig = this.pageRelation.translated.slotConfig;
                }

                cmsPage.sections.forEach((section) => {
                    section.blocks.forEach((block) => {
                        block.slots.forEach((slot) => {
                            /**
                             * Check if the slot has been overwritten content
                             * If so, merge the slot config with overwitten config
                             */
                            if (this.pageRelation.slotConfig && this.pageRelation.slotConfig[slot.id]) {
                                if (!this.overwrittenSlotIds.includes(slot.id)) {
                                    this.overwrittenSlotIds.push(slot.id);
                                }

                                if (slot.config === null) {
                                    if (slot.translated.config) {
                                        slot.config = cloneDeep(slot.translated.config);
                                    } else {
                                        slot.config = {};
                                    }
                                }

                                merge(slot.config, cloneDeep(this.pageRelation.slotConfig[slot.id]));
                                merge(slot.translated.config, cloneDeep(this.pageRelation.slotConfig[slot.id]));

                                /**
                                 * If it have the product config
                                 * remove the duplicate productIds from the product config
                                 */
                                if ('products' in slot.config) {
                                    slot.config.products.value = [...new Set(slot.config.products.value)]
                                    slot.translated.config.products.value = [...new Set(slot.translated.config.products.value)]
                                }
                            } else {
                                if (onReset && !this.resetContentSlotIds.includes(slot.id)) {
                                    this.resetContentSlotIds.push(slot.id);
                                }

                                slot.config = cloneDeep(slot.translated.config);
                            }
                        });
                    });
                });

                this.updateCmsPageDataMapping();
                this.isSlotConfigLoading = false;
                Shopware.State.commit('cmsPageState/setCurrentPage', cmsPage);
                return this.cmsPage;
            });
        },

        updateCmsPageDataMapping() {
            Shopware.State.commit('cmsPageState/setCurrentMappingEntity', 'guided_shopping_presentation_cms_page');
            Shopware.State.commit(
                'cmsPageState/setCurrentMappingTypes',
                this.cmsService.getEntityMappingTypes('guided_shopping_presentation_cms_page'),
            );
            Shopware.State.commit('cmsPageState/setCurrentDemoEntity', this.pageRelation);
        },

        async onSave(savePresentation) {
            this.isSaveSuccessful = false;
            const pageOverrides = await this.getCmsPageOverrides();

            if (type.isPlainObject(pageOverrides)) {
                this.pageRelation.slotConfig = cloneDeep(pageOverrides);
            }

            this.resetTempData();

            await savePresentation(true);
        },

        onReset() {
            this.isDirty = true;
            this.pageRelation.slotConfig = null;
            this.pageRelation.translated.slotConfig = null;

            Shopware.State.dispatch('cmsPageState/resetCmsPageState')
                .then(this.getAssignedCmsPage(true));
        },

        async getCmsPageOverrides() {
            if (this.cmsPage === null) {
                return null;
            }

            this.deleteSpecifcKeys(this.cmsPage.sections);
            const changesetGenerator = new ChangesetGenerator();
            const { changes } = changesetGenerator.generate(this.cmsPage);
            const slotOverrides = {};

            if (changes === null) {
                return slotOverrides;
            }

            const promises = [];
            if (type.isArray(changes.sections)) {
                changes.sections.forEach((section) => {
                    if (type.isArray(section.blocks)) {
                        section.blocks.forEach((block) => {
                            if (type.isArray(block.slots)) {
                                block.slots.forEach((slot) => {
                                    /**
                                     * Check if no have changes of data
                                     * and this slot hasn't already been overwritten yet
                                     * If so, skip override content of this slot
                                     */
                                    if (!this.isDirty && !this.overwrittenSlotIds.includes(slot.id)) {
                                        return;
                                    }

                                    /**
                                     * Check is this slot has already been reset the content
                                     * If so, skip override content of this slot
                                     */
                                    if (this.resetContentSlotIds.includes(slot.id)) {
                                        return;
                                    }

                                    /**
                                     * Product config in slot couldn't override automatically by using bind 2 ways
                                     * So, I have to override it in manually
                                     */
                                    if ('products' in slot.config && !slot.config?.products?.value.length) {
                                        promises.push(this.cmsSlotRepository.get(slot.id));
                                    }

                                    slotOverrides[slot.id] = slot.config;
                                });
                            }
                        })
                    }
                });
            }

            await Promise.all(promises).then((response) => {
                response.forEach((cmsSlot) => {
                    slotOverrides[cmsSlot.id].products.value = cmsSlot.translated?.config?.products?.value;
                })

                if (promises.length) {
                    this.createNotificationInfo({
                        message: this.$tc('swag-guided-shopping-presentation.cmsConfig.productSlotConfigRule'),
                    });
                }
            })

            return slotOverrides;
        },

        deleteSpecifcKeys(sections) {
            if (!sections) {
                return;
            }

            sections.forEach((section) => {
                if (!section.blocks) {
                    return;
                }

                section.blocks.forEach((block) => {
                    if (!block.slots) {
                        return;
                    }

                    block.slots.forEach((slot) => {
                        if (!slot.config) {
                            return;
                        }

                        Object.values(slot.config).forEach((configField) => {
                            if (configField.entity) {
                                delete configField.entity;
                            }
                            if (configField.required) {
                                delete configField.required;
                            }
                            if (configField.type) {
                                delete configField.type;
                            }
                        });
                    });
                });
            });
        },

        hasSlotConfig(pageRelation) {
            if (!pageRelation) return false;
            const config = pageRelation.slotConfig;

            if (!config) return false;
            return Object.keys(config).length > 0;
        },

        reloadSlotConfig() {
            if (this.isSlotConfigLoading) return;
            this.isSlotConfigLoading = true
            Shopware.State.dispatch('cmsPageState/resetCmsPageState')
                .then(this.getAssignedCmsPage);
        },

        saveSlotConfig() {
            this.$root.$emit('presentation-save');
        },

        resetTempData() {
            this.overwrittenSlotIds = [];
            this.resetContentSlotIds = [];
            this.isDirty = false;
        }
    },

    watch: {
        cmsPages() {
            if (this.cmsPages?.length) {
                this.initPageRelation();
                this.reloadSlotConfig();
            }
        },
        cmsPageRelationId() {
            this.isDirty = false;

            if (this.isLoading) {
                return;
            }
            this.reloadSlotConfig();
        },
        cmsPage: {
            handler(newValue) {
                if (this.isDirty) return;

                if (newValue) {
                    newValue.sections.forEach((section) => {
                        if (!section.blocks || this.isDirty) {
                            return;
                        }

                        section.blocks.forEach((block) => {
                            if (!block.slots || this.isDirty) {
                                return;
                            }

                            block.slots.forEach((slot) => {
                                if (this.isDirty) {
                                    return;
                                }

                                const slotOldConfigs = slot.translated.config;
                                const slotNewConfigs = slot.config;

                                Object.entries(slotNewConfigs).forEach((entry) => {
                                    if (this.isDirty) {
                                        return;
                                    }

                                    const [key, configObject] = entry;

                                    if (
                                        slot.type === 'product-name' &&
                                        configObject.value === null &&
                                        slotOldConfigs[key].source === 'mapped' &&
                                        slotOldConfigs[key].value === 'product.name'
                                    ) {
                                        return;
                                    }

                                    if (slot.type === 'image-gallery' && key === 'sliderItems' ) {
                                        if (!configObject.length) {
                                            return;
                                        }
                                    }

                                    if (
                                        typeof configObject.value === "object" &&
                                        configObject.value !== null &&
                                        typeof slotOldConfigs[key].value === "object" &&
                                        slotOldConfigs[key].value !== null
                                    ) {
                                        if (
                                          !Object.keys(configObject.value).length &&
                                          !slotOldConfigs[key].value.length
                                        ) {
                                          return;
                                        } else {
                                          if (
                                            JSON.stringify(configObject.value) ===
                                            JSON.stringify(slotOldConfigs[key].value)
                                          ) {
                                            return;
                                          } else {
                                            this.isDirty = true;
                                            return false;
                                          }
                                        }
                                    }

                                    if (configObject.value !== slotOldConfigs[key].value) {
                                        this.isDirty = true;
                                        return;
                                    }
                                });
                            });
                        });
                    });
                }
            },
            deep: true,
        }
    }
});
