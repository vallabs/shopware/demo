import template from './sw-cms-block-notes.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-notes', {
    template,
});
