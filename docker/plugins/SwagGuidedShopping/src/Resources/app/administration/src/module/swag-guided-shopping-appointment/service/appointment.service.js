export default class AppointmentService {

    getUrl(appointment, presentationPath) {
        let domain = 'https://???';
        if (appointment.salesChannelDomain) {
            domain = appointment.salesChannelDomain.url;
        }
        domain = domain.toString().replace(/\/+$/, '');

        let path = '/' + presentationPath + '/' + this._slugify(appointment.presentationPath);
        path = path.toString().replace(/\/\//g, '/');

        return domain + path;
    }

    _slugify(text) {
        if (!text) return '';
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(/ä/g, 'ae')           // Replace ä with ae
            .replace(/ö/g, 'oe')           // Replace ö with oe
            .replace(/ü/g, 'ue')           // Replace u with ue
            .replace(/[^\w\-\/]+/g, '')       // Remove all non-word chars, minuses or slashes
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }
}
