import './sw-entity-single-select.scss';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.override('sw-entity-single-select', {

    data() {
        return {
            presentationSettingPageMapping: {
                detail: [
                    'presentation_product_detail',
                    'product_detail',
                ],
                listing: [
                    'presentation_product_list',
                    'product_list',
                ],
            },
        };
    },

    props: {
        pageType: {
            type: String,
            required: false,
        },
        mailTemplateType: {
            type: String,
            required: false,
        },
    },

    methods: {
        loadData() {
            this.isLoading = true;
            const pageTypes = this.presentationSettingPageMapping[this.pageType] ?? []

            if (
                this.$route.name === "sw.extension.config"
                && this.$route.params.namespace === "SwagGuidedShopping"
            ) {
                const criteria = new Criteria();

                if (pageTypes?.length > 0) {
                    criteria.addFilter(
                        Criteria.equalsAny('type', pageTypes),
                    );
                }

                if (this.mailTemplateType) {
                    criteria.addFilter(
                        Criteria.equals('mailTemplateType.technicalName', this.mailTemplateType),
                    );
                }

                return this.repository.search(criteria, { ...this.context, inheritance: true })
                    .then((result) => {
                        this.displaySearch(result);
                        this.isLoading = false;
                        return result;
                    });
            }

            return this.$super('loadData');
        },
    }
});
