export default {
    namespaced: true,

    state() {
        return {
            presentation: {},
            apiContext: {},
            customFieldSets: [],
            loading: {
                init: false,
                presentation: false,
                parentPresentation: false,
                customFieldSets: false,
            },
            localMode: false,
            currentLanguageId: ''
        };
    },

    getters: {
        isLoading: (state) => {
            return Object.values(state.loading).some((loadState) => loadState);
        },

        showPresentationCard(state, getters) {
            return (key) => {
                if (state.presentation && state.presentation.parentId) {
                    return true;
                }

                const cardKeys = ['essential_characteristics', 'custom_fields', 'labelling'];

                if (cardKeys.includes(key) && !getters.showModeSetting) {
                    return false;
                }

                return state.modeSettings.includes(key);
            };
        },
    },

    mutations: {
        setApiContext(state, apiContext) {
            state.apiContext = apiContext;
        },

        setLocalMode(state, value) {
            state.localMode = value;
        },

        setLoading(state, value) {
            const name = value[0];
            const data = value[1];

            if (typeof data !== 'boolean') {
                return false;
            }

            if (state.loading[name] !== undefined) {
                state.loading[name] = data;
                return true;
            }
            return false;
        },

        setPresentationId(state, presentationId) {
            state.presentationId = presentationId;
        },

        setPresentation(state, newPresentation) {
            state.presentation = newPresentation;
        },

        setCustomFieldSets(state, newCustomFieldSet) {
            state.customFieldSets = newCustomFieldSet;
        },

        setCurrentLanguageId(state, languageId) {
            state.currentLanguageId = languageId;
        }
    },

};
