import template from './sw-cms-layout-modal.html.twig';
import './sw-cms-layout-modal.scss';

const { Component } = Shopware;

Component.override('sw-cms-layout-modal', {
    template,

    data() {
        return {
            listMode: "list",
            showBulkDeleteModal: false,
            isBulkLoading: false,
            selected: [],
            selectedPageObject: [],
        };
    },

    props: {
        multipleSelect: {
            type: Boolean,
            required: false,
            default: false,
        },

        allowDelete : {
            type: Boolean,
            required: false,
            default: false,
        },   
    },

    computed: {
        columnConfig() {
            return [{
                property: 'name',
                label: this.$tc('sw-cms.list.gridHeaderName'),
                inlineEdit: 'string',
                primary: true,
            }, {
                property: 'type',
                label: this.$tc('sw-cms.list.gridHeaderType'),
            }, {
                property: 'createdAt',
                label: this.$tc('sw-cms.list.gridHeaderCreated'),
            },{
                property: 'extensions.createdBy.username',
                label: this.$tc('sw-cms.list.gridHeaderCreatedBy'),
            }, {
                property: 'updatedAt',
                label: this.$tc('sw-cms.list.gridHeaderUpdated'),
            }];
        },

        pageTypes() {
            return {
                page: this.$tc('sw-cms.sorting.labelSortByShopPages'),
                landingpage: this.$tc('sw-cms.sorting.labelSortByLandingPages'),
                product_list: this.$tc('sw-cms.sorting.labelSortByCategoryPages'),
                product_detail: this.$tc('sw-cms.sorting.labelSortByProductPages'),
                presentation_landingpage: this.$tc('sw-cms.sorting.labelSortByPresentationLandingPages'),
                presentation_product_list: this.$tc('sw-cms.sorting.labelSortByPresentationCategoryPages'),
                presentation_product_detail: this.$tc('sw-cms.sorting.labelSortByPresentationProductPages'),
            };
        },

        gridPreSelection() {
            return this.selectedPageObject.reduce((result, item) => {
                result[item.id] = item;
                return result;
            }, {});
        },
    },

    beforeCreate() {
        // remove preSelection watcher
        this.$options.watch.preSelection = null;
    },

    created() {
        // re-assign preSelection watcher
        this.$watch('preSelection', function handler(newSelection) {
            this.selectedPageObject = [];
            this.selected = [];

            if (newSelection) {
                this.selectedPageObject.push(newSelection);
            }

            if (newSelection?.id) {
                this.selected.push(newSelection?.id);
            }
        }, { immediate: true });
    },

    methods: {
        selectItem(page) {
            const layoutId = page.id;

            if (this.selected.includes(layoutId)) {
                this.selected = this.multipleSelect ? this.selected.filter(item => item !== layoutId) : [];
                this.selectedPageObject = this.multipleSelect ? this.selectedPageObject.filter(item => item.id !== page.id) : [];
            } else {
                if (this.multipleSelect) {
                    this.selected.push(layoutId);
                    this.selectedPageObject.push(page);
                } else {
                    this.selected = [layoutId];
                    this.selectedPageObject = [page];
                }
            }
        },

        confirmBulkDeleteLayouts() {
            this.$emit('bulk-delete-confirm', this.selected);
        },

        onSelection(page) {
            this.selectItem(page);
        },

        gridItemClasses(pageId, index) {
            return [
                {
                    'is--selected': this.selectedPageObject.findIndex(item => item.id === pageId) >= 0,
                },
                'sw-cms-layout-modal__content-item',
                `sw-cms-layout-modal__content-item--${index}`,
            ];
        },

        selectLayout() {
            const selected = this.multipleSelect ? this.selected : this.selected[0];
            const selectedPageObject = this.multipleSelect ? this.selectedPageObject : this.selectedPageObject[0];
            this.$emit('modal-layout-select', selected, selectedPageObject);
            this.closeModal();
        },

        selectInGrid(column) {
            const columnEntries = Object.entries(column);
            if (columnEntries.length === 0) {
                [this.selected, this.selectedPageObject] = [[], []];
                return;
            }
            this.selected = columnEntries.map(item => item[0]);
            this.selectedPageObject = columnEntries.map(item => item[1]);
        },

        async deleteLayouts() {
            await this.pageRepository.syncDeleted(this.selected);
            await this.getList();
            this.selected = [];
            this.$refs.gridLayouts.resetSelection();
        },
    },
});
