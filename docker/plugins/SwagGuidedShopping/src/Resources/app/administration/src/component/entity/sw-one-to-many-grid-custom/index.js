import template from './sw-one-to-many-grid-custom.html.twig';

const { Component } = Shopware;

Component.extend('sw-one-to-many-grid-custom', 'sw-one-to-many-grid', {
    template,
    
    props: {
        enableCompactMode: {
            type: Boolean,
            default: true,
            required: false,
        },
    },
});
