import template from './swag-guided-shopping-appointment-send-appointment-modal.html';

const { Component } = Shopware;

Component.register('swag-guided-shopping-appointment-send-appointment-modal', {
    template,

    data() {
        return {
            cancelAppointmentMessage: "",
            isSendMailAll: true,
            sendMailOptions: [
                {
                    name: this.$tc('swag-guided-shopping-appointment.base.attendees.sendMailToAll'),
                    value: true,
                },
                {
                    name: this.$tc('swag-guided-shopping-appointment.base.attendees.sendEmailToNewly'),
                    value: false,
                }
            ]
        }
    },

    methods: {
        onCloseSendAppointmentModal() {
            this.$emit("closeModal");
        },

        onConfirmSendAppointment() {
            this.$emit("confirmSend", this.isSendMailAll);
        },
    }
})
