const { Component, Filter } = Shopware;

Component.override('sw-extension-config', {
    computed: {
        image() {
            if (this.isSwagGuidedShopping()) {
                return Filter.getByName('asset')('swagguidedshopping/static/img/plugin.png');
            } else {
                this.$super('image');
            }
        },

        extensionLabel() {
            if (this.isSwagGuidedShopping()) {
                return 'Digital Sales Rooms';
            } else {
                return this.$super('extensionLabel');
            }
        }
    },

    methods: {
        isSwagGuidedShopping() {
            const namespace = 'SwagGuidedShopping';

            return (this.extension?.name === namespace
                || (this.$route.name === 'sw.extension.config' && this.$route.params.namespace === namespace)
            );
        }
    }
});
