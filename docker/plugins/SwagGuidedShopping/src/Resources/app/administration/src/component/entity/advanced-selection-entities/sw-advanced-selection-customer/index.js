import template from './sw-advanced-selection-customer.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('sw-advanced-selection-customer', {
    template,

    inject: [
        'repositoryFactory',
    ],

    data() {
        return {
            availableAffiliateCodes: [],
            availableCampaignCodes: [],
            filterLoading: false
        };
    },

    computed: {
        customerRepository() {
            return this.repositoryFactory.create('customer');
        },

        customerContext() {
            return { ...Shopware.Context.api, inheritance: true };
        },

        customerColumns() {
            return [
                {
                    property: 'firstName',
                    dataIndex: 'lastName,firstName',
                    inlineEdit: 'string',
                    label: 'sw-customer.list.columnName',
                    routerLink: 'sw.customer.detail',
                    width: '250px',
                    allowResize: true,
                    primary: true,
                    useCustomSort: true,
                }, {
                    property: 'defaultBillingAddress.street',
                    label: 'sw-customer.list.columnStreet',
                    allowResize: true,
                    useCustomSort: true,
                }, {
                    property: 'defaultBillingAddress.zipcode',
                    label: 'sw-customer.list.columnZip',
                    align: 'right',
                    allowResize: true,
                    useCustomSort: true,
                }, {
                    property: 'defaultBillingAddress.city',
                    label: 'sw-customer.list.columnCity',
                    allowResize: true,
                    useCustomSort: true,
                }, {
                    property: 'customerNumber',
                    dataIndex: 'customerNumber',
                    naturalSorting: true,
                    label: 'sw-customer.list.columnCustomerNumber',
                    allowResize: true,
                    inlineEdit: 'string',
                    align: 'right',
                    useCustomSort: true,
                }, {
                    property: 'group',
                    dataIndex: 'group',
                    naturalSorting: true,
                    label: 'sw-customer.list.columnGroup',
                    allowResize: true,
                    inlineEdit: 'string',
                    align: 'right',
                    useCustomSort: true,
                }, {
                    property: 'email',
                    inlineEdit: 'string',
                    label: 'sw-customer.list.columnEmail',
                    allowResize: true,
                    useCustomSort: true,
                }, {
                    property: 'affiliateCode',
                    inlineEdit: 'string',
                    label: 'sw-customer.list.columnAffiliateCode',
                    allowResize: true,
                    visible: false,
                    useCustomSort: true,
                }, {
                    property: 'campaignCode',
                    inlineEdit: 'string',
                    label: 'sw-customer.list.columnCampaignCode',
                    allowResize: true,
                    visible: false,
                    useCustomSort: true,
                }, {
                    property: 'boundSalesChannelId',
                    label: 'sw-customer.list.columnBoundSalesChannel',
                    allowResize: true,
                    visible: false,
                    useCustomSort: true,
                }, {
                    property: 'active',
                    inlineEdit: 'boolean',
                    label: 'sw-customer.list.columnActive',
                    allowResize: true,
                    visible: false,
                    useCustomSort: true,
                }
            ];
        },

        customerFilters() {
            return {
                'affiliate-code-filter': {
                    property: 'affiliateCode',
                    type: 'multi-select-filter',
                    label: this.$tc('sw-customer.filter.affiliateCode.label'),
                    placeholder: this.$tc('sw-customer.filter.affiliateCode.placeholder'),
                    valueProperty: 'key',
                    labelProperty: 'key',
                    options: this.availableAffiliateCodes,
                },
                'campaign-code-filter': {
                    property: 'campaignCode',
                    type: 'multi-select-filter',
                    label: this.$tc('sw-customer.filter.campaignCode.label'),
                    placeholder: this.$tc('sw-customer.filter.campaignCode.placeholder'),
                    valueProperty: 'key',
                    labelProperty: 'key',
                    options: this.availableCampaignCodes,
                },
                'customer-group-request-filter': {
                    property: 'requestedGroupId',
                    type: 'existence-filter',
                    label: this.$tc('sw-customer.filter.customerGroupRequest.label'),
                    placeholder: this.$tc('sw-customer.filter.customerGroupRequest.placeholder'),
                    optionHasCriteria: this.$tc('sw-customer.filter.customerGroupRequest.textHasCriteria'),
                    optionNoCriteria: this.$tc('sw-customer.filter.customerGroupRequest.textNoCriteria'),
                },
                'salutation-filter': {
                    property: 'salutation',
                    label: this.$tc('sw-customer.filter.salutation.label'),
                    placeholder: this.$tc('sw-customer.filter.salutation.placeholder'),
                    labelProperty: 'displayName',
                },
                'account-status-filter': {
                    property: 'active',
                    label: this.$tc('sw-customer.filter.status.label'),
                    placeholder: this.$tc('sw-customer.filter.status.placeholder'),
                },
                'default-payment-method-filter': {
                    property: 'defaultPaymentMethod',
                    label: this.$tc('sw-customer.filter.defaultPaymentMethod.label'),
                    placeholder: this.$tc('sw-customer.filter.defaultPaymentMethod.placeholder'),
                },
                'group-filter': {
                    property: 'group',
                    label: this.$tc('sw-customer.filter.customerGroup.label'),
                    placeholder: this.$tc('sw-customer.filter.customerGroup.placeholder'),
                },
                'billing-address-country-filter': {
                    property: 'defaultBillingAddress.country',
                    label: this.$tc('sw-customer.filter.billingCountry.label'),
                    placeholder: this.$tc('sw-customer.filter.billingCountry.placeholder'),
                },
                'shipping-address-country-filter': {
                    property: 'defaultShippingAddress.country',
                    label: this.$tc('sw-customer.filter.shippingCountry.label'),
                    placeholder: this.$tc('sw-customer.filter.shippingCountry.placeholder'),
                },
                'tags-filter': {
                    property: 'tags',
                    label: this.$tc('sw-customer.filter.tags.label'),
                    placeholder: this.$tc('sw-customer.filter.tags.placeholder'),
                },
            };
        },

        customerAssociations() {
            return [
                'defaultBillingAddress',
                'group',
                'requestedGroup',
                'salesChannel',
                'tags'
            ];
        },

        filterSelectCriteria() {
            const criteria = new Criteria(1, 1);
            criteria.addFilter(Criteria.not(
                'AND',
                [Criteria.equals('affiliateCode', null), Criteria.equals('campaignCode', null)],
            ));
            criteria.addAggregation(Criteria.terms('affiliateCodes', 'affiliateCode', null, null, null));
            criteria.addAggregation(Criteria.terms('campaignCodes', 'campaignCode', null, null, null));

            return criteria;
        },
    },

    created() {
        this.loadFilterValues()
    },

    methods: {
        loadFilterValues() {
            this.filterLoading = true;

            return this.customerRepository.search(this.filterSelectCriteria)
                .then(({ aggregations }) => {
                    this.availableAffiliateCodes = aggregations?.affiliateCodes?.buckets ?? [];
                    this.availableCampaignCodes = aggregations?.campaignCodes?.buckets ?? [];
                    this.filterLoading = false;

                    return aggregations;
                }).catch(() => {
                    this.filterLoading = false;
                });
        },
    }
});
