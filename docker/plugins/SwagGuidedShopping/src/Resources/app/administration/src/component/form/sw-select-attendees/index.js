import template from './sw-select-attendees.html.twig';
import './sw-select-attendees.scss';

const { Component, Context } = Shopware;
const { Criteria, EntityCollection } = Shopware.Data;

Component.register('sw-select-attendees', {
    template,

    inject: [
        'repositoryFactory',
        'feature',
        'ruleConditionDataProviderService',
    ],

    props: {
        invitedAttendees: {
            type: Array,
            required: false,
            default() {
                return [];
            },
        }
    },

    data() {
        return {
            customerCollection: [],
        };
    },
    created() {
        this.loadCustomer();
    },
    computed: {
        customerRepository() {
            return this.repositoryFactory.create('customer');
        },
        optionCriteria() {
            const criteria = new Criteria(1, 25);
            if (this.invitedAttendees.length) {
                const ids = this.invitedAttendees.map(x => x.customerId).filter(Boolean);

                ids.length && criteria.addFilter(Criteria.not('AND', [ Criteria.equalsAny('id', ids) ]));
            }
            return criteria;
        },
    },

    methods: {
        loadCustomer() {
            this.customerCollection = new EntityCollection(
                this.customerRepository.route,
                this.customerRepository.entityName,
                Context.api,
            );
        },
        changeValue(val) {
            this.$emit("change", val);
        },
        getCustomerName(customer) {
            if (!customer) {
                return '';
            }

            let parts = [customer.firstName, customer.lastName];
            parts = parts.filter(Boolean);

            return parts.join(' ') ? `(${parts.join(' ')})` : '';
        },
        getAdvancedSelectionParameters() {
            return {};
        },
    },
});
