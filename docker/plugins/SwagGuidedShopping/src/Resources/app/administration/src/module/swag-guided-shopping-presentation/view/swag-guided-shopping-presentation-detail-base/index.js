import template from './swag-guided-shopping-presentation-detail-base.html.twig';
import './swag-guided-shopping-presentation-detail-base.scss';

const { Component, Mixin } = Shopware;
const { mapPropertyErrors, mapState, mapGetters } = Component.getComponentHelper();

Component.register('swag-guided-shopping-presentation-detail-base', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    data() {
        return {
        };
    },

    computed: {
        ...mapState('swagGuidedShoppingPresentationDetail', [
            'presentation',
            'customFieldSets',
            'loading'
        ]),

        ...mapGetters('swagGuidedShoppingPresentationDetail', [
            'isLoading',
        ]),

        ...mapPropertyErrors('presentation', [
            'name'
        ]),

        customFieldsExists() {
            if (!this.customFieldSets.length > 0) {
                return false;
            }

            return this.customFieldSets.some(set => set.customFields.length > 0);
        },

        showCustomFieldsCard() {
            return !this.isloading && this.customFieldsExists;
        },
    },
});
