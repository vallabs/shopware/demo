const { Component } = Shopware;
const { Criteria, EntityCollection } = Shopware.Data;

Component.extend('sw-entity-attendees-select', 'sw-entity-multi-select', {
    data() {
        return {
            attendeeExists: true,
        };
    },

    methods: {
        resetActiveItem(position = 0) {
            if (this.$refs.swSelectResultList) {
                this.$refs.swSelectResultList.setActiveItemIndex(position);
            }
        },

        search(searchTerm) {
            this.filterSearchGeneratedAttendees();

            Promise.all([this.checkAttendeeExists(this.searchTerm), this.$super('search', searchTerm)]).then(() => {
                if (!this.attendeeExists && this.validateEmail(this.searchTerm)) {
                    const newAttendee = this.repository.create(this.entityCollection.context, -1);
                    newAttendee.email = this.$tc('global.sw-tag-field.listItemAdd', 0, { term: this.searchTerm });

                    this.resultCollection.unshift(newAttendee);
                    this.$nextTick(this.resetActiveItem);
                }
            });
        },

        addItem(item) {
            if (item.id === -1) {
                if (this.isLoading) {
                    return;
                }

                this.addNewAttendeeEmail();
            } else {
                if (this.isSelected(item)) {
                    this.remove(item);
                    return;
                }

                this.$emit('item-add', item);

                const newCollection = EntityCollection.fromCollection(this.currentCollection);
                newCollection.unshift(item);

                this.emitChanges(newCollection);

                this.$refs.selectionList.focus();
                this.$refs.selectionList.select();
                this.searchTerm = '';
            }
        },

        validateEmail(e) {
            var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
            return String(e).search (filter) != -1;
        },

        async addNewAttendeeEmail() {
            const item = this.repository.create(this.entityCollection.context);
            item.email = this.searchTerm;
            this.addItem(item);
            this.searchTerm = '';
            this.criteria.setPage(1);
            this.criteria.setLimit(this.resultLimit);
            this.criteria.setTerm('');
            this.resultCollection = null;
            this.isLoading = true;
            await this.loadData();
            this.resetActiveItem();
            this.isLoading = false;
        },
        checkAttendeeExists(term) {
            if (term.trim().length === 0) {
                this.attendeeExists = true;
                return Promise.resolve();
            }

            const criteria = new Criteria(1, 25);
            criteria.addFilter(
                Criteria.equals('email', term),
            );

            return this.repository.search(criteria, this.context).then((response) => {
                this.attendeeExists = response.total > 0;
            });
        },

        filterSearchGeneratedAttendees() {
            this.resultCollection = this.resultCollection.filter(entity => {
                return entity.id !== -1;
            });
        },
    },
});
