import template from './sw-cms-el-config-image-gallery.html.twig';

const { Component } = Shopware;

Component.override('sw-cms-el-config-image-gallery', {
    template
});
