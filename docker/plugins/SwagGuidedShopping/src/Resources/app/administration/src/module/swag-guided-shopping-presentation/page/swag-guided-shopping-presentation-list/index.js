import template from './swag-guided-shopping-presentation-list.html.twig';


const {Component, Mixin} = Shopware;
const {Criteria} = Shopware.Data;

Component.register('swag-guided-shopping-presentation-list', {
    template,

    inject: ['repositoryFactory', 'acl', 'systemConfigApiService'],

    mixins: [
        Mixin.getByName('listing')
    ],

    data() {
        return {
            presentations: null,
            showDeleteModal: false
        }
    },

    computed: {
        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        presentationRepository() {
            return this.repositoryFactory.create('guided_shopping_presentation');
        },

        presentationCriteria() {
            const criteria = new Criteria(this.page, this.limit);
            var today = new Date();

            criteria.setTerm(this.term);
            criteria.addFilter(Criteria.equals('parentId', null))
            criteria.addAssociation('appointments');
            criteria.addAssociation('appointments.guideUser');
            criteria.getAssociation('appointments').addFilter(
                Criteria.multi('OR',[
                    //is self mode and no longer accessible (self mode get no started and not ended flags)
                    Criteria.multi('AND', [
                        Criteria.equals('mode', 'self'),
                        Criteria.range('accessibleTo', {lte: this.formatDate(today)})
                    ]),

                    //is started and ended
                    Criteria.not('OR', [
                        Criteria.equals('startedAt', null),
                        Criteria.equals('endedAt', null)
                    ])

                ]));
            criteria.addSorting(Criteria.sort('createdAt', 'desc'));

            return criteria;
        },

        columns() {
            return [{
                property: 'name',
                dataIndex: 'name',
                label: this.$tc('swag-guided-shopping-presentation.admin.fields.name'),
                routerLink: 'swag.guided.shopping.presentation.detail',
                inlineEdit: 'string',
                allowResize: true,
                primary: true
            }, {
                property: 'countEndedAppointments',
                label: this.$tc('swag-guided-shopping-presentation.admin.fields.countEndedAppointments'),
                allowResize: true,
                sortable: false
            }, {
                property: 'createdAt',
                label: this.$tc('swag-guided-shopping-presentation.admin.fields.createdAt'),
                allowResize: true,
                sortable: true
            }, {
                property: 'createdBy',
                dataIndex: 'createdBy.username',
                label: this.$tc('swag-guided-shopping-presentation.admin.fields.createdBy'),
                allowResize: true,
                sortable: true
            }, {
                property: 'updatedAt',
                label: this.$tc('swag-guided-shopping-presentation.admin.fields.updatedAt'),
                allowResize: true,
                sortable: true
            }, {
                property: 'updatedBy',
                dataIndex: 'updatedBy.username',
                label: this.$tc('swag-guided-shopping-presentation.admin.fields.updatedBy'),
                allowResize: true,
                sortable: true
            }];
        }
    },

    methods: {
        getList() {
            this.presentationRepository.search(this.presentationCriteria, Shopware.Context.api).then((result) => {
                this.presentations = result;
            });
        },

        resetList() {
            this.getList();
        },

        formatDate(date) {
            return `${date.getFullYear()}-${(`0${date.getMonth() + 1}`).slice(-2)}-${date.getDate()}`;
        },

        onChangeLanguage(languageId) {
            this.getList(languageId);
        },

        getUserName(user) {
            if (!user) {
                return '';
            }
            let parts = [user.firstName, user.lastName];
            parts = parts.filter(Boolean);

            let name = parts.join(' ');

            if (!name) {
                name = user.username;
            }

            return name;
        },

        onDelete(id) {
            this.showDeleteModal = id;
        },

        onCloseDeleteModal() {
            this.showDeleteModal = false;
        },

        onConfirmDelete(id) {
            this.showDeleteModal = false;

            return this.presentationRepository.delete(id).then(() => {
                this.getList();
            });
        },

        onDuplicatePresentation(presentation) {
            const behavior = {
                cloneChildren: false,
                overwrites: {
                    name: `${presentation.name} ${this.$tc('global.default.copy')}`,
                    active: false,
                },
            };

            const presentationId = presentation.id;

            this.presentationRepository.clone(presentationId, Shopware.Context.api, behavior).then((clone) => {
                const criteria = new Criteria();

                this.presentationRepository.get(clone.id, { ...Shopware.Context.api }, criteria).then(async (clonePresentation) => {
                    await this.createDefaultAppointments(clonePresentation);
                    await this.clonePresentationCmsPage(presentationId, clonePresentation)

                    // save presentation
                    await this.presentationRepository.save(clonePresentation, Shopware.Context.api).then(() => {
                        this.resetList();
                    });
                });

            }).catch(() => {
                this.createNotificationError({
                    message: this.$tc('swag-guided-shopping-presentation.detail.defaultDomainIsNotSetError'),
                });
            });
        },

        async createDefaultAppointments(clonePresentation) {
            if (!this.acl.can('guided_shopping_appointment.creator')) return;

            const response = await this.systemConfigApiService.getValues('SwagGuidedShopping');
            const defaultDomain = response['SwagGuidedShopping.config.defaultAppointmentDomain'];
            if (!defaultDomain) return false;

            let appointment = this.appointmentRepository.create(Shopware.Context.api);
            appointment.active = true;
            appointment.mode = 'self';
            appointment.name = clonePresentation.name;
            appointment.attendeeRestrictionType = 'open';
            appointment.presentationPath = appointment.id;
            appointment.videoAudioSettings = 'none';
            appointment.default = true;
            appointment.salesChannelDomainId = defaultDomain;

            clonePresentation.appointments = [appointment];
        },

        async clonePresentationCmsPage(originalPresentationId, clonePresentation) {
            const clonePresentationId = clonePresentation.id;
            const criteria = new Criteria();
            criteria.addAssociation('cmsPages');

            this.presentationRepository.get(originalPresentationId, { ...Shopware.Context.api }, criteria).then(async (presentation) => {
                presentation.cmsPages?.forEach((cmsPage) => {
                    // remove id for clone, not update the original one
                    delete cmsPage['id'];
                    delete cmsPage['createdAt'];
                    delete cmsPage['updatedAt'];

                    const cmsRepository = this.repositoryFactory.create(  // Create CMS Repository
                        clonePresentation.cmsPages.entity,
                        clonePresentation.cmsPages.source
                    );

                    const newCmsPage = cmsRepository.create(); // Create entity
                    Object.keys(cmsPage).forEach((key) => {
                        if (key === 'presentationId') {
                            newCmsPage[key] = clonePresentationId;
                        } else {
                            newCmsPage[key] = cmsPage[key];
                        }
                    });

                    cmsRepository.save(newCmsPage);// save presentation cms page
                    clonePresentation.cmsPages.add(newCmsPage);
                });
            });
        }
    }
});
