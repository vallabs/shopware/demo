import template from './swag-guided-shopping-appointment-list.html.twig';
import './swag-guided-shopping-appointment-list.scss';

const { Component, Mixin, Defaults } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('swag-guided-shopping-appointment-list', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        Mixin.getByName('listing')
    ],

    data() {
        return {
            appointments: null
        }
    },

    computed: {
        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        appointmentCriteria() {
            const criteria = new Criteria(this.page, this.limit);
            criteria.addFilter(Criteria.equals('isPreview', false));

            criteria.setTerm(this.term);
            criteria.addAssociation('presentation');
            criteria.getAssociation('presentation').addFilter(Criteria.equals('versionId', Defaults.versionId));
            criteria.addAssociation('salesChannelDomain.language');

            criteria.addSorting(Criteria.sort('createdAt', 'desc'));

            return criteria;
        },

        columns() {
            return [{
                property: 'name',
                dataIndex: 'name',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.name'),
                routerLink: 'swag.guided.shopping.appointment.detail',
                inlineEdit: 'string',
                allowResize: true,
                primary: true
            }, {
                property: 'presentation',
                dataIndex: 'presentation.name',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.presentation'),
                allowResize: true
            }, {
                property: 'salesChannelDomain.language.name',
                label: this.$tc('swag-guided-shopping-presentation.detailAppointments.columnLanguage'),
                allowResize: true,
                sortable: true
            }, {
                property: 'activity',
                label: this.$tc('swag-guided-shopping-presentation.detailAppointments.columnDate'),
                allowResize: true,
                sortable: false,
            }, {
                property: 'guideUserName',
                dataIndex: 'guideUser.firstName,guideUser.lastName',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.guide'),
                allowResize: true,
            }, {
                property: 'urls',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.urls'),
                allowResize: true,
                sortable: false
            }, {
                property: 'active',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.active'),
                inlineEdit: 'bool',
                allowResize: true,
            }, {
                property: 'createdAt',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.createdAt'),
                allowResize: true,
                sortable: true
            }, {
                property: 'createdBy',
                dataIndex: 'createdBy.username',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.createdBy'),
                allowResize: true,
                sortable: true
            }, {
                property: 'updatedAt',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.updatedAt'),
                allowResize: true,
                sortable: true
            }, {
                property: 'updatedBy',
                dataIndex: 'updatedBy.username',
                label: this.$tc('swag-guided-shopping-appointment.admin.fields.updatedBy'),
                allowResize: true,
                sortable: true
            }];
        }
    },

    methods: {
        getList() {
            this.appointmentRepository.search(this.appointmentCriteria, Shopware.Context.api).then((result) => {
                this.appointments = result;
            });
        },

        onChangeLanguage(languageId) {
            this.getList(languageId);
        },

        getUserName(user) {
            if (!user) {
                return '';
            }
            let parts = [user.firstName, user.lastName];
            parts = parts.filter(Boolean);

            let name = parts.join(' ');

            if (!name) {
                name = user.username;
            }

            return name;
        },

        isAccessible(appointment) {
            if (!appointment.active) return false;

            const from = appointment.accessibleFrom ? new Date(appointment.accessibleFrom).getTime() : 0;
            const to = appointment.accessibleTo ? new Date(appointment.accessibleTo).getTime() : 0;

            return from < Date.now() && (to > Date.now() || !to)
        },

        getPresentationName(presentation) {
            /**
             * If the associated presentation has version id is different from default version id,
             * it means the parent presentation is deleted
             * So, we should not show the appointment's presentation name
             */
            return presentation.versionId === Defaults.versionId ? presentation.translated.name || presentation.name : '';
        }
    }
});
