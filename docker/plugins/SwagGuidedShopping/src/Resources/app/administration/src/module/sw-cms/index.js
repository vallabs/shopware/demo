import './elements';
import './blocks';
import './component';
import './page/sw-cms-create';
import './page/sw-cms-detail';
import './page/sw-cms-list';

import deDe from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Locale.extend('de-DE', deDe);
Shopware.Locale.extend('en-GB', enGB);
