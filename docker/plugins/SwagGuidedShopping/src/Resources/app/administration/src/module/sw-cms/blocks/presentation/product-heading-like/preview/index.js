import template from './sw-cms-preview-product-heading-like.html.twig';
import './sw-cms-preview-product-heading-like.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-product-heading-like', {
    template,
});
