const defaultSearchConfiguration = {
    _searchable: true,
    name: {
        _searchable: true,
        _score: 500 // reference at searchRankingPoint.HIGH_SEARCH_RANKING
    }
};

export default defaultSearchConfiguration;
