import template from './sw-data-grid-settings.html.twig';

const { Component } = Shopware;

Component.override('sw-data-grid-settings', {
    template,

    props: {
        enableCompact: {
            type: Boolean,
            default: true,
            required: false,
        },
    },
});
