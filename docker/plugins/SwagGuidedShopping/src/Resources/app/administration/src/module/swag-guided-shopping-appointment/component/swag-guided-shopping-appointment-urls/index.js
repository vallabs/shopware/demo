import template from './swag-guided-shopping-appointment-urls.html.twig';
import './swag-guided-shopping-appointment-urls.scss';

const { Component, Mixin } = Shopware;

Component.register('swag-guided-shopping-appointment-urls', {
    template,

    inject: ['appointmentService'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    props: {
        appointment: {
            type: Object,
            required: true,
            default: null
        },
        small: {
            type: Boolean,
            required: false,
            default: false
        }
    },

    computed: {
        salesChannelDomainId() {
            return this.appointment.salesChannelDomainId;
        },

        notGuided() {
            return this.appointment.mode === 'self' && !this.appointment.startedAt;
        },

        guideUrl() {
            return this.appointmentService.getUrl(this.appointment, 'guide');
        },

        publicUrl() {
            return this.appointmentService.getUrl(this.appointment, 'presentation');
        },
    },
});
