import template from './swag-guided-shopping-appointment-detail-base.html.twig';
import './swag-guided-shopping-appointment-detail-base.scss';

const { Component, Mixin } = Shopware;
const { mapPropertyErrors, mapState, mapGetters } = Component.getComponentHelper();

Component.register('swag-guided-shopping-appointment-detail-base', {
    template,

    inject: ['repositoryFactory', 'acl', 'gsMailService', 'getCachedAppointmentData', 'setAppointmentForm'],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    props: {
        currentAppointment: {
            type: Object,
            required: false,
            default: null
        }
    },

    data() {
        return {
            showCancelAppointmentModal: false,
            currentCancelRecipient: null,
        };
    },

    created() {
        this.createdComponent();
    },

    computed: {
        ...mapState('swagGuidedShoppingAppointmentDetail', [
            'appointment',
            'loading'
        ]),

        ...mapGetters('swagGuidedShoppingAppointmentDetail', [
            'isLoading',
        ]),

        ...mapPropertyErrors('appointment', [
            'name'
        ]),

        isNameRequired() {
            return true;
        },

        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        cachedAppointmentData() {
            return this.getCachedAppointmentData();
        }
    },

    methods: {
        createdComponent() {
            if (this.currentAppointment != null) {
                Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setAppointment', this.currentAppointment);
                Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setAppointmentId', this.currentAppointment.id);
            }
        },

        onOpenCancelAppointmentModal() {
            this.showCancelAppointmentModal = !!this.appointment;
        },

        onCloseCancelAppointmentModal() {
            this.showCancelAppointmentModal = false;
            this.currentCancelRecipient = null;
        },

        async onConfirmCancelAppointment(id, isSendMail, cancelAppointmentMessage) {
            if (!id && !this.appointment) {
                return;
            }

            try {
                if(this.currentCancelRecipient) {
                    // send email to individually attendees
                    if(isSendMail) {
                        await this.gsMailService.cancelInvitation(id, cancelAppointmentMessage, this.currentCancelRecipient.attendeeEmail);
                    }

                    this.appointment.attendees.remove(this.currentCancelRecipient.id)
                    await this.appointmentRepository.save(this.appointment, Shopware.Context.api);
                    await this.onCloseCancelAppointmentModal();

                    this.$root.$emit('appointment-reload');
                }
                else {
                    this.appointment.active = false;
                    await this.appointmentRepository.save(this.appointment, Shopware.Context.api);

                    // send email to all attendees
                    if(isSendMail) {
                        await this.gsMailService.cancelInvitation(id, cancelAppointmentMessage);
                    }
                    await this.appointmentRepository.delete(id);
                    await this.onCloseCancelAppointmentModal();
                    await this.$router.push({name: 'swag.guided.shopping.appointment.index'});
                }
            } catch(e) {
                this.createNotificationError({
                    message: this.$tc(`global.error-codes.${e.response?.data?.errors[0]?.code}`),
                });
            }
        },

        onCancelRecipient(recipient) {
            this.currentCancelRecipient = recipient;
            this.onOpenCancelAppointmentModal();
        },
    }

});
