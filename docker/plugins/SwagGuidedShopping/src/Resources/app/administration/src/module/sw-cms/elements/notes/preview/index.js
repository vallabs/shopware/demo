import template from './sw-cms-el-preview-notes.html.twig';
import './sw-cms-el-preview-notes.scss';

const { Component } = Shopware;

Component.register('sw-cms-el-preview-notes', {
    template,
});
