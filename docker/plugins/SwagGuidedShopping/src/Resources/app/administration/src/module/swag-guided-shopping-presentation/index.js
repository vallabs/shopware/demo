import './page/swag-guided-shopping-presentation-list';
import './page/swag-guided-shopping-presentation-detail';
import './view/swag-guided-shopping-presentation-detail-base';
import './view/swag-guided-shopping-presentation-detail-appointments';
import './view/swag-guided-shopping-presentation-detail-cms-pages';
import './view/swag-guided-shopping-presentation-detail-cms-config';
import './component/swag-guided-shopping-presentation-product-assignment'
import './acl';
import '../swag-guided-shopping-appointment/acl';
import defaultSearchConfiguration from './search-configuration';

const {Module, Application} = Shopware;

Module.register('swag-guided-shopping-presentation', {
    type: 'plugin',
    name: 'presentation',
    title: 'swag-guided-shopping-presentation.admin.moduleTitle',
    description: 'swag-guided-shopping-presentation.admin.moduleDescription',
    color: '#FFD700',
    icon: 'regular-list-xs',
    entity: 'guided_shopping_presentation',

    routes: {
        index: {
            components: {
                default: 'swag-guided-shopping-presentation-list'
            },
            path: 'index',
            meta: {
                privilege: 'guided_shopping_presentation.viewer',
                appSystem: {
                    view: 'list'
                }
            }
        },
        create: {
            component: 'swag-guided-shopping-presentation-detail',
            path: 'create',
            redirect: {
                name: 'swag.guided.shopping.presentation.create.base'
            },
            meta: {
                privilege: 'guided_shopping_presentation.creator',
            },
            children: {
                base: {
                    component: 'swag-guided-shopping-presentation-detail-base',
                    path: 'base',
                    meta: {
                        parentPath: 'swag.guided.shopping.presentation.index',
                        privilege: 'guided_shopping_presentation.creator',
                    }
                }
            }
        },
        detail: {
            component: 'swag-guided-shopping-presentation-detail',
            path: 'detail/:id?',
            props: {
                default: (route) => ({presentationId: route.params.id})
            },
            redirect: {
                name: 'swag.guided.shopping.presentation.detail.base'
            },
            meta: {
                privilege: 'guided_shopping_presentation.viewer',
                appSystem: {
                    view: 'detail'
                }
            },
            children: {
                base: {
                    component: 'swag-guided-shopping-presentation-detail-base',
                    path: 'base',
                    meta: {
                        parentPath: 'swag.guided.shopping.presentation.index',
                        // Todo
                        // privilege: 'presentation.viewer'
                    }
                },
                cms: {
                    component: 'swag-guided-shopping-presentation-detail-cms-config',
                    path: 'cms/:relationId?',
                    meta: {
                        parentPath: 'swag.guided.shopping.presentation.index',
                        // Todo
                        // privilege: 'presentation.viewer'
                    }
                }
            }
        }
    },

    navigation: [{
        id: 'swag-guided-shopping',
        label: 'swag-guided-shopping-presentation.admin.mainNavigationEntry',
        parent: 'sw-marketing',
        position: 10
    }, {
        label: 'swag-guided-shopping-presentation.admin.presentationNavigationEntry',
        path: 'swag.guided.shopping.presentation.index',
        icon: 'regular-list-xs',
        parent: 'swag-guided-shopping',
        position: 1,
        privilege: 'guided_shopping_presentation.viewer'
    }],

    defaultSearchConfiguration,
});

Application.addServiceProviderDecorator('searchTypeService', searchTypeService => {
    searchTypeService.upsertType('guided_shopping_presentation', {
        entityName: 'guided_shopping_presentation',
        placeholderSnippet: 'swag-guided-shopping-presentation.admin.placeholderSearchBarSnippets',
        listingRoute: 'swag.guided.shopping.presentation.index'
    });

    return searchTypeService;
});

Application.addServiceProviderDecorator('customFieldDataProviderService', customFieldDataProviderService => {
  customFieldDataProviderService.addEntityName('guided_shopping_presentation');
  return customFieldDataProviderService;
});
