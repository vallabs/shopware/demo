const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.override('sw-search-bar', {

    computed: {
        criteriaCollection() {
            const superCriteria = this.$super('criteriaCollection');
            const additionalCriteria = {
                guided_shopping_presentation: new Criteria().addFilter(Criteria.equals('parentId', null)),
                guided_shopping_appointment: new Criteria().addFilter(Criteria.equals('isPreview', false))
            };

            Object.assign(superCriteria, additionalCriteria);

            return superCriteria;
        },
    }
});
