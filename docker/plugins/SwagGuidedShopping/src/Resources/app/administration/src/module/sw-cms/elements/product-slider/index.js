import './config';

const productSlider = Shopware.Service('cmsService').getCmsElementConfigByName('product-slider');

Shopware.Service('cmsService').registerCmsElement(productSlider);
