import template from './sw-cms-create-wizard.html.twig';
import './sw-cms-create-wizard.scss';

const { Component } = Shopware;

Component.override('sw-cms-create-wizard', {
    template,

    data() {
        const pageTypeNames = {
            page: this.$tc('sw-cms.detail.label.pageTypeShopPage'),
            landingpage: this.$tc('sw-cms.detail.label.pageTypeLandingpage'),
            product_list: this.$tc('sw-cms.detail.label.pageTypeCategory'),
            product_detail: this.$tc('sw-cms.detail.label.pageTypeProduct'),
            presentation_landingpage: this.$tc('sw-cms.detail.label.pageTypePresentationLandingpage'),
            presentation_product_list: this.$tc('sw-cms.detail.label.pageTypePresentationCategory'),
            presentation_product_detail: this.$tc('sw-cms.detail.label.pageTypePresentationProduct'),
        };
        const pageTypeIcons = {
            page: 'default-object-lightbulb',
            landingpage: 'regular-dashboard',
            product_list: 'regular-shopping-basket',
            product_detail: 'regular-tag',
            presentation_landingpage: 'regular-dashboard',
            presentation_product_list: 'regular-shopping-basket',
            presentation_product_detail: 'regular-tag',
        };
        return {
            step: 1,
            pageTypeNames,
            pageTypeIcons,
            steps: {
                pageType: 1,
                sectionType: 2,
                pageName: 3
            }
        };
    },

    created() {
        this.createdComponent;
        if (this.presentationMode && this.pageType) {
            this.onPresentationPageTypeSelect(this.pageType)
        }
    },

    computed: {
        pagePreviewMedia() {
            if (this.page.sections.length < 1) {
                return '';
            }
            const imgPath = this.page.type.startsWith('presentation') ? 'swagguidedshopping/static/img/cms' : 'administration/static/img/cms';
            return `url(${this.assetFilter(`${imgPath}/preview_${this.page.type}_${this.page.sections[0].type}.png`)})`;
        },
        presentationMode() {
            return this.$route.query?.referer == "swag.guided.shopping.presentation.detail.base"
        },
        presentationChosen() {
            return this.page.originalType.substring(0,12) === 'presentation';
        },
        referer() {
            if (this.$route.query?.referer) {
                return this.$route.query?.referer;
            }
            return 'sw.cms.index';
        },
        refererId() {
            if (this.$route.query?.refererId) {
                return this.$route.query?.refererId;
            }
            return null;
        },
        pageType() {
            if (this.$route.query?.pageType) {
                return this.$route.query?.pageType;
            }
            return null;
        },
        isNew() {
            return this.$route.query?.new;
        }

    },

    methods: {
        onPresentationPageTypeSelect(type) {
            this.page.type = type;
            this.goToStep('pageName');
            this.$emit('on-section-select', 'default');
        },
    }
});
