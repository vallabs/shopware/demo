import template from "./swag-guided-shopping-appointment-email.twig";
import "./swag-guided-shopping-appointment-email.scss";

const { Component } = Shopware;

Component.register("swag-guided-shopping-appointment-email", {
    template,

    inject: ['gsMailService'],

    props: {
        attendees: {
            type: Array,
            required: true,
            default: () => [],
        },

        valueLimit: {
            type: Number,
            required: false,
            default: 7,
        },
        appointmentId: {
            type: String,
            required: true,
        },

        statusList: {
            type: Array,
            required: false,
            default: () => ["accepted", "maybe", "declined", "pending"],
        },

        colorList: {
            type: Array,
            require: false,
            default: () => ["green", "orange", "red", "gray"],
        },
    },

    data() {
        return {
            limit: this.valueLimit,
            activePopup: null,
            isShowDetailPopup: false,
        };
    },

    computed: {
        visibleValues() {
            if (!this.attendees || this.attendees.length <= 0) {
                return [];
            }
            return this.attendees.slice(0, this.limit);
        },
    },

    methods: {
        onShowMore() {
            this.limit = this.attendees.length;
        },

        getStatusClass(invitationStatus) {
            return invitationStatus.toLowerCase();
        },

        getStatusCount(status) {
            const count = this.attendees.reduce((accumulator, attendee) => {
                if (
                    attendee.invitationStatus === status ||
                    (status === "pending" && !attendee.invitationStatus)
                ) {
                    return accumulator + 1;
                }
                return accumulator;
            }, 0);

            return `${count} ${this.$tc(
                `swag-guided-shopping-appointment.general.${status}`
            )}`;
        },

        displayStatusList() {
            const statusCountArray = this.statusList.map((status) => {
                return this.getStatusCount(status);
            });
            return statusCountArray.join(", ");
        },

        generateColor(status) {
            const index = this.statusList.indexOf(status);
            if (index !== -1) {
                return this.colorList[index];
            }
            return "gray";
        },

        generateStatusText(invitationStatus) {
            return invitationStatus
            ? this.$tc(
                  `swag-guided-shopping-appointment.general.${invitationStatus}`
              )
            : this.$tc(`swag-guided-shopping-appointment.general.pending`);
        },

        onCancelAppointment(attendee) {
            this.$emit("removeAttendee", attendee);
        },

        showDetailPopup(attendeeEmail) {
            this.isShowDetailPopup = attendeeEmail;
        }
    },
});
