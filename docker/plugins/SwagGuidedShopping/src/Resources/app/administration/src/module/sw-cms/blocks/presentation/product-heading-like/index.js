import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'product-heading-like',
    label: 'sw-cms.blocks.presentation.productHeadingLike.label',
    category: 'presentation',
    component: 'sw-cms-block-product-heading-like',
    previewComponent: 'sw-cms-preview-product-heading-like',
    defaultConfig: {
        marginTop: '20px',
        marginLeft: '20px',
        marginBottom: '20px',
        marginRight: '20px',
        sizingMode: 'boxed',
    },
    slots: {
        left: 'product-name',
        right: 'like',
    },
});
