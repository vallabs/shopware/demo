import template from './swag-guided-shopping-presentation-product-assignment.html.twig';
import './swag-guided-shopping-presentation-product-assignment.scss';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;
const { mapPropertyErrors } = Shopware.Component.getComponentHelper();
const ShopwareError = Shopware.Classes.ShopwareError;

Component.register('swag-guided-shopping-presentation-product-assignment', {
    template,

    inject: ['repositoryFactory', 'acl'],

    mixins: [
        'placeholder',
    ],

    props: {
        entity: {
            type: Object,
            required: true,
        },
        pickedProducts: {
            type: Array,
            required: false,
        },
        isLoading: {
            type: Boolean,
            required: true,
        },
    },

    data() {
        return {
            productStreamFilter: null,
            productStreamInvalid: false,
            manualAssignedProductsCount: 0,
            searchableFields: ['name', 'manufacturer.name'],
        };
    },

    computed: {

        productCollection () {
            if (!this.pickedProducts) {
                return this.entity.products;
            }
            return this.pickedProducts;
        },

        productStreamRepository() {
            return this.repositoryFactory.create('product_stream');
        },

        productColumns() {
            return [
                {
                    property: 'name',
                    label: this.$tc('sw-category.base.products.columnNameLabel'),
                    dataIndex: 'name',
                    routerLink: 'sw.product.detail',
                    sortable: false,
                },
            ];
        },

        productCriteria() {
            return (new Criteria(1, 10))
                .addAssociation('options.group')
                .addAssociation('cover.media');
        },

        productStreamInvalidError() {
            if (this.productStreamInvalid) {
                return new ShopwareError({
                    code: 'PRODUCT_STREAM_INVALID',
                    detail: this.$tc('sw-category.base.products.dynamicProductGroupInvalidMessage'),
                });
            }
            return null;
        },

        productAssignmentTypes() {
            return [
                {
                    value: 'product',
                    label: this.$tc('sw-category.base.products.productAssignmentTypeManualLabel'),
                },
                {
                    value: 'product_stream',
                    label: this.$tc('sw-category.base.products.productAssignmentTypeStreamLabel'),
                },
            ];
        },

        dynamicProductGroupHelpText() {
            const link = {
                name: 'sw.product.stream.index',
            };

            const helpText = this.$tc('sw-category.base.products.dynamicProductGroupHelpText.label', 0, {
                link: `<sw-internal-link
                           :router-link=${JSON.stringify(link)}
                           :inline="true">
                           ${this.$tc('sw-category.base.products.dynamicProductGroupHelpText.linkText')}
                       </sw-internal-link>`,
            });

            try {
                // eslint-disable-next-line no-new
                new URL(this.$tc('sw-category.base.products.dynamicProductGroupHelpText.videoUrl'));
            } catch {
                return helpText;
            }

            return `${helpText}
                    <br>
                    <sw-external-link
                        href="${this.$tc('sw-category.base.products.dynamicProductGroupHelpText.videoUrl')}">
                        ${this.$tc('sw-category.base.products.dynamicProductGroupHelpText.videoLink')}
                    </sw-external-link>`;
        },
    },

    watch: {
        'entity.productStreamId'(id) {
            if (!id) {
                this.productStreamFilter = null;
                return;
            }
            this.loadProductStreamPreview();
        },
    },

    created() {
        this.createdComponent();
    },

    methods: {
        createdComponent() {
            if (!this.entity.productAssignmentType) {
                this.entity.productAssignmentType = 'product';
            }
            if (!this.entity.productStreamId) {
                return;
            }
            this.loadProductStreamPreview();
        },

        changedCollection(collection) {
            this.entity.products = collection;
        },

        loadProductStreamPreview() {
            this.productStreamRepository.get(this.entity.productStreamId)
                .then((response) => {
                    this.productStreamFilter = response.apiFilter;
                    this.productStreamInvalid = response.invalid;
                }).catch(() => {
                    this.productStreamFilter = null;
                    this.productStreamInvalid = true;
                });
        },

        onPaginateManualProductAssignment(assignment) {
            this.manualAssignedProductsCount = assignment.total;
        },
    },
});
