import template from './swag-guided-shopping-appointment-detail.html.twig';
import swagGuidedShoppingAppointmentDetailState from './state';
import './swag-guided-shopping-appointment-detail.scss';

const { Component, Mixin } = Shopware;
const { Criteria } = Shopware.Data;
const { mapState, mapGetters } = Shopware.Component.getComponentHelper();
const { cloneDeep } = Shopware.Utils.object;

Component.register('swag-guided-shopping-appointment-detail', {
    template,

    inject: ['repositoryFactory', 'acl', 'gsMailService'],

    provide() {
        return {
            setAppointmentForm: this.setAppointmentForm,
            getCachedAppointmentData: () => this.cachedAppointmentData,
        }
    },

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    shortcuts: {
        'SYSTEMKEY+S': {
            active() {
                return this.acl.can('guided_shopping_appointment.editor');
            },
            method: 'onSave'
        },
        ESCAPE: 'onCancel'
    },

    props: {
        appointmentId: {
            type: String,
            required: false,
            default: null
        }
    },

    data() {
        return {
            appointmentForm: null,
            term: '',
            isSaveSuccessful: false,
            showSendAppointmentModal: false,
            cachedAppointmentData: null,
        };
    },

    computed: {
        ...mapState('swagGuidedShoppingAppointmentDetail', [
            'appointment'
        ]),

        ...mapGetters('swagGuidedShoppingAppointmentDetail', [
            'appointmentRepository',
            'isLoading'
        ]),

        ...mapState('cmsPageState', [
            'currentPage'
        ]),


        creationMode() {
            return !this.appointmentId;
        },

        identifier() {
            return this.appointmentTitle;
        },

        appointmentTitle() {
            // return name
            return this.placeholder(this.appointment, 'name', this.$tc('swag-guided-shopping-appointment.detail.textHeadline'));
        },

        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        appointmentCriteria() {
            const criteria = new Criteria();
            criteria.addAssociation('attendees.customer');
            criteria.addAssociation('attendees.user');
            criteria.getAssociation('attendees')
                .addSorting(Criteria.sort('type', 'DESC')) // type: GUIDE, CLIENT
                .addSorting(Criteria.sort('joinedAt', 'DESC'))
                .addSorting(Criteria.sort('attendeeName'));
            criteria.addAssociation('videoChat');
            return criteria;
        },

        tooltipSave() {
            const systemKey = this.$device.getSystemKey();

            return {
                message: `${systemKey} + S`,
                appearance: 'light'
            };
        },

        tooltipCancel() {
            return {
                message: 'ESC',
                appearance: 'light'
            };
        },
    },

    beforeCreate() {
        Shopware.State.registerModule('swagGuidedShoppingAppointmentDetail', swagGuidedShoppingAppointmentDetailState);
    },

    created() {
        this.createdComponent();
    },

    beforeDestroy() {
        Shopware.State.unregisterModule('swagGuidedShoppingAppointmentDetail');
    },

    destroyed() {
        this.destroyedComponent();
    },

    watch: {
        appointmentId() {
            this.destroyedComponent();
            this.createdComponent();
        }
    },

    methods: {
        createdComponent() {
            Shopware.State.dispatch('cmsPageState/resetCmsPageState');

            // when create
            if (this.creationMode) {
                // set language to system language
                if (!Shopware.State.getters['context/isSystemDefaultLanguage']) {
                    Shopware.State.commit('context/resetLanguageToDefault');
                }
            }

            // initialize default state
            this.initState();

            this.$root.$on('appointment-reload', () => {
                this.loadAll();
            });

        },

        setAppointmentForm(value) {
            this.appointmentForm = value;
        },

        onSearch(value) {
            if (value.length === 0) {
                value = undefined;
            }
            this.term = value;
        },

        destroyedComponent() {
            this.$root.$off('appointment-reload');
        },

        initState() {
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setApiContext', Shopware.Context.api);

            // when appointment exists
            if (this.appointmentId) {
                return this.loadState();
            }

            // When no appointment id exists init state and new appointment with the repositoryFactory
            return this.createState();
        },

        loadState() {
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLocalMode', false);
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setAppointmentId', this.appointmentId);
            Shopware.State.commit('shopwareApps/setSelectedIds', [this.appointmentId]);

            return this.loadAll();
        },

        loadAll() {
            return Promise.all([
                this.loadAppointment()
            ]);
        },

        createState() {
            // set local mode
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLocalMode', true);
            Shopware.State.commit('shopwareApps/setSelectedIds', []);

            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', true]);

            // create empty appointment
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setAppointment', this.appointmentRepository.create(Shopware.Context.api));
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setAppointmentId', this.appointment.id);

            return Promise.all([]).then(() => {
                Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', false]);
            });
        },

        loadAppointment() {
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', true]);

            this.appointmentRepository.get(
                this.appointmentId || this.appointment.id,
                Shopware.Context.api,
                this.appointmentCriteria
            ).then((res) => {
                this.cachedAppointmentData = cloneDeep(res);

                Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setAppointment', res);

                Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', false]);
                if (!res.presentationId) {
                    this.$nextTick(() => {
                        Shopware.State.commit('error/addApiError', {
                            expression: `guided_shopping_appointment.${res.id}.presentationId`,
                            error: {
                                code: 'c1051bb4-d103-4f74-8988-acbcafc7fdc3',
                            },
                        });
                    });
                }
            });
        },


        abortOnLanguageChange() {
            return Shopware.State.getters['swagGuidedShoppingAppointmentDetail/hasChanges'];
        },

        saveOnLanguageChange() {
            return this.onSave();
        },

        onChangeLanguage(languageId) {
            Shopware.State.commit('context/setApiLanguageId', languageId);
            this.initState();
        },

        saveFinish() {
            this.isSaveSuccessful = false;

            if (!this.appointmentId) {
                this.$router.push({name: 'swag.guided.shopping.appointment.detail', params: {id: this.appointment.id}});
            }
        },

        onSave(sendMail) {
            this.isSaveSuccessful = false;

            if (sendMail) {
                if (!this.appointment._isNew && this.appointmentForm.isNewInvitedAttendee) {
                    this.showSendAppointmentModal = true;
                } else {
                    this.onSendMailToAttendees(true);
                }
            } else {
                return this.saveAppointment().then(this.onSaveFinished);
            }
        },

        onSaveFinished(response) {
            const updatePromises = [];

            Promise.all(updatePromises).then(() => {
                switch (response) {
                    case 'empty': {
                        this.isSaveSuccessful = true;
                        Shopware.State.commit('error/resetApiErrors');
                        break;
                    }

                    case 'success': {
                        this.isSaveSuccessful = true;

                        break;
                    }

                    default: {
                        const titleSaveError = this.$tc('global.default.error');
                        const messageSaveError = this.$tc(
                            'global.notification.notificationSaveErrorMessageRequiredFieldsInvalid'
                        );

                        this.createNotificationError({
                            title: titleSaveError,
                            message: messageSaveError
                        });
                        break;
                    }
                }
            });

        },

        onCancel() {
            this.$router.push({name: 'sw.appointment.index'});
        },

        saveAppointment() {
            Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', true]);

            return new Promise((resolve) => {
                // check if appointment exists
                this.appointmentRepository.save(this.appointment, Shopware.Context.api).then(async () => {
                    this.loadAll().then(() => {
                        Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', false]);
                        resolve('success');
                    });
                }).catch((response) => {
                    Shopware.State.commit('swagGuidedShoppingAppointmentDetail/setLoading', ['appointment', false]);
                    resolve(response);
                });
            });
        },

        async onSendMailToAttendees(sendMailToAll = true) {
            // Show error if cannot send mail
            if(this.appointment.mode === 'guided' && (!this.appointment.accessibleFrom || !this.appointment.accessibleTo)) {
                this.createNotificationError({
                    message: this.$tc(`global.error-codes.INVITATION_EMAIL_SENDING_TIME_ERROR`),
                });
                return;
            }

            try {
                await this.saveAppointment().then(this.onSaveFinished);
                await (sendMailToAll
                    ? this.gsMailService.sendInvitationForAll(this.appointment.id)
                    : this.gsMailService.sendInvitation(this.appointment.id)
                );
                this.onCloseSendAppointmentModal();
            } catch(e) {
                this.createNotificationError({
                    message: this.$tc(`global.error-codes.${e.response?.data?.errors[0]?.code}`),
                });
            }
        },

        onCloseSendAppointmentModal() {
            this.showSendAppointmentModal = false;
        },
    },
});
