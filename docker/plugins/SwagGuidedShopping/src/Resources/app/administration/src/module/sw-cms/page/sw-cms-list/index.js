import template from './sw-cms-list.html.twig';
import './sw-cms-list.scss';

const { Component } = Shopware;

Component.override('sw-cms-list', {
    template,

    computed: {
        sortPageTypes() {
            const currentSortPageTypes = this.$super('sortPageTypes');
            currentSortPageTypes.push({
                groupName: this.$tc('sw-cms.sorting.labelSortByPresentationGroup'),
                subTypes: [
                    { value: 'presentation_landingpage', name: this.$tc('sw-cms.sorting.labelSortByLandingPages') },
                    { value: 'presentation_product_list', name: this.$tc('sw-cms.sorting.labelSortByCategoryPages') },
                    { value: 'presentation_product_detail', name: this.$tc('sw-cms.sorting.labelSortByProductPages') }
                ]
            });
            return currentSortPageTypes;
        },

        pageTypes() {
            const currentPageTypes = this.$super('sortPageTypes');
            const presentationPageTypes = {
                presentation_landingpage: this.$tc('sw-cms.sorting.labelSortByPresentationLandingPages'),
                presentation_product_list: this.$tc('sw-cms.sorting.labelSortByPresentationCategoryPages'),
                presentation_product_detail: this.$tc('sw-cms.sorting.labelSortByPresentationProductPages')
            };

            return {...currentPageTypes, ...presentationPageTypes};
        }
    },

    methods: {
        deleteCmsPage(page) {
            this.isLoading = true;
            return this.pageRepository.delete(page.id).then(() => {
                this.resetList();
            }).catch(() => {
                this.isLoading = false;
            });
        },

        getPageType(page) {
            const result = this.$super('getPageType', page);
            if (result) return result;
            const presentationPageTypes = {
                presentation_landingpage: this.$tc('sw-cms.sorting.labelSortByPresentationLandingPages'),
                presentation_product_list: this.$tc('sw-cms.sorting.labelSortByPresentationCategoryPages'),
                presentation_product_detail: this.$tc('sw-cms.sorting.labelSortByPresentationProductPages')
            };
            return presentationPageTypes[page.type];
        },
    }
});
