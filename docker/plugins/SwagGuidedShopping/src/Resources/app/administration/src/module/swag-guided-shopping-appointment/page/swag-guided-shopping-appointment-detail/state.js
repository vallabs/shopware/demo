const { Criteria } = Shopware.Data;

export default {
    namespaced: true,

    state() {
        return {
            appointment: {},
            apiContext: {},
            loading: {
                init: false,
                appointment: false,
            },
            localMode: false,
        };
    },

    getters: {
        isLoading: (state) => {
            return Object.values(state.loading).some((loadState) => loadState);
        },

        showAppointmentCard(state, getters) {
            return (key) => {
                if (state.appointment && state.appointment.parentId) {
                    return true;
                }

                const cardKeys = ['essential_characteristics', 'custom_fields', 'labelling'];

                if (cardKeys.includes(key) && !getters.showModeSetting) {
                    return false;
                }

                return state.modeSettings.includes(key);
            };
        },
    },

    mutations: {
        setApiContext(state, apiContext) {
            state.apiContext = apiContext;
        },

        setLocalMode(state, value) {
            state.localMode = value;
        },

        setLoading(state, value) {
            const name = value[0];
            const data = value[1];

            if (typeof data !== 'boolean') {
                return false;
            }

            if (state.loading[name] !== undefined) {
                state.loading[name] = data;
                return true;
            }
            return false;
        },

        setAppointmentId(state, appointmentId) {
            state.appointmentId = appointmentId;
        },

        setAppointment(state, newAppointment) {
            state.appointment = newAppointment;
        },
    },
};
