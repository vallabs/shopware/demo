const { Component } = Shopware;
const { Criteria, EntityCollection } = Shopware.Data;

Component.override('sw-cms-el-config-product-slider', {
    methods: {
        createdComponent() {
            this.initElementConfig('product-slider');
            this.productCollection = new EntityCollection('/product', 'product', Shopware.Context.api);
            if (this.element.config.products.value.length <= 0) {
                return;
            }

            if (this.element.config.products.source === 'product_stream') {
                this.loadProductStream();
            } else {
                // We have to fetch the assigned entities again
                // ToDo: Fix with NEXT-4830
                const criteria = new Criteria(1, 100);
                criteria.addAssociation('cover');
                criteria.addAssociation('options.group');

                if (this.$route.name === 'swag.guided.shopping.presentation.detail.cms') {
                    const elementId = this.element.id;
                    const slotConfig = this.cmsPageState.currentDemoEntity.translated.slotConfig;
                    const slotConfigLength = slotConfig ? Object.keys(slotConfig).length : 0;

                    if (!slotConfigLength) {
                        criteria.setIds(this.element.config.products.value);
                    } else {
                        this.element.config = slotConfig[elementId];
                        const products = slotConfig[elementId]?.products?.value;
                        if (products && products.length) {
                            this.element.config.products.value = this.element.translated.config.products.value = products;
                        }
                    }
                }

                criteria.setIds(this.element.config.products.value);

                this.productRepository
                    .search(criteria, Object.assign({}, Shopware.Context.api, { inheritance: true }))
                    .then((result) => {
                        this.productCollection = result;
                    });
            }
        },
    }
});
