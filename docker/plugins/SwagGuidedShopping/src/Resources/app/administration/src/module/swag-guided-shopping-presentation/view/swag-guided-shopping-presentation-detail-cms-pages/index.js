import template from './swag-guided-shopping-presentation-detail-cms-pages.html.twig';
import './swag-guided-shopping-presentation-detail-cms-pages.scss';

const { Component, Context, Mixin } = Shopware;
const { Criteria, EntityCollection } = Shopware.Data;
const { mapState, mapGetters } = Component.getComponentHelper();
const Defaults = Shopware.Defaults;
const utils = Shopware.Utils;

Component.register('swag-guided-shopping-presentation-detail-cms-pages', {
    template,

    inject: [
        'repositoryFactory',
        'acl',
        'feature',
        'systemConfigApiService',
        'appointmentService',
    ],

    mixins: [
      Mixin.getByName('notification'),
    ],

    data() {
        return {
            isLoadingData: false,
            searchableFields: ['name'],
            currentCmsPageRelation: null,
            pdpId: null,
            isPDP: false,
            activeProductSelection: '',
            showLayoutSelectionModal: false,
            showBulkDeleteConfirmModal: false,
            bulkDeleteElements: [],
            searchResultIds: [],
            contentOverwrittenIconSize: 32,
            isLoadingPreviewPresentation: false,
        }
    },

    computed: {
        ...mapState('swagGuidedShoppingPresentationDetail', [
            'presentation',
            'customFieldSets',
            'loading',
            'currentLanguageId',
        ]),

        ...mapGetters('context', [
            'isSystemDefaultLanguage',
        ]),

        ...mapGetters('swagGuidedShoppingPresentationDetail', [
            'isLoading',
        ]),

        isLoadingGrid() {
            return this.isLoadingData || this.isLoading;
        },

        assignedCmsPageColumns() {
            return [
                {
                    property: 'position',
                    label: this.$tc('swag-guided-shopping-presentation.detailCmsPages.inputCmsPagePosition'),
                    sortable: false,
                },
                {
                    property: 'title',
                    label: this.$tc('swag-guided-shopping-presentation.detailCmsPages.columnName'),
                    primary: true,
                    sortable: false,
                    inlineEdit: 'string'
                }, {
                    property: 'data',
                    label: this.$tc('swag-guided-shopping-presentation.detailCmsPages.products'),
                    sortable: false
                }, {
                    property: 'sections',
                    label: this.$tc('swag-guided-shopping-presentation.detailCmsPages.columnSections'),
                    sortable: false,
                    align: 'center'
                }
            ];
        },

        pageTypesFlat() {
            return Object.keys(this.pageTypes);
        },

        pageTypes() {
            return {
                presentation_landingpage: this.$tc('sw-cms.detail.label.pageTypeLandingpage'),
                presentation_product_list: this.$tc('sw-cms.detail.label.pageTypeCategory'),
                presentation_product_detail: this.$tc('sw-cms.detail.label.pageTypeProduct'),
            };
        },

        assignmentRepository() {
            return this.repositoryFactory.create(
                this.presentation.cmsPages.entity,
                this.presentation.cmsPages.source
            );
        },

        cmsPageRepository() {
            return this.repositoryFactory.create('cms_page');
        },

        productRepository() {
            return this.repositoryFactory.create('product');
        },

        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        presentationRepository() {
            return this.repositoryFactory.create('guided_shopping_presentation');
        },

        presentationCmsPageRepository() {
            return this.repositoryFactory.create('guided_shopping_presentation_cms_page');
        },

        salesChannelDomainRepository() {
            return this.repositoryFactory.create('sales_channel_domain');
        },

        searchContext() {
            return {
                ...Context.api,
                inheritance: true
            };
        },

        productCriteria() {
            return (new Criteria())
                .addAssociation('options.group')
                .addAssociation('cover.media');
        },

        total() {
            if (!this.presentation.cmsPages || !Array.isArray(this.presentation.cmsPages)) {
                return 0;
            }

            return this.presentation.cmsPages.length;
        },

        onEditPresentationStructure() {
            if (this.isSystemDefaultLanguage) {
                return this.$tc('sw-privileges.tooltip.warning');
            }

            return this.$tc('swag-guided-shopping-presentation.general.buttonChangePresentationStructureWarning');
        },
    },

    methods: {
        getTitle(item) {
            return item.title || item.translated?.title || item.cmsPage?.translated?.name || item.cmsPage?.name;
        },

        openLayoutTab(cmsPageRelationId) {
            this.$router.push({name: 'swag.guided.shopping.presentation.detail.cms', params: {id: this.presentation.id, relationId: cmsPageRelationId}});
        },

        async onToggleCmsPage(cmsPageIds) {
            if (!cmsPageIds.length) return;

            this.isLoadingData = true;

            const cmsPagePromises = cmsPageIds.map(cmsPageId => {
                const newCmsPage = this.assignmentRepository.create();
                newCmsPage.presentationId = this.presentation.id;
                newCmsPage.guidedShoppingPresentationVersionId = this.presentation.versionId;
                newCmsPage.cmsPageId = cmsPageId;
                newCmsPage.position = this.presentation.cmsPages.length + 1;
                newCmsPage.translated = [];
                this.presentation.cmsPages.add(newCmsPage);

                return new Promise(async (resolve) => {
                    const criteria = new Criteria();
                    criteria.addAssociation('sections');
                    const cmsPage = await this.cmsPageRepository.get(cmsPageId, {...Context.api}, criteria);
                    newCmsPage.cmsPage = cmsPage;
                    newCmsPage.cmsPageVersionId = cmsPage.versionId;
                    resolve(cmsPage);
                });
            });

            Promise.all(cmsPagePromises)
                .then(() => {
                    this.$root.$emit('presentation-save');
                }).finally(() => {
                    this.isLoadingData = false;
                });
        },

        async removeItem(item) {
            const oldPosition = item.position;

            this.presentation.cmsPages.remove(item.id);
            this.presentation.cmsPages.forEach((assignedCmsPage) => {
                if (assignedCmsPage.position <= oldPosition) {
                    return;
                }

                assignedCmsPage.position -= 1;
            });

            await this.updatePreviewPresentation();
        },

        isSelected(item) {
            return false;
        },

        openInPageBuilder(cmsPageId, pageType = null) {
            const queryParams = {
                referer: this.$route.name,
                refererId: this.presentation.id,
                refererName: this.presentation.name,
            };

            if (pageType) {
                queryParams.pageType = pageType;
            }

            if (!cmsPageId) {
                this.$router.push({name: 'sw.cms.create', query: queryParams});
            } else {
                this.$router.push({name: 'sw.cms.detail', params: {id: cmsPageId}, query: queryParams});
            }
        },

        async onEditProductAssignment(relation) {
            if (relation.productStreamId) {
                relation.productAssignmentType = 'product_stream';
            }

            if (!relation.products) {
                relation.products = await this.createProductCollection(relation.pickedProductIds);
            }

            if (relation.cmsPage.type === 'presentation_product_detail') {
                this.isPDP = true;
                this.pdpId = relation.productId;
            }

            this.currentCmsPageRelation = relation;
        },

        createProductCollection(ids) {
            if (ids && ids.length) {
                const criteria = new Criteria();
                criteria.setIds(ids);
                criteria.addAssociation('cover.media');
                criteria.addAssociation('options.group');
                return this.productRepository.search(criteria, { ...Context.api, inheritance: true });
            }
            return new EntityCollection('/product', 'product', { ...Context.api, inheritance: true });
        },

        async onSaveProductModal() {
            const relation = this.currentCmsPageRelation;

            /**
             * reset productStreamId
             * if choose Manual selection for selecting product(s)
             */
            if (relation.productAssignmentType === 'product') {
                relation.productStreamId = null;
            }

            /**
             * reset pickedProductIds
             * if choose Dynamic product group for selecting product(s)
             */
            if (relation.productAssignmentType === 'product_stream') {
                relation.pickedProductIds = null;
            } else {
                relation.pickedProductIds = relation.products.getIds();
            }

            if (this.pdpId) {
                this.currentCmsPageRelation.productId = this.pdpId;
            }

            this.currentCmsPageRelation = null;
            this.isPDP = false;

            await this.updatePreviewPresentation();
        },

        onCloseCmsModal() {
            this.currentCmsPageRelation = null;
            this.pdpId = null;
            this.isPDP = false;
        },

        onCloseBulkDeleteConfirmModal() {
            this.bulkDeleteElements =[];
            this.showBulkDeleteConfirmModal = false;
        },

        onOpenBulkDeleteConfirmModal(data) {
            this.bulkDeleteElements = data;
            this.showBulkDeleteConfirmModal = true;
        },

        onConfirmBulkDelete() {
            this.onCloseBulkDeleteConfirmModal();
            this.$refs.cmsLayoutModal.deleteLayouts();
        },

        isProductRelationEmpty(relation) {
            return !(relation.productStreamId || (relation.pickedProductIds && relation.pickedProductIds.length) || relation.productId);
        },

        shouldShowProductSingleSelect(relationId) {
            return this.activeProductSelection === relationId;
        },

        showProductSingleSelect(relationId) {
            this.activeProductSelection = relationId;
        },

        async changeSingleProduct() {
            this.activeProductSelection = '';
            await this.updatePreviewPresentation();
        },

        getCmsPreviewMediaIds(cmsPage) {
            const ids = [];

            if (cmsPage.previewMediaId) {
                ids.push(cmsPage.previewMediaId);
            }

            const sections = cmsPage?.sections;
            if (sections) {
                sections.forEach((section) => {
                    const slot = section.blocks[0]?.slots[0]

                    const mediaConfig = slot?.translated?.config?.media;
                    if (mediaConfig) {
                        ids.push(mediaConfig.value);
                    }
                });
            }

            return ids;
        },

        selectExistingLayout() {
            this.showLayoutSelectionModal = true;
        },

        closeLayoutModal() {
            this.showLayoutSelectionModal = false;
        },

        onCancelInlineEdit() {
            this.$root.$emit('presentation-reload');
        },

        async onInlineEditSave() {
            await this.updatePreviewPresentation();
        },

        async onChangePosition() {
            await this.updatePreviewPresentation();
        },

        async openPreviewPresentation() {
            if (this.isLoadingPreviewPresentation) {
                return;
            }

            this.isLoadingPreviewPresentation = true;
            const previewCmsPages = this.presentation.cmsPages;
            const presentationId = this.presentation.id;

            const previewPresentation = await this.getPreviewPresentation(presentationId);
            let previewAppointment = null;

            if (previewPresentation) {
                await this.updatePreviewPresentationWithCmsPages(previewPresentation, previewCmsPages);
                await this.presentationRepository.save(previewPresentation);

                if (previewPresentation.appointments?.first()) {
                    previewAppointment = previewPresentation.appointments?.first();
                } else {
                    previewAppointment = await this.createPreviewAppointment();
                    if (!previewAppointment) {
                        this.isLoadingPreviewPresentation = false;
                        this.createNotificationError({
                            message: this.$tc('swag-guided-shopping-presentation.general.emptySalesChannelDomain'),
                        });
                        return;
                    }

                    previewPresentation.appointments = [previewAppointment];
                    await this.presentationRepository.save(previewPresentation);
                }
            } else {
                previewAppointment = await this.createPreviewAppointment();
                if (!previewAppointment) {
                    this.isLoadingPreviewPresentation = false;
                    this.createNotificationError({
                        message: this.$tc('swag-guided-shopping-presentation.general.emptySalesChannelDomain'),
                    });
                    return;
                }

                await this.createPreviewPresentation(previewCmsPages, previewAppointment);
            }

            const previewUrl = await this.appointmentService.getUrl(previewAppointment, 'guide');

            const newWindow = window.open(previewUrl);
            if (!newWindow) {
              this.createNotificationInfo({
                message: this.$tc('swag-guided-shopping-presentation.general.browserPopupBlocked'),
              });
            } else {
              newWindow.focus();
              this.createNotificationInfo({
                message: this.$tc('swag-guided-shopping-presentation.general.successPreviewPresentation'),
              });
            }

            this.isLoadingPreviewPresentation = false;
        },

        onSearchTermChange(searchTerm) {
            this.isLoadingData = true;

            const criteria = new Criteria();
            criteria.addAssociation('cmsPage');
            criteria.addFilter(Criteria.equals('presentationId', this.presentation.id));
            criteria.addFilter(Criteria.equals('guidedShoppingPresentationVersionId', Defaults.versionId));
            criteria.addSorting(Criteria.sort('position', 'ASC'));

            if (searchTerm) {
                criteria.addFilter(Criteria.contains('cmsPage.name', searchTerm));
                this.presentationCmsPageRepository.searchIds(criteria).then((response) => {
                    this.searchResultIds = response.data;
                    return Promise.resolve();
                });
            } else {
                this.searchResultIds = [];
            }

            this.isLoadingData = false;
        },

        hasSlotConfig(item) {
            return item.slotConfig && Object.keys(item.slotConfig).length > 0;
        },

        async getPreviewPresentation(presentationId) {
            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('parentId', presentationId));
            criteria.addAssociation('appointments')

            const previewPresentations = await this.presentationRepository.search(criteria);
            if (previewPresentations.first()) {
                return previewPresentations.first();
            }
            return null;
        },

        async updatePreviewPresentationWithCmsPages(previewPresentation, previewCmsPages) {
            await this.deleteAllPreviewPresentationCmsPages(previewPresentation.id);

            previewCmsPages.forEach((previewCmsPage) => {
                // remove id for clone, not update the original one
                delete previewCmsPage['id'];
                delete previewCmsPage['createdAt'];
                delete previewCmsPage['updatedAt'];

                /**
                 * clone presentationCmsPage with previewPresentationId
                 */
                const newCmsPage = this.assignmentRepository.create();
                Object.keys(previewCmsPage).forEach((key) => {
                    if (key === 'presentationId') {
                        newCmsPage[key] = previewPresentation.id;
                    } else {
                        newCmsPage[key] = previewCmsPage[key];
                    }
                });

                previewPresentation.cmsPages.add(newCmsPage)
            });

            return previewPresentation;
        },

        async deleteAllPreviewPresentationCmsPages(previewPresentationId) {
            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('presentationId', previewPresentationId));

            const result = await this.presentationCmsPageRepository.searchIds(criteria);
            if (result.total) {
                await this.presentationCmsPageRepository.syncDeleted(result.data);
            }
        },

        async createPreviewPresentation(previewCmsPages, previewAppointment = null) {
            if (!this.isSystemDefaultLanguage) {
                await Shopware.State.commit('context/resetLanguageToDefault');
            }
            const previewPresentationId = utils.createId();
            const previewPresentation = this.presentationRepository.create(Shopware.Context.api, previewPresentationId);
            previewPresentation.active = true;
            previewPresentation.name = `preview-${this.presentation.name}`;
            previewPresentation.parentId = this.presentation.id;
            await this.updatePreviewPresentationWithCmsPages(previewPresentation, previewCmsPages)

            if (previewAppointment) {
                previewPresentation.appointments = [previewAppointment];
            }

            await this.presentationRepository.save(previewPresentation);
            await Shopware.State.commit('context/setApiLanguageId', this.currentLanguageId);
        },

        async updatePreviewPresentation() {
            const presentationId = this.presentation.id;
            const previewCmsPages = this.presentation.cmsPages;

            const previewPresentation = await this.getPreviewPresentation(presentationId);
            if (previewPresentation) {
                await this.updatePreviewPresentationWithCmsPages(previewPresentation, previewCmsPages);
                await this.presentationRepository.save(previewPresentation);
            }
        },

        async createPreviewAppointment() {
            const response = await this.systemConfigApiService.getValues('SwagGuidedShopping');
            const defaultDomainId = response['SwagGuidedShopping.config.defaultAppointmentDomain'];
            if (!defaultDomainId) return null;

            const salesChannelDomain = await this.salesChannelDomainRepository.search((new Criteria()).setIds([defaultDomainId]));
            if (!salesChannelDomain.total) return null;

            const previewAppointment = this.appointmentRepository.create();
            previewAppointment.active = true;
            previewAppointment.mode = 'guide';
            previewAppointment.name = `${this.presentation.name} - Preview appointment`;
            previewAppointment.attendeeRestrictionType = 'open';
            previewAppointment.presentationPath = Math.random().toString(36).substring(2,7) + this.presentation.id;
            previewAppointment.isPreview = true;
            previewAppointment.videoAudioSettings = 'none';
            previewAppointment.default = false;
            previewAppointment.salesChannelDomainId = defaultDomainId;
            previewAppointment.salesChannelDomain = salesChannelDomain[0];

            return previewAppointment;
        },
    }
});
