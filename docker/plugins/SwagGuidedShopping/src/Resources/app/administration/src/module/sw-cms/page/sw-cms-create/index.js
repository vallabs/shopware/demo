import template from './sw-cms-create.html.twig';

const { Component } = Shopware;

Component.override('sw-cms-create', {
    template,

    computed: {
        cmsPageTypes() {
            return {
                page: this.$tc('sw-cms.detail.label.pageTypeShopPage'),
                landingpage: this.$tc('sw-cms.detail.label.pageTypeLandingpage'),
                product_list: this.$tc('sw-cms.detail.label.pageTypeCategory'),
                product_detail: this.$tc('sw-cms.detail.label.pageTypeProduct'),
                presentation_landingpage: this.$tc('sw-cms.detail.label.pageTypePresentationLandingpage'),
                presentation_product_list: this.$tc('sw-cms.detail.label.pageTypePresentationCategory'),
                presentation_product_detail: this.$tc('sw-cms.detail.label.pageTypePresentationProduct'),

            };
        },
    },

    methods: {
        onWizardComplete() {
            this.onPageTypeChange();

            this.wizardComplete = true;
            this.onSave();
        },

        onSave() {
            if (this.page.originalType) {
                this.page.type = this.page.originalType;
            }

            this.isSaveSuccessful = false;

            if ((this.isSystemDefaultLanguage && !this.page.name) || !this.page.type) {
                const warningMessage = this.$tc('sw-cms.detail.notification.messageMissingFields');
                this.createNotificationWarning({
                    message: warningMessage,
                });

                return Promise.reject();
            }

            this.deleteEntityAndRequiredConfigKey(this.page.sections);

            this.isLoading = true;

            if (this.$route.query?.refererId) {
                this.page.customFields = this.page.customFields || {};
                this.page.customFields.refererId = this.$route.query?.refererId;
                this.page.customFields.referer = this.$route.query?.referer;
                this.page.customFields.refererName = this.$route.query?.refererName ;
            }

            return this.pageRepository.save(this.page).then(() => {
                this.isLoading = false;
                this.isSaveSuccessful = true;

                this.$route.query.new = true;

                this.$router.push({ name: 'sw.cms.detail', params: { id: this.page.id }, query: this.$route.query });
            }).catch((exception) => {
                this.isLoading = false;

                this.createNotificationError({
                    message: exception.message,
                });

                return Promise.reject(exception);
            });
        },
    },
});
