import './component';
import './preview';

Shopware.Service('cmsService').registerCmsBlock({
    name: 'image-gallery-big',
    label: 'sw-cms.blocks.image.imageGalleryBig.label',
    category: 'image',
    component: 'sw-cms-block-image-gallery-big',
    previewComponent: 'sw-cms-preview-image-gallery-big',
    defaultConfig: {
        marginBottom: '20px',
        marginTop: '20px',
        marginLeft: '20px',
        marginRight: '20px',
        sizingMode: 'boxed'
    },
    slots: {
        imageGallery: {
            type: 'image-gallery',
            default: {
                config: {
                    bigImageMode: { source: 'static', value: true }
                },
            }
        },
    }
});
