import template from './sw-cms-block-image-gallery-big.html.twig';

const { Component } = Shopware;

Component.register('sw-cms-block-image-gallery-big', {
    template
});
