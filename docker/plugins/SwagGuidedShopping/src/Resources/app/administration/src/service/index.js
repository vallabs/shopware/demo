/**
 * @package customer-order
 */

import GSMailApiService from './gs-mail.api.service';

Shopware.Service().register('gsMailService', () => {
    const initContainer = Shopware.Application.getContainer('init');
    return new GSMailApiService(initContainer.httpClient, Shopware.Service('loginService'));
});
