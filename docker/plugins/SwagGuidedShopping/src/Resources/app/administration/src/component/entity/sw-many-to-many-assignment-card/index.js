import template from './sw-many-to-many-assignment-card.html.twig';
import './sw-many-to-many-assignment-card.scss';

const { Component } = Shopware;

Component.override('sw-many-to-many-assignment-card', {
    template,
});
