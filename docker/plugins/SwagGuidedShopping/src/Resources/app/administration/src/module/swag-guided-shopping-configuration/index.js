import './page/swag-guided-shopping-configuration-list';

const {Module} = Shopware;

Module.register('swag-guided-shopping-configuration', {
    type: 'plugin',
    name: 'configuration',
    title: 'swag-guided-shopping-configuration.admin.moduleTitle',
    description: 'swag-guided-shopping-configuration.admin.moduleDescription',
    color: '#FFD700',
    icon: 'regular-list-xs',

    routes: {
        index: {
            component: 'swag-guided-shopping-configuration-list',
            path: 'index'
        },
    },

    navigation: [{
        label: 'swag-guided-shopping-configuration.admin.configurationNavigationEntry',
        path: 'swag.guided.shopping.configuration.index',
        parent: 'swag-guided-shopping',
        position: 3
    }],
});
