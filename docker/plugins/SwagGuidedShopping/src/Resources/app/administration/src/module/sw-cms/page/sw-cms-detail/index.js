import template from './sw-cms-detail.html.twig';

const { Component } = Shopware;

Component.override('sw-cms-detail', {
    template,

    data() {
        return {
            productPresentationDetailBlocks: [
                {
                    type: 'cross-selling',
                    elements: [
                        {
                            slot: 'content',
                            type: 'cross-selling',
                            config: {},
                        },
                    ],
                },
                {
                    type: 'product-description-reviews',
                    elements: [
                        {
                            slot: 'content',
                            type: 'product-description-reviews',
                            config: {},
                        },
                    ],
                },
                {
                    type: 'gallery-buybox',
                    elements: [
                        {
                            slot: 'left',
                            type: 'image-gallery',
                            config: {},
                        },
                        {
                            slot: 'right',
                            type: 'buy-box',
                            config: {},
                        },
                    ],
                },
                {
                    type: 'product-heading-like',
                    elements: [
                        {
                            slot: 'left',
                            type: 'product-name',
                            config: {},
                        },
                        {
                            slot: 'right',
                            type: 'like',
                            config: {},
                        },
                    ],
                },
            ]
        };
    },

    computed: {
        cmsPageTypes() {
            return {
                page: this.$tc('sw-cms.detail.label.pageTypeShopPage'),
                landingpage: this.$tc('sw-cms.detail.label.pageTypeLandingpage'),
                product_list: this.$tc('sw-cms.detail.label.pageTypeCategory'),
                product_detail: this.$tc('sw-cms.detail.label.pageTypeProduct'),
                presentation_landingpage: this.$tc('sw-cms.detail.label.pageTypePresentationLandingpage'),
                presentation_product_list: this.$tc('sw-cms.detail.label.pageTypePresentationCategory'),
                presentation_product_detail: this.$tc('sw-cms.detail.label.pageTypePresentationProduct'),

            };
        },
        loadPageCriteria() {
            const criteria = this.$super('loadPageCriteria');
            criteria.getAssociation('sections').addAssociation('translations'); // add translations extension
            return criteria;
        },
        referer() {
            if (this.$route.query?.referer) {
                return this.$route.query?.referer;
            }
            return 'sw.cms.index';
        },
        refererId() {
            if (this.$route.query?.refererId) {
                return this.$route.query?.refererId;
            }
            return null;
        },
        isNew() {
            return this.$route.query?.new;
        }
    },

    watch: {
        page() {
            this.holdOriginalPageType()
        },
    },


    methods: {
        onCloseClick() {
            this.closeDetailPage();
        },

        closeDetailPage() {
            let temp = {};
            if (this.refererId) {
                temp = {
                    name: this.referer,
                    params: { id:  this.refererId }
                };
                if (this.isNew) {
                    temp.query = { newCmsPageId: this.page.id };
                }
                this.$router.push(temp);
            } else {
                this.$router.push({
                    name: 'sw.cms.index'
                });
            }
        },
        isProductPageElement(slot) {
            return this.$super('isProductPageElement', slot) || ['like'].includes(slot.type);
        },
        holdOriginalPageType() {
            if (!this.page.originalType) {
                this.page.originalType = this.page.type;
            }
            if (this.page.originalType === 'presentation_product_detail' || this.page.type === 'presentation_product_detail') {
                this.page.type = 'product_detail';
            }
        },

        processProductDetailType() {
            if (this.page.originalType === 'presentation_product_detail') {
                this.productDetailBlocks = this.productPresentationDetailBlocks;
            }
            this.$super('processProductDetailType');
        },


        onPageTypeChange() {
            this.holdOriginalPageType();

            if (this.page.type === 'product_list' || this.page.type === 'presentation_product_list') {
                this.processProductListingType();
            } else {
                this.page.sections.forEach((section) => {
                    section.blocks.forEach((block) => {
                        if (block.type === 'product-listing') {
                            section.blocks.remove(block.id);
                        }
                    });
                });
            }

            if (this.page.type === 'product_detail') {
                this.processProductDetailType();
            }

            this.checkSlotMappings();
            this.onPageUpdate();
        },

        onSaveEntity() {
            if (this.page.originalType) {
                this.page.type = this.page.originalType;
            }

            this.$super('onSaveEntity');
        },
    }
});
