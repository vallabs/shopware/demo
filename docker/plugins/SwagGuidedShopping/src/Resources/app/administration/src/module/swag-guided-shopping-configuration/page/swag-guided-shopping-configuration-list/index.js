import template from './swag-guided-shopping-configuration-list.html.twig';

const {Component} = Shopware;

Component.register('swag-guided-shopping-configuration-list', {
    template,

    created() {
        this.$router.replace({name: 'sw.extension.config', params: {namespace: 'SwagGuidedShopping'}});
    },

    data() {
        return {}
    },
});
