import './config';

const imageGallery = Shopware.Service('cmsService').getCmsElementConfigByName('image-gallery');

imageGallery.defaultConfig.bigImageMode = {
    source: 'static',
    value: null
};

Shopware.Service('cmsService').registerCmsElement(imageGallery);
