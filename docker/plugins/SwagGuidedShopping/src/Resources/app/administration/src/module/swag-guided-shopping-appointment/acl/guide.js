// if you change something here please also be aware of the method "isAllowedToGuide"
Shopware.Service('privileges').addPrivilegeMappingEntry({
    category: 'permissions',
    parent: 'guided_shopping',
    key: 'guided_shopping_guide',
    roles: {
        editor: {
            privileges: [
                'guided_shopping_appointment:update',
                'guided_shopping_presentation:read',
                'guided_shopping_appointment:read',
                'guided_shopping_appointment_attendee:read',
                'guided_shopping_appointment_attendee:create',
                'guided_shopping_appointment_attendee:update',
                'guided_shopping_appointment_video_chat:read',
                'guided_shopping_appointment_video_chat:update',
                'guided_shopping_appointment_video_chat:delete',
                'guided_shopping_attendee_product_collection:read',
                'guided_shopping_attendee_product_collection:create',
                'guided_shopping_attendee_product_collection:delete',
                'guided_shopping_presentation_cms_page:read',
                'guided_shopping_presentation_cms_page:update',
                'guided_shopping_presentation_cms_page:create',
                'guided_shopping_presentation_cms_page_translation:read',
                'guided_shopping_presentation_cms_page_translation:update',
                'guided_shopping_presentation_cms_page_translation:create',
                'guided_shopping_presentation_translation:read',
                'customer:read',
                'customer_address:read',
                'country:read',
                'order_customer:read',
                'order:read',
                'order_line_item:read',
                'state_machine_state:read',
                'product_stream:read',
                'product:read',
                'system_config:read'
            ],
            dependencies: [
                'guided_shopping_appointment.viewer',
                'guided_shopping_presentation.viewer',
                'guided_shopping_appointment.editor',
                'guided_shopping_presentation.editor',
                'guided_shopping_appointment.creator',
                'guided_shopping_presentation.creator',
            ]
        }
    }
});