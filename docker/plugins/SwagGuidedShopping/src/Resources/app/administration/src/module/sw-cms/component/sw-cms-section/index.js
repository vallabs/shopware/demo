const { Component } = Shopware;

Component.override('sw-cms-section', {
    methods: {
        createdComponent() {
            if (!this.section.backgroundMediaMode) {
                this.section.backgroundMediaMode = 'cover';
            }

            /**
             * Find the translation by the current language id
             * And assign translated name to the name field
             */
            const translation = this.section.extensions.translations.find(element => element.languageId === Shopware.Context.api.languageId);
            if (translation && translation.name) {
                this.section.name = translation.name;
            }

            if (this.cmsPageState.selectedSection && this.cmsPageState.selectedSection.id === this.section.id) {
                this.$store.dispatch('cmsPageState/setSection', this.section); // trigger event to load the save changes
            }
        },
    }
});
