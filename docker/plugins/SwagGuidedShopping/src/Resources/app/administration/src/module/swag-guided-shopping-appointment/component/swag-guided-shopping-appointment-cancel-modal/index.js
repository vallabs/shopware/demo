import template from './swag-guided-shopping-appointment-cancel-modal.html';
import "./swag-guided-shopping-appointment-cancel-modal.scss";

const { Component } = Shopware;

Component.register('swag-guided-shopping-appointment-cancel-modal', {
    template,

    props: {
        appointment: {
            type: Object,
            required: false,
            default: null
        },
    },

    data() {
        return {
            cancelAppointmentMessage: "",
        }
    },

    computed: {
        appointmentName() {
            return this.appointment.name;
        },
    },

    methods: {
        onCloseCancelAppointmentModal() {
            this.$emit("closeModal");
        },

        onConfirmCancelAppointment(id, isSendMail) {
            this.$emit("confirmCancel", id, isSendMail, this.cancelAppointmentMessage);
        },
    }
})
