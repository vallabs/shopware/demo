import template from './swag-guided-shopping-product-variant-info.html.twig';
import './swag-guided-shopping-product-variant-info.scss';

const { Component } = Shopware;

Component.override('sw-product-variant-info', {
    template,

    props: {
        media: {
            type: Object,
            required: false,
            default: null
        },
    },

    computed: {
        titleClasses() {
            if (this.media) {
                return "has-media";
            }
        }
    },
    watch: {
        helpText() {
            if (this.variations && this.variations.length > 0) {
                this.tooltipWidth = 500;
            } else {
                this.tooltipWidth = 200;
            }
        }
    },
});
