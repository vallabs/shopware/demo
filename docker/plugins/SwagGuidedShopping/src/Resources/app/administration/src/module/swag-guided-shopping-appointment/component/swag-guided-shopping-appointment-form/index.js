import template from './swag-guided-shopping-appointment-form.html.twig';
import './swag-guided-shopping-appointment-form.scss';

const { Component, Mixin, Context, Defaults } = Shopware;
const { mapPropertyErrors } = Component.getComponentHelper();
const { Criteria, EntityCollection } = Shopware.Data;
const types = Shopware.Utils.types;
const DEFAULT_APPOINTMENT_DURATION = 30;

Component.register('swag-guided-shopping-appointment-form', {
    template,

    inject: [
        'mailService',
        'repositoryFactory',
        'acl',
        'feature',
        'systemConfigApiService',
        'appointmentService'
    ],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder')
    ],

    props: {
        appointment: {
            type: Object,
            required: false,
            default: null
        },
        isLoading: {
            type: Boolean,
            required: false,
            default: false
        },
        isModal: {
            type: Boolean,
            required: false,
            default: false
        },
        cachedAppointmentData: {
            type: Object,
            required: false,
            default: null
        },
    },

    data() {
        return {
            showVideoConfiguration: false,
            appointmentPresentationPath: '',
            isLoadingCustomerData: false,
            defaultDomain: null,
            customFieldSets: [],
            customerCollection: [],
            mailPreview: null,
            sendInvitationMailTemplateId: null,
            sendInvitationMailTemplateType: null,
            sendInvitationMailTemplate: null,
            endTimeEdited: false,
            duration: DEFAULT_APPOINTMENT_DURATION,
            isDirty: false,
            datepickerConfig: {
                onClose: (selectedDates, dateStr, instance) => {
                    instance.setDate(new Date(instance.input.value).toISOString(), true)
                }
            },
            adminTimeZone: 'UTC',
        };
    },

    watch: {
        appointment: {
            deep: true,
            immediate: true,
            handler(newValue, oldValue) {
                if (newValue !== oldValue) {
                    this.configData();
                }

                this.isDirty = this.detectAppointmentDataChanges();
            },
        },
        'appointment.accessibleFrom'(){
            this.loadMailTemplate();
        },
        'appointment.accessibleTo'(){
            this.loadMailTemplate();
        },
        'appointment.mode'(){
            this.loadMailTemplate();
        },
        salesChannelDomainId() {
            if (this.salesChannelDomainId === null) {
                return;
            }
            this.appointment.salesChannelDomainId = this.salesChannelDomainId;
            this.domainRepository.get(
                this.salesChannelDomainId,
                Shopware.Context.api,
            ).then((res) => {
                this.appointment.salesChannelDomain = res;
                this.salesChannelRepository.get(
                    this.appointment.salesChannelDomain.salesChannelId,
                    Shopware.Context.api,
                ).then((salesChannelRes) => {
                    this.appointment.salesChannelDomain.salesChannel = salesChannelRes;
                });
            });
        },
    },

    async created() {
        this.adminTimeZone = Shopware?.State?.get('session')?.currentUser?.timeZone ?? 'UTC';

        this.systemConfigApiService.getValues('SwagGuidedShopping').then(async response => {
            this.defaultDomain = response['SwagGuidedShopping.config.defaultAppointmentDomain'];
            this.createDefaultData();
            this.loadRules();
            if (response['SwagGuidedShopping.config.sendInvitationMailTemplate']) {
                this.sendInvitationMailTemplateId = response['SwagGuidedShopping.config.sendInvitationMailTemplate'];
                await this.loadMailTemplateType();
                await this.loadMailTemplate();
            }
        });
    },

    computed: {
        ...mapPropertyErrors('appointment', [
            'name',
            'presentationPath',
            'guideUserId',
            'salesChannelDomainId',
            'accessibleFrom',
            'accessibleTo',
            'presentationId'
        ]),

        isNewInvitedAttendee() {
            return !!this.appointment.attendees?.find(x => x._isNew);
        },

        canSendMail() {
            if (!this.appointmentInvitedAttendees.length && !this.isNewInvitedAttendee) {
                return false;
            }
            return this.isDirty;
        },

        isAppointmentDateTimeChanged() {
            const oldFromValue = new Date(this.cachedAppointmentData.accessibleFrom).getTime();
            const oldToValue = new Date(this.cachedAppointmentData.accessibleTo).getTime();
            const newFromValue = new Date(this.appointment.accessibleFrom).getTime();
            const newToValue = new Date(this.appointment.accessibleTo).getTime();
            return oldFromValue !== newFromValue || oldToValue !== newToValue;
        },

        isLoadingGrid() {
            return this.isLoadingCustomerData || this.isLoading;
        },

        salesChannelDomainId() {
            return this.appointment.salesChannelDomainId;
        },

        videoRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment_video_chat');
        },

        mailTemplateRepository() {
            return this.repositoryFactory.create('mail_template');
        },

        mailTemplateTypeRepository() {
            return this.repositoryFactory.create('mail_template_type');
        },

        domainRepository() {
            return this.repositoryFactory.create('sales_channel_domain');
        },

        attendeeRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment_attendee');
        },

        salesChannelRepository() {
            return this.repositoryFactory.create('sales_channel');
        },

        customerRepository() {
            return this.repositoryFactory.create('customer');
        },

        domainCriteria() {
            const criteria = new Criteria();

            criteria.addFilter(Criteria.equals('salesChannel.typeId', Defaults.storefrontSalesChannelTypeId));

            return criteria;
        },

        presentationCriteria() {
            const criteria = new Criteria();

            criteria.addFilter(Criteria.equals('parentId', null));
            criteria.addFilter(Criteria.equals('active', true));
            criteria.addFilter(Criteria.equals('versionId', Defaults.versionId));
            criteria.addSorting(Criteria.sort('createdAt', 'DESC'));

            return criteria;
        },

        modes() {
            return [
                {
                    value: 'self',
                    label: this.$tc('swag-guided-shopping-appointment.base.configuration.modeSelfLabel')
                },
                {
                    value: 'guided',
                    label: this.$tc('swag-guided-shopping-appointment.base.configuration.modeGuidedLabel')
                }
            ];
        },

        videoAudioSettings() {
            return [
                {
                    value: 'both',
                    label: this.$tc('swag-guided-shopping-appointment.base.configuration.bothVideoAudioLabel')
                },
                {
                    value: 'audio-only',
                    label: this.$tc('swag-guided-shopping-appointment.base.configuration.audioOnlyLabel')
                },
                {
                    value: 'none',
                    label: this.$tc('swag-guided-shopping-appointment.base.configuration.noneVideoAudioLabel')
                }
            ];
        },

        attendeeRestrictionTypes() {
            return [
                {
                    value: 'open',
                    label: this.$tc('swag-guided-shopping-appointment.base.attendees.attendeeRestrictionTypeOpen')
                },
                {
                    value: 'customer',
                    label: this.$tc('swag-guided-shopping-appointment.base.attendees.attendeeRestrictionTypeCustomer')
                },
                // {
                //     value: 'rules',
                //     label: this.$tc('swag-guided-shopping-appointment.base.attendees.attendeeRestrictionTypeRules')
                // }
            ];
        },

        assignedCustomerColumns() {
            return [
                {
                    property: 'attendeeName',
                    label: this.$tc('swag-guided-shopping-appointment.base.attendees.columnName'),
                    primary: true,
                    sortable: true
                },
                {
                    property: 'mail',
                    label: this.$tc('swag-guided-shopping-appointment.base.attendees.columnMail'),
                    sortable: true
                },
                {
                    property: 'joinedAt',
                    label: this.$tc('swag-guided-shopping-appointment.base.attendees.columnJoinedAt'),
                    sortable: true
                },
                {
                    property: 'type',
                    label: this.$tc('swag-guided-shopping-appointment.base.attendees.columnType'),
                    sortable: true
                }
            ];
        },

        attendeeSearchCriteria() {
            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('guest', false));
            criteria.addFilter(Criteria.equals('active', true));
            criteria.addSorting(Criteria.sort('firstName'));
            return criteria;
        },

        searchContext() {
            return {
                ...Context.api
            };
        },

        emailString() {
            if (!this.appointment.attendees) {
                return '';
            }
            let emailString = '';
            this.appointment.attendees.forEach(function (attendee) {
                const customer = attendee.customer;
                if (!customer) {
                    return;
                }
                if (emailString !== '') {
                    emailString += '; ';
                }
                emailString += this.getCustomerName(customer) + ' <' + customer.email + '>';
            }, this);

            return emailString;
        },

        ruleRepository() {
            return this.repositoryFactory.create('rule');
        },

        hasRuleCollection() {
            return this.appointment && this.appointment.attendeeRuleCollection;
        },

        rulesCriteria() {
            const criteria = new Criteria();
            criteria.addFilter(Criteria.equals('invalid', false));
            return criteria;
        },

        appointmentAttendees() {
            return this.appointment.attendees;
        },

        customFieldSetRepository() {
          return this.repositoryFactory.create('custom_field_set');
        },

        customFieldSetCriteria() {
          const criteria = new Criteria(1, 25);

          criteria
            .addFilter(Criteria.equals('relations.entityName', 'guided_shopping_appointment'));
          criteria.getAssociation('customFields')
            .addSorting(Criteria.sort('config.customFieldPosition', 'ASC', true));

          return criteria;
        },

        customFieldsExists() {
          if (!this.customFieldSets.length > 0) {
            return false;
          }

          return this.customFieldSets.some(set => set.customFields.length > 0);
        },

        showCustomFieldsCard() {
          return !this.isLoading && this.customFieldsExists;
        },

        appointmentInvitedAttendees() {
            return this.appointment.attendees?.filter(x => !x._isNew && x.type === 'CLIENT' && !!x.attendeeEmail) ?? [];
        }
    },

    methods: {
        formatDate(val, options) {
            if (!val) {
                return '';
            }

            const givenDate = new Date(val);

            if (isNaN(givenDate)) {
                return '';
            }

            const lastKnownLang = Shopware.Application.getContainer('factory').locale.getLastKnownLocale();

            const dateTimeFormatter = new Intl.DateTimeFormat(lastKnownLang, {
                timeZone: this.adminTimeZone,
                ...options,
            });

            return dateTimeFormatter.format(givenDate);
        },
        async loadMailTemplateType() {
            this.sendInvitationMailTemplate = await this.mailTemplateRepository.get(this.sendInvitationMailTemplateId, Shopware.Context.api)
            this.sendInvitationMailTemplateType = await this.getMailTemplateType(this.sendInvitationMailTemplate);
        },
        async loadMailTemplate() {
            if (!this.sendInvitationMailTemplateType) return;
            this.sendInvitationMailTemplateType.templateData.appointment.salesChannelDomain.salesChannel.translated.name = this.appointment?.salesChannelDomain?.salesChannel?.name;
            this.sendInvitationMailTemplateType.templateData.appointment.message = '';
            this.sendInvitationMailTemplateType.templateData.appointment.mode = this.appointment.mode;
            if (this.appointment.accessibleFrom) {
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleFrom.date = this.formatDate(this.appointment.accessibleFrom, {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                });
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleFrom.time = this.formatDate(this.appointment.accessibleFrom, {
                    hour: 'numeric',
                    minute: 'numeric',
                })
            } else {
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleFrom.date = 'DD MM YYYY';
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleFrom.time = 'hh:mm';
            }
            if (this.appointment.accessibleTo) {
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleTo.date = this.formatDate(this.appointment.accessibleTo, {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                });
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleTo.time = this.formatDate(this.appointment.accessibleTo, {
                    hour: 'numeric',
                    minute: 'numeric',
                })
            } else {
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleTo.date = 'DD MM YYYY';
                this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleTo.time = 'hh:mm';
            }
            this.sendInvitationMailTemplateType.templateData.appointment.customFields.isSameDay =
                this.formatDate(this.appointment.accessibleFrom, {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                })
                ===
                this.formatDate(this.appointment.accessibleTo, {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                })

            this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleFrom.timezone = this.adminTimeZone;
            this.sendInvitationMailTemplateType.templateData.appointment.customFields.accessibleTo.timezone = this.adminTimeZone;

            this.mailPreview = await this.mailService.buildRenderPreview(
                this.sendInvitationMailTemplateType,
                this.mailPreviewContent(this.sendInvitationMailTemplate),
            )
        },
        getMailTemplateType(mailTemplate) {
            if (!mailTemplate.mailTemplateTypeId) {
                return Promise.resolve();
            }

            return this.mailTemplateTypeRepository.get(mailTemplate.mailTemplateTypeId).then((item) => {
                return item;
            });
        },
        mailPreviewContent(mailTemplateTemp) {
            const mailTemplate = { ...mailTemplateTemp };

            if (mailTemplate.contentHtml) {
                mailTemplate.contentHtml = this.replaceContent(mailTemplate.contentHtml);
            }

            if (mailTemplate.translated?.contentHtml) {
                mailTemplate.translated.contentHtml = this.replaceContent(mailTemplate.translated.contentHtml);
            }

            if (mailTemplate.contentPlain) {
                mailTemplate.contentPlain = this.replaceContent(mailTemplate.contentPlain);
            }

            if (mailTemplate.translated?.contentPlain) {
                mailTemplate.translated.contentPlain = this.replaceContent(mailTemplate.translated.contentPlain);
            }
            return mailTemplate;
        },
        replaceContent(string) {
            // Replace .at([index]), first -> `.[index]` to suitable with mail template data
            return string.replace(/\.at\(([0-9]*)\)\./g, (matchs) => {
                const index = parseInt(matchs.match(/[0-9]/g).join(''), 10);
                return `.${index}.`;
            }).replace(/\.first\./g, '.0.');
        },
        changeValue(data) {
            this.appointment.attendees = this.appointment.attendees.filter(x => !x._isNew);
            data.forEach(customer => {
                const newAttendee = this.attendeeRepository.create();
                newAttendee.appointmentId = this.appointment.id;
                newAttendee.type = 'CLIENT';
                newAttendee.attendeeEmail = customer.email;
                if (!customer._isNew) {
                    newAttendee.customerId = customer.id;
                    newAttendee.attendeeName = `${customer.firstName} ${customer.lastName}`;
                }
                this.appointment.attendees.add(newAttendee);
            })
        },

        toggleVideoConfiguration() {
            this.showVideoConfiguration = !this.showVideoConfiguration;
        },

        presentationPathChanged(e) {
            if (!e) {
                this.appointmentPresentationPath = e;
                this.appointment.presentationPath = this.appointment.id;
                return;
            }
            let url = e;
            url = url.replace(/\s+/g, '-').toLowerCase();

            while (url.charAt(0) === '/') {
                url = url.substring(1);
            }

            const convertedUrl = this.appointmentService._slugify(url);

            if (convertedUrl.length > 0) {
                this.appointmentPresentationPath = convertedUrl;
                this.appointment.presentationPath = convertedUrl;
            } else {
                this.appointmentPresentationPath = null;
                this.appointment.presentationPath = this.appointment.id;
            }
        },

        calculateFromTime() {
            const accessibleFrom = new Date();
            accessibleFrom.setMinutes(Math.ceil(accessibleFrom.getMinutes() / 30) * 30, 0, 0);

            return accessibleFrom.toISOString();
        },

        calculateEndTime() {
            const accessibleTo = new Date(this.appointment.accessibleFrom);
            accessibleTo.setMinutes(accessibleTo.getMinutes() + this.duration);

            return accessibleTo.toISOString();
        },

        onChangeAccessibleFrom() {
            if(!this.endTimeEdited) {
                this.appointment.accessibleTo = this.calculateEndTime();
            }
        },

        onChangeAccessibleTo() {
            this.endTimeEdited = true;
        },

        createDefaultData() {
            let isNew = false;
            try {
                isNew = this.appointment.isNew();
            } catch (e) {
                isNew = false;
            }

            if (isNew) {
                // fill empty data
                this.appointment.active = true;
                this.appointment.mode = 'guided';
                this.appointment.attendeeRestrictionType = 'open';
                this.appointment.presentationPath = this.appointment.id;
                this.appointment.videoAudioSettings = 'both';
                this.appointment.videoChat = this.videoRepository.create(Shopware.Context.api);
                this.appointment.videoChat.appointmentId = this.appointment.id;
                this.appointment.salesChannelDomainId = this.defaultDomain;
                this.appointment.accessibleFrom = this.calculateFromTime();
                this.appointment.accessibleTo = this.calculateEndTime();
            } else {
                this.duration = (new Date(this.appointment.accessibleTo) - new Date(this.appointment.accessibleFrom))/60000;
            }
        },

        onChangeVideoAudioSettings(setting) {
            if (setting !== 'none') {
                if (!this.appointment.videoChat) {
                    this.appointment.videoChat = this.videoRepository.create(Shopware.Context.api);
                    this.appointment.videoChat.appointmentId = this.appointment.id;
                }
            } else {
                if (this.appointment.videoChat) {
                    this.appointment.videoChat.startAsBroadcast = false;
                }
            }

            this.appointment.videoAudioSettings = setting;
        },

        isDisableDelete(item) {
            return item.joinedAt ? true : false;
        },

        isSelected(item) {
            return this.appointment.attendees.some(p => p.customerId === item.id);
        },

        getAttendeeName(attendee) {
            if (attendee.attendeeName) {
                return attendee.attendeeName;
            }
            return this.getCustomerName(attendee.customer);
        },

        getCustomerName(customer) {
            if (!customer) {
                return '';
            }
            const company = customer.company ? '(' + customer.company + ')' : '';

            let parts = [customer.firstName, customer.lastName, company];
            parts = parts.filter(Boolean);

            return parts.join(' ');
        },

        getAttendeeLink(attendee) {
          if (!attendee) {
            return '';
          }

          if (attendee.customerId)  {
            const route = this.$router.resolve({ name: 'sw.customer.detail', params: {id: attendee.customerId}});
            return route.href;
          }

          if (attendee.userId)  {
            return this.$router.resolve({ name: 'sw.users.permissions.user.detail', params: {id: attendee.userId}}).href;
          }
        },

        getUserName(user) {
            if (!user) {
                return '';
            }
            let parts = [user.firstName, user.lastName];
            parts = parts.filter(Boolean);

            let name = parts.join(' ');

            if (!name) {
                name = user.username;
            }

            return name;
        },

        getAttendeeJoinedAt(attendee) {
            if (!attendee.joinedAt) {
                return `- ${this.$tc('swag-guided-shopping-appointment.base.attendees.notJoined')} -`;
            }

            const options = {
                dateStyle: 'long',
                timeStyle: 'short',
                timeZone: 'UTC'
            };

            return new Intl.DateTimeFormat('en-GB', options).format(new Date(attendee.joinedAt)); // 4 April 2023 at 13:25
        },

        loadRules() {
            if (!this.appointment || this.appointment.attendeeRulesLoaded) {
                return;
            }

            if (types.isEmpty(this.appointment.attendeeRuleIds)) {
                this.appointment.attendeeRuleCollection = this.createRuleCollection();
                this.appointment.attendeeRulesLoaded = true;
                return;
            }

            const criteria = new Criteria();
            criteria.setIds(this.appointment.attendeeRuleIds);


            this.ruleRepository.search(criteria, Shopware.Context.api).then((rules) => {
                this.appointment.attendeeRuleCollection = rules;
            });

            this.appointment.attendeeRulesLoaded = true;
        },

        createRuleCollection() {
            return new EntityCollection('/rule', 'rule', Shopware.Context.api, new Criteria());
        },

        onChangeRule(ruleCollection) {
            this.appointment.attendeeRuleIds = [];

            ruleCollection.forEach(rule => {
                this.appointment.attendeeRuleIds.push(rule.id);
            });
        },

        onChangeType(value) {
            if (value) {
                this.appointment.attendeeRestrictionType = this.attendeeRestrictionTypes[1].value;
            } else {
                this.appointment.attendeeRestrictionType = this.attendeeRestrictionTypes[0].value;
            }
        },

        onChangeMode(value) {
            if (value === null) {
                this.appointment.mode = this.modes[0].value;
            }
            if (value === 'self') {
                this.appointment.videoAudioSettings = 'none';
            }
        },

        loadCustomFieldSets() {
          return this.customFieldSetRepository.search(this.customFieldSetCriteria)
              .then((customFieldSets) => {
                  this.customFieldSets = customFieldSets;
              });
        },

        removeAttendee(attendee) {
            this.$emit("cancel-recipient", attendee);
        },

        configData() {
            this.loadRules();
            this.loadCustomFieldSets();
            this.appointmentPresentationPath = this.appointment.presentationPath === this.appointment.id ? '' : this.appointment.presentationPath;
        },

        detectAppointmentDataChanges() {
            if (this.appointment._isNew) {
                return true;
            }
            if (!this.appointment._isNew && !this.cachedAppointmentData) {
                return false;
            }
            const isGuideChanged = this.appointment.guideUserId !== this.cachedAppointmentData.guideUserId;
            const isModeChanged = this.appointment.mode !== this.cachedAppointmentData.mode;
            const isNameChanged = this.appointment.name !== this.cachedAppointmentData.name;
            const isMessageChanged = this.appointment.message !== this.cachedAppointmentData.message;
            const isPresentationSelectionChanged = this.appointment.presentationId !== this.cachedAppointmentData.presentationId;
            const isAttendeeRestrictionTypeChanged = this.appointment.attendeeRestrictionType !== this.cachedAppointmentData.attendeeRestrictionType;
            const isSaleChannelChanged = this.appointment.salesChannelDomain.url !== this.cachedAppointmentData.salesChannelDomain.url;
            const isPresentationPathChanged = this.appointment.presentationPath !== this.cachedAppointmentData.presentationPath;
            const isVideoAndAudioChanged = this.appointment.videoAudioSettings !== this.cachedAppointmentData.videoAudioSettings;
            let isBroadcastModeChanged = false;
            if ((!this.appointment.videoChat && this.cachedAppointmentData.videoChat) || (this.appointment.videoChat && !this.cachedAppointmentData.videoChat)) {
                isBroadcastModeChanged = true;
            } else if (this.appointment.videoChat && this.cachedAppointmentData.videoChat) {
                isBroadcastModeChanged = this.appointment.videoChat.startAsBroadcast !== this.cachedAppointmentData.videoChat.startAsBroadcast;
            }
            return this.isNewInvitedAttendee || this.isAppointmentDateTimeChanged || isPresentationSelectionChanged || isGuideChanged || isModeChanged || isNameChanged || isMessageChanged || isSaleChannelChanged || isPresentationPathChanged || isAttendeeRestrictionTypeChanged || isVideoAndAudioChanged || isBroadcastModeChanged;
        },
    
    }
});
