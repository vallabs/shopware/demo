import template from './sw-product-stream-grid-preview.html.twig';

const { Component, Context } = Shopware;

Component.override('sw-product-stream-grid-preview', {
    template,
    methods: {
        loadProducts() {
            this.criteria.term = this.searchTerm || null;
            this.criteria.filters = [...this.filters];
            this.criteria.limit = this.limit;
            this.criteria.setPage(this.page);
            this.criteria.addAssociation('manufacturer');
            this.criteria.addAssociation('options.group');
            this.criteria.addAssociation('cover');
            this.criteria.addAssociation('media');

            return this.productRepository.search(this.criteria, {
                ...Context.api,
                inheritance: true,
            }).then((products) => {
                this.products = products;
                this.total = products.total;

                this.isLoading = false;
            });
        },
    }
});
