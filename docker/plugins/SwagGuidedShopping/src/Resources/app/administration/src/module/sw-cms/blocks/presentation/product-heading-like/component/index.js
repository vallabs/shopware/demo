import template from './sw-cms-block-product-heading-like.html.twig';
import './sw-cms-block-product-heading-like.scss';

const { Component, State } = Shopware;

Component.register('sw-cms-block-product-heading-like', {
    template,

    computed: {
        currentDeviceView() {
            return State.get('cmsPageState').currentCmsDeviceView;
        },

        currentDeviceViewClass() {
            if (this.currentDeviceView) {
                return `is--${this.currentDeviceView}`;
            }

            return null;
        },
    },
});
