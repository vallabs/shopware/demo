const {Module, Application, Service} = Shopware;

import './page/swag-guided-shopping-appointment-list';
import './page/swag-guided-shopping-appointment-detail';
import './view/swag-guided-shopping-appointment-detail-base';
import './component/swag-guided-shopping-appointment-form';
import './component/swag-guided-shopping-appointment-urls';
import './component/swag-guided-shopping-appointment-button-copyable';
import './component/swag-guided-shopping-appointment-email';
import './component/swag-guided-shopping-appointment-cancel-modal';
import './component/swag-guided-shopping-appointment-send-appointment-modal';
import './acl';
import './acl/guide';
import AppointmentService from './service/appointment.service';
import defaultSearchConfiguration from './search-configuration';

Service().register('appointmentService', () => {
    return new AppointmentService();
});

Module.register('swag-guided-shopping-appointment', {
    type: 'plugin',
    name: 'appointment',
    title: 'swag-guided-shopping-appointment.admin.moduleTitle',
    description: 'swag-guided-shopping-appointment.admin.moduleDescription',
    color: '#FFD700',
    icon: 'regular-list-xs',
    entity: 'guided_shopping_appointment',

    routes: {
        index: {
            components: {
                default: 'swag-guided-shopping-appointment-list'
            },
            path: 'index',
            meta: {
                privilege: 'guided_shopping_appointment.viewer',
                appSystem: {
                    view: 'list'
                }
            }
        },
        create: {
            component: 'swag-guided-shopping-appointment-detail',
            path: 'create',
            redirect: {
                name: 'swag.guided.shopping.appointment.create.base'
            },
            meta: {
                privilege: 'guided_shopping_appointment.creator',
            },
            children: {
                base: {
                    component: 'swag-guided-shopping-appointment-detail-base',
                    path: 'base',
                    meta: {
                        parentPath: 'swag.guided.shopping.appointment.index',
                        privilege: 'guided_shopping_appointment.creator',
                    }
                }
            }
        },
        detail: {
            component: 'swag-guided-shopping-appointment-detail',
            path: 'detail/:id?',
            props: {
                default: (route) => ({appointmentId: route.params.id})
            },
            redirect: {
                name: 'swag.guided.shopping.appointment.detail.base'
            },
            meta: {
                privilege: 'guided_shopping_appointment.viewer',
                appSystem: {
                    view: 'detail'
                }
            },
            children: {
                base: {
                    component: 'swag-guided-shopping-appointment-detail-base',
                    path: 'base',
                    meta: {
                        parentPath: 'swag.guided.shopping.appointment.index',
                        privilege: 'guided_shopping_appointment.viewer',
                    }
                },
                cms: {
                    component: 'swag-guided-shopping-appointment-detail-cms',
                    path: 'cms',
                    meta: {
                        parentPath: 'swag.guided.shopping.appointment.index',
                        privilege: 'guided_shopping_appointment.viewer',
                    }
                }
            }
        }
    },

    navigation: [{
        label: 'swag-guided-shopping-appointment.admin.appointmentNavigationEntry',
        path: 'swag.guided.shopping.appointment.index',
        parent: 'swag-guided-shopping',
        position: 2,
        privilege: 'guided_shopping_appointment.viewer'
    }],

    defaultSearchConfiguration,
});

Application.addServiceProviderDecorator('searchTypeService', searchTypeService => {
    searchTypeService.upsertType('guided_shopping_appointment', {
        entityName: 'guided_shopping_appointment',
        placeholderSnippet: 'swag-guided-shopping-appointment.admin.placeholderSearchBarSnippets',
        listingRoute: 'swag.guided.shopping.appointment.index'
    });

    return searchTypeService;
});

Application.addServiceProviderDecorator('customFieldDataProviderService', customFieldDataProviderService => {
  customFieldDataProviderService.addEntityName('guided_shopping_appointment');
  return customFieldDataProviderService;
});
