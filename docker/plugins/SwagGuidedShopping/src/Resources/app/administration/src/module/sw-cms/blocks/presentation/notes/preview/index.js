import template from './sw-cms-preview-notes.html.twig';
import './sw-cms-preview-notes.scss';

const { Component } = Shopware;

Component.register('sw-cms-preview-notes', {
    template,
});
