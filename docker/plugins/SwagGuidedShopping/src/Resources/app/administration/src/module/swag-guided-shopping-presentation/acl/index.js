Shopware.Service('privileges').addPrivilegeMappingEntry({
    category: 'permissions',
    parent: 'guided_shopping',
    key: 'guided_shopping_presentation',
    roles: {
        viewer: {
            privileges: [
                'guided_shopping_presentation:read',
                'guided_shopping_appointment:read',
                'guided_shopping_presentation_cms_page:read',
                'cms_page:read',
                'cms_section:read',
                'cms_block:read',
                'cms_slot:read',
                'user:read',
                'product:read',
                'product_stream:read',
                'system_config:read',
                'sales_channel_domain:read',
                'product_media:read',
                'media:read'
            ],
            dependencies: []
        },
        editor: {
            privileges: [
                'guided_shopping_presentation:update',
                'guided_shopping_presentation_cms_page:update',
                'guided_shopping_presentation_cms_page:delete',
                'guided_shopping_presentation_cms_page:create'
            ],
            dependencies: [
                'guided_shopping_presentation.viewer'
            ]
        },
        creator: {
            privileges: [
                'guided_shopping_presentation:create'
            ],
            dependencies: [
                'guided_shopping_presentation.viewer',
                'guided_shopping_presentation.editor'
            ]
        },
        deleter: {
            privileges: [
                'guided_shopping_presentation:delete',
                'guided_shopping_presentation_cms_page:delete',
                'guided_shopping_appointment:delete',
                'guided_shopping_appointment_attendee:delete',
                'guided_shopping_appointment_video_chat:delete',
            ],
            dependencies: [
                'guided_shopping_presentation.viewer'

            ]
        }
    }
});