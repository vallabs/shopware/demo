import template from './swag-guided-shopping-presentation-detail-appointments.html.twig';
import './swag-guided-shopping-presentation-detail-appointments.scss';


const { Component, Mixin, State } = Shopware;
const { Criteria } = Shopware.Data;
const { mapState, mapGetters } = Component.getComponentHelper();
const { cloneDeep } = Shopware.Utils.object;

Component.register('swag-guided-shopping-presentation-detail-appointments', {
    template,

    inject: [
        'repositoryFactory',
        'acl',
        'feature',
        'gsMailService'
    ],

    mixins: [
        Mixin.getByName('notification'),
        Mixin.getByName('placeholder'),
        Mixin.getByName('listing'),
    ],

    data() {
        return {
            appointmentForm: null,
            showAddModal: false,
            showEditModal: false,
            currentAppointment: null,
            cachedAppointmentData: null,
            showDeleteModal: false,
            currentCancelAppointment: null,
            modalLoading: false,
            includeOptions: [],
            appointments: [],
            appointmentsLoading: false,
            showSendAppointmentModal: false,
            currentCancelRecipient: null,
        };
    },

    computed: {
        ...mapState('swagGuidedShoppingPresentationDetail', [
            'presentation',
            'customFieldSets',
            'loading'
        ]),

        ...mapGetters('context', [
            'isSystemDefaultLanguage',
        ]),

        ...mapGetters('swagGuidedShoppingPresentationDetail', [
            'isLoading',
        ]),

        appointmentRepository() {
            return this.repositoryFactory.create('guided_shopping_appointment');
        },

        appointmentColumns() {
            return [
                {
                    property: 'name',
                    routerLink: 'swag.guided.shopping.appointment.detail',
                    label: this.$tc('swag-guided-shopping-presentation.detailAppointments.columnName'),
                    allowResize: true,
                    sortable: false,
                    width: '230px',
                }, {
                    property: 'activity',
                    label: this.$tc('swag-guided-shopping-appointment.admin.fields.activity'),
                    allowResize: true,
                    width: '200px',
                    sortable: false
                }, {
                    property: 'guideName',
                    dataIndex: 'guideName',
                    label: this.$tc('swag-guided-shopping-presentation.detailAppointments.columnGuide'),
                    width: '120px',
                    allowResize: true,
                    sortable: false
                }, {
                    property: 'urls',
                    dataIndex: 'urls',
                    label: this.$tc('swag-guided-shopping-appointment.admin.fields.urls'),
                    width: '320px',
                    allowResize: true,
                    sortable: false
                }
            ];
        },

        appointmentCriteria() {
            const criteria = new Criteria();

            criteria.addAssociation('presentation');
            criteria.addAssociation('guideUser');
            criteria.addAssociation('salesChannelDomain.language');
            criteria.addFilter(Criteria.equals('presentationId', this.presentation.id));
            criteria.addFilter(Criteria.equals('isPreview', false));
            criteria.addSorting(Criteria.sort('createdAt', 'DESC'));
            criteria.setTerm(this.term);

            return criteria;
        },

        isSystemLanguage() {
            return State.get('context').api.systemLanguageId === this.currentLanguage;
        },

        currentLanguage() {
            return State.get('context').api.languageId;
        },

        singleAppointmentCriteria() {
            const criteria = new Criteria();
            criteria.addAssociation('attendees.customer');
            criteria.getAssociation('attendees')
                .addSorting(Criteria.sort('type', 'DESC')) // type: GUIDE, CLIENT
                .addSorting(Criteria.sort('joinedAt', 'DESC'))
                .addSorting(Criteria.sort('attendeeName'));
            criteria.addAssociation('videoChat');
            return criteria;
        },

        onEditPresentationStructure() {
            if (this.isSystemDefaultLanguage) {
                return this.$tc('sw-privileges.tooltip.warning');
            }

            return this.$tc('swag-guided-shopping-presentation.general.buttonChangePresentationStructureWarning');
        },
    },

    methods: {
        createdComponent() {
            this.getList();
        },

        onCreateNewAppointment() {
            this.showAddModal = true;
            this.createNewPresentationAppointment();
        },

        createNewPresentationAppointment() {
            const newAppointment = this.appointmentRepository.create();
            newAppointment.presentationId = this.presentation.id;
            newAppointment.guidedShoppingPresentationVersionId = this.presentation.versionId;
            newAppointment.presentation = this.presentation;
            newAppointment.active = true;

            this.currentAppointment = newAppointment;
        },

        async onSaveAppointment(sendMail) {
            if (this.currentAppointment === null) {
                return;
            }

            if (sendMail) {
                if (!this.currentAppointment._isNew && this.appointmentForm.isNewInvitedAttendee) {
                    this.showSendAppointmentModal = true;
                } else {
                    this.onSendMailToAttendees(true);
                }
            } else {
                await this.appointmentRepository.save(this.currentAppointment, Shopware.Context.api);
                await this.getList();
                this.onCloseModal();
            }
        },

        async onSendMailToAttendees(sendMailToAll = true) {
            // Show error if cannot send mail
            if(this.currentAppointment.mode ==='guided' && (!this.currentAppointment.accessibleFrom || !this.currentAppointment.accessibleTo)) {
                this.createNotificationError({
                    message: this.$tc(`global.error-codes.INVITATION_EMAIL_SENDING_TIME_ERROR`),
                });
                return;
            }

            try {
                await this.appointmentRepository.save(this.currentAppointment, Shopware.Context.api);

                await (sendMailToAll
                    ? this.gsMailService.sendInvitationForAll(this.currentAppointment.id)
                    : this.gsMailService.sendInvitation(this.currentAppointment.id)
                );

                await this.getList();
                this.onCloseSendAppointmentModal();
                this.onCloseModal();
            } catch(e) {
                this.createNotificationError({
                    message: this.$tc(`global.error-codes.${e.response?.data?.errors[0]?.code}`),
                });
            }
        },

        onCloseModal() {
            if (this.$route.query.hasOwnProperty('detailId')) {
                this.$route.query.detailId = null;
            }

            this.currentAppointment = null;
        },

        onEditAppointment(id) {
            this.appointmentRepository.get(id, Shopware.Context.api, this.singleAppointmentCriteria).then((result) => {
                this.currentAppointment = result;
                this.cachedAppointmentData = cloneDeep(result);
                this.showEditModal = id;
            });
        },

        onDeleteAppointment(id) {
            this.showDeleteModal = id;
        },

        onConfirmDeleteAppointment(id) {
            this.onCloseDeleteModal();

            return this.appointmentRepository.delete(id).then(() => {
                this.getList();
            });
        },

        onCloseDeleteModal() {
            this.showDeleteModal = false;
        },

        onOpenCancelAppointmentModal() {
            this.currentCancelAppointment = this.currentAppointment;
        },

        onCloseCancelAppointmentModal() {
            this.currentCancelAppointment = null;
            this.currentCancelRecipient = null;
        },

        async onConfirmCancelAppointment(id, isSendMail, cancelAppointmentMessage) {
            if (!id && !this.currentCancelAppointment) {
                return;
            }

            try {
                if(this.currentCancelRecipient) {
                    // send email to individually attendees
                    if(isSendMail) {
                        await this.gsMailService.cancelInvitation(id, cancelAppointmentMessage, this.currentCancelRecipient.attendeeEmail);
                    }

                    this.currentCancelAppointment.attendees.remove(this.currentCancelRecipient.id)
                    await this.appointmentRepository.save(this.currentCancelAppointment, Shopware.Context.api);
                    this.currentAppointment = await this.appointmentRepository.get(id, Shopware.Context.api, this.singleAppointmentCriteria);
                } else {
                    this.currentCancelAppointment.active = false;
                    await this.appointmentRepository.save(this.currentCancelAppointment, Shopware.Context.api);

                    // send email to all attendees
                    if(isSendMail) {
                        await this.gsMailService.cancelInvitation(id, cancelAppointmentMessage);
                    }
                    await this.appointmentRepository.delete(id);
                    this.currentCancelAppointment = null;
                    this.onCloseModal();
                }

                await this.getList();
                this.onCloseCancelAppointmentModal();

            } catch(e) {
                this.createNotificationError({
                    message: this.$tc(`global.error-codes.${e.response?.data?.errors[0]?.code}`),
                });
            }
        },

        onCancelRecipient(recipient) {
            this.currentCancelRecipient = recipient;
            this.onOpenCancelAppointmentModal();
        },

        getList() {
            this.appointmentsLoading = true;

            return this.appointmentRepository.search(this.appointmentCriteria).then((response) => {
                this.total = response.total;
                this.appointments = response;
                this.appointmentsLoading = false;
                return Promise.resolve();
            });
        },

        getFilterCriteria() {
            return new Criteria();
        },

        onSearchTermChange(searchTerm) {
            this.term = searchTerm;
            this.getList();
        },

        isAccessible(appointment) {
            if (!appointment.active) return false;

            const from = appointment.accessibleFrom ? new Date(appointment.accessibleFrom).getTime() : 0;
            const to = appointment.accessibleTo ? new Date(appointment.accessibleTo).getTime() : 0;

            return from < Date.now() && (to > Date.now() || !to)
        },

        getUserName(user) {
            if (!user) {
                return '';
            }
            let parts = [user.firstName, user.lastName];
            parts = parts.filter(Boolean);

            let name = parts.join(' ');

            if (!name) {
                name = user.username;
            }

            return name;
        },

        onCloseSendAppointmentModal() {
            this.showSendAppointmentModal = false;
        },
    }
});
