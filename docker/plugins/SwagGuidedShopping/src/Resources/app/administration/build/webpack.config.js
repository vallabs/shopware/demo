const { join, resolve } = require('path');

module.exports = () => {
  return {
    resolve: {
      alias: {
        'vue': resolve(
          join(process.env.PWD, 'node_modules', 'vue')
        )
      }
    }
  };
}
