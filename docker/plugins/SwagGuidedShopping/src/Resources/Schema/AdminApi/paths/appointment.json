{
  "openapi": "3.0.0",
  "info": [],
  "paths": {
    "/_action/guided-shopping/appointment/{appointmentId}/join-as-guide": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Join a meeting as a guide",
        "description": "This route is used to join appointment as guide.",
        "operationId": "joinAppointmentAsGuide",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/JoinAppointmentResponse"
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/start": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Start a presentation for a appointment",
        "description": "Complete all needed data start the presentation",
        "operationId": "startAppointment",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "requestBody": {
          "required": false,
          "content": {
            "application/json": {
              "schema": {
                "properties": {
                  "attendeeIds": {
                    "type": "array",
                    "items": {
                      "type": "string",
                      "pattern": "^[0-9a-f]{32}$"
                    },
                    "description": "The attendee ids are waiting the appointment to start"
                  }
                }
              }
            }
          }
        },
        "responses": {
          "204": {
            "description": ""
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/end": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "End a presentation for a appointment",
        "description": "Complete all needed data and remove unneeded to close the presentation",
        "operationId": "endAppointment",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": ""
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/attendee/{attendeeId}/sw-context-token": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get the sw-context-token for a attendee",
        "description": "Returns the context-token for the given attendee if the attendee granted the permission for the cart",
        "operationId": "getAttendeeToken",
        "parameters": [
          {
            "name": "attendeeId",
            "in": "path",
            "description": "Attendee id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "The sw-context-token from the attendee",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "attendee-sw-context-token": {
                      "type": "string"
                    }
                  },
                  "example": {
                    "attendee-sw-context-token": "context token of attendee"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/instant-listing": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Add a instant listing to the presentation",
        "description": "Add a page as the instant listing to the presentation",
        "operationId": "addInstantListing",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "productIds",
                  "currentPageGroupId"
                ],
                "properties": {
                  "productIds": {
                    "type": "array",
                    "items": {
                      "type": "string",
                      "pattern": "^[0-9a-f]{32}$"
                    },
                    "description": "Ids of the products which should be added to the instant listing"
                  },
                  "currentPageGroupId": {
                    "type": "string",
                    "description": "Id of the current cms page",
                    "pattern": "^[0-9a-f]{32}$"
                  }
                },
                "type": "object"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Created a new instant listing",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "index": {
                      "type": "integer",
                      "description": "The current index of the instant listing which is created by this request"
                    }
                  },
                  "example": {
                    "index": 1
                  }
                }
              }
            }
          }
        }
      },
      "patch": {
        "tags": [
          "Appointment"
        ],
        "summary": "Update a instant listing from the presentation",
        "description": "Updates the products for the given listing",
        "operationId": "updateInstantListing",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "required": [
                  "addProductIds",
                  "removeProductIds",
                  "currentPageGroupId"
                ],
                "properties": {
                  "addProductIds": {
                    "type": "array",
                    "items": {
                      "type": "string",
                      "pattern": "^[0-9a-f]{32}$"
                    },
                    "description": "Ids of the products which should be added to the instant listing"
                  },
                  "removeProductIds": {
                    "type": "array",
                    "items": {
                      "type": "string",
                      "pattern": "^[0-9a-f]{32}$"
                    },
                    "description": "Ids of the products which should be removed to the instant listing"
                  },
                  "currentPageGroupId": {
                    "type": "string",
                    "description": "Id of the current cms page",
                    "pattern": "^[0-9a-f]{32}$"
                  }
                },
                "type": "object"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Updated the instant listing",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "string",
                      "pattern": "^[0-9a-f]{32}$",
                      "description": "The id of the instant listing page"
                    },
                    "pickedProductIds": {
                        "type": "array",
                        "items": {
                            "type": "string",
                            "pattern": "^[0-9a-f]{32}$"
                        },
                        "description": "Ids of the products which are picked for the instant listing"
                    },
                    "title": {
                        "type": "string",
                        "description": "The title of the instant listing"
                    }
                  },
                  "example": {
                    "id": "018c3e5ffd3a70899fa1321bf7e2a7f7",
                    "pickedProductIds": [
                      "018c19b80403709cb2cef54f70860042",
                      "018c19b804027291955d8f076c272d0d"
                    ],
                    "title": "Default Digital Sales Rooms product listing page"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/presentation/state": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get the current presentation state",
        "description": "Returns the presentation state for all and the guide",
        "operationId": "getGuidePresentationState",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GuidePresentationStateResponse"
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/video-room": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Create a video room",
        "description": "Creates a video room on the remote video tool",
        "operationId": "createVideoRoom",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/VideoChatCreateStruct"
                }
              }
            }
          }
        }
      },
      "delete": {
        "tags": [
          "Appointment"
        ],
        "summary": "Delete a video room",
        "description": "Delete a video room and tokens on the remote video tool",
        "operationId": "deleteVideoRoom",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {

              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/widgets/attendee-insights": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get attendee insights",
        "description": "Get attendee insights for the given appointment",
        "operationId": "getAttendeeInsights",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetAttendeeInsightsResponse"
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/widgets/cart-insights": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get cart insights",
        "description": "Get cart insights for the given appointment",
        "operationId": "getCartInsights",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetCartInsightsResponse"
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/widgets/cart-statistics": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get cart statistics",
        "description": "Get cart statistics (cart line items) of all the attendees for the given appointment",
        "operationId": "getCartStatistics",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Cart"
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/widgets/last-seen": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get last seen products insights",
        "description": "Get last seen products of all attendees for the given appointment",
        "operationId": "getLastSeenProductsInsights",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "requestBody": {
          "required": false,
          "content": {
              "application/json": {
                "schema": {
                  "properties": {
                    "limit": {
                      "type": "integer",
                      "description": "The limit of the products which should be returned",
                      "default": 10
                    },
                    "page": {
                      "type": "integer",
                      "description": "The page of the products which should be returned",
                      "default": 1
                    },
                    "attendeeId": {
                      "type": "string",
                      "pattern": "^[0-9a-f]{32}$",
                      "description": "The attendee id for which the last seen products should be returned"
                    }
                  }
                }
              }
          }
        },
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/WidgetProductListing"
                }
              }
            }
          }
        }
      }
    },
    "/_action/guided-shopping/appointment/{appointmentId}/widgets/wishlist": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get wishlist insights",
        "description": "Get wishlist of all the attendees for the given appointment",
        "operationId": "getWishlistInsights",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/WidgetProductListing"
                }
              }
            }
          }
        }
      }
    },
    "/search/guided-shopping-appointment": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Search appointments",
        "description": "Return list of appointments",
        "operationId": "searchAppointments",
        "parameters": [],
        "requestBody": {
          "required": false,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/GetListBodyRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "total": {
                      "type": "integer"
                    },
                    "data": {
                      "type": "array",
                      "items": {
                        "$ref": "#/components/schemas/Appointment"
                      }
                    },
                    "aggregations": {
                      "type": "array",
                      "items": {
                        "type": "object"
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/guided-shopping-appointment": {
      "post": {
        "tags": [
          "Appointment"
        ],
        "summary": "Create a new appointment",
        "description": "",
        "operationId": "createAppointment",
        "parameters": [],
        "requestBody": {
          "required": false,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Appointment"
              }
            }
          }
        },
        "responses": {
          "204": {
            "description": ""
          }
        }
      }
    },
    "/guided-shopping-appointment/{appointmentId}": {
      "get": {
        "tags": [
          "Appointment"
        ],
        "summary": "Get detail of appointment",
        "description": "",
        "operationId": "getAppointment",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "properties": {
                    "data": {
                      "$ref": "#/components/schemas/Appointment"
                    }
                  }
                }
              }
            }
          }
        }
      },
      "patch": {
        "tags": [
          "Appointment"
        ],
        "summary": "Update partially the existing appointment",
        "description": "",
        "operationId": "updateAppointment",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "requestBody": {
          "required": false,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Appointment"
              }
            }
          }
        },
        "responses": {
          "204": {
            "description": ""
          }
        }
      },
      "delete": {
        "tags": [
          "Appointment"
        ],
        "summary": "Delete appointment",
        "description": "",
        "operationId": "deleteAppointment",
        "parameters": [
          {
            "name": "appointmentId",
            "in": "path",
            "description": "Appointment id",
            "required": true,
            "schema": {
              "type": "string",
              "pattern": "^[0-9a-f]{32}$"
            }
          }
        ],
        "responses": {
          "204": {
            "description": ""
          }
        }
      }
    }
  }
}
