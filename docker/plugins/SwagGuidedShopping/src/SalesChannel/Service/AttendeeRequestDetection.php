<?php declare(strict_types=1);

namespace SwagGuidedShopping\SalesChannel\Service;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeCollection;
use SwagGuidedShopping\Content\Appointment\Attendee\AttendeeEntity;
use SwagGuidedShopping\Content\Appointment\Exception\AttendeeNotFoundException;
use SwagGuidedShopping\SalesChannel\Exception\RequiredRightsNotGrantedException;

class AttendeeRequestDetection
{
    /**
     * @param EntityRepository<AttendeeCollection> $attendeeRepository
     */
    public function __construct(
        private readonly EntityRepository $attendeeRepository
    ) {
    }

    public function detectAttendee(AttendeeEntity $actingAttendee, Context $context, ?string $clientAttendeeId = null): AttendeeEntity
    {
        if (!$clientAttendeeId) {
            return $actingAttendee;
        }

        /** @var AttendeeEntity|null $clientAttendee */
        $clientAttendee = $this->attendeeRepository->search(new Criteria([$clientAttendeeId]), $context)->first();

        if (!$clientAttendee) {
            throw new AttendeeNotFoundException($clientAttendeeId);
        }

        // make sure the attendee for whom you are requesting for is in your appointment to have proper rights
        if (!($actingAttendee->isGuide())) {
            throw new RequiredRightsNotGrantedException('you need to be a guide to perform this operation');
        }

        // make sure the attendee for whom you are requesting for is in your appointment to have proper rights
        if (!($clientAttendee->getAppointmentId() === $actingAttendee->getAppointmentId())) {
            throw new RequiredRightsNotGrantedException('you need to be in the same appointment as your client to perform this operation');
        }

        return $clientAttendee;
    }
}
