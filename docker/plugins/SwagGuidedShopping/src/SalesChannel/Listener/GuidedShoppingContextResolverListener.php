<?php declare(strict_types=1);

namespace SwagGuidedShopping\SalesChannel\Listener;

use SwagGuidedShopping\Framework\Routing\GuidedShoppingRequestContextResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class GuidedShoppingContextResolverListener implements EventSubscriberInterface
{
    public function __construct(
        private readonly GuidedShoppingRequestContextResolver $contextResolver
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => [
                ['resolveContext', -100],
            ],
        ];
    }

    public function resolveContext(ControllerEvent $event): void
    {
        $this->contextResolver->resolve($event->getRequest());
    }
}
