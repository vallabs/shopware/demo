<?php declare(strict_types=1);

namespace SwagGuidedShopping\SalesChannel\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class RouteAliasNotFoundException extends ShopwareHttpException
{
    public function __construct(?string $alias)
    {
        parent::__construct(\sprintf('the alias (%s) could not be found for this route', $alias));
    }

    public function getStatusCode(): int
    {
        return Response::HTTP_NOT_FOUND;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__ROUTE_ALIAS_NOT_FOUND;
    }
}
