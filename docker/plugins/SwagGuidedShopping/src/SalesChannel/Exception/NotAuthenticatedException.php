<?php declare(strict_types=1);

namespace SwagGuidedShopping\SalesChannel\Exception;

use Shopware\Core\Framework\ShopwareHttpException;
use SwagGuidedShopping\Exception\ErrorCode;
use Symfony\Component\HttpFoundation\Response;

class NotAuthenticatedException extends ShopwareHttpException
{
    public function getStatusCode(): int
    {
        return Response::HTTP_UNAUTHORIZED;
    }

    public function getErrorCode(): string
    {
        return ErrorCode::GUIDED_SHOPPING__NOT_AUTHENTICATED;
    }
}
