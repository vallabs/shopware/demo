<?php declare(strict_types=1);

namespace SwagGuidedShopping\Factory;

use Shopware\Core\System\SystemConfig\SystemConfigService;
use SwagGuidedShopping\Content\VideoChat\Service\Client\DailyClient;
use SwagGuidedShopping\Struct\ConfigKeys;

class DailyClientFactory
{
    public function __construct(
        private readonly SystemConfigService $configService
    ) {
    }

    /**
     * @throws \Exception
     */
    public function createClient(): DailyClient
    {
        $baseUrl = $this->configService->getString(ConfigKeys::BASE_URL);
        $apiKey = $this->configService->getString(ConfigKeys::API_KEY);

        return new DailyClient($baseUrl, $apiKey);
    }
}
