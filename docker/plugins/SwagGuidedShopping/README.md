# Digital Sales Rooms Plugin
![intro](./docs/assets/intro.png)
## Introduction
**Digital Sales Rooms** gives you the opportunity to present a shopping event designed by you to the selected customers.
It doesn't matter whether you want to interact with the customer in real-time or the customer navigates independently and at any time through the shopping event. Both is possible without any problems.

It's combined from [Shopware Admin](https://docs.shopware.com/en/shopware-6-en/first-steps/administration-overview) & [Shopware Frontends](https://frontends.shopware.com/) (isolated FrontEnd App)

## Prerequisites
1. [node](https://nodejs.org/en) >= v18 
2. [pnpm](https://pnpm.io/installation) >= 8
3. [make](https://formulae.brew.sh/formula/make) command.
4. Instance of [Shopware 6 (supported versions: > 6.5.6.0)](https://developer.shopware.com/docs/guides/installation/from-scratch) 
5. [SwagShopwarePwa plugin (supported versions > 0.4.*)](https://github.com/shopware/SwagShopwarePwa) installed in an instance of Shopware 6
6. `"symfony/mercure": "^0.6.2"` from [package](https://packagist.org/packages/symfony/mercure#v0.6.2) (compatible with Shopware 6.5).
7. `"spatie/icalendar-generator": "^2.5"` from [package](https://packagist.org/packages/spatie/icalendar-generator#v2.5.0) (compatible with Shopware 6.5).
8. `"@daily-co/daily-js": "^0.48.0"` from [daily.co](http://daily.co/) (compatible with Shopware 6.5).

## Installation
Please read [here](./docs/installation.md)
