# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2023-10-31
### Ensure the plugin is compatible with Shopware 6.5

## [1.2.1] - 2023-09-12
### We provide this minor version to:
- Improve the performance when opening the product listing modal on the PWA page
- Fix the bug when loading CMS pages from the Storefront side

## [1.2.0] - 2023-08-31
### Added
**One of the key additions is the integration of unit tests throughout the code. It also contains:**
* GS-88 - Appointment invitations and self-guided presentations can now be sent directly to customers via Digital Sales Rooms from the admin.
* GS-585 - Digital Sales Rooms can be connected with `Mercure.rocks` in anonymous mode.
* GS-652 - In the Instant Listing, a guide can select or deselect several products and add or edit them accordingly.
* GS-707 - API documentation is now available for Digital Sales Rooms.
* GS-759 - Instant Listing: If a guide refreshes the page and already makes some changes without saving in the instant listing, the current state will be saved.
* GS-763 - Add essential entity endpoints to API documentation.

### Fixed
* GS-751 - Bug fix in Instant Listing when adding/removing products.
* GS-766 - In the admin it was sometimes not possible to change the order of presentation pages. This issue has now been resolved.
* GS-775 - Fix validation error when update existing appointment.

### Changed
* GS-706 - Create appointments without using `Daily.co`.
* GS-708 - Improve the Select/Deselect all the products in creating/updating an instant listing.
* GS-753 - Improve multiple selections when filtering in instant listing modal.
* GS-758 - The self-appointment doesn't need accessible times to send the invitation.
* GS-761 - Refactor route paths and update them in API docs.
* GS-762 - Modify the invitation mail template.
* GS-764 - Modify services to be compatible with CMS extension plugin.
* GS-789 - Remove redundant default error message when you delete a CMS page used anywhere.

## [1.1.0] - 2023-04-27
### Added
* GS-355 - Add unit tests for `useMediaDevices`.
* GS-642 - Make the cms page layout configurable.
* GS-626 - Digital Sales Rooms has been optimised so that it can be better adapted to the customer's needs.
* GS-586 - Salesperson can conduct a sales appointment without a previously created presentation. Based on customer requirements that a customer communicates during the appointment, a slide with matching products can be created with Instant Listing.
* GS-623 - Add additional slots for `SwGsLayer` & allow to select a product while clicking on anywhere of the product box.
* GS-619 - Make the layout exchangeable.
* GS-641 - Add custom fields for the appointments and presentations.
* GS-622 - Guide can add products to the watch list for a customer in advance of a customer appointment.
* GS-645 - Retailers can see whether customers have attended a guided appointment or opened an unguided presentation.
* GS-653 - Presentations can be copied in the admin.
* GS-652 - In the Instant Listing, a guide can select or deselect several products and add or edit them accordingly.

### Fixed
* GS-485 - The **Go to Guide** function could not be used if several cross-selling elements were included on a product detail page.
* GS-454 - Fix error that you can not duplicate the cms page in the Shopping Experience page.
* GS-499 - Fix Admin search with the presentation and appointment area.
* GS-630 - Fix the arrow navigation style in Safari.
* GS-631 - Fix placeholder of the like list.
* GS-632 - Fix the PDP displays wrong content.
* GS-648 - Fix the quantity input of products in the cart.
* GS-627 - Fix the cart price.
* GS-639 - Fix group articles in the product selection.
* GS-447 - Customers invited to a closed appointment can now choose to allow sellers to add products to the customer's shopping cart/tool list when they sign up for the appointment.
* GS-615 - Users can set presentations to active/inactive. If a presentation is inactive, no customer can access the presentation. The salesperson can always access a presentation.
* GS-663 - Fixed an error that caused the general shopping cart to display products/turnover.
* GS-469 - **Go to Guide** function is now working if you attend later to the already starter presentation.
* GS-649 - Optimisation of the quantity entry in the shopping basket. The number of quantity entries can be increased or reduced manually or via arrow navigation.  In particular, the UX was optimised so that the customer can better determine the number of products.
* GS-678 - Burger menu has been adapted.
* GS-613 - Fix the product assignment modal displays not correct according to the type of layout.
* GS-681 - Fix the preview presentation feature.

### Changed
* GS-624 - Split up the navigation components.
* GS-618 - Scroll optimisation.
* GS-614 - Fixed the bug that prevented the preview function for presentations in Admin from working via Ipads.
* GS-669 - Changing the position of the currency symbol on the price.
* GS-638 - Adjust attendee table in the appointment detail.
* GS-621 - Clean up stored presentations in the live session.
* GS-620 - Improve performance memory.
* GS-645 - Retailers can see whether customers have attended a guided appointment or opened an unguided presentation (Additional changes).
* GS-676 - Adjustment of the German translation in the Daily.co checkbox in the login - Modal.
* GS-635 - Optimised product hover behaviour in the shopping cart.
* GS-625 - Refactor z-index.
* GS-675 - Update the preview presentation feature.
* GS-670 - Enable sound when video disabled.

## [1.0.0] - 2023-01-31
### **First release**
