/**
 * Setup repo branch. Do not run manually, only for initial setup refreshes.
 */

import { execa } from "execa";
import path from "path";
import fs from "fs-extra";

const __dirname = new URL(".", import.meta.url).pathname;
console.error("aaa", __dirname);

const tempDir = path.resolve(__dirname, "../temp");
const repoDir = `${tempDir}/diw-repo`;

const list = [
  "bin",
  "docs",
  "src",
  "tests",
  ".php-cs-fixer.php",
  "composer.json",
  "composer.lock",
  "makefile",
  "phpstan.neon",
  "phpunit.xml",
  "README.md",
];

async function run() {
  if (fs.existsSync(repoDir)) {
    fs.removeSync(repoDir);
  }
  /**
   * Clone repo
   */
  await execa(
    "git",
    [
      "clone",
      "--single-branch",
      "--branch",
      // "master",
      "feat--upgrade-shopware-pwa-and-storefront-ui",
      "git@bitbucket.org:dasistweb/shopware-guided-shopping.git",
      repoDir,
    ],
    {
      stdio: "inherit",
    }
  );

  // remove existing files
  await Promise.all(list.map((name) => fs.remove(name)));

  await fs.copy(path.join(repoDir, "custom/plugins/SwagGuidedShopping"), ".", {
    filter: (src) => !src.endsWith(".gitignore"),
  });

  // CLEAN
  if (fs.existsSync(repoDir)) {
    fs.removeSync(repoDir);
  }
}

run();
