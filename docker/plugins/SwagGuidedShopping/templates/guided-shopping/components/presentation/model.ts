export enum Sidebar {
  CART = 'CartSidebar',
  LAST_SEEN = 'LastSeenSidebar',
  PARTICIPANTS = 'ParticipantsSidebar',
  GUIDED_TOOLS = 'GuideToolsSidebar',
  LIKE_LIST = 'LikeListSidebar',
  PRODUCT_LISTING = 'ProductListingSidebar',
  PRODUCT_LISTING_UPSERT = 'ProductListingUpsertSidebar',
}

export enum ProductListingEventEnum {
  GO_BACK = 'goBack',
  OPEN_PRODUCT_SELECTION_SIDEBAR = 'openProductSelectionSidebar',
  CLOSE_PRODUCT_LISTING_LAYER = 'closeProductListingLayer',
}

export enum SavedProductsEventEnum {
  GO_BACK = 'goBack',
  CLOSE_SAVED_PRODUCTS_LAYER = 'closeSavedProductsLayer',
}
