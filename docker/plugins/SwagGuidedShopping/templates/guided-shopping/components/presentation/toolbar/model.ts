import { SlideGroupList } from '~/logic/interfaces';

export enum ToolbarMode {
  GUIDE = 'GUIDE',
  CLIENT = 'CLIENT',
  SELF = 'SELF',
}

type BtnHandler = {
  disabled?: boolean;
  on?: boolean;
  count?: number;
  hidden?: boolean;
};

type ToolbarMeta = {
  name: string;
  desc: string;
};

export type EventName =
  | 'goTo'
  | 'navigateToNextSlide'
  | 'navigateToPrevSlide'
  | 'navigateToSlide'
  | 'toggleGuideTools'
  | 'toggleParticipantsTools'
  | 'toggleMic'
  | 'toggleCam'
  | 'toggleSetting'
  | 'toggleLikeList'
  | 'toggleCart'
  | 'toggleLastSeenProduct'
  | 'leavePresentation';

export type NavigationController = {
  prevBtn?: BtnHandler;
  nextBtn?: BtnHandler;
  currentSlideIndex?: number;
  slidesCount?: number;
  slideGroupList?: SlideGroupList;
  loading?: boolean;
};

export type InteractionController = {
  lastSeenBtn?: BtnHandler;
  likelistBtn?: BtnHandler;
  cartBtn?: BtnHandler;
};

export type GuideToolsController = {
  guideBtn?: BtnHandler;
};

export type ParticipantsToolsController = {
  participantsBtn?: BtnHandler;
};

export type MediaController = {
  hidden?: boolean;
  cameraBtn?: BtnHandler;
  microphoneBtn?: BtnHandler;
  settingBtn?: BtnHandler;
  leaveBtn?: BtnHandler;
};

export type Props = {
  toolbarMode?: ToolbarMode;
  meta?: ToolbarMeta;
  navigationController?: NavigationController;
  mediaController?: MediaController;
  guideToolsController?: GuideToolsController;
  participantsToolsController?: ParticipantsToolsController;
  interactionController?: InteractionController;
};

export type Emits = {
  onChange: (event: EventName, data?: any) => void;
};

export const propsKey = Symbol('props') as InjectionKey<Props>;
export const emitsKey = Symbol('emits') as InjectionKey<Emits>;
