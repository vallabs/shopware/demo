<script setup lang="ts">
  type Filtering = {
    order: string;
    sort: string;
  };

  const emit = defineEmits<{
    (e: 'close'): void;
    (e: 'open-page'): void;
    (e: 'filter', criteria: Filtering): void;
  }>();

  const props = withDefaults(
    defineProps<{
      count?: number;
      allowOpenPage?: boolean;
      sortOptions?: { order: string; sort: 'desc' | 'asc' }[];
      grantedPermission?: boolean;
      loading?: boolean;
      searchKey?: string;
      items: any[];
    }>(),
    {
      grantedPermission: false,
      count: 0,
      loading: false,
      searchKey: '',
      allowOpenPage: false,
      sortOptions: () => [
        { order: 'quantity', sort: 'desc' },
        { order: 'revenue', sort: 'desc' },
        { order: 'name', sort: 'asc' },
      ],
    },
  );

  const { t } = useI18n();
  const { clients, inactiveClients, controlledClient, setControlledClient, permissionGranted } =
    useParticipants();
  const { isGuide } = useUser();
  const { requestTokenPermission } = useCart();
  const criteria = reactive<Filtering>({
    order: 'quantity',
    sort: 'desc',
  });

  const searchValue = ref();
  const ALL_PARTICIPANTS = {
    attendeeId: 'all',
    attendeeName: t('presentation.allParticipants'),
  };

  const participants = computed(() => [
    ALL_PARTICIPANTS,
    ...Object.values(clients.value),
    ...Object.values(inactiveClients.value),
  ]);
  const selectedParticipant = ref(
    controlledClient.value ? controlledClient.value : ALL_PARTICIPANTS,
  );
  const allParticipantsSelected = computed(
    () => isGuide.value && selectedParticipant.value?.attendeeId === ALL_PARTICIPANTS.attendeeId,
  );

  const onSearchChanged = (val: string) => {
    // Escape special characters in the search value for use in regular expressions.
    const temp = val.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    searchValue.value = temp;
  };

  const displayedItems = computed<any[]>(() => {
    if (!props.items) return [];
    if (allParticipantsSelected.value && props.searchKey) {
      const regex = new RegExp(searchValue.value, 'i');
      return props.items.filter((item) => regex.test(item[props.searchKey] ?? ''));
    } else {
      return props.items;
    }
  });

  const changeFilter = (order: string, sort: string) => {
    criteria.order = order;
    criteria.sort = sort;
    emit('filter', criteria);
  };

  watch(
    selectedParticipant,
    (participant) => {
      if (isGuide.value && participant) {
        searchValue.value = '';

        if (participant.attendeeId !== 'all') {
          setControlledClient(participant.attendeeId);
        } else {
          setControlledClient('all');
        }
      }
    },
    {
      immediate: true,
    },
  );
</script>

<template>
  <SwSidebar :show-back-btn="false" name="cart-sidebar" @close="$emit('close')">
    <template #header>
      <div class="flex w-full flex-col gap-3">
        <div>
          <p class="text-xl font-bold">
            <slot name="title">{{ $t('wishlist.header') }}</slot>
          </p>
          <span class="text-(xs information-subdued)">
            {{ !count ? $t('cart.noProducts') : $t('cart.products', { p: count }) }}
          </span>
          <SwLink
            v-if="allowOpenPage && isGuide && !allParticipantsSelected && count"
            class="text-(xs information-subdued) inline-flex items-center gap-1 pl-1"
            @click="emit('open-page')"
            >{{ $t('openOnPage') }}
            <span class="i-sw-solid:external-link-s text-xs" />
          </SwLink>
        </div>
        <SwSelect
          v-if="isGuide"
          v-model="selectedParticipant"
          :options="participants"
          label-field="attendeeName"
          key-field="attendeeId"
          size="medium"
          return-object-value
          :label="$t('participant')"
          :placeholder="$t('participant')"
        >
          <template #button-text="{ value, labelName }">
            {{ labelName }} {{ value?.inactive ? `(${$t('inactive')})` : '' }}
          </template>
          <template #item-label="{ value }">
            {{ value?.attendeeName }} {{ value?.inactive ? `(${$t('inactive')})` : '' }}
          </template>
        </SwSelect>

        <SwButton
          v-if="!allParticipantsSelected && !permissionGranted"
          class="!py-2.5"
          @click="requestTokenPermission(selectedParticipant.attendeeId)"
        >
          {{ $t('presentation.participants.requestCartPermission') }}
        </SwButton>
        <div v-if="allParticipantsSelected" class="flex gap-3">
          <SwSearchInput class="w-full" @change="onSearchChanged" />
          <SwDropdownMenu>
            <template #trigger-button="{ open }">
              <SwButton
                variant="action"
                size="medium"
                :disabled="displayedItems.length <= 1"
                @click="open"
              >
                <template #icon>
                  <span class="i-sw-solid:sort text-base" />
                </template>
                <template #default>
                  <span class="capitalize">{{ $t('sort.sort') }}</span>
                </template>
              </SwButton>
            </template>
            <template #content>
              <button
                v-for="option in sortOptions"
                :key="option.order"
                class="hover:bg-interactive-pressed rounded-2 relative flex h-6 items-center px-2 py-1 text-left capitalize"
                @click="changeFilter(option.order, option.sort)"
              >
                <span
                  v-if="criteria?.order === option.order"
                  class="i-sw-regular:checkmark-xs text-primary absolute bottom-0 left-0"
                />
                <span :class="['pl-4', { 'text-primary': criteria?.order === option.order }]">
                  {{ $t(option.order) }}
                </span>
              </button>
            </template>
          </SwDropdownMenu>
        </div>
      </div>
    </template>

    <template #body>
      <slot name="messages" />
      <SwLoading v-if="loading" class="text-primary z-loading absolute inset-0 bg-white text-3xl" />
      <div
        v-if="!allParticipantsSelected && !grantedPermission"
        class="flex h-full flex-col items-center justify-center gap-4 p-10"
      >
        <slot name="errors">
          <p class="text-(base information-dark center) font-semibold">
            {{ $t('presentation.needAskPermission') }}
          </p>
          <SwButton @click="requestTokenPermission(selectedParticipant.attendeeId)">{{
            $t('presentation.participants.requestPermission')
          }}</SwButton>
        </slot>
      </div>
      <div
        v-else-if="!count"
        class="flex h-full flex-col items-center justify-center p-10 text-center"
      >
        <div class="bg-background-raised text-information-subdued mb-4 rounded-full p-6">
          <slot name="empty-icon" />
        </div>
        <p class="text-(base information-dark) mb-1 font-semibold">
          <slot name="empty-label" v-bind="{ isAllSelected: allParticipantsSelected }" />
        </p>
        <p class="text-(sm information-subdued)">
          <slot name="empty-description" v-bind="{ isAllSelected: allParticipantsSelected }" />
        </p>
      </div>
      <div v-else-if="count">
        <div class="flex flex-col px-6">
          <div v-for="item of displayedItems" :key="item?.id">
            <slot name="item" v-bind="{ item, isAllSelected: allParticipantsSelected }" />
          </div>
        </div>
      </div>
    </template>
    <template #footer>
      <slot name="footer" />
    </template>
  </SwSidebar>
</template>
