<script lang="ts" setup>
  import get from 'lodash/get';
  import { ShopwareSearchParams } from '@shopware-pwa/types';
  import isEqual from 'lodash/isEqual';
  import { ProductFilteringControlKey } from '~/helpers/injection';

  const injection = inject(ProductFilteringControlKey);
  const { t } = useI18n();
  const props = withDefaults(
    defineProps<{
      value: { [k: string]: boolean };
      modalController: any;
      loading?: boolean;
      sortEditable?: boolean;
      multipleSelection?: boolean;
    }>(),
    {
      sortEditable: true,
      multipleSelection: true,
    },
  );
  const removeAllProductsModalController = useModal();
  const editProductSortModalController = useModal();
  const {
    setInitialListing,
    search,
    getElements,
    getSortingOrders,
    getTotalPagesCount,
    loadingMore,
    loading: searching,
    loadMore,
    getCurrentPage,
  } = useListing({
    listingType: 'productsListing',
  });

  const products = computed(() => {
    return getElements.value?.filter((x) => props.value[x.id]);
  });

  const productsCount = computed(() => {
    return products.value?.length;
  });

  const DEFAULT_VALUE = {
    ids: [],
    order: 'last-added',
    term: undefined,
  };

  const currentCriteria = reactive<Partial<ShopwareSearchParams>>({ ...DEFAULT_VALUE });

  const sortLabel = computed(() => {
    const sort = sortOptions.value?.find((el) => el.key === currentCriteria.order);

    return sort?.label;
  });

  const sortOptions = computed(() => {
    const options = getSortingOrders.value.map((el) => {
      return {
        key: el.key,
        label: get(el, 'translated.label', el.label),
      };
    });

    options.unshift({ key: 'last-added', label: t('sort.lastAdded') });
    return options;
  });

  const convertCriteria = () => {
    const temp = { ...currentCriteria };
    if (temp.term) {
      temp.query = [
        {
          score: 500,
          query: { type: 'contains', field: 'name', value: temp.term },
        },
        {
          score: 500,
          query: { type: 'contains', field: 'productNumber', value: temp.term },
        },
      ];
      delete temp.term;
    }

    if (temp.order) {
      if (temp.order === 'last-added') {
        temp.useIdSorting = true;
        delete temp.order;
      } else {
        temp.useIdSorting = false;
      }
    }
    return temp;
  };

  const refreshProductsSelection = () => {
    const temp = convertCriteria();

    return search(temp);
  };

  const handleSearch = async (input: string) => {
    currentCriteria.term = input;
    await refreshProductsSelection();
  };

  const handleSort = async (sort: string) => {
    currentCriteria.order = sort;
    await refreshProductsSelection();
  };

  const handleLoadMore = async () => {
    if (getTotalPagesCount.value <= getCurrentPage.value) return;
    const temp = convertCriteria();
    await loadMore({
      ...temp,
      p: getCurrentPage.value + 1,
    });
  };

  const handleRemoveAll = () => {
    setInitialListing(null as any);
    injection?.removeAll();
  };

  const selectedProductIdsCount = computed(() => {
    return Object.values(props.value || {}).filter(Boolean)?.length;
  });

  watch(
    () => props.value,
    async (newValue, oldValue) => {
      if (
        !isEqual(Object.keys(newValue).sort(), Object.keys(oldValue ?? {}).sort()) &&
        Object.keys(newValue).length
      ) {
        currentCriteria.ids = Object.keys(newValue || {});
        await refreshProductsSelection();
      }
    },
    {
      immediate: true,
    },
  );
</script>

<template>
  <div class="flex min-h-0 flex-1 flex-col gap-4">
    <div class="flex items-center justify-between">
      <p class="font-bold">{{ $t('product.products') }} ({{ selectedProductIdsCount }})</p>

      <SwLink
        v-if="selectedProductIdsCount && multipleSelection"
        variant="danger"
        class="text-sm"
        @click="removeAllProductsModalController.open()"
      >
        {{ $t('removeAll') }}
      </SwLink>
    </div>

    <SwButton
      size="large"
      :disabled="modalController.isOpen.value"
      class="!py-3"
      @click="modalController.open()"
      >{{ $t('product.addProducts') }}</SwButton
    >

    <SwSearchInput class="py-1.75" :value="currentCriteria.term" @change="handleSearch" />

    <div v-if="productsCount && sortEditable" class="flex items-center gap-2">
      <span class="text-sm font-bold">{{ $t('sort.sortedBy') }}: </span>
      <span class="text-(sm information)">{{ sortLabel }}</span>

      <SwLink class="text-sm" @click="() => editProductSortModalController.open()">
        {{ $t('edit') }}
      </SwLink>
    </div>

    <div class="relative flex-1">
      <SwLoading v-if="searching" class="text-primary absolute inset-0 z-50 bg-white/50 text-3xl" />
      <div v-if="!selectedProductIdsCount" class="min-h-50 h-full">
        <div class="flex h-full flex-col items-center justify-center gap-4 text-center">
          <div class="bg-background-raised rounded-full p-6">
            <span class="i-sw-solid:product text-information-subdued h-6 w-6" />
          </div>
          <div class="flex flex-col gap-1">
            <p class="font-semibold">{{ $t('product.messages.noProductsAdded') }}</p>
            <p class="text-information-subdued text-sm">
              {{ $t('product.messages.addFirstProducts') }}
            </p>
          </div>
        </div>
      </div>
      <div v-else-if="!productsCount && !searching" class="min-h-50 h-full">
        <div class="flex h-full flex-col items-center justify-center gap-4 text-center">
          <div class="bg-background-raised rounded-full p-6">
            <span class="i-sw-solid:product text-information-subdued h-6 w-6" />
          </div>
          <div class="flex flex-col gap-1">
            <p class="font-semibold">{{ $t('product.messages.noProductsFound') }}</p>
          </div>
        </div>
      </div>
      <InfiniteSelectedProductList
        v-else
        :products="products"
        :is-loading-more="loadingMore"
        @load-more="handleLoadMore"
      />
    </div>
  </div>

  <RemoveAllProductsModal
    v-if="removeAllProductsModalController.isOpen"
    :controller="removeAllProductsModalController"
    @submit="handleRemoveAll"
  />

  <EditProductSortModal
    v-if="editProductSortModalController.isOpen"
    :controller="editProductSortModalController"
    :value="currentCriteria.order"
    :options="sortOptions"
    @submit="handleSort"
  />
</template>
