export default defineNuxtRouteMiddleware(async (to, from) => {
  const { setUserType } = useUser();
  const { setAppointmentId } = useAppointment();
  setUserType(UserType.CLIENT);
  setAppointmentId(to.params.appointmentId as string);
  // await refreshUser();
  // if (!joinedAppointment.value && !joinError.value) {
  //   try {
  //     await join();
  //   } catch (e) {}
  // }
});
