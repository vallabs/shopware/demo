export default defineNuxtRouteMiddleware(async (to, from) => {
  const { isGuideAuthenticated } = useAdminApiInstance();
  const localePath = useLocalePath();
  const { setUserType } = useUser();
  const { setAppointmentId } = useAppointment();
  setUserType(UserType.GUIDE);
  setAppointmentId(to.params.appointmentId as string);
  if (process.client) {
    if (
      isGuideAuthenticated.value &&
      (to?.name as string)?.startsWith('guide-appointmentId-login_')
    ) {
      await abortNavigation();
      return navigateTo(
        localePath({
          name: 'guide-appointmentId',
          params: { appointmentId: to.params.appointmentId as string },
        }),
      );
    }
    if (!isGuideAuthenticated.value && (to?.name as string)?.startsWith('guide-appointmentId_')) {
      await abortNavigation();
      return navigateTo(
        localePath({
          name: 'guide-appointmentId-login',
          params: { appointmentId: to.params.appointmentId as string },
        }),
      );
    }
  }
});
