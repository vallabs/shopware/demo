export default defineNuxtRouteMiddleware(() => {
  const { setUserType } = useUser();
  setUserType(UserType.CLIENT);
});
