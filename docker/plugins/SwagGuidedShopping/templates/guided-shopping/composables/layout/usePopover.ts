const DEFAULT_PANEL_WIDTH = 230;
const DEFAULT_SPACING = 4;

/**
 * Composable to manage popover
 *
 * @public
 * @category Base layout
 * @param el HTML element
 * @param fitPanel Size of popover is fit panel or not
 * @param customWidthPanel custom size of popover
 */
export const usePopover = (
  el: Ref<HTMLElement | undefined>,
  fitPanel = true,
  customWidthPanel = DEFAULT_PANEL_WIDTH,
) => {
  const getPanelStyling = () => {
    const styling: any = {};
    const btnRect = unref(el)?.getBoundingClientRect();

    if (!btnRect) return {};
    const popoverWidth = fitPanel ? btnRect.width : customWidthPanel;
    const screenWidth = window.innerWidth;
    const screenHeight = window.innerHeight;

    const nearBottomEdge = btnRect.bottom + 300 > screenHeight;
    const nearRightEdge = !fitPanel && btnRect.left + popoverWidth > screenWidth;

    if (nearBottomEdge) {
      styling.bottom = `${screenHeight - btnRect.top + DEFAULT_SPACING}px`;
      styling.bottomValue = screenHeight - btnRect.top + DEFAULT_SPACING;
      if (!nearRightEdge) {
        styling.left = `${btnRect.left}px`;
        styling.leftValue = btnRect.left;
      } else {
        styling.right = `${screenWidth - btnRect.right}px`;
        styling.rightValue = screenWidth - btnRect.right;
      }
    } else {
      styling.top = `${btnRect.bottom + DEFAULT_SPACING}px`;
      styling.topValue = btnRect.bottom + DEFAULT_SPACING;
      if (!nearRightEdge) {
        styling.left = `${btnRect.left}px`;
        styling.leftValue = btnRect.left;
      } else {
        styling.right = `${screenWidth - btnRect.right}px`;
        styling.rightValue = screenWidth - btnRect.right;
      }
    }

    styling.width = `${popoverWidth}px`;
    styling.widthValue = popoverWidth;
    styling.elWidth = `${btnRect.width}px`;
    styling.elWidthValue = btnRect.width;
    return styling;
  };

  return {
    getPanelStyling,
  };
};
