/**
 * Interface of toast config
 *
 * @public
 * @category Base layout
 */
export interface ToastConfig {
  /**
   * Toast ID
   */
  id?: number;

  /**
   * Toast content
   */
  content: string;

  /**
   * Variant value
   */
  variant?: 'default' | 'info' | 'error' | 'success' | 'warning';

  /**
   * Toast action text
   */
  actionText?: string;

  /**
   * Toast action function
   */
  actionFn?: Function;

  /**
   * The close button is displayed or not
   */
  showCloseBtn?: boolean;

  /**
   * The time that the toast is displayed by ms
   */
  duration?: number; // ms

  /**
   * The toast displayed or not
   */
  show?: boolean;
}

/**
 * Composable to manage toast
 *
 * @category Base layout
 */
const _useToasts = () => {
  const defaultDuration = 5000;
  const list = ref<ToastConfig[]>([]);
  const timeoutMap = new Map();

  const createToast = (config: ToastConfig) => {
    const id = Date.now();
    config.id = id;
    config.show = true;
    list.value.push(config);
    setDuration(config);
    return config;
  };

  const updateToast = (config: ToastConfig) => {
    if (!config.id) return;
    const index = list.value.findIndex((item: ToastConfig) => item.id === config.id);
    if (index < 0) return;
    list.value[index] = config;
    return config;
  };

  const upsertToast = (config: ToastConfig) => {
    if (config.id) {
      removeDuration(config.id);
      setDuration(config);
      return updateToast(config);
    } else {
      return createToast(config);
    }
  };

  const removeDuration = (id?: number) => {
    if (!id) return;
    const timeoutId = timeoutMap.get(id);
    clearTimeout(timeoutId);
    timeoutMap.delete(timeoutId);
  };

  const setDuration = (config: ToastConfig) => {
    const timeoutId = setTimeout(() => {
      closeToast(config.id);
    }, config.duration || defaultDuration);

    timeoutMap.set(config.id, timeoutId);
  };

  const closeToast = (id?: number) => {
    if (!id) return;
    const index = list.value.findIndex((c) => c.id === id);
    if (index >= 0) {
      list.value[index].show = false;
    }
    setTimeout(() => {
      const index = list.value.findIndex((c) => c.id === id);
      if (index >= 0) {
        list.value.splice(index, 1);
      }
    }, 1000);
  };

  return {
    toasts: computed(() => list.value),
    createToast,
    closeToast,
    updateToast,
    upsertToast,
    setDuration,
    removeDuration,
  };
};

/**
 * Composable to manage toast
 *
 * @public
 * @category Base layout
 */
export const useToasts = createSharedComposable(_useToasts);
