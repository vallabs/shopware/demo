import { Sidebar } from '~/components/presentation/model';

/**
 * Composable to create simple sidebar controllers
 *
 * @public
 * @category Base layout
 */
export const useSidebar = () => {
  const sidebarCpn = ref<Sidebar>();
  const isOpen = ref(false);

  const open = (cpnName: Sidebar) => {
    isOpen.value = true;
    sidebarCpn.value = cpnName;
  };

  const close = () => {
    isOpen.value = false;
  };

  const toggle = (cpnName: Sidebar) => {
    if (sidebarCpn.value && cpnName !== sidebarCpn.value && isOpen.value) {
      return (sidebarCpn.value = cpnName);
    }
    if (isOpen.value) {
      close();
    } else {
      open(cpnName);
    }
  };

  return {
    isOpen: computed(() => isOpen.value),
    sidebarCpn: computed(() => sidebarCpn.value),
    open,
    close,
    toggle,
  };
};

/**
 * Composable to manage left sidebar
 *
 * @public
 * @category Base layout
 */
export const useLeftSidebar = createSharedComposable(useSidebar);

/**
 * Composable to manage right sidebar
 *
 * @public
 * @category Base layout
 */
export const useRightSidebar = createSharedComposable(useSidebar);
