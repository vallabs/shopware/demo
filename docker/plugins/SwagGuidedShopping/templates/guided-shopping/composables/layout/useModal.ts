/**
 * Use it to create simple modal controllers
 *
 * @public
 * @category Base layout
 */
export const useModal = <T = any>() => {
  const { forceCloseAll } = useSharedModal();
  const isOpen = ref(false);
  const data = ref<T | undefined>();

  const open = (value?: T) => {
    data.value = value;
    isOpen.value = true;
  };

  const close = () => {
    isOpen.value = false;
  };

  const toggle = () => {
    isOpen.value = !isOpen.value;
  };

  watch(forceCloseAll, () => {
    if (forceCloseAll.value) {
      close();
    }
  });

  return {
    data: computed(() => data.value),
    isOpen: computed(() => isOpen.value),
    open,
    close,
    toggle,
  };
};

/**
 * Composable to manage shared modal
 *
 * @public
 * @category Base layout
 */
export const useSharedModal = createSharedComposable(() => {
  const forceCloseAll = refAutoReset(false, 100);

  const closeAll = () => {
    forceCloseAll.value = true;
  };

  return {
    forceCloseAll: computed(() => forceCloseAll.value),
    closeAll,
  };
});

/**
 * Composable to manage side menu modal
 *
 * @public
 * @category Base layout
 */
export const useSideMenuModal = createSharedComposable(useModal);
