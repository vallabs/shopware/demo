/**
 * Composable to create simple target
 *
 * @category Base layout
 */
const _useTarget = () => {
  const hoveringEl = ref<HTMLElement | null>(null);

  const setHoveringElement = (element: HTMLElement | null) => {
    hoveringEl.value = element;
  };

  return {
    hoveringEl: computed(() => hoveringEl.value),
    setHoveringElement,
  };
};

/**
 * Composable to create simple target
 *
 * @public
 * @category Base layout
 */
export const useTarget = createSharedComposable(_useTarget);
