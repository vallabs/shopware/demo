// import { useAdminApiInstance } from './admin/useAdminApiInstance';
import { LikeListService } from '~/logic/api-client';

/**
 * Composable to manage bookmark list
 *
 * @public
 * @category Bookmark List
 */
export type UseBookmarkReturn = {
  /**
   * Load the bookmark ids of guide
   * @function
   * @returns {void}
   */
  loadBookmarkIds(): void;

  /**
   * Set of bookmarked ids
   */
  bookmarkIds: Ref<Map<string, boolean>>;
};

/**
 * Composable to use bookmark list
 *
 * @category Bookmark List
 */
const _useBookmark = (): UseBookmarkReturn => {
  const { apiInstance } = useShopwareContext();
  const { isGuide } = useUser();

  const bookmarkIds = ref<Map<string, boolean>>(new Map());

  const loadBookmarkIds = async () => {
    if (!isGuide.value) return;
    const idList = await LikeListService.getLikedProductIds(apiInstance);

    bookmarkIds.value = new Map();
    idList.forEach((id) => bookmarkIds.value.set(id, true));
  };

  return {
    loadBookmarkIds,
    bookmarkIds: computed(() => bookmarkIds.value),
  };
};

/**
 * Composable to use bookmark list
 *
 * @public
 * @category Bookmark List
 */
export const useBookmark = createSharedComposable(_useBookmark);
