import { CmsAction, CmsActionType, ScrollToTarget } from '~/logic/interfaces';

/**
 * Interface of Scroll Options
 *
 * @public
 * @category Interactions
 */
export interface ScrollOptions {
  /**
   * Container included scroll
   */
  container: string;

  /**
   * Offset value of scroll
   */
  offset?: number;
}

/**
 * Scroll Options
 */
const SCROLL_OPTIONS: ScrollOptions = {
  container: 'cms-container',
  offset: 100,
};

/**
 * Enum of scroll container type
 *
 * @public
 * @category Interactions
 */
export enum SCROLL_CONTAINER_TYPE {
  STANDARD = 'standard',
  PRODUCT_VIRTUAL = 'products-virtual',
}

const HIGHLIGHT_DURATION = 5000;
const HOVERING_TIME = 1500;

// type SlotContext = {
//   type: string,
// }

/**
 * Composable to manage element interaction
 *
 * @category Interactions
 */
const _useElementInteraction = (defaultScrollOptions: ScrollOptions = SCROLL_OPTIONS) => {
  const containerRef = ref<any>({});
  const lastElementScrolledTo = ref<ScrollToTarget>();
  const scrollOptions = ref({
    ...SCROLL_OPTIONS,
    ...defaultScrollOptions,
  });
  const { publishInteraction } = useInteractions();
  const { mercurePublish, connected } = useMercure();
  const debounceHover = ref();
  const { isGuide, isClient, attendee } = useUser();
  const { lastClientHoveredElement, lastGuideHoveredElement } = usePresentationState();
  //   const { publishHover } = useHoverElement(elementId);
  //   const scrollToIsLoading = ref<boolean>(false);
  //   const slotContext = ref<SlotContext>(slotContextOptions);
  //   const { appointment } = useAppointment();

  //   function setSlotContext(value: SlotContext) {
  //     slotContext.value = value;
  //   }

  const setContainerRef = (id: string, ref: any) => {
    containerRef.value = {
      ...containerRef.value,
      [id]: ref,
    };
  };

  /**
   * forces client to scroll to guide position
   *
   * @returns {Promise<void>}
   */
  const publishScrollTo = (elementId: string, highlightId = '', actions: CmsAction[] = []) => {
    mercurePublish.guide(
      MercureEvent.SCROLLED_TO,
      buildScrollToTarget(elementId, highlightId, actions),
    );
  };

  //   function handleCmsElements(): boolean {
  //     const element = tryGetElement();
  //     if (!element) return false;
  //     const index = getTargetIndex(element);
  //     if (index === null) return false;
  //     mercurePublish.guide('scrolledTo', {
  //       elementId: elementId,
  //       index,
  //       attendeeId: appointment.value?.attendeeId,
  //       slotContext: slotContext.value
  //     });

  //     return true;
  //   }

  //   // tabs: product-description-reviews and cross-selling
  //   // image-gallery
  //   // product-slider

  //   function getTargetIndex(element: Element): number {
  //     let target = null;

  //     if (slotContext.value.type === 'image-gallery') {
  //       target = element.querySelector('.gallery.gallery-big');
  //       if (!target) target = element.querySelector('.sf-gallery__item--selected');
  //     } else if (slotContext.value.type === 'product-description-reviews' || slotContext.value.type ===  'cross-selling') {
  //       target = element.querySelector('.sf-tabs');
  //       const listTabs = target.getElementsByClassName('sf-tabs__title');
  //       return Array.from(listTabs).findIndex((x: any) => x.className.includes('is-active'));
  //     } else {
  //       target = element.closest('.glide__slide');
  //     }
  //     if (!target) return null;

  //     try {
  //       return target.querySelector('.glide').glide.index;
  //     } catch (e) {
  //       const isProductGlider = target.classList.contains('glide__slide') && target.firstChild.id;

  //       let children: Array<Element> = target.parentElement.children;
  //       if (!isProductGlider) return Array.from(children).indexOf(target);

  //       children = Array.from(children).filter((item:Element) => !item.classList.contains('glide__slide--clone'));
  //       const productIndex = Array.from(children).indexOf(target)
  //       if (productIndex > 0) return productIndex;

  //       let idArray = [];
  //       children.forEach((child: Element) => {
  //         const innerElement = <Element>child.firstChild;
  //         idArray.push(innerElement.id);
  //       });

  //       return idArray.indexOf(target.firstChild.id);
  //     }
  //   }

  //   function getTargetsForElementType(element: Element): Array<Element> {
  //     let target = null;

  //     if (slotContext.value.type === 'image-gallery') {
  //       target = element.querySelector('.sf-gallery__item');
  //     } else if (slotContext.value.type === 'product-description-reviews' || slotContext.value.type ===  'cross-selling') {
  //       target = element.getElementsByClassName('sf-tabs__title');
  //       return Array.from(target).length ? Array.from(target) : null;
  //     } else {
  //       target = element.closest('.glide__slide');
  //     }

  //     if (!target) return null;
  //     return Array.from(target.parentElement.children);
  //   }

  //   function tryGetElement(tempElementId = elementId): Element {
  //     try {
  //       return document.querySelector(`#element-${tempElementId}`)
  //     } catch (e) {
  //       return null;
  //     }
  //   }

  /**
   * client scrolls to guide
   */
  const goToGuide = () => {
    if (!lastGuideHoveredElement.value || !isClient.value) return;
    scrollToElement(lastGuideHoveredElement.value);
  };

  /**
   * guide scrolls to client
   */
  const goToClient = (attendeeId: string) => {
    if (!lastClientHoveredElement.value?.[attendeeId]) return;
    scrollToElement(lastClientHoveredElement.value?.[attendeeId]);
  };

  const setLastElementScrolledTo = (elementData: ScrollToTarget) => {
    lastElementScrolledTo.value = elementData;
    resetLastElementScrolledToDebounced();
  };

  const resetLastElementScrolledTo = () => {
    lastElementScrolledTo.value = undefined;
  };

  const resetLastElementScrolledToDebounced = useDebounceFn(() => {
    resetLastElementScrolledTo();
  }, HIGHLIGHT_DURATION);

  const scrollToElement = async (elementData: ScrollToTarget) => {
    if (!elementData) return;
    const { elementId, containerId, actions } = elementData;
    const container = containerRef.value[containerId];
    const target = document.getElementById(elementId);
    if (!container || !target) return;
    // scroll to the element position
    containerRef.value?.[containerId]?.scrollTo({
      top: target.offsetTop - (scrollOptions.value?.offset ?? 0),
      behavior: 'smooth',
    });
    await nextTick();
    if (actions?.length) {
      try {
        actions.forEach((action) => {
          const $ref = containerRef.value?.[action.elementId];
          if (!$ref) return;
          if (action.type === CmsActionType.VIRTUAL_SCROLLING) {
            const sizes = $ref.items?.[0].item.data?.length;
            $ref.scrollToItem(Math.floor(action.itemIndex / sizes));
          } else if (action.type === CmsActionType.SLIDER) {
            $ref.slideTo(action.slideIndex);
          } else if (action.type === CmsActionType.TAB) {
            $ref.changeTab(action.tabIndex);
          }
        });
      } catch (e) {}
    }
    await nextTick();
    setTimeout(() => {
      setLastElementScrolledTo(elementData);
    });
  };

  const buildScrollToTarget = (
    elementId: string,
    highlightId = '',
    actions: CmsAction[] = [],
  ): ScrollToTarget => {
    return {
      elementId,
      highlightId: highlightId || elementId,
      containerId: scrollOptions.value.container,
      actions,
      publishedBy: attendee.value.id,
    };
  };

  const publishHoverStart = (
    elementId: string,
    highlightId?: string,
    actions: CmsAction[] = [],
    duration = HOVERING_TIME,
  ) => {
    if (!canPublishHover(elementId, highlightId)) return;

    debounceHover.value = setTimeout(() => {
      publishHover(elementId, highlightId, actions);
    }, duration);
  };

  const publishHoverEnd = () => {
    clearDebounceHover();
  };

  onBeforeUnmount(() => {
    clearDebounceHover();
  });

  const clearDebounceHover = () => {
    if (debounceHover.value) {
      clearTimeout(debounceHover.value);
      debounceHover.value = null;
    }
  };

  // publishes currently hovered element by guide or client
  const publishHover = (elementId: string, highlightId?: string, actions: CmsAction[] = []) => {
    if (!canPublishHover(elementId, highlightId)) return;
    publishGuideHover(buildScrollToTarget(elementId, highlightId, actions));
    publishClientHover(buildScrollToTarget(elementId, highlightId, actions));
  };

  const canPublishHover = (elementId: string, highlightId?: string) => {
    if (!elementId || !connected.value) return;
    if (!highlightId && isGuide.value && lastGuideHoveredElement.value?.elementId === elementId)
      return false;
    if (
      !highlightId &&
      isClient.value &&
      lastClientHoveredElement.value?.[attendee.value.id]?.elementId === elementId
    )
      return false;
    if (highlightId && isGuide.value && lastGuideHoveredElement.value?.highlightId === highlightId)
      return false;
    if (
      highlightId &&
      isClient.value &&
      lastClientHoveredElement.value?.[attendee.value.id]?.highlightId === highlightId
    )
      return false;
    return true;
  };

  const publishGuideHover = (elementData: ScrollToTarget) => {
    if (!isGuide.value) return;

    mercurePublish.guide(MercureEvent.GUIDE_HOVERED, elementData);
    publishInteraction({
      name: InteractionName.GUIDE_HOVERED,
      payload: {
        hoveredElementId: elementData.highlightId,
      },
    });
  };

  const publishClientHover = (elementData: ScrollToTarget) => {
    if (!isClient.value) return;

    mercurePublish.client(MercureEvent.CLIENT_HOVERED, elementData);
  };

  return {
    goToGuide,
    goToClient,
    //     publishScrollTo,
    //     scrollToIsLoading,
    //     setSlotContext,
    lastElementScrolledTo: computed(() => lastElementScrolledTo.value),
    scrollToElement,
    publishScrollTo,
    resetLastElementScrolledTo,
    publishHoverStart,
    publishHoverEnd,
    publishHover,
    setContainerRef,
    containerRef: computed(() => containerRef.value),
  };
};

/**
 * Composable to manage element interaction
 *
 * @public
 * @category Interactions
 */
export const useElementInteraction = createSharedComposable(_useElementInteraction);
