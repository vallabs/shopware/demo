import { CmsPage } from '@shopware-pwa/types';
import { AppService } from '~/logic/api-client';
import { ShopPage } from '~/logic/interfaces';

export const _useShopPage = () => {
  const { apiInstance } = useShopwareContext();
  const shopPageModalController = useModal();
  const shopPagesData = ref<{
    [key: string]: CmsPage | null;
  }>({});

  const currentShopPage = ref<CmsPage | null>();

  const isLoading = ref(false);

  const fetchShopPage = async (shopPageName: ShopPage) => {
    isLoading.value = true;
    const layout = await AppService.getShopPage(shopPageName, apiInstance);
    currentShopPage.value = layout;
    shopPagesData.value[shopPageName] = layout;
    isLoading.value = false;
  };

  const openShopPage = async (shopPageName: ShopPage) => {
    currentShopPage.value = null;
    shopPageModalController.open();
    const layout = getShopPageLayout(shopPageName);
    // if we already have the layout, we don't need to fetch it again
    if (layout) {
      currentShopPage.value = layout;
      return;
    }
    await fetchShopPage(shopPageName);
  };

  const getShopPageLayout = (shopPageName: ShopPage) => {
    return shopPagesData.value?.[shopPageName];
  };

  return {
    currentShopPage: computed(() => currentShopPage.value),
    isLoading: computed(() => isLoading.value),
    openShopPage,
    shopPageModalController,
  };
};

/**
 * Composable to manage shop pages
 *
 * @public
 * @category Storefront
 */
export const useShopPage = createSharedComposable(_useShopPage);
