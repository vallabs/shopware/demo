const PRESENTATION_LAYER_NAMESPACE = 'presentationLayer';

/**
 * Composable to manage Mercure observer
 *
 * @category Presentation
 */
const _useMercureObserver = () => {
  const { mercureSubscribe, mercureUnsubscribe } = useMercure();

  const isSubscribed = ref<boolean>(false);

  /**
   * registers all event listeners
   * and removes all previously registered listeners
   */
  const subscribeToMercureEvents = () => {
    if (isSubscribed.value) return;
    isSubscribed.value = true;

    //   mercureSubscribe('highlighted', (value) => {
    //     highlightedElement.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    // updateState = (key: MercureEvent, state: any)
    mercureSubscribe(MercureEvent.SCROLLED_TO, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.STATE_FOR_ALL, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.STATE_FOR_GUIDES, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.STATE_FOR_CLIENTS, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.GUIDE_HOVERED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.CLIENT_HOVERED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.NAVIGATED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.CHANGED_CART, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.REQUESTED_TOKEN_PERMISSIONS, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.LAST_ATTENDEE_CHANGE_CART, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.GRANTED_TOKEN_PERMISSION, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.DYNAMIC_PAGE_OPENED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.DYNAMIC_PAGE_CLOSED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.ALL_DYNAMIC_PAGE_CLOSED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(
      MercureEvent.DYNAMIC_PRODUCT_LISTING_PAGE_LOADED_MORE,
      PRESENTATION_LAYER_NAMESPACE,
    );
    mercureSubscribe(MercureEvent.GUIDE_CHANGED_LIKES, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.CLIENT_CHANGED_LIKES, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.NEW_PRODUCT_LISTING_CREATED, PRESENTATION_LAYER_NAMESPACE);
    mercureSubscribe(MercureEvent.PRODUCT_LISTING_UPDATED, PRESENTATION_LAYER_NAMESPACE);

    // mercureSubscribe(
    //   'guide.hovered',
    //   (value: string) => {
    //     console.log('guide.hovered', value);

    //     lastGuideHoveredElement.value = value;
    //   },
    //   PRESENTATION_LAYER_NAMESPACE,
    // );

    //   mercureSubscribe('client.changedLikes', (value: string) => {
    //     lastClientChangedLikes.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('product.wasLiked', (value: string) => {
    //     likedProduct.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('guide.changedLikes', (value: string) => {
    //     lastGuideChangedLikes.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('client.changedCart', (value: string) => {
    //     lastClientChangedCart.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('guide.changedCart', (value: string) => {
    //     lastGuideChangedCart.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('scrolledTo', (value) => {
    //     scrollToTarget.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('grantedTokenPermissions', (value) => {
    //     grantedTokenPermissions.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('dynamicPage.opened', (value: DynamicPageResponse) => {
    //     currentDynamicPage.value = value;
    //     highlightedElement.value = '';
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('dynamicProductListingPage.loadedMore', (value: DynamicProductListingPage) => {
    //     if (value.page && value.type) {
    //       if (value.page > (currentLoadedInfo.value?.page || 0)) {
    //         currentLoadedInfo.value = value;
    //       }
    //     }
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('dynamicPage.closed', (value: DynamicPageResponse) => {
    //     currentDynamicPage.value = value;
    //     highlightedElement.value = '';
    //   }, PRESENTATION_LAYER_NAMESPACE);

    //   mercureSubscribe('activeSpeakerUpdated', (value: string) => {
    //     activeSpeaker.value = value;
    //   }, PRESENTATION_LAYER_NAMESPACE);
  };

  // /**
  //  * removes all event listeners
  //  */
  const unsubscribeMercureEvents = () => {
    if (!isSubscribed.value) return;
    isSubscribed.value = false;

    //   mercureUnsubscribe('highlighted', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('client.changedLikes', PRESENTATION_LAYER_NAMESPACE);
    // mercureUnsubscribe(MercureEvent.PRODUCT_WAS_LIKED, PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('guide.changedLikes', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('client.changedCart', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('guide.changedCart', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('navigated', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('dynamicPage.opened', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('dynamicProductListingPage.loadedMore', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('dynamicPage.closed', PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.SCROLLED_TO, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.REQUESTED_TOKEN_PERMISSIONS, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.GRANTED_TOKEN_PERMISSION, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.STATE_FOR_ALL, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.STATE_FOR_GUIDES, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.STATE_FOR_CLIENTS, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.GUIDE_HOVERED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.CLIENT_HOVERED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.NAVIGATED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.CHANGED_CART, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.LAST_ATTENDEE_CHANGE_CART, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.DYNAMIC_PAGE_OPENED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.DYNAMIC_PAGE_CLOSED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.ALL_DYNAMIC_PAGE_CLOSED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.GUIDE_CHANGED_LIKES, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.CLIENT_CHANGED_LIKES, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(
      MercureEvent.DYNAMIC_PRODUCT_LISTING_PAGE_LOADED_MORE,
      PRESENTATION_LAYER_NAMESPACE,
    );
    mercureUnsubscribe(MercureEvent.NEW_PRODUCT_LISTING_CREATED, PRESENTATION_LAYER_NAMESPACE);
    mercureUnsubscribe(MercureEvent.PRODUCT_LISTING_UPDATED, PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('newInstantListingCreated', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('instantListingUpdated', PRESENTATION_LAYER_NAMESPACE);
    //   mercureUnsubscribe('activeSpeakerUpdated', PRESENTATION_LAYER_NAMESPACE);
  };

  /**
   * remove all subscribed events
   */
  onUnmounted(() => {
    unsubscribeMercureEvents();
  });

  return {
    subscribeToMercureEvents,
    //   highlightedElement,
    //   lastClientChangedLikes,
    //   likedProduct,
    //   lastGuideChangedLikes,
    //   lastClientChangedCart,
    //   lastGuideChangedCart,
    //   scrollToTarget,
    //   grantedTokenPermissions,
    //   currentDynamicPage,
    //   newInstantListing,
    //   instantListingUpdated,
    //   currentLoadedInfo,
    //   activeSpeaker
  };
};

/**
 * Composable to manage Mercure observer
 *
 * @public
 * @category Presentation
 */
export const useMercureObserver = createSharedComposable(_useMercureObserver);
