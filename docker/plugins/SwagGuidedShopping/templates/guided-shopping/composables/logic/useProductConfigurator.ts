import {
  useProductConfigurator as coreUseProductConfigurator,
  UseProductConfiguratorReturn,
} from '@shopware-pwa/composables-next';
import { Product } from '@shopware-pwa/types';
import { invokePost, getProductEndpoint } from '@shopware-pwa/api-client';

/**
 * Composable to manage Product configurator
 */
export const useProductConfigurator = (): UseProductConfiguratorReturn => {
  const { apiInstance } = useShopwareContext();
  const coreFunctionality = coreUseProductConfigurator();
  const { product } = useProduct();

  //   const isLoadingOptions = ref(!!product.value.options?.length);
  const parentProductId = computed(() => product.value?.parentId);
  const findVariantForSelectedOptions = async (options?: {
    [code: string]: string;
  }): Promise<Product | undefined> => {
    const filter = [
      {
        type: 'equals',
        field: 'parentId',
        value: parentProductId.value,
      },
      ...Object.values(options || coreFunctionality.getSelectedOptions.value).map((id) => ({
        type: 'equals',
        field: 'optionIds',
        value: id,
      })),
    ];
    try {
      /* istanbul ignore next */
      if (apiInstance) {
        apiInstance.defaults.headers.common['sw-include-seo-urls'] = 'true';
      }
      const response = await invokePost(
        {
          address: getProductEndpoint(),
          payload: {
            limit: 1,
            filter,
            includes: {
              product: [
                'id',
                'translated',
                'productNumber',
                'calculatedPrice',
                'calculatedPrices',
                'calculatedCheapestPrice',
                'calculatedMaxPurchase',
                'seoUrls',
                'cover',
                'stock',
                'options',
                'name',
                'description',
                'media',
                'shippingFree',
                'deliveryTime',
                'availableStock',
                'totalReviews',
                'minPurchase',
                'maxPurchase',
                'purchaseSteps',
                'restockTime',
                'isCloseout',
                'packUnit',
                'tax',
                'unit',
                'packUnitPlural',
                'referenceUnit',
                'purchaseUnit',
                'releaseDate',
                'customFields',
                'sortedProperties',
              ],
              seo_url: ['seoPathInfo'],
            },
            associations: {
              seoUrls: {},
              cover: {},
              media: {},
              properties: {
                associations: {
                  group: {},
                },
              },
            },
          },
        },
        apiInstance,
      );
      return (response as { data?: { elements?: Array<Product> } })?.data?.elements?.[0]; // return first matching product
    } catch (e) {
      console.error('SwProductDetails:findVariantForSelectedOptions', e);
    }
  };

  return {
    ...coreFunctionality,
    findVariantForSelectedOptions,
  };
};
