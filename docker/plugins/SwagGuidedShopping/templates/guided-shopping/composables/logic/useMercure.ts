import { EventSourcePolyfill } from 'event-source-polyfill';
import { useAppointment } from './useAppointment';
import { ToastConfig } from '~/composables/layout/useToasts';
import { ActionName } from '~/logic/interfaces';

/**
 * Composable to manage enum of Mercure Event
 *
 * @public
 * @category Presentation
 */
export enum MercureEvent {
  HEARTBEAT = 'heartbeat',
  SCROLLED_TO = 'scrolledTo',
  HIGHLIGHTED = 'highlighted',
  GUIDE_HOVERED = 'guide.hovered',
  CLIENT_HOVERED = 'client.hovered',
  CLIENT_CHANGED_LIKES = 'client.changedLikes',
  GUIDE_CHANGED_LIKES = 'guide.changedLikes',
  // PRODUCT_LIKE_CHANGED = 'product.likeChange',
  CHANGED_CART = 'changedCart',
  LAST_ATTENDEE_CHANGE_CART = 'lastAttendeeChangeCart',
  DYNAMIC_PAGE_OPENED = 'dynamicPage.opened',
  DYNAMIC_PAGE_CLOSED = 'dynamicPage.closed',
  ALL_DYNAMIC_PAGE_CLOSED = 'all.dynamicPages.closed',
  DYNAMIC_PRODUCT_LISTING_PAGE_LOADED_MORE = 'dynamicProductListingPage.loadedMore',
  NAVIGATED = 'navigated',
  REQUESTED_TOKEN_PERMISSIONS = 'requestedTokenPermissions',
  GRANTED_TOKEN_PERMISSION = 'grantedTokenPermission',
  STATE_FOR_ALL = 'state-for-all',
  STATE_FOR_GUIDES = 'state-for-guides',
  STATE_FOR_CLIENTS = 'state-for-clients',
  STATE_FOR_ME = 'state-for-me',
  NEW_PRODUCT_LISTING_CREATED = 'newProductListingCreated',
  PRODUCT_LISTING_UPDATED = 'productListingUpdated',
  ACTIVE_SPEAKER_UPDATED = 'activeSpeakerUpdated',
}

/**
 * Interface of Mercure event callbacks
 *
 * @public
 * @category Presentation
 */
export interface MercureEventCallbacks {
  [key: string]: {
    [key: string]: { (value: string, local?: boolean): void }[];
  };
}

const DEFAULT_NAMESPACE = 'mercure';

/**
 * Composable to manage Mercure
 *
 * @category Presentation
 */
const _useMercure = () => {
  const config = useRuntimeConfig();
  const { apiInstance, setXHRRequests, xhrRequests } = useShopwareContext();
  const nuxtApp = useNuxtApp();
  const { isGuide, isClient } = useUser();
  const { joinedAppointment, appointment, isSelfService } = useAppointment();
  const { upsertToast } = useToasts();
  const currentToastConfig = ref<ToastConfig>();
  const { updateState } = usePresentationState();
  const { t } = useI18n();
  // const { sharedRef } = useSharedState();
  // const { broadcast } = useIntercept();
  // const _isConnected = sharedRef<boolean>(`${contextName}-isConnected`);
  const _showConnectError = ref<boolean>(false);
  const isTryingToConnect = ref<boolean>();
  const _isConnected = ref<boolean>();
  const _lastMercureMsgId = ref<string>();
  const eventCallbacks = ref<MercureEventCallbacks>({});

  let eventSource: EventSource | null = null;

  const reconnectFrequencyMultiplier = 2;
  const reconnectMaxSeconds = 64;
  let reconnectFrequencySeconds = 1;
  const showErrorMsgAfterSeconds = 8;

  /**
   * connects to the mercure server
   */
  const connect = (reconnect = false) => {
    if (
      isSelfService.value ||
      _isConnected.value ||
      isTryingToConnect.value ||
      !joinedAppointment.value ||
      !appointment.value
    )
      return;

    // show toast if connecting or reconnecting
    if (reconnect) {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        duration: 8000,
        content: t('presentation.reconnecting'),
        variant: 'warning',
      });
    } else {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        duration: 8000,
        content: t('presentation.connecting'),
        variant: 'info',
      });
    }
    isTryingToConnect.value = true;

    // build url
    const url = new URL(appointment.value.mercureHubPublicUrl);
    appointment.value.mercureSubscriberTopics.forEach((topic: string) => {
      url.searchParams.append('topic', encodeURIComponent(topic));

      // append id of last event on reconnect so the client gets all events from that point on
      if (reconnect && _lastMercureMsgId.value) {
        url.searchParams.append('Last-Event-ID', _lastMercureMsgId.value);
      }
    });
    const options = !config.public.allowAnonymousMercure
      ? {
          withCredentials: true,
        }
      : {};
    eventSource = new EventSourcePolyfill(url.toString(), {
      ...options,
      headers: {
        Authorization: `Bearer ${appointment.value.JWTMercureSubscriberToken}`,
      },
    });
    eventSource.onmessage = onEventSourceMessage;
    eventSource.onopen = !reconnect ? onEventSourceOpen : onEventSourceReconnectedOpen;
    eventSource.onerror = onEventSourceError;
  };
  /**
   * on message receive from the mercure server
   * @param e
   */
  const onEventSourceMessage = (e: MessageEvent) => {
    if (!e.data) return;
    const data = JSON.parse(e.data);
    if (data.appId === nuxtApp.$appId.value) return;
    _lastMercureMsgId.value = e.lastEventId;
    if (!data) return;
    Object.entries(data).forEach(([eventName, value]) => {
      const name = eventName.toString() as MercureEvent;
      onValueChangeFromMercure(name, value);
    });
  };

  /**
   * on connection open to the mercure server
   */
  const onEventSourceOpen = () => {
    resetReconnectDelay();
    _isConnected.value = true;
    _showConnectError.value = false;
    isTryingToConnect.value = false;
    currentToastConfig.value = upsertToast({
      ...currentToastConfig.value,
      duration: 8000,
      content: t('presentation.connected'),
      variant: 'success',
    });
    console.log('[Mercure] connected');
  };

  /**
   * on reconnected connection open to the mercure server
   */
  const onEventSourceReconnectedOpen = () => {
    resetReconnectDelay();
    _isConnected.value = true;
    _showConnectError.value = false;
    isTryingToConnect.value = false;
    currentToastConfig.value = upsertToast({
      ...currentToastConfig.value,
      duration: 8000,
      content: t('presentation.reconnected'),
      variant: 'success',
    });
    console.log('[Mercure] reconnected');
    // broadcast("mercure-reconnect", { eventSource: eventSource });
  };

  /**
   * on connection error to the mercure server
   */
  const onEventSourceError = () => {
    _isConnected.value = false;
    isTryingToConnect.value = false;
    eventSource?.close();
    eventSource = null;

    currentToastConfig.value = upsertToast({
      ...currentToastConfig.value,
      duration: 8000,
      content: t('presentation.connectionLost'),
      variant: 'error',
    });

    setTimeout(() => {
      if (reconnectFrequencySeconds >= showErrorMsgAfterSeconds) {
        _showConnectError.value = true;
      }
      connect(true);
      increaseReconnectDelay();
    }, reconnectFrequencySeconds * 1000);
  };

  /**
   * increments the reconnect delay
   */
  const increaseReconnectDelay = () => {
    reconnectFrequencySeconds *= reconnectFrequencyMultiplier;
    if (reconnectFrequencySeconds >= reconnectMaxSeconds) {
      reconnectFrequencySeconds = reconnectMaxSeconds;
    }
  };

  /**
   * resets the reconnect delay
   */
  const resetReconnectDelay = () => {
    reconnectFrequencySeconds = 1;
  };

  /**
   * fires all registered callbacks for the event
   *
   * @param event
   * @param value
   */
  const onValueChangeFromMercure = (event: MercureEvent, value: any, local?: boolean) => {
    if (!eventCallbacks.value[event]) return;
    updateState(event, value, local);
    Object.values(eventCallbacks.value[event]).forEach((callbacks) => {
      callbacks.forEach((callback) => {
        if (typeof callback === 'function') callback(value, local);
      });
    });
  };

  /**
   * registers a callback for a mercure event on the current element
   * @param event
   * @param callback
   * @param namespace
   */
  const mercureSubscribe = (
    event: MercureEvent,
    namespace = DEFAULT_NAMESPACE,
    cb: (payload: any, local?: boolean) => void = () => {},
  ) => {
    eventCallbacks.value = eventCallbacks.value || {};
    eventCallbacks.value[event] = eventCallbacks.value[event] || {};
    eventCallbacks.value[event][namespace] = eventCallbacks.value[event][namespace] || [];
    eventCallbacks.value[event][namespace].push(cb);
  };

  /**
   * unregisters all callbacks for a mercure event on the current element
   *
   * @param event
   * @param namespace
   */
  const mercureUnsubscribe = (event: MercureEvent, namespace = DEFAULT_NAMESPACE) => {
    eventCallbacks.value[event] = eventCallbacks.value[event] || {};
    delete eventCallbacks.value[event][namespace];
  };

  /**
   * publish mercure event only allowed as a client
   *
   * @param event
   * @param value
   */
  const mercureClientPublish = <T>(
    event: MercureEvent,
    value: T | null = null,
    actionName?: ActionName,
  ) => {
    if (!isClient.value || isSelfService.value) return;
    return mercurePublish(event, value, actionName);
  };

  /**
   * publish mercure event only allowed as a guide
   *
   * @param event
   * @param value
   */
  const mercureGuidePublish = <T>(
    event: MercureEvent,
    value: T | null = null,
    actionName?: ActionName,
  ) => {
    if (!isGuide.value || isSelfService.value) return;
    return mercurePublish(event, value, actionName);
  };

  /**
   * publish mercure event only locally
   *
   * @param event
   * @param value
   */
  const mercureLocalPublish = <T>(event: MercureEvent, value: T | any = null) => {
    onValueChangeFromMercure(event, value, true);
  };

  /**
   * publish mercure event
   *
   * @param event
   * @param value
   */
  const mercurePublish = async <T>(
    event: MercureEvent,
    value: T | null = null,
    actionName?: ActionName,
  ) => {
    if (!apiInstance || isSelfService.value || !_isConnected.value || !appointment.value) return;
    mercureLocalPublish(event, value);
    await publishEvent(event, value, actionName);
  };

  /**
   * publishes mercure event
   *
   * @param event
   * @param value
   * @param appointment
   */
  const publishEvent = <T>(event: MercureEvent, value: T, actionName?: ActionName) => {
    if (!appointment.value) return;
    if (!appointment.value.JWTMercurePublisherToken) return;
    if (!appointment.value.mercurePublisherTopic) return;
    const data = {
      [event]: value,
      appId: nuxtApp.$appId.value,
    };

    // cross domain mercure configuration
    const http = new XMLHttpRequest();
    const url = appointment.value.mercureHubPublicUrl;
    const body = new URLSearchParams({
      data: JSON.stringify(data),
      topic: appointment.value.mercurePublisherTopic,
      private: 'on',
    });
    http.open('POST', url, true);
    http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    http.setRequestHeader('Authorization', 'Bearer ' + appointment.value.JWTMercurePublisherToken);
    if (xhrRequests.value[event]) {
      xhrRequests.value[event].abort();
    }
    setXHRRequests(actionName ?? event, http);
    http.send(body);
  };

  return {
    mercurePublish: {
      guide: mercureGuidePublish,
      client: mercureClientPublish,
      local: mercureLocalPublish,
      all: mercurePublish,
    },
    mercureSubscribe,
    mercureUnsubscribe,
    connect,
    onEventSourceMessage,
    onEventSourceOpen,
    onEventSourceReconnectedOpen,
    onEventSourceError,
    connected: computed(() => _isConnected.value),
    showConnectError: computed(() => _showConnectError.value),
  };
};

/**
 * Composable to manage Mercure
 *
 * @public
 * @category Presentation
 */
export const useMercure = createSharedComposable(_useMercure);
