import { ProductsService } from '~/logic/api-client';

export const useAdminProduct = () => {
  const { adminApiInstance } = useAdminApiInstance();

  const _product = ref();
  const _parentProduct = ref();

  const getAdminProduct = async (data: { productId: string; parentId?: string }): Promise<void> => {
    const ids = [data.productId];
    if (data.parentId) {
      ids.push(data.parentId);
    }
    const response = await ProductsService.getAdminProduct(
      {
        associations: {
          deliveryTime: {},
        },
        ids,
        rating: undefined,
      },
      adminApiInstance,
    );
    _product.value = response[0];
    if (data.parentId) {
      _parentProduct.value = response[1];
    }
  };

  const prices = computed(() => {
    return {
      taxRate: _product.value?.tax
        ? _product.value?.tax.taxRate
        : _parentProduct.value?.tax?.taxRate,
      price: {
        net: _product.value?.price?.length
          ? _product.value?.price?.[0]?.net
          : _parentProduct.value?.price?.[0]?.net,
        gross: _product.value?.price?.length
          ? _product.value?.price?.[0]?.gross
          : _parentProduct.value?.price?.[0]?.gross,
      },
      purchasePrice: {
        net: _product.value?.purchasePrices?.length
          ? _product.value?.purchasePrices?.[0]?.net
          : _parentProduct.value?.purchasePrices?.[0]?.net,
        gross: _product.value?.purchasePrices?.length
          ? _product.value?.purchasePrices?.[0]?.gross
          : _parentProduct.value?.purchasePrices?.[0]?.gross,
      },
      listPrice: {
        net: _product.value?.price?.length
          ? _product.value?.price?.[0]?.listPrice?.net
          : _parentProduct.value?.price?.[0]?.listPrice?.net,
        gross: _product.value?.price?.length
          ? _product.value?.price?.[0]?.listPrice?.gross
          : _parentProduct.value?.price?.[0]?.listPrice?.gross,
      },
      cheapestPrice: {
        net: _product.value?.price?.length
          ? _product.value?.price?.[0]?.regulationPrice?.net
          : _parentProduct.value?.price?.[0]?.regulationPrice?.net,
        gross: _product.value?.price?.length
          ? _product.value?.price?.[0]?.regulationPrice?.gross
          : _parentProduct.value?.price?.[0]?.regulationPrice?.gross,
      },
    };
  });

  const deliverability = computed(() => {
    return {
      stock: _product.value?.stock,
      availableStock: _product.value?.availableStock,
      isCloseout:
        _product.value?.isCloseout !== null
          ? _product.value?.isCloseout
          : _parentProduct.value?.isCloseout,
      deliveryTime:
        _product.value?.deliveryTime !== null
          ? _product.value?.deliveryTime
          : _parentProduct.value?.deliveryTime,
      restockTime:
        _product.value?.restockTime !== null
          ? _product.value?.restockTime
          : _parentProduct.value?.restockTime,
      shippingFree:
        _product.value?.shippingFree !== null
          ? _product.value?.shippingFree
          : _parentProduct.value?.shippingFree,
      minPurchase:
        _product.value?.minPurchase !== null
          ? _product.value?.minPurchase
          : _parentProduct.value?.minPurchase,
      purchaseSteps:
        _product.value?.purchaseSteps !== null
          ? _product.value?.purchaseSteps
          : _parentProduct.value?.purchaseSteps,
      maxPurchase:
        _product.value?.maxPurchase !== null
          ? _product.value?.maxPurchase
          : _parentProduct.value?.maxPurchase,
    };
  });

  const specifications = computed(() => {
    return {
      width: _product.value?.width !== null ? _product.value?.width : _parentProduct.value?.width,
      height:
        _product.value?.height !== null ? _product.value?.height : _parentProduct.value?.height,
      length:
        _product.value?.length !== null ? _product.value?.length : _parentProduct.value?.length,
      weight:
        _product.value?.weight !== null ? _product.value?.weight : _parentProduct.value?.weight,
      unit: _product.value?.unit !== null ? _product.value?.unit : _parentProduct.value?.unit,
      packUnit:
        _product.value?.packUnit !== null
          ? _product.value?.packUnit
          : _parentProduct.value?.packUnit,
      packUnitPlural:
        _product.value?.packUnitPlural !== null
          ? _product.value?.packUnitPlural
          : _parentProduct.value?.packUnitPlural,
      referenceUnit:
        _product.value?.referenceUnit !== null
          ? _product.value?.referenceUnit
          : _parentProduct.value?.referenceUnit,
      purchaseUnit:
        _product.value?.purchaseUnit !== null
          ? _product.value?.purchaseUnit
          : _parentProduct.value?.purchaseUnit,
    };
  });

  return {
    getAdminProduct,
    prices,
    deliverability,
    specifications,
  };
};
