import dayjs from 'dayjs';
import { ShopwareSearchParams } from '@shopware-pwa/types';
import {
  CustomerInsightsAttendee,
  CustomerInsightsCustomerData,
  CustomerInsightsYearOrder,
  OrderCustomer,
} from '~/logic/interfaces';
import { CustomerService, OrderService } from '~/logic/api-client';

/**
 * Composable to manage customer insights
 *
 * @category Account
 * @param attendeeId Attendee ID
 */
export const useCustomerInsights = (attendeeId: Ref<string>) => {
  const { appointment } = useAppointment();
  const { adminApiInstance } = useAdminApiInstance();
  const _storeAttendeeInsights = ref<CustomerInsightsAttendee>();
  const _storeCurrentCustomerData = ref<CustomerInsightsCustomerData>();
  const _lastYearOrders = ref<CustomerInsightsYearOrder[]>([]);
  const orders = ref<OrderCustomer[]>([]);
  const _currentYearOrders = ref<CustomerInsightsYearOrder[]>([]);

  const lastYear = computed(() => new Date().getFullYear() - 1);
  const currentYear = computed(() => new Date().getFullYear());

  const loadCustomerData = async () => {
    if (!attendeeId.value) return;

    const customerDataCriteria: Partial<ShopwareSearchParams> = {
      associations: {
        productCollections: {},
        customer: {
          associations: {
            defaultBillingAddress: {
              associations: {
                country: {},
              },
            },
          },
        },
      },
      includes: {
        customer: ['email', 'defaultBillingAddress'],
      },
      filter: [
        {
          type: 'equals',
          field: 'id',
          value: attendeeId.value,
        },
      ],
    };

    try {
      _storeCurrentCustomerData.value = await CustomerService.getCustomerData(
        customerDataCriteria,
        adminApiInstance,
      );
    } catch (e) {
      // handleError(e as ClientApiError, {
      //   context: contextName,
      //   method: "loadCustomerData",
      // });
    }
  };

  const loadCustomerOrders = async (page = 1) => {
    const customerId = _storeCurrentCustomerData.value?.customerId;
    if (!customerId) return;
    try {
      const orderCustomerCriteria: Partial<ShopwareSearchParams> = {
        includes: {
          order: ['amountNet', 'amountTotal', 'orderNumber', 'lineItems', 'orderDate'],
          order_customer: ['order'],
        },
        associations: {
          order: {},
        },
        filter: [
          {
            type: 'multi',
            operator: 'and',
            queries: [
              {
                type: 'equals',
                field: 'order.stateMachineState.technicalName',
                value: 'completed',
              },
              {
                type: 'equals',
                field: 'customerId',
                value: customerId,
              },
            ],
          },
        ],
      };
      orders.value = await OrderService.getOrderCustomer(orderCustomerCriteria, adminApiInstance);
    } catch (e) {
      //
    }
  };

  const loadCustomerInsights = async () => {
    if (!appointment?.value?.id) return;
    try {
      const res = await CustomerService.getAttendeeInsightsData(
        appointment.value.id,
        adminApiInstance,
      );
      _storeAttendeeInsights.value = res.attendees[attendeeId.value];
    } catch (e) {
      // handleError(e as ClientApiError, {
      //   context: contextName,
      //   method: "loadAttendeeData",
      // });
    }
  };

  const loadCustomerOrdersStatistics = async () => {
    const customerId = _storeCurrentCustomerData.value?.customerId;
    if (!customerId) return;
    _lastYearOrders.value = [];
    _currentYearOrders.value = [];
    const DATE_FORMAT = 'YYYY-MM-DD';
    const pastYearStartDate = dayjs().subtract(1, 'year').startOf('year').format(DATE_FORMAT);
    const currentYearEndDate = dayjs().endOf('year').format(DATE_FORMAT);

    const orderCustomerCriteria: Partial<ShopwareSearchParams> = {
      includes: {
        order: ['amountNet', 'amountTotal', 'orderNumber', 'lineItems', 'orderDate'],
        order_customer: ['order'],
      },
      associations: {
        order: {
          associations: {
            lineItems: {},
          },
        },
      },
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'order.stateMachineState.technicalName',
              value: 'completed',
            },
            {
              type: 'equals',
              field: 'customerId',
              value: customerId,
            },
            {
              type: 'range',
              field: 'order.orderDate',
              parameters: {
                gte: pastYearStartDate,
                lte: currentYearEndDate,
              },
            },
          ],
        },
      ],
      // total_count_mode: 0,
    };

    const orderCustomerResponse = await OrderService.getOrderCustomer(
      orderCustomerCriteria,
      adminApiInstance,
    );

    // sort after last and current year
    orderCustomerResponse.forEach((item) => {
      const orderDateYear = new Date(item.order.orderDate).getFullYear();

      if (orderDateYear === lastYear.value) {
        _lastYearOrders.value.push(item.order);
      } else if (orderDateYear === currentYear.value) {
        _currentYearOrders.value.push(item.order);
      }
    });
  };

  // const refreshCustomerData = async (attendeeId: string) => {
  //   if (!_storeCurrentCustomerData.value || _storeCurrentCustomerData.value.id !== attendeeId)
  //     return;
  //   await loadCustomerData(_storeCurrentCustomerData.value.id, true);
  //   isLoading.value = false;
  // };

  return {
    loadCustomerOrders,
    loadCustomerData,
    loadCustomerInsights,
    loadCustomerOrdersStatistics,
    orders,
    // refreshCustomerData,
    customerData: computed(() => _storeCurrentCustomerData.value),
    insightsData: computed(() => _storeAttendeeInsights.value),
    customerLikesCount: computed(
      () =>
        _storeCurrentCustomerData.value?.productCollections.filter((item) => item.alias === 'liked')
          .length,
    ),
    customerDislikesCount: computed(
      () =>
        _storeCurrentCustomerData.value?.productCollections.filter(
          (item) => item.alias === 'disliked',
        ).length,
    ),
    lastYear,
    currentYear,
    lastYearOrdersItemCount: computed(() => {
      const lastFlattenedLineItems = _lastYearOrders.value.flatMap((order) => order.lineItems);
      return lastFlattenedLineItems.reduce((count, lineItem) => {
        const item = lineItem as any;
        return count + item.quantity;
      }, 0);
    }),
    currentYearOrdersItemCount: computed(() => {
      const currentFlattenedLineItems = _currentYearOrders.value.flatMap(
        (order) => order.lineItems,
      );
      return currentFlattenedLineItems.reduce((count, lineItem) => {
        const item = lineItem as any;
        return count + item.quantity;
      }, 0);
    }),
    lastYearOrdersTotalPrice: computed(() => {
      return _lastYearOrders.value.reduce(
        (accumulator: number, item) => item.amountTotal + accumulator,
        0,
      );
    }),
    currentYearOrdersTotalPrice: computed(() => {
      return _currentYearOrders.value.reduce(
        (accumulator: number, item) => item.amountTotal + accumulator,
        0,
      );
    }),
  };
};
