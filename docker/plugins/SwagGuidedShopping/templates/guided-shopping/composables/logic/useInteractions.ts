import { ClientApiError } from '@shopware-pwa/types';
import { AxiosRequestConfig } from 'axios';
import {
  DynamicInteractionName,
  DynamicLikeListPage,
  DynamicProductPage,
  DynamicProductListingPage,
} from './useDynamicPage';
import { InteractionService } from '~/logic/api-client';

/**
 * Composable to manage enum of interaction name
 *
 * @public
 * @category Interactions
 */
export enum InteractionName {
  QUICKVIEW_OPENED = 'quickview.opened',
  QUICKVIEW_CLOSED = 'quickview.closed',
  GUIDE_HOVERED = 'guide.hovered',
  PAGE_VIEWED = 'page.viewed',
  BROADCASTMODE_TOGGLED = 'broadcastMode.toggled',
  KEEP_ALIVE = 'keep.alive',
  PRODUCT_VIEWED = 'product.viewed',
  PRODUCT_LIKED = 'attendee.product.collection.liked',
  PRODUCT_UN_LIKED = 'attendee.product.collection.removed',
}

/**
 * Interface of interaction
 *
 * @public
 * @category Interactions
 */
interface Interaction<T, Y> {
  /**
   * Interaction name
   */
  name: T;

  /**
   * Interaction payload
   */
  payload?: Y;
}

type QuickViewOpenedInteraction = Interaction<
  InteractionName.QUICKVIEW_OPENED,
  { productId: string }
>;

type QuickViewClosedInteraction = Interaction<
  InteractionName.QUICKVIEW_CLOSED,
  { productId: string }
>;

type GuideHoveredInteraction = Interaction<
  InteractionName.GUIDE_HOVERED,
  { hoveredElementId: string }
>;

type PageViewedInteraction = Interaction<
  InteractionName.PAGE_VIEWED,
  {
    pageId: string;
    sectionId: string;
    slideAlias: number;
  }
>;

type BroadcastModeToggledInteraction = Interaction<
  InteractionName.BROADCASTMODE_TOGGLED,
  {
    active: boolean;
  }
>;

type ProductViewedInteraction = Interaction<InteractionName.PRODUCT_VIEWED, { productId: string }>;

type ProductLikedInteraction = Interaction<InteractionName.PRODUCT_LIKED, { productId: string }>;
type ProductUnlikedInteraction = Interaction<
  InteractionName.PRODUCT_UN_LIKED,
  { productId: string }
>;

type KeepAliveInteraction = Interaction<InteractionName.KEEP_ALIVE, {}>;
type DynamicProductPageOpenedInteraction = Interaction<
  DynamicInteractionName.DYNAMIC_PRODUCT_PAGE_OPENED,
  DynamicProductPage
>;

type DynamicPageOpenedInteraction = Interaction<
  DynamicInteractionName.DYNAMIC_PAGE_OPENED,
  DynamicLikeListPage
>;

type DynamicProductListingPageInteraction = Interaction<
  DynamicInteractionName.DYNAMIC_PRODUCT_LISTING_PAGE_OPENED,
  DynamicProductListingPage
>;

type DynamicPageClosedInteraction = Interaction<DynamicInteractionName.DYNAMIC_PAGE_CLOSED, {}>;

// {
//   name: 'guide.hovered';
//   payload: {
//     hoveredElementId: string;
//   };
// };

// type DynamicPageInteraction = {
//   payload: DynamicPageResponse;
// };

// type DynamicProductPageInteraction = {
//   payload: DynamicProductPage;
// };

// type DynamicProductListingPageInteraction = {
//   payload: {
//     page: number;
//   };
// };

// type LikeListProductLikedInteraction = ProductInteraction & {
//   name: 'attendee.product.collection.liked';
// };

// type LikeListProductDislikedInteraction = ProductInteraction & {
//   name: 'attendee.product.collection.disliked';
// };

// type LikeListProductRemovedInteraction = ProductInteraction & {
//   name: 'attendee.product.collection.removed';
// };

// type DynamicPageClosedInteraction = {
//   name: 'dynamicPage.closed';
// };

// type QuickViewClosed = ProductInteraction & {
//   name: 'quickview.closed';
// };

type Interactions =
  // | LikeListProductLikedInteraction
  // | LikeListProductDislikedInteraction
  // | LikeListProductRemovedInteraction
  // | DynamicProductListingPageLoadedMoreInteraction
  | DynamicPageClosedInteraction
  | ProductViewedInteraction
  | ProductLikedInteraction
  | ProductUnlikedInteraction
  | DynamicProductListingPageInteraction
  | DynamicPageOpenedInteraction
  | DynamicProductPageOpenedInteraction
  | QuickViewOpenedInteraction
  | QuickViewClosedInteraction
  | GuideHoveredInteraction
  | PageViewedInteraction
  | BroadcastModeToggledInteraction
  | KeepAliveInteraction;

/**
 * Composable to manage interactions
 *
 * @public
 * @category Interactions
 */
export const useInteractions = () => {
  const { apiInstance } = useShopwareContext();
  const { isSelfService } = useAppointment();
  const { isClient } = useUser();

  /**
   * usage:
   *    import {useInteractions} from './useInteractions';
   *    ...
   *    const {publishInteraction} = useInteractions();
   *    publishInteraction({
   *      name: 'my.interaction',
   *      payload: {
   *        some: 'data',
   *      }
   *    });
   *
   * @param interaction
   */
  const publishInteraction = async (interaction: Interactions, config?: AxiosRequestConfig) => {
    try {
      return await InteractionService.send(interaction, apiInstance, config);
    } catch (e) {
      // TODO: should handle error
      e as ClientApiError;
    }
  };

  /**
   * only publishes interaction if user is a client
   *
   * usage:
   *    import {useInteractions} from './useInteractions';
   *    ...
   *    const {publishClientInteraction} = useInteractions();
   *    publishClientInteraction({
   *      name: 'my.interaction',
   *      payload: {
   *        some: 'data',
   *      }
   *    });
   *
   * @param interaction
   */
  const publishClientInteraction = (interaction: Interactions) => {
    if (isSelfService.value) return;
    if (!isClient.value) return;
    return publishInteraction(interaction);
  };

  return {
    publishInteraction,
    publishClientInteraction,
  };
};
