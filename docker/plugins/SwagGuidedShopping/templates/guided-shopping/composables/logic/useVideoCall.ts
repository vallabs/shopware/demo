// import { debounce } from '@shopware-pwa/helpers'
// import { getApplicationContext, useSharedState } from '@shopware-pwa/composables';
// import { computed, onMounted, watch } from "@vue/composition-api"
// import { useAppointment } from "./useAppointment";
// import { useErrorHandler } from "./useErrorHandler";
// import { useState } from './useState';
import DailyIframe, {
  DailyCall,
  DailyEventObjectActiveSpeakerChange,
  DailyEventObjectParticipant,
  DailyEventObjectParticipantLeft,
  DailyEventObjectParticipants,
  DailyEventObjectTrack,
  DailyParticipant,
  DailyEventObjectCameraError,
} from '@daily-co/daily-js';
import { ToastConfig } from '~/composables/layout/useToasts';
import { StreamParticipant } from '~/logic/interfaces';

// import { VideoAudioSettingsType, VideoParticipant, VideoParticipants } from "../interfaces";
// import { AppointmentService } from '../api-client';
// import { useInteractions } from "./useInteractions";
// import { useVideoSetup } from "./useVideoSetup";
// import { DeviceError, useMediaDevices } from "./useMediaDevices";
// import { useMercureObserver } from './useMercureObserver';
// import { useMercure } from "./useMercure";
// import { useWatchers } from './useWatchers';

// const COMPOSABLE_NAME = 'useVideoCall';

// const MICROPHONE_STATE_STORAGE_KEY = 'sw-gs-call-microphone-state';
// const CAMERA_STATE_STORAGE_KEY = 'sw-gs-call-camera-state';
// const SPEAKER_DEVICE_SET_DELAY = 300;

// declare global {
//   interface Window {
//     dailyCallInstanceCreated: boolean,
//     dailyCallInstance: DailyCall,
//   }
// }

declare global {
  interface Window {
    dailyCallInstance: DailyCall;
  }
}

/**
 * Composable to video call
 *
 * @category Presentation
 */
const _useVideoCall = () => {
  const { stateForAll, stateForGuides, stateForMe, stateForClients, updateState } =
    usePresentationState();
  const { isSelfService, isAppointmentRunning, isAppointmentEnded } = useAppointment();
  const { callEnabled, isVideoCall, isVoiceCall } = useMediaDevices();
  const { cameraDeviceId, microphoneDeviceId, speakerDeviceId, joinedWithAudioOnly } =
    useVideoSetup();
  const { publishInteraction } = useInteractions();
  const { isGuide, isClient, attendee, updateAttendeeData } = useUser();
  const {
    self,
    activeSpeaker,
    removeStreamParticipant,
    upsertStreamParticipants,
    resetStreamParticipants,
    setActiveSpeaker,
    removeActiveSpeaker,
  } = useParticipants();
  const { upsertToast } = useToasts();
  const currentToastConfig = ref<ToastConfig>();

  const isBroadCastEnabled = computed(() => stateForAll.value?.broadcastMode);
  const _hasJoinedCall = ref<boolean>(false);
  const _microphoneState = useSessionStorage<boolean | null>('microphoneState', null, {
    serializer: {
      read: (v: string) => {
        try {
          return JSON.parse(v);
        } catch (e) {
          return null;
        }
      },
      write: (v: boolean) => JSON.stringify(v),
    },
  });
  const _cameraState = useSessionStorage<boolean | null>('cameraState', null, {
    serializer: {
      read: (v: string) => {
        try {
          return JSON.parse(v);
        } catch (e) {
          return null;
        }
      },
      write: (v: boolean) => JSON.stringify(v),
    },
  });
  const videoRoomUrl = computed(() => stateForAll.value?.videoRoomUrl);
  const videoToken = computed(() => {
    if (isGuide.value) {
      return stateForGuides.value?.videoGuideToken;
    }
    if (isClient.value) {
      return stateForClients.value?.videoClientToken;
    }
  });

  const watchAppointmentEnded = watch(isAppointmentEnded, async (isEnded) => {
    if (isEnded) {
      if (window.dailyCallInstance) {
        await window.dailyCallInstance.leave();
        await window.dailyCallInstance.destroy();
      }

      resetStreamParticipants();
      watchAppointmentEnded();
    }
  });
  // // const { cameraDeviceId } = useVideoSetup();

  // const videoAudioSettings = computed(() => stateForAll.value?.videoAudioSettings!);
  // const callEnabled = computed(
  //   () =>
  //     [VideoAudioSettingsType.VIDEO_CALL, VideoAudioSettingsType.VOICE_CALL].includes(
  //       videoAudioSettings.value,
  //     ) && !isSelfService.value,
  // );
  // const isVoiceCall = computed(
  //   () => videoAudioSettings.value === VideoAudioSettingsType.VOICE_CALL,
  // );
  // const isVideoCall = computed(
  //   () => videoAudioSettings.value === VideoAudioSettingsType.VIDEO_CALL,
  // );

  const setup = () => {
    initCall();
    // startReinitializeWatchers();
  };

  // const contextName = COMPOSABLE_NAME;
  // const { handleError } = useErrorHandler();
  // const { activeSpeaker } = useMercureObserver();
  // const { mercurePublish } = useMercure();
  // const { hasSavedSettings, mediaDeviceChanged, getMediaDevices, handleDeviceError, microphoneDevice, cameraDevice, speakerDevice, setDevicesFromStorage, isAudioOnly, microphoneCount} = useMediaDevices();
  // const { stopDeviceChanges } = useVideoSetup();
  // const { apiInstance } = getApplicationContext({ contextName });
  // const _reinitializeWatchersStarted = ref<boolean>(false);
  // const _connectedWatchersStarted = ref<boolean>(false);
  // const _deviceSettingsWatchersStarted = ref<boolean>(false);
  // const _activeSpeakerId = ref<string>();
  // const _speakerDeviceMuted = ref<boolean>(true);
  // const speakerAudios = ref<any>(null);
  // const { addWatcher } = useWatchers();

  // watch([speakerAudios], () => {
  //   unlockSpeakerAudio();
  // });

  // function unlockSpeakerAudio() {
  //   if (!speakerAudios.value) return;
  //   speakerAudios.value.forEach(async (audio) => {
  //     try {
  //       await audio.play();
  //     } catch (e) {
  //       handleDeviceError('SPEAKER_NOT_ALLOWED');
  //     }
  //   });
  // }

  // const room = {
  //   running: appointmentIsRunning,
  //   url: computed(() => stateForAll.value?.videoRoomUrl),
  //   token: computed(() => {
  //     if (isGuide.value) return stateForGuides.value?.videoGuideToken;
  //     if (isClient.value) return stateForClients.value?.videoClientToken;
  //   }),
  // };

  // let members = {};
  // const participants = ref<VideoParticipants>({
  //   self: null,
  //   members: members,
  //   memberCount: 0,
  //   guide: null
  // });

  // onMounted(() => {
  //   _microphoneState.value = JSON.parse(sessionStorage.getItem(MICROPHONE_STATE_STORAGE_KEY));
  //   _cameraState.value = JSON.parse(sessionStorage.getItem(CAMERA_STATE_STORAGE_KEY));
  //   if (isAudioOnly.value) _cameraState.value = false;
  //   // first request to get plain list without labels
  //   getMediaDevices(true, true);
  // });

  // const setup = () => {
  //   initCall();
  //   startReinitializeWatchers();
  // };

  /**
   * creates all necessary call objects and watchers
   */
  const initCall = async () => {
    // const instance: DailyCall = window.dailyCallInstance;

    // if (!hasSavedSettings.value) {
    //   _hasJoinedCall.value = false;
    //   return;
    // } // will be retried by watcher on retry
    // if (!appointmentIsRunning.value) return; // will be retried by watcher on retry

    // if (instance) return;
    // if (_hasJoinedCall.value) return;

    // stopDeviceChanges();

    // const devices = await getMediaDevices(true, true); // this list will have empty labels too if no setup window appears after refresh

    // set shared refs from storage for the device ids
    // setDevicesFromStorage(devices);
    if (!_hasJoinedCall.value) {
      await createCallInstance();
      await joinCall();
    } else {
      updateCall();
    }
    // await getMediaDevices(true); // if daily is the first one to ask for permissions we have to update the list again
    // startDeviceSettingsWatchers();
  };

  // /**
  //  * register watchers
  //  */
  // function startReinitializeWatchers() {
  //   if (_reinitializeWatchersStarted.value) return;
  //   _reinitializeWatchersStarted.value = true;

  //   addWatcher(watch(hasSavedSettings, (savedSettings) => {
  //     if (!savedSettings) return;
  //     initCall();
  //   }));
  //   addWatcher(watch([appointmentIsRunning, room.token], ([running, token]) => {
  //     if (!running || !token) return;
  //     initCall();
  //   }));
  // }

  // /**
  //  * register device settings watchers
  //  */
  // function startDeviceSettingsWatchers() {
  //   if (_deviceSettingsWatchersStarted.value) return;
  //   _deviceSettingsWatchersStarted.value = true;

  //   addWatcher(watch(mediaDeviceChanged, (config) => {
  //     if (!config) return;
  //     const storageKey = config.storageKey;
  //     const deviceId = config.deviceId;

  //     if (!deviceId) return;
  //     updateCallDevice(storageKey, deviceId);
  //   }));
  // }

  // /**
  //  * watchers for when the call is instantiated
  //  */
  // function startConnectedWatchers() {
  //   if (_connectedWatchersStarted.value) return;
  //   _connectedWatchersStarted.value = true;

  //   // watch for broadcast mode activation on client side
  //   if (isClient.value) {
  //     addWatcher(watch(isBroadCastEnabled, (state) => {
  //       if (state === false) return;

  //       toggleMicrophone(false);
  //       toggleCamera(false);
  //     }));
  //   }

  //   // update guide attendee name for daily
  //   addWatcher(watch(currentGuide, () => {
  //     const instance: DailyCall = window.dailyCallInstance;
  //     if (!currentGuide.value || !instance) return;
  //     instance.setUserName(currentGuide.value.attendeeName);
  //   }));

  //   // update client attendee name for daily
  //   addWatcher(watch(attendeeName, () => {
  //     const instance: DailyCall = window.dailyCallInstance;
  //     if (!attendeeName.value && !instance) return;
  //     instance.setUserName(attendeeName.value);
  //   }));

  //   addWatcher(watch(activeSpeaker, setActiveSpeaker));
  // }

  // /**
  //  * returns options with which the instance is created
  //  */
  // const getInstanceOptions = () => {
  //   const options: any = {};

  //   // force camera state to off when audioOnly is active
  //   if (isAudioOnly.value) _cameraState.value = false;

  //   if (!_microphoneState.value) {
  //     options.audioSource = false;
  //   } else if (microphoneDevice.value) {
  //     options.audioSource = microphoneDevice.value;
  //   }

  //   if (!_cameraState.value) {
  //     options.videoSource = false;
  //   } else if (microphoneDevice.value) {
  //     options.videoSource = cameraDevice.value;
  //   }

  //   return options;
  // };

  const updateCall = () => {
    const instance: DailyCall = window.dailyCallInstance;
    if (!instance) return;
    instance.setInputDevicesAsync({
      videoDeviceId: cameraDeviceId.value,
      audioDeviceId: microphoneDeviceId.value,
    });
  };

  /**
   * initializes the video call
   */
  const createCallInstance = async () => {
    if (window.dailyCallInstance) return;
    // if (!hasSavedSettings.value) return false;
    if (isSelfService.value) return;

    if (!callEnabled.value) return;

    if (isVoiceCall.value) {
      window.dailyCallInstance = await DailyIframe.createCallObject({
        startVideoOff: true,
        videoSource: false,
      });
    } else if (isVideoCall.value) {
      window.dailyCallInstance = await DailyIframe.createCallObject();
    }
    // try {
    //   // const options = getInstanceOptions();
    //   if (callEnabled.value) {
    //     if (isVideoCall.value) {
    //       dailyCallInstance.value = await DailyIframe.createCallObject({
    //         startVideoOff: true,
    //         videoSource: false,
    //       });
    //     } else {
    //       dailyCallInstance.value = await DailyIframe.createCallObject();
    //     }
    //   }
    // } catch (e) {
    //   dailyCallInstance.value = await DailyIframe.createCallObject({
    //     audioSource: undefined,
    //     videoSource: undefined
    //   });
    // }

    // startConnectedWatchers();
  };

  /**
   * register daily events
   */
  const registerCallEvents = () => {
    // on<T extends DailyEvent>(
    //   event: T,
    //   handler: (event?: DailyEventObject<T>) => void
    // ): DailyCall;
    window.dailyCallInstance.on('joined-meeting', onLocalParticipantJoined);
    window.dailyCallInstance.on('participant-joined', onClientParticipantJoined);
    window.dailyCallInstance.on('participant-left', onParticipantLeft);
    // window.dailyCallInstance.on('participant-updated', (event) => {
    //   console.log(3333, event);
    //   updateParticipant(event);
    // });
    window.dailyCallInstance.on('track-stopped', onTrackChanged);
    window.dailyCallInstance.on('track-started', onTrackChanged);

    window.dailyCallInstance.on('active-speaker-change', onActiveSpeakerChanged);
    window.dailyCallInstance.on('camera-error', onDeviceError);
    window.dailyCallInstance.on('left-meeting', () => {
      _hasJoinedCall.value = false;
    });
  };

  /**
   * @param dailyError
   */
  const onDeviceError = (dailyError?: DailyEventObjectCameraError) => {
    console.log('device-error', dailyError);
    // if (isAudioOnly.value) return;
    if (!dailyError?.error) {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        content: 'streamErrors.noDeviceFound',
        variant: 'error',
      });
      return;
    }
    const { error } = dailyError;
    if (error.type === 'unknown') {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        content: 'streamErrors.unknown',
        variant: 'error',
      });
    } else if (error.type === 'permissions') {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        content: 'streamErrors.permissions',
        variant: 'error',
      });
    } else if (error.type === 'cam-in-use') {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        content: 'streamErrors.cameraInUse',
        variant: 'error',
      });
    } else if (error.type === 'mic-in-use') {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        content: 'streamErrors.microphoneInUse',
        variant: 'error',
      });
    } else if (error.type === 'cam-mic-in-use') {
      currentToastConfig.value = upsertToast({
        ...currentToastConfig.value,
        content: 'streamErrors.cameraMicrophoneInUse',
        variant: 'error',
      });
    }
  };

  /**
   * checks if call is join able
   */
  const canJoin = () => {
    return (
      window.dailyCallInstance &&
      isAppointmentRunning.value &&
      videoRoomUrl.value &&
      videoToken.value
    );
  };

  /**
   * join the call
   */
  const joinCall = async () => {
    if (isSelfService.value) return;
    // if (_hasJoinedCall.value) return;

    try {
      if (!canJoin()) return;

      const instance: DailyCall = window.dailyCallInstance;
      // const instance: DailyCall = window.dailyCallInstance;
      registerCallEvents();
      const options: any = {
        url: videoRoomUrl.value,
        token: videoToken.value,
        activeSpeakerMode: true,
      };
      if (joinedWithAudioOnly.value) {
        // on audio only mode disable devices with "false" "for ever" (see daily docs)
        options.audioSource = false;
        options.videoSource = false;
      } else if (isVoiceCall.value) {
        // otherwise do not set the the device options to let it open for adding it later or using default device by daily
        options.audioSource = microphoneDeviceId.value;
        // camera should always off
        options.startVideoOff = true;
        toggleCamera(false);

        // default microphone state is on
        let initialMicrophoneState = true;

        // assign old state if available
        if (_microphoneState.value !== null) {
          initialMicrophoneState = _microphoneState.value;
        }

        // microphone should be off if broadcast is enabled
        if (isBroadCastEnabled.value && isClient.value) {
          initialMicrophoneState = false;
        }
        options.startAudioOff = !initialMicrophoneState;
        toggleCamera(initialMicrophoneState);
      } else if (isVideoCall.value) {
        options.audioSource = microphoneDeviceId.value;
        options.videoSource = cameraDeviceId.value;

        // default microphone state is off, video on
        let initialMicrophoneState = false;
        let initialCameraState = true;

        // assign old state if available
        if (_microphoneState.value !== null) {
          initialMicrophoneState = _microphoneState.value;
        }
        if (_cameraState.value !== null) {
          initialCameraState = _cameraState.value;
        }

        // microphone & video should be off if broadcast is enabled
        if (isBroadCastEnabled.value && isClient.value) {
          initialMicrophoneState = false;
          initialCameraState = false;
        }

        options.startAudioOff = !initialMicrophoneState;
        options.startVideoOff = !initialCameraState;
        toggleMicrophone(initialMicrophoneState);
        toggleCamera(initialCameraState);
      }

      instance.setUserName(attendee.value.name ?? '');
      await instance.setOutputDeviceAsync({ outputDeviceId: speakerDeviceId.value ?? 'default' });

      // join video call room
      await instance.join(options);

      // do this after join to mute video and cam, because on join we only can inject deviceId OR false, if we inject false the user can never be unmuted
      // if (isBroadCastEnabled.value && !isGuide.value) {
      //   instance.setLocalVideo(false);
      //   instance.setLocalAudio(false);
      // }else {
      //   toggleMicrophone(_microphoneState.value);
      //   toggleCamera(_cameraState.value);
      // }

      // save video call id in attendee

      // window.addEventListener("beforeunload", instance.leave);

      // _hasJoinedCall.value = true;
    } catch (e) {
      // handleError(e, {
      //   context: contextName,
      //   method: 'joinCall',
      // });
    }
  };

  // /**
  //  * updates the currently used device for the call instance
  //  *
  //  * @param storageKey
  //  * @param deviceId
  //  */
  // function updateCallDevice(storageKey, deviceId) {
  //   const instance: DailyCall = window.dailyCallInstance;
  //   if (!instance) return;

  //   switch (storageKey) {
  //     case 'microphone-device':
  //       instance.setInputDevicesAsync({ 'audioDeviceId': deviceId });
  //       break;
  //     case 'camera-device':
  //       instance.setInputDevicesAsync({ 'videoDeviceId': deviceId });
  //       break;
  //     case 'speaker-device':
  //       setOutputDevice(deviceId);
  //       break;
  //     default:
  //       break
  //   }
  // }

  /**
   * toggles audio state
   *
   * @param forcedState
   */
  const toggleMicrophone = (state?: boolean) => {
    const instance: DailyCall = window.dailyCallInstance;

    _microphoneState.value = state !== undefined ? state : !_microphoneState.value;
    // sessionStorage.setItem(MICROPHONE_STATE_STORAGE_KEY, JSON.stringify(_microphoneState.value));

    if (!self.value || !instance) return;

    // const self = participants.value.self;
    instance.setLocalAudio(_microphoneState.value);
    // self.data.audio = _microphoneState.value;
  };

  /**
   * toggles video state
   *
   * @param forcedState
   */
  const toggleCamera = (state?: boolean) => {
    const instance: DailyCall = window.dailyCallInstance;

    _cameraState.value = state !== undefined ? state : !_cameraState.value;
    // sessionStorage.setItem(CAMERA_STATE_STORAGE_KEY, JSON.stringify(_cameraState.value));

    if (!self.value || !instance) return;
    // const self = participants.value.self;
    instance.setLocalVideo(_cameraState.value);
    // self.data.video = _cameraState.value;
  };

  /**
   * toggles the broadcast mode via interaction
   */
  const toggleBroadcastMode = (status: boolean) => {
    publishInteraction({
      name: InteractionName.BROADCASTMODE_TOGGLED,
      payload: {
        active: status,
      },
    });
  };

  const onLocalParticipantJoined = (event?: DailyEventObjectParticipants) => {
    if (!event?.participants?.local) return;

    const streamParticipant = getParticipantData(event.participants.local);

    if (!streamParticipant) return;

    upsertStreamParticipants(streamParticipant);

    updateAttendeeData({
      videoUserId: streamParticipant.data.session_id,
    });

    updateState(MercureEvent.STATE_FOR_ME, {
      ...stateForMe.value,
      videoUserId: streamParticipant.data.session_id,
    });

    _hasJoinedCall.value = true;
  };

  const onClientParticipantJoined = (event?: DailyEventObjectParticipant) => {
    const streamParticipant = getParticipantData(event?.participant);
    upsertStreamParticipants(streamParticipant);
  };

  const onTrackChanged = (event?: DailyEventObjectTrack) => {
    const streamParticipant = getParticipantData(event?.participant);
    upsertStreamParticipants(getParticipantData(event?.participant));

    if (streamParticipant?.data.local) {
      _microphoneState.value = streamParticipant.data.tracks.audio.state !== 'off';
    }

    // update active speaker when no active speaker is set or the active speaker change their media
    if (
      (!activeSpeaker.value &&
        streamParticipant &&
        !streamParticipant?.data.local &&
        streamParticipant.streams.video &&
        streamParticipant.streams.audio) ||
      (activeSpeaker.value &&
        streamParticipant &&
        streamParticipant.data.user_id === activeSpeaker.value.data.user_id)
    ) {
      setActiveSpeaker(streamParticipant?.data.user_id);
    }
  };

  /**
   * executed when a participant leaves the call
   *
   * @param event
   */
  const onParticipantLeft = (event?: DailyEventObjectParticipantLeft) => {
    if (event?.participant.session_id) {
      removeStreamParticipant(event.participant.session_id);

      // if the user who left from the mtg is the active speaker, so remove the active speaker
      if (
        activeSpeaker.value &&
        event.participant.session_id === activeSpeaker.value.data.user_id
      ) {
        removeActiveSpeaker();
      }
    }
  };

  /**
   * executed when the active speaker changes
   *
   * @param event
   */
  const onActiveSpeakerChanged = (event?: DailyEventObjectActiveSpeakerChange) => {
    if (
      !event?.activeSpeaker?.peerId ||
      event?.activeSpeaker?.peerId === activeSpeaker.value?.data.session_id ||
      event?.activeSpeaker?.peerId === self.value?.videoUserId
    )
      return;
    const activeSpeakerId = event?.activeSpeaker.peerId;
    setActiveSpeaker(activeSpeakerId);
    // DailyEventObjectActiveSpeakerChange
    // mercurePublish.all('activeSpeakerUpdated', activeSpeakerId);
  };

  // /**
  //  * checks if the guide has left the meeting and deletes him
  //  * @param participantId
  //  */
  // function checkIfGuideLeft(participantId) {
  //   if (participants.value.guide.data.session_id !== participantId) return;

  //   participants.value.guide = null;
  // }

  /**
   * creates a participant object
   * @param event
   */
  const getParticipantData = (
    participant?: DailyParticipant | null,
  ): StreamParticipant | undefined => {
    if (!participant) return;
    return {
      data: participant,
      streams: {
        video:
          participant.tracks.video.state &&
          participant.tracks.video.state !== 'off' &&
          participant.tracks.video.persistentTrack
            ? new MediaStream([participant.tracks.video.persistentTrack])
            : null,
        audio:
          participant.tracks.audio.state &&
          participant.tracks.audio.state !== 'off' &&
          participant.tracks.audio.persistentTrack
            ? new MediaStream([participant.tracks.audio.persistentTrack])
            : null,
      },
    };
  };

  // /**
  //  * returns if the participant is local
  //  *
  //  * @param event
  //  */
  // function checkIfLocalParticipant(event): boolean {
  //   const participant = getParticipantFromEvent(event);
  //   if (participant) return participant.local;
  //   return false;
  // }

  // /**
  //  * helper method to get the participant from daily events
  //  *
  //  * @param event
  //  */
  // function getParticipantFromEvent(event): DailyParticipant {
  //   if (event.participant) return event.participant;
  //   if (event.participants && event.participants.local) return event.participants.local;
  //   return null;
  // }

  // const ownParticipant = computed(() => participants.value.self);
  // const ownVideoStream = computed(() => ownParticipant.value?.streams?.video);
  // const memberParticipantsById = computed(() => participants.value.members);
  // const memberParticipants = computed(() => Object.values(memberParticipantsById.value));
  // const memberCount = computed(() => memberParticipants.value.length);
  // const hasMembers = computed(() => memberCount.value > 0);
  // const callEnabled = computed(() => [VideoAudioSettingsType.BOTH, VideoAudioSettingsType.AUDIO_ONLY].includes(stateForAll?.value?.videoAudioSettings) && !isSelfService.value);
  // const videoChatEnabled = computed(() => stateForAll?.value?.videoAudioSettings === VideoAudioSettingsType.BOTH && !isSelfService.value);

  // /**
  //  * helper method to publish the active speaker on join and speaker change
  //  * @param activeSpeakerId
  //  */
  // function setActiveSpeaker(activeSpeakerId) {
  //   const self = participants.value.self;
  //   if (!activeSpeakerId || activeSpeakerId === self.data.session_id) return;
  //   _activeSpeakerId.value = activeSpeakerId;
  // }

  // const _evaluatedActiveSpeaker: { value: any } = computed(() => {
  //   if (memberParticipants.value.length == 1) {
  //     return memberParticipants.value[0];
  //   }
  //   return memberParticipantsById.value[_activeSpeakerId.value];
  // });

  // const activeSpeakerStream = computed(() => _evaluatedActiveSpeaker.value?.streams?.video);

  return {
    // setup,
    // ownParticipant,
    // ownVideoStream,
    // memberParticipantsById,
    // memberParticipants,
    // memberCount,
    // hasMembers,
    // activeSpeaker: computed(() => _evaluatedActiveSpeaker.value),
    // activeSpeakerStream,
    toggleMicrophone,
    toggleCamera,
    microphoneState: computed(() => _microphoneState.value),
    cameraState: computed(() => _cameraState.value),
    hasJoinedCall: computed(() => _hasJoinedCall.value),
    // speakerDeviceMuted: computed(() => _speakerDeviceMuted.value),
    toggleBroadcastMode,
    isBroadCastEnabled,
    // speakerAudios,
    // unlockSpeakerAudio,
    // updateCallDevice,
    setup,
    // videoChatEnabled
  };
};

/**
 * Composable to video call
 *
 * @public
 * @category Presentation
 */
export const useVideoCall = createSharedComposable(_useVideoCall);
