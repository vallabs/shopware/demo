import { AppointmentService } from '~/logic/api-client';
import { StreamParticipant, ParticipantState } from '~/logic/interfaces';

/**
 * Composable to manage participants
 *
 * @category Account
 */
const _useParticipants = () => {
  const { stateForGuides, stateForClients, stateForMe } = usePresentationState();
  const { isGuide, isClient } = useUser();
  const { adminApiInstance } = useAdminApiInstance();

  const streamParticipants = ref<Record<string, StreamParticipant>>({});
  const contextTokenParticipants = ref<Record<string, string | null>>({});
  const controlledClient = ref<ParticipantState>();

  const guides = computed<Record<string, ParticipantState>>(() => {
    if (isGuide.value) return stateForGuides.value?.guides || {};
    else return stateForClients.value?.guides || {};
  });

  const clients = computed<Record<string, ParticipantState>>(() => {
    if (isGuide.value) return stateForGuides.value?.clients || {};
    else return stateForClients.value?.clients || {};
  });

  const setControlledClient = (clientId?: string) => {
    controlledClient.value = clientId
      ? clients.value[clientId] || inactiveClients.value[clientId]
      : undefined;
  };

  const permissionGranted = computed(() => {
    return (
      isClient.value ||
      (isGuide.value &&
        controlledClient.value &&
        (stateForGuides.value?.clients[controlledClient.value.attendeeId]
          ?.guideCartPermissionsGranted ||
          stateForGuides.value?.inactiveClients[controlledClient.value.attendeeId]
            ?.guideCartPermissionsGranted))
    );
  });

  const self = computed(() => stateForMe.value);

  const inactiveClients = computed<Record<string, ParticipantState>>(() => {
    if (!stateForGuides.value?.inactiveClients) return {};
    const temp = Object.values(stateForGuides.value?.inactiveClients);
    temp.forEach((key: ParticipantState) => {
      stateForGuides.value!.inactiveClients[key.attendeeId].inactive = true;
    });

    return stateForGuides.value?.inactiveClients;
  });

  const activeParticipants = computed<ParticipantState[]>(() => {
    const result: ParticipantState[] = [];

    [Object.values(guides.value), Object.values(clients.value)].forEach((list) => {
      list.forEach((participant) => {
        const streamParticipant = streamParticipants.value[participant.videoUserId];

        if (streamParticipant) {
          if (streamParticipant.data.owner) {
            result.unshift(participant);
          } else {
            result.push(participant);
          }
        }
      });
    });

    return result;
  });

  const notJoinedParticipants = computed<ParticipantState[]>(() => {
    const result: ParticipantState[] = [];

    for (const participant of Object.values(inactiveClients.value)) {
      if (!participant.hasJoined) {
        result.push(participant);
      }
    }
    return result;
  });

  const inactiveParticipants = computed<ParticipantState[]>(() => {
    const result: ParticipantState[] = [];

    for (const participant of Object.values(inactiveClients.value)) {
      if (participant.hasJoined) {
        result.push(participant);
      }
    }

    for (const participant of Object.values(clients.value)) {
      if (!streamParticipants.value[participant.videoUserId]) {
        result.push(participant);
      }
    }

    return result;
  });

  const activeSpeaker = ref<StreamParticipant>();

  const checkParticipantIsClient = (participant: ParticipantState) => {
    return (
      !!clients.value[participant.attendeeId] || !!inactiveClients.value[participant.attendeeId]
    );
  };

  const removeStreamParticipant = (id: string) => {
    delete streamParticipants.value[id];
  };

  const setActiveSpeaker = (participantUserId: string) => {
    activeSpeaker.value = streamParticipants.value[participantUserId];
  };

  const removeActiveSpeaker = () => {
    activeSpeaker.value = undefined;
  };

  const upsertStreamParticipants = (participant?: StreamParticipant) => {
    if (participant) {
      streamParticipants.value[participant.data.user_id] = participant;
    }
  };

  const getContextTokenForCurrentClient = async (forceRefresh?: boolean) => {
    const attendeeId = controlledClient.value?.attendeeId;
    if (!attendeeId || !isGuide.value) return;
    if (!forceRefresh && contextTokenParticipants.value?.[attendeeId]) {
      return contextTokenParticipants.value[attendeeId];
    }
    try {
      const newToken = await AppointmentService.getAttendeeContextToken(
        attendeeId,
        adminApiInstance,
      );
      contextTokenParticipants.value[attendeeId] = newToken;
      return newToken;
    } catch (e) {
      // reset current client for guide if error occured
      contextTokenParticipants.value[attendeeId] = null;
      throw e;
    }
  };

  const resetStreamParticipants = () => {
    streamParticipants.value = {};
  };

  return {
    self,
    clients,
    guides,
    permissionGranted,
    checkParticipantIsClient,
    inactiveClients,
    activeSpeaker: computed(() => activeSpeaker.value),
    streamParticipants: computed(() => streamParticipants.value),
    activeParticipants,
    inactiveParticipants,
    notJoinedParticipants,
    removeStreamParticipant,
    setActiveSpeaker,
    removeActiveSpeaker,
    getContextTokenForCurrentClient,
    upsertStreamParticipants,
    controlledClient: computed(() => controlledClient.value),
    setControlledClient,
    resetStreamParticipants,
  };
};

/**
 * Composable to manage participants
 *
 * @public
 * @category Account
 */
export const useParticipants = createSharedComposable(_useParticipants);
