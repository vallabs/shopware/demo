import { getMainImageUrl } from '@shopware-pwa/helpers-next';
import type {
  LineItem,
  LineItemType,
  PropertyGroupOptionCart,
  CartProductItem,
} from '@shopware-pwa/types';

/**
 * Composable to manage cart item
 * @public
 * @category Cart & Checkout
 */
export type UseCartItemReturn = {
  /**
   * Calculated price {number} for the current item
   */
  itemRegularPrice: ComputedRef<number | undefined>;

  /**
   * Calculated price {number} for the current item if list price is set
   */
  itemSpecialPrice: ComputedRef<number | undefined>;

  /**
   * Total price for the current item of given quantity in the cart
   */
  itemTotalPrice: ComputedRef<number | undefined>;

  /**
   * Thumbnail url for the current item's entity
   */
  itemImageThumbnailUrl: ComputedRef<string>;

  /**
   * Options (of variation) for the current item
   */
  itemOptions: ComputedRef<PropertyGroupOptionCart[]>;

  /**
   * Type of the current item: "product" or "promotion"
   */
  itemType: ComputedRef<LineItemType | undefined>;

  /**
   * Determines if the current item is a product
   */
  isProduct: ComputedRef<boolean>;

  /**
   * Determines if the current item is a promotion
   */
  isPromotion: ComputedRef<boolean>;

  /**
   * Stock information for the current item
   */
  itemStock: ComputedRef<number | undefined>;

  /**
   * Quantity of the current item in the cart
   */
  itemQuantity: ComputedRef<number | undefined>;

  /**
   * Changes the current item quantity in the cart
   * @function
   * @param quantity The new number of cart item in the cart
   */
  changeItemQuantity(quantity: number): Promise<void>;

  /**
   * Removes the current item from the cart
   * @function
   */
  removeItem(): Promise<void>;
};

/**
 * Composable to manage specific cart item
 * @public
 * @category Cart & Checkout
 * @function
 * @param cartItem Cart item
 */
export const useCartItem = (cartItem: Ref<LineItem>): UseCartItemReturn => {
  if (!cartItem) {
    throw new Error('[useCartItem] mandatory cartItem argument is missing.');
  }

  const { changeProductQuantity, removeItem: removeItemInCart, setCartStatistics } = useCart();

  const itemQuantity = computed(() => cartItem.value.quantity);
  const itemImageThumbnailUrl = computed(() => getMainImageUrl(cartItem.value));

  const itemRegularPrice = computed(
    () => cartItem.value?.price?.listPrice?.price || cartItem.value?.price?.unitPrice,
  );

  const itemSpecialPrice = computed(
    () => cartItem.value?.price?.listPrice?.price && cartItem.value?.price?.unitPrice,
  );

  const itemTotalPrice = computed(() => cartItem.value.price?.totalPrice);

  const itemOptions = computed(
    () =>
      (cartItem.value.type === 'product' && (cartItem.value.payload as CartProductItem)?.options) ||
      [],
  );

  const itemStock = computed(() => cartItem.value.deliveryInformation?.stock);

  const itemType = computed(() => cartItem.value.type);

  const isProduct = computed(() => cartItem.value.type === 'product');

  const isPromotion = computed(() => cartItem.value.type === 'promotion');

  const removeItem = async () => {
    await removeItemInCart(cartItem.value);
    setCartStatistics(cartItem.value.id, -itemQuantity.value);
  };

  const changeItemQuantity = async (quantity: number): Promise<void> => {
    const changeAmount = quantity - itemQuantity.value;
    if (!changeAmount) return;
    await changeProductQuantity({
      id: cartItem.value.id,
      quantity,
      changeAmount,
    });
    setCartStatistics(cartItem.value.id, changeAmount);
  };

  return {
    changeItemQuantity,
    removeItem,
    itemRegularPrice,
    itemSpecialPrice,
    itemTotalPrice,
    itemOptions,
    itemStock,
    itemQuantity,
    itemType,
    itemImageThumbnailUrl,
    isProduct,
    isPromotion,
  };
};
