import { ClientApiError, Product } from '@shopware-pwa/types';
import { LastSeenService, buildRequestConfig } from '~/logic/api-client';
import { ActionName } from '~/logic/interfaces';

/**
 * Composable to manage last seen products return
 *
 * @public
 * @category Last seen list
 */
export type UseLastSeenReturn = {
  /**
   * Loads the last seen products for the current participant. If the participant is a guide,
   * it fetches last seen products for the controlled client; otherwise, it fetches last seen
   * products for the client itself.
   * @function
   */
  loadLastSeen: () => Promise<Product[] | undefined>;

  /**
   * Load last seen products specifically for guide participants.
   * @param {Product[]} [newLastSeen] - Optional new set of last seen products.
   */
  // loadGuideLastSeen: (newLastSeen?: Product[]) => Promise<Product[] | undefined>;

  /**
   * Reset last seen products and errors to default values.
   * @function
   * @returns {void}
   */
  resetLastSeen: () => void;

  /**
   * Count of last seen products.
   */
  lastSeenCount: ComputedRef<number | undefined>;

  /**
   * Loading state of last seen products.
   */
  isLastSeenLoading: ComputedRef<boolean>;

  /**
   * Array of last seen products.
   */
  lastSeenProducts: ComputedRef<Product[] | undefined>;
};

/**
 * Composable to manage last seen products
 *
 * @category Last seen list
 */
export const _useLastSeen = (): UseLastSeenReturn => {
  const { setAbortActions, abortAction, apiInstance, getActionController } = useShopwareContext();
  const isLastSeenLoading = ref<boolean>(false);

  const lastSeenProducts = computed(() => _storeLastSeen.value || []);

  const lastSeenCount = computed(() => lastSeenProducts.value.length);

  const _storeLastSeen = ref<Product[]>();
  const _storeLastSeenErrors = ref<ClientApiError | null>(null);
  setAbortActions([ActionName.LOAD_LAST_SEEN]);

  // const { isGuide, isClient } = useUser();
  // const { getContextTokenForCurrentClient, controlledClient } = useParticipants();
  // const { adminApiInstance } = useAdminApiInstance();

  const resetLastSeen = () => {
    _storeLastSeen.value = undefined;
    _storeLastSeenErrors.value = null;
  };

  const loadLastSeen = async (): Promise<Product[] | undefined> => {
    _storeLastSeenErrors.value = null;
    abortAction(ActionName.LOAD_LAST_SEEN);
    isLastSeenLoading.value = true;
    try {
      const data = await LastSeenService.getLastSeenProducts(
        apiInstance,
        buildRequestConfig({
          controller: getActionController(ActionName.LOAD_LAST_SEEN),
        }),
      );
      _storeLastSeen.value = data?.elements || [];
      return _storeLastSeen.value;
    } catch (e) {
      _storeLastSeen.value = undefined;
      _storeLastSeenErrors.value = e as ClientApiError;
    } finally {
      isLastSeenLoading.value = false;
    }
  };

  // const loadGuideLastSeen = async (newLastSeen?: Product[]): Promise<Product[] | undefined> => {
  //   isLastSeenLoading.value = true;
  //   _storeLastSeenErrors.value = null;
  //   abortAction(ActionName.LOAD_LAST_SEEN);
  //   if (newLastSeen) {
  //     _storeLastSeen.value = newLastSeen;
  //     return newLastSeen;
  //   }
  //   try {
  //     const data = await LastSeenService.getLastSeenProducts(
  //       apiInstance,
  //       buildRequestConfig({
  //         controller: getActionController(ActionName.LOAD_LAST_SEEN),
  //       }),
  //     );
  //     _storeLastSeen.value = data?.elements || [];
  //   } catch (e) {
  //     _storeLastSeenErrors.value = e as ClientApiError;
  //   } finally {
  //     isLastSeenLoading.value = false;
  //   }
  // };

  return {
    loadLastSeen,
    // loadGuideLastSeen,
    resetLastSeen,
    lastSeenCount,
    lastSeenProducts,
    isLastSeenLoading: computed(() => isLastSeenLoading.value),
  };
};

/**
 * Composable to manage last seen products return
 *
 * @public
 * @category Last seen list
 */
export const useLastSeen = createSharedComposable(_useLastSeen);
