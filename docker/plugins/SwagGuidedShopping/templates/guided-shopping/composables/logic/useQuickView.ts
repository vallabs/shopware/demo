import { ClientApiError } from '@shopware-pwa/types';
import { DynamicPagesService } from '~/logic/api-client';

/**
 * Composable to manage quick view
 *
 * @category Presentation
 */
export const _useQuickView = () => {
  const { apiInstance } = useShopwareContext();
  const { stateForAll } = usePresentationState();
  const { publishInteraction } = useInteractions();
  const { loadLastSeen } = useLastSeen();
  const quickViewCmsPage = ref<any>(null);
  const isLoading = ref<any>(false);
  const currentProductId = ref<string>();
  const quickViewModalController = useModal<string>();

  const getQuickViewCmsPage = async () => {
    if (currentProductId.value === null) return false;
    try {
      if (!currentProductId.value || !stateForAll.value?.quickviewPageId) {
        const e = {
          message: 'No product id or quickviewPageId provided',
        };
        throw e;
      }
      const data = await DynamicPagesService.getQuickView(
        {
          productId: currentProductId.value,
          cmsPageLayoutId: stateForAll.value?.quickviewPageId,
        },
        apiInstance,
      );
      quickViewCmsPage.value = data;
    } catch (e) {
      e as ClientApiError;
    } finally {
      isLoading.value = false;
    }
  };

  const initQuickView = async (productId: string) => {
    setQuickViewProduct(productId);
    if (!currentProductId.value) return;
    await publishInteraction({
      name: InteractionName.QUICKVIEW_OPENED,
      payload: {
        productId: currentProductId.value,
      },
    });
    loadLastSeen();
  };

  const onQuickViewClosed = () => {
    return publishInteraction({
      name: InteractionName.QUICKVIEW_CLOSED,
      payload: {
        productId: currentProductId.value ?? '',
      },
    });
  };

  // TODO: old logic, need refactor this method later
  const setQuickViewProduct = async (productId: string) => {
    const isNewProduct = isQuickViewProductChanging(productId);
    if (!isNewProduct) return;
    setQuickViewLoadingState(isNewProduct);
    currentProductId.value = productId;
    await getQuickViewCmsPage();
  };

  // TODO: old logic, need refactor this method later
  const isQuickViewProductChanging = (newId: string) => {
    const currentId = quickViewCmsPage?.value?.product?.id;
    return currentId !== newId;
  };

  // TODO: old logic, need refactor this method later
  const setQuickViewLoadingState = (state: boolean | null = null) => {
    if (state == null) return;
    isLoading.value = state;
  };

  return {
    quickViewCmsPage: computed(() => quickViewCmsPage.value),
    isQuickViewLoading: computed(() => isLoading.value),
    initQuickView,
    quickViewModalController,
    isQuickViewProductChanging,
    setQuickViewProduct,
    setQuickViewLoadingState,
    onQuickViewClosed,
  };
};

/**
 * Composable to manage quick view
 *
 * @public
 * @category Presentation
 */
export const useQuickView = createSharedComposable(_useQuickView);
