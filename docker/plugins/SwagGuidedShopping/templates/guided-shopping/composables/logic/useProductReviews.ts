import { ClientApiError, ProductReview } from '@shopware-pwa/types';
import { ActionName } from '~/logic/interfaces';
import { ProductsService, buildRequestConfig } from '~/logic/api-client';

export type UseProductReviewReturn = {
  /**
   * All items in product review list for given product based on parameters
   */
  productReviews: ComputedRef<ProductReview[]>;

  /**
   * Load the product review and related data
   */
  loadProductReviews: (parameters?: { limit: number; page: number }) => Promise<void>;

  /**
   * Loading state of review list
   */
  isLoading: ComputedRef<boolean>;
};

const useProductReview = (productId: Ref<string> | ComputedRef<string>): UseProductReviewReturn => {
  const { setAbortActions, abortAction, apiInstance, getActionController } = useShopwareContext();

  setAbortActions([ActionName.LOAD_PRODUCT_REVIEWS]);

  const _storeReviewList = ref<ProductReview[]>([]);
  const _storeReviewListError = ref<ClientApiError | null>(null);
  const isLoading = ref(false);

  const loadProductReviews = async (
    parameters: { limit: number; page: number } = {
      limit: 10,
      page: 1,
    },
  ) => {
    isLoading.value = true;
    abortAction(ActionName.LOAD_PRODUCT_REVIEWS);
    try {
      const result = await ProductsService.loadProductReviews(
        productId.value,
        {
          ...parameters,
          rating: undefined,
        },
        apiInstance,
        buildRequestConfig({
          controller: getActionController(ActionName.LOAD_PRODUCT_REVIEWS),
        }),
      );
      _storeReviewList.value = result.elements;
    } catch (e) {
      _storeReviewListError.value = e as ClientApiError;
    } finally {
      isLoading.value = false;
    }
  };

  return {
    productReviews: computed(() => _storeReviewList.value),
    loadProductReviews,
    isLoading: computed(() => isLoading.value),
  };
};

export default useProductReview;
