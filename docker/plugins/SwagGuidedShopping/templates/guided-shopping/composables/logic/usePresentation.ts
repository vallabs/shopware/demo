import { ClientApiError } from '@shopware-pwa/types';
import { watchDebounced } from '@vueuse/core';
import set from 'lodash/set';
import { PresentationService, buildRequestConfig } from '~/logic/api-client';
import {
  PresentationPageModel,
  SlideGroupList,
  ActionName,
  SlideGroupItem,
} from '~/logic/interfaces';

/**
 * Composable to manage presentation
 *
 * @category Presentation
 */
const _usePresentation = () => {
  const { apiInstance, setAbortActions, abortAction, getActionController } = useShopwareContext();
  const { adminApiInstance } = useAdminApiInstance();
  const { requestCloseAllDynamicPages } = useDynamicPage();
  const { resetLastElementScrolledTo } = useElementInteraction();
  const { appointmentId, isSelfService } = useAppointment();
  const { stateForGuides, stateForMe, updateState, navigationPageIndex } = usePresentationState();
  const { isGuide, isClient, setAttendeeName, attendee } = useUser();
  const { publishInteraction } = useInteractions();
  const { mercurePublish } = useMercure();
  const { loadLastSeen } = useLastSeen();
  const stateLoaded = ref(false);
  const presentation = ref<PresentationPageModel>();
  const slides = computed(() => presentation.value?.navigation || []);

  setAbortActions([ActionName.LOAD_SLIDE, ActionName.SWITCH_SLIDE]);

  // this variable handles the visual current page index, navigationPageIndex is the actual current page index
  const currentSlideIndex = ref<number>(0);

  const slideGroupList = computed<SlideGroupList>(() => {
    const tree: Map<string, SlideGroupItem> = new Map();

    slides.value.forEach((item) => {
      if (!tree.has(item.groupId)) {
        tree.set(item.groupId, { id: item.groupId, name: item.groupName, items: [] });
      }

      tree.get(item.groupId)!.items.push(item);
    });

    return Array.from(tree, ([_, items]) => items);
  });

  const canGoPrev = computed(() => currentSlideIndex.value > 1);
  const canGoNext = computed(() => currentSlideIndex.value < slides.value.length);

  watchDebounced(
    currentSlideIndex,
    (i) => {
      if (i === navigationPageIndex.value) return;
      switchSlide(i);
    },
    { debounce: 700 },
  );

  watch(navigationPageIndex, async (i) => {
    if (!i) return;
    if (i !== currentSlideIndex.value) {
      currentSlideIndex.value = i;
    }
    const pageResult = await loadSlide(currentSlideIndex.value - 1, true);

    if (pageResult?.cmsPage.type === 'presentation_product_detail' && pageResult?.product?.id) {
      await publishInteraction({
        name: InteractionName.PRODUCT_VIEWED,
        payload: {
          productId: pageResult.product.id,
        },
      });

      await loadLastSeen();
    }
  });

  const currentSlide = computed(() => {
    if (!navigationPageIndex.value) return;
    return slides.value[navigationPageIndex.value - 1];
  });

  const debounceSwitchSlide = (index: number) => {
    currentSlideIndex.value = index;
  };

  const switchSlide = async (index: number, forceRefresh = true) => {
    // abort if switchSlide action is overlapped
    abortAction(ActionName.SWITCH_SLIDE);
    const idx = index - 1;
    const navItem = presentation.value?.navigation[idx];

    if (!navItem) return;
    if (isGuide.value) {
      requestCloseAllDynamicPages();
    }
    if (index === navigationPageIndex.value) {
      // if it's the current page, just load the slide
      loadSlide(currentSlideIndex.value - 1, forceRefresh, ActionName.SWITCH_SLIDE);
    } else if (!isSelfService.value && isGuide.value) {
      // if it's not the current page and in guide mode, navigate via mercure guide
      mercurePublish.guide(MercureEvent.NAVIGATED, index);
    } else {
      // if it's not the current page and in self mode, navigate via mercure local
      mercurePublish.local(MercureEvent.NAVIGATED, index);
    }

    await publishInteraction(
      {
        name: InteractionName.PAGE_VIEWED,
        payload: {
          pageId: navItem.groupId,
          sectionId: navItem.sectionId,
          slideAlias: index,
        },
      },
      buildRequestConfig({ controller: getActionController(ActionName.SWITCH_SLIDE) }),
    );
  };

  const loadSlide = async (
    idx: number,
    forceRefresh?: boolean,
    mainActionName = ActionName.LOAD_SLIDE,
  ) => {
    resetLastElementScrolledTo();
    const navItem = presentation.value?.navigation[idx];
    if (!navItem) return;
    if (!slides.value[idx]?.cmsPageResult || forceRefresh) {
      abortAction(mainActionName);
      navItem.cmsPageResult = await PresentationService.getSlideData(
        {
          pageId: navItem.groupId,
          slideId: navItem.sectionId,
        },
        apiInstance,
        buildRequestConfig({ controller: getActionController(mainActionName) }),
      );
      navItem.pickedProductsCount =
        navItem.cmsPageResult.extensions.cmsPageRelation.pickedProductIds?.length;
    }

    return slides.value[idx].cmsPageResult;
  };

  const updatePickedProductIds = (idx: number, ids: string[]) => {
    const navItem = presentation.value?.navigation[idx];
    if (!navItem) return;
    set(navItem, 'cmsPageResult.extensions.cmsPageRelation.pickedProductIds', ids);
    navItem.pickedProductsCount = ids.length;
  };

  const loadPresentation = async () => {
    presentation.value = await PresentationService.getPage(apiInstance);
  };

  const loadState = async () => {
    try {
      if (isGuide.value) {
        const response = await PresentationService.getStateForGuides(
          appointmentId.value!,
          adminApiInstance,
        );
        updateState(MercureEvent.STATE_FOR_ALL, response.stateForAll);
        updateState(MercureEvent.STATE_FOR_GUIDES, response.stateForGuides);
        updateState(MercureEvent.STATE_FOR_ME, response.stateForMe);

        if (!attendee.value?.name) {
          setAttendeeName(stateForGuides.value?.guides[attendee.value.id]?.attendeeName ?? '');
        }
        // TODO: assign proper states depending on response type when filled
      } else if (isSelfService.value || isClient.value) {
        // also load state for client of guide joins self service appointment
        const response = await PresentationService.getStateForClients(apiInstance);
        // if (!skipStateForAll)
        //   _storeState.value.stateForAll = response.stateForAll;
        // _storeState.value.stateForClients = response.stateForClients;
        // if (
        //   !lastGuideHoveredElement.value &&
        //   response.stateForClients?.hoveredElementId
        // ) {
        //   lastGuideHoveredElement.value =
        //     response.stateForClients.hoveredElementId;
        // }
        updateState(MercureEvent.STATE_FOR_ALL, response.stateForAll);
        updateState(MercureEvent.STATE_FOR_CLIENTS, response.stateForClients);
        updateState(MercureEvent.STATE_FOR_ME, response.stateForMe);
        if (!attendee.value?.name) {
          setAttendeeName(stateForMe.value?.attendeeName ?? '');
        }
      }

      stateLoaded.value = true;
    } catch (e) {
      // TODO: should handle error
      e as ClientApiError;
    }
  };

  const goPrev = () => {
    if (canGoPrev.value) {
      debounceSwitchSlide(currentSlideIndex.value - 1);
    }
  };

  const goNext = () => {
    if (canGoNext.value) {
      debounceSwitchSlide(currentSlideIndex.value + 1);
    }
  };

  const goTo = (index: number, forceRefresh = true) => {
    if (index > 0 && index <= slides.value.length) {
      switchSlide(index, forceRefresh);
    }
  };

  return {
    loadState,
    currentSlideIndex: computed(() => currentSlideIndex.value),
    currentSlide,
    slides: computed(() => slides.value),
    slideGroupList,
    slidesCount: computed(() => slides.value.length),
    canGoPrev,
    switchSlide,
    canGoNext,
    updatePickedProductIds,
    loadPresentation,
    goNext,
    goPrev,
    goTo,
    loadSlide,
    stateLoaded,
  };
};

/**
 * Composable to manage presentation
 *
 * @public
 * @category Presentation
 */
export const usePresentation = createSharedComposable(_usePresentation);
