import { Product, ClientApiError } from '@shopware-pwa/types';
import { ActionName } from '~/logic/interfaces';
import { LikeListService, buildRequestConfig } from '~/logic/api-client';

/**
 * Composable to manage a wishlist item
 *
 * @public
 * @category Wishlist
 */
export type UseLikeItemReturn = {
  /**
   * is product liked
   */
  isLiked: ComputedRef<Boolean>;

  /**
   * Add product to like list
   */
  like(): Promise<void>;

  /**
   * Remove a product in like list
   */
  unlike(): Promise<void>;

  /**
   * Toggle a product in like list
   */
  toggleLike(): Promise<void>;

  /**
   * Loading state when perform action for like
   */
  loading: ComputedRef<Boolean>;
};

/**
 * The composable to manage when the client change a their wishlist item
 *
 * @public
 * @category Wishlist
 */
export type IClientChangedLike = {
  /**
   * Changed by which attendee
   */
  changedBy: string;

  /**
   * Product ID
   */
  productId: string;

  /**
   * Like status
   */
  like: boolean;
};

/**
 * The composable to manage when the guide act for a client to change
 * a their wishlist item
 *
 * @public
 * @category Wishlist
 */
export type IGuideChangedLike = IClientChangedLike & {
  /**
   * Client ID
   */
  clientId: string;
};

/**
 * The composable to manage a wishlist item
 *
 * @public
 * @category Wishlist
 * @param productId Product ID
 */
export const useLikeItem = (productId: Ref<string> | ComputedRef<string>): UseLikeItemReturn => {
  const { likeList, loadLikeList } = useLikeList();
  const { adminApiInstance } = useAdminApiInstance();
  const { mercurePublish } = useMercure();
  const { apiInstance, getActionController } = useShopwareContext();
  const { controlledClient, self } = useParticipants();
  const { isGuide, isClient } = useUser();
  const { publishInteraction } = useInteractions();
  const _storeLikeListErrors = ref<ClientApiError | null>(null);
  const loading = ref(false);

  const isLiked = computed(() => {
    return !!(
      (isClient.value || (isGuide.value && controlledClient.value)) &&
      likeList.value.some((item: Product) => item.id === productId.value)
    );
  });

  const like = async (): Promise<void> => {
    if (isGuide.value) {
      if (!controlledClient.value) return;
      loading.value = true;
      await LikeListService.likeProductForAttendee(
        {
          productId: productId.value,
          attendeeId: controlledClient.value.attendeeId,
        },
        adminApiInstance,
      );
      await Promise.all([
        loadLikeList(),
        mercurePublish.guide<IGuideChangedLike>(MercureEvent.GUIDE_CHANGED_LIKES, {
          changedBy: self.value?.attendeeId ?? '',
          clientId: controlledClient.value.attendeeId,
          productId: productId.value,
          like: true,
        }),
        publishInteraction({
          name: InteractionName.PRODUCT_LIKED,
          payload: {
            productId: productId.value,
          },
        }),
      ]);
      loading.value = false;
    } else if (isClient.value) {
      loading.value = true;
      try {
        await LikeListService.likeProduct(productId.value, apiInstance);

        await Promise.all([
          loadLikeList(),
          mercurePublish.client<IClientChangedLike>(MercureEvent.CLIENT_CHANGED_LIKES, {
            changedBy: self.value?.attendeeId ?? '',
            productId: productId.value,
            like: true,
          }),
          publishInteraction({
            name: InteractionName.PRODUCT_LIKED,
            payload: {
              productId: productId.value,
            },
          }),
        ]);
      } catch (e) {
        _storeLikeListErrors.value = e as ClientApiError;
      }
      loading.value = false;
    }
  };

  const unlike = async (): Promise<void> => {
    if (isGuide.value) {
      if (!controlledClient.value) return;
      loading.value = true;
      await LikeListService.unlikeProductForAttendee(
        {
          productId: productId.value,
          attendeeId: controlledClient.value.attendeeId,
        },
        adminApiInstance,
      );
      await Promise.all([
        loadLikeList(),
        mercurePublish.guide(MercureEvent.GUIDE_CHANGED_LIKES, {
          changedBy: self.value?.attendeeId,
          clientId: controlledClient.value.attendeeId,
          productId: productId.value,
          like: false,
        }),
        publishInteraction({
          name: InteractionName.PRODUCT_UN_LIKED,
          payload: {
            productId: productId.value,
          },
        }),
      ]);
      loading.value = false;
    } else if (isClient.value) {
      loading.value = true;
      try {
        await LikeListService.unlikeProduct(
          productId.value,
          apiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.REMOVE_FROM_LIKE_LIST),
          }),
        );
        await Promise.all([
          loadLikeList(),
          mercurePublish.client(MercureEvent.CLIENT_CHANGED_LIKES, {
            changedBy: self.value?.attendeeId,
            productId: productId.value,
            like: false,
          }),
          publishInteraction({
            name: InteractionName.PRODUCT_UN_LIKED,
            payload: {
              productId: productId.value,
            },
          }),
        ]);
      } catch (e) {
        _storeLikeListErrors.value = e as ClientApiError;
      }
      loading.value = false;
    }
  };

  const toggleLike = async (): Promise<void> => {
    if (!isLiked.value) {
      await like();
    } else {
      await unlike();
    }
  };

  return {
    isLiked,
    like,
    unlike,
    toggleLike,
    loading: computed(() => loading.value),
  };
};
