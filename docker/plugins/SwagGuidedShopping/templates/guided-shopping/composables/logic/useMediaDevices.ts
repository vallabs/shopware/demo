import { MediaDevices, VideoAudioSettingsType } from '~/logic/interfaces';

const FAKE_SPEAKER_ID = '__fakeSpeakerDeviceId';

type DeviceStorageKey = 'microphone-device' | 'speaker-device' | 'camera-device';

// export type DeviceList = {
//   videoInput: MediaDeviceInfo[],
//   audioInput: MediaDeviceInfo[],
//   audioOutput: MediaDeviceInfo[],
// };

const DEVICE_KEYS: { [key: string]: DeviceStorageKey } = {
  microphone: 'microphone-device',
  speaker: 'speaker-device',
  camera: 'camera-device',
};

/**
 * Type of device error
 *
 * @public
 * @category Presentation
 */
export type DeviceError =
  | 'NO_DEVICES_FOUND'
  | 'CAMERA_IN_USE'
  | 'MICROPHONE_IN_USE'
  | 'CAMERA_AND_MICROPHONE_IN_USE'
  | 'SPEAKER_NOT_ALLOWED';
// export type MediaDeviceChanged = { storageKey, deviceId };

/**
 * Type of media device changed
 *
 * @public
 * @category Presentation
 */
export type MediaDeviceChanged = any;

const SAVED_SETTINGS_STORAGE_KEY = 'sw-gs-call-saved-settings';
const DEVICES_STORAGE_KEY = 'sw-gs-call-devices';

/**
 * Composable to manage media devices
 *
 * @category Presentation
 */
const _useMediaDevices = () => {
  const { stateForAll } = usePresentationState();
  const { isSelfService } = useAppointment();
  const videoAudioSettings = computed(
    () => stateForAll.value?.videoAudioSettings as VideoAudioSettingsType,
  );
  const callEnabled = computed(
    () =>
      [VideoAudioSettingsType.VIDEO_CALL, VideoAudioSettingsType.VOICE_CALL].includes(
        videoAudioSettings.value,
      ) && !isSelfService.value,
  );
  const isVoiceCall = computed(
    () => videoAudioSettings.value === VideoAudioSettingsType.VOICE_CALL,
  );
  const isVideoCall = computed(
    () => videoAudioSettings.value === VideoAudioSettingsType.VIDEO_CALL,
  );
  // const contextName = COMPOSABLE_NAME;
  // // const { sharedRef } = useSharedState();

  // const microphoneDevice = ref<string>();
  // const speakerDevice = ref<string>();
  // const cameraDevice = ref<string>();

  const hasMediaDevicesStored = ref<boolean>();
  // const _hasSavedSettings = ref<boolean>();
  // const _mediaDeviceChanged = ref<MediaDeviceChanged>();
  const deviceError = ref<DeviceError>();
  const mediaStream = ref<MediaStream>();

  const mediaDevices = ref<MediaDevices>({
    videoInput: [],
    audioInput: [],
    audioOutput: [],
  });

  const microphoneDevices = computed<MediaDeviceInfo[]>(() => mediaDevices.value.audioInput);
  const speakerDevices = computed<MediaDeviceInfo[]>(() => mediaDevices.value.audioOutput);
  const cameraDevices = computed<MediaDeviceInfo[]>(() => mediaDevices.value.videoInput);
  const isMissingDevices = computed<boolean>(
    () => !mediaDevices.value.audioInput?.length || !mediaDevices.value.videoInput?.length,
  );

  /**
   * checks if the devices are set in the session storage
   */
  const storeHasDevices = () => {
    hasMediaDevicesStored.value = Object.values(DEVICE_KEYS).every((storageKey) => {
      return !!getDeviceFromStorage(storageKey);
    });
  };

  const init = async () => {
    storeHasDevices();

    // request media permissions with default devices
    await getMediaStream();

    // request all the availables devices after got media permissions
    await getMediaDevices();

    // add event listener when the availables devices changed
    if (navigator?.mediaDevices) {
      navigator.mediaDevices.addEventListener('devicechange', onDeviceChange);
    }
  };

  const cleanup = () => {
    if (navigator && navigator.mediaDevices) {
      navigator.mediaDevices.removeEventListener('devicechange', onDeviceChange);
    }

    stopTracking();
  };

  const stopTracking = () => {
    if (mediaStream.value) {
      for (const track of mediaStream.value.getTracks()) {
        track.stop();
      }
    }
  };

  const onDeviceChange = () => getMediaDevices(true);

  /**
   * evaluates available devices and saves them
   */
  const getMediaDevices = async (
    renewList = false,
    allowEmptyLabels = false,
  ): Promise<MediaDevices | undefined> => {
    try {
      let devices = getMediaDevicesFromStorage();

      if (!devices || renewList) {
        // if no devices were found in storage or we want to force
        const enumeratedDevices = await navigator.mediaDevices.enumerateDevices();
        devices = getDevices(enumeratedDevices, allowEmptyLabels);
      }

      mediaDevices.value = devices;
      return devices;
    } catch (err) {
      handleDeviceError('NO_DEVICES_FOUND');
    }
  };

  /**
   * returns a new mediaStream constrained to the currently used devices
   * @param devices
   */
  const getMediaStream = async (devices?: {
    video: string;
    audio: string;
  }): Promise<MediaStream | undefined> => {
    try {
      stopTracking();

      const constraints = getConstraints(devices);

      mediaStream.value = await navigator.mediaDevices.getUserMedia(constraints);
      return mediaStream.value;
    } catch (e) {
      handleDeviceError('NO_DEVICES_FOUND');
    }
  };

  /**
   * returns the constraints to create a media stream with the first selectable mediaDevices
   *
   * @param devices
   */
  const getConstraints = (devices?: { audio: string; video: string }): MediaStreamConstraints => {
    const constraints: MediaStreamConstraints = {};
    constraints.audio = devices?.audio ? { deviceId: { exact: devices?.audio } } : true;
    if (isVideoCall.value) {
      constraints.video = devices?.video ? { deviceId: { exact: devices?.video } } : true;
    }
    return constraints;
  };

  /**
   * returns object sorted by devices kind
   */
  const getDevices = (deviceInfo: MediaDeviceInfo[], allowEmptyLabels = false): MediaDevices => {
    const devices: MediaDevices = {
      videoInput: [],
      audioInput: [],
      audioOutput: [],
    };

    deviceInfo.forEach((info) => {
      if (!allowEmptyLabels && !info.label) return;

      switch (info.kind) {
        case 'audioinput':
          devices.audioInput.push(info);
          break;
        case 'audiooutput':
          devices.audioOutput.push(info);
          break;
        case 'videoinput':
          devices.videoInput.push(info);
          break;
        default:
          break;
      }
    });

    addFakeSpeaker(devices);
    // setMediaDevicesToStorage(devices);

    return devices;
  };

  // /**
  //  * returns a device form the storage
  //  * falls back to first entry for device type
  //  *
  //  * @param devices
  //  * @param type
  //  * @param storageKey
  //  */
  // const readDeviceFromStorage = (devices, type: 'audioInput' | 'audioOutput' | 'videoInput', storageKey) => {
  //   let device = getDeviceFromStorage(storageKey);
  //   if (!device && devices[type].length > 0) device = devices[type][0].deviceId;
  //   return device;
  // };

  // /**
  //  * sets devices to storage or first value for select box
  //  *
  //  * @param devices
  //  */
  // const setDevicesFromStorage = (devices) => {
  //   microphoneDevice.value = readDeviceFromStorage(devices, 'audioInput', 'microphone-device');
  //   speakerDevice.value = readDeviceFromStorage(devices, 'audioOutput', 'speaker-device');

  //   // disable camera evaluation if audioOnly is active
  //   if (isAudioOnly.value) {
  //     cameraDevice.value = null;
  //   } else {
  //     cameraDevice.value = readDeviceFromStorage(devices, 'videoInput', 'camera-device');
  //   }
  // };

  /**
   * Firefox always uses the default Speaker
   * so we need to fake this because the speaker response is empty
   *
   * @param devices
   */
  const addFakeSpeaker = (devices: MediaDevices) => {
    if (devices.audioOutput.length !== 0) return;

    devices.audioOutput.push({
      deviceId: FAKE_SPEAKER_ID,
      groupId: FAKE_SPEAKER_ID,
      kind: 'audiooutput',
      label: 'System Speaker',
    } as MediaDeviceInfo);
  };

  /**
   * sets the found media devices to the session storage
   */
  // const setMediaDevicesToStorage = (devices: MediaDevices) => {
  //   sessionStorage.setItem(DEVICES_STORAGE_KEY, JSON.stringify(devices));
  // };

  /**
   * retrieves the media devices from the session storage
   */
  const getMediaDevicesFromStorage = (): MediaDevices | null => {
    const devices = sessionStorage.getItem(DEVICES_STORAGE_KEY);
    try {
      return JSON.parse(devices as string);
    } catch (e) {
      return null;
    }
  };

  // /**
  //  * saves used device to storage
  //  *
  //  * @param storageKey
  //  * @param deviceId
  //  */
  // const setDeviceToStorage = (storageKey: DeviceStorageKey, deviceId) => {
  //   _mediaDeviceChanged.value = { storageKey, deviceId };
  //   if (deviceId === null || deviceId === 'null') deviceId = '';
  //   sessionStorage.setItem(`sw-gs-call-${storageKey}`, deviceId);
  // };

  // /**
  //  * sets the initially selected devices in the storage
  //  */
  // const saveDevicesToStorage = () => {
  //   setDeviceToStorage('microphone-device', microphoneDevice.value);
  //   setDeviceToStorage('speaker-device', speakerDevice.value);
  //   setDeviceToStorage('camera-device', cameraDevice.value);

  //   setSavedSettings();
  // };

  // const setSavedSettings = () => {
  //   sessionStorage.setItem(SAVED_SETTINGS_STORAGE_KEY, 'true');
  //   _hasSavedSettings.value = true;
  // };

  // const setAudioOnly = () => {
  //   //reset devices
  //   cameraDevice.value = null;
  //   // microphoneDevice.value = null;

  //   sessionStorage.setItem(AUDIO_ONLY_STORAGE_KEY, 'true');
  //   isAudioOnly.value = true;
  // };

  // /**
  //  * saves used device to storage
  //  *
  //  * @param storageKey
  //  */
  // const unsetDeviceFromStorage = (storageKey: DeviceStorageKey) => {
  //   _mediaDeviceChanged.value = { storageKey, 'deviceId': null };
  //   sessionStorage.removeItem(`sw-gs-call-${storageKey}`);
  // };

  // /**
  //  * sets the found media devices to the session storage
  //  */
  // const unsetDevicesFromStorage = () => {
  //   unsetDeviceFromStorage('microphone-device');
  //   unsetDeviceFromStorage('speaker-device');
  //   unsetDeviceFromStorage('camera-device');
  // };

  // /**
  //  * resets the whole device state
  //  */
  // const clearDeviceState = () => {
  //   unsetDevicesFromStorage();
  //   sessionStorage.removeItem(AUDIO_ONLY_STORAGE_KEY);
  //   sessionStorage.removeItem(DEVICES_STORAGE_KEY);
  //   sessionStorage.removeItem(MICROPHONE_STATE_STORAGE_KEY);
  //   sessionStorage.removeItem(CAMERA_STATE_STORAGE_KEY);
  //   sessionStorage.removeItem(SAVED_SETTINGS_STORAGE_KEY);
  // };

  /**
   * resets the whole device state
   */
  const clearDeviceStateOnError = () => {
    sessionStorage.removeItem(SAVED_SETTINGS_STORAGE_KEY);
  };

  // /**
  //  * returns the length of devices found
  //  */
  // const hasDevices = () => {
  //   let deviceCount = 0;
  //   Object.values(mediaDevices.value).forEach(deviceArray => deviceCount += deviceArray.length);
  //   return deviceCount > 0;
  // };

  /**
   * returns session stored device
   * @param storageKey
   */
  const getDeviceFromStorage = (storageKey: DeviceStorageKey): string | null => {
    let value = sessionStorage.getItem(`sw-gs-call-${storageKey}`);
    if (value == 'null') value = null;
    return value;
  };

  // /**
  //  * returns the device keys for outside usage
  //  */
  // const getDeviceKeys = () => {
  //   return DEVICE_KEYS
  // };

  /**
   * opens error modal if device error occurs
   * @param {DeviceError} errType
   */
  const handleDeviceError = (errType: DeviceError) => {
    if (!deviceError.value) clearDeviceStateOnError();
    deviceError.value = errType;
  };

  // /**
  //  * opens error modal if device error occurs
  //  */
  // const unsetDeviceError = () => {
  //   deviceError.value = null;
  // };

  // const deviceCount = (type) => {
  //   let deviceCount = 0;
  //   mediaDevices.value[type].forEach(deviceArray => {
  //     if (deviceArray.deviceId) deviceCount++;
  //   });
  //   return deviceCount;
  // }

  return {
    // getDeviceKeys,
    getMediaDevices,
    // storeHasDevices,
    // hasDevices,
    // saveDevicesToStorage,
    // setDevicesFromStorage,
    // unsetDevicesFromStorage,
    // clearDeviceState,
    // getDeviceFromStorage,
    deviceError: computed(() => deviceError.value),
    mediaStream: computed(() => mediaStream.value),
    // mediaDeviceChanged: computed(() => _mediaDeviceChanged.value),
    mediaDevices: computed(() => mediaDevices.value),
    // hasMediaDevicesStored: computed(() => hasMediaDevicesStored.value),
    // hasSavedSettings: computed(() => _hasSavedSettings.value),
    // cameraCount: computed(() => deviceCount('videoInput')),
    // microphoneCount: computed(() => deviceCount('audioInput')),
    // speakerCount: computed(() => deviceCount('audioOutput')),
    // isAudioOnly,
    microphoneDevices,
    speakerDevices,
    cameraDevices,
    // microphoneDevice,
    // speakerDevice,
    // setAudioOnly,
    // setDeviceToStorage,
    cleanup,
    init,
    // getConstraints,
    handleDeviceError,
    // unsetDeviceError,
    getMediaStream,
    isMissingDevices,
    callEnabled,
    isVoiceCall,
    isVideoCall,
  };
};

/**
 * Composable to manage media devices
 *
 * @public
 * @category Presentation
 */
export const useMediaDevices = createSharedComposable(_useMediaDevices);
