import { SlideModel } from '~/logic/interfaces';
import { DynamicPagesService } from '~/logic/api-client';

/**
 * Composable to manage enum of dynamic page type
 * @public
 * @category Presentation
 */
export enum DynamicPageType {
  PRODUCT = 'product',
  PRODUCT_LIST = 'productList',
  LIKE_LIST = 'likeList',
}

/**
 * Composable to manage dynamic page
 * @public
 * @category Presentation
 */
export type DynamicPageData = {
  id: string;
  static?: string;
  cms?: SlideModel;
};

/**
 * Composable to manage enum of dynamic interaction name
 * @public
 * @category Presentation
 */
export enum DynamicInteractionName {
  DYNAMIC_PRODUCT_PAGE_OPENED = 'dynamicProductPage.opened',
  DYNAMIC_PRODUCT_LISTING_PAGE_OPENED = 'dynamicProductListingPage.opened',
  DYNAMIC_PAGE_OPENED = 'dynamicPage.opened',
  DYNAMIC_PAGE_CLOSED = 'dynamicPage.closed',
}

/**
 * Composable to manage base dynamic page
 * @public
 * @category Presentation
 */
export type BaseDynamicPage<T> = {
  /**
   * Dynamic page ID
   */
  id: string;
  /**
   * Dynamic page type
   */
  type: T;

  /**
   * Is dynamic page opened
   */
  opened: boolean;
};

/**
 * Composable to manage dynamic product page
 * @public
 * @category Presentation
 */
export type DynamicProductPage = BaseDynamicPage<DynamicPageType.PRODUCT> & {
  /**
   * Product ID
   */
  productId: string;
};

/**
 * Composable to manage dynamic product listing page
 * @public
 * @category Presentation
 */
export type DynamicProductListingPage = BaseDynamicPage<DynamicPageType.PRODUCT_LIST> & {
  /**
   * Page number
   */
  page: number;
};

/**
 * Composable to manage dynamic like list page
 * @public
 * @category Presentation
 */
export type DynamicLikeListPage = BaseDynamicPage<DynamicPageType.LIKE_LIST>;

/**
 * Composable to manage dynamic page
 * @public
 * @category Presentation
 */
export type DynamicPage = DynamicProductPage | DynamicProductListingPage | DynamicLikeListPage;

/**
 * Composable to manage dynamic page
 * @category Presentation
 */
const _useDynamicPage = () => {
  const { stateForAll, openedDynamicPage } = usePresentationState();
  const { resetLastElementScrolledTo } = useElementInteraction();
  const { mercurePublish } = useMercure();
  const { publishInteraction } = useInteractions();
  const { loadLastSeen } = useLastSeen();
  const { apiInstance } = useShopwareContext();
  const { isSelfService } = useAppointment();
  const dynamicCmsPages = ref<DynamicPageData[]>([]);
  const loading = ref<boolean>();
  // const _storeProductPage = sharedRef<CmsPage>(
  //   `${contextName}-productPage`,
  //   null
  // );
  const _slidePosition = ref<number>(0);

  // const dynamicPageProductId = computed(() =>
  //   dynamicPage?.value?.opened ? dynamicPage?.value?.productId : null,
  // );

  // open dynamic page and inform others about it
  const requestDynamicPage = async (
    data:
      | Omit<DynamicProductPage, 'opened' | 'id'>
      | Omit<DynamicLikeListPage, 'opened' | 'id'>
      | Omit<DynamicProductListingPage, 'opened' | 'id'>,
    showLoading = true,
    id?: string,
  ) => {
    if (
      openedDynamicPage.value?.type === data.type &&
      data.type === DynamicPageType.PRODUCT &&
      data.productId &&
      (openedDynamicPage.value as any)?.productId === data.productId
    ) {
      return;
    }

    if (openedDynamicPage.value?.type === data.type && data.type === DynamicPageType.LIKE_LIST) {
      return;
    }

    resetLastElementScrolledTo();
    // const handleScrollingAndNavigation = page.type !== 'productList';

    // // store original listing position
    // let oldPageFound = false;
    // if (_storeLastDynamicPage.value) {
    //   oldPageFound = true;
    // }

    // // store last dynamic page to open it again if a dynamic page was opened on a dynamic page
    // if (handleScrollingAndNavigation) {
    //   _storeLastDynamicPage.value = dynamicPage.value;
    // }
    if (showLoading) {
      loading.value = true;
    }
    let interactionName: DynamicInteractionName | undefined;
    let interactionOptions:
      | Omit<DynamicProductPage, 'opened' | 'type' | 'id'>
      | Omit<DynamicLikeListPage, 'opened' | 'type' | 'id'>
      | Omit<DynamicProductListingPage, 'opened' | 'type' | 'id'> = {};

    if (data.type === DynamicPageType.PRODUCT) {
      interactionName = DynamicInteractionName.DYNAMIC_PRODUCT_PAGE_OPENED;
      interactionOptions = {
        productId: (data as DynamicProductPage).productId,
      };
    } else if (data.type === DynamicPageType.PRODUCT_LIST) {
      interactionName = DynamicInteractionName.DYNAMIC_PRODUCT_LISTING_PAGE_OPENED;
      interactionOptions = {
        page: (data as DynamicProductListingPage).page,
      };
    } else if (data.type === DynamicPageType.LIKE_LIST) {
      interactionName = DynamicInteractionName.DYNAMIC_PAGE_OPENED;
      interactionOptions = {};
    }

    if (!interactionName) {
      loading.value = false;
      return;
    }

    const RANDOM_ID = (Math.random() + 1).toString(36).substring(7);
    const currentId = id || RANDOM_ID;

    const payload: any = {
      id: currentId,
      type: data.type,
      opened: true,
      ...interactionOptions,
    };

    if (isSelfService.value) {
      mercurePublish.local(MercureEvent.DYNAMIC_PAGE_OPENED, payload);
    } else {
      // publish interaction & mercure also
      try {
        await publishInteraction({
          name: interactionName,
          payload,
        });
        mercurePublish.guide(MercureEvent.DYNAMIC_PAGE_OPENED, payload);
      } catch (e) {
        loading.value = false;
      }

      // if (!oldPageFound) {
      //   _slidePosition.value = document?.getElementsByClassName('sw-gs-layout')?.item(0)?.scrollTop;
      // }

      // if (handleScrollingAndNavigation) {
      //   resetSlidePosition(true);
      // }
    }
    return payload;
  };

  const appendDynamicCmsPage = (value?: DynamicPageData) => {
    if (!value) return;
    dynamicCmsPages.value.push(value);
  };

  const closeAllDynamicCmsPages = () => {
    dynamicCmsPages.value = [];
  };

  const closeSpecificDynamicCmsPage = (data: { id: string }) => {
    if (!data?.id) return;
    dynamicCmsPages.value = dynamicCmsPages.value.filter((page) => page.id !== data.id);
  };

  const openDynamicProductPage = async (data: Omit<DynamicProductPage, 'opened'>) => {
    if (!data.productId) return;
    loading.value = true;

    try {
      const res = await DynamicPagesService.getDetail(data.productId, apiInstance);
      appendDynamicCmsPage({
        id: data.id,
        cms: res,
      });
    } catch (e) {
      // handleError(e, {
      //   context: contextName,
      //   method: "loadProductPage",
      // });
    }
    loading.value = false;

    await publishInteraction({
      name: InteractionName.PRODUCT_VIEWED,
      payload: {
        productId: data.productId,
      },
    });

    loadLastSeen();
  };

  const openDynamicLikeListPage = (data: Omit<DynamicLikeListPage, 'opened'>) => {
    appendDynamicCmsPage({
      id: data.id,
      static: 'LikeListPage',
    });
    loading.value = false;
  };

  // // open dynamic page and inform others about it
  // const openDynamicPage = (page: DynamicPageResponse) => {
  //   if (!page.opened) return;

  //   const handleScrollingAndNavigation = page.type !== 'productList';

  //   // store original listing position
  //   let oldPageFound = false;
  //   if (_storeLastDynamicPage.value) {
  //     oldPageFound = true;
  //   }

  //   // store last dynamic page to open it again if a dynamic page was opened on a dynamic page
  //   if (handleScrollingAndNavigation) {
  //     _storeLastDynamicPage.value = dynamicPage.value;
  //   }

  //   let interactionName = null;
  //   if (page.type === 'product') {
  //     interactionName = 'dynamicProductPage.opened';
  //   } else if (page.type === 'productList') {
  //     interactionName = 'dynamicProductListingPage.opened';
  //   } else if (page.type === 'likeList') {
  //     interactionName = 'dynamicPage.opened';
  //   }

  //   if (!interactionName) return;

  //   if (isSelfService.value) {
  //     mercurePublish.local(InteractionName.DYNAMIC_PAGE_OPENED, page);
  //   } else {
  //     publishInteraction({
  //       name: interactionName,
  //       payload: page,
  //     });
  //     mercurePublish.guide(InteractionName.DYNAMIC_PAGE_OPENED, page);
  //   }

  //   if (!oldPageFound) {
  //     _slidePosition.value = document?.getElementsByClassName('sw-gs-layout')?.item(0)?.scrollTop;
  //   }

  //   if (handleScrollingAndNavigation) {
  //     resetSlidePosition(true);
  //   }
  // };

  // close dynamic page and inform others about it
  const requestCloseSpecificDynamicPage = async (
    payload:
      | Pick<DynamicProductPage, 'id' | 'type'>
      | Pick<DynamicLikeListPage, 'id' | 'type'>
      | Pick<DynamicProductListingPage, 'id' | 'type'>,
  ) => {
    if (isSelfService.value) {
      mercurePublish.local(MercureEvent.DYNAMIC_PAGE_CLOSED, payload);
    } else {
      mercurePublish.guide(MercureEvent.DYNAMIC_PAGE_CLOSED, payload);
      await publishInteraction({
        name: DynamicInteractionName.DYNAMIC_PAGE_CLOSED,
      });
    }

    // // TODO: clean product infos

    // if (_storeLastDynamicPage.value) {
    //   await openDynamicPage(_storeLastDynamicPage.value);

    //   // reset last dynamic page
    //   _storeLastDynamicPage.value = null;
    // }

    // resetSlidePosition();
  };

  const requestCloseAllDynamicPages = async () => {
    if (isSelfService.value) {
      mercurePublish.local(MercureEvent.ALL_DYNAMIC_PAGE_CLOSED);
    } else {
      mercurePublish.guide(MercureEvent.ALL_DYNAMIC_PAGE_CLOSED);
      await publishInteraction({
        name: DynamicInteractionName.DYNAMIC_PAGE_CLOSED,
      });
    }
  };

  const resetSlidePosition = (toTop = false) => {
    let position = _slidePosition.value;
    if (toTop) position = 0;

    try {
      document?.getElementsByClassName('sw-gs-layout')?.item(0)?.scrollTo(0, position);
    } catch (e) {}
  };

  return {
    slidePosition: computed(() => _slidePosition.value),
    resetSlidePosition,
    dynamicCmsPages: computed(() => dynamicCmsPages.value),
    // dynamicPageProductId,
    closeAllDynamicCmsPages,
    appendDynamicCmsPage,
    openDynamicProductPage,
    openDynamicLikeListPage,
    closeSpecificDynamicCmsPage,
    loading: computed(() => loading.value),
    requestDynamicPage,
    requestCloseSpecificDynamicPage,
    requestCloseAllDynamicPages,
  };
};

/**
 * Composable to manage dynamic page
 * @public
 * @category Presentation
 */
export const useDynamicPage = createSharedComposable(_useDynamicPage);
