import { addPromotionCode } from '@shopware-pwa/api-client';
import { Cart, LineItem, CartErrors, ClientApiError } from '@shopware-pwa/types';
import { CalculatedTax } from '@shopware-pwa/types/shopware-6-client/models/system/tax/CalculatedTax';
import { UserType } from './useUser';
import { ActionName } from '~/logic/interfaces';
import { MinimumLineItem } from '~/logic/api-client/services/cartService';
import { CartService, buildRequestConfig } from '~/logic/api-client';

/**
 * Composable to manage last attendee change their cart
 * @public
 * @category Cart & Checkout
 */
export type LastAttendeeChangeCart = {
  /**
   * User type
   */
  type: UserType;

  /**
   * Changed by which attendee
   */
  changeBy: string;

  /**
   * A cart item
   */
  item: MinimumLineItem;
};

/**
 * Composable to manage cart
 *
 * @public
 * @category Cart & Checkout
 */
export type UseCartReturn = {
  /**
   * Add product by id and quantity
   * @function
   * @param params Parameters to add product
   */
  addProduct(params: { id: string; quantity?: number }): Promise<Cart | undefined>;

  /**
   * Adds a promotion code to the cart
   * @function
   * @param promotionCode Promotion code
   */
  addPromotionCode(promotionCode: string): Promise<Cart>;

  /**
   * Lists all applied and active promotion codes
   */
  appliedPromotionCodes: ComputedRef<LineItem[]>;

  /**
   * Total discount to the cart
   */
  totalDiscount: ComputedRef<number | undefined>;

  /**
   * Current Cart object
   */
  cart: ComputedRef<Cart | undefined>;

  /**
   * Current Cart statistics
   */
  cartStatistics: ComputedRef<{ [k: string]: number }>;

  /**
   * Set Cart statistics
   * @function
   * @param productId Product ID
   * @param quantity Number of product in cart
   */
  setCartStatistics: (productId: string, quantity: number) => void;

  /**
   * All items in the cart
   */
  cartItems: ComputedRef<LineItem[]>;

  /**
   * Changes the quantity of a product in the cart
   * @function
   * @param params Parameters to change product quantity
   */
  changeProductQuantity(params: { id: string; quantity: number; changeAmount?: number }): void;

  /**
   * The number of items in the cart
   */
  count: ComputedRef<number>;

  /**
   * Refreshes the cart object and related data
   * If @param newCart is provided, it will be used as a new cart object
   */
  refreshCart(newCart?: Cart): Promise<Cart | undefined>;

  /**
   * Get all the cart items of all participants in an appointment.
   * @param order Order ID
   * @param sort Sort by
   */
  getAllCarts(order?: string, sort?: string): Promise<Cart | undefined>;

  /**
   * Resets the cart and associated errors to their initial state.
   * @function
   * @returns {void}
   */
  // resetCart(): void;

  /**
   * Removes the provided LineItem from the cart
   * @function
   * @param lineItem Line item which will be removed
   */
  removeItem(lineItem: LineItem): Promise<void>;

  /**
   * The filter of guide all carts
   */
  allCartsFilter: ComputedRef<{ order: string; sort: string }>;

  /*
   * The total price of the cart (including calculated costs like shipping)
   */
  totalPrice: ComputedRef<number>;

  /**
   * Shipping price
   */
  shippingTotal: ComputedRef<number>;

  /**
   * api Error
   */
  apiError: Ref<ClientApiError | null>;

  /**
   * Cart errors
   */
  cartErrors: Ref<CartErrors | null>;

  /**
   * The total price of all cart items
   */
  subtotal: ComputedRef<number>;

  /**
   * The net price of all cart items
   */
  netPrice: ComputedRef<number>;

  /**
   * The total tax of all cart items
   */
  tax: ComputedRef<number>;

  /**
   * The calculated taxes of all cart items
   */
  calculatedTaxes: ComputedRef<CalculatedTax[]>;

  /**
   * `true` if the cart contains no items
   */
  isEmpty: ComputedRef<boolean>;

  /**
   * `true` if cart contains only digital items
   */
  isVirtualCart: ComputedRef<boolean>;

  /**
   * `true` if cart contains at least a digital item
   */
  hasVirtualCartItem: ComputedRef<boolean>;
  /**
   * Get cart errors
   * @function
   */
  consumeCartErrors(): CartErrors;

  /**
   * Request Cart permission Token
   * @function
   * @param attendeeId Attendee ID
   */
  requestTokenPermission(attendeeId?: string): void;

  /**
   * `true` if apply net price (net, tax-free)
   */
  isNetPrice: ComputedRef<boolean>;

  /**
   * Loading state of Cart
   */
  isCartLoading: Ref<boolean>;
};

/**
 * Cart management logic. Copy all logic from `coreUseCart` and add additional logic for guide.
 */
const _useCart = (): UseCartReturn => {
  const { apiInstance } = useShopwareContext();
  const { adminApiInstance } = useAdminApiInstance();
  const { getContextTokenForCurrentClient, controlledClient, self } = useParticipants();
  const { appointment, appointmentId } = useAppointment();
  const { setAbortActions, abortAction, getActionController } = useShopwareContext();
  const { mercurePublish } = useMercure();
  const { isGuide, isClient } = useUser();

  setAbortActions([
    ActionName.LOAD_CART,
    ActionName.ADD_TO_CART,
    ActionName.REMOVE_FROM_CART,
    ActionName.UPDATE_PRODUCT_QUANTITY,
  ]);

  const _storeCart = ref<Cart | undefined>();
  const _storeCartStatistics = ref<{ [k: string]: number }>({});
  const _storeCartApiErrors = ref<ClientApiError | null>(null);
  const _storeCartErrors = ref<CartErrors | null>(null);
  const isCartLoading = ref<boolean>(false);
  const _allCartsFilter = ref({
    order: 'quantity',
    sort: 'desc',
  });

  const setCartStatistics = (productId: string, quantity: number) => {
    if (!isGuide.value) return;

    const currentQuantity = _storeCartStatistics.value[productId] || 0;
    _storeCartStatistics.value[productId] = currentQuantity + quantity;
  };

  const refreshCart = async (newCart?: Cart): Promise<Cart | undefined> => {
    _storeCartApiErrors.value = null;
    isCartLoading.value = true;
    if (newCart) {
      _storeCart.value = newCart;
      return newCart;
    }
    abortAction(ActionName.LOAD_CART);
    try {
      if (isGuide.value) {
        if (!controlledClient.value) return;
        const token = await getContextTokenForCurrentClient();
        _storeCart.value = await CartService.Guide.getCart(
          {
            salesChannelId: appointment.value?.salesChannelId ?? '',
          },
          adminApiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.LOAD_CART),
            headers: {
              'sw-context-token': token ?? '',
            },
          }),
        );
      } else {
        const result = await CartService.Client.getCart(
          apiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.LOAD_CART),
          }),
        );
        _storeCart.value = result;
        setCartErrors(result);
      }
    } catch (e) {
      _storeCart.value = undefined;
      _storeCartApiErrors.value = e as ClientApiError;
      throw e;
    } finally {
      isCartLoading.value = false;
    }
  };

  const getAllCarts = async (
    order: string = _allCartsFilter.value.order,
    sort: string = _allCartsFilter.value.sort,
  ) => {
    _allCartsFilter.value = {
      order,
      sort,
    };
    _storeCartApiErrors.value = null;
    isCartLoading.value = true;
    if (!isGuide.value) return;
    abortAction(ActionName.LOAD_CART);
    try {
      _storeCart.value = await CartService.Guide.getAllCarts(
        {
          appointmentId: appointmentId.value ?? '',
        },
        adminApiInstance,
        buildRequestConfig({
          controller: getActionController(ActionName.LOAD_CART),
          params: _allCartsFilter.value,
        }),
      );
      _storeCartStatistics.value = _storeCart.value.lineItems.reduce(
        (sum: { [k: string]: number }, x: LineItem) => {
          if (!x.referencedId) return sum;
          sum[x.referencedId] = x.quantity;
          return sum;
        },
        {},
      );
      return _storeCart.value;
    } catch (e) {
      _storeCart.value = undefined;
      _storeCartApiErrors.value = e as ClientApiError;
      throw e;
    } finally {
      isCartLoading.value = false;
    }
  };

  const requestTokenPermission = (attendeeId?: string) => {
    if (!attendeeId) return;
    mercurePublish.guide(MercureEvent.REQUESTED_TOKEN_PERMISSIONS, attendeeId);
  };

  const addProduct = async (params: MinimumLineItem): Promise<Cart | undefined> => {
    let result;
    abortAction(ActionName.ADD_TO_CART);
    try {
      if (isGuide.value && controlledClient.value) {
        const token = await getContextTokenForCurrentClient();
        if (!token) return;
        mercurePublish.guide(MercureEvent.LAST_ATTENDEE_CHANGE_CART, {
          type: UserType.GUIDE,
          changeBy: self.value?.attendeeId,
        });
        result = await CartService.Guide.addLineItems(
          {
            salesChannelId: appointment.value?.salesChannelId ?? '',
            itemsToAdd: [params],
          },
          adminApiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.ADD_TO_CART),
          }),
        );
      } else if (isClient.value) {
        mercurePublish.client(MercureEvent.LAST_ATTENDEE_CHANGE_CART, {
          type: UserType.CLIENT,
          changeBy: self.value?.attendeeId,
          item: params,
        });
        result = await CartService.Client.addLineItems(
          {
            itemsToAdd: [params],
          },
          apiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.ADD_TO_CART),
          }),
        );
      }
      _storeCart.value = result;
      if (result) setCartErrors(result);
      return result;
    } catch (e) {
      _storeCart.value = undefined;
      _storeCartApiErrors.value = e as ClientApiError;
      throw e;
    }
  };

  const removeItem = async (lineItem: LineItem) => {
    abortAction(ActionName.REMOVE_FROM_CART);
    let result;
    try {
      if (isGuide.value && controlledClient.value) {
        const token = await getContextTokenForCurrentClient();
        if (!token) return;
        mercurePublish.guide(MercureEvent.LAST_ATTENDEE_CHANGE_CART, {
          type: UserType.GUIDE,
          changeBy: self.value?.attendeeId,
        });
        result = await CartService.Guide.removeLineItems(
          {
            salesChannelId: appointment.value?.salesChannelId ?? '',
            itemsToRemove: [
              {
                id: lineItem.id,
                quantity: lineItem.quantity,
              },
            ],
          },
          adminApiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.REMOVE_FROM_CART),
          }),
        );
      } else if (isClient.value) {
        mercurePublish.client(MercureEvent.LAST_ATTENDEE_CHANGE_CART, {
          type: UserType.CLIENT,
          changeBy: self.value?.attendeeId,
          item: {
            id: lineItem.id,
            quantity: -lineItem.quantity,
          },
        });
        result = await CartService.Client.removeLineItems(
          {
            itemsToRemove: [
              {
                id: lineItem.id,
                quantity: lineItem.quantity,
              },
            ],
          },
          apiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.REMOVE_FROM_CART),
          }),
        );
      }

      _storeCart.value = result;
      if (result) setCartErrors(result);
    } catch (e) {
      _storeCart.value = undefined;
      _storeCartApiErrors.value = e as ClientApiError;
      throw e;
    }
  };

  const changeProductQuantity = async (params: {
    id: string;
    quantity: number;
    changeAmount?: number;
  }) => {
    let result;
    abortAction(ActionName.UPDATE_PRODUCT_QUANTITY);
    try {
      if (isGuide.value && controlledClient.value) {
        const token = await getContextTokenForCurrentClient();
        if (!token) return;
        mercurePublish.guide(MercureEvent.LAST_ATTENDEE_CHANGE_CART, {
          type: UserType.GUIDE,
          changeBy: self.value?.attendeeId,
        });
        result = await CartService.Guide.changeLineItemQuantities(
          {
            salesChannelId: appointment.value?.salesChannelId ?? '',
            itemsToUpdate: [params],
          },
          adminApiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.UPDATE_PRODUCT_QUANTITY),
          }),
        );
      } else if (isClient.value) {
        mercurePublish.client(MercureEvent.LAST_ATTENDEE_CHANGE_CART, {
          type: UserType.CLIENT,
          changeBy: self.value?.attendeeId,
          item: {
            id: params.id,
            quantity: params.changeAmount,
          },
        });
        result = await CartService.Client.changeLineItemQuantities(
          {
            itemsToUpdate: [params],
          },
          apiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.UPDATE_PRODUCT_QUANTITY),
          }),
        );
      }
      _storeCart.value = result;
      if (result) setCartErrors(result);
    } catch (e) {
      _storeCart.value = undefined;
      _storeCartApiErrors.value = e as ClientApiError;
      throw e;
    }
  };

  const submitPromotionCode = async (promotionCode: string) => {
    const result = await addPromotionCode(promotionCode, apiInstance);
    _storeCart.value = result;
    setCartErrors(result);
    return result;
  };

  const appliedPromotionCodes = computed(() => {
    return cartItems.value.filter((cartItem: LineItem) => cartItem.type === 'promotion');
  });

  const totalDiscount = computed(() => {
    if (appliedPromotionCodes.value.length) {
      return appliedPromotionCodes.value.reduce(
        (total, item) => total + (item.price?.totalPrice || 0),
        0,
      );
    }
  });

  const cart: ComputedRef<Cart | undefined> = computed(() => _storeCart.value);

  const cartItems = computed(() => {
    return cart.value ? cart.value.lineItems || [] : [];
  });

  const count = computed(() => {
    return cartItems.value.reduce(
      (accumulator: number, lineItem: LineItem) =>
        lineItem.type === 'product' ? lineItem.quantity + accumulator : accumulator,
      0,
    );
  });

  const isEmpty = computed(() => count.value <= 0);

  const totalPrice = computed(() => {
    const cartPrice = cart.value && cart.value.price && cart.value.price.totalPrice;
    return cartPrice || 0;
  });

  const shippingTotal = computed(() => {
    const shippingTotal = cart.value?.deliveries?.[0]?.shippingCosts?.totalPrice;
    return shippingTotal || 0;
  });

  const tax = computed(() => {
    return (cart.value?.price?.calculatedTaxes ?? []).reduce((sum, item) => sum + item.tax, 0);
  });

  const calculatedTaxes = computed(() => {
    return cart.value?.price?.calculatedTaxes || [];
  });

  const subtotal = computed(() => {
    const cartPrice = cart.value?.price?.positionPrice;
    return cartPrice || 0;
  });

  const netPrice = computed(() => {
    const priceTemp = cart.value?.price?.netPrice;
    return priceTemp || 0;
  });

  const isNetPrice = computed(() => {
    return ['net', 'tax-free'].includes(cart.value?.price?.taxStatus as string);
  });

  const isVirtualCart = computed(() => {
    return (
      cartItems.value.length > 0 &&
      cartItems.value
        .filter((element) => element.type !== 'promotion')
        .every((item) => item.states.includes('is-download'))
    );
  });

  const hasVirtualCartItem = computed(() => {
    return (
      cartItems.value.length > 0 &&
      cartItems.value
        .filter((element) => element.type !== 'promotion')
        .some((item) => item.states.includes('is-download'))
    );
  });

  /**
   * Add cart errors to the sharable variable
   *
   * @param {Cart} cart
   */
  const setCartErrors = (cart: Cart) => {
    // if (Object.keys(cart.errors).length) {
    //   _storeCartErrors.value = Object.assign(
    //     _storeCartErrors.value ? _storeCartErrors.value : {},
    //     cart.errors,
    //   );
    // }
    // set value instead of append
    _storeCartErrors.value = { ...cart.errors };
  };

  /**
   * Get cart errors and clear variable
   *
   * @returns {CartErrors}
   */
  const consumeCartErrors = () => {
    const errors = _storeCartErrors.value
      ? JSON.parse(JSON.stringify(_storeCartErrors.value))
      : null;
    _storeCartErrors.value = null;
    return errors;
  };

  // const resetCart = () => {
  //   _storeCart.value = undefined;
  //   _storeCartErrors.value = null;
  // };

  return {
    apiError: computed(() => _storeCartApiErrors.value),
    cartErrors: computed(() => _storeCartErrors.value),
    allCartsFilter: computed(() => _allCartsFilter.value),
    cartStatistics: computed(() => _storeCartStatistics.value),
    setCartStatistics,
    refreshCart,
    addProduct,
    addPromotionCode: submitPromotionCode,
    appliedPromotionCodes,
    totalDiscount,
    cart,
    cartItems,
    changeProductQuantity,
    count,
    removeItem,
    totalPrice,
    shippingTotal,
    subtotal,
    tax,
    netPrice,
    isNetPrice,
    isEmpty,
    calculatedTaxes,
    isVirtualCart,
    hasVirtualCartItem,
    consumeCartErrors,
    getAllCarts,
    requestTokenPermission,
    isCartLoading: computed(() => isCartLoading.value),
    // resetCart,
  };
};

/**
 * Composable to manage cart
 * @public
 * @category Cart & Checkout
 */
export const useCart = createSharedComposable(_useCart);
