// import {
//   useSharedState,
//   getApplicationContext,
//   useIntercept,
// } from "@shopware-pwa/composables";
// import { AppointmentService } from "../api-client";
import { ClientApiError } from '@shopware-pwa/types';
// import { useErrorHandler } from "./useErrorHandler";
// import { computed } from "@vue/composition-api";
// import { useUser } from "@shopware-pwa/composables";
import { AppointmentService } from '~/logic/api-client';
import { getErrorMessages } from '~/helpers/errors';
import { AppointmentModel, VideoAudioSettingsType } from '~/logic/interfaces';
// import {useState} from "./useState";

/**
 * The appointment status
 *
 * @public
 * @category Appointment
 */
export enum AppointmentStatus {
  INITIAL,
  RUNNING,
  ENDED,
}

/**
 * Composable to manage appointment
 *
 * @category Appointment
 */
const _useAppointment = () => {
  const nuxtApp = useNuxtApp();
  const appointment = ref<AppointmentModel | null>();
  const appointmentId = ref<string>();
  const { stateForAll } = usePresentationState();
  const { createToast } = useToasts();

  // const contextName = COMPOSABLE_NAME;
  // const { apiInstance, cookies } = getApplicationContext({ contextName });
  const { apiInstance } = useShopwareContext();
  const { adminApiInstance } = useAdminApiInstance();
  // const { sharedRef } = useSharedState();
  const { user, isLoggedIn, setAttendeeId, cookieGuestAttendeeName, isGuide } = useUser();
  // const { broadcast } = useIntercept();
  // const joinedAppointment = sharedRef<boolean>(
  //   `${contextName}-joinedAppointment`
  // );
  const isSelfService = computed(() => stateForAll.value?.appointmentMode === 'self');
  const isPreview = computed(() => appointment.value?.isPreview);
  const mediaShouldBeUsed = ref<boolean>();
  // const appointmentIsRunning = computed(() => stateForAll.value?.running);

  // const _attendeeName = sharedRef<string>(`${contextName}-attendeeName`);
  const joinError = ref<ClientApiError | undefined>();
  // const joinErrorAsGuide = sharedRef<boolean>(
  //   `${contextName}-joinErrorAsGuide`
  // );
  // const selfServiceReconnectTime: number = 10000;
  // const _appointmentStarted = sharedRef<boolean>(
  //   `${contextName}-appointmentStarted`,
  //   false
  // );
  // const _attendeeName = ref<string>('');
  // const _appointmentStarted = ref<boolean>(false);

  const setAppointmentId = (id: string) => {
    appointmentId.value = id;
  };

  const checkStatus = () => {
    if (!stateForAll.value?.started) return AppointmentStatus.INITIAL;
    else if (stateForAll.value?.ended) return AppointmentStatus.ENDED;
    else if (stateForAll.value?.running) return AppointmentStatus.RUNNING;
    else return AppointmentStatus.INITIAL;
  };

  const isAppointmentInitializing = computed(() => {
    if (stateForAll.value === undefined) return undefined;
    return checkStatus() === AppointmentStatus.INITIAL;
  });

  const isAppointmentRunning = computed(() => {
    if (stateForAll.value === undefined) return undefined;
    return checkStatus() === AppointmentStatus.RUNNING;
  });

  const isAppointmentEnded = computed(() => {
    if (stateForAll.value === undefined) return undefined;
    return checkStatus() === AppointmentStatus.ENDED;
  });

  const getVideoAndAudioSetting =
    async (): Promise<AppointmentService.AppointmentVideoAndAudioSetting | void> => {
      if (!appointmentId.value) return;
      try {
        const setting = await AppointmentService.getAppointmentVideoAndAudioSetting(
          appointmentId.value,
          apiInstance,
        );
        mediaShouldBeUsed.value = setting.videoAudioSettings !== VideoAudioSettingsType.NONE;
      } catch (e) {
        const message = getErrorMessages(e as ClientApiError) as string;
        createToast({ content: message, variant: 'error' });
      }
    };

  // const storedUserName = useCookie(ATTENDEE_NAME_COOKIE_NAME, {
  //   maxAge: ATTENDEE_NAME_COOKIE_LIFESPAN,
  //   sameSite: 'lax',
  //   path: ATTENDEE_NAME_COOKIE_PATH,
  // });

  // const getAttendeeName = () => {
  //   if (user.value && user.value.firstName && user.value.lastName) {
  //     return `${user.value.firstName} ${user.value.lastName}`;
  //   }

  //   return storedUserName.value || '';
  // };

  const start = async () => {
    if (!appointmentId.value || !isGuide.value) return;
    try {
      await AppointmentService.startAppointment(appointmentId.value!, adminApiInstance);
      // _appointmentStarted.value = true;
    } catch (e) {
      const message = getErrorMessages(e as ClientApiError) as string;
      createToast({ content: message, variant: 'error' });
    }
  };

  const end = async () => {
    if (!appointmentId.value || !isGuide.value) return;
    try {
      await AppointmentService.endAppointment(appointmentId.value!, adminApiInstance);
      createToast({
        content: 'presentation.guideTool.presentationHasEnded',
        variant: 'info',
      });
    } catch (e) {
      const message = getErrorMessages(e as ClientApiError) as string;
      createToast({ content: message, variant: 'error' });
    }
  };

  // const resetAppointmentValues = (onlyErrors = false) => {
  //   // joinError.value = null;
  //   // joinErrorAsGuide.value = false;

  //   if (!onlyErrors) {
  //     appointmentId.value = '';
  //     // joinedAppointment.value = false;
  //   }
  // };

  const join = async () => {
    if (!appointmentId.value) return;
    try {
      if (isGuide.value) {
        appointment.value = await AppointmentService.joinAsGuide(
          appointmentId.value,
          adminApiInstance,
        );
        if (!appointment.value) return;
        // set id new because it could have been a path and the id is needed for further actions
        appointmentId.value = appointment.value.id;
        nuxtApp.$contextToken.value = appointment.value.newContextToken;

        apiInstance.config.contextToken = appointment.value.newContextToken;
        apiInstance.update();
      } else {
        let name = '';
        if (isLoggedIn.value) {
          name = [user.value?.firstName, user.value?.lastName].filter(Boolean).join(' ');
        } else {
          name = cookieGuestAttendeeName.value ?? '';
        }
        appointment.value = await AppointmentService.joinAsClient(
          appointmentId.value,
          name,
          apiInstance,
        );
      }

      setAttendeeId(appointment.value.attendeeId);

      joinError.value = undefined;

      // joinedAppointment.value = true;
      // resetAppointmentValues(true);
    } catch (e) {
      const error = e as ClientApiError;
      joinError.value = error;
      throw error;
      // joinErrorAsGuide.value = guide;

      // try to reconnect client if "not accessible" error occured in self service
      // const error = e as ClientApiError;
      // if (error.statusCode && error.statusCode === 307) {
      //   // setTimeout(async () => {
      //   //   await join(false);
      //   // }, selfServiceReconnectTime);

      //   return;
      // }

      // handleError(error, {
      //   context: contextName,
      //   method: 'join',
      //   push: true,
      //   asGuide: true,
      // });

      // broadcast("useAppointment-join-error", e);
    }
  };

  // async function joinAsGuide() {
  //   return await join(true);
  // }

  // async function joinAsClient() {
  //   return await join(false);
  // }

  return {
    start,
    end,
    join,
    // joinAsGuide,
    // joinAsClient,
    joinedAppointment: computed(() => !!appointment.value),
    isSelfService,
    appointment,
    appointmentId: computed(() => appointmentId.value),
    setAppointmentId,
    joinError,
    // attendeeName: computed(() => _attendeeName.value),
    // appointmentStarted: computed(() => _appointmentStarted.value),
    // joinError,
    // joinErrorAsGuide,
    // resetAppointmentValues,
    isPreview,
    isAppointmentInitializing,
    isAppointmentRunning,
    isAppointmentEnded,
    mediaShouldBeUsed: computed(() => mediaShouldBeUsed.value),
    getVideoAndAudioSetting,
  };
};

/**
 * Composable to manage appointment
 *
 * @public
 * @category Appointment
 */
export const useAppointment = createSharedComposable(_useAppointment);
