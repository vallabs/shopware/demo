import { LikeListService } from '~/logic/api-client';
// import { useAdminApiInstance } from './admin/useAdminApiInstance';
// import { LikeListService, buildRequestConfig } from '~/logic/api-client';

/**
 * Composable to manage bookmark item
 *
 * @public
 * @category Bookmark List
 */
export type UseBookmarkItemReturn = {
  /**
   * is product bookmarked
   */
  isBookmarked: ComputedRef<Boolean>;

  /**
   * Set product id
   * @function
   * @param id Product ID
   */
  setProductId(id: string): void;

  /**
   * Add product to bookmark list
   * @function
   */
  addBookmark(): Promise<void>;

  /**
   * Remove a product in bookmark list
   * @function
   */
  removeBookmark(): Promise<void>;

  /**
   * Toggle a product in bookmark list
   * @function
   */
  toggleBookmark(): Promise<void>;

  /**
   * Loading state when perform action for bookmark
   */
  loading: ComputedRef<Boolean>;
};

/**
 * Composable to manage bookmark item
 *
 * @public
 * @category Bookmark List
 */
export const useBookmarkItem = (
  productIdParams?: Ref<string> | ComputedRef<string>,
): UseBookmarkItemReturn => {
  const productId = ref();

  if (productIdParams) {
    syncRefs(productIdParams, productId);
  }

  const { bookmarkIds, loadBookmarkIds } = useBookmark();
  // const { adminApiInstance } = useAdminApiInstance();
  // const { mercurePublish } = useMercure();
  const { apiInstance } = useShopwareContext();
  const loading = ref();
  // const { controlledClient, self } = useParticipants();
  // const { isGuide, isClient } = useUser();
  // const { publishInteraction } = useInteractions();
  // const _storeLikeListErrors = ref<ClientApiError | null>(null);

  const isBookmarked = computed(() => {
    return bookmarkIds.value.get(productId.value) || false;
  });

  const setProductId = (id: string) => {
    productId.value = id;
  };

  const toggleBookmark = async (): Promise<any> => {
    if (isBookmarked.value) {
      await removeBookmark();
    } else {
      await addBookmark();
    }
  };

  const removeBookmark = async () => {
    loading.value = true;
    try {
      await LikeListService.unlikeProduct(productId.value, apiInstance);
      await loadBookmarkIds();
    } catch (e) {}
    loading.value = false;
  };

  const addBookmark = async () => {
    loading.value = true;
    try {
      await LikeListService.likeProduct(productId.value, apiInstance);
      await loadBookmarkIds();
    } catch (e) {}
    loading.value = false;
  };

  return {
    setProductId,
    toggleBookmark,
    isBookmarked,
    removeBookmark,
    addBookmark,
    loading: computed(() => loading.value),
  };
};
