import { useStorage } from '@vueuse/core';
import { ProductsService, buildRequestConfig } from '~/logic/api-client';
import { ActionName, SlideItem } from '~/logic/interfaces';

/**
 * Composable to manage product listing
 *
 * @category Presentation
 */
const _useProductListing = () => {
  const { slides, loadSlide, updatePickedProductIds } = usePresentation();
  const mode = ref<'create' | 'edit'>();
  const { mercurePublish } = useMercure();
  const { appointmentId } = useAppointment();
  const { getActionController, setAbortActions, abortAction } = useShopwareContext();
  const { adminApiInstance } = useAdminApiInstance();

  const productListingStorage = useStorage<{
    [key: string]: {
      add: string[];
      remove: string[];
    };
  }>(`product-listing-storage`, {}, sessionStorage);

  setAbortActions([ActionName.UPDATE_LISTING]);

  const currentProductListingIndex = ref<number>();
  const currentProductListingSlide = computed<SlideItem>(
    () => slides.value?.[currentProductListingIndex.value!],
  );
  const currentListingKey = computed(
    () => currentProductListingSlide.value?.cmsPageResult?.extensions.cmsPageRelation.id || 'new',
  );

  const currentPickedProductIds = computed(() => {
    return (
      currentProductListingSlide.value?.cmsPageResult?.extensions?.cmsPageRelation
        ?.pickedProductIds ?? []
    ).reduce((acc: { [k: string]: boolean }, curr: string) => {
      acc[curr] = true;
      return acc;
    }, {});
  });

  const createProductListing = async (payload: {
    productIds: string[];
    name: string;
    currentPageGroupId: string | null;
    goToNewSlide: boolean;
  }) => {
    try {
      const response = await ProductsService.saveNewInstantListing(
        {
          productIds: payload.productIds,
          currentPageGroupId: payload.currentPageGroupId,
          appointmentIdToSync: appointmentId.value!,
          pageName: payload.name,
        },
        adminApiInstance,
      );

      delete productListingStorage.value?.new;

      const slideIndex = response.index;

      const mercurePayload = {
        slideIndex,
        goToNewSlide: payload.goToNewSlide,
      };

      mercurePublish.guide(MercureEvent.NEW_PRODUCT_LISTING_CREATED, mercurePayload);
    } catch {
      // handle error
    }
  };

  const updateProductListing = async (params: {
    removeProductIds: string[];
    addProductIds: string[];
  }) => {
    if (!currentProductListingSlide.value || !currentProductListingSlide.value.cmsPageResult) {
      return;
    }

    try {
      abortAction(ActionName.UPDATE_LISTING);

      const res = await ProductsService.saveInstantListingUpdate(
        {
          removeProductIds: params.removeProductIds,
          addProductIds: params.addProductIds,
          currentPageGroupId: currentProductListingSlide.value.groupId,
          appointmentIdToSync: appointmentId.value!,
          pageName: currentProductListingSlide.value.groupName,
        },
        adminApiInstance,
        buildRequestConfig({
          controller: getActionController(ActionName.UPDATE_LISTING),
        }),
      );

      delete productListingStorage.value?.[currentListingKey.value];

      updatePickedProductIds(currentProductListingIndex.value!, res.pickedProductIds);

      mercurePublish.guide(
        MercureEvent.PRODUCT_LISTING_UPDATED,
        currentListingKey.value,
        ActionName.UPDATE_LISTING,
      );
    } catch {
    } finally {
      // generalLoading.value = false;
    }
  };

  const listingSlides = computed(() => {
    return slides.value.filter((slide) => slide.isInstantListing);
  });

  const editCurrentProductListing = async (slideIdx: number) => {
    mode.value = 'edit';
    await loadSlide(slideIdx, true);
    currentProductListingIndex.value = slideIdx;
  };

  const createCurrentProductListing = () => {
    mode.value = 'create';
    currentProductListingIndex.value = undefined;
  };

  const resetCurrentProductListing = () => {
    mode.value = undefined;
    currentProductListingIndex.value = undefined;
  };

  return {
    productListingStorage,
    listingSlides,
    editCurrentProductListing,
    createCurrentProductListing,
    currentProductListingSlide,
    resetCurrentProductListing,
    isEditMode: computed(() => mode.value === 'edit'),
    isCreateMode: computed(() => mode.value === 'create'),
    updateProductListing,
    createProductListing,
    currentListingKey,
    currentPickedProductIds,
  };
};

/**
 * Composable to manage product listing
 *
 * @public
 * @category Presentation
 */
export const useProductListing = createSharedComposable(_useProductListing);
