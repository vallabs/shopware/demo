import { MercureEvent } from './useMercure';
import { DynamicPage } from './useDynamicPage';
import { LastAttendeeChangeCart } from './useCart';
import { IClientChangedLike, IGuideChangedLike } from './useLikeItem';
import {
  //   InstantListingUpdateData,
  PresentationStateForAll,
  PresentationStateForClients,
  PresentationStateForGuides,
  PresentationStateForMe,
  ScrollToTarget,
} from '~/logic/interfaces';

/**
 * Composable to manage presentation state
 *
 * @category Presentation
 */
const _usePresentationState = () => {
  const highlightedElement = ref<string>();
  const lastGuideHoveredElement = ref<ScrollToTarget>();
  const lastClientHoveredElement = ref<{ [key: string]: ScrollToTarget }>({});
  const lastClientChangedLikes = ref<IClientChangedLike>();
  const lastGuideChangedLikes = ref<IGuideChangedLike>();
  // const likedProduct = ref<LikedProduct>();
  const lastChangedCart = ref<{ key: string }>();
  const lastAttendeeChangeCart = ref<LastAttendeeChangeCart | null>();
  const scrollToTarget = ref<ScrollToTarget>();
  const navigationPageIndex = ref<number>(0);
  const grantedTokenPermission = ref<string>();
  const openedDynamicPage = ref<DynamicPage | undefined>();
  const closedDynamicPage = ref<DynamicPage | undefined>();
  const closedAllDynamicPages = ref<{ value: boolean }>({ value: false });
  //   const newInstantListing = ref<boolean>(false);
  //   const instantListingUpdated = ref<InstantListingUpdateData>();
  const activeSpeaker = ref();
  const productListingLoadedMoreInfo = ref<{ type: string; page: number }>();
  // const currentLoadedInfo = ref<DynamicProductListingPage>();
  const stateForAll = ref<PresentationStateForAll>();
  const stateForGuides = ref<PresentationStateForGuides>();
  const stateForClients = ref<PresentationStateForClients>();
  const stateForMe = ref<PresentationStateForMe>();
  const newProductListingCreated = ref<{ slideIndex: number; goToNewSlide: boolean }>();
  const productListingUpdated = ref<{ groupId: string }>();

  const isTokenPermissionsRequestShown = ref(false);

  const hideTokenPermissionsRequest = () => {
    isTokenPermissionsRequestShown.value = false;
  };

  const updateState = (key: MercureEvent, state: any, local?: boolean) => {
    switch (key) {
      case MercureEvent.HIGHLIGHTED:
        highlightedElement.value = state;
        break;
      case MercureEvent.GUIDE_HOVERED:
        lastGuideHoveredElement.value = state;
        break;
      case MercureEvent.CLIENT_HOVERED:
        lastClientHoveredElement.value[state.publishedBy] = state;
        break;
      case MercureEvent.CLIENT_CHANGED_LIKES:
        lastClientChangedLikes.value = state;
        break;
      case MercureEvent.NEW_PRODUCT_LISTING_CREATED:
        newProductListingCreated.value = state;
        break;
      case MercureEvent.PRODUCT_LISTING_UPDATED:
        if (local) return;
        productListingUpdated.value = { groupId: state };
        break;
      // case MercureEvent.PRODUCT_WAS_LIKED:
      //   likedProduct.value = state;
      //   break;
      case MercureEvent.GUIDE_CHANGED_LIKES:
        lastGuideChangedLikes.value = state;
        break;
      case MercureEvent.CHANGED_CART:
        // this event fires from server when someone make the cart changes. but we need to know who did it, so LAST_ATTENDEE_CHANGE_CART will tell us about it.
        lastChangedCart.value = { key: state };
        break;
      case MercureEvent.LAST_ATTENDEE_CHANGE_CART:
        lastAttendeeChangeCart.value = state;
        break;
      case MercureEvent.DYNAMIC_PAGE_OPENED:
        openedDynamicPage.value = state;
        break;
      case MercureEvent.DYNAMIC_PAGE_CLOSED:
        closedDynamicPage.value = state;
        openedDynamicPage.value = undefined;
        break;
      case MercureEvent.ALL_DYNAMIC_PAGE_CLOSED:
        closedAllDynamicPages.value = { value: true };
        break;
      case MercureEvent.DYNAMIC_PRODUCT_LISTING_PAGE_LOADED_MORE:
        productListingLoadedMoreInfo.value = state;
        break;
      case MercureEvent.NAVIGATED:
        navigationPageIndex.value = state;
        highlightedElement.value = '';
        break;
      case MercureEvent.REQUESTED_TOKEN_PERMISSIONS:
        if (!stateForMe.value?.attendeeId) return false;
        if (state === stateForMe.value.attendeeId && !isTokenPermissionsRequestShown.value) {
          isTokenPermissionsRequestShown.value = true;
        }
        break;
      case MercureEvent.GRANTED_TOKEN_PERMISSION:
        grantedTokenPermission.value = state;
        break;
      case MercureEvent.STATE_FOR_ALL:
        stateForAll.value = state;
        break;
      case MercureEvent.STATE_FOR_GUIDES:
        stateForGuides.value = state;
        break;
      case MercureEvent.STATE_FOR_CLIENTS:
        stateForClients.value = state;
        break;
      case MercureEvent.STATE_FOR_ME:
        stateForMe.value = state;
        break;
      case MercureEvent.ACTIVE_SPEAKER_UPDATED:
        activeSpeaker.value = state;
        break;
      case MercureEvent.SCROLLED_TO:
        scrollToTarget.value = state;
        break;
      case MercureEvent.HEARTBEAT:
        break;
      default:
        console.warn(`Unknown event type: ${key}`);
    }
  };

  return {
    stateForAll: computed(() => stateForAll.value),
    stateForGuides: computed(() => stateForGuides.value),
    stateForClients: computed(() => stateForClients.value),
    stateForMe: computed(() => stateForMe.value),
    openedDynamicPage: computed(() => openedDynamicPage.value),
    closedDynamicPage: computed(() => closedDynamicPage.value),
    closedAllDynamicPages: computed(() => closedAllDynamicPages.value),
    lastGuideHoveredElement: computed(() => lastGuideHoveredElement.value),
    lastClientHoveredElement: computed(() => lastClientHoveredElement.value),
    navigationPageIndex: computed(() => navigationPageIndex.value),
    isTokenPermissionsRequestShown: computed(() => isTokenPermissionsRequestShown.value),
    lastChangedCart: computed(() => lastChangedCart.value),
    lastAttendeeChangeCart: computed(() => lastAttendeeChangeCart.value),
    grantedTokenPermission: computed(() => grantedTokenPermission.value),
    scrollToTarget: computed(() => scrollToTarget.value),
    lastGuideChangedLikes: computed(() => lastGuideChangedLikes.value),
    lastClientChangedLikes: computed(() => lastClientChangedLikes.value),
    productListingUpdated: computed(() => productListingUpdated.value),
    newProductListingCreated: computed(() => newProductListingCreated.value),
    productListingLoadedMoreInfo: computed(() => productListingLoadedMoreInfo.value),
    // likedProduct: computed(() => likedProduct.value),
    updateState,
    hideTokenPermissionsRequest,
  };
};

/**
 * Composable to manage presentation state
 *
 * @public
 * @category Presentation
 */
export const usePresentationState = createSharedComposable(_usePresentationState);
