import { Product, ClientApiError } from '@shopware-pwa/types';
import { ActionName } from '~/logic/interfaces';
import { LikeListService, buildRequestConfig } from '~/logic/api-client';

/**
 * Composable to manage liked product item
 *
 * @public
 * @category Wishlist
 */
export type LikedProduct = {
  /**
   * type liked product
   */
  type: string;

  /**
   * Attendee has liked product
   */
  attendee: string;

  /**
   * Product ID has been liked
   */
  productId: string;
};

/**
 * Composable to manage like list
 *
 * @public
 * @category Wishlist
 */
export type UseLikeListReturn = {
  /**
   * Load the like list and related data
   */
  loadLikeList(): Promise<Product[] | undefined>;

  /**
   * Reset like list
   */
  resetLikeList(): void;

  /**
   * All items in like list
   */
  likeList: ComputedRef<Product[] | []>;

  /**
   * Number of items in the cart
   */
  likeListCount: ComputedRef<number | undefined>;

  /**
   * Loading state of like list
   */
  isLikeListLoading: Ref<boolean>;

  /**
   * Get like list statistics of all participants in the appointment
   * @function
   * @param order Order ID
   * @param sort Sort by
   */
  getLikeListStatistics: (order?: string, sort?: string) => Promise<void>;

  /**
   * The filter of guide all like list
   */
  allLikeListFilter: ComputedRef<{ order: string; sort: string }>;
};

/**
 * Composable to manage like list
 *
 * @category Wishlist
 */
const _useLikeList = (): UseLikeListReturn => {
  const { adminApiInstance } = useAdminApiInstance();
  const { setAbortActions, abortAction, apiInstance, getActionController } = useShopwareContext();
  const { isGuide, isClient } = useUser();
  const { controlledClient } = useParticipants();
  const { appointmentId } = useAppointment();

  setAbortActions([ActionName.LOAD_LIKE_LIST]);

  const _storeLikeList = ref<Product[]>();
  const _storeLikeListErrors = ref<ClientApiError | null>(null);
  const likeList: ComputedRef<Product[]> = computed(() => _storeLikeList.value ?? []);
  const isLikeListLoading = ref<boolean>(false);

  const likeListCount = computed(() => likeList.value?.length);

  const resetLikeList = () => {
    _storeLikeList.value = undefined;
    _storeLikeListErrors.value = null;
  };

  const _allLikeListFilter = ref({
    order: 'quantity',
    sort: 'desc',
  });

  const loadLikeList = async (): Promise<Product[] | undefined> => {
    _storeLikeListErrors.value = null;
    abortAction(ActionName.LOAD_LIKE_LIST);
    isLikeListLoading.value = true;
    try {
      let data;
      if (isGuide.value) {
        if (!controlledClient.value) {
          _storeLikeList.value = undefined;
          return;
        }

        data = await LikeListService.getLikedProductsForAttendee(
          {
            searchCriteria: {
              rating: undefined,
            },
            attendeeId: controlledClient.value.attendeeId,
          },
          apiInstance,
          adminApiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.LOAD_LIKE_LIST),
          }),
        );
        _storeLikeList.value = data?.data || [];
      } else if (isClient.value) {
        data = await LikeListService.getLikedProducts(
          {
            rating: undefined,
          },
          apiInstance,
          buildRequestConfig({
            controller: getActionController(ActionName.LOAD_LIKE_LIST),
          }),
        );
      }
      _storeLikeList.value = data?.elements || [];
    } catch (e) {
      _storeLikeListErrors.value = e as ClientApiError;
    } finally {
      isLikeListLoading.value = false;
    }
  };

  const getLikeListStatistics = async (
    order: string = _allLikeListFilter.value.order,
    sort: string = _allLikeListFilter.value.sort,
  ) => {
    _allLikeListFilter.value = {
      order,
      sort,
    };
    isLikeListLoading.value = true;
    abortAction(ActionName.LOAD_LIKE_LIST);
    try {
      const data = await LikeListService.getAllLikedProducts(
        {
          appointmentId: appointmentId.value ?? '',
        },
        adminApiInstance,
        buildRequestConfig({
          controller: getActionController(ActionName.LOAD_LIKE_LIST),
          params: _allLikeListFilter.value,
        }),
      );
      _storeLikeList.value = Object.values(data?.products) || [];
    } catch (e) {
      _storeLikeListErrors.value = e as ClientApiError;
    } finally {
      isLikeListLoading.value = false;
    }
  };

  return {
    loadLikeList,
    likeList,
    resetLikeList,
    likeListCount,
    getLikeListStatistics,
    isLikeListLoading: computed(() => isLikeListLoading.value),
    allLikeListFilter: computed(() => _allLikeListFilter.value),
  };
};

/**
 * Composable to manage like list
 *
 * @public
 * @category Wishlist
 */
export const useLikeList = createSharedComposable(_useLikeList);
