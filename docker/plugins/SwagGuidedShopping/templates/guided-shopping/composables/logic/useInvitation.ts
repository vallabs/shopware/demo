import { InvitationAnswer, InvitationDetail } from '~/logic/interfaces';
import { AppointmentService } from '~/logic/api-client';

/**
 * Composable to manage invitation to appointment
 *
 * @category Appointment
 */
const _useInvitation = () => {
  const { apiInstance } = useShopwareContext();
  const { appointmentId } = useAppointment();

  const receiverAnswer = ref<InvitationAnswer>();
  const invitationDetail = ref<InvitationDetail>();

  const respondAppointmentInvitation = async (token?: string, answer?: InvitationAnswer) => {
    if (!appointmentId.value || !token || !answer) return;

    try {
      const res = await AppointmentService.respondAppointmentInvitation(
        appointmentId.value,
        { token, answer },
        apiInstance,
      );

      receiverAnswer.value = res?.answer;
      invitationDetail.value = res?.appointment;
    } catch {}
  };

  return {
    receiverAnswer: computed(() => receiverAnswer.value),
    invitationDetail: computed(() => invitationDetail.value),
    respondAppointmentInvitation,
  };
};

/**
 * Composable to manage invitation to appointment
 *
 * @public
 * @category Appointment
 */
export const useInvitation = createSharedComposable(_useInvitation);
