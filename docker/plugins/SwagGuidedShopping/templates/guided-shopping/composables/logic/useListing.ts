/* override from the core useListing */
import {
  useListing as coreUseListing,
  createListingComposable,
  ListingType,
} from '@shopware-pwa/composables-next';
import type { CustomListingType, UseListingReturn } from '@shopware-pwa/composables-next';
import { Product, ShopwareSearchParams } from '@shopware-pwa/types';
import { ProductsService, PresentationService, buildRequestConfig } from '~/logic/api-client';
import { PRODUCT_LIST_PAGE_SIZE } from '~/helpers/constants';
import { ActionName } from '~/logic/interfaces';

type BaseParams<T> = {
  listingType: T;
  categoryId?: string;
  defaultSearchCriteria?: Partial<ShopwareSearchParams>;
};

type PresentationListingParams = {
  pageId?: string;
  slideId?: string;
};

type ProductsFilteringParams = never;

type AdditionalParams<T> = T extends 'presentationListing'
  ? PresentationListingParams
  : T extends 'productsFiltering'
  ? ProductsFilteringParams
  : never;

/**
 * Composable to manage type of product listing
 *
 * @public
 * @category Presentation
 */
export type CustomizedProductListing = UseListingReturn<Product> & {
  /**
   * List product ids
   */
  allProductIds: ComputedRef<string[]>;
};

/**
 * Composable to manage product listing
 *
 * @public
 * @category Presentation
 */
export const useListing = <T extends CustomListingType>(
  params?: BaseParams<T>,
  options?: AdditionalParams<T>,
) => {
  const { apiInstance, abortAction, setAbortActions, getActionController } = useShopwareContext();

  if (params?.listingType === 'presentationListing') {
    const additionalParams = options as PresentationListingParams;
    const searchMethod = (searchCriteria: Partial<ShopwareSearchParams>) => {
      if (!additionalParams?.pageId || !additionalParams?.slideId) {
        throw new Error('[useListing][presentation] The layout does not exist.');
      }
      return PresentationService.getSlideProducts(
        {
          pageId: additionalParams?.pageId,
          slideId: additionalParams?.slideId,
          query: searchCriteria,
        },
        apiInstance,
      );
    };

    return createListingComposable<Product>({
      listingKey: params.listingType,
      searchMethod,
      searchDefaults: (params?.defaultSearchCriteria || {}) as ShopwareSearchParams,
    });
  } else if (
    params?.listingType &&
    ['productsFiltering', 'productsListing'].includes(params.listingType)
  ) {
    setAbortActions([ActionName.LOAD_PRODUCT_LIST, ActionName.LOAD_LISTING_PRODUCTS]);

    const allProductIds = ref<string[]>([]);

    let actionName: ActionName;
    if (params.listingType === 'productsFiltering') {
      actionName = ActionName.LOAD_PRODUCT_LIST;
    } else {
      actionName = ActionName.LOAD_LISTING_PRODUCTS;
    }

    const formatCriteria = (searchCriteria: Partial<ShopwareSearchParams>) => {
      return {
        limit: PRODUCT_LIST_PAGE_SIZE,
        interaction: true,
        loadVariants: true,
        ...searchCriteria,
        manufacturer: Array.isArray(searchCriteria.manufacturer)
          ? searchCriteria.manufacturer.join('|')
          : undefined,
        properties: Array.isArray(searchCriteria.properties)
          ? searchCriteria.properties.join('|')
          : undefined,
      } as ShopwareSearchParams;
    };

    const searchMethod = (searchCriteria: Partial<ShopwareSearchParams>) => {
      abortAction(actionName);

      return ProductsService.getAllProductsWithData(
        formatCriteria(searchCriteria),
        apiInstance,
        buildRequestConfig({ controller: getActionController(actionName) }),
      ).then((res) => {
        if (res?.extensions?.allIds) {
          allProductIds.value = res.extensions.allIds.data;
        }
        return res;
      });
    };

    if (params.listingType === 'productsListing') {
      return createListingComposable<Product>({
        listingKey: params.listingType,
        searchMethod,
        searchDefaults: (params?.defaultSearchCriteria || {}) as ShopwareSearchParams,
      });
    }

    return {
      ...createListingComposable<Product>({
        listingKey: params.listingType,
        searchMethod,
        searchDefaults: (params?.defaultSearchCriteria || {}) as ShopwareSearchParams,
      }),
      allProductIds: computed(() => allProductIds.value),
    } as CustomizedProductListing;
  } else {
    const coreFunctionality = coreUseListing(params as BaseParams<ListingType>);
    return coreFunctionality;
  }
};
