import { useUser as coreUseUser } from '@shopware-pwa/composables-next';
import { AttendeeData } from 'logic/interfaces';
import { COOKIE_LIFESPAN, COOKIE_PATH } from '@/helpers/constants';
import { AppointmentService } from '~/logic/api-client';

/**
 * Composable to manage enum of user type
 *
 * @category Account
 */
export enum UserType {
  GUIDE = 'GUIDE',
  CLIENT = 'CLIENT',
}

/**
 * Type of attendee info
 *
 * @category Account
 */
type AttendeeInfo = {
  /**
   * Attendee ID
   */
  id: string;

  /**
   * Attendee name
   */
  name: string;
};

/**
 * Composable to manage user
 *
 * @category Account
 */
const _useUser = () => {
  const type = ref<UserType | undefined>();
  const attendee = ref<AttendeeInfo>({
    id: '',
    name: '',
  });
  const coreFunctionality = coreUseUser();
  const isGuide = computed(() => type.value === UserType.GUIDE);
  const isClient = computed(() => type.value === UserType.CLIENT);
  const cookieGuestAttendeeName = useCookie<string | undefined>('sw-guest-attendee-name', {
    maxAge: COOKIE_LIFESPAN,
    sameSite: 'lax',
    path: COOKIE_PATH,
  });
  const { apiInstance } = useShopwareContext();

  const setCookieGuestAttendeeName = (name?: string) => {
    cookieGuestAttendeeName.value = name;
  };

  const setAttendeeId = (id: string) => {
    attendee.value.id = id;
  };

  const setUserType = (userType?: UserType) => {
    type.value = userType;
  };

  const setAttendeeName = (name: string) => {
    attendee.value.name = name;
    if (isClient.value && !coreFunctionality.isLoggedIn.value) {
      setCookieGuestAttendeeName(name);
    }
  };

  const updateAttendeeData = async (data: AttendeeData) => {
    try {
      await AppointmentService.updateAttendee(data, apiInstance);
      if (data.attendeeName) setAttendeeName(data.attendeeName);
    } catch (e) {
      // const error = e as ClientApiError;
    }
  };

  return {
    ...coreFunctionality,
    setAttendeeId,
    setAttendeeName,
    updateAttendeeData,
    cookieGuestAttendeeName: computed(() => cookieGuestAttendeeName.value),
    setCookieGuestAttendeeName,
    isGuide,
    isClient,
    setUserType,
    type: computed(() => type.value),
    attendee: computed(() => attendee.value),
  };
};

/**
 * Composable to manage enum of user type
 *
 * @public
 * @category Account
 */
export const useUser = createSharedComposable(_useUser);
