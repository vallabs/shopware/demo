/**
 * Composable to manage price return
 *
 * @public
 * @category Product
 */
export type UsePriceReturn = {
  /**
   * Format price i.e. (2) -> 2.00 $
   * @function
   * @param value Price
   * @param marked Price has marked by asterisk symbol or not
   */
  getFormattedPrice(value: number | string | undefined, marked?: boolean): string;

  /**
   * Update configuration
   * @function
   * @param params Parameters to update price
   */
  update(params: { localeCode: string | undefined; currencyCode: string }): void;
};

/**
 * Composable for getting formatted price
 * Set the default currency code and locale in order to format a price correctly
 *
 * @category Product
 * @param params Parameters to update price
 */
const _usePrice = (params?: {
  localeCode: string | undefined;
  currencyCode: string;
}): UsePriceReturn => {
  const { sessionContext } = useSessionContext();
  const currencyLocale = ref<string | undefined>();
  const currencyCode = ref<string>('');
  const { t } = useI18n();

  if (params) {
    currencyCode.value = params.currencyCode;
    currencyLocale.value = params.localeCode;
  }

  const update = (params: { localeCode?: string | undefined; currencyCode: string }) => {
    _setCurrencyCode(params.currencyCode);
    _setLocaleCode(
      params.localeCode ||
        currencyLocale.value ||
        (typeof navigator !== 'undefined' && navigator?.language) ||
        'en-US',
    );
  };

  const _setCurrencyCode = (code: string) => {
    currencyCode.value = code;
  };

  const _setLocaleCode = (locale: string) => {
    currencyLocale.value = locale;
  };

  /**
   * Format price (2) -> $ 2.00
   */
  const getFormattedPrice = (value: number | string | undefined, marked = false): string => {
    if (typeof value === 'undefined') {
      return '';
    }

    if (!currencyLocale.value) {
      return value.toString();
    }

    const formattedPrice = new Intl.NumberFormat(currencyLocale.value, {
      style: 'currency',
      currency: currencyCode.value,
    }).format(+value);

    return `${formattedPrice}${marked ? t('star') : ''}`;
  };

  watch(
    () => sessionContext.value?.currency,
    (newCurrency) => {
      if (newCurrency)
        update({
          // locale code is read only once on SSR because it's unavailable in the context
          currencyCode: newCurrency?.isoCode as string,
        });
    },
    {
      immediate: true,
    },
  );

  return {
    getFormattedPrice,
    update,
  };
};

/**
 * Composable for getting formatted price
 * Set the default currency code and locale in order to format a price correctly
 *
 * @public
 * @category Product
 * @param params Parameters to update price
 */
export const usePrice = createSharedComposable(_usePrice);
