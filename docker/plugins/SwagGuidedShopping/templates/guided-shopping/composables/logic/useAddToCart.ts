import { Product, Cart, LineItem } from '@shopware-pwa/types';

/**
 * The composable to manage a cart item
 *
 * @public
 * @category Cart & Checkout
 */
export type UseAddToCartReturn = {
  /**
   * Add to cart method
   * @type {function}
   */
  addToCart(quantity?: number): Promise<Cart | undefined>;

  /**
   * Returns product count in stock
   */
  getStock: ComputedRef<number | undefined>;

  /**
   * loading add to cart
   */
  loading: ComputedRef<boolean>;

  /**
   * Returns product count in available stock
   */
  getAvailableStock: ComputedRef<number | null>;

  /**
   * Flag if product is already in cart
   */
  isInCart: ComputedRef<boolean>;

  /**
   * Count of the product quantity already in the cart
   */
  count: ComputedRef<number>;

  /**
   * Count of the product quantity already in all cart of all attendees in current appointment
   */
  countAll: ComputedRef<number>;
};

/**
 * Composable to manage adding product to cart
 *
 * @public
 * @category Cart & Checkout
 * @param product Product
 */
export const useAddToCart = (product: Ref<Product>): UseAddToCartReturn => {
  const _product = computed(() => unref(product));
  const loading = ref(false);

  const { addProduct, cartStatistics, setCartStatistics, cartItems } = useCart();

  const addToCart = async (
    quantity = _product.value.minPurchase || 1,
  ): Promise<Cart | undefined> => {
    if (loading.value) return;
    loading.value = true;
    try {
      const addToCartResponse = await addProduct({
        id: _product.value.id,
        quantity,
      });
      setCartStatistics(_product.value.id, quantity);
      return addToCartResponse;
    } catch (e) {
      // TODO: handle error
    } finally {
      loading.value = false;
    }
  };

  const getStock = computed(() => _product.value?.stock);

  const getAvailableStock = computed(() => _product.value?.availableStock);

  const isInCart = computed(() =>
    cartItems.value.some((item: LineItem) => item.referencedId === _product.value?.id),
  );

  const countAll = computed(() => cartStatistics.value[_product.value?.id] || 0);

  return {
    addToCart,
    loading: computed(() => loading.value),
    getStock,
    getAvailableStock,
    isInCart,
    count: computed(
      () =>
        cartItems.value.find((item: LineItem) => item.referencedId === _product.value?.id)
          ?.quantity || 0,
    ),
    countAll,
  };
};
