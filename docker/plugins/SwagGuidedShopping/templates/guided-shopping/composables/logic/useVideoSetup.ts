import { MediaDevices } from '~/logic/interfaces';
// import { useErrorHandler } from "./useErrorHandler";
// import { useMediaDevices, DeviceList } from "./useMediaDevices";
// import { useState } from './useState';

/**
 * Constant saved settings storage key
 *
 * @public
 * @category Presentation
 */
export const SAVED_SETTINGS_STORAGE_KEY = 'sw-gs-call-saved-settings';

/**
 * Composable to video setup
 *
 * @category Presentation
 */
const _useVideoSetup = () => {
  const microphoneDeviceId = useSessionStorage<string | null>('microphoneDeviceId', null);
  const speakerDeviceId = useSessionStorage<string | null>('speakerDeviceId', null);
  const cameraDeviceId = useSessionStorage<string | null>('cameraDeviceId', null);
  const hasSavedSettings = useSessionStorage<boolean | null>('hasSavedSettings', null);
  const joinedWithAudioOnly = useSessionStorage<boolean>('joinedWithAudioOnly', false);

  // const { stateForAll } = useState();
  const {
    getMediaDevices,
    // setDevicesFromStorage,
    // mediaDeviceChanged,
    // microphoneDevice,
    // cameraDevice,
    mediaDevices,
    deviceError,
    handleDeviceError,
    getMediaStream,
    callEnabled,
    isVoiceCall,
    // isAudioOnly,
  } = useMediaDevices();
  // const { addWatcher } = useWatchers();
  // const _videoEl = ref<HTMLVideoElement>(null);
  // const _isSetupReady = ref<boolean>(false);
  // const _watchersStarted = ref<boolean>(false);
  // const _stopDeviceChange = ref<boolean>(false);
  const needSetup = computed(() => callEnabled.value && !hasSavedSettings.value);
  const stream = ref();
  const streamLoading = ref();

  const setHasSavedSettings = (v: boolean | null) => {
    hasSavedSettings.value = v;
  };

  /**
   * sets the initially selected devices in the storage
   */
  const applySelectedDevices = (params: {
    microphoneDeviceId?: string;
    speakerDeviceId?: string;
    cameraDeviceId?: string;
  }) => {
    microphoneDeviceId.value = params.microphoneDeviceId;
    speakerDeviceId.value = params.speakerDeviceId;
    cameraDeviceId.value = params.cameraDeviceId;
    hasSavedSettings.value = true;

    // setSavedSettings();
  };

  const setJoinedWithAudioOnly = (val: boolean) => {
    joinedWithAudioOnly.value = val;
    hasSavedSettings.value = val;
  };

  // const setSavedSettings = () => {
  //   hasSavedSettings.value = true;
  // };

  /**
   * init custom stream to request devices
   *
   * @param videoEl
   */
  const initSetup = async () => {
    try {
      // triggers browser permission request for default devices
      await onDeviceChange(
        {
          audio: mediaDevices.value?.audioInput[0]?.deviceId,
          video: mediaDevices.value?.videoInput[0]?.deviceId,
        },
        true,
      );

      // request devices again to get their full name
      const devices = await getMediaDevices(true);

      if (!devices) return;

      // if it would has been an permission error, the exception would be thrown a way earlier
      // if your cam is already used by f.e. zoom firefox on windows will deliver the devices without labels anyway but does NOT throw a "permission denied" exeption
      // so we make sure that there are labels, and if not we throw a custom exception
      ensureDeviceLabels(devices);

      // load from storage but use defaults, watchers are already started, its like choosing one with the dropdown
      // setDevicesFromStorage(devices);

      // _isSetupReady.value = true;
    } catch (err) {
      if (err === 'CAMERA_IN_USE' || err === 'MICROPHONE_IN_USE') {
        handleDeviceError(err);
      } else {
        handleDeviceError('NO_DEVICES_FOUND');
      }
    }
  };

  const ensureDeviceLabels = (devices: MediaDevices) => {
    if (isVoiceCall.value) return;
    devices.videoInput.forEach((info) => {
      if (!info?.label) throw 'CAMERA_IN_USE';
    });
    devices.audioInput.forEach((info) => {
      if (!info?.label) throw 'MICROPHONE_IN_USE';
    });
  };

  // const stopDeviceChanges = () => {
  //   _stopDeviceChange.value = true;
  // };

  /**
   * attach the stream to the video element
   * @param stream
   */
  // const gotMediaStream = (stream: any) => {
  //   try {
  //     stream.value = stream;
  //   } catch (e: any) {
  //     handleError(e, {
  //       context: contextName,
  //       method: 'gotMediaStream',
  //     });
  //   }
  // };

  /**
   * callback when devices change or get set up
   */
  const onDeviceChange = async (state: any, ensureException = false) => {
    if (deviceError.value || isVoiceCall.value) return;

    try {
      streamLoading.value = true;
      stream.value = await getMediaStream(state);
    } catch (err: any) {
      if (err.code === 0) handleDeviceError('NO_DEVICES_FOUND');
      if (ensureException) throw 'NO_DEVICES_FOUND';
    } finally {
      streamLoading.value = false;
    }
  };

  /**
   * registers a callback that listens to permission change
   * experimental feature, does not work in firefox
   */
  // const registerPermissionChangeEvent = async () => {
  //   try {
  //     const status = await navigator.permissions.query({ name: "camera" });
  //     status.addEventListener("change", () => {
  //       onDeviceChange({
  //         video: cameraDevice.value
  //       });
  //     }, { once: true });
  //   } catch (e) {
  //     // experimental feature, ignore if not available
  //   }
  // };

  // const startWatchers = async () => {
  //   if (_watchersStarted.value) return;
  //   _watchersStarted.value = true;

  //   registerPermissionChangeEvent();

  //   addWatcher(watch(mediaDeviceChanged, (state) => {
  //     if (_stopDeviceChange.value) return;
  //     if (!state) return;

  //     const deviceId = state.deviceId;
  //     if (!deviceId) return;

  //     const data = {
  //       audio: microphoneDevice.value,
  //       video: cameraDevice.value,
  //     };

  //     if (state.storageKey === 'camera-device') data.video = deviceId;
  //     if (state.storageKey === 'microphone-device') data.audio = deviceId;

  //     onDeviceChange(data);
  //   }));

  // };

  return {
    applySelectedDevices,
    hasSavedSettings: computed(() => hasSavedSettings.value),
    setHasSavedSettings,
    needSetup,
    initSetup,
    stream: computed(() => stream.value),
    streamLoading: computed(() => streamLoading.value),
    setJoinedWithAudioOnly,
    onDeviceChange,
    microphoneDeviceId: computed(() => microphoneDeviceId.value),
    speakerDeviceId: computed(() => speakerDeviceId.value),
    cameraDeviceId: computed(() => cameraDeviceId.value),
    joinedWithAudioOnly: computed(() => joinedWithAudioOnly.value),
    // startWatchers,
    // stopDeviceChanges,
    // isSetupReady: computed(() => _isSetupReady.value),
  };
};

/**
 * Composable to video setup
 *
 * @public
 * @category Presentation
 */
export const useVideoSetup = createSharedComposable(_useVideoSetup);
