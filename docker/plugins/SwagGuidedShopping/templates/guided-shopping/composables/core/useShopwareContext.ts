import { useShopwareContext as coreUseShopwareContext } from '@shopware-pwa/composables-next';
import { AxiosResponse, AxiosRequestConfig } from 'axios';

/**
 * Regex of API path exceptions
 */
const API_PATH_EXCEPTIONS = [
  /\/store-api\/account\/customer/,
  /\/store-api\/guided-shopping\/appointment\/.*\/join-/,
];

/**
 * Composable to test Regex
 *
 * @function
 * @param text Text will be tested regex
 */
const testRegex = (text: string) => {
  return API_PATH_EXCEPTIONS.every((regex) => {
    return !regex.test(text);
  });
};

/**
 * Composable to manage Shopware context
 */
const _useShopwareContext = () => {
  const coreFunctionality = coreUseShopwareContext();
  const abortControllers = ref<{ [key: string]: AbortController }>({});
  const xhrRequests = ref<{ [key: string]: XMLHttpRequest }>({});

  const setXHRRequests = (key: string, http: XMLHttpRequest) => {
    xhrRequests.value[key] = http;
  };

  coreFunctionality.apiInstance._axiosInstance.interceptors.request.use(
    (config: AxiosRequestConfig) => {
      return config;
    },
    (error) => Promise.reject(error),
  );

  coreFunctionality.apiInstance._axiosInstance.interceptors.response.use(
    (response: AxiosResponse) => {
      return response;
    },
    async (error: any) => {
      if ([401, 403].includes(error.response.status) && testRegex(error.config.url)) {
        const { refreshUser } = useUser();
        await refreshUser();
      }
      return Promise.reject(error);
    },
  );

  const newInterceptor = (
    coreFunctionality.apiInstance._axiosInstance.interceptors.response as any
  ).handlers.pop();

  (coreFunctionality.apiInstance._axiosInstance.interceptors.response as any).handlers = [
    newInterceptor,
    ...(coreFunctionality.apiInstance._axiosInstance.interceptors.response as any).handlers,
  ];

  const setAbortActions = (keys: string[]) => {
    keys.forEach((key) => {
      if (!abortControllers.value[key]) {
        abortControllers.value[key] = new AbortController();
      }
    });
  };

  const abortAction = (key: string) => {
    if (abortControllers.value[key]) {
      abortControllers.value[key].abort();
      abortControllers.value[key] = new AbortController();
      if (xhrRequests.value[key]) {
        xhrRequests.value[key].abort();
      }
      return abortControllers.value[key];
    }
  };

  const getActionController = (key: string) => {
    return abortControllers.value[key];
  };

  return {
    ...coreFunctionality,
    setXHRRequests,
    xhrRequests: computed(() => xhrRequests.value),
    setAbortActions,
    abortAction,
    getActionController,
  };
};

/**
 * Composable to manage Shopware context
 *
 * @public
 */
export const useShopwareContext = createSharedComposable(_useShopwareContext);
