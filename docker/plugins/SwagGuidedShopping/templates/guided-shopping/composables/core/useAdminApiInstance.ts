import { createInstance } from '@shopware-pwa/api-client';
import { ClientApiError } from '@shopware-pwa/types';
import { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import set from 'lodash/set';
import { AuthRequestData, AuthResponseData, ShopwareAdminApiInstance } from '~/logic/interfaces';
import { COOKIE_PATH, COOKIE_LIFESPAN } from '~/helpers/constants';
// import {useErrorHandler} from "./useErrorHandler";

// const COOKIE_NAME: string = "adminApiAuth";
// const PRE_EXPIRE_TOKEN_REFRESH_TIME = 10 * 1000; // 10s in ms to refresh token 10 seconds before it will be revoked

// let refreshPromise: Promise<any>;
// let resolveRefreshPromise: Function | null = null;
// let rejectRefreshPromise: Function | null = null;

/**
 * Auth settings type
 */
type AuthSettings = {
  expiresAt: number;
  accessToken: string;
  refreshToken: string;
};

/**
 * Composable to manage Admin API instance
 */
const _useAdminApiInstance = () => {
  const nuxtApp = useNuxtApp();
  // const { apiInstance, cookies } = getApplicationContext({ contextName });
  const { apiInstance } = useShopwareContext();
  const localePath = useLocalePath();
  const router = useRouter();
  const route = useRoute();
  // const { sharedRef } = useSharedState();
  // const expiresAt = ref<number>(0);
  // const isAuthenticated = ref<boolean>(false);
  // const refreshToken = ref<string>("");
  // const accessToken = ref<string>("");

  const _createCustomApiInstance = () => {
    const newApiInstance = createInstance(apiInstance.config);

    newApiInstance.onConfigChange(({ config }) => {
      try {
        languageId.value = config.languageId;
      } catch (e) {
        // Sometimes cookie is set on server after request is send, it can fail silently
      }
    });

    newApiInstance._axiosInstance.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        (
          config.headers as any
        ).Authorization = `Bearer ${adminApiInstanceCookie.value?.accessToken}`;
        (config.headers as any)['sw-context-token-alias'] = nuxtApp.$contextToken.value as string;
        return config;
      },
      (error) => {
        return Promise.reject(error);
      },
    );

    newApiInstance._axiosInstance.interceptors.response.use(
      (response: AxiosResponse) => {
        return response;
      },
      async (error: AxiosError) => {
        const tempError = error;
        if ([401, 403].includes(tempError?.response?.status ?? 0)) {
          if (isAuthenticated.value && !error.config.url?.includes('/api/oauth/token')) {
            try {
              const res = await keepAliveToken();
              set(tempError, 'config.headers.Authorization', `Bearer ${res.access_token}`);
              return newApiInstance._axiosInstance.request(error.config);
            } catch (e) {
              unsetAuthentication();
            }
          } else {
            unsetAuthentication();
          }
        }
        return Promise.reject(error);
      },
    );

    const newInterceptor = (
      newApiInstance._axiosInstance.interceptors.response as any
    ).handlers.pop();

    (newApiInstance._axiosInstance.interceptors.response as any).handlers = [
      newInterceptor,
      ...(newApiInstance._axiosInstance.interceptors.response as any).handlers,
    ];

    return newApiInstance;
  };

  const customApiInstance = _createCustomApiInstance();
  const { setUserType } = useUser();

  const languageId = useCookie('sw-language-id', {
    maxAge: COOKIE_LIFESPAN,
    sameSite: 'lax',
    path: COOKIE_PATH,
  });

  const adminApiInstanceCookie = useCookie<AuthSettings | undefined>('sw-admin-api-auth', {
    maxAge: COOKIE_LIFESPAN,
    sameSite: 'lax',
    path: COOKIE_PATH,
    encode: (value: any) => btoa(JSON.stringify(value)),
    decode: (value: any) => JSON.parse(atob(value)),
  });
  const isAuthenticated = computed(
    () =>
      !!adminApiInstanceCookie.value?.expiresAt &&
      !!adminApiInstanceCookie.value?.accessToken &&
      !!adminApiInstanceCookie.value?.refreshToken,
  );

  const invokeOAuthToken = async (requestData: AuthRequestData) => {
    try {
      const response = await customApiInstance.invoke.post('/api/oauth/token', requestData);
      saveAuthentication(response.data);
      return response.data;
    } catch (e) {
      const error = e as ClientApiError;
      throw error;
    }
  };

  const keepAliveToken = () => {
    // await tryGetAuthenticationCookie();
    if (!isAuthenticated.value) return;
    /**
     * TODO: this method should be invoked in api client interceptor
     * for example if session expiration is in less than X
     */

    // if (refreshPromise) return refreshPromise;
    // if (!refreshToken.value) return;

    // refreshPromise = new Promise((resolve, reject): void => {
    //   resolveRefreshPromise = resolve;
    //   rejectRefreshPromise = () => {
    //     reject("token refresh failed");
    //   };
    // }).catch(unsetAuthentication);

    const requestData: AuthRequestData = {
      client_id: 'administration',
      scopes: 'write',
      grant_type: 'refresh_token',
      refresh_token: adminApiInstanceCookie.value?.refreshToken,
    };

    return invokeOAuthToken(requestData);
  };

  /**
   * this message is only called by login screen
   * @param username
   * @param password
   */
  const authenticate = async (username: string, password: string): Promise<void> => {
    if (!customApiInstance) {
      throw new Error('critical error');
    }

    const requestData: AuthRequestData = {
      client_id: 'administration',
      scopes: 'write',
      grant_type: 'password',
      username,
      password,
    };

    await invokeOAuthToken(requestData);
  };

  const unsetAuthentication = () => {
    adminApiInstanceCookie.value = undefined;
    setUserType(undefined);
    const appointmentId = route.params.appointmentId as string;
    router.push(localePath({ name: 'guide-appointmentId-login', params: { appointmentId } }));
  };

  const getGuideLoginUrl = (previousUrl: string) => {
    return `${localePath(`${previousUrl}/login`)}`;
  };

  const saveAuthentication = (data: AuthResponseData) => {
    const expiresInMs = data.expires_in * 1000;

    adminApiInstanceCookie.value = {
      expiresAt: Date.now() + expiresInMs,
      accessToken: data.access_token,
      refreshToken: data.refresh_token,
    };

    // tryGetAuthenticationCookie();

    // if (refreshPromise) {
    //   resolveRefreshPromise();
    //   refreshPromise = null;
    // }

    // isAuthenticated.value = true;

    // setTimeout(() => {
    //   const catchRefreshPromiseRejection = () => {};
    //   keepAliveToken().catch(catchRefreshPromiseRejection);
    // }, data.expires_in * 1000 - PRE_EXPIRE_TOKEN_REFRESH_TIME);
    // keepAliveToken();
  };

  // function tryGetAuthenticationCookie() {
  //   const cookieData = cookies.get(COOKIE_NAME);
  //   if (!cookieData) {
  //     expiresAt.value = 0;
  //     isAuthenticated.value = null;
  //     accessToken.value = null;
  //     refreshToken.value = null;
  //     return;
  //   }

  //   expiresAt.value = cookieData.expiresAt;
  //   isAuthenticated.value = cookieData.isAuthenticated;
  //   accessToken.value = cookieData.accessToken;
  //   refreshToken.value = cookieData.refreshToken;
  // }

  /**
   * add Authentication to the header
   * @param config
   */
  // function setBearerToken(config?: AxiosRequestConfig) {
  //   config = config || {};
  //   config.headers = config.headers || {};
  //   config.headers[
  //     "Authorization"
  //   ] = `Bearer ${adminApiInstanceCookie.value?.accessToken}`;
  //   config.headers["sw-context-token-alias"] = contextToken.value as string; //cookies.get("sw-context-token");
  //   return config;
  // }

  // async function assureAuthentication(): Promise<void> {
  //   // if (refreshPromise) return refreshPromise; // do nothing wait for last refresh

  //   if (!customApiInstance) {
  //     throw new Error("critical error");
  //   }

  //   if (isAuthenticated.value) return;

  //   throw new Error("not authenticated");
  // }

  const adminApiInstance: ShopwareAdminApiInstance = {
    ...customApiInstance,
    isAuthenticated,
    refreshToken: computed(() => adminApiInstanceCookie.value?.refreshToken || ''),
    accessToken: computed(() => adminApiInstanceCookie.value?.accessToken || ''),
  };

  return {
    adminApiInstance,
    isGuideAuthenticated: computed(() => isAuthenticated.value),
    authenticate,
    unsetAuthentication,
    keepAliveToken,
    getGuideLoginUrl,
  };
};

/**
 * Composable to manage Admin API instance
 *
 * @public
 * @category Auth
 */
export const useAdminApiInstance = createSharedComposable(_useAdminApiInstance);
