export interface ShopwareError {
  code: string;
  detail: string;
  meta: {
    parameters: object;
  };
  status: string;
  title: string;
}
