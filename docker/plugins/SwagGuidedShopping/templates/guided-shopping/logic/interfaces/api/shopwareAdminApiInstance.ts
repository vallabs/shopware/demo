import { ShopwareApiInstance } from '@shopware-pwa/api-client';

export interface ShopwareAdminApiInstance extends ShopwareApiInstance {
  isAuthenticated: ComputedRef<boolean>;
  refreshToken: ComputedRef<string>;
  accessToken: ComputedRef<string>;
}

export interface AuthRequestData {
  client_id: string;
  scopes: string;
  grant_type: string;
  username?: string;
  password?: string;
  refresh_token?: string;
}

export interface AuthResponseData {
  expires_in: number;
  access_token: string;
  refresh_token: string;
}
