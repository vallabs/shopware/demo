import { Product } from '@shopware-pwa/types';

type Price = {
  net: number;
  gross: number;
  linked: boolean;
  listPrice: Price[] | null;
  currencyId: number;
  regulationPrice: null | Partial<Price>;
  percentage: null;
  apiAlias: 'price';
};

export type AdminProduct = Product & {
  purchasePrices?: Price[];
};
