import { DynamicPage } from 'composables/logic/useDynamicPage';
import { ParticipantState } from './participants';

export enum VideoAudioSettingsType {
  VIDEO_CALL = 'both',
  VOICE_CALL = 'audio-only',
  NONE = 'none',
}

export enum AttendeeRestrictionType {
  OPEN = 'open',
  CUSTOMER = 'customer',
  RULES = 'rules',
}

export interface PresentationStateForAll {
  accessibleFrom: string;
  accessibleTo: string;
  allowUserActionsForGuide: boolean;
  appointmentMode: string;
  attendeeRestrictionType: AttendeeRestrictionType;
  broadcastMode: boolean;
  currentGuideProductId: string;
  currentPageId: string;
  currentDynamicPage: DynamicPage;
  currentSectionId: string;
  currentSlideAlias: number;
  started: boolean;
  startedAt: string;
  running: boolean;
  ended: boolean;
  endedAt: string;
  lastActiveGuideSection: string;
  productDetailDefaultPageId: string;
  productListingDefaultPageId: string;
  quickviewPageId: string;
  videoAudioSettings: VideoAudioSettingsType;
  videoRoomUrl: string;
  extensions: any;
}

export interface PresentationStateForGuides {
  clients: Record<string, ParticipantState>;
  inactiveClients: Record<string, ParticipantState>;
  guides: Record<string, ParticipantState>;
  videoGuideToken: string;
  extensions: any;
  quickViewState: any;
}

export interface PresentationStateForClients {
  videoClientToken: string;
  extensions: any;
  clients: Record<string, ParticipantState>;
  guides: Record<string, ParticipantState>;
}

export interface PresentationStateForMe extends ParticipantState {
  attendeeSubmittedAt: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
}

export interface PresentationStateModel {
  stateForAll: PresentationStateForAll;
  stateForGuides: PresentationStateForGuides;
  stateForClients: PresentationStateForClients;
  stateForMe: PresentationStateForMe;
}
