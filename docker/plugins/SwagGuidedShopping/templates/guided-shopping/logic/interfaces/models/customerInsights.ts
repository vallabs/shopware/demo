import { BillingAddress } from '@shopware-pwa/types';

export interface CustomerInsightsProductCollection {
  alias: string;
}

export interface CustomerInsightsAttendee {
  cartSum: string;
  extensions: [];
  id: string;
  productCount: number;
}

export interface CustomerInsightsAttendeeData {
  attendees: { [key: string]: CustomerInsightsAttendee };
  currencyId: string;
  currencySymbol: string;
  extensions: [];
}

export interface CustomerInsightsCustomer {
  apiAlias: string;
  email: string;
  defaultBillingAddress: BillingAddress;
}

export interface CustomerInsightsCustomerData {
  apiAlias: string;
  appointmentId: string;
  attendeeName: string;
  createdAt: string;
  customer: CustomerInsightsCustomer;
  customerId: string;
  extensions: [];
  guideCartPermissionsGranted: boolean;
  id: string;
  joinedAt: string;
  productCollections: CustomerInsightsProductCollection[];
  type: string;
  updatedAt: string;
  videoUserId: string;
}

export interface CustomerInsightsYearOrder {
  apiAlias: string;
  amountNet: number;
  amountTotal: number;
  orderNumber: string;
  lineItems: [];
  orderDate: string;
}

export interface OrderCustomer {
  apiAlias: string;
  order: CustomerInsightsYearOrder;
}
