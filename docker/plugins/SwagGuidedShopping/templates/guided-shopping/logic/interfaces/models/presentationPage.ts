import { CmsPage, Product } from '@shopware-pwa/types';

export type CmsPageRelation = {
  pickedProductIds: string[];
  isInstantListing: boolean;
  presentationId: string;
  cmsPageId: string;
  id: string;
  title: string;
};

export type Extensions = {
  cmsPageRelation: CmsPageRelation;
};

export type SlideModel = {
  apiAlias: 'pwa_page_result';
  cmsPage: CmsPage;
  extensions: Extensions;
  product?: Product;
};

export interface NoteItem {
  id: string;
  type: string;
  translated: {
    config: {
      content: {
        value: string;
      };
    };
  };
}

export interface NavigationItem {
  groupId: string;
  groupName: string;
  cmsPageId: string;
  sectionId: string;
  sectionName: string;
  index: number;
  notes?: NoteItem[];
  isInstantListing?: boolean;
  pickedProductsCount?: number;
}

export type SlideItem = NavigationItem & {
  cmsPageResult?: SlideModel;
};

export interface PresentationPageModel {
  cmsPageResults: Record<string, any>[];
  navigation: SlideItem[];
}

export type SlideGroupItem = {
  id: string;
  name: string;
  items: NavigationItem[];
};

export type SlideGroupList = SlideGroupItem[];
