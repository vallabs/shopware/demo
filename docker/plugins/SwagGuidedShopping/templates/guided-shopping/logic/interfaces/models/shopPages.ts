export enum ShopPage {
  GTC = 'tosPage',
  REVOCATION = 'revocationPage',
  SHIPPING = 'shippingPaymentInfoPage',
  PRIVACY = 'privacyPage',
  IMPRINT = 'imprintPage',
  ERROR = 'http404Page',
  MAINTAIN = 'maintenancePage',
  CONTACT = 'contactPage',
  NEWSLETTER = 'newsletterPage',
}
