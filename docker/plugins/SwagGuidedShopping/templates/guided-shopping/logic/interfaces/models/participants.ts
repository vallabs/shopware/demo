import { DailyParticipant } from '@daily-co/daily-js';

export interface ParticipantState {
  attendeeId: string;
  attendeeName: string;
  videoUserId: string;
  guideCartPermissionsGranted?: boolean;
  inactive?: boolean;
  hasJoined: boolean;
}

export interface StreamParticipant {
  data: DailyParticipant;
  streams: {
    video: MediaStream | null;
    audio: MediaStream | null;
  };
}
