export interface MediaDevices {
  videoInput: MediaDeviceInfo[];
  audioInput: MediaDeviceInfo[];
  audioOutput: MediaDeviceInfo[];
}

// export interface CustomMediaDeviceInfo {
//   readonly deviceId: string;
//   readonly groupId: string;
//   readonly kind: MediaDeviceKind;
//   readonly label: string;
// }
