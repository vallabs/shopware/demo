export enum CmsActionType {
  SLIDER = 'slider',
  TAB = 'tab',
  VIRTUAL_SCROLLING = 'virtual-scrolling',
}

export type CmsAction =
  | {
      elementId: string;
      type: CmsActionType.SLIDER;
      slideIndex: number;
    }
  | {
      elementId: string;
      type: CmsActionType.TAB;
      tabIndex: number;
    }
  | {
      elementId: string;
      type: CmsActionType.VIRTUAL_SCROLLING;
      itemIndex: number;
    };

export interface ScrollToTarget {
  elementId: string;
  highlightId: string;
  containerId: string;
  actions?: CmsAction[];
  publishedBy: string;
}
