export enum ActionName {
  LOAD_PRODUCT_LIST = 'loadProductList',
  SWITCH_SLIDE = 'switchSlide',
  LOAD_CART = 'loadCart',
  ADD_TO_CART = 'addToCart',
  REMOVE_FROM_CART = 'removeFromCart',
  UPDATE_PRODUCT_QUANTITY = 'updateProductQuantity',
  LOAD_LIKE_LIST = 'loadLikeList',
  LOAD_LAST_SEEN = 'loadLastSeen',
  REMOVE_FROM_LIKE_LIST = 'removeFromLikeList',
  LOAD_SLIDE = 'loadSlide',
  LOAD_LISTING_PRODUCTS = 'loadListingProducts',
  UPDATE_LISTING = 'updateListingProducts',
  LOAD_PRODUCT_REVIEWS = 'loadProductReviews',
}
