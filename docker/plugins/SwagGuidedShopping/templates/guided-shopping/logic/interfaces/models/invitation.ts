export enum InvitationAnswer {
  ACCEPTED = 'accepted',
  MAYBE = 'maybe',
  DECLINED = 'declined',
}

export enum InvitationStatus {
  STARTED = 'started',
  ENDED = 'ended',
}

export type InvitationDetail = {
  accessibleFrom: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
  accessibleTo: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
  id: string;
  status?: InvitationStatus;
};

export interface AppointmentInvitation {
  answer: InvitationAnswer;
  appointment: InvitationDetail;
}
