import { ShopwareApiInstance, getProducts } from '@shopware-pwa/api-client';
import { EntityResult, Product, ShopwareSearchParams } from '@shopware-pwa/types';
import { AxiosRequestConfig } from 'axios';
import { getLastSeenProductsEndpoint } from '../endpoints';
import { ShopwareAdminApiInstance } from '~/logic/interfaces';

export const getLastSeenProducts = async (
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<EntityResult<'product', Product> | undefined> => {
  const getLastSeenResponse = await apiInstance.invoke.get(getLastSeenProductsEndpoint(), config);

  if (getLastSeenResponse.data.collection.lastSeen.length === 0) {
    return;
  }

  return await getProducts(
    {
      ids: getLastSeenResponse.data.collection.lastSeen,
      associations: {
        options: {
          associations: {
            group: {},
          },
        },
      },
    } as ShopwareSearchParams,
    apiInstance,
  );
};

export const getLastSeenProductsForAttendee = async (
  apiInstance: ShopwareApiInstance,
  adminApiInstance: ShopwareAdminApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const getLastSeenResponse = await adminApiInstance.invoke.get(
    getLastSeenProductsEndpoint(),
    config,
  );

  return await getProducts(
    { ids: getLastSeenResponse.data.collection.lastSeen } as ShopwareSearchParams,
    apiInstance,
  );
};
