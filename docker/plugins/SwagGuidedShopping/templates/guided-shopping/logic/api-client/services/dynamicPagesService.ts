import { ShopwareApiInstance } from '@shopware-pwa/api-client';
import { getQuickViewEndpoint, getCmsProductDetailPage } from '../endpoints';
import { SlideModel } from '~/logic/interfaces';

export const getQuickView = (
  params: {
    productId: string;
    cmsPageLayoutId?: string;
  },
  apiInstance: ShopwareApiInstance,
): Promise<SlideModel> => {
  return apiInstance.invoke
    .get(getQuickViewEndpoint(params.productId, params.cmsPageLayoutId ?? ''))
    .then((response: { data: SlideModel }) => {
      return response.data;
    });
};

export const getDetail = (
  productId: string,
  apiInstance: ShopwareApiInstance,
): Promise<SlideModel> => {
  return apiInstance.invoke
    .get(getCmsProductDetailPage(productId))
    .then((response: { data: SlideModel }) => {
      return response.data;
    });
};
