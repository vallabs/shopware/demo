import { ShopwareApiInstance } from '@shopware-pwa/api-client';
import { CmsPage } from '@shopware-pwa/types';
import { ShopPage } from 'logic/interfaces/models/shopPages';
import { getShopPageEndpoint } from '../endpoints';

export const getShopPage = (
  layoutName: ShopPage,
  apiInstance: ShopwareApiInstance,
): Promise<CmsPage> => {
  return apiInstance.invoke.get(getShopPageEndpoint(layoutName)).then((response) => {
    return response.data;
  });
};
