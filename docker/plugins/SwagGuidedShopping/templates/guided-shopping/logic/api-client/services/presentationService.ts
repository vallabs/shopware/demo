import { ShopwareApiInstance } from '@shopware-pwa/api-client';
import { CmsPage, ListingResult, Product } from '@shopware-pwa/types';
import { AxiosRequestConfig } from 'axios';
import {
  getPresentationPageEndpoint,
  getPresentationPageDataEndpoint,
  getPresentationGuideStateEndpoint,
  getPresentationClientStateEndpoint,
  getCmsPageByIdEndpoint,
  getPresentationPageProductsEndpoint,
} from '../endpoints';
import { PresentationPageModel, ShopwareAdminApiInstance, SlideModel } from '../../interfaces';

export const getPage = (apiInstance: ShopwareApiInstance): Promise<PresentationPageModel> => {
  return apiInstance.invoke
    .get(getPresentationPageEndpoint())
    .then((response: { data: PresentationPageModel }) => {
      return response.data;
    });
};

export const getSlideData = (
  data: {
    pageId: string;
    slideId: string;
  },
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<SlideModel> => {
  return apiInstance.invoke
    .get(getPresentationPageDataEndpoint(data.pageId, data.slideId), config)
    .then((response: { data: SlideModel }) => {
      return response.data;
    });
};

export const getSlideProducts = (
  data: {
    pageId: string;
    slideId: string;
    query: object;
  },
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<ListingResult<Product>> => {
  return apiInstance.invoke
    .post(getPresentationPageProductsEndpoint(data.pageId, data.slideId), data.query, config)
    .then((response: { data: ListingResult<Product> }) => {
      return response.data;
    });
};

export const getStateForGuides = (
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<any> => {
  // TODO; proper typing
  return adminApiInstance.invoke
    .get(getPresentationGuideStateEndpoint(appointmentId))
    .then((response) => {
      return response.data;
    });
};

export const getStateForClients = (apiInstance: ShopwareApiInstance): Promise<any> => {
  return apiInstance.invoke
    .get(getPresentationClientStateEndpoint())
    .then((response: { data: any }) => {
      return response.data;
    });
};

export const getCmsPageById = (
  pageId: string,
  apiInstance: ShopwareApiInstance,
): Promise<CmsPage> => {
  return apiInstance.invoke
    .get(getCmsPageByIdEndpoint(pageId))
    .then((response: { data: CmsPage }) => {
      return response.data;
    });
};
