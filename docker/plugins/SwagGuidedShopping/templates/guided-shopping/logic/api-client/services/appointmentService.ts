import { ShopwareApiInstance } from '@shopware-pwa/api-client';
import {
  AppointmentModel,
  ShopwareAdminApiInstance,
  AttendeeData,
  InvitationAnswer,
  AppointmentInvitation,
  VideoAudioSettingsType,
} from '~/logic/interfaces';
import {
  getJoinAsGuideEndpoint,
  getJoinAsClientEndpoint,
  getAppointmentStartEndpoint,
  getAppointmentEndEndpoint,
  updateAttendeeEndpoint,
  getAppointmentByPathEndpoint,
  getAttendeeContextTokenEndpoint,
  respondInvitationEndpoint,
  getVideoAndAudioSetting,
} from '~/logic/api-client/endpoints';

export interface AppointmentVideoAndAudioSetting {
  apiAlias: 'appointment_video_audio_settings';
  videoAudioSettings: VideoAudioSettingsType;
}

export const startAppointment = (
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<void> => {
  return adminApiInstance.invoke.post(getAppointmentStartEndpoint(appointmentId));
};

export const endAppointment = (
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<void> => {
  return adminApiInstance.invoke.post(getAppointmentEndEndpoint(appointmentId));
};

export const joinAsGuide = async (
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<AppointmentModel | null> => {
  const appointmentResp = await adminApiInstance.invoke.post(getAppointmentByPathEndpoint(), {
    filter: [
      {
        type: 'equals',
        field: 'presentationPath',
        value: appointmentId,
      },
    ],
  });

  const id = appointmentResp?.data?.data[0]?.id || appointmentId;
  if (!id) return null;

  const joinResp = await adminApiInstance.invoke.post(getJoinAsGuideEndpoint(id));
  // add appointment id
  joinResp.data.id = id;

  return joinResp.data as AppointmentModel;
};

export const joinAsClient = (
  appointmentId: string,
  attendeeName: string,
  apiInstance: ShopwareApiInstance,
): Promise<AppointmentModel> => {
  return apiInstance.invoke
    .post(getJoinAsClientEndpoint(appointmentId), {
      attendeeName: attendeeName?.toString(),
    })
    .then((response) => {
      return response.data as AppointmentModel;
    });
};

export const updateAttendee = (
  data: AttendeeData,
  apiInstance: ShopwareApiInstance,
): Promise<void> => {
  return apiInstance.invoke.patch(updateAttendeeEndpoint(), data);
};

export const getAttendeeContextToken = async (
  attendeeId: string,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<string> => {
  const tokenResponse = await adminApiInstance.invoke.get(
    getAttendeeContextTokenEndpoint(attendeeId),
  );
  return tokenResponse ? tokenResponse.data['attendee-sw-context-token'] : '';
};

export const getAppointmentVideoAndAudioSetting = async (
  appointmentId: string,
  apiInstance: ShopwareApiInstance,
): Promise<AppointmentVideoAndAudioSetting> => {
  return apiInstance.invoke.get(getVideoAndAudioSetting(appointmentId)).then((response) => {
    return response.data as AppointmentVideoAndAudioSetting;
  });
};

export const respondAppointmentInvitation = (
  appointmentId: string,
  data: { token: string; answer: InvitationAnswer },
  apiInstance: ShopwareApiInstance,
): Promise<AppointmentInvitation | undefined> => {
  return apiInstance.invoke
    .patch<AppointmentInvitation>(respondInvitationEndpoint(appointmentId), data)
    .then((res) => res.data);
};
