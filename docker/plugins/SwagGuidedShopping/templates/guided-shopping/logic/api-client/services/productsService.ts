import { ShopwareApiInstance } from '@shopware-pwa/api-client';
import {
  ListingResult,
  Product,
  DeliveryTime,
  ShopwareSearchParams,
  ProductReview,
} from '@shopware-pwa/types';
import { AxiosRequestConfig } from 'axios';
import {
  //   getProductStreamsEndpoint,
  //   getSyncProductsEndpoint,
  getAllProductsWithDataEndpoint,
  getSyncProductsEndpoint,
  getAdminProductEndpoint,
  getDeliveryTimeEndpoint,
  getProductReviewsEndpoint,
} from '../endpoints';
// import { ProductStreamFilterItem, SelectedFilters } from '../../interfaces/models/instantListing';

import { ShopwareAdminApiInstance, AdminProduct } from '~/logic/interfaces';

export const getAllProductsWithData = async (
  criteria: ShopwareSearchParams,
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<ListingResult<Product>> => {
  const productResponse = await apiInstance.invoke.post(
    getAllProductsWithDataEndpoint(),
    criteria,
    config,
  );

  return productResponse.data;
};

// export const getProductStreams = async (
//   criteria: SelectedFilters,
//   adminApiInstance: ShopwareAdminApiInstance,
// ): Promise<ProductStreamFilterItem[]> => {
//   const productStreamResponse = await adminApiInstance.invoke.post(
//     getProductStreamsEndpoint(),
//     criteria,
//   );

//   return productStreamResponse.data.data;
// };

export const saveNewInstantListing = async (
  params: {
    productIds: string[];
    currentPageGroupId: string | null;
    appointmentIdToSync: string;
    pageName: string;
  },
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<{ index: number }> => {
  const productIds = params.productIds;
  const pageName = params.pageName;
  const currentPageGroupId = params.currentPageGroupId;

  const res = await apiInstance.invoke.post(
    getSyncProductsEndpoint(params.appointmentIdToSync),
    {
      productIds,
      currentPageGroupId,
      pageName,
    },
    config,
  );
  return res.data;
};

export const saveInstantListingUpdate = async (
  params: {
    addProductIds: string[];
    removeProductIds: string[];
    currentPageGroupId: string;
    appointmentIdToSync: string;
    pageName: string;
  },
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<{ id: string; pickedProductIds: string[]; title: string }> => {
  const res = await apiInstance.invoke.patch(
    getSyncProductsEndpoint(params.appointmentIdToSync),
    params,
    config,
  );
  return res.data;
};

export const getAdminProduct = async (
  searchCriteria: ShopwareSearchParams,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<AdminProduct[]> => {
  const resp = await adminApiInstance.invoke.post(getAdminProductEndpoint(), searchCriteria);
  return resp.data.data;
};

export const getDeliveryTime = async (
  searchCriteria: ShopwareSearchParams,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<DeliveryTime[]> => {
  const resp = await adminApiInstance.invoke.post(getDeliveryTimeEndpoint(), searchCriteria);
  return resp.data.data;
};

export const loadProductReviews = async (
  productId: string,
  searchCriteria: ShopwareSearchParams,
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<ListingResult<ProductReview>> => {
  const result = await apiInstance.invoke.post(
    getProductReviewsEndpoint(productId),
    searchCriteria,
    config,
  );
  return result.data;
};
