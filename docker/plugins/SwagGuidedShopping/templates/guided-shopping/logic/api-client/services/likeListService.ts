import { ShopwareApiInstance, getProductEndpoint } from '@shopware-pwa/api-client';
import { EntityResult, Product, ShopwareSearchParams } from '@shopware-pwa/types';
import { AxiosRequestConfig } from 'axios';
import {
  getLikedProductsEndpoint,
  // getDislikedProductsEndpoint,
  likedProductEndpoint,
  // addDislikedProductEndpoint,
  // removeDislikedProductEndpoint,
  getLikesDislikesForAttendeeEndpoint,
  addLikesDislikesForAttendeeEndpoint,
  removeLikesDislikesForAttendeeEndpoint,
  getAllLikedProductsEndpoint,
} from '~/logic/api-client/endpoints';
import { ShopwareAdminApiInstance } from '~/logic/interfaces';

export const getLikedProductIds = async (
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<string[]> => {
  const response = await apiInstance.invoke.get(getLikedProductsEndpoint(), config);
  return response.data.collection.liked;
};

export const getLikedProducts = async (
  searchCriteria: Partial<ShopwareSearchParams>,
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<EntityResult<'product', Product[]> | undefined> => {
  const getLikeResponse = await apiInstance.invoke.get(getLikedProductsEndpoint(), config);

  if (getLikeResponse.data.collection.liked.length === 0) {
    return;
  }

  return await getProductsFromIdList(
    {
      searchCriteria,
      idList: getLikeResponse.data.collection.liked,
    },
    apiInstance,
  );
};

// export const getDislikedProducts = async (
//   searchCriteria: ShopwareSearchParams,
//   apiInstance: ShopwareApiInstance,
// ): Promise<EntityResult<'product', Product[]> | undefined> => {
//   const getDislikeResponse = await apiInstance.invoke.get(getDislikedProductsEndpoint());

//   if (getDislikeResponse.data.collection.disliked.length === 0) {
//     return;
//   }

//   return await getProductsFromIdList(
//     searchCriteria,
//     getDislikeResponse.data.collection.disliked,
//     apiInstance,
//   );
// };

export const likeProduct = async (
  productId: string,
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const addLikeResponse = await apiInstance.invoke.post(likedProductEndpoint(productId), config);
  return addLikeResponse.data;
};

export const likeProductForAttendee = async (
  data: {
    productId: string;
    attendeeId: string;
  },
  adminApiInstance: ShopwareAdminApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const { productId, attendeeId } = data;

  const addLikeResponse = await adminApiInstance.invoke.post(
    addLikesDislikesForAttendeeEndpoint(),
    {
      alias: 'liked',
      productId,
      attendeeId,
    },
    config,
  );

  return addLikeResponse.data;
};

export const unlikeProduct = async (
  productId: string,
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const result = await apiInstance.invoke.delete(likedProductEndpoint(productId), config);
  return result.data;
};

export const unlikeProductForAttendee = async (
  data: {
    productId: string;
    attendeeId: string;
  },
  adminApiInstance: ShopwareAdminApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const { productId, attendeeId } = data;
  // get like entry of product
  const getLikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'productId',
              value: productId,
            },
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId,
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'liked',
            },
          ],
        },
      ],
    },
    config,
  );

  const id = getLikeResponse?.data?.data[0]?.id;
  if (!id) return null;

  // remove like
  const removeLikeResponse = await adminApiInstance.invoke.delete(
    removeLikesDislikesForAttendeeEndpoint(id),
  );
  return removeLikeResponse.data;
};

// export const removeDislikedProduct = async (
//   productId: string,
//   apiInstance: ShopwareApiInstance,
// ): Promise<any> => {
//   const removeDislikeResponse = await apiInstance.invoke.delete(
//     removeDislikedProductEndpoint(productId),
//   );
//   return removeDislikeResponse.data;
// };

// export const removeDislikedProductForAttendee = async (
//   productId: string,
//   attendeeId: string,
//   adminApiInstance: ShopwareAdminApiInstance,
// ): Promise<any> => {
//   // get dislike entry of product
//   const getDislikeResponse = await adminApiInstance.invoke.post(
//     getLikesDislikesForAttendeeEndpoint(),
//     {
//       filter: [
//         {
//           type: 'multi',
//           operator: 'and',
//           queries: [
//             {
//               type: 'equals',
//               field: 'productId',
//               value: productId,
//             },
//             {
//               type: 'equals',
//               field: 'attendeeId',
//               value: attendeeId,
//             },
//             {
//               type: 'equals',
//               field: 'alias',
//               value: 'disliked',
//             },
//           ],
//         },
//       ],
//     },
//   );

//   const id = getDislikeResponse?.data?.data[0]?.id;
//   if (!id) return null;

//   // remove dislike
//   const removeDislikeResponse = await adminApiInstance.invoke.delete(
//     removeLikesDislikesForAttendeeEndpoint(id),
//   );
//   return removeDislikeResponse.data;
// };

export const getLikedProductsForAttendee = async (
  data: {
    searchCriteria: ShopwareSearchParams;
    attendeeId: string;
  },
  apiInstance: ShopwareApiInstance,
  adminApiInstance: ShopwareAdminApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const { searchCriteria, attendeeId } = data;
  // get product ids of likes
  const getLikeResponse = await adminApiInstance.invoke.post(
    getLikesDislikesForAttendeeEndpoint(),
    {
      filter: [
        {
          type: 'multi',
          operator: 'and',
          queries: [
            {
              type: 'equals',
              field: 'attendeeId',
              value: attendeeId,
            },
            {
              type: 'equals',
              field: 'alias',
              value: 'liked',
            },
          ],
        },
      ],
    },
    config,
  );

  const productIds: string[] = [];
  getLikeResponse.data.data.forEach((product: { productId: string }) => {
    productIds.push(product.productId);
  });

  if (productIds.length === 0) return null;

  // get product data
  const products = await getProductsFromIdList(
    {
      searchCriteria,
      idList: productIds,
    },
    apiInstance,
  );
  return products;
};

// export const getDislikedProductsForAttendee = async (
//   searchCriteria: ShopwareSearchParams,
//   attendeeId: string,
//   apiInstance: ShopwareApiInstance,
//   adminApiInstance: ShopwareAdminApiInstance,
// ): Promise<any> => {
//   // get product ids of dislikes
//   const getDislikeResponse = await adminApiInstance.invoke.post(
//     getLikesDislikesForAttendeeEndpoint(),
//     {
//       filter: [
//         {
//           type: 'multi',
//           operator: 'and',
//           queries: [
//             {
//               type: 'equals',
//               field: 'attendeeId',
//               value: attendeeId,
//             },
//             {
//               type: 'equals',
//               field: 'alias',
//               value: 'disliked',
//             },
//           ],
//         },
//       ],
//     },
//   );

//   const productIds: string[] = [];
//   getDislikeResponse.data.data.forEach((product: { productId: string }) => {
//     productIds.push(product.productId);
//   });

//   if (productIds.length === 0) return null;

//   // get product data
//   const products = await getProductsFromIdList(searchCriteria, productIds, apiInstance);
//   return products;
// };

export const getProductsFromIdList = async (
  data: {
    searchCriteria: Partial<ShopwareSearchParams>;
    idList: string[];
  },
  apiInstance: ShopwareApiInstance,
) => {
  const { searchCriteria, idList } = data;
  const resp = await apiInstance.invoke.post(getProductEndpoint(), {
    ...searchCriteria,
    filter: [
      {
        type: 'equalsAny',
        field: 'id',
        value: idList,
      },
    ],
    associations: {
      options: {
        associations: {
          group: {},
        },
      },
    },
  });

  return resp.data;
};

// export const getLikesAndDislikesForProduct = async (
//   productId: string,
//   adminApiInstance: ShopwareAdminApiInstance,
// ): Promise<any> => {
//   // get total likes and dislike of the product
//   const getLikeResponse = await adminApiInstance.invoke.post(
//     getLikesDislikesForAttendeeEndpoint(),
//     {
//       includes: {
//         guided_shopping_attendee_product_collection: ['alias'],
//       },
//       filter: [
//         {
//           type: 'multi',
//           operator: 'or',
//           queries: [
//             {
//               type: 'equals',
//               field: 'product.parentId',
//               value: productId,
//             },
//             {
//               type: 'equals',
//               field: 'product.id',
//               value: productId,
//             },
//           ],
//         },
//         {
//           type: 'equalsAny',
//           field: 'alias',
//           value: ['liked', 'disliked'],
//         },
//         {
//           type: 'equals',
//           field: 'attendee.type',
//           value: 'CLIENT',
//         },
//       ],
//       aggregations: [
//         {
//           name: 'sum',
//           type: 'terms',
//           field: 'alias',
//         },
//       ],
//       total_count_mode: 0,
//     },
//   );

//   return getLikeResponse.data.data;
// };

export const getAllLikedProducts = async (
  data: {
    appointmentId: string;
  },
  adminApiInstance: ShopwareAdminApiInstance,
  config?: AxiosRequestConfig,
) => {
  const result = await adminApiInstance.invoke.get(
    getAllLikedProductsEndpoint(data.appointmentId),
    config,
  );
  return result.data;
};
