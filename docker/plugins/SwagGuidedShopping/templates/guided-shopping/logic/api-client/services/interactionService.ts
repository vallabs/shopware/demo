import { ShopwareApiInstance } from '@shopware-pwa/api-client';
import { AxiosRequestConfig } from 'axios';
import { getInteractionEndpoint } from '../endpoints';

export const send = async (
  data: object,
  apiInstance: ShopwareApiInstance,
  config?: AxiosRequestConfig,
): Promise<any> => {
  const response = await apiInstance.invoke.post(getInteractionEndpoint(), data, config);
  return response.data;
};
