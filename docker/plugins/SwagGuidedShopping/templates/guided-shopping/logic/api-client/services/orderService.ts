import { AxiosRequestConfig } from 'axios';
import { ShopwareSearchParams } from '@shopware-pwa/types';
import { getOrderLineItemEndpoint, getOrderCustomerEndpoint } from '../endpoints';
import { ShopwareAdminApiInstance, OrderLineItem, OrderCustomer } from '../../interfaces';

export const getOrderLineItems = async (
  searchCriteria: ShopwareSearchParams,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<OrderLineItem[]> => {
  const resp = await adminApiInstance.invoke.post(getOrderLineItemEndpoint(), searchCriteria);
  return resp.data.data;
};

export const getOrderCustomer = async (
  searchCriteria: Partial<ShopwareSearchParams>,
  adminApiInstance: ShopwareAdminApiInstance,
  config: AxiosRequestConfig = {},
): Promise<OrderCustomer[]> => {
  const resp = await adminApiInstance.invoke.post(
    getOrderCustomerEndpoint(),
    searchCriteria,
    config,
  );

  return resp.data.data;
};
