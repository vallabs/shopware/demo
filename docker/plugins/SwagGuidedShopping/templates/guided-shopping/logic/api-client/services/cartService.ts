import { Cart, LineItem } from '@shopware-pwa/types';
import { AxiosRequestConfig } from 'axios';
import {
  ShopwareApiInstance,
  getCheckoutCartEndpoint,
  getCheckoutCartLineItemEndpoint,
} from '@shopware-pwa/api-client';
// import { ShopwareAdminApiInstance, CartInsights } from '../../interfaces';
import { ShopwareAdminApiInstance } from '~/logic/interfaces';
import {
  getGuideAllCartsEndpoint,
  getGuideCartEndpoint,
  getGuideCartLineItemEndpoint,
  // getCartInsightsEndpoint,
} from '~/logic/api-client/endpoints';

export type MinimumLineItem = {
  id: string;
  quantity?: number;
};

export const Client = {
  /**
   * fetch cart
   * @param apiInstance
   * @param config
   */
  getCart: async (apiInstance: ShopwareApiInstance, config?: AxiosRequestConfig): Promise<Cart> => {
    const result = await apiInstance.invoke.get(getCheckoutCartEndpoint(), config);
    return result.data;
  },

  /**
   * add line items
   *
   * @param itemsToAdd
   * @param apiInstance
   */
  addLineItems: async (
    data: {
      itemsToAdd: MinimumLineItem[];
    },
    apiInstance: ShopwareApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const items = data.itemsToAdd.map((product) => {
      const item: Partial<LineItem> = {
        quantity: product.quantity,
        type: 'product',
        referencedId: product.id,
        id: product.id,
      };
      return item;
    });

    const result = await apiInstance.invoke.post(
      getCheckoutCartLineItemEndpoint(),
      { items },
      config,
    );
    return result.data;
  },

  /**
   * update quantities
   *
   * @param itemsToUpdate
   * @param apiInstance
   */
  changeLineItemQuantities: async (
    data: {
      itemsToUpdate: MinimumLineItem[];
    },
    apiInstance: ShopwareApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const items = data.itemsToUpdate.map((product) => {
      const item: Partial<LineItem> = {
        quantity: parseInt(`${product.quantity}`, 10),
        id: product.id,
      };
      return item;
    });

    const result = await apiInstance.invoke.patch(
      getCheckoutCartLineItemEndpoint(),
      { items },
      config,
    );
    return result.data;
  },

  /**
   * remove line items
   *
   * @param itemsToRemove
   * @param apiInstance
   */
  removeLineItems: async (
    data: {
      itemsToRemove: MinimumLineItem[];
    },
    apiInstance: ShopwareApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const ids = data.itemsToRemove.map((item) => item.id);

    const result = await apiInstance.invoke.delete(getCheckoutCartLineItemEndpoint(), {
      ...config,
      data: { ids },
    });
    return result.data;
  },
};

export const Guide = {
  /**
   * fetch cart of client
   *
   * @param salesChannelId
   * @param adminApiInstance
   */
  getCart: async (
    data: {
      salesChannelId: string;
    },
    adminApiInstance: ShopwareAdminApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const result = await adminApiInstance.invoke.get(
      getGuideCartEndpoint(data.salesChannelId),
      config,
    );
    return result.data;
  },

  /**
   * fetch all carts of all clients
   *
   * @param appointmentId
   * @param adminApiInstance
   */
  getAllCarts: async (
    data: {
      appointmentId: string;
    },
    adminApiInstance: ShopwareAdminApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const result = await adminApiInstance.invoke.get(
      getGuideAllCartsEndpoint(data.appointmentId),
      config,
    );
    return result.data;
  },

  /**
   * add line items for client
   *
   * @param clientToken
   * @param salesChannelId
   * @param itemsToAdd
   * @param adminApiInstance
   */
  addLineItems: async (
    data: {
      salesChannelId: string;
      itemsToAdd: MinimumLineItem[];
    },
    adminApiInstance: ShopwareAdminApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const items = data.itemsToAdd.map((product) => {
      const item: Partial<LineItem> = {
        quantity: product.quantity,
        type: 'product',
        referencedId: product.id,
        id: product.id,
      };
      return item;
    });

    const result = await adminApiInstance.invoke.post(
      getGuideCartLineItemEndpoint(data.salesChannelId),
      { items },
      config,
    );
    return result.data;
  },

  /**
   * change line item quantities for client
   *
   * @param clientToken
   * @param salesChannelId
   * @param itemsToUpdate
   * @param adminApiInstance
   */
  changeLineItemQuantities: async (
    data: {
      salesChannelId: string;
      itemsToUpdate: MinimumLineItem[];
    },
    adminApiInstance: ShopwareAdminApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const items = data.itemsToUpdate.map((product) => {
      const item: Partial<LineItem> = {
        quantity: parseInt(`${product.quantity}`, 10),
        id: product.id,
      };
      return item;
    });

    const result = await adminApiInstance.invoke.patch(
      getGuideCartLineItemEndpoint(data.salesChannelId),
      { items },
      config,
    );
    return result.data;
  },

  /**
   * remove line items for client
   *
   * @param clientToken
   * @param salesChannelId
   * @param itemsToRemove
   * @param adminApiInstance
   */
  removeLineItems: async (
    data: {
      salesChannelId: string;
      itemsToRemove: MinimumLineItem[];
    },
    adminApiInstance: ShopwareAdminApiInstance,
    config?: AxiosRequestConfig,
  ): Promise<Cart> => {
    const ids = data.itemsToRemove.map((item) => item.id);
    const result = await adminApiInstance.invoke.delete(
      getGuideCartLineItemEndpoint(data.salesChannelId),
      {
        ...config,
        data: { ids },
      },
    );
    return result.data;
  },
};

// export async function getCartInsights(
//   appointmentId: string,
//   adminApiInstance: ShopwareAdminApiInstance,
// ): Promise<CartInsights> {
//   const resp = await adminApiInstance.invoke.get(getCartInsightsEndpoint(appointmentId));

//   return resp.data;
// }
