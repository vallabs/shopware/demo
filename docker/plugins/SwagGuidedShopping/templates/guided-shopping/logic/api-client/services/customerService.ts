import { ShopwareSearchParams } from '@shopware-pwa/types';
import {
  ShopwareAdminApiInstance,
  CustomerInsightsCustomerData,
  CustomerInsightsAttendeeData,
} from '../../interfaces';
import { getAttendeeEndpoint, getAttendeeInsightsEndpoint } from '../endpoints';

export const getCustomerData = async (
  criteria: Partial<ShopwareSearchParams>,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<CustomerInsightsCustomerData> => {
  const resp = await adminApiInstance.invoke.post(getAttendeeEndpoint(), criteria);

  return resp.data.data[0];
};

export const getAttendeeInsightsData = async (
  appointmentId: string,
  adminApiInstance: ShopwareAdminApiInstance,
): Promise<CustomerInsightsAttendeeData> => {
  const resp = await adminApiInstance.invoke.get(getAttendeeInsightsEndpoint(appointmentId));

  return resp.data;
};
