import { ShopPage } from 'logic/interfaces/models/shopPages';

export const getJoinAsGuideEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/join-as-guide`;

export const getJoinAsClientEndpoint = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/join-as-client`;

export const getPresentationPageEndpoint = () =>
  `/store-api/guided-shopping/appointment/presentation`;

export const getPresentationPageDataEndpoint = (pageId: string, slideId: string) =>
  `/store-api/guided-shopping/appointment/presentation/${pageId}/slide/${slideId}`;

export const getPresentationPageProductsEndpoint = (pageId: string, slideId: string) =>
  `/store-api/guided-shopping/appointment/presentation/${pageId}/slide/${slideId}/products`;

export const getAppointmentStartEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/start`;

export const getAppointmentEndEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/end`;

export const getPresentationGuideStateEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/presentation/state`;

export const getPresentationClientStateEndpoint = () =>
  `/store-api/guided-shopping/appointment/presentation/state`;

export const getLikesDislikesForAttendeeEndpoint = () =>
  `/api/search/guided-shopping-attendee-product-collection`;

export const addLikesDislikesForAttendeeEndpoint = () =>
  `/api/guided-shopping-attendee-product-collection`;

export const removeLikesDislikesForAttendeeEndpoint = (id: string) =>
  `/api/guided-shopping-attendee-product-collection/${id}`;

export const getInteractionEndpoint = () => `/store-api/guided-shopping/interaction`;

export const getLikedProductsEndpoint = () =>
  `/store-api/guided-shopping/appointment/collection/liked`;

export const getDislikedProductsEndpoint = () =>
  `/store-api/guided-shopping/appointment/collection/disliked`;

export const likedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/liked/${productId}`;

export const getAllLikedProductsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/widgets/wishlist`;

export const addDislikedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/disliked/${productId}`;

export const removeDislikedProductEndpoint = (productId: string) =>
  `/store-api/guided-shopping/appointment/collection/disliked/${productId}`;

export const getLastSeenProductsEndpoint = () =>
  `/store-api/guided-shopping/appointment/collection/last-seen`;

export const getCmsPageByIdEndpoint = (pageId: string) => `/store-api/cms/${pageId}`;

export const updateAttendeeEndpoint = () => `/store-api/guided-shopping/appointment/attendee`;

export const getQuickViewEndpoint = (productId: string, cmsPageLayoutId: string) =>
  `/store-api/guided-shopping/quickview/${productId}/${cmsPageLayoutId}`;

export const getGuideCartEndpoint = (salesChannelId: string) =>
  `/api/_proxy/store-api/${salesChannelId}/checkout/cart`;

export const getGuideAllCartsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/widgets/cart-statistics`;

export const getGuideCartLineItemEndpoint = (salesChannelId: string) =>
  `/api/_proxy/store-api/${salesChannelId}/checkout/cart/line-item`;

export const getAppointmentByPathEndpoint = () => `/api/search/guided-shopping-appointment`;

export const getCartInsightsEndpoint = (appointmentId: string) =>
  `/api/guided-shopping/appointment/${appointmentId}/widgets/cart-insights`;

export const getOrderLineItemEndpoint = () => `/api/search/order-line-item`;

export const getOrderCustomerEndpoint = () => `/api/search/order-customer`;

export const getAttendeeEndpoint = () => `/api/search/guided-shopping-appointment-attendee`;

export const getAttendeeInsightsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/widgets/attendee-insights`;

export const getProductStreamsEndpoint = () => `/api/search/product-stream`;

export const getSyncProductsEndpoint = (appointmentId: string) =>
  `/api/_action/guided-shopping/appointment/${appointmentId}/instant-listing`;

export const getAttendeeContextTokenEndpoint = (attendeeId: string) =>
  `/api/_action/guided-shopping/appointment/attendee/${attendeeId}/sw-context-token`;

export const getAllProductsWithDataEndpoint = () => `/store-api/guided-shopping/product-listing`;

export const respondInvitationEndpoint = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/attendee/respond-invitation`;

export const getVideoAndAudioSetting = (appointmentId: string) =>
  `/store-api/guided-shopping/appointment/${appointmentId}/video-audio-settings`;

export const getShopPageEndpoint = (layoutName: ShopPage) =>
  `/store-api/guided-shopping/shop-pages/${layoutName}`;

export const getAdminProductEndpoint = () => `/api/search/product`;

export const getDeliveryTimeEndpoint = () => `/api/search/delivery-time`;

export const getProductReviewsEndpoint = (productId: string) =>
  `store-api/product/${productId}/reviews`;

export const getCmsProductDetailPage = (productId: string) =>
`store-api/guided-shopping/product/${productId}`;
