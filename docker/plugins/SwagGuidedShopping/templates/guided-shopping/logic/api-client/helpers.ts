import { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';

export const buildRequestConfig = (options: {
  controller?: AbortController;
  headers?: AxiosRequestHeaders;
  params?: any;
}) => {
  const temp: AxiosRequestConfig = {};
  if (options.controller) {
    temp.signal = options.controller.signal;
  }
  const { headers, params } = options;
  return { ...temp, headers, params };
};
