import { Readable } from 'stream';
import { SitemapStream, streamToPromise } from 'sitemap';
import getURL from 'requrl';
import sitemapObject from '../sitemap';

export default defineEventHandler((event) => {
  const stream = new SitemapStream({ hostname: getURL(event.node.req) });
  event.node.res.setHeader('Content-Type', 'application/xhtml+xml');

  return streamToPromise(Readable.from(sitemapObject).pipe(stream)).then((data) => data.toString());
});
