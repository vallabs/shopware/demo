import { SliderElementConfig } from '@shopware-pwa/composables-next';
import { Product as IProduct } from '@shopware-pwa/types/shopware-6-client/models/content/product/Product';
import { LineItem as ILineItem } from '@shopware-pwa/types/shopware-6-client/models/checkout/cart/line-item/LineItem';
import { Cart as ICart } from '@shopware-pwa/types/shopware-6-client/models/checkout/cart/Cart';
import { ProductListingResult as IProductListingResult } from '@shopware-pwa/types/shopware-6-client/response/ProductListingResult';
import { ListingResult as IListingResult } from '@shopware-pwa/types/shopware-6-client/response/ListingResult';
import { PropertyGroupOption, ProductMedia, Promotion } from '@shopware-pwa/types';
import { CmsPage as ICmsPage } from '@shopware-pwa/types/shopware-6-client/models/content/cms/CmsPage';
import { CustomerAddress as ICustomerAddress } from '@shopware-pwa/types/shopware-6-client/models/checkout/customer/CustomerAddress';

export type CountryState = {
  name: string;
  id: string;
  countryId: string;
  shortCode: string;
  active: boolean;
  translated: {
    name: string;
  };
};
declare global {
  type AppLoadingConfig = {
    mainClass?: string;
  };
}
declare module '#app' {
  interface NuxtApp {
    $contextToken: CookieRef<string | null | undefined>;
    $appId: RemovableRef<string | null | undefined>;
    $setLoading(loading: boolean, config?: AppLoadingConfig): void;
  }
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $contextToken: CookieRef<string | null | undefined>;
    $appId: RemovableRef<string | null | undefined>;
    $setLoading(loading: boolean): void;
  }
}

declare module '@shopware-pwa/types' {
  type ClientApiError = {
    message?: string;
    messages: ShopwareError[];
    statusCode: number;
  };

  type Product = IProduct & {
    cover?: ProductMedia;
    extensions: {
      attendeeProductCollections?: {
        likes?: number;
        unlikes?: number;
      };
    };
  };

  type LineItem = ILineItem & {
    extensions?: {
      meta?: {
        attendees: { id: string; name: string }[];
      };
    };
    payload: (IProduct | Promotion) & {
      options: { group: string; option: string }[] | null;
    };
  };

  type Cart = ICart & {
    lineItems: LineItem[];
  };

  type ShopwareSearchParams = {
    /**
     *  Not mentioned in the store-api docs
     */
    p?: number | undefined;
    page?: number | undefined;
    limit?: number | undefined;
    filter?: Array<EqualsFilter | EqualsAnyFilter | RangeFilter | MultiFilter> | undefined;
    sort?: Array<StoreSort> | undefined;
    postFilter?: Array<EqualsFilter | EqualsAnyFilter | RangeFilter | MultiFilter> | undefined;
    associations?: ShopwareAssociation;
    aggregations?: Array<Aggregation> | undefined;
    grouping?: Array<Grouping>;

    /**
     *  Not mentioned in the store-api docs
     */
    order?: string | undefined;
    ids?: string[];
    properties?: string | string[] | undefined;
    manufacturer?: string | string[] | undefined;
    includes?: Includes;
    'min-price'?: number | undefined;
    'max-price'?: number | undefined;
    'shipping-free'?: boolean | undefined;
    rating: number | undefined;
    query?: string | ShopwareSearchQueryItem[];
    term?: string;
    useIdSorting?: boolean;
    loadAllIds?: boolean;
    loadVariants?: boolean;
  };

  type ShopwareSearchQueryItem = {
    score: number;
    query: { type: string; field: string; value: string };
  };

  type ListingResult<T> = IListingResult<T> & {
    extensions?: {
      allIds?: {
        total: number;
        data: string[];
      };
    };
  };

  type CmsPageType =
    | 'product_list'
    | 'landingpage'
    | 'product_detail'
    | 'presentation_product_detail'
    | 'presentation_product_list'
    | 'presentation_landingpage';

  type CmsPage = Omit<ICmsPage, 'type'> & {
    type: CmsPageType;
  };

  type CustomerAddress = Omit<ICustomerAddress, 'countryState'> & {
    countryState: CountryState;
  };
}

declare module '@shopware-pwa/composables-next' {
  type CustomListingType =
    | 'productSearchListing'
    | 'categoryListing'
    | 'presentationListing'
    | 'productsFiltering' // all products in the filtering modal
    | 'productsListing'; // products of the instant listing

  type CustomSliderElementConfig = SliderElementConfig & {
    rotate?: ElementConfig<boolean>;
    autoSlide?: ElementConfig<boolean>;
    autoplayTimeout?: ElementConfig<number>;
    speed?: ElementConfig<number>;
  };
}

export {};
