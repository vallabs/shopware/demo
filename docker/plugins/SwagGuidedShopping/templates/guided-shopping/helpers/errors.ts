import { ClientApiError } from '@shopware-pwa/types';

/**
 * Helper to help getting error messages
 *
 * @public
 * @function
 * @param errors Errors
 * @param multiple The error(s) return is multiple or not
 */
export const getErrorMessages = (errors: ClientApiError, multiple?: boolean): string[] | string => {
  if (multiple) {
    return errors.messages.map((err) => {
      return err.detail ?? err.title ?? 'errors.unknown';
    });
  } else {
    return errors.messages[0]?.detail ?? errors.messages[0]?.title ?? 'errors.unknown';
  }
};
