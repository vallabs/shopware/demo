export const DAILY_PRIVACY_POLICY_LINK = 'https://www.daily.co/legal/privacy';
export const COOKIE_LIFESPAN: number = 60 * 60 * 24; // min for cookie save
export const PRE_EXPIRE_TOKEN_REFRESH_TIME = 10 * 1000;
export const COOKIE_PATH = '/';
export const PRODUCT_LIST_PAGE_SIZE = 36;
export const ORDER_LIST_PAGE_SIZE = 10;
export const SIDEBAR_ANIMATION_TIME = 500; // ms
export const PROMOTION_TYPE = 'promotion';
export const SALUTATION_DEFAULT = 'not_specified';
export const MODAL_CONTAINER_ID = 'modal-container';
export const DOWNLOAD_FLAG = 'is-download';
