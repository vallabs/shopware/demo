export const formatProductOptions = (
  options: { group?: string; name: string }[] = [],
  separator = ', ',
) => {
  return (
    options
      .sort((a, b) =>
        (a?.group ?? '') < (b?.group ?? '') ? -1 : (a?.group ?? '') > (b?.group ?? '') ? 1 : 0,
      )
      .map((option) => option.name)
      .join(separator) || ''
  );
};
