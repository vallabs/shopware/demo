import type { CustomListingType } from '@shopware-pwa/composables-next';
import { Product } from '@shopware-pwa/types';
import { ParticipantState, SlideItem } from '~/logic/interfaces';

export const CmsPageKey = Symbol('CmsPageKey') as InjectionKey<{
  type: string;
  options: SlideItem | undefined;
}>;

export const CmsPageProductDetailKey = Symbol('CmsPageProductDetailKey') as InjectionKey<
  Ref<Product | undefined>
>;

export const CmsSlotKey = Symbol('CmsSlotKey') as InjectionKey<{
  elementId: string;
}>;

export const ProductListingKey = Symbol('ListingType') as InjectionKey<{
  listingType: CustomListingType;
}>;

export const ParticipantsControlKey = Symbol('ParticipantsControlKey') as InjectionKey<{
  openStatistics: (participant: ParticipantState) => void;
  actForUser: (participant: ParticipantState) => void;
}>;

export const ProductFilteringControlKey = Symbol('ProductFilteringControlKey') as InjectionKey<{
  add: (ids: { [k: string]: boolean }) => void;
  remove: (ids: { [k: string]: boolean }, refresh?: boolean) => void;
  removeAll: () => void;
  showBookmarkShortcut: boolean;
}>;
