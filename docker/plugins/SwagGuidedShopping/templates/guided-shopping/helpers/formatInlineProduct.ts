import { getProductName, getSmallestThumbnailUrl } from '@shopware-pwa/helpers-next';
import { Product } from '@shopware-pwa/types';
import { formatProductOptions } from './formatProductOptions';

export const formatInlineProduct = (product: Product) => {
  return {
    name: getProductName({ product }),
    options: formatProductOptions(
      product?.options?.map((option) => ({
        group: option.group?.name,
        name: option.translated.name,
      })),
    ),
    image: getSmallestThumbnailUrl(product.cover?.media) || product.cover?.media?.url || '',
  };
};
