import dayjs from 'dayjs';

export const chunkArray = (arr: any[], chunkSize: number) => {
  const chunkedArray = [];
  let id = 0;

  for (let i = 0; i < arr.length; i += chunkSize) {
    chunkedArray.push({
      id,
      data: arr.slice(i, i + chunkSize),
    });
    id++;
  }

  return chunkedArray;
};

export const formatDate = (dateStr: string, format = 'DD.MM.YYYY') => {
  return dayjs(dateStr).format(format);
};

export const truncateTexts = (texts: string[], amount = 15) => {
  return {
    texts: texts?.slice(0, texts.length < amount ? texts.length : amount).join(', '),
    more: texts?.length > amount ? texts.length - amount : 0,
  };
};
