import account from './account.json';
import form from './form.json';
import changePassword from './changePassword.json';
import checkout from './checkout.json';
import general from './general.json';
import cart from './cart.json';
import listing from './listing.json';
import product from './product.json';
import newsletter from './newsletter.json';
import auth from './auth.json';
import validation from './validation.json';
import presentation from './presentation.json';

export default Object.assign(
  account,
  form,
  changePassword,
  checkout,
  general,
  cart,
  listing,
  product,
  newsletter,
  auth,
  validation,
  presentation,
);
