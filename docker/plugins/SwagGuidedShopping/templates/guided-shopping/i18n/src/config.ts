/**
 * i18n configuration
 */
export default {
  defaultLocale: 'en-US',
  locales: [
    {
      code: 'en-GB',
      iso: 'en-GB',
      file: 'en-US.ts',
    },
    {
      code: 'en-US',
      iso: 'en-US',
      file: 'en-US.ts',
    },
    {
      code: 'de-DE',
      iso: 'de-DE',
      file: 'de-DE.ts',
    },
  ],
};
