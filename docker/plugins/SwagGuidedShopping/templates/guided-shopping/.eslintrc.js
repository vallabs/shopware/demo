module.exports = {
  root: true,
  extends: [
    'plugin:@typescript-eslint/recommended',
    '@nuxtjs/eslint-config-typescript',
    'plugin:nuxt/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:prettier/recommended',
    'plugin:@intlify/vue-i18n/recommended',
  ],
  plugins: ['prefer-arrow'],
  settings: {
    'vue-i18n': {
      localeDir: './i18n/**/*.json',
      messageSyntaxVersion: '^9.0.0',
    },
  },
  parser: 'vue-eslint-parser',
  ignorePatterns: ['dist/', 'node_modules/'],
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    'vue/multi-word-component-names': 'off',
    // https://eslint.vuejs.org/rules/component-tags-order.html
    'vue/component-tags-order': [
      'error',
      {
        order: ['script', 'template', 'style'],
      },
    ],
    'vue/component-name-in-template-casing': [
      'error',
      'PascalCase',
      {
        registeredComponentsOnly: false,
        // https://github.com/intlify/vue-i18n-next/issues/829
        ignores: ['i18n-t'],
      },
    ],
    'vue/no-multiple-template-root': 'off',
    // https://eslint-plugin-vue-i18n.intlify.dev/rules/#recommended
    '@intlify/vue-i18n/no-v-html': 'off',
    '@intlify/vue-i18n/no-html-messages': 'off',
    '@intlify/vue-i18n/no-missing-keys': 'error',
    '@intlify/vue-i18n/valid-message-syntax': 'error',
    '@intlify/vue-i18n/no-duplicate-keys-in-locale': 'error',
    '@intlify/vue-i18n/no-missing-keys-in-other-locales': 'error',
    '@intlify/vue-i18n/key-format-style': ['error', 'camelCase'],
    'prefer-arrow/prefer-arrow-functions': [
      'warn',
      {
        disallowPrototype: true,
        classPropertiesAllowed: false,
      },
    ],
  },
  overrides: [
    {
      files: ['./i18n/**/*.json'],
      extends: ['plugin:@intlify/vue-i18n/base'],
    },
  ],
};
