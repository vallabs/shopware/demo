module.exports = {
  plugins: ['prettier-plugin-tailwindcss'],
  endOfLine: 'auto',
  printWidth: 100,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  useTabs: false,
  tabWidth: 2,
  vueIndentScriptAndStyle: true,
};
