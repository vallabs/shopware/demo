export default defineI18nConfig(() => ({
  legacy: false,
  fallbackLocale: 'en',
  allowComposition: true,
  globalInjection: true,
}));
