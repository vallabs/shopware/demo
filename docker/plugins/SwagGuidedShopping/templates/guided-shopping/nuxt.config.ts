import fs from 'node:fs/promises';
import transformerDirective from '@unocss/transformer-directives';
import { presetUno, presetIcons, transformerVariantGroup, presetWebFonts } from 'unocss';
import { presetForms } from '@julr/unocss-preset-forms';
import { theme } from '@unocss/preset-mini';
import colors from './assets/colors';
import i18nConfig from './i18n/src/config';

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  imports: {
    dirs: [
      // Scan top-level modules
      'composables',
      // ... or scan all modules within given directory
      'composables/**',
    ],
  },
  runtimeConfig: {
    public: {
      shopware: {
        // Guided shopping demo instance
        shopwareEndpoint: process.env.SHOPWARE_ENDPOINT,
        shopwareAccessToken: process.env.SHOPWARE_ACCESS_TOKEN,
        // Frontends demo instance
        // shopwareEndpoint: "https://demo-frontends.shopware.store",
        // shopwareAccessToken: "SWSCBHFSNTVMAWNZDNFKSHLAYW",
        devStorefrontUrl: '',
      },
      allowAnonymousMercure: process.env.ALLOW_ANONYMOUS_MERCURE,
    },
  },
  hooks: {
    listen: () => {
      // Run ESLint before the server starts
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      require('child_process').spawnSync('pnpm', ['lint'], { stdio: 'inherit' });
    },
  },
  alias: {
    /**
     * TODO: Temp fix until new VueUse published:
     * - https://github.com/vueuse/vueuse/pull/2449
     * - https://github.com/vueuse/vueuse/actions/workflows/publish.yml
     */
    useMeta: '~/composables/layout/useMeta',
  },
  /**
   * Commented because of the StackBlitz error
   * Issue: https://github.com/shopware/frontends/issues/88
   */
  typescript: {
    typeCheck: true,
    strict: true,
  },
  modules: [
    '@vueuse/nuxt',
    '@unocss/nuxt',
    '@shopware-pwa/nuxt3-module',
    '@shopware-pwa/cms-base',
    '@nuxt/devtools',
    '@nuxtjs/i18n',
    'nuxt-swiper',
  ],
  css: ['vue-virtual-scroller/dist/vue-virtual-scroller.css'],
  // components: true,
  components: {
    dirs: [
      {
        path: '~/components',
        pathPrefix: false,
      },
    ],
    global: true,
  },
  vueuse: {
    ssrHandlers: true,
  },
  // Unocss bug fix https://github.com/nuxt/framework/issues/7623
  experimental: {
    inlineSSRStyles: false,
  },
  nitro: {
    compressPublicAssets: true,
  },
  unocss: {
    uno: true, // enabled `@unocss/preset-uno`
    icons: false, // enabled `@unocss/preset-icons`
    attributify: true, // enabled `@unocss/preset-attributify`,
    preflight: true,
    transformers: [transformerDirective(), transformerVariantGroup()],
    layers: {
      components: -1,
      default: 1,
      utilities: 2,
    },
    presets: [
      presetUno(),
      presetForms(),
      presetWebFonts({
        provider: 'google',
        fonts: {
          inter: [
            {
              name: 'Inter',
              weights: ['100', '200', '300', '400', '500', '600', '700', '800', '900'],
              italic: true,
            },
          ],
        },
      }),
      presetIcons({
        mode: 'mask',
        extraProperties: {
          display: 'block',
          'font-size': '1.5rem',
        },
        collections: {
          'sw-solid': (iconName) => {
            return fs.readFile(`./assets/icons/solid/${iconName}.svg`, 'utf-8');
          },
          'sw-regular': (iconName) => {
            return fs.readFile(`./assets/icons/regular/${iconName}.svg`, 'utf-8');
          },
          'sw-custom': (iconName) => {
            return fs.readFile(`./assets/icons/custom/${iconName}.svg`, 'utf-8');
          },
        },
      }),
    ],
    theme: {
      extend: {
        width: 'width',
        height: 'height',
      },
      colors: {
        ...colors,
        border: colors.gray[400],
        primary: {
          DEFAULT: colors.shopware_brand[900],
          hover: colors.brand_vivacious[600],
          pressed: colors.brand_vivacious[700],
          disabled: colors.shopware_brand[200],
        },
        background: {
          DEFAULT: colors.white,
          raised: colors.gray[50],
          elevated: colors.gray[100],
          dark: colors.darkgray[900],
        },
        information: {
          DEFAULT: colors.darkgray[600],
          dark: colors.darkgray[900],
          subdued: colors.darkgray[200],
          disabled: colors.gray[400],
        },
        interactive: {
          DEFAULT: colors.white,
          hover: colors.gray[100],
          pressed: colors.gray[200],
        },
        danger: {
          DEFAULT: colors.crimson[500],
        },
      },
      boxShadow: {
        '1': '0px 1px 1px rgba(0, 0, 0, 0.08)',
        '2': '0px 2px 2px rgba(0, 0, 0, 0.08)',
        '3': '0px 2px 8px rgba(0, 0, 0, 0.08)',
        '4': '0px 0px 16px rgba(0, 0, 0, 0.08)',
        '5': '0px 12px 24px rgba(0, 0, 0, 0.08)',
      },
      breakpoints: {
        ...theme.breakpoints,
        '3xl': '1920px',
        '4xl': '2560px',
      },
    },
    shortcuts: {
      'z-sidebar': 'z-[1000]',
      'z-target': 'z-[1005]',
      'z-toolbar': 'z-[1010]',
      'z-modal': 'z-[1015]',
      'z-toasts': 'z-[1020]',
      'z-loading': 'z-[1025]',
      'z-backdrop': 'z-[1030]',
      'z-videoview': 'z-[1035]',
      'z-tooltip': 'z-[1040]',
      'aspect-product-standard-thumbnail': 'aspect-[1/1]',
      'aspect-product-big-thumbnail': 'aspect-[4/5]',
    },
  },
  router: {
    options: {
      linkExactActiveClass: 'text-brand-primary',
    },
  },
  i18n: {
    vueI18n: './i18n.config.ts',
    locales: i18nConfig.locales,
    strategy: 'prefix_except_default',
    defaultLocale: i18nConfig.defaultLocale,
    langDir: './i18n/src/langs/',
    experimental: {
      jsTsFormatResource: true,
    },
  },
});
