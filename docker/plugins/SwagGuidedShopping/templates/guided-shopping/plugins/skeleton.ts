export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.directive('skeleton', (el, binding) => {
    let $span;
    if (binding.value) {
      el.classList.add('relative', 'text-transparent');
      $span = document.createElement('span');
      $span.className = 'absolute left-0 top-0 animate-pulse bg-gray-100';
      $span.style.minWidth = `100px`;
      $span.style.width = `${el.offsetWidth}px`;
      $span.style.height = `${el.offsetHeight}px`;
      el.appendChild($span);
    } else {
      el.classList.remove('relative', 'text-transparent');
      if ($span) {
        ($span as HTMLSpanElement).remove();
      }
    }
  });
});
