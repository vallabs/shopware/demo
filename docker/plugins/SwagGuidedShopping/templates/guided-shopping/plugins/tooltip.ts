import { isTouchDevice } from '~/helpers/isTouchDevice';

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.directive('tooltip', {
    mounted: (el, binding) => {
      let $span: HTMLElement | null;
      const { content, maxWidth } = binding.value;
      const eventIn = isTouchDevice() ? 'touchstart' : 'mouseenter';
      const eventOut = isTouchDevice() ? 'touchend' : 'mouseleave';

      const handleHoverIn = () => {
        if ($span) return;
        const { getPanelStyling } = usePopover(el, false);
        const styling = getPanelStyling();
        $span = document.createElement('span');
        $span.innerHTML = content;
        $span.style.maxWidth = maxWidth ?? '10rem';
        $span.className =
          'text-center bg-background-dark pointer-events-none fixed z-tooltip rounded-md p-2 text-white text-xs font-medium';
        if (styling.bottom) $span.style.bottom = styling.bottom;
        if (styling.left) {
          $span.style.left = styling.leftValue + styling.elWidthValue / 2 + 'px';
          $span.style.transform = 'translateX(-50%)';
        }
        if (styling.top) $span.style.top = styling.top;
        if (styling.right) {
          $span.style.right = styling.rightValue - styling.elWidthValue / 2 + 'px';
          $span.style.transform = 'translateX(50%)';
        }
        document.body.appendChild($span);
      };

      const handleHoverOut = () => {
        if ($span) {
          ($span as HTMLElement).remove();
          $span = null;
        }
      };

      (binding.dir as any).methods = {
        handleHoverIn,
        handleHoverOut,
      };
      (binding.dir as any).data = {
        eventIn,
        eventOut,
      };

      el.addEventListener(eventIn, (binding.dir as any).methods.handleHoverIn);
      el.addEventListener(eventOut, (binding.dir as any).methods.handleHoverOut);
    },
    beforeUnmount: (el, binding) => {
      if (el && (binding.dir as any).data && (binding.dir as any).methods) {
        el.removeEventListener(
          (binding.dir as any).data.eventIn,
          (binding.dir as any).methods.handleHoverIn,
        );
        el.removeEventListener(
          (binding.dir as any).data.eventOut,
          (binding.dir as any).methods.handleHoverOut,
        );
        delete (binding.dir as any).data;
        delete (binding.dir as any).methods;
      }
    },
  });
});
